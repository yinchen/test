# auto.root

This repository is the automation code for the product IBM DSM (Data Server Manager) enterprise and IBM Db2 warehouse console.

The automation code mainly includes two parts: API test and UI test.

### Structure

The entrance for the automation is the folder **auto.root**.
In the following, let's introduce sub-folders one by one:

- **auto.api**: 
This is API test code. 

	 - **console**:
	 This is API test code for product IBM Db2 warehouse console

	 - **enterprise**:
	 This is API test code for product IBM DSM enterprise

- **auto.ui**:
This is UI test code.
	
	 - **com.ibm.datatools.dsm.tests.ui.base**:
	 This is UI test **base** code for all products.
	 
	 - **com.ibm.datatools.dsm.tests.ui.console**:
	 This is UI test code for product IBM Db2 warehouse console
	 
	 - **com.ibm.datatools.dsm.tests.ui.enterprise**:
	 This is UI test code for product IBM DSM enterprise


- **auto.base.utils**:
This is utils code, used by API and UI test.

- **personal_bvt**:
This is shell scripts, used for personal bvt.

- **setup**:
This is shell scripts, used for setup the DSM enterprise.

### How to run the automation

1. Install the maven 
We used the maven to run the test, so please make sure maven is installed successfully before run the test.

2. Update the maven central repository to CDL internal nexus repository server
	
```
	<mirror>	
		<id>nexus</id>			
		<name>internal nexus repository</name>				
		<url>http://9.30.33.109:8081/nexus/content/groups/public/</url>		
		<mirrorOf>central</mirrorOf>		
	</mirror>
	
```


3. Go to the automation entrance folder **auto.root**, use the below command to run the test.

*mvn clean test -P ${profile}*

${profile} is variable, please replace the value for the variable in practise.

for example:
mvn clean test -P Ent_UI_BVT


- **DSM enterprise**:	

	- **API Test**:

	Ent_API_DBVT: Used for DSM enterprise API daily build verification test

	Ent_API_DReg: Used for DSM enterprise API daily regression test
	
	Ent_API_PBVT: Used for DSM enterprise API personal build verification test
	
	Ent_API_PReg: Used for DSM enterprise API personal regression test

	- **UI Test**:

	Ent_UI_BVT: Used for DSM enterprise UI build verification test
	
	Ent_UI_Reg: Used for DSM enterprise UI regression test

- **IBM Db2 warehouse console**:

	- **API Test**:
	
	Con_API_FVT2: Used for IBM Db2 warehouse console function verification V2 test.
	
	Con_API_FVT3: Used for IBM Db2 warehouse console function verification V3 test.	

	- **UI Test**:
	
	Con_UI_BVT: Used for IBM Db2 warehouse console build verification test


**Note:**
For UI test, please make sure the firefox browser 48 or 49 is installed. 
Please refer to the the script "setupFirefox.sh" for guideline.
	






# frisby

The project is moved from https://github.ibm.com/tools-for-aps/webauto/tree/master/dashdb-data-movement/restapitest/frisby, please refer to the frisby/README.md for its instruction. 















