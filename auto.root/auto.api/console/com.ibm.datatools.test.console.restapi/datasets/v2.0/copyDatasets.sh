#!/bin/bash

sudo cp ~/datasets/*.csv ~/../$1/
sudo chmod 660 ~/../$1/*.csv
sudo chown $1:db2iadm1 ~/../$1/*.csv

sudo cp ~/datasets/*.dat ~/../$1/
sudo chmod 660 ~/../$1/*.dat
sudo chown $1:db2iadm1 ~/../$1/*.dat
