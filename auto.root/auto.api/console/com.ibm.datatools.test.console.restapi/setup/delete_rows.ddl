------------------------------------------
-- Delete rows from all test tables 
-- Faster than dropping and recreating tables 
-- when running tests in a dev environment 
------------------------------------------

DELETE FROM "BIGTABLE"@
DELETE FROM "ORDER_DETAILS"@
DELETE FROM "ORDER_DETAILS_1.0"@
-- DELETE FROM "ORDER_DETAILS2"@
-- DELETE FROM "ORDER_DETAILS3"@
-- DELETE FROM "ORDER_DETAILS4"@
DELETE FROM "ORDER_DETAILS_RC1"@
DELETE FROM "ORDER_DETAILS_RC2"@
DELETE FROM "ORDER_DETAILS_RC3"@
DELETE FROM "TINY_TABLE"@
DELETE FROM "TinyTable"@
DELETE FROM "*Tiny_Table" @
DELETE FROM "TINY_TABLE_1.0"@
DELETE FROM "TINY_TABLE2"@
DELETE FROM "TINY_TABLE3"@
DELETE FROM "TINY_TABLE_CP1"@
DELETE FROM "TINY_TABLE_CP2"@
DELETE FROM "TINY_TABLE_CP3"@
DELETE FROM "TINY_TABLE_CP4"@
DELETE FROM "TINY_TABLE_CD1"@
DELETE FROM "TINY_TABLE_CD2"@
DELETE FROM "TINY_TABLE_CD3"@
DELETE FROM "TINY_TABLE_CD4"@
DELETE FROM "TINY_TABLE_CD5"@
DELETE FROM "TINY_TABLE_CD6"@
DELETE FROM "TINY_TABLE_CD7"@
DELETE FROM "TINY_TABLE_CD8"@
DELETE FROM "TINY_TABLE_3COL"@
DELETE FROM "TINY_TABLE_3COL_CD9"@
DELETE FROM "TINY_TABLE_3COL_CD10"@
DELETE FROM "TINY_TABLE_3COL_SD1"@
DELETE FROM "TINY_TABLE_3COL_SD2"@
DELETE FROM "TINY_TABLE_3COL_SD3"@
DELETE FROM "TINY_TABLE_3COL_SD4"@
DELETE FROM "TINY_TABLE_3COL_SD5"@
DELETE FROM "TINY_TABLE_3COL_SD6"@
DELETE FROM "TINY_TABLE_3COL_SD7"@
DELETE FROM "TINY_TABLE_3COL_SD8"@
DELETE FROM "TINY_TABLE_3COL_SD9"@
DELETE FROM "TINY_TABLE_3COL_SD10"@
DELETE FROM "TINY_TABLE_HR1"@
DELETE FROM "TINY_TABLE_HR2"@
DELETE FROM "TINY_TABLE_HR3"@
DELETE FROM "TINY_TABLE_HR4"@
DELETE FROM "HIDDENCOLTABLE"@
DELETE FROM "ERRORTABLE"@
DELETE FROM "TAB_WITH_TRAILING_SPACE  "@
DELETE FROM "  TAB_WITH_LEADING_SPACE"@
DELETE FROM "DUMPFILE_TEST_TABLE"@
DELETE FROM "TIME_TABLE"@
DELETE FROM "DATE_TABLE"@
DELETE FROM "TIMESTAMP_TABLE"@
DELETE FROM "IDENTITYALWAYSTABLE"@
DELETE FROM "IDENTITYDEFAULTTABLE"@
DELETE FROM "LOAD_S3_TABLE"@
DELETE FROM "LOAD_SOFTLAYER_TABLE"@