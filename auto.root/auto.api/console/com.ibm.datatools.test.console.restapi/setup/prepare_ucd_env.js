//
//
// SET THE ENV VARS BASED ON UCD PROPERTIES
//
//

// DEPENDANCIES
var request = require('request'),
    path = require('path'),
    fs = require('fs');

// CONSTANTS
var myname = path.basename(__filename);
var ucdHost = "ucdeploy.swg-devops.com";
var adminUserID = "blutestadmin";
var adminPwd = "W0nderland!!";
var userID = "blutest";
var userPwd = "Password!234";


function usage() {
    console.log("Usage\n  node " + myname + " <ucd-username> <ucd-pass> <ucd-server-name>");
    process.exit(1);
}

if (process.argv.length != 5) {
    usage();
}

// Params we will get from UCD
var LDAPpassword = "";
var DB2password = "";
var IP = "";

// get input params
var username = process.argv[2];
var password = process.argv[3];
var serverName = process.argv[4];

console.log('Fetching env properties for ' + serverName + ' from UCD...');

// MAIN FUNCTION TO REQUEST PARAMS
request({
    url: "https://" + username + ":" + password + "@" + ucdHost + "/cli/environment/getProperties?application=dashDB&environment=" + serverName,
    method: 'GET'
}, function(err, resp, body) {
    if (err) {
        console.log(err);
        //Return an exit code of 1
        process.exit(1);
    } else if (resp.statusCode == 200) {
        console.log('SUCCESS!');
        var envobj = JSON.parse(body);
        var property;

        envobj.forEach(function(property) {
            if (property["name"] == "LDAP_PW") {
                LDAPpassword = property["value"];
            }
            if (property["name"] == "DB2_PW") {
                DB2password = property["value"];
            }
            if (property["name"] == "BLU_URL") {
                IP = property["value"].trim().replace(":8443", "").replace("https://", "");
            }
        });
        //Check if we found an ID to match
        if (LDAPpassword == "") {
            //Did not find it!
            console.log("ERROR: Unable to find the LDAPpassword for " + serverName);
            //Return an exit code of 1
            process.exit(1);
        }
        //Check if we found an ID to match
        if (DB2password == "") {
            //Did not find it!
            console.log("ERROR: Unable to find the DB2password for " + serverName);
            //Return an exit code of 1
            process.exit(1);
        }
        //Check if we found an ID to match
        if (IP == "") {
            //Did not find it!
            console.log("ERROR: Unable to find the URL for " + serverName);
            //Return an exit code of 1
            process.exit(1);
        }

        // Prepare users on the machine...
        createUsers();

    } else {
        //We did not get the expected status code from the API call
        console.log("ERROR: StatusCode of " + resp.statusCode + " from call to /cli/environment/getProperties");
        //Return an exit code of 1
        process.exit(1);
    }

});

function createUsers() {
    let token = "";
    // Allow connection using self signed certificate 
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    let authBody = { "userid": "bluadmin", "password": LDAPpassword };
    let adminBody = {
        "id": adminUserID,
        "password": adminPwd,
        "role": "bluadmin",
        "email": "admin@ca.ibm.com"
    };
    let userBody = {
        "id": userID,
        "password": userPwd,
        "role": "bluusers",
        "email": "user@ca.ibm.com"
    };

    console.log('Authenticating with console v2...')
    request.post({
        url: "https://" + IP + "/dashdb-api/v2/auth",
        method: 'POST',
        json: authBody
    }, function(err, resp, body) {
        if (err) {
            console.log(err);
            //Return an exit code of 1
            process.exit(1);
        } else if (resp.statusCode == 200) {
            console.log('SUCCESS!\nCreating admin user ' + adminUserID + ' on ' + serverName + '...');
            token = resp.body.token;
            request.post({
                url: "https://" + IP + "/dashdb-api/v2/users",
                method: 'POST',
                json: adminBody,
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            }, function(err, resp, body) {
                if (err) {
                    console.log(err);
                    //Return an exit code of 1
                    // process.exit(1);
                } else if (resp.statusCode == 201) {
                    console.log('SUCCESS!')
                } else {
                    //We did not get the expected status code from the API call
                    console.log("ERROR: StatusCode " + resp.statusCode + " from call to create admin user: ", body);
                    //Return an exit code of 1
                    // process.exit(1);
                }
            });
            console.log('Creating nonadmin user ' + userID + ' on ' + serverName + '...');
            request.post({
                url: "https://" + IP + "/dashdb-api/v2/users",
                method: 'POST',
                json: userBody,
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            }, function(err, resp, body) {
                if (err) {
                    console.log(err);
                    //Return an exit code of 1
                    // process.exit(1);
                } else if (resp.statusCode == 201) {
                    console.log('SUCCESS!')
                } else {
                    //We did not get the expected status code from the API call
                    console.log("ERROR: StatusCode of " + resp.statusCode + " from call to create user", body);
                    //Return an exit code of 1
                    // process.exit(1);
                }
            });
            setProps();
        } else {
            //We did not get the expected status code from the API call
            console.log("ERROR: StatusCode of " + resp.statusCode + " from call to https://" + IP + "/dashdb-api/v2/auth", resp.body.error);
            //Return an exit code of 1
            process.exit(1);
        }
    });
}

function setProps() {
    let script = 'export DASHDB_HOST=' + IP +
        '\nexport NONADMIN_USERID=' + userID +
        '\nexport NONADMIN_USERPWD=' + userPwd +
        '\nexport ADMIN_USERID=' + adminUserID +
        '\nexport ADMIN_USERPWD=' + adminPwd +
        '\n'

    fs.writeFile("./export.sh", script, function(err) {
        if (err) {
            console.log("ERROR: could not create export script with UCD params");
            //Return an exit code of 1
            process.exit(1);
        }

        console.log("Saved settings to ./export.sh");
    });
}