package com.ibm.datatools.test.console.restapi.testcases;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.json.JSON;
import org.apache.commons.json.JSONArray;
import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;

import com.ibm.datatools.test.utils.HTTPClient;
import com.ibm.datatools.test.utils.JsonUtil;
import com.ibm.datatools.test.utils.RESTUtils;
import com.ibm.datatools.test.utils.Response;
import com.ibm.datatools.test.utils.Setting;



public class TestCase {
	Setting setting = Setting.getSetting();
	
	public JSONObject testCall(boolean isAdmin, JSONObject detail) throws Exception {
		JSONObject ret = null;
		HTTPClient client = new HTTPClient(isAdmin);
		
		//replace the variable in the JSON string		
		String strDetail = detail.toString();
		strDetail = RESTUtils.replaceKnownParams(strDetail, client.getUser(), client.getPassword());
		detail = (JSONObject) JSON.parse(strDetail);
		
		JSONObject out = detail.getJSONObject("out");
		JSONObject in = detail.getJSONObject("in");		
		
		Response res = null;
		
		String api = null;
		String prefixURL = in.optString("prefixURL");
		String postURL = in.optString("url");
		String method = detail.getString("method");
		
		int t = detail.optInt("wait");
		Thread.sleep(t*1000);
		
		if(prefixURL != null) {
			api = setting.getPrefixURL() + "/" + prefixURL + "/" + postURL;
		}
		else {
			api = setting.getURL() + "/" + postURL;
		}		
		System.out.println("api is: "+api);		
		
		boolean upload = false;
		if(in.containsKey("uploadfile")) {			
			res = client.call(true, in.getString("uploadfile"), method, api);
			upload = true;
		}
		else {			
		    res = client.call(method, api, in.optString("json"));	
		}
		
		if (setting.generateCurl()) {
			generateCurl(isAdmin, api, method, in.optString("json"), upload, in.optString("uploadfile"));
		}
		
		ret = res.getJsonResult();
		if (res.getResult().length() < 1100) {
			System.out.println("in TestCase testCall() result for case \""+ detail.optString("description") + "\"" );
			System.out.println(res.getResult());
		}
		if (null != out.opt("code"))
			assertEquals(res.getStatus(), out.getInt("code"));	
		
		
		
		//for out-json
		String js_path = out.optString("json_path");
		JSONObject js = out.optJSONObject("json");
		if(js_path != null && !js_path.isEmpty()) {			
			JSONArray jsonArray = res.getJsonResult().getJSONArray(js_path.split("\\.")[0]);
			int count = 0;
			for(int i=0; i<jsonArray.size(); i++) {
				try {
					verifyResultForJSON(jsonArray.getJSONObject(i),js);
				}
				catch (Exception e) {
					count ++;
				}				
			}
			if(count == jsonArray.size())
				throw new Exception("Expect "+js+" but none of the key are matched");
		} else if (res.getJsonResult() != null) {
			verifyResultForJSON(res.getJsonResult(),js);
		} else if(res.getJsonArrayResult()!=null){
			for(int i=0; i<res.getJsonArrayResult().size(); i++) {
				verifyResultForJSON(res.getJsonArrayResult().getJSONObject(i),js);
			}	
		}
		
		
		
		//for out-json_types
		js_path = out.optString("json_types_path");
		js = out.optJSONObject("json_types");
		if(js_path != null && !js_path.isEmpty()) {			
			JSONArray jsonArray = res.getJsonResult().getJSONArray(js_path.split("\\.")[0]);
			for(int i=0; i<jsonArray.size(); i++) {				
				verifyResultForJSONTypes(jsonArray.getJSONObject(i),js);							
			}
		}else if(res.getJsonResult()!=null){
			verifyResultForJSONTypes(res.getJsonResult(),js);
		}else if(res.getJsonArrayResult()!=null){
			for(int i=0; i<res.getJsonArrayResult().size(); i++) {
				verifyResultForJSONTypes(res.getJsonArrayResult().getJSONObject(i),js);
			}	
		}
		
		//for out-header
		js = out.optJSONObject("header");
		if (null != js) {
			Iterator it = js.keys();
			Map<String, List<String>> act = res.getHeaders();
			while (it.hasNext()) {
				String key = (String) it.next();
				List values = act.get(key.toLowerCase());
				String actv = "", exp = js.optString(key);
				if (null != values)
					actv = values.toString();
				if (!actv.contains(exp))
					throw new Exception("expect "+ exp + " in header " + key + " but it's " + actv);
			}
			
		}
		
		//for body
		String body = out.optString("body");
		if(null != body){
			assertEquals(res.getResult(), body);	
		}
		
		return ret;
	}	
	
	
	private void verifyResultForJSON(JSONObject act, JSONObject exp) throws Exception {		
		
		if (null != exp) {
			Iterator it = exp.keys();			
			while(it.hasNext()) {
				String key = (String) it.next();
				if(JsonUtil.isGoodJson(act.optString(key))) {
					verifyResultForJSON(act.optJSONObject(key), exp.optJSONObject(key));
				}else if(JsonUtil.isGoodJsonArray(exp.optString(key))) {					
					JSONArray expArray = (JSONArray)JSON.parse(exp.optString(key));
					JSONArray actArray = (JSONArray)JSON.parse(act.optString(key));					
					for(int i=0; i<expArray.size(); i++) {
						if (i == actArray.size()) {//act array has less keys than expect array
							throw new Exception("expect result size for "+key+" is "+expArray.size()+" but get only "+actArray.size());
						}
						Object acttmp = actArray.opt(i);
						if (acttmp instanceof JSONObject)
							verifyResultForJSON((JSONObject)acttmp, expArray.getJSONObject(i));
						else
							assertEquals(acttmp, expArray.opt(i));
					}					
				}else
				{
					String exps = exp.optString(key), acts = act.optString(key);
					if (exps != null && exps.startsWith("&P&")) {
						if (!acts.matches(exps.substring(3)))
							throw new Exception("The valud of " + key + " " + acts + " doesn't match "+ exps);	
					}
					else if(acts!= null && exps!= null && !acts.trim().equals(exps.trim()))
					{
						throw new Exception("expect "+ exps + " but it's "+acts+" for key "+key);
					}
					else if((acts==null && exps!=null) || (acts!=null && exps==null)) {
						throw new Exception("expect "+ exps + " but it's "+acts+" for key "+key);
					}
				}					
				
			}			
		}
	}
	
	private void verifyResultForJSONTypes(JSONObject act, JSONObject exp) throws Exception  {		
		
		if (null != exp) {
			Iterator it = exp.keys();			
			while(it.hasNext()) {
				String key = (String) it.next();
				if(JsonUtil.isGoodJson(exp.optString(key))) {
					verifyResultForJSONTypes(act.getJSONObject(key), exp.getJSONObject(key));
				}else if(JsonUtil.isGoodJsonArray(exp.optString(key))) {					
					JSONArray expArray = (JSONArray)JSON.parse(exp.optString(key));
					JSONArray actArray = (JSONArray)JSON.parse(act.optString(key));					
					for(int i=0; i<expArray.size(); i++) {
						if (i == actArray.size()) {//act array has less keys than expect array
							throw new Exception("expect result size for "+key+" is "+expArray.size()+" but get only "+actArray.size());
						}
						verifyResultForJSONTypes(actArray.getJSONObject(i),expArray.getJSONObject(i));
					}
				}else
				{
					if (!act.containsKey(key))
						throw new Exception("expect contains property "+ key + " but it's absent");
				}
				
			}			
		}		
		
	}
	
	private void generateCurl(boolean isAdmin, String uri, String method, String body, boolean upload, String filepath) {
		StringBuffer sb = new StringBuffer();
		sb.append("curl -k");
		if (upload)
			sb.append(" -H \"Content-Type: multipart/form-data\"");
		else
			sb.append(" -H \"Content-Type: application/json\"");
		
		if (isAdmin)
			sb.append(" -H \"Authorization: Bearer $adminAuthToken\"");
		else
			sb.append(" -H \"Authorization: Bearer $authToken\"");
		
		sb.append(" -X");
		sb.append(" ");
		sb.append(method.toUpperCase());
		sb.append(" \"");
		sb.append(uri);
		sb.append("\"");
		
		if (upload) {
			//curl -k -H "Content-Type: multipart/form-data" -H "Authorization: Bearer $authToken" -X POST "host" -F "data=@2.txt"
			sb.append(" -F \"data=@");
			sb.append(filepath);
			sb.append("\"");
		} else if (method.equalsIgnoreCase("post") || method.equalsIgnoreCase("put")) {
			sb.append(" -d '");
			sb.append(body);
			sb.append("'");
		}
		
		try {
			writeToFile(sb.toString());
		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	private void writeToFile(String command) throws JSONException, IOException {
		Path path = Paths.get(setting.getCurlFileName());
		Files.write(path, "\n".getBytes(), StandardOpenOption.APPEND);
		Files.write(path, command.getBytes(), StandardOpenOption.APPEND);
	}
}
