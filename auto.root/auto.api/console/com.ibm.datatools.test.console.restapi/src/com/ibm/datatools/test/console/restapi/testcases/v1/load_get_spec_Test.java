package com.ibm.datatools.test.console.restapi.testcases.v1;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.test.utils.JsonUtil;
import com.ibm.datatools.test.console.restapi.testcases.TestCase;


public class load_get_spec_Test {
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException, JSONException {		
		return FileTools.readJSONFile("/v1.0/load_get_spec.json");
	}	
	//need update
	@Test(dataProvider = "testdata")
	public void test(Object key,Object inputPara) throws Exception {
		new TestCase().testCall(false, (JSONObject)inputPara);
	}
	
	@DataProvider(name = "testdata2")
	public Object[][] loadJSON2() throws FileNotFoundException, IOException, JSONException {		
		return FileTools.readJSONFile("/v1.0/load_get_spec_invalidChars.json");
	}	
	
	@Test(dataProvider = "testdata2")
	public void testInvalidChars(Object key,Object inputPara) throws Exception {
		String[] invalidChars = {"*","!","\\\\\""};      //invalid characters *,!,"
		for(int i=0; i<invalidChars.length; i++)
		{
			String afterReplacement = JsonUtil.replaceVariable(inputPara.toString(), "invalidChars", invalidChars[i]);
			new TestCase().testCall(false, JsonUtil.toJSONObject(afterReplacement));
		}
		
	}
}
