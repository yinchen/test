package com.ibm.datatools.test.console.restapi.testcases.v1;

import org.apache.commons.json.JSON;
import org.apache.commons.json.JSONArray;
import org.apache.commons.json.JSONObject;
import org.testng.annotations.Test;

import com.ibm.datatools.test.utils.JsonUtil;
import com.ibm.datatools.test.console.restapi.testcases.TestCase;


public class load_local_get_byid_spec_Test {	


	//need to update to validate the json result
	@Test
	public void test() throws Exception {
		
		JSONArray jsons = JsonUtil.getJsonUnderDef("/v1.0/load_local_get_byid_spec.json");
		JSONObject result = new TestCase().testCall(false, jsons.getJSONObject(0));
		
		String afterReplacement = jsons.getJSONObject(1).toString();				
//		afterReplacement = JsonUtil.replaceVariable(afterReplacement,"LOAD_ID", result.getJSONObject("result").getString("LOAD_ID"));
//	    afterReplacement = JsonUtil.replaceVariable(afterReplacement,"TABLE",result.getJSONObject("result").getString("TABLE"));
//	    afterReplacement = JsonUtil.replaceVariable(afterReplacement,"SCHEMA",result.getJSONObject("result").getString("SCHEMA"));
//	    afterReplacement = JsonUtil.replaceVariable(afterReplacement,"START_TIME",result.getJSONObject("result").getString("START_TIME"));
//	    afterReplacement = JsonUtil.replaceVariable(afterReplacement,"LOAD_LOGFILE",result.getJSONObject("result").getString("LOAD_LOGFILE"));
	    
	    afterReplacement = JsonUtil.replaceVariables(afterReplacement, result.optJSONObject("result"));
		new TestCase().testCall(false, (JSONObject) JSON.parse(afterReplacement));
	}

}
