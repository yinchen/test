package com.ibm.datatools.test.console.restapi.testcases.v1;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.test.console.restapi.testcases.TestCase;

public class load_server_param_has_header_row_spec {
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException, JSONException {		
		return FileTools.readJSONFile("/v1.0/load_server_param_has_header_row_spec.json");
	}	

	@Test(dataProvider = "testdata")
	public void test(Object key,Object inputPara) throws Exception {
		//new TestFactory().runSpec(false, "/v1.0/home_spec.json");
		new TestCase().testCall(true, (JSONObject)inputPara);
	}
}
