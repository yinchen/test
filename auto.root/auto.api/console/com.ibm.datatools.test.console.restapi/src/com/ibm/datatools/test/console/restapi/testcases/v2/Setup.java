package com.ibm.datatools.test.console.restapi.testcases.v2;

import org.testng.annotations.BeforeSuite;

import com.ibm.datatools.test.utils.RESTUtils;
import com.ibm.datatools.test.utils.Response;
import com.ibm.datatools.test.utils.Setting;



public class Setup {
	
	@BeforeSuite(alwaysRun=true,enabled=true)
	public static void runSetupDDL() throws Exception {
		
		System.out.println("The current tags value in conf file is:"+Setting.getSetting().getTags());
		runDDL("setup/setup.ddl");
		runDDL("setup/DB2_BLUE_CensusPopulation.sql");
		runDDL("setup/DB2_BLUE_GOSALES.sql");
	}
	
	private static void runDDL(String name) throws Exception{
		Response res = RESTUtils.runDDL(name);
		System.out.println("in Setup runSetupDDL() "+res.getResult());
//		assertNotNull(res.getJsonResult());
//		String id = res.getJsonResult().optString("id");
//		assertNotNull(id);
//		
//		String status = "";
//		HTTPClient client = new HTTPClient(true);
//		do {
//			Thread.sleep(2*1000);			
//			res = client.call(HTTPClient.METHOD_GET, Setting.getSetting().getURL() + "/sql_jobs/" + id);
//			System.out.println("in Setup runSetupDDL() "+res.getResult());
//			assertEquals(res.getStatus(), 200);
//			assertNotNull(res.getJsonResult());
//			status = res.getJsonResult().optString("status");
//			assertNotNull(status);
////			System.out.println("status "+status);
//		}
//		while (status.equals("running"));
		
		Thread.sleep(60*1000);
	}
	
	public static void main(String[] args) throws Exception {
		runSetupDDL();
	}
}
