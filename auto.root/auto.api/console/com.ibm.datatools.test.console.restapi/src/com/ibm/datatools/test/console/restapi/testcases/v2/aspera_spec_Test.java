package com.ibm.datatools.test.console.restapi.testcases.v2;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.test.utils.JsonUtil;
import com.ibm.datatools.test.console.restapi.testcases.TestCase;


public class aspera_spec_Test {
	
	private String token = null;
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException, JSONException {		
		return FileTools.readJSONFile("/v2.0/aspera_1.json");
	}	
	
	@Test(dataProvider = "testdata")
	public void test(Object key,Object inputPara) throws Exception {
		JSONObject result = new TestCase().testCall(false, (JSONObject)inputPara);
		token = result.get("aspera_token").toString();
	}
	
	@DataProvider(name = "testdata2")
	public Object[][] loadJSON2() throws FileNotFoundException, IOException, JSONException {		
		return FileTools.readJSONFile("/v2.0/aspera_2.json");
	}	
	
	@Test(dataProvider = "testdata2")
	public void test2(Object key,Object inputPara) throws Exception {
		String afterReplacement = JsonUtil.replaceVariable(inputPara.toString(), "aspera_token", token);
		new TestCase().testCall(false, JsonUtil.toJSONObject(afterReplacement));
		
	}

}
