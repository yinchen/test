package com.ibm.datatools.test.console.restapi.testcases.v2;

import java.util.Random;

import org.apache.commons.json.JSON;
import org.apache.commons.json.JSONArray;
import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


import com.ibm.datatools.test.console.restapi.testcases.TestCase;
import com.ibm.datatools.test.utils.JsonUtil;

public class auth_policies_spec_Test {
	private final static int casenum = 2;
	private final static int step = 2;
	private final static int sucnum = 1;
	private JSONArray source = JsonUtil.getJsonUnderDef("/v2.0/auth_policies.json");
	private String id = null;
	private final static Random rand = new Random();
	
	@Test(dataProvider="data")
	private void test1(Object casename,int i) throws Exception {
		int index = 0;
		if (i < sucnum)
			index = step*i;
		else
			index = step*sucnum + (i-sucnum);
		JSONObject payload = source.getJSONObject(index);
		String sp = JsonUtil.replaceVariable(payload.toString(), "policy_name", "Policy name - " + rand.nextInt(1000));
		JSONObject result = new TestCase().testCall(true, (JSONObject) JSON.parse(sp));
		if (i < sucnum) {
			JSONArray resoures = result.optJSONArray("resources");
			String id = ((JSONObject) resoures.get(0)).optString("id");
			this.id = id;
		}
	}
	
	/**
	 * get load job by id
	 * @param i
	 * @throws Exception
	 */
	@Test(dataProvider="data2")
	private void test1_2(Object casename,int i) throws Exception {
		JSONObject payload = source.getJSONObject(step*i+1);
		JsonUtil.replaceVariable(payload.toString(), "policy_id", id);
		Thread.sleep(5*1000);
	}
	
	@DataProvider(name="data")
	public Object[][] dataProvide() throws JSONException {
		Object[][] ret = new Object[0][0];
		if (source != null && source.size() > 0) {
			ret = new Object[casenum][2];
			int index = 0;
			for (int i=0; i<casenum; i++) {
				if (i < sucnum)
					index = step*i;
				else
					index = step*sucnum + (i-sucnum);
				ret[i][0] = source.getJSONObject(index).optString("description");
				ret[i][1] = new Integer(i);
			}
		}
		return ret;
	}
	
	@DataProvider(name="data2")
	public Object[][] dataProvide2() throws JSONException {
		Object[][] ret = new Object[0][0];
		if (source != null && source.size() > 0) {
			ret = new Object[sucnum][2];
			for (int i=0; i<sucnum; i++) {
				ret[i][0] = source.getJSONObject(step*i+1).optString("description");
				ret[i][1] = new Integer(i);
			}
		}
		return ret;
	}
}
