package com.ibm.datatools.test.console.restapi.testcases.v2;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.test.console.restapi.testcases.TestCase;


public class create_table_for_permissions_error_Test {
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException, JSONException {		
		return FileTools.readJSONFile("/v2.0/create_table_for_permissions_error.json");
	}	
	
	@Test(dataProvider = "testdata")
	public void test(Object key,Object inputPara) throws Exception {
		new TestCase().testCall(false, (JSONObject)inputPara);
	}

}
