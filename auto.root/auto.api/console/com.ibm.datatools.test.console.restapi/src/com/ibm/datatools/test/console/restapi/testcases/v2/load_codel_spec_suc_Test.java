package com.ibm.datatools.test.console.restapi.testcases.v2;

import org.apache.commons.json.JSON;
import org.apache.commons.json.JSONArray;
import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.test.utils.JsonUtil;
import com.ibm.datatools.test.console.restapi.testcases.TestCase;


public class load_codel_spec_suc_Test {
	
	private final static int casenum = 10;
	private final static int step = 3;
	private JSONArray source = JsonUtil.getJsonUnderDef("/v2.0/load_codel_spec_suc.json");
	private String[] ids = new String[casenum];
//	private String[] logs = new String[casenum];
	
	/**
	 * post load job
	 * @param i
	 * @throws Exception
	 */
	@Test(dataProvider="data")
	private void test1(Object casename,int i) throws Exception {
		JSONObject result = new TestCase().testCall(true, source.getJSONObject(step*i));
		String id = result.optString("id");
		ids[i] = id;
	}
	
	/**
	 * get load job by id
	 * @param i
	 * @throws Exception
	 */
	@Test(dataProvider="data2")
	private void test1_2(Object casename,int i) throws Exception {
		JSONObject payload = source.getJSONObject(step*i+1);
		String sp = JsonUtil.replaceVariable(payload.toString(), "id", ids[i]);
		Thread.sleep(60*1000);
		JSONObject result = new TestCase().testCall(true, (JSONObject) JSON.parse(sp));
//		logs[i] = result.optJSONObject("status").optString("log_file");
	}
	
	/**
	 * get load after test1_2 log
	 * @param i
	 * @throws Exception
	 */
	@Test(dataProvider="data3")
	private void test1_3(Object casename,int i) throws Exception {
		JSONObject payload = source.getJSONObject(step*i+2);
		String sp = JsonUtil.replaceVariable(payload.toString(), "id", ids[i]);
		new TestCase().testCall(true, (JSONObject) JSON.parse(sp));
	}
	
	@DataProvider(name="data")
	public Object[][] dataProvide() throws JSONException {
		Object[][] ret = new Object[casenum][2];
		for (int i=0; i<casenum; i++) {
			ret[i][0] = source.getJSONObject(step*i).optString("description");
			ret[i][1] = new Integer(i);
		}
		return ret;
	}
	
	@DataProvider(name="data2")
	public Object[][] dataProvide2() throws JSONException {
		Object[][] ret = new Object[casenum][2];
		for (int i=0; i<casenum; i++) {
			ret[i][0] = source.getJSONObject(step*i+1).optString("description");
			ret[i][1] = new Integer(i);
		}
		return ret;
	}
	
	@DataProvider(name="data3")
	public Object[][] dataProvide3() throws JSONException {
		Object[][] ret = new Object[casenum][2];
		for (int i=0; i<casenum; i++) {
			ret[i][0] = source.getJSONObject(step*i+2).optString("description");
			ret[i][1] = new Integer(i);
		}
		return ret;
	}
}
