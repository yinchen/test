package com.ibm.datatools.test.console.restapi.testcases.v2;

import org.apache.commons.json.JSONArray;
import org.apache.commons.json.JSONException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.test.utils.JsonUtil;
import com.ibm.datatools.test.console.restapi.testcases.TestCase;


public class load_codepage_spec_fail_Test {
	
	private final static int casenum = 5;
	private JSONArray source = JsonUtil.getJsonUnderDef("/v2.0/load_codepage_spec_fail.json");
	
	/**
	 * post load job
	 * @param i
	 * @throws Exception
	 */
	private void test1(int i) throws Exception {
		new TestCase().testCall(true, source.getJSONObject(i));
	}
	
	@DataProvider(name="data")
	public Object[][] dataProvide() throws JSONException {
		Object[][] ret = new Object[casenum][2];
		for (int i=0; i<casenum; i++) {
			ret[i][0] = source.getJSONObject(i).optString("description");
			ret[i][1] = new Integer(i);
		}
		return ret;
	}
	
	@Test(dataProvider="data")
	public void test(Object casename,Integer i) throws Exception {
		test1(i);
	}
}
