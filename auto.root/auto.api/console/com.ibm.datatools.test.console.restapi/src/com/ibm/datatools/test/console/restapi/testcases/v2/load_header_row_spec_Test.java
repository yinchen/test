package com.ibm.datatools.test.console.restapi.testcases.v2;

import org.apache.commons.json.JSON;
import org.apache.commons.json.JSONArray;
import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.test.utils.JsonUtil;
import com.ibm.datatools.test.console.restapi.testcases.TestCase;


public class load_header_row_spec_Test {
	
	private final static int casenum = 8;
	private final static int step = 3;
	private final static int sucnum = 4;
	private JSONArray source = JsonUtil.getJsonUnderDef("/v2.0/load_header_row_spec.json");
	private String[] ids = new String[20];
	
	/**
	 * post load job
	 * @param i
	 * @throws Exception
	 */
	@Test(dataProvider="data")
	private void test1(Object casename,int i) throws Exception {
		int index = 0;
		if (i < sucnum)
			index = step*i;
		else
			index = step*sucnum + (i-sucnum);
		JSONObject result = new TestCase().testCall(true, source.getJSONObject(index));
		String id = result.optString("id");
		ids[i] = id;
	}
	
	/**
	 * get load job by id
	 * @param i
	 * @throws Exception
	 */
	@Test(dataProvider="data2")
	private void test1_2(Object casename,int i) throws Exception {
		JSONObject payload = source.getJSONObject(step*i+1);
		String sp = JsonUtil.replaceVariable(payload.toString(), "id", ids[i]);
		Thread.sleep(30*1000);
		new TestCase().testCall(true, (JSONObject) JSON.parse(sp));
	}
	
	/**
	 * get load after test1_2 log
	 * @param i
	 * @throws Exception
	 */
	@Test(dataProvider="data3")
	private void test1_3(Object casename,int i) throws Exception {
		JSONObject payload = source.getJSONObject(step*i+2);
		String sp = JsonUtil.replaceVariable(payload.toString(), "id", ids[i]);
		Thread.sleep(5*1000);
		new TestCase().testCall(true, (JSONObject) JSON.parse(sp));
	}
	
	@DataProvider(name="data")
	public Object[][] dataProvide() throws JSONException {
		Object[][] ret = new Object[casenum][2];
		int index = 0;
		for (int i=0; i<casenum; i++) {
			if (i < sucnum)
				index = step*i;
			else
				index = step*sucnum + (i-sucnum);
			ret[i][0] = source.getJSONObject(index).optString("description");
			ret[i][1] = new Integer(i);
		}
		return ret;
	}
	
	@DataProvider(name="data2")
	public Object[][] dataProvide2() throws JSONException {
		Object[][] ret = new Object[sucnum][2];
		for (int i=0; i<sucnum; i++) {
			ret[i][0] = source.getJSONObject(step*i+1).optString("description");
			ret[i][1] = new Integer(i);
		}
		return ret;
	}
	
	@DataProvider(name="data3")
	public Object[][] dataProvide3() throws JSONException {
		Object[][] ret = new Object[sucnum][2];
		for (int i=0; i<sucnum; i++) {
			ret[i][0] = source.getJSONObject(step*i+2).optString("description");
			ret[i][1] = new Integer(i);
		}
		return ret;
	}
}
