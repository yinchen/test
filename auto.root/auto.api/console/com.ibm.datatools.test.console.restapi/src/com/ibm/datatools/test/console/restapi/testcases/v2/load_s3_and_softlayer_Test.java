package com.ibm.datatools.test.console.restapi.testcases.v2;

import org.apache.commons.json.JSON;
import org.apache.commons.json.JSONArray;
import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.test.utils.JsonUtil;
import com.ibm.datatools.test.console.restapi.testcases.TestCase;


public class load_s3_and_softlayer_Test {
	
	private final static int casenum = 5;
	private final static int step = 2;
	private final static int sucnum = 2;
	private JSONArray source = JsonUtil.getJsonUnderDef("/v2.0/load_s3_and_softlayer.json");
	private String[] ids = new String[sucnum];
	
	/**
	 * post load job
	 * @param i
	 * @throws Exception
	 */
	private void test1(int i) throws Exception {
		int index = 0;
		if (i < sucnum)
			index = step*i;
		else
			index = step*sucnum + (i-sucnum);
		JSONObject result = new TestCase().testCall(true, source.getJSONObject(index));
		String id = result.optString("id");
		if (i < sucnum)
			ids[i] = id;
	}
	
	/**
	 * get load job by id
	 * @param i
	 * @throws Exception
	 */
	private void test1_2(int i) throws Exception {
		JSONObject payload = source.getJSONObject(step*i+1);
		String sp = JsonUtil.replaceVariable(payload.toString(), "id", ids[i]);
		Thread.sleep(10*1000);
		new TestCase().testCall(true, (JSONObject) JSON.parse(sp));		
	}
	
	@DataProvider(name="data")
	public Object[][] dataProvide() throws JSONException {
		Object[][] ret = new Object[0][0];
		int index = 0;
		if (source != null && source.size() > 0) {
			ret = new Object[casenum][2];
			for (int i = 0; i < casenum; i++) {
				if (i < sucnum)
					index = step * i;
				else
					index = step * sucnum + (i - sucnum);

				ret[i][0] = source.getJSONObject(index).optString("description");
				ret[i][1] = new Integer(i);
			}
		}
		return ret;
	}
	
	@Test(dataProvider="data")
	public void test(Object casename,Integer i) throws Exception {
		test1(i);
		if (i < sucnum) {
			test1_2(i);
		}
	}
}
