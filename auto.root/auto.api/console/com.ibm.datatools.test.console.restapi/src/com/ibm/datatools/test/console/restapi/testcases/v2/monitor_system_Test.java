package com.ibm.datatools.test.console.restapi.testcases.v2;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.test.console.restapi.testcases.TestCase;


public class monitor_system_Test {
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException, JSONException {		
		return FileTools.readJSONFile("/v2.0/monitor_system.json");
	}	
	
	@Test(dataProvider = "testdata")
	public void test(Object key,Object inputPara) throws Exception {
		for (int i=0; i<100; i++) {
			new Thread(){
				public void run() {
				try {
					new TestCase().testCall(true, (JSONObject)inputPara);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				};
			}.start();
		}
		Thread.sleep(10*60*1000);
	}	

}
