package com.ibm.datatools.test.console.restapi.testcases.v2;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.test.console.restapi.testcases.TestCase;

public class ssl_enforce_spec {
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException, JSONException {		
		return FileTools.readJSONFile("/v2.0/ssl_enforce_spec.json");
	}	
	
	@Test(dataProvider = "testdata")
	public void test(Object key,Object inputPara) throws Exception {
		new TestCase().testCall(true, (JSONObject)inputPara);
	}
	
	
	@DataProvider(name = "testdata2")
	public Object[][] loadJSON2() throws FileNotFoundException, IOException, JSONException {		
		return FileTools.readJSONFile("/v2.0/ssl_enforce.json");
	}	
	
	@Test(dataProvider = "testdata2")
	public void test2(Object key,Object inputPara) throws Exception {
		new TestCase().testCall(true, (JSONObject)inputPara);
	}
}
