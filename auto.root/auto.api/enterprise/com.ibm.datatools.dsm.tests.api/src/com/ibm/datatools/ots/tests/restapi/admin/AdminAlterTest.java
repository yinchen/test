package com.ibm.datatools.ots.tests.restapi.admin;

import java.util.LinkedList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.testng.Assert;

import com.ibm.datatools.ots.tests.restapi.common.AdminAlterObjectService;
import com.ibm.datatools.ots.tests.restapi.common.AdminSQLEditorService;
import com.jayway.jsonpath.JsonPath;

public class AdminAlterTest extends AdminTestBase
{
	protected AdminAlterObjectService alterService = new AdminAlterObjectService();
	protected AdminSQLEditorService sqlEditorService = new AdminSQLEditorService();

	/**
	 * the common function for the alter test
	 */
	protected void assertEquals( String key, String[] expected ) throws InterruptedException
	{

		// The key defined in the properties
		String query = alterService.getJSONData( key );
		
		List<String> commands = new LinkedList<String>();

		JSONObject resObj =
				alterService.callAlterObjectService( AdminAlterObjectService.ALTER_OBJECT, query, dbProfile );

		// Get the response Data
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "items" ) );
		JSONArray ja = responseData.getJSONArray( "items" );

		int size = expected.length;
		Assert.assertNotNull( ja );
		
		if(expected[0].contains("ALTER TABLE") && expected[0].contains("ADD COLUMN") && expected[0].contains("GENERATED"))			
		{
			int generatedPos = expected[0].indexOf("GENERATED");
			String firstStr = expected[0].substring(0,generatedPos - 1);
			int integerPos = 0;
			if(expected[0].contains("INTEGER")){
				integerPos = expected[0].indexOf("INTEGER");
			}
			String commonStr = expected[0].substring(0,integerPos).replaceAll("ADD COLUMN", "ALTER COLUMN");
			String middleStr =  commonStr + " DROP DEFAULT";
			String lastStr = commonStr + " SET " + expected[0].substring(generatedPos,expected[0].length());
			if(!(firstStr.indexOf("DEFAULT") >= 0)){
				firstStr += " DEFAULT 0";
			}
			commands.add( firstStr);
			commands.add( middleStr );
			commands.add( lastStr );
			size = commands.size();
		}
		
		Assert.assertEquals( size, ja.size() );

		String ddl = "";
		if(!(expected[0].contains("ALTER TABLE") && expected[0].contains("ADD COLUMN") && expected[0].contains("GENERATED")))
		for ( int i = 0; i < ja.size(); i++ )
		{
			ddl = JsonPath.read( responseData, "$.items[" + i + "].statement" );
			// Assert.assertTrue(Pattern.matches(expected[i],ddl)); //just for
			// recording
			Assert.assertEquals( ddl, expected[ i ] );
		}
	}

	/**
	 * the common function for the alter test
	 */
	protected void assertEquals( String key, List<String> expected ) throws InterruptedException
	{

		// The key defined in the properties
		String query = alterService.getJSONData( key );

		JSONObject resObj =
				alterService.callAlterObjectService( AdminAlterObjectService.ALTER_OBJECT, query, dbProfile );

		// Get the response Data
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "items" ) );
		JSONArray ja = responseData.getJSONArray( "items" );

		int size = expected.size();
		Assert.assertNotNull( ja );
		Assert.assertEquals( size, ja.size() );

		String ddl = "";
		for ( int i = 0; i < ja.size(); i++ )
		{
			ddl = JsonPath.read( responseData, "$.items[" + i + "].statement" );
			// Assert.assertTrue(Pattern.matches(expected[i],ddl)); //just for
			// recording
			Assert.assertEquals( ddl, expected.get( i ) );
		}
	}

}
