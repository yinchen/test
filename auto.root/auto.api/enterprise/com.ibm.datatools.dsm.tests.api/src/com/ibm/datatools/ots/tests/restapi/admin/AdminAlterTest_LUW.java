package com.ibm.datatools.ots.tests.restapi.admin;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.AdminAlterObjectService;
import com.ibm.datatools.test.utils.AdminConst;
import com.jayway.jsonpath.JsonPath;

public class AdminAlterTest_LUW extends AdminAlterTest
{
	private static final Logger logger = LogManager
			.getLogger(AdminAlterTest_LUW.class);
	/**
	 * @author ybjwang@cn.ibm.com
	 * some data prepare for Index,MQT,Alias,Schema testing
	 * 
	 */
	
	@BeforeClass
	public void beforeTest() {
		//only testAlterTableProperties and testIndexBatchPrivilegeDefinition in BVT suite
		//so skip the unused pre-condition
		if(!this.isBVT){
			testAlterIndex_precondition();	
			testAlterMQT_precondition();
			testAlterSchema_precondition();
			testAlterAlias_precondition();
			testAlterStorageGroup_precondition();
			testCreateTableColumnOrganized_precondition();
			testCreateTableSYSTEMP_precondition();
			testCreateTableAPPTEMP_precondition();
			testCreateTableBITEMP_precondition();
			testCreateTableAddDistributionKey_precondition();
			testAlterTableAddFGAC_precondition();
			testViewTableData_precondition();
			testTableGenerateDDL_precondition();
			testCreateTableSpace_precondition();
			testCreateSequence_precondition();
			testCreateTrigger_precondition();
			testCreateView_precondition();
			testCreateClusteredIndex_precondition();
		}
	}
	
	
	/**
	 * @author ybjwang@cn.ibm.com
	 * clean up the data prepared at before test
	 */
	
	@AfterClass
	public void afterTest() {
		if(!this.isBVT){
			testAlterIndex_clean();
			testAlterSchema_clean();
			testAlterMQTs_clean();
			testAlterAlias_clean();
			testAlterStorageGroup_clean();
			testCreateTableColumnOrganized_clean();
			testCreateTableSYSTEMP_clean();
			testCreateTableAPPTEMP_clean();
			testCreateTableBITEMP_clean();
			testCreateTableAddDistributionKey_clean();
			testAlterTableAddFGAC_clean();
			testViewTableData_clean();
			testTableGenerateDDL_clean();
			testSequence_clean();
			testTrigger_clean();
			testView_clean();
			testCreateClusteredIndex_clean();
		}
	}
	
	private void testCreateTableSpace_precondition(){
		//Drop the tablespace created from testCreateTableSpaceExeDDL
		List<String> ddlList = new ArrayList<String>();

		ddlList.add("DROP TABLESPACE NEW_TABLESPACE");
		ddlList.add("DROP TABLESPACE NEW_TS_USERTEMP");
		ddlList.add("DROP TABLESPACE NEW_TS_SYSTEMP");
		ddlList.add("DROP TABLESPACE NEW_TS_REGULAR");
					
		sqlEditorService.runSQLinJDBC(ddlList, ";", this.dbProfile, false);
	}
	private void testAlterStorageGroup_precondition(){
		
		dropStorageGroup(false);
		createStoragGroup();
		
	}
	private void createStoragGroup(){
		//Storage Group is not shown on UI part for DB2V9.7
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97)){
									return;
			}
		String sqlStatement = "CREATE STOGROUP \"NEWSTGRP4STG1\" ON \'/tmp\'";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile, true);
		
	}
	private void testAlterStorageGroup_clean(){
		dropStorageGroup(true);
	}
	private void dropStorageGroup(boolean real){
		//Storage Group is not shown on UI part for DB2V9.7
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97)){
											return;
			}
		String sqlStatement = "DROP STOGROUP NEWSTGRP4STG1";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile, real);
		//Drop the StorageGroup created from testCreateStorageGroupExecuteDDL
		sqlStatement = "DROP STOGROUP NEWSTGRP4STG";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile, false);
		
	}
	private void testAlterAlias_precondition(){
		dropAlias_Alias(false);
		dropTable_Alias(false);
		dropSchema_Alias(false);
		createSchema_Alias();
		createTable_Alias();
		addDataToTable_Alias();
		createAlias_Alias();
		
	}
	private void testAlterMQT_precondition() {
		dropMQT(false);
		dropTable_MQT(false);
		dropTableSpace_MQTs(false);
		dropSchema_MQT(false);	
		createSchema_MQT();
		createTable_MQT();
		insertData4Table_MQT();
		createTableSpace_MQTs();
		createMQT();
	}
	private void testAlterSchema_precondition(){
		
		dropSchema_Schema(false);
		createSchema_Schema();
		
	}
	
	private void testAlterAlias_clean(){
		dropAlias_Alias(true);
		dropTable_Alias(true);
		dropSchema_Alias(true);	
	}
	
	/**
	 * @author ybjwang@cn.ibm.com
	 * Clearn env for schema test suit
	 * 	 */
	private void testAlterSchema_clean(){
		
		dropSchema_Schema(true);
	}
	private void testAlterMQTs_clean(){
		
		dropMQT(true);
		dropTableSpace_MQTs(true);
		dropTable_MQT(true);
		dropSchema_MQT(true);
		
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Create schema for Alias testing suits
	 */
	private void createSchema_Alias()
	{
		String sqlStatement = "CREATE SCHEMA SCHEMA4ALIAS";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile, true);
		
	}
	private void dropSchema_Alias(boolean real){
		String sqlStatement = "DROP SCHEMA SCHEMA4ALIAS RESTRICT";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile, real);
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Create  tables for Alias testing and insert one column data
	 */
	private void createTable_Alias() {
			String sqlStatement = null;
			sqlStatement = "CREATE TABLE SCHEMA4ALIAS.TABLE4ALIAS (";
			String fieldsSql = null;
			for (int i = 1; i <= 3; i++) {
				if (i == 1) {
					fieldsSql = "field1 int";
				} else {
					fieldsSql = fieldsSql + ",field" + i + " int";
				}
			}
			sqlStatement = sqlStatement + fieldsSql + ");";
			//The first table for create Alias
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
			//The second table for Alter Alias
			sqlStatement = "CREATE  TABLE SCHEMA4ALIAS.TABLE4ALIAS1 (FIELD1 INT,FIELD2 INT)";
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Create Alias for View Alias data testing
	 */
	private void createAlias_Alias(){
		
		String sqlStatement = "CREATE ALIAS SCHEMA4ALIAS.ALIASTEST1 FOR SCHEMA4ALIAS.TABLE4ALIAS1";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Drop Alias
	 * @param real
	 */
	private void dropAlias_Alias(boolean real){
		
		String sqlStatement = "DROP ALIAS SCHEMA4ALIAS.ALIASTEST1";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,real);
		//Drop the Alias created from testCreateAlias
		sqlStatement = "DROP ALIAS SCHEMA4ALIAS.ALIASTEST";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,false);
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Add some data for Table
	 */
	private void addDataToTable_Alias(){
		
		String sqlStatement = "INSERT INTO \"SCHEMA4ALIAS\".\"TABLE4ALIAS1\" (\"FIELD1\", \"FIELD2\") VALUES (12, 15)";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
	}
	
	/**
		 * Drop Table created for Alias
		 * @param real
		 */
	private void dropTable_Alias(boolean real){
		
		List<String> dropList = new ArrayList<String>();
		dropList.add("DROP TABLE SCHEMA4ALIAS.TABLE4ALIAS");
		dropList.add("DROP TABLE SCHEMA4ALIAS.TABLE4ALIAS1");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile,real);
			
		}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Create schema for schema testing suits
	 */
	private void createSchema_Schema()
	{
		String sqlStatement = "CREATE SCHEMA SCHEMA4SCHEMA1";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile, true);
		
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Drop schema for schema testing suits
	 * @param real
	 */
	private void dropSchema_Schema(boolean real){
		
		String sqlStatement = "DROP SCHEMA SCHEMA4SCHEMA1 RESTRICT";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile, real);
		//Drop Schema created from testCreateSchemaExecuteDDL
		sqlStatement = "DROP SCHEMA SCHEMA4SCHEMA RESTRICT";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile, false);
	}
	private void createSchema_MQT() {
		// TODO Auto-generated method stub
		String sqlStatement = "CREATE SCHEMA SCHEMA4MQT";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile, true);
	}
	private void createTable_MQT() {
		List<String> createList = new ArrayList<String>();

		String fieldsSql = null;
		for (int i = 1; i <= 3; i++) {
			if (i == 1) {
				fieldsSql = "field1 int not null";
			} else {
				fieldsSql = fieldsSql + ",field" + i + " int not null";
			}
		}

		String constraintsSql = null;
		for (int i = 2; i <= 3; i++) {
			if (i == 2) {
				constraintsSql = "CONSTRAINT UNIQUE_CONSTRAINT_TEST" + i + " UNIQUE (field" + i + ")";
			} else {
				constraintsSql = constraintsSql + ", CONSTRAINT UNIQUE_CONSTRAINT_TEST" + i + " UNIQUE (field" + i
						+ ") ";
			}
		}

		createList.add(
		"CREATE TABLE SCHEMA4MQT.TABLE4MQT (" + fieldsSql + ", CONSTRAINT PRIMARY_CONSTRAINT_TEST PRIMARY KEY (field1), "
				+ constraintsSql + "); ");

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}
	/**
	 * @author ybjwang@cn.ibm.com	
	 * Insert data for Table4MQT to veirfy the data 
	 */
	private void insertData4Table_MQT(){
		String sqlStatement = "INSERT INTO  SCHEMA4MQT.TABLE4MQT(\"FIELD1\",\"FIELD2\",\"FIELD3\") VALUES (\'16\',\'17\',\'18\')";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile, true);
	}
	private void createMQT() {
		// TODO Auto-generated method stub
		List<String> createList = new ArrayList<String>();

		for (int i = 1; i <= 3; i++) {
			createList.add("CREATE TABLE SCHEMA4MQT.MQT" + i + " as (select a.field" + i + " field" + i
					+ "MQT from SCHEMA4MQT.TABLE4MQT a) data initially deferred REFRESH DEFERRED MAINTAINED BY USER IN TABLESPACE4MQTS");
		}

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
		//Create anotherMQT for view data testing
		createList= new ArrayList<String>();
		createList.add("CREATE TABLE \"SCHEMA4MQT\".\"MQT4\" AS ( SELECT FIELD1, FIELD2 FROM SCHEMA4MQT.TABLE4MQT ) DATA INITIALLY DEFERRED REFRESH IMMEDIATE MAINTAINED BY SYSTEM");
		createList.add("SET INTEGRITY FOR \"SCHEMA4MQT\".\"MQT4\" IMMEDIATE CHECKED FULL ACCESS;");
		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}
	private void dropSchema_MQT(boolean real) {
		// TODO Auto-generated method stub
		String sqlStatement = "DROP SCHEMA SCHEMA4MQT RESTRICT";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile, real);
	}
	private void dropTable_MQT(boolean real) {
		// TODO Auto-generated method stub
		List<String> dropList = new ArrayList<String>();
		dropList.add("Drop table SCHEMA4MQT.TABLE4MQT");
		//dropList.add("Drop table SCHEMA4MQT.TABLE4MQT2");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile,real);
	}
	
	private void dropMQT(boolean real) {
		// TODO Auto-generated method stub
		List<String> dropList = new ArrayList<String>();
		dropList.add("Drop table SCHEMA4MQT.MQT1");
		dropList.add("Drop table SCHEMA4MQT.MQT2");
		dropList.add("Drop table SCHEMA4MQT.MQT3");
		dropList.add("Drop table SCHEMA4MQT.MQT4");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile,real);
		//Drop MQTs created by testCreateSysMQTsExcuteDDL and testCreateUserMQTsExcuteDDL
		List<String> dropMQTListCreated = new ArrayList<String>();
		dropMQTListCreated.add("Drop table SCHEMA4MQT.MQTTest4System");
		dropMQTListCreated.add("Drop table SCHEMA4MQT.MQTTest4User");
		sqlEditorService.runSQLinJDBC(dropMQTListCreated, ";", this.dbProfile,false);
	}
	
	/**
	 * @author ybjwang@cn.ibm.com
	 */
	private void testAlterIndex_clean() {
		// TODO Auto-generated method stub
		dropIndex(true);
		dropTable_Index(true);
		dropSchema_Index(true);
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * This method is used to drop the created Index during before class.
	 */
	private void dropIndex(boolean real) {
		// TODO Auto-generated method stub
		List<String> dropList = new ArrayList<String>();
		
		for (int i = 0; i <= 3; i++) {
			dropList.add("DROP INDEX SCHEMA4INDEX.INDTEST" + i );
		}
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, real);
		//Drop Index created from testDropIndexAndExeDDL
		String dropIndexCreated = "DROP INDEX SCHEMA4INDEX.NEW_INDEX1";
		sqlEditorService.runSQLinJDBC(dropIndexCreated, ";", this.dbProfile, false);
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 */
	private void dropTable_Index(boolean real) {
		// TODO Auto-generated method stub
		List<String> dropList = new ArrayList<String>();
		dropList.add("Drop table SCHEMA4INDEX.TABLE4INDEX");
		dropList.add("Drop table SCHEMA4INDEX.TABLE4INDEX2");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile,real);
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 */
	private void testAlterIndex_precondition() {
		// TODO Auto-generated method stub
		dropIndex(false);
		dropTable_Index(false);
		dropSchema_Index(false);
		createSchema_Index();
		createTable_Index();
		createIndex();
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 */
	private void createSchema_Index() {
		// TODO Auto-generated method stub
		String sqlStatement = "CREATE SCHEMA SCHEMA4INDEX";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile, true);
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 */
	private void dropSchema_Index(boolean real){
		String sqlStatement = "DROP SCHEMA SCHEMA4INDEX RESTRICT";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile, real);
	}

/**
 * @author ybjwang@cn.ibm.com
 * This method is to create some Index to use for AlterIndex testing.
 */
	private void createIndex() {
		// TODO Auto-generated method stub
		List<String> createList = new ArrayList<String>();
		createList.add("CREATE INDEX SCHEMA4INDEX.INDTEST0 ON SCHEMA4INDEX.TABLE4INDEX (FIELD1 ASC,FIELD2 ASC);");

		for (int i = 1; i <= 3; i++) {
			createList.add("CREATE INDEX SCHEMA4INDEX.INDTEST" + i + " ON SCHEMA4INDEX.TABLE4INDEX (FIELD" + i + " DESC);");
		}

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}
	private void createTableSpace_MQTs(){
		
		String sqlStatement = "CREATE TABLESPACE TABLESPACE4MQTS";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
	}
	private void dropTableSpace_MQTs(boolean real){
		
		String sqlStatement = "DROP TABLESPACE TABLESPACE4MQTS";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,real);
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Create two tables for index testing
	 */
		private void createTable_Index() {
			String sqlStatement = null;
			sqlStatement = "create table SCHEMA4INDEX.TABLE4INDEX (";
			String fieldsSql = null;
			for (int i = 1; i <= 3; i++) {
				if (i == 1) {
					fieldsSql = "field1 int";
				} else {
					fieldsSql = fieldsSql + ",field" + i + " int";
				}
			}
			sqlStatement = sqlStatement + fieldsSql + ");";

			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
			
			sqlStatement = "create table SCHEMA4INDEX.TABLE4INDEX2 (field1 int)";
			
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
	}

		
		public void testCreateTableColumnOrganized_precondition() {
				
			sqlEditorService.runSQLinJDBC( "DROP SCHEMA SCH_COLORG RESTRICT;", ";", this.dbProfile, false );
				
			sqlEditorService.runSQLinJDBC( "CREATE SCHEMA SCH_COLORG;", ";", this.dbProfile, true );
				
	    }
			
		public void testCreateTableColumnOrganized_clean() {
			sqlEditorService.runSQLinJDBC( "DROP SCHEMA SCH_COLORG RESTRICT;", ";", this.dbProfile, true );		
		}
			
	    public void testCreateTableSYSTEMP_precondition() {
				
			sqlEditorService.runSQLinJDBC( "DROP SCHEMA SCH_SYSTEMP RESTRICT;", ";", this.dbProfile, false );
				
			sqlEditorService.runSQLinJDBC( "CREATE SCHEMA SCH_SYSTEMP;", ";", this.dbProfile, true );
				
		}
			
		public void testCreateTableSYSTEMP_clean() {
			sqlEditorService.runSQLinJDBC( "DROP SCHEMA SCH_SYSTEMP RESTRICT;", ";", this.dbProfile, true );		
		}
			
		public void testCreateTableAPPTEMP_precondition() {
				
			sqlEditorService.runSQLinJDBC( "DROP SCHEMA SCH_APPTEMP RESTRICT;", ";", this.dbProfile, false );
				
			sqlEditorService.runSQLinJDBC( "CREATE SCHEMA SCH_APPTEMP;", ";", this.dbProfile, true );		
	    }
			
		public void testCreateTableAPPTEMP_clean() {
				sqlEditorService.runSQLinJDBC( "DROP SCHEMA SCH_APPTEMP RESTRICT;", ";", this.dbProfile, true );		
		}
			
	    public void testCreateTableBITEMP_precondition() {
				
				sqlEditorService.runSQLinJDBC( "DROP SCHEMA SCH_BITEMP RESTRICT;", ";", this.dbProfile, false );
				
				sqlEditorService.runSQLinJDBC( "CREATE SCHEMA SCH_BITEMP;", ";", this.dbProfile, true );		
	    }
			
	    public void testCreateTableBITEMP_clean() {
				sqlEditorService.runSQLinJDBC( "DROP SCHEMA SCH_BITEMP RESTRICT;", ";", this.dbProfile, true );		
		}
			
		public void testCreateTableAddDistributionKey_precondition() {
					
					sqlEditorService.runSQLinJDBC( "DROP SCHEMA SCH_DISKEY RESTRICT;", ";", this.dbProfile, false );
					
					sqlEditorService.runSQLinJDBC( "CREATE SCHEMA SCH_DISKEY;", ";", this.dbProfile, true );		
		}
				
		public void testCreateTableAddDistributionKey_clean() {
					sqlEditorService.runSQLinJDBC( "DROP SCHEMA SCH_DISKEY RESTRICT;", ";", this.dbProfile, true );		
		}
				
		public void testAlterTableAddFGAC_precondition() {
					
					List<String> dropList = new ArrayList<String>();

					dropList.add("DROP TABLE SCH_FGAC.TAB_FGAC;");
					dropList.add("DROP SCHEMA SCH_FGAC RESTRICT;");
					
					sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);
					
					List<String> createList = new ArrayList<String>();

					createList.add("CREATE SCHEMA SCH_FGAC;");
					createList.add("CREATE TABLE SCH_FGAC.TAB_FGAC(NEW_COLUMN1 CHAR(5), NEW_COLUMN2 CHAR(5));");	
					
					sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
		}
				
		public void testAlterTableAddFGAC_clean() {
					List<String> dropList = new ArrayList<String>();

					dropList.add("DROP TABLE SCH_FGAC.TAB_FGAC;");
					dropList.add("DROP SCHEMA SCH_FGAC RESTRICT;");
					
					sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, true);
					
		}
				
	   public void testViewTableData_precondition() {				
					 List<String> dropList = new ArrayList<String>();
					 dropList.add("DROP TABLE SCH_VIEWDATA.TAB_VIEWDATA;");
					 dropList.add("DROP SCHEMA SCH_VIEWDATA RESTRICT;");
					 sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);
						
					 List<String> createList = new ArrayList<String>();
					 createList.add("CREATE SCHEMA SCH_VIEWDATA;");
					 createList.add("CREATE TABLE SCH_VIEWDATA.TAB_VIEWDATA(COL1 INT);");
					 createList.add("INSERT INTO SCH_VIEWDATA.TAB_VIEWDATA VALUES(1),(2),(3);");
					 sqlEditorService.runSQLinJDBC(createList, ";",this.dbProfile, true);
						
		}
					
		public void testViewTableData_clean() {
						List<String> dropList = new ArrayList<String>();
						dropList.add("DROP TABLE SCH_VIEWDATA.TAB_VIEWDATA;");
						dropList.add("DROP SCHEMA SCH_VIEWDATA RESTRICT;");
						sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, true);		
		}
		
		 public void testTableGenerateDDL_precondition() {				
			 List<String> dropList = new ArrayList<String>();
			 dropList.add("DROP TABLE SCH_TABGENDDL.TAB_GENDDL;");
			 dropList.add("DROP SCHEMA SCH_TABGENDDL RESTRICT;");
			 dropList.add("DROP TABLESPACE TS_TABGENDDL;");
			 sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);
				
			 List<String> createList = new ArrayList<String>();
			 createList.add("CREATE TABLESPACE TS_TABGENDDL;");
			 createList.add("CREATE SCHEMA SCH_TABGENDDL;");
			 createList.add("CREATE TABLE SCH_TABGENDDL.TAB_GENDDL(NEW_COLUMN1 CHAR(5)) IN TS_TABGENDDL;");
			 sqlEditorService.runSQLinJDBC(createList, ";",this.dbProfile, true);
				
        }
			
       public void testTableGenerateDDL_clean() {
				List<String> dropList = new ArrayList<String>();
				 dropList.add("DROP TABLE SCH_TABGENDDL.TAB_GENDDL;");
				 dropList.add("DROP SCHEMA SCH_TABGENDDL RESTRICT;");
				 dropList.add("DROP TABLESPACE TS_TABGENDDL;");
				sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, true);		
        }


	/**
	 * Test the alter table properties function in admin part.
	 * include: RENAME TABLE,DROP RESTRICT, ALTER TABLESPACE, LOCKSIZE ROW.
	 * @throws InterruptedException 
	 * 
	 * */
	@Test(description = "Test the alter table properties action in admin part")
	public void testAlterTableProperties() throws InterruptedException{

		logger.info("Running Alter Table properties test...");
		
		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"items":[{"statement":"RENAME TABLE \"DB2ADMIN\".\"T1\" TO \"NEW_TABLE\""},
		 * {"statement":"ALTER TABLE \"DB2ADMIN\".\"NEW_TABLE\" NOT VOLATILE CARDINALITY DROP RESTRICT ON DROP DATA CAPTURE CHANGES PCTFREE 6 ACTIVATE NOT LOGGED INITIALLY WITH EMPTY TABLE LOCKSIZE TABLE APPEND ON COMPRESS YES STATIC DEACTIVATE VALUE COMPRESSION LOG INDEX BUILD OFF"},
		 * {"statement":"CALL SYSPROC.ADMIN_MOVE_TABLE('DB2ADMIN','TABLE1','USERSPACE2','USERSPACE2','USERSPACE2', NULL, NULL, NULL, NULL, NULL, 'MOVE')"},
		 * {"statement":"ALTER TABLE \"DB2ADMIN\".\"TABLE2\" LOCKSIZE ROW"},
		 * {"statement":"CALL SYSPROC.ADMIN_MOVE_TABLE('DB2ADMIN','TABLE2','USERSPACE1','USERSPACE2','USERSPACE3', NULL, NULL, NULL, NULL, NULL, 'MOVE')"}]}
		 * 
		 * To retrieve the statement, at first we look over if the length of items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of key "statement", that is $.items[0].statement
		 * */
		String expected[] = {
				"RENAME TABLE \"DB2ADMIN\".\"T1\" TO \"NEW_TABLE\"",
				"ALTER TABLE \"DB2ADMIN\".\"NEW_TABLE\" DROP RESTRICT ON DROP DATA CAPTURE CHANGES PCTFREE 6 ACTIVATE NOT LOGGED INITIALLY WITH EMPTY TABLE LOCKSIZE TABLE APPEND ON COMPRESS YES STATIC DEACTIVATE VALUE COMPRESSION LOG INDEX BUILD OFF",
				"CALL SYSPROC.ADMIN_MOVE_TABLE('DB2ADMIN','TABLE1','USERSPACE2','USERSPACE2','USERSPACE2', NULL, NULL, NULL, NULL, NULL, 'MOVE')",
				"ALTER TABLE \"DB2ADMIN\".\"TABLE2\" LOCKSIZE ROW",
				"CALL SYSPROC.ADMIN_MOVE_TABLE('DB2ADMIN','TABLE2','USERSPACE1','USERSPACE2','USERSPACE3', NULL, NULL, NULL, NULL, NULL, 'MOVE')" };
		assertEquals("getAlterTableProperties_LUW", expected);
		logger.info("PASSED: Alter Table  properties executed successfully");
	}

	/**
	 * Test the alter table add columns function in admin part.
	 * include: ADD COLUMNS
	 * */
	@Test(description = "Test the alter table add columns action in admin part")
	public void testAlterTableAddColumns() throws InterruptedException {

		logger.info("Running Alter Table add columns test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"ALTER TABLE \"DB2ADMIN\".\"T1\" ADD COLUMN \"COL1\" CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa' ADD COLUMN \"COL2\" INTEGER WITH DEFAULT 32 ADD COLUMN \"COL3\" CLOB(1024) INLINE LENGTH 68 ADD COLUMN \"COL4\" DECIMAL(6,3) NOT NULL WITH DEFAULT 12"}]}
		 * 
		 * To retrieve the statement, at first we look over if the length of items
		 * array is correct, then we are looking for the first index of the
		 * items array -> the return is a object, we look for the value of key
		 * "statement", that is $.items[0].statement
		 * */

		String expected[] = {
				"ALTER TABLE \"DB2ADMIN\".\"T1\" ADD COLUMN \"COL1\" CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa' ADD COLUMN \"COL2\" INTEGER WITH DEFAULT 32 ADD COLUMN \"COL3\" CLOB(1024) INLINE LENGTH 68 ADD COLUMN \"COL4\" DECIMAL(6,3) NOT NULL WITH DEFAULT 12"};
		assertEquals("getAlterTableAddColumns_LUW", expected);

		logger.info("PASSED: Alter Table add column executed successfully");
	}

	
	/**
	 * Test the alter table alter columns function in admin part.
	 * include: ALTER COLUMNS, DROP EXPRESSION
	 * */
	@Test(description = "Test the alter table alter columns action in admin part")
	public void testAlterTableAlterColumn() throws InterruptedException {

		logger.info("Running Alter Table alter columns test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"ALTER TABLE \"DB2ADMIN\".\"T1\" RENAME COLUMN \"COL1\" TO \"NEW_COL1\""},
		 * {"statement":"ALTER TABLE \"DB2ADMIN\".\"T1\" ALTER COLUMN \"NEW_COL1\" SET DATA TYPE CHAR(25)"},
		 * {"statement":"ALTER TABLE \"DB2ADMIN\".\"T1\" ALTER COLUMN \"NEW_COL1\" SET NOT HIDDEN ALTER COLUMN \"COL2\" SET IMPLICITLY HIDDEN"},
		 * {"statement":"ALTER TABLE \"DB2ADMIN\".\"T1\" ALTER COLUMN \"NEW_COL1\" SET NOT NULL ALTER COLUMN \"COL2\" DROP NOT NULL"},
		 * {"statement":"ALTER TABLE \"DB2ADMIN\".\"T1\" ALTER COLUMN \"COL2\" SET INLINE LENGTH 2000"},
		 * {"statement":"ALTER TABLE \"DB2ADMIN\".\"T1\" ALTER COLUMN \"NEW_COL1\" SET DEFAULT 'WA' ALTER COLUMN \"COL2\" DROP DEFAULT"},
		 * {"statement":"ALTER TABLE \"DB2ADMIN\".\"T1\" ALTER COLUMN \"COL2\" COMPRESS SYSTEM DEFAULT"},
		 * {"statement":"SET INTEGRITY FOR \"DB2ADMIN\".\"T1\" OFF CASCADE DEFERRED"},
		 * {"statement":"ALTER TABLE \"DB2ADMIN\".\"T1\" ALTER COLUMN \"COL4\" DROP EXPRESSION ALTER COLUMN \"COL5\" SET GENERATED ALWAYS AS ( 'bbbb' )"},
		 * {"statement":"SET INTEGRITY FOR \"DB2ADMIN\".\"T1\" IMMEDIATE CHECKED FULL ACCESS FORCE GENERATED"},
		 * {"statement":"ALTER TABLE \"DB2ADMIN\".\"T1\" DROP COLUMN \"COL3\""}]} 
		 * To retrieve the statement, at first we look over if the length of items
		 * array is correct, then we are looking for the first index of the
		 * items array -> the return is a object, we look for the value of key
		 * "statement", that is $.items[0].statement
		 * */

		String expected[] = {
				"ALTER TABLE \"DB2ADMIN\".\"T1\" RENAME COLUMN \"COL1\" TO \"NEW_COL1\"",
				"ALTER TABLE \"DB2ADMIN\".\"T1\" ALTER COLUMN \"NEW_COL1\" SET DATA TYPE CHAR(25)",
				"ALTER TABLE \"DB2ADMIN\".\"T1\" ALTER COLUMN \"NEW_COL1\" SET NOT HIDDEN ALTER COLUMN \"COL2\" SET IMPLICITLY HIDDEN",
				"ALTER TABLE \"DB2ADMIN\".\"T1\" ALTER COLUMN \"NEW_COL1\" SET NOT NULL ALTER COLUMN \"COL2\" DROP NOT NULL",
				"ALTER TABLE \"DB2ADMIN\".\"T1\" ALTER COLUMN \"COL2\" SET INLINE LENGTH 2000",
				"ALTER TABLE \"DB2ADMIN\".\"T1\" ALTER COLUMN \"NEW_COL1\" SET DEFAULT 'WA' ALTER COLUMN \"COL2\" DROP DEFAULT",
				"ALTER TABLE \"DB2ADMIN\".\"T1\" ALTER COLUMN \"COL2\" COMPRESS SYSTEM DEFAULT",
				"SET INTEGRITY FOR \"DB2ADMIN\".\"T1\" OFF CASCADE DEFERRED",
				"ALTER TABLE \"DB2ADMIN\".\"T1\" ALTER COLUMN \"COL4\" DROP EXPRESSION ALTER COLUMN \"COL5\" SET GENERATED ALWAYS AS ( 'bbbb' )",
				"SET INTEGRITY FOR \"DB2ADMIN\".\"T1\" IMMEDIATE CHECKED FULL ACCESS FORCE GENERATED",
				"ALTER TABLE \"DB2ADMIN\".\"T1\" DROP COLUMN \"COL3\""};
		assertEquals("getAlterTableAlterColumns_LUW", expected);

		logger.info("PASSED: Alter Table alter column executed successfully");
	}

	/**
	 * Test the alter table alter columns identity function in admin part.
	 * */
	@Test(description = "Test the alter table alter columns identity action in admin part")
	public void testAlterTableAlterColumnIdentity() throws InterruptedException {

		logger.info("Running Alter Table alter column identity test...");

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"ALTER TABLE \"DB2ADMIN\".\"T1\" ALTER COLUMN \"COL1\" DROP IDENTITY"},
		 *{"statement":"ALTER TABLE \"DB2ADMIN\".\"T2\" ALTER COLUMN \"COL1\" SET GENERATED BY DEFAULT AS IDENTITY"},
		 *{"statement":"ALTER TABLE \"DB2ADMIN\".\"T3\" ALTER COLUMN \"COL1\" SET GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 2 MINVALUE 1 MAXVALUE 62300 CYCLE )"},
		 *{"statement":"ALTER TABLE \"DB2ADMIN\".\"T4\" ALTER COLUMN \"COL1\" SET GENERATED ALWAYS RESTART WITH 3 SET INCREMENT BY 2 SET MINVALUE 1 SET MAXVALUE 62300 SET CYCLE SET NO CACHE"},
		 *{"statement":"ALTER TABLE \"DB2ADMIN\".\"T5\" ALTER COLUMN \"COL1\" RESTART WITH 4 SET INCREMENT BY 4 SET MINVALUE 2 SET NO CYCLE SET CACHE 12"}]}

		 * To retrieve the statement, at first we look over if the length of items
		 * array is correct, then we are looking for the first index of the
		 * items array -> the return is a object, we look for the value of key
		 * "statement", that is $.items[0].statement
		 * */

		String expected[] = {
				"ALTER TABLE \"DB2ADMIN\".\"T1\" ALTER COLUMN \"COL1\" DROP IDENTITY",
				"ALTER TABLE \"DB2ADMIN\".\"T2\" ALTER COLUMN \"COL1\" SET GENERATED BY DEFAULT AS IDENTITY",
				"ALTER TABLE \"DB2ADMIN\".\"T3\" ALTER COLUMN \"COL1\" SET GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 2 MINVALUE 1 MAXVALUE 62300 CYCLE )",
				"ALTER TABLE \"DB2ADMIN\".\"T4\" ALTER COLUMN \"COL1\" SET GENERATED ALWAYS RESTART WITH 3 SET INCREMENT BY 2 SET MINVALUE 1 SET MAXVALUE 62300 SET CYCLE SET NO CACHE",
				"ALTER TABLE \"DB2ADMIN\".\"T5\" ALTER COLUMN \"COL1\" RESTART WITH 4 SET INCREMENT BY 4 SET MINVALUE 2 SET NO CYCLE SET CACHE 12"};
		assertEquals("getAlterTableAlterColumnIdentity_LUW", expected);

		logger.info("PASSED: Alter Table alter column identity executed successfully");
	}
	
	/**
	 * Test the alter table alter columns NON_PROMOTION_DATATYPE function in admin part.
	 * */
	@Test(description = "Test the alter table alter columns NON_PROMOTION_DATATYPE action in admin part")
	public void testAlterTableAlterColumnDatatype() throws InterruptedException {

		logger.info("Running Alter Table alter column NON_PROMOTION_DATATYPE test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * {"NAME":"A1","opr":"DROPCREATE"}
		 * */

		String query = alterService.getJSONData("getAlterTableAlterColumnNonPromotionDatatype_LUW");

		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.ALTER_OBJECT, query, dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		Assert.assertEquals(responseData.getString("opr"), "DROPCREATE");

		logger.info("PASSED: Alter Table alter column NON_PROMOTION_DATATYPE executed successfully");
	}
	
	/**
	 * Test the alter table alter columns PromotionDatatype function in admin part.
	 * */
	@Test(description = "Test the alter table alter columns PromotionDatatype action in admin part")
	public void testPromotionDatatype() throws InterruptedException {

		logger.info("Running Alter Table alter column PROMOTION_DATATYPE test...");

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"ALTER TABLE \"DB2ADMIN\".\"T1\" ALTER COLUMN \"COL1\" SET DATA TYPE INTEGER"},
		 *{"statement":"ALTER TABLE \"DB2ADMIN\".\"A1\" ALTER COLUMN \"COL1\" SET DATA TYPE CHAR(30) ALTER COLUMN \"COL2\" SET DATA TYPE CHAR(25)"}]}

		 * To retrieve the statement, at first we look over if the length of items
		 * array is correct, then we are looking for the first index of the
		 * items array -> the return is a object, we look for the value of key
		 * "statement", that is $.items[0].statement
		 * */

		String expected[] = {
				"ALTER TABLE \"DB2ADMIN\".\"T1\" ALTER COLUMN \"COL1\" SET DATA TYPE INTEGER",
				"ALTER TABLE \"DB2ADMIN\".\"A1\" ALTER COLUMN \"COL1\" SET DATA TYPE CHAR(30) ALTER COLUMN \"COL2\" SET DATA TYPE CHAR(25)"};
		assertEquals("getAlterTableAlterColumnPromotionDatatype_LUW", expected);

		logger.info("PASSED: Alter Table alter column PromotionDatatype executed successfully");
	}
	
	/**
	 * Test the alter table constraints function in admin part.
	 * include: DROP CONSTRAINT,  ADD CONSTRAINT
	 * */
	@Test(description = "Test the alter table constraints action in admin part")
	public void testAlterTableConstraints() throws InterruptedException {

		logger.info("Running Alter Table constraints test...");

		/**
		 * {"items":[{"statement":"ALTER TABLE \"DB2INST1\".\"ACT\" DROP CONSTRAINT \"PK_ACT\""},
		 * {"statement": "ALTER TABLE \"DB2INST1\".\"ACT\" ADD CONSTRAINT \"NEW_CONSTRAINT1\" UNIQUE (\"ACTKWD\" )" }]} 
		 * 
		 * To retrieve the statement, at first we look over if the length of items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */

		String expected[] = {
				"ALTER TABLE \"DB2INST1\".\"ACT\" DROP CONSTRAINT \"PK_ACT\"",
				"ALTER TABLE \"DB2INST1\".\"ACT\" ADD CONSTRAINT \"NEW_CONSTRAINT1\" UNIQUE (\"ACTKWD\" ) NOT ENFORCED"};
		assertEquals("getAlterTableConstraints_LUW", expected);
		
		logger.info("PASSED: Alter Table constraints executed successfully");
	}
	
	/**
	 * @author cyinmei@cn.ibm.com
	 * Test the alter table add column mask and row permission in admin part.
	 * Generate column mask DDL request returns incorrect DDL, opened defect 91430, so comment this method until defect is fixed
	 * */
	@Test(description = "Test the alter table add column mask and row permission in admin part")
	public void testAlterTableAddFGAC() throws InterruptedException {

		logger.info("Running Alter Table add column mask and row permission test...");

		//FGAC is supported in DB2 v10.1 or above, so for v9.7 just return;
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97))
		{ return; }
		
		/**
		 * {"items":[{"statement":"ALTER TABLE \"SCH_FGAC\".\"TAB_FGAC\"  ACTIVATE ROW ACCESS CONTROL"},
		 * {"statement":"ALTER TABLE \"SCH_FGAC\".\"TAB_FGAC\"  ACTIVATE COLUMN ACCESS CONTROL"},
		 * {"statement":"CREATE OR REPLACE PERMISSION \"SCH_FGAC\".\"NEW_PERMISSION1\" ON \"SCH_FGAC\".\"TAB_FGAC\" FOR ROWS WHERE (SESSION_USER='HR') AND (NEW_COLUMN2='HR') ENFORCED FOR ALL ACCESS ENABLE"},
		 * {"statement":"CREATE OR REPLACE MASK \"SCH_FGAC\".\"NEW_COLUMNMASK1\" ON \"SCH_FGAC\".\"TAB_FGAC\" FOR COLUMN \"NEW_COLUMN1\" RETURN CASE WHEN(SESSION_USER='HR') THEN NEW_COLUMN1 ELSE NULL END ENABLE"}]}
		 * */

		String expected[] = {
				"ALTER TABLE \"SCH_FGAC\".\"TAB_FGAC\"  ACTIVATE ROW ACCESS CONTROL",
				"ALTER TABLE \"SCH_FGAC\".\"TAB_FGAC\"  ACTIVATE COLUMN ACCESS CONTROL",
		        "CREATE OR REPLACE PERMISSION \"SCH_FGAC\".\"NEW_PERMISSION1\" ON \"SCH_FGAC\".\"TAB_FGAC\" FOR ROWS WHERE (SESSION_USER='HR') AND (NEW_COLUMN2='HR') ENFORCED FOR ALL ACCESS ENABLE",
		        "CREATE OR REPLACE MASK \"SCH_FGAC\".\"NEW_COLUMNMASK1\" ON \"SCH_FGAC\".\"TAB_FGAC\" FOR COLUMN \"NEW_COLUMN1\" RETURN CASE WHEN(SESSION_USER='HR') THEN NEW_COLUMN1 ELSE NULL END ENABLE"};
		assertEquals("getAlterTableAddFGAC_LUW", expected);
		
		logger.info("PASSED: Alter Table add column mask and row permission executed successfully");
	}
	
	
	/**
	 * @author cyinmei@cn.ibm.com
	 * This case is to execute DDL from testAlterTableAddFGAC() method to verify DDL can be deployed successfully in DB server
	 * Will provide ALTER DDL explicitly while it is the same as the output in testAlterTableAddFGAC() method 
	 * */
	@Test(description = "Test the alter table add column mask and row permission DDL execution action in admin part")
	public void testAlterTableAddFGACExecuteDDL() throws InterruptedException {

		logger.info("Running Alter Table add column mask and row permission DDL execution test...");
		
		//FGAC is supported in DB2 v10.1 or above, so for v9.7 just return;
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97))
		{ return; }

		String alterStatement[] = {
				"ALTER TABLE \"SCH_FGAC\".\"TAB_FGAC\"  ACTIVATE ROW ACCESS CONTROL",
				"ALTER TABLE \"SCH_FGAC\".\"TAB_FGAC\"  ACTIVATE COLUMN ACCESS CONTROL",
		        "CREATE OR REPLACE PERMISSION \"SCH_FGAC\".\"NEW_PERMISSION1\" ON \"SCH_FGAC\".\"TAB_FGAC\" FOR ROWS WHERE (SESSION_USER='HR') AND (NEW_COLUMN2='HR') ENFORCED FOR ALL ACCESS ENABLE",
		        "CREATE OR REPLACE MASK \"SCH_FGAC\".\"NEW_COLUMNMASK1\" ON \"SCH_FGAC\".\"TAB_FGAC\" FOR COLUMN \"NEW_COLUMN1\" RETURN CASE WHEN(SESSION_USER='HR')  THEN NEW_COLUMN1  ELSE NULL END ENABLE"};

        String sqlStatement = null;
		
		for(int i=0; i<alterStatement.length; i++){
			sqlStatement = alterStatement[i];
			
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
		}
		
		logger.info("PASSED: Alter Table constraints add column mask and row permission DDL execution successfully");
	}
	
	/**
	 * @author cyinmei@cn.ibm.com
	 * Test Generate DDL for a table
	 */
	@Test(description = "Test the generate DDL for a table in admin part")
	public void testTableGenerateDDL()  throws InterruptedException {
		
		logger.info("Running generate DDL for table");
		
		/**
		 * Response Data
		 * {"items":[{"ddl":"-- This CLP file was created using DB2LOOK Version \"10.5\" \n-- Timestamp: Fri 29 Jul 2016 01:07:35 PM CST\n-- Database Name: ENCRYPDB       \n-- Database Manager Version: DB2\/LINUXX8664 Version 10.5.5 \n-- Database Codepage: 1208\n-- Database Collating Sequence is: IDENTITY\n-- Alternate collating sequence(alt_collate): null\n-- varchar2 compatibility(varchar2_compat): OFF\n\n\nCONNECT TO ENCRYPDB;\n\n------------------------------------------------\n-- DDL Statements for Table \"SCH_TABGENDDL\".\"TAB_GENDDL\"\n------------------------------------------------\n \nDROP TABLE \"SCH_TABGENDDL\".\"TAB_GENDDL\";\n\n\nCREATE TABLE \"SCH_TABGENDDL\".\"TAB_GENDDL\"  (\n\t\t  \"NEW_COLUMN1\" CHAR(5 OCTETS) )   \n\t\t IN \"TS_TABGENDDL\"  \n\t\t ORGANIZE BY ROW; \n\n\n\n\n\n\nCOMMIT WORK;\n\nCONNECT RESET;"}]} 
		 */
	//String expected ="-- This CLP file was created using DB2LOOK Version \"10.5\" \n-- Timestamp: Fri 29 Jul 2016 10:15:18 AM CST\n-- Database Name: ENCRYPDB       \n-- Database Manager Version: DB2\/LINUXX8664 Version 10.5.5 \n-- Database Codepage: 1208\n-- Database Collating Sequence is: IDENTITY\n-- Alternate collating sequence(alt_collate): null\n-- varchar2 compatibility(varchar2_compat): OFF\n\n\nCONNECT TO ENCRYPDB;\n\n------------------------------------------------\n-- DDL Statements for Table \"DB2INST3\".\"TAB_GENDDL\"\n------------------------------------------------\n \n\nCREATE TABLE \"DB2INST3\".\"TAB_GENDDL\"  (\n\t\t  \"NEW_COLUMN1\" CHAR(5 OCTETS) )   \n\t\t IN \"yoyo\u8868\u7a7a\u95f41\"  \n\t\t ORGANIZE BY ROW; \n\n\n\n\n\n\nCOMMIT WORK;\n\nCONNECT RESET;"; 
	
	String query = alterService.getJSONData("getTableGenerateDDL_LUW");
	
	JSONObject resObj =alterService.callAlterObjectService(AdminAlterObjectService.GENERATE_DDL,query, dbProfile);
	//Get the response data
	JSONObject responseData =(JSONObject) resObj.get("ResponseData");
	
	JSONArray ja = responseData.getJSONArray( "items" );
	
	Assert.assertNotNull( ja );
			
	String ddl = "";
	
	ddl = JsonPath.read( responseData, "$.items[0].ddl" );
	
	Assert.assertTrue(ddl.contains("CONNECT TO"));
	Assert.assertTrue(ddl.contains("DROP TABLE \"SCH_TABGENDDL\".\"TAB_GENDDL\";"));
	
	//if DB2 version is 10.5 or above, the generated DDL will include ORGANIZE BY
	if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_105)){
    	Assert.assertTrue(ddl.contains("\n\n\nCREATE TABLE \"SCH_TABGENDDL\".\"TAB_GENDDL\"  (\n\t\t  \"NEW_COLUMN1\" CHAR(5 OCTETS) )   \n\t\t IN \"TS_TABGENDDL\"  \n\t\t ORGANIZE BY ROW;"));    
	}
	
	//if DB2 version is under 10.5, generated DDL will not include ORGANIZE BY
	if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97)|| this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_101)){
    	
    	Assert.assertTrue(ddl.contains("\n\n\nCREATE TABLE \"SCH_TABGENDDL\".\"TAB_GENDDL\"  (\n\t\t  \"NEW_COLUMN1\" CHAR(5) )   \n\t\t IN \"TS_TABGENDDL\" ;"));  			
	}    
    
    Assert.assertTrue(ddl.contains("COMMIT WORK;"));
	Assert.assertTrue(ddl.endsWith("CONNECT RESET;"));	
	
	
	logger.info("PASSED: generate table DDL is correct");
	
	}
	
	
	/**
	 * @author ybjwang@cn.ibm.com
	 * @throws InterruptedException
	 */
	@Test(description= "Test the rename Index action in Admin Part")
	public void testAlterIndex_Rename() throws InterruptedException{
		
		logger.info("Running Rename An Index");
		
		String expected[] = {
				"RENAME INDEX \"SCHEMA4INDEX\".\"INDTEST0\" TO \"PK_CUSTOMER1\"",
				};
		
		assertEquals("getAlterIndexRename_LUW", expected);
		
		logger.info("PASSED: Rename Index DDL is correct");
		
		/**
		 * Verify the DDL execute successfully
		 */
		String sqlStatement = null;
		
		for(int i=0; i<expected.length; i++){
			sqlStatement = expected[i];
			
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
		}
		/**
		 * Rename it back for dropIndex use.
		 */
		sqlStatement = "RENAME INDEX \"SCHEMA4INDEX\".\"PK_CUSTOMER1\" TO \"INDTEST0\"";
		
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
		
		logger.info("PASSED: Rename index DDL executed successfully");		
	}
	
	/**
	 * @author ybjwang@cn.ibm.com
	 * @throws InterruptedException
	 */
	@Test(description= "Test the Compress Index action in Admin Part")
	public void testAlterIndex_CompressAndExeDDL() throws InterruptedException{
		/**
		 * Administer->Indexes, then select one index and click Compress Index Statistics
		 */
		logger.info("Running Compress Index");
		
		String expected []= {"ALTER INDEX SCHEMA4INDEX.INDTEST2 COMPRESS YES","CALL SYSPROC.ADMIN_CMD( 'REORG TABLE SCHEMA4INDEX.TABLE4INDEX' )"};		
		
		String query = alterService.getJSONData("getAlterIndexCompress_LUW");
		
		JSONObject resObj =alterService.callAlterObjectService(AdminAlterObjectService.COMPRESS_INDEX_COMMAND,query, dbProfile);
		//Get the response Data
		JSONObject responseData =(JSONObject) resObj.get("ResponseData");
		
		JSONArray ja = responseData.getJSONArray( "items" );
		
		Assert.assertNotNull( ja );
				
		String ddl = "";
		
		for(int i =0; i<ja.size(); i++){
			ddl = JsonPath.read( responseData, "$.items[" + i + "].statement" );
			
			Assert.assertEquals( ddl, expected[i] );
		}
		
		logger.info("PASSED: Compress Index DDL is correct");
		
		String sqlStatement = null;
		
		for(int i=0; i<expected.length; i++){
			
			sqlStatement = expected[i];
			
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
		}
		
		logger.info("PASSED: Compress Index DDL execute successfully");
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test collect static of Index action for both verify DDL is correct or not and DDL execution
	 * @throws InterruptedException
	 */
	@Test(description= "Test the Collect Statics of Index action in Admin Part")
	public void testAlterIndex_CollStaticsAndExeDDL() throws InterruptedException{
		/**
		 * Administer->Indexes, then select one index and clickCollect Index Statistics
		 */
		
		String index_statics_expected= "CALL SYSPROC.ADMIN_CMD( 'RUNSTATS ON TABLE SCHEMA4INDEX.TABLE4INDEX ON ALL COLUMNS AND INDEXES ALL SET PROFILE' )";		
		
		String query = alterService.getJSONData("getAlterIndexStatics_LUW");
		
		JSONObject resObj =alterService.callAlterObjectService(AdminAlterObjectService.STATICS_COMMAND,query, dbProfile);
		//Get the response Data
		JSONObject responseData =(JSONObject) resObj.get("ResponseData");
		
		JSONArray ja = responseData.getJSONArray( "items" );
		
		Assert.assertNotNull( ja );
				
		String ddl = "";
		ddl = JsonPath.read( responseData, "$.items[0].statement" );
		
		Assert.assertEquals( ddl, index_statics_expected );
		
		logger.info("PASSED: Collect Statics of Index DDL is correct");
		
		sqlEditorService.runSQLinJDBC(index_statics_expected, ";", this.dbProfile,true);
		
		logger.info("PASSED: Collect Statics of Index DDL exectute successfuly");
	}
	
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test Drop Index action for both verify DDL is correct or not and DDL execution
	 * @throws InterruptedException
	 */
	@Test(description= "Test the drop Index action in Admin Part",dependsOnMethods={"testCreateUniqueIndex"},alwaysRun=true)
	public void testDropIndexAndExeDDL() throws InterruptedException{
		
		logger.info("Running Drop Index");
		
		String expected[] = {"DROP INDEX \"SCHEMA4INDEX\".\"NEW_INDEX1\""};
		
		assertEquals("getDropIndex_LUW", expected);
		
		logger.info("PASSED: Drop index DDL is correct");
		
		/**
		 * Verify the DDL execute successfully
		 */
		String sqlStatement = null;
		
		for(int i=0; i<expected.length; i++){
			sqlStatement = expected[i];
			
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
		}
		
		logger.info("PASSED: Drop index DDL executed successfully");		
	}
	
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test Generate DDL for an Index
	 */
	@Test(description = "Test the generate DDL for an index in admin part")
	public void testAlterIndex_ExtraDDL()  throws InterruptedException {
		
		//db2look doesn't support this kind of Object for DB2v9.7 AND DB2v10.1
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97)){
			return;
			}
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_101)){
			return;
			}
		logger.info("Running Extract DDL for Index");
		
		/**
		 * Response Data
		 * 
		{"items":[
		{"ddl":"\n\nCREATE UNIQUE INDEX \"CUST_CID_XMLIDX\" ON \"CUSTOMER\" \n\t\t(\"INFO\" ASC)\n\t\tGENERATE KEY USING XMLPATTERN '\/customerinfo\/@Cid'\n\t\t  AS SQL DOUBLE   IGNORE INVALID VALUES \n\t\tCOMPRESS NO \n\t\tINCLUDE NULL KEYS ALLOW REVERSE SCANS;"}]}
		 */
	String expected ="\n\nCREATE INDEX \"SCHEMA4INDEX\".\"INDTEST1\" ON \"SCHEMA4INDEX\".\"TABLE4INDEX\" \n\t\t(\"FIELD1\" DESC)\n\t\t\n\t\tCOMPRESS NO \n\t\tINCLUDE NULL KEYS ALLOW REVERSE SCANS;"; 
	
	String query = alterService.getJSONData("getAlterExtractIndexDDL_LUW");
	
	JSONObject resObj =alterService.callAlterObjectService(AdminAlterObjectService.GENERATE_DDL,query, dbProfile);
	//Get the response Data
	JSONObject responseData =(JSONObject) resObj.get("ResponseData");
	
	JSONArray ja = responseData.getJSONArray( "items" );
	
	Assert.assertNotNull( ja );
			
	String ddl = "";
	
	ddl = JsonPath.read( responseData, "$.items[0].ddl" );
	
	Assert.assertEquals( ddl, expected );
	
	logger.info("PASSED: Extract Index DDL is correct");
	
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test Generate DDL for MQTs
	 */
	@Test(description = "Test the generate DDL for an index in admin part")
	public void testAlterMQTs_ExtraDDL()  throws InterruptedException {
	
		logger.info("Running Extract DDL for MQTs");
		
		/**
		 * Response Data
		 * 
		{"items":[{
		"ddl":"-- This CLP file was created using DB2LOOK Version \"11.1\" \n-- Timestamp: Mon 18 Jul 2016 03:17:54 PM CST\n-- Database Name: SAMPLE         \n-- Database Manager Version: DB2\/LINUXX8664 Version 11.1.0 \n-- Database Codepage: 1208\n-- Database Collating Sequence is: IDENTITY\n-- Alternate collating sequence(alt_collate): null\n-- varchar2 compatibility(varchar2_compat): OFF\n\n\nCONNECT TO SAMPLE;\n\n------------------------------------------------\n-- DDL Statements for Table \"SCHEMA4MQT\".\"MQT1\"\n------------------------------------------------\n SET CURRENT SCHEMA = \"DB2INST1\";\nSET CURRENT PATH = \"SYSIBM\",\"SYSFUN\",\"SYSPROC\",\"SYSIBMADM\",\"DB2INST1\";\n\nCREATE TABLE SCHEMA4MQT.MQT1 as (select a.field1 field1MQT from SCHEMA4MQT.TABLE4MQT a) DATA INITIALLY DEFERRED REFRESH DEFERRED ENABLE QUERY OPTIMIZATION MAINTAINED BY USER IN \"IBMDB2SAMPLEREL\" \t\tORGANIZE BY COLUMN;\n\n\n\n\n\n\n\nCOMMIT WORK;\n\nCONNECT RESET;"}]}
		 */
	String query = alterService.getJSONData("getAlterExtractMQTsDDL_LUW");
	
	JSONObject resObj =alterService.callAlterObjectService(AdminAlterObjectService.GENERATE_DDL,query, dbProfile);
	//Get the response Data
	JSONObject responseData =(JSONObject) resObj.get("ResponseData");
	
	JSONArray ja = responseData.getJSONArray( "items" );
	
	Assert.assertNotNull( ja );
			
	String ddl =  JsonPath.read( responseData, "$.items[0].ddl" );
	
	String createString="\n\nCREATE TABLE SCHEMA4MQT.MQT3";
	String maintaniedString="DATA INITIALLY DEFERRED REFRESH DEFERRED";
	String alterString ="ALTER TABLE \"SCHEMA4MQT\".\"MQT3\"";
	String commitString="COMMIT WORK";
	String connectString="CONNECT RESET";
	
	Assert.assertTrue(ddl.contains(commitString));
	Assert.assertTrue(ddl.contains(connectString));

	if (!this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97)){
		
		Assert.assertTrue(ddl.contains(createString));
		Assert.assertTrue(ddl.contains(maintaniedString));
		Assert.assertTrue(ddl.contains(alterString));
	
		}
	logger.info("PASSED: Extract Index DDL is correct");
	
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test Reorganize MQTs
	 */
	@Test(description = "Test Reorganize MQTs in admin part")
	public void testAlterMQTs_Reorganize()  throws InterruptedException {
	
		logger.info("Running Reorganize MQTs");
		/**
		 * {"items":[{
		 * "statement":"CALL SYSPROC.ADMIN_CMD( 'REORG TABLE DB2INST1.ADEFUSR' )"}]}
		 */
		
		String expected[] ={"CALL SYSPROC.ADMIN_CMD( 'REORG TABLE SCHEMA4MQT.MQT3' )"};
		
		String query = alterService.getJSONData( "getAlterReorganizeMQTs_LUW" );

		JSONObject resObj =
				alterService.callAlterObjectService(AdminAlterObjectService.REORGNIZE_MQT_COMMAND,query, dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "items" ) );
		JSONArray ja = responseData.getJSONArray( "items" );

		int size = expected.length;
		Assert.assertNotNull( ja );
		Assert.assertEquals( size, ja.size() );

		String ddl = "";
		for ( int i = 0; i < ja.size(); i++ )
		{
			ddl = JsonPath.read( responseData, "$.items[" + i + "].statement" );
			// Assert.assertTrue(Pattern.matches(expected[i],ddl)); //just for
			// recording
			Assert.assertEquals( ddl, expected[i] );
		}
		logger.info("Reorganize MQTs DDL is corrct");
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test execute DDL of Reorganize MQTs
	 */
	@Test(description = "Test Reorganize MQTs in admin part")
	public void testAlterMQTs_ReorganizeExeDDL()  throws InterruptedException {
		
		logger.info("Running: Reorignize MQT DDL ");
		String ddl = "CALL SYSPROC.ADMIN_CMD( 'REORG TABLE SCHEMA4MQT.MQT4' )";
		sqlEditorService.runSQLinJDBC(ddl, ";", this.dbProfile,true);
		
		logger.info("PASSED: Reorignize MQT DDL run successfully");
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test Reorganize MQTs
	 */
	@Test(description = "Test Reorganize MQTs in admin part")
	public void testAlterMQTs_Compress()  throws InterruptedException {
	
		logger.info("Running Compress MQTs");
		/**
		 * 
		{"items":[
		{"statement":"ALTER TABLE SCHEMA4MQT.MQT3 COMPRESS YES ADAPTIVE"},
		{"statement":"CALL SYSPROC.ADMIN_CMD( 'REORG TABLE SCHEMA4MQT.MQT3' )"}]}
		 */
		
		String expected[] ={"ALTER TABLE SCHEMA4MQT.MQT3 COMPRESS YES ADAPTIVE","CALL SYSPROC.ADMIN_CMD( 'REORG TABLE SCHEMA4MQT.MQT3' )"};
		
		String query = alterService.getJSONData( "getAlterCompressMQTs_LUW" );

		JSONObject resObj =
				alterService.callAlterObjectService(AdminAlterObjectService.COMPRESS_TABLE_COMMAND,query, dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "items" ) );
		JSONArray ja = responseData.getJSONArray( "items" );

		int size = expected.length;
		Assert.assertNotNull( ja );
		Assert.assertEquals( size, ja.size() );

		String ddl = "";
		for ( int i = 0; i < ja.size(); i++ )
		{
			ddl = JsonPath.read( responseData, "$.items[" + i + "].statement" );
			// Assert.assertTrue(Pattern.matches(expected[i],ddl)); //just for
			// recording
			Assert.assertEquals( ddl, expected[i] );
		}
		logger.info("Compress MQTs DDL is corrct");
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test execute DDL of Reorganize MQTs
	 */
	@Test(description = "Test Reorganize MQTs in admin part")
	public void testAlterMQTs_CompressExeDDL()  throws InterruptedException {
		
		logger.info("Running: Compress MQT DDL ");
		List<String> ddlList = new ArrayList<String>();
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97)){
			ddlList.add("ALTER TABLE SCHEMA4MQT.MQT4 COMPRESS YES");
		}else{
			ddlList.add("ALTER TABLE SCHEMA4MQT.MQT4 COMPRESS YES ADAPTIVE");
		}
		ddlList.add("CALL SYSPROC.ADMIN_CMD( 'REORG TABLE SCHEMA4MQT.MQT4' )");
		sqlEditorService.runSQLinJDBC(ddlList, ";", this.dbProfile,true);
		
		logger.info("PASSED: Compressing MQT DDL run successfully");
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test Reorganize MQTs
	 */
	@Test(description = "Test Coll Stats MQTs in admin part")
	public void testAlterMQTs_CollStats()  throws InterruptedException {
	
		logger.info("Running Collect Statics MQTs");
		/**
		 *{"items":[{"statement":"CALL SYSPROC.ADMIN_CMD( 'RUNSTATS ON TABLE SCHEMA4MQT.MQT ON ALL COLUMNS AND
 					INDEXES ALL SET PROFILE' )"}]}
		 */
		
		String expected[] ={"CALL SYSPROC.ADMIN_CMD( 'RUNSTATS ON TABLE SCHEMA4MQT.MQT3' )"};
		
		String query = alterService.getJSONData( "getAlterCollStaticMQTs_LUW" );

		JSONObject resObj =
				alterService.callAlterObjectService(AdminAlterObjectService.STATICS_COMMAND,query, dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "items" ) );
		JSONArray ja = responseData.getJSONArray( "items" );

		int size = expected.length;
		Assert.assertNotNull( ja );
		Assert.assertEquals( size, ja.size() );

		String ddl = "";
		for ( int i = 0; i < ja.size(); i++ )
		{
			ddl = JsonPath.read( responseData, "$.items[" + i + "].statement" );
			// Assert.assertTrue(Pattern.matches(expected[i],ddl)); //just for
			// recording
			Assert.assertEquals( ddl, expected[i] );
		}
		logger.info("Collect Static MQTs DDL is corrct");
	}
	@Test(description = "Test Coll Stats MQTs in admin part")
	public void testAlterMQTs_CollStatsExeDDL()  throws InterruptedException {
		
		logger.info("Executing Collect Statics MQTs DDL");
		/**
		 * {"items":[{
		 * "statement":"CALL SYSPROC.ADMIN_CMD( 'REORG TABLE DB2INST1.ADEFUSR' )"}]}
		 */
		
		String ddl ="CALL SYSPROC.ADMIN_CMD( 'RUNSTATS ON TABLE SCHEMA4MQT.MQT3' )";
		sqlEditorService.runSQLinJDBC(ddl, ";", this.dbProfile,true);
		
		logger.info("PASSED: Collect Static MQT DDL run successfully");
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test the view data for MQT1
	 * 
	 */
	@Test(description="Test View data for MQT")
	public void testViewMQTData ()throws InterruptedException{
		logger.info("Running View MQT1 data...");
		/**
		 * {"cells":[
		 * {"field":"FIELD1MAQT","alwaysShown":"true","datatype":"string","applyTooltip":true,"dataType":"string","name":"NEW_COLUMN1 [CHAR]","internalDataType":"CHAR"}],
		 * "exceed":false,
		 * "items":[{"FIELD1MAT":"16 "}]}
		 */
		String query = alterService.getJSONData("getViewMQTData_LUW");
		JSONObject resObj =alterService.callAlterObjectService(AdminAlterObjectService.BROWSEDATA_COMMAND,query, dbProfile);
		//Get the response Data
		JSONObject responseData =(JSONObject) resObj.get("ResponseData");
		String field1_value = JsonPath.read( responseData, "$.items[0].FIELD1" );
		String field2_value = JsonPath.read( responseData, "$.items[0].FIELD2" );
		Assert.assertEquals(field1_value, "16");
		Assert.assertEquals(field2_value, "17");
		
		logger.info("View MQT data is correct");
	}
	/**
	 * Test the alter triggers action in admin part
	 * include: CREATE TRIGGER AFTER INSERT OR UPDATE ,input string "when clause" in When text box and "action clause" in Action text box
	 * DROP TRIGGER
	 * */
	@Test(description = "Test the alter triggers action in admin part")
	public void testAlterTriggers() throws InterruptedException {

		logger.info("Running Alter Triggers...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"items":[{"statement":"CREATE TRIGGER \"OPM\".\"NEW_TRIGGER2\" AFTER INSERT OR UPDATE OF ACTKWD,ACTDESC ON \"DB2INST1\".\"ACT\" FOR EACH STATEMENT SECURED WHEN ( \"when clause\" ) \"action clause\""},
		 * {"statement":"CREATE TRIGGER \"DB2INST1\".\"NEW_TRIGGER4\" BEFORE INSERT ON \"DB2INST1\".\"ACT\" FOR EACH ROW"},
		 * {"statement":"DROP TRIGGER \"DB2INST1\".\"NEW_TRIGGER1\""}]}
		 * 
		 * To retrieve the statement, at first we look over if the length of items array is correct, then we are looking for the first index of the items array
		 * -> the return is a object, we look for the value of key "statement",
		 * that is $.items[0].statement
		 * */
		String expected[] = {
				"CREATE TRIGGER \"OPM\".\"NEW_TRIGGER2\" AFTER INSERT OR UPDATE OF ACTKWD,ACTDESC ON \"DB2INST1\".\"ACT\" FOR EACH STATEMENT SECURED WHEN ( \"when clause\" ) \"action clause\"",
				"CREATE TRIGGER \"DB2INST1\".\"NEW_TRIGGER4\" BEFORE INSERT ON \"DB2INST1\".\"ACT\" FOR EACH ROW",
				"DROP TRIGGER \"DB2INST1\".\"NEW_TRIGGER1\""};
		assertEquals("getAlterTriggers_LUW", expected);

		logger.info("PASSED: Alter triggers executed successfully");
	}
	
	/**
	 * Test the alter privileges action in admin part
	 * include: GRANT INDEX ON TABLE, GRANT DELETE ON TABLE, GRANT INSERT ON TABLE, GRANT ALTER ON TABLE, GRANT ROLE
	 * */
	@Test(description = "Test the alter dependencies action in admin part")
	public void testAlterPrivileges() throws InterruptedException {

		logger.info("Running Alter privileges...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * [{"statement":"GRANT 32, 4 ON TABLE \"DB2INST1\".\"ACT\" TO ROLE \"SYSDEBUGPRIVATE\""},
		 * {"statement":"GRANT 1 ON TABLE \"DB2INST1\".\"ACT\" TO ROLE \"SYSDEBUG\""},
		 * {"statement":"GRANT ROLE \"SYSDEBUG\" TO GROUP \"DB2IADM1\""}]
		 * 
		 * To retrieve the statement, at first we look over if the length of items array is correct, then we are looking for the first index of the items array
		 * -> the return is a object, we look for the value of key "statement",
		 * that is $.items[0].statement
		 * */
		String expected[] = {
				"GRANT DELETE ON TABLE \"DB2INST1\".\"ABC\" TO USER \"USER1\" WITH GRANT OPTION",
				"GRANT DELETE ON TABLE \"DB2INST1\".\"ABC\" TO ROLE \"SYSTS_ADM\" WITH GRANT OPTION",
				"GRANT DELETE ON TABLE \"DB2INST1\".\"ABC\" TO ROLE \"SYSTS_USR\" WITH GRANT OPTION"};
		assertEquals("getAlterPrivileges_LUW", expected);

		logger.info("PASSED: Alter privileges executed successfully");
	}
	

	/**
	 * Test the alter privileges action in admin part
	 * */
	@Test(description = "Test the alter distribution action in admin part")
	public void testAlterDistribution() throws InterruptedException {

		logger.info("Running Alter Distribution...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"items":[{"statement":"ALTER TABLE \"DB2INST1\".\"CL_SCHED\" ADD DISTRIBUTE BY HASH (\"ENDING\")"}]}
		 * 
		 * To retrieve the statement, at first we look over if the length of items array is correct, then we are looking for the first index of the items array
		 * -> the return is a object, we look for the value of key "statement",
		 * that is $.items[0].statement
		 * */
		String expected[] = {
				"ALTER TABLE \"DB2INST1\".\"CL_SCHED\" ADD DISTRIBUTE BY HASH (\"ENDING\")"};
		assertEquals("getAlterDistribution_LUW", expected);

		logger.info("PASSED: Alter distribution executed successfully");

	}
	
	/**
	 * Test the alter data partitions statement
	 * include: add
	 * */
	@Test(description = "Test the alter  data partitions action in admin part")
	public void testAddPartitions() throws InterruptedException {
		logger.info("Running Alter Data Partitions test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * To retrieve the statement, we are looking for the 0 index of the
		 * items array -> the return is a object, we look for the value of key
		 * "statement" in the returned object, that is $.items[0].statement
		 * */
		String expected[] = {
				"ALTER TABLE \"DB2ADMIN\".\"T1\" ADD DISTRIBUTE BY HASH (\"COL1\",\"COL2\")",
				"ALTER TABLE \"DB2ADMIN\".\"T2\" DROP DISTRIBUTION",
				"ALTER TABLE \"DB2ADMIN\".\"T2\" ADD DISTRIBUTE BY HASH (\"COL1\",\"COL2\")",
				"ALTER TABLE \"DB2ADMIN\".\"T3\" DROP DISTRIBUTION",
				"CREATE TABLE \"DB2ADMIN\".\"T4\" ( \"COL1\" CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', \"COL2\" INTEGER WITH DEFAULT 32 ) DISTRIBUTE BY HASH (\"COL1\",\"COL2\")" };

		assertEquals("getAddPartitions_LUW", expected);

		logger.info("PASSED: Alter data partitions executed successfully");

	}
	/**
	 * Test the alter data partitions statement
	 * include: drop
	 * */
	@Test(description = "Test the drop  data partitions action in admin part")
	public void testDropPartitions() throws InterruptedException {
		logger.info("Running Drop Data Partitions test...");

		String query = alterService.getJSONData("getDropPartitions_LUW");
		
		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.ALTER_OBJECT, query, dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		Assert.assertEquals(responseData.getString("opr"), "DROPCREATE");

		logger.info("PASSED: Drop data partitions executed successfully");

	}
		
	/**
	 * Test the alter table space action in admin part
	 * */
	@Test(description = "Test the alter table space action in admin part")
	public void testAlterTableSpace() throws InterruptedException {

		logger.info("Running Alter Table Space...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"items":[{"statement":"ALTER TABLESPACE \"SYSCATSPACE\" DROPPED TABLE RECOVERY ON AUTORESIZE YES INCREASESIZE Default K MANAGED BY AUTOMATIC STORAGE USING STOGROUP"},
		 * {"statement":"RENAME TABLESPACE \"SYSCATSPACE\" TO \"SYSCATSPACE1\""},
		 * {"statement":"ALTER TABLESPACE \"SYSCATSPACE1\" DROPPED TABLE RECOVERY ON AUTORESIZE YES INCREASESIZE Default K MANAGED BY AUTOMATIC STORAGE USING STOGROUP"}]}

		 * To retrieve the statement, at first we look over if the length of
		 * items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */
		

		
		String expected[] = {
		"ALTER TABLESPACE \"SYSCATSPACE\" DROPPED TABLE RECOVERY ON MANAGED BY AUTOMATIC STORAGE USING STOGROUP",
		"RENAME TABLESPACE \"SYSCATSPACE\" TO \"SYSCATSPACE1\"",
		"ALTER TABLESPACE \"SYSCATSPACE1\" DROPPED TABLE RECOVERY ON MANAGED BY AUTOMATIC STORAGE USING STOGROUP",
		"GRANT 0 ON TABLESPACE \"SYSCATSPACE1\" TO ROLE \"SYSDEBUGPRIVATE\" WITH GRANT OPTION",
		"GRANT 0 ON TABLESPACE \"SYSCATSPACE1\" TO GROUP \"DB2IADM1\" WITH GRANT OPTION"};

		assertEquals("getAlterTablespace_LUW", expected);

		logger.info("PASSED: Alter table space executed successfully");

	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Genereate DDL for alter TableSpace
	 */
	@Test(description = "Test the alter table space action in admin part")
	public void testAlterTableSpaceExecuteDDL() throws InterruptedException {

		logger.info("Executing DDL for Alter Table Space...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"items":[{"statement":"ALTER TABLESPACE \"SYSCATSPACE\" DROPPED TABLE RECOVERY ON AUTORESIZE YES INCREASESIZE Default K MANAGED BY AUTOMATIC STORAGE USING STOGROUP"},
		 * {"statement":"RENAME TABLESPACE \"SYSCATSPACE\" TO \"SYSCATSPACE1\""},
		 * {"statement":"ALTER TABLESPACE \"SYSCATSPACE1\" DROPPED TABLE RECOVERY ON AUTORESIZE YES INCREASESIZE Default K MANAGED BY AUTOMATIC STORAGE USING STOGROUP"}]}

		 * To retrieve the statement, at first we look over if the length of
		 * items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */
		

		
		String ddl = "ALTER TABLESPACE SYSCATSPACE DROPPED TABLE RECOVERY ON AUTORESIZE YES";
		sqlEditorService.runSQLinJDBC(ddl, ";", this.dbProfile,true);
		logger.info("PASSED: Alter table space executed successfully");

	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test the Generate TableSpace DDL
	 */
	@Test(description="Generate TableSpace DDL executing")
	public void testTableSpaceGenerateDDL() throws InterruptedException{
		
		logger.info("Running Generate DDL for TableSpace");
		
		/**
		 * Response Data
		 * 
		{"items":[{
		"ddl":ALTER TABLESPACE \"SYSCATSPACE\"\n      PREFETCHSIZE AUTOMATIC\n      OVERHEAD INHERIT\n      NO FILE SYSTEM CACHING \n      AUTORESIZE YES \n      TRANSFERRATE INHERIT;\n\n\nALTER TABLESPACE \"SYSCATSPACE\"\n      USING STOGROUP \"IBMSTOGROUP\"; \n\n\nALTER TABLESPACE \"TEMPSPACE1\"\n      PREFETCHSIZE AUTOMATIC\n      OVERHEAD INHERIT\n      FILE SYSTEM CACHING \n      TRANSFERRATE INHERIT;\n\n\nALTER TABLESPACE \"USERSPACE1\"\n      PREFETCHSIZE AUTOMATIC\n      OVERHEAD INHERIT\n      NO FILE SYSTEM CACHING \n      AUTORESIZE YES \n      TRANSFERRATE INHERIT\n      DATA TAG INHERIT;\n\n\nALTER TABLESPACE \"USERSPACE1\"\n      USING STOGROUP \"IBMSTOGROUP\"; \n\n\nCOMMIT WORK;\n\nCONNECT RESET;\n\n"
		 */
	String query = alterService.getJSONData("getGenerateTableSpaceDDL_LUW");
	
	JSONObject resObj =alterService.callAlterObjectService(AdminAlterObjectService.GENERATE_DDL,query, dbProfile);
	//Get the response Data
	JSONObject responseData =(JSONObject) resObj.get("ResponseData");
	
	JSONArray ja = responseData.getJSONArray( "items" );
	
	Assert.assertNotNull( ja );
			
	String ddl =  JsonPath.read( responseData, "$.items[0].ddl" );
	
	String alterString="ALTER TABLESPACE \"SYSCATSPACE\"";
	String alterString_DB29="ALTER TABLESPACE SYSCATSPACE";
	String prefetchsizeString="PREFETCHSIZE AUTOMATIC";
	String overheadString="OVERHEAD";
	String autoresizeString="AUTORESIZE YES";
	String transferrateString="TRANSFERRATE";
	String commitString="COMMIT WORK";
	String connectString="CONNECT RESET";
	
	if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97))
		{ 	Assert.assertTrue(ddl.contains(alterString_DB29)); 
		}else{
			Assert.assertTrue(ddl.contains(alterString));
		}	
	Assert.assertTrue(ddl.contains(prefetchsizeString));
	Assert.assertTrue(ddl.contains(overheadString));
	Assert.assertTrue(ddl.contains(autoresizeString));
	Assert.assertTrue(ddl.contains(transferrateString));
	Assert.assertTrue(ddl.contains(commitString));
	Assert.assertTrue(ddl.contains(connectString));
	
	logger.info("PASSED: GENERATE TABLESPACE DDL IS CORRECT");
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test the create table-space statement
	 * */
	@Test(description = "Test the create table-space action in admin part")
	public void testCreateLargeTableSpace() throws InterruptedException {
		logger.info("Running Create Table Space test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":
		 * "CREATE TABLESPACE \"NEW_TABLESPACE\" MANAGED BY AUTOMATIC STORAGE USING STOGROUP IBMSTOGROUP AUTORESIZE YES INITIALSIZE 4 K INCREASESIZE 5 PERCENT MAXSIZE 200 K FILE SYSTEM CACHING DATA TAG 1"
		 * }]}
		 * 
		 * To retrieve the statement, we are looking for the 0 index of the
		 * items array -> the return is a object, we look for the value of key
		 * "statement" in the returned object, that is $.items[0].statement
		 * */
		String expected[] = {
		"CREATE TABLESPACE \"NEW_TABLESPACE\" MANAGED BY AUTOMATIC STORAGE USING STOGROUP IBMSTOGROUP AUTORESIZE YES INITIALSIZE 100 K INCREASESIZE 5 PERCENT MAXSIZE 200 K FILE SYSTEM CACHING DATA TAG 1"};

		assertEquals("getCreateTableSpace_LUW", expected);

		logger.info("PASSED: Create Table Space executed successfully");

	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test the create table-space statement
	 * */
	@Test(description = "Test Create large table space with managed by automatic storage for my DB in admin part")
	public void testCreateLargeTableSpaceExecuteDDL() throws InterruptedException {
		logger.info("Execute Create Table Space DDL...");

		
		String ddl = "CREATE TABLESPACE \"NEW_TABLESPACE\"";

		sqlEditorService.runSQLinJDBC(ddl, ";", this.dbProfile,true);

		logger.info("PASSED: Create Table Space DDL can be executed successfully");

	}
	/**
	 * Test the create table-space statement
	 * */
	@Test(description = "Test the create table-space action in admin part")
	public void testCreateRegularTableSpace() throws InterruptedException {
		logger.info("Running Create Table Space test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{
		 * "statement":"CREATE REGULAR TABLESPACE \"NEW_TABLESPACE\" MANAGED BY SYSTEM USING ( 'Container1' )"}]}
		 * 
		 * To retrieve the statement, we are looking for the 0 index of the
		 * items array -> the return is a object, we look for the value of key
		 * "statement" in the returned object, that is $.items[0].statement
		 * */
		String expected[] = {
		"CREATE REGULAR TABLESPACE \"NEW_TS_REGULAR\" MANAGED BY SYSTEM USING ( 'Container1' )"};

		assertEquals("getCreateTableSpace_Regular_LUW", expected);

		logger.info("PASSED: Create Regular Table Space DDL is correct");

	}
	/**
	 * Test the create table-space statement
	 * */
	@Test(description = "Test the create regular table-space action in admin part")
	public void testCreateRegularTableSpaceExcDDL() throws InterruptedException {
		logger.info("Execute Create Table Space DDL...");

		
		String ddl = "CREATE REGULAR TABLESPACE \"NEW_TS_REGULAR\" MANAGED BY SYSTEM USING ( 'Container1' )";

		sqlEditorService.runSQLinJDBC(ddl, ";", this.dbProfile,true);

		logger.info("PASSED: Create Regular Table Space DDL can be executed successfully");

	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test the create table-space statement
	 * */
	@Test(description = "Test the create system temporary table-space action in admin part")
	public void testCreateSysTempTableSpace() throws InterruptedException {
		logger.info("Running Create System Tempoary Table Space test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{
		 * "statement":"CREATE SYSTEM TEMPORARY TABLESPACE \"NEW_TABLESPACE\" IN DATABASE PARTITION GROUP \"IBMTEMPGROUP\""}]}
		 * 
		 * To retrieve the statement, we are looking for the 0 index of the
		 * items array -> the return is a object, we look for the value of key
		 * "statement" in the returned object, that is $.items[0].statement
		 * */
		String expected[] = {
		"CREATE SYSTEM TEMPORARY TABLESPACE \"NEW_TS_SYSTEMP\" IN DATABASE PARTITION GROUP \"IBMTEMPGROUP\" MANAGED BY AUTOMATIC STORAGE"};

		assertEquals("getCreateTableSpace_SysTemp_LUW", expected);

		logger.info("PASSED: Create Table Space executed successfully");

	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test the create table-space statement
	 * */
	@Test(description = "Test the create System Temporary table-space action in admin part")
	public void testCreateSysTempTableSpacEDDL() throws InterruptedException {
		
		logger.info("Execute Create System Temporary Table Space DDL...");

		String ddl = "CREATE SYSTEM TEMPORARY TABLESPACE \"NEW_TS_SYSTEMP\" IN DATABASE PARTITION GROUP \"IBMTEMPGROUP\"";

		sqlEditorService.runSQLinJDBC(ddl, ";", this.dbProfile,true);

		logger.info("PASSED: Create System Temporary Table Space DDL can be executed successfully");

	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test the create table-space statement
	 * */
	@Test(description = "Test the create user temporary table-space action in admin part")
	public void testCreateUserTempTableSpace() throws InterruptedException {
		logger.info("Running Create System Temporary Table Space test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{
		 * "statement":"CREATE USER TEMPORARY TABLESPACE \"NEW_TABLESPACE\""}]}
		 * 
		 * To retrieve the statement, we are looking for the 0 index of the
		 * items array -> the return is a object, we look for the value of key
		 * "statement" in the returned object, that is $.items[0].statement
		 * */
		String expected[] = {
		"CREATE USER TEMPORARY TABLESPACE \"NEW_TS_USERTEMP\" MANAGED BY AUTOMATIC STORAGE"};

		assertEquals("getCreateTableSpace_UserTemp_LUW", expected);

		logger.info("PASSED: Create User Temporary Table Space executed successfully");

	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test the create table-space statement
	 * */
	@Test(description = "Test the create user temporary table-space action in admin part")
	public void testUserTempCreateTableSpaceExcDDL() throws InterruptedException {
		
		logger.info("Execute Create User Temporary Table Space DDL...");

		String ddl = "CREATE USER TEMPORARY TABLESPACE \"NEW_TS_USERTEMP\"";

		sqlEditorService.runSQLinJDBC(ddl, ";", this.dbProfile,true);

		logger.info("PASSED: Create User Temporary Table Space DDL can be executed successfully");

	}
	/**
	 * Author:Wang Yun
	 * Test the drop index action in Admin part
	 */
	@Test(description = "Test the drop table space action in admin part")
	public void testDropTableSpace() throws InterruptedException{
		
		logger.info("Running Drop Table Space...");
		
		String expected[] = {"DROP TABLESPACE \"NEW_TABLESPACE\""};
		
		assertEquals("getDropTableSpace_LUW", expected);

		logger.info("PASSED: Drop TableSpace DDL is correct");
	}
	
	/**
	**@author ybjwang@cn.ibm.com
	*Drop all the tablespaces that created for testing tablespace
	*/
	@Test(description = "Test the drop table-space action in admin part",dependsOnMethods={"testUserTempCreateTableSpaceExcDDL","testCreateSysTempTableSpacEDDL","testCreateRegularTableSpaceExcDDL","testCreateLargeTableSpaceExecuteDDL"},alwaysRun=true)
	public void testDropTableSpaceExecuteDDL() throws InterruptedException {
		logger.info("Running Drop Table Space test...");

		List<String> ddlList = new ArrayList<String>();

		ddlList.add("DROP TABLESPACE NEW_TABLESPACE");
		ddlList.add("DROP TABLESPACE NEW_TS_USERTEMP");
		ddlList.add("DROP TABLESPACE NEW_TS_SYSTEMP");
		ddlList.add("DROP TABLESPACE NEW_TS_REGULAR");
					
		sqlEditorService.runSQLinJDBC(ddlList, ";", this.dbProfile, true);

		logger.info("PASSED: Drop Table Space DDL can be executed successfully");

	}
	/**
	 * Test the create simple formatter table action in admin part
	 * */
	@Test(description = "Test the create simple formatter table action in admin part")
	public void testCreateTableSimpleFormatter() throws InterruptedException {

		logger.info("Running Create Simple Formatter Table...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"items":[{"statement":"CREATE TABLE DB2ADMIN.T1 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL )"}]}
		 * 
		 * To retrieve the statement, at first we look over if the length of
		 * items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */
		String expected[] = {
		"CREATE TABLE DB2ADMIN.T1 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL )"};

		assertEquals("getCreateTableSimpleFormatter_LUW", expected);

		logger.info("PASSED: Create simple formatter table executed successfully");

	}
	
	/**
	 * Test the create table with alter options action in admin part
	 * */
	@Test(description = "Test create table with alter options action in admin part")
	public void testCreateTableWithAlterOptions() throws InterruptedException {

		logger.info("Running Create Table with alter options...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"items":[{"statement":"CREATE TABLE \"DB2INST1\".\"TABLE1\" ( \"NEW_COLUMN1\" CHAR(5) WITH DEFAULT aaaa, \"NEW_COLUMN2\" INTEGER, \"NEW_COLUMN3\" BLOB(1024) )"},
		 * {"statement":"ALTER TABLE \"DB2INST1\".\"TABLE1\" VOLATILE APPEND ON"}]}
		 * 
		 * To retrieve the statement, at first we look over if the length of
		 * items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */
		String expected[] = {
		"CREATE TABLE \"DB2INST1\".\"TABLE1\" ( \"NEW_COLUMN1\" CHAR(5) WITH DEFAULT aaaa, \"NEW_COLUMN2\" INTEGER, \"NEW_COLUMN3\" BLOB(1024) )",
		"ALTER TABLE \"DB2INST1\".\"TABLE1\" VOLATILE APPEND ON"};

		assertEquals("getCreateTableWithAlterOptions_LUW", expected);

		logger.info("PASSED: Create table with alter options executed successfully");

	}
	
	/**
	 * Test the create table data type action in admin part
	 * */
	@Test(description = "Test create table data type action in admin part")
	public void testCreateTableDataType() throws InterruptedException {
		
		logger.info("Running Create Table data type...");

		String query = alterService.getJSONData("getCreateTableDataType_LUW");
		
		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.ALTER_OBJECT, query, dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);
		Assert.assertTrue(responseData.containsKey("items"));
		JSONArray ja=responseData.getJSONArray("items");
		
		Assert.assertNotNull(ja);
		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"items":[{"statement":"CREATE TABLE DB2ADMIN.T1 ( ccc SMALLINT, eee INTEGER, ddd BIGINT, bbb FLOAT(12), fff DOUBLE, aaa DECIMAL(12, 2), ggg NUMERIC(12, 2), hhh REAL, iii BOOLEAN, jjj DECFLOAT(16), col1 CHAR(25 OCTETS), col2 VARCHAR(25 OCTETS), col3 LONG VARCHAR, col4 CLOB(1024 OCTETS) INLINE LENGTH 68, col5 DBCLOB(1024 CODEUNITS16) INLINE LENGTH 68, col6 BLOB(1024), col7 GRAPHIC(10 CODEUNITS16), col8 VARGRAPHIC(10 CODEUNITS16), col9 LONG VARGRAPHIC, col10 DATE, col11 TIME, col12 TIMESTAMP(9), col13 CHAR(32) FOR BIT DATA, col14 VARCHAR(32) FOR BIT DATA, col15 LONG VARCHAR FOR BIT DATA )"}]}
		 * 
		 * To retrieve the statement, at first we look over if the length of
		 * items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */

		String ddl = JsonPath.read(responseData,"$.items[0].statement");
		Assert.assertTrue(ddl.startsWith("CREATE TABLE DB2ADMIN.T1 ( ccc SMALLINT, eee INTEGER, ddd BIGINT,"));
		Assert.assertTrue(ddl.endsWith("col15 LONG VARCHAR FOR BIT DATA )"));
		logger.info("PASSED: Create table data type executed successfully");

	}
	
	/**
	 * Test the create table column generated identity options action in admin part
	 * */
	@Test(description = "Test create table column generated identity options action in admin part")
	public void testCreateTableColumnGeneratedIdentityOptions() throws InterruptedException {

		logger.info("Running Create Table column generated identity options...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 *  {"items":[{"statement":"CREATE TABLE DB2ADMIN.T1 ( 
		 *  col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', 
		 *  col2 INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 2000 NO CYCLE CACHE 20 ORDER ), 
		 *  col3 INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 2000 NO CYCLE CACHE 20 ), 
		 *  col4 INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 2000 NO CYCLE ), 
		 *  col5 INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 2000 CYCLE ), 
		 *  col6 INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 2000 ), 
		 *  col7 INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 1 MINVALUE 1 ), 
		 *  col8 INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 1 ), 
		 *  col9 INTEGER NOT NULL GENERATED BY DEFAULT AS IDENTITY ( START WITH 1 INCREMENT BY 1 ), 
		 *  col10 INTEGER NOT NULL GENERATED BY DEFAULT AS IDENTITY ( START WITH 1 INCREMENT BY 1 ), 
		 *  col11 TIMESTAMP NOT NULL GENERATED BY DEFAULT FOR EACH ROW ON UPDATE AS ROW CHANGE TIMESTAMP, 
		 *  col12 CLOB(1024) INLINE LENGTH 68 NOT NULL )"}]}
		 *  
		 * To retrieve the statement, at first we look over if the length of
		 * items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */
		String expected[] = {
		"CREATE TABLE DB2ADMIN.T1 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 2000 NO CYCLE CACHE 20 ORDER ), col3 INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 2000 NO CYCLE CACHE 20 ), col4 INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 2000 NO CYCLE ), col5 INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 2000 CYCLE ), col6 INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 2000 ), col7 INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 1 MINVALUE 1 ), col8 INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 1 ), col9 INTEGER NOT NULL GENERATED BY DEFAULT AS IDENTITY ( START WITH 1 INCREMENT BY 1 ), col10 INTEGER NOT NULL GENERATED BY DEFAULT AS IDENTITY ( START WITH 1 INCREMENT BY 1 ), col11 TIMESTAMP NOT NULL GENERATED BY DEFAULT FOR EACH ROW ON UPDATE AS ROW CHANGE TIMESTAMP, col12 CLOB(1024) INLINE LENGTH 68 NOT NULL )"};

		assertEquals("getCreateTableColumnGeneratedIdentityOptions_LUW", expected);

		logger.info("PASSED: Create table column generated identity options executed successfully");

	}
	
	/**
	 * Test the create table column other options action in admin part
	 * */
	@Test(description = "Test create table column other options action in admin part")
	public void testCreateTableColumnOtherOptions() throws InterruptedException {

		logger.info("Running Create Table column other options...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 *  {"items":[{"statement":"CREATE TABLE DB2ADMIN.T1 ( 
		 *  col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', 
		 *  col2 INTEGER NOT NULL COMPRESS SYSTEM DEFAULT, 
		 *  col3 INTEGER NOT NULL, 
		 *  col4 INTEGER NOT NULL IMPLICITLY HIDDEN, 
		 *  col5 INTEGER NOT NULL, 
		 *  col6 CLOB(1024) INLINE LENGTH 68 NOT NULL, 
		 *  col7 CLOB(1024) INLINE LENGTH 68 NOT LOGGED, 
		 *  col8 CLOB(1024) INLINE LENGTH 68 COMPACT )"}]}
		 *  
		 * To retrieve the statement, at first we look over if the length of
		 * items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */
		String expected[] = {
		"CREATE TABLE DB2ADMIN.T1 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL COMPRESS SYSTEM DEFAULT, col3 INTEGER NOT NULL, col4 INTEGER NOT NULL IMPLICITLY HIDDEN, col5 INTEGER NOT NULL, col6 CLOB(1024) INLINE LENGTH 68 NOT NULL, col7 CLOB(1024) INLINE LENGTH 68 NOT LOGGED, col8 CLOB(1024) INLINE LENGTH 68 COMPACT )"};

		assertEquals("getCreateTableColumnOtherOptions_LUW", expected);

		logger.info("PASSED: Create table column other options executed successfully");

	}
	
	/**
	 * Test the create table with data capture action in admin part
	 * */
	@Test(description = "Test create table  with data capture action in admin part")
	public void testCreateTableWithDataCapture() throws InterruptedException {

		logger.info("Running Create Table  with data capture...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 *  {"items":[{"statement":"CREATE TABLE DB2ADMIN.T1 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL ) DATA CAPTURE CHANGES"},
		 *  {"statement":"CREATE TABLE DB2ADMIN.T2 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL )"}]}
		 *  
		 * To retrieve the statement, at first we look over if the length of
		 * items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */
		String expected[] = {
		"CREATE TABLE DB2ADMIN.T1 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL ) DATA CAPTURE CHANGES",
		"CREATE TABLE DB2ADMIN.T2 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL )"};

		assertEquals("getCreateTableWithDataCapture_LUW", expected);

		logger.info("PASSED: Create table  with data capture executed successfully");

	}
	
	/**
	 * Test the create table with compress action in admin part
	 * */
	@Test(description = "Test create table  with compress action in admin part")
	public void testCreateTableWithCompress() throws InterruptedException {

		logger.info("Running Create Table  with compress...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 *  {"items":[{"statement":"CREATE TABLE DB2ADMIN.T1 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL ) COMPRESS YES ADAPTIVE VALUE COMPRESSION"},
		 *  {"statement":"CREATE TABLE DB2ADMIN.T2 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL ) COMPRESS YES STATIC"},
		 *  {"statement":"CREATE TABLE DB2ADMIN.T3 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL ) COMPRESS YES"},
		 *  {"statement":"CREATE TABLE DB2ADMIN.T4 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL ) COMPRESS NO"}]}
		 * To retrieve the statement, at first we look over if the length of
		 * items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */
		String expected[] = {
		"CREATE TABLE DB2ADMIN.T1 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL ) COMPRESS YES ADAPTIVE VALUE COMPRESSION",
		"CREATE TABLE DB2ADMIN.T2 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL ) COMPRESS YES STATIC",
		"CREATE TABLE DB2ADMIN.T3 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL ) COMPRESS YES",
		"CREATE TABLE DB2ADMIN.T4 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL ) COMPRESS NO"};

		assertEquals("getCreateTableWithCompress_LUW", expected);

		logger.info("PASSED: Create table  with compress executed successfully");

	}
	
	
	/**
	 * Test the create table with not logged initially action in admin part
	 * */
	@Test(description = "Test create table with not logged initially action in admin part")
	public void testCreateTableWithNotLoggedInitially() throws InterruptedException {

		logger.info("Running Create Table with not logged initially...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"items":[{"statement":"CREATE TABLE DB2ADMIN.T1 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL ) NOT LOGGED INITIALLY"}]}
		 * 
		 * To retrieve the statement, at first we look over if the length of
		 * items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */
		String expected[] = {
		"CREATE TABLE DB2ADMIN.T1 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL ) NOT LOGGED INITIALLY"};

		assertEquals("getCreateTableWithNotLoggedInitially_LUW", expected);

		logger.info("PASSED: Create table with not logged initially executed successfully");

	}
	/**
	 * Test the create table with table space action in admin part
	 * */
	@Test(description = "Test create table with table space action in admin part")
	public void testCreateTableWithTableSpace() throws InterruptedException {

		logger.info("Running Create Table with table space initially...");

		/**
		 * Path the response data, for this case, the response data is:
		 *  
		 *  {"items":[{"statement":"CREATE TABLE DB2ADMIN.T1 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL ) IN Tablespace CYCLE"},
		 *  {"statement":"CREATE TABLE DB2ADMIN.T2 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL ) IN TABLESPACE NO CYCLE INDEX IN Tablespace_index"},
		 *  {"statement":"CREATE TABLE DB2ADMIN.T3 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL ) IN Tablespace INDEX IN Tablespace_index LONG IN Tablespace_long"}]}
		 *  
		 * To retrieve the statement, at first we look over if the length of
		 * items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */
		String expected[] = {
		"CREATE TABLE DB2ADMIN.T1 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL ) IN Tablespace CYCLE",
		"CREATE TABLE DB2ADMIN.T2 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL ) IN TABLESPACE NO CYCLE INDEX IN Tablespace_index",
		"CREATE TABLE DB2ADMIN.T3 ( col1 CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', col2 INTEGER NOT NULL, col3 CLOB(1024) INLINE LENGTH 68 NOT NULL ) IN Tablespace INDEX IN Tablespace_index LONG IN Tablespace_long"};

		assertEquals("getCreateTableWithTableSpace_LUW", expected);

		logger.info("PASSED: Create table with table space executed successfully");

	}
	
	/**
	 * Test the create table add constraint action in admin part
	 * */
	@Test(description = "Test create table add constraint action in admin part")
	public void testCreateTableAddConstraint() throws InterruptedException {

		logger.info("Running Create Table add constraint initially...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"items":[{"statement":"CREATE TABLE \"DB2INST1\".\"TABLE\" ( \"NEW_COLUMN1\" CHAR(5), \"NEW_COLUMN2\" INTEGER )"},
		 * {"statement":"ALTER TABLE \"DB2INST1\".\"TABLE\" ADD CONSTRAINT \"NEW_CONSTRAINT1\" PRIMARY KEY (\"NEW_COLUMN1\" )"},
		 * {"statement":"ALTER TABLE \"DB2INST1\".\"TABLE\" ADD CONSTRAINT \"NEW_CONSTRAINT2\" CHECK NOT ENFORCED"},
		 * {"statement":"ALTER TABLE \"DB2INST1\".\"TABLE\" ADD CONSTRAINT \"NEW_CONSTRAINT3\" UNIQUE"},
		 * {"statement":"ALTER TABLE \"DB2INST1\".\"TABLE\" ADD CONSTRAINT \"NEW_CONSTRAINT4\" FOREIGN KEY (\"NEW_COLUMN1\" ) REFERENCES \"DB2INST1\".\"ACT\" NOT ENFORCED"}]}
		 *   
		 * To retrieve the statement, at first we look over if the length of
		 * items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */
		String expected[] = {
		"CREATE TABLE \"DB2INST1\".\"TABLE\" ( \"NEW_COLUMN1\" CHAR(5), \"NEW_COLUMN2\" INTEGER )",
		"ALTER TABLE \"DB2INST1\".\"TABLE\" ADD CONSTRAINT \"NEW_CONSTRAINT1\" PRIMARY KEY (\"NEW_COLUMN1\" ) NOT ENFORCED",
		"ALTER TABLE \"DB2INST1\".\"TABLE\" ADD CONSTRAINT \"NEW_CONSTRAINT2\" CHECK NOT ENFORCED",
		"ALTER TABLE \"DB2INST1\".\"TABLE\" ADD CONSTRAINT \"NEW_CONSTRAINT3\" UNIQUE NOT ENFORCED",
		"ALTER TABLE \"DB2INST1\".\"TABLE\" ADD CONSTRAINT \"NEW_CONSTRAINT4\" FOREIGN KEY (\"NEW_COLUMN1\" ) REFERENCES \"DB2INST1\".\"ACT\" NOT ENFORCED"};

		assertEquals("getCreateTableAddConstraint_LUW", expected);

		logger.info("PASSED: Create table add constraint executed successfully");

	}
	
	/**
	 * Test the create table add partition action in admin part
	 * */
	@Test(description = "Test create table add partition action in admin part")
	public void testCreateTableAddPartition() throws InterruptedException {

		logger.info("Running Create Table add partition initially...");

		/**
		 * Path the response data, for this case, the response data is:
		 *  
		 *  {"items":[{"statement":"CREATE TABLE \"DB2ADMIN\".\"T1\" ( \"COL1\" CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', \"COL2\" INTEGER WITH DEFAULT 32 ) PARTITION BY RANGE ( C1 ) (, )"},
		 *  {"statement":"CREATE TABLE \"DB2ADMIN\".\"T2\" ( \"C1\" DATE, \"C2\" INTEGER WITH DEFAULT 32 ) PARTITION BY RANGE ( C1 ) ( EVERY 1 MONTH )"}]}
		 *  
		 * To retrieve the statement, at first we look over if the length of
		 * items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */
		String expected[] = {
		"CREATE TABLE \"DB2ADMIN\".\"T1\" ( \"COL1\" CHAR(25) NOT NULL WITH DEFAULT 'aaaaaa', \"COL2\" INTEGER WITH DEFAULT 32 ) PARTITION BY RANGE ( C1 ) (, )",
		"CREATE TABLE \"DB2ADMIN\".\"T2\" ( \"C1\" DATE, \"C2\" INTEGER WITH DEFAULT 32 ) PARTITION BY RANGE ( C1 ) ( EVERY 1 MONTH )"};

		assertEquals("getCreateTableAddPartition_LUW", expected);

		logger.info("PASSED: Create table add partition executed successfully");

	}
	
	/**
	 * @author cyinmei@cn.ibm.com
	 * This case is to create a column organized table and verify DDL
	 * DB2 version must be 10.5 or above
	 * */
	@Test(description = "Test create column organized table in admin part")
	public void testCreateTableColumnOrganized() throws InterruptedException {

		logger.info("Running Create Column Organized Table ...");
		
		//column-organized table is supported in DB2 v10.5 or above, so for v9.7 and v10.1 DBs just return;
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97) || this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_101) )
		{ return; }		

		/**
		 * Paste the response data, for this case, the response data is:
		 *  
		 *  {"items":[{"statement":"CREATE TABLE \"SCH_COLORG\".\"TAB_COLORG\" ( \"COL1\" CHAR(5) ) ORGANIZE BY COLUMN"}]}
		 * */
		String expected[] = {"CREATE TABLE \"SCH_COLORG\".\"TAB_COLORG\" ( \"COL1\" CHAR(5) ) ORGANIZE BY COLUMN"};
		
		assertEquals("getCreateTableColumnOrganized_LUW", expected);

		logger.info("PASSED: Create Column Organized Table executed successfully");

	}
	
	/**
	 * @author cyinmei@cn.ibm.com
	 * This case is to execute DDL from testCreateTableColumnOrganized() method to verify DDL can be deployed successfully in DB server
	 * We will provide CREATE DDL explicitly while it is the same as the output in testCreateTableColumnOrganized() method
	 * Will execute DROP DDL as well to clean env
	 * */
	@Test(description = "Test create column organized table in admin part")
	public void testCreateTableColumnOrganizedExecuteDDL() throws InterruptedException {

		logger.info("Running Create Column Organized Table ...");
		
		//column-organized table is supported in DB2 v10.5 or above, so for v9.7 and v10.1 DBs just return;
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97) || this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_101) )
		{ return; }

		String createStatement = "CREATE TABLE \"SCH_COLORG\".\"TAB_COLORG\" ( \"COL1\" CHAR(5) ) ORGANIZE BY COLUMN";
		
		sqlEditorService.runSQLinJDBC(createStatement, ";", this.dbProfile, true);

		logger.info("PASSED: Create Column Organized table executed successfully");
		
		/*
		 * To clean the env, so drop the table then
		 */
		
        String dropStatement = "DROP TABLE \"SCH_COLORG\".\"TAB_COLORG\"";
		
		sqlEditorService.runSQLinJDBC(dropStatement, ";", this.dbProfile, true);

		logger.info("PASSED: Drop Column Organized table executed successfully");

	}
	
	/**
	 * @author cyinmei@cn.ibm.com
	 * This case is to create a system temporal table and verify DDL
	 * DB2 version must be 10 or above
	 * */
	@Test(description = "Test create system temporal table in admin part")
	public void testCreateTableSYSTEMP() throws InterruptedException {

		logger.info("Running Create system temporal table ...");
		
		//system temporal table is supported in DB2 v10 or above, so for v9.7 just return;
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97))
		{ return; }

		/**
		 * Paste the response data, for this case, the response data is:
		 *  
		 *  {"items":[{"statement":"CREATE TABLE \"SCH_SYSTEMP\".\"TAB_SYSTEMP\" ( \"SYSTEM_START_TIME\" TIMESTAMP(12) NOT NULL GENERATED ALWAYS AS ROW BEGIN, \"SYSTEM_END_TIME\" TIMESTAMP(12) NOT NULL GENERATED ALWAYS AS ROW END, \"TRANS_ID\" TIMESTAMP(12) GENERATED ALWAYS AS TRANSACTION START ID, \"NEW_COLUMN1\" CHAR(5), PERIOD SYSTEM_TIME(\"SYSTEM_START_TIME\",\"SYSTEM_END_TIME\") )"},{"statement":"CREATE TABLE \"SCH_SYSTEMP\".\"TAB_SYSTEMP_HIST\" ( \"SYSTEM_START_TIME\" TIMESTAMP(12) NOT NULL, \"SYSTEM_END_TIME\" TIMESTAMP(12) NOT NULL, \"TRANS_ID\" TIMESTAMP(12), \"NEW_COLUMN1\" CHAR(5) )"},{"statement":"ALTER TABLE \"SCH_SYSTEMP\".\"TAB_SYSTEMP\" ADD VERSIONING USE HISTORY TABLE \"SCH_SYSTEMP\".\"TAB_SYSTEMP_HIST\""}]}
		 * */
		
		String expected[] = {
				"CREATE TABLE \"SCH_SYSTEMP\".\"TAB_SYSTEMP\" ( \"SYSTEM_START_TIME\" TIMESTAMP(12) NOT NULL GENERATED ALWAYS AS ROW BEGIN, \"SYSTEM_END_TIME\" TIMESTAMP(12) NOT NULL GENERATED ALWAYS AS ROW END, \"TRANS_ID\" TIMESTAMP(12) GENERATED ALWAYS AS TRANSACTION START ID, \"NEW_COLUMN1\" CHAR(5), PERIOD SYSTEM_TIME(\"SYSTEM_START_TIME\",\"SYSTEM_END_TIME\") )",
				"CREATE TABLE \"SCH_SYSTEMP\".\"TAB_SYSTEMP_HIST\" ( \"SYSTEM_START_TIME\" TIMESTAMP(12) NOT NULL, \"SYSTEM_END_TIME\" TIMESTAMP(12) NOT NULL, \"TRANS_ID\" TIMESTAMP(12), \"NEW_COLUMN1\" CHAR(5) )",
				"ALTER TABLE \"SCH_SYSTEMP\".\"TAB_SYSTEMP\" ADD VERSIONING USE HISTORY TABLE \"SCH_SYSTEMP\".\"TAB_SYSTEMP_HIST\""};

		assertEquals("getCreateTableSystemTemporal_LUW", expected);

		logger.info("PASSED: Create system temporal table executed successfully");

	}
	
	/**
	 * @author cyinmei@cn.ibm.com
	 * This case is to execute DDL from testCreateTableSYSTEMP() method to verify DDL can be deployed successfully in DB server
	 * Will provide CREATE DDL explicitly while it is the same as the output in testCreateTableSYSTEMP() method
	 * Then will execute DROP DLL to clean env 
	 * */
	@Test(description = "Test execute create system temporal table DDL in admin part")
	public void testCreateTableSYSTEMPExecuteDDL() throws InterruptedException {

		logger.info("Running execute system temporal table DDL ...");
		
		//system temporal table is supported in DB2 v10 or above, so for v9.7 just return;
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97))
		{ return; }

		String createStatement[] = {
				"CREATE TABLE \"SCH_SYSTEMP\".\"TAB_SYSTEMP\" ( \"SYSTEM_START_TIME\" TIMESTAMP(12) NOT NULL GENERATED ALWAYS AS ROW BEGIN, \"SYSTEM_END_TIME\" TIMESTAMP(12) NOT NULL GENERATED ALWAYS AS ROW END, \"TRANS_ID\" TIMESTAMP(12) GENERATED ALWAYS AS TRANSACTION START ID, \"NEW_COLUMN1\" CHAR(5), PERIOD SYSTEM_TIME(\"SYSTEM_START_TIME\",\"SYSTEM_END_TIME\") )",
				"CREATE TABLE \"SCH_SYSTEMP\".\"TAB_SYSTEMP_HIST\" ( \"SYSTEM_START_TIME\" TIMESTAMP(12) NOT NULL, \"SYSTEM_END_TIME\" TIMESTAMP(12) NOT NULL, \"TRANS_ID\" TIMESTAMP(12), \"NEW_COLUMN1\" CHAR(5) )",
				"ALTER TABLE \"SCH_SYSTEMP\".\"TAB_SYSTEMP\" ADD VERSIONING USE HISTORY TABLE \"SCH_SYSTEMP\".\"TAB_SYSTEMP_HIST\""};

		
        String sqlStatement = null;
		
		for(int i=0; i<createStatement.length; i++){
			sqlStatement = createStatement[i];
			
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
		}

		logger.info("PASSED: Create system temporal table DDL executed successfully");
		
		/*
		 * To clean the env, so drop the table 
		 */
        String dropStatement[] = {
        		"ALTER TABLE \"SCH_SYSTEMP\".\"TAB_SYSTEMP\" DROP VERSIONING",
        		"DROP TABLE \"SCH_SYSTEMP\".\"TAB_SYSTEMP_HIST\"",
        		"DROP TABLE \"SCH_SYSTEMP\".\"TAB_SYSTEMP\""};
        for(int i=0; i<dropStatement.length; i++){
			sqlStatement = dropStatement[i];
			
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
		}  

		logger.info("PASSED: Drop system temporal table DDL executed successfully");

	}
	
	/**
	 * @author cyinmei@cn.ibm.com
	 * This case is to create an application temporal table and verify DDL
	 * DB2 version must be 10 or above
	 * */
	@Test(description = "Test create application temporal table in admin part")
	public void testCreateTableAPPTEMP() throws InterruptedException {

		logger.info("Running Create application temporal table ...");
		
		//application temporal table is supported in DB2 v10 or above, so for v9.7 just return;
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97))
		{ return; }

		/**
		 * Paste the response data, for this case, the response data is:
		 *  
		 *{"items":[{"statement":"CREATE TABLE \"SCH_APPTEMP\".\"TAB_APPTEMP\" ( \"APPLICATION_START_TIME\" TIMESTAMP(6) NOT NULL, \"APPLICATION_END_TIME\" TIMESTAMP(6) NOT NULL, \"NEW_COLUMN1\" CHAR(5), PERIOD BUSINESS_TIME(\"APPLICATION_START_TIME\",\"APPLICATION_END_TIME\") )"}]}	 
		 * */
		
		String expected[] = {"CREATE TABLE \"SCH_APPTEMP\".\"TAB_APPTEMP\" ( \"APPLICATION_START_TIME\" TIMESTAMP(6) NOT NULL, \"APPLICATION_END_TIME\" TIMESTAMP(6) NOT NULL, \"NEW_COLUMN1\" CHAR(5), PERIOD BUSINESS_TIME(\"APPLICATION_START_TIME\",\"APPLICATION_END_TIME\") )"};

		assertEquals("getCreateTableApplicationTemporal_LUW", expected);

		logger.info("PASSED: Create application temporal table executed successfully");

	}
	
	/**
	 * @author cyinmei@cn.ibm.com
	 * This case is to execute DDL from testCreateTableAPPTEMP() method to verify DDL can be deployed successfully in DB server
	 * Will provide CREATE DDL explicitly while it is the same as the output in testCreateTableAPPTEMP() method
	 * Then will execute DROP DLL to clean env 
	 * */
	@Test(description = "Test execute create application temporal table DDL in admin part")
	public void testCreateTableAPPTEMPExecuteDDL() throws InterruptedException {

		logger.info("Running execute application temporal table DDL ...");
		
		//application temporal table is supported in DB2 v10 or above, so for v9.7 just return;
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97))
		{ return; }

		String createStatement[] = {"CREATE TABLE \"SCH_APPTEMP\".\"TAB_APPTEMP\" ( \"APPLICATION_START_TIME\" TIMESTAMP(6) NOT NULL, \"APPLICATION_END_TIME\" TIMESTAMP(6) NOT NULL, \"NEW_COLUMN1\" CHAR(5), PERIOD BUSINESS_TIME(\"APPLICATION_START_TIME\",\"APPLICATION_END_TIME\") )"};

		
        String sqlStatement = null;
		
		for(int i=0; i<createStatement.length; i++){
			sqlStatement = createStatement[i];
			
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
		}

		logger.info("PASSED: Create applicaiton temporal table DDL executed successfully");
		
		/*
		 * To clean the env, so drop the table 
		 */
        String dropStatement[] = {"DROP TABLE \"SCH_APPTEMP\".\"TAB_APPTEMP\""};
        		
        for(int i=0; i<dropStatement.length; i++){
			sqlStatement = dropStatement[i];
			
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
		}  

		logger.info("PASSED: Drop application temporal table DDL executed successfully");

	}
	
	/**
	 * @author cyinmei@cn.ibm.com
	 * This case is to create a bitemporal table and verify DDL
	 * DB2 version must be 10 or above
	 * */
	@Test(description = "Test create bitemporal table in admin part")
	public void testCreateTableBITEMP() throws InterruptedException {

		logger.info("Running Create bitemporal table ...");
		
		//bitemporal table is supported in DB2 v10 or above, so for v9.7 just return;
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97))
		{ return; }

		/**
		 * Paste the response data, for this case, the response data is:
		 *  
		 *{"items":[{"statement":"CREATE TABLE \"SCH_BITEMP\".\"TAB_BITEMP\" ( \"SYSTEM_START_TIME\" TIMESTAMP(12) NOT NULL GENERATED ALWAYS AS ROW BEGIN, \"SYSTEM_END_TIME\" TIMESTAMP(12) NOT NULL GENERATED ALWAYS AS ROW END, \"TRANS_ID\" TIMESTAMP(12) GENERATED ALWAYS AS TRANSACTION START ID, \"APPLICATION_START_TIME\" TIMESTAMP(6) NOT NULL, \"APPLICATION_END_TIME\" TIMESTAMP(6) NOT NULL, \"NEW_COLUMN1\" CHAR(5), PERIOD BUSINESS_TIME(\"APPLICATION_START_TIME\",\"APPLICATION_END_TIME\"), PERIOD SYSTEM_TIME(\"SYSTEM_START_TIME\",\"SYSTEM_END_TIME\") )"},{"statement":"CREATE TABLE \"SCH_BITEMP\".\"TAB_BITEMP_HIST\" ( \"SYSTEM_START_TIME\" TIMESTAMP(12) NOT NULL, \"SYSTEM_END_TIME\" TIMESTAMP(12) NOT NULL, \"TRANS_ID\" TIMESTAMP(12), \"APPLICATION_START_TIME\" TIMESTAMP(6) NOT NULL, \"APPLICATION_END_TIME\" TIMESTAMP(6) NOT NULL, \"NEW_COLUMN1\" CHAR(5) )"},{"statement":"ALTER TABLE \"SCH_BITEMP\".\"TAB_BITEMP\" ADD VERSIONING USE HISTORY TABLE \"SCH_BITEMP\".\"TAB_BITEMP_HIST\""}]}
		 * */
		
		String expected[] = {
				"CREATE TABLE \"SCH_BITEMP\".\"TAB_BITEMP\" ( \"SYSTEM_START_TIME\" TIMESTAMP(12) NOT NULL GENERATED ALWAYS AS ROW BEGIN, \"SYSTEM_END_TIME\" TIMESTAMP(12) NOT NULL GENERATED ALWAYS AS ROW END, \"TRANS_ID\" TIMESTAMP(12) GENERATED ALWAYS AS TRANSACTION START ID, \"APPLICATION_START_TIME\" TIMESTAMP(6) NOT NULL, \"APPLICATION_END_TIME\" TIMESTAMP(6) NOT NULL, \"NEW_COLUMN1\" CHAR(5), PERIOD BUSINESS_TIME(\"APPLICATION_START_TIME\",\"APPLICATION_END_TIME\"), PERIOD SYSTEM_TIME(\"SYSTEM_START_TIME\",\"SYSTEM_END_TIME\") )",
				"CREATE TABLE \"SCH_BITEMP\".\"TAB_BITEMP_HIST\" ( \"SYSTEM_START_TIME\" TIMESTAMP(12) NOT NULL, \"SYSTEM_END_TIME\" TIMESTAMP(12) NOT NULL, \"TRANS_ID\" TIMESTAMP(12), \"APPLICATION_START_TIME\" TIMESTAMP(6) NOT NULL, \"APPLICATION_END_TIME\" TIMESTAMP(6) NOT NULL, \"NEW_COLUMN1\" CHAR(5) )",
				"ALTER TABLE \"SCH_BITEMP\".\"TAB_BITEMP\" ADD VERSIONING USE HISTORY TABLE \"SCH_BITEMP\".\"TAB_BITEMP_HIST\""};

		assertEquals("getCreateTableBiTemporal_LUW", expected);

		logger.info("PASSED: Create bitemporal table executed successfully");

	}
	
	/**
	 * @author cyinmei@cn.ibm.com
	 * This case is to execute DDL from testCreateTableBITEMP() method to verify DDL can be deployed successfully in DB server
	 * Will provide CREATE DDL explicitly while it is the same as the output in testCreateTableBITEMP() method
	 * Then will execute DROP DLL to clean env 
	 * */
	@Test(description = "Test execute create bitemporal table DDL in admin part")
	public void testCreateTableBITEMPExecuteDDL() throws InterruptedException {

		logger.info("Running execute bitemporal table DDL ...");
		
		//bitemporal table is supported in DB2 v10 or above, so for v9.7 just return;
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97))
		{ return; }

		String createStatement[] = {
				"CREATE TABLE \"SCH_BITEMP\".\"TAB_BITEMP\" ( \"SYSTEM_START_TIME\" TIMESTAMP(12) NOT NULL GENERATED ALWAYS AS ROW BEGIN, \"SYSTEM_END_TIME\" TIMESTAMP(12) NOT NULL GENERATED ALWAYS AS ROW END, \"TRANS_ID\" TIMESTAMP(12) GENERATED ALWAYS AS TRANSACTION START ID, \"APPLICATION_START_TIME\" TIMESTAMP(6) NOT NULL, \"APPLICATION_END_TIME\" TIMESTAMP(6) NOT NULL, \"NEW_COLUMN1\" CHAR(5), PERIOD BUSINESS_TIME(\"APPLICATION_START_TIME\",\"APPLICATION_END_TIME\"), PERIOD SYSTEM_TIME(\"SYSTEM_START_TIME\",\"SYSTEM_END_TIME\") )",
				"CREATE TABLE \"SCH_BITEMP\".\"TAB_BITEMP_HIST\" ( \"SYSTEM_START_TIME\" TIMESTAMP(12) NOT NULL, \"SYSTEM_END_TIME\" TIMESTAMP(12) NOT NULL, \"TRANS_ID\" TIMESTAMP(12), \"APPLICATION_START_TIME\" TIMESTAMP(6) NOT NULL, \"APPLICATION_END_TIME\" TIMESTAMP(6) NOT NULL, \"NEW_COLUMN1\" CHAR(5) )",
				"ALTER TABLE \"SCH_BITEMP\".\"TAB_BITEMP\" ADD VERSIONING USE HISTORY TABLE \"SCH_BITEMP\".\"TAB_BITEMP_HIST\""};

        String sqlStatement = null;
		
		for(int i=0; i<createStatement.length; i++){
			sqlStatement = createStatement[i];
			
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
		}

		logger.info("PASSED: Create bitemporal table DDL executed successfully");
		
		/*
		 * To clean the env, so drop the table 
		 */
		String dropStatement[] = {
        		"ALTER TABLE \"SCH_BITEMP\".\"TAB_BITEMP\" DROP VERSIONING",
        		"DROP TABLE \"SCH_BITEMP\".\"TAB_BITEMP_HIST\"",
        		"DROP TABLE \"SCH_BITEMP\".\"TAB_BITEMP\""};
        		
        for(int i=0; i<dropStatement.length; i++){
			sqlStatement = dropStatement[i];
			
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
		}  

		logger.info("PASSED: Drop bitemporal table DDL executed successfully");

	}
	
	/**
	 * @author cyinmei@cn.ibm.com
	 * This case is to create a table with distribution key and verify DDL
	 * 
	 * */
	@Test(description = "Test create table with distribution key in admin part")
	public void testCreateTableAddDistrubutionKey() throws InterruptedException {

		logger.info("Running Create table with distribution key ...");

		/**
		 * Paste the response data, for this case, the response data is:
		 *  
		 *{"items":[{"statement":"CREATE TABLE \"SCH_DISKEY\".\"TAB_DISKEY\" ( \"NEW_COLUMN1\" CHAR(5) ) DISTRIBUTE BY HASH (\"NEW_COLUMN1\")"}]}
		 * */
		
		String expected[] = {"CREATE TABLE \"SCH_DISKEY\".\"TAB_DISKEY\" ( \"NEW_COLUMN1\" CHAR(5) ) DISTRIBUTE BY HASH (\"NEW_COLUMN1\")"};

		assertEquals("getCreateTableAddDistributionKey_LUW", expected);

		logger.info("PASSED: Create table with distribution key executed successfully");

	}
	
	/**
	 * @author cyinmei@cn.ibm.com
	 * This case is to execute DDL from testCreateTableAddDistributionKey() method to verify DDL can be deployed successfully in DB server
	 * Will provide CREATE DDL explicitly while it is the same as the output in testCreateTableAddDistributionKey() method
	 * Then will execute DROP DLL to clean env 
	 * */
	@Test(description = "Test execute create table with distribution key DDL in admin part")
	public void testCreateTableAddDistributionKeyExecuteDDL() throws InterruptedException {

		logger.info("Running execute table with distribution key DDL ...");

		String createStatement[] = {"CREATE TABLE \"SCH_DISKEY\".\"TAB_DISKEY\" ( \"NEW_COLUMN1\" CHAR(5) ) DISTRIBUTE BY HASH (\"NEW_COLUMN1\")"};

        String sqlStatement = null;
		
		for(int i=0; i<createStatement.length; i++){
			sqlStatement = createStatement[i];
			
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
		}

		logger.info("PASSED: Create bitemporal table DDL executed successfully");
		
		/*
		 * To clean the env, so drop the table 
		 */
		String dropStatement[] = {"DROP TABLE \"SCH_DISKEY\".\"TAB_DISKEY\""};
        		
        for(int i=0; i<dropStatement.length; i++){
			sqlStatement = dropStatement[i];
			
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
		}  

		logger.info("PASSED: Drop bitemporal table DDL executed successfully");

	}
	
	
	/**
	 * drop test
	 */
	
	/**
	 * Test the drop table action in admin part
	 * */
	@Test(description = "Test drop table action in admin part")
	public void testDropTable() throws InterruptedException {

		logger.info("Running drop table initially...");

		/**
		 * Path the response data, for this case, the response data is:
		 *  
		 * {"items":[{"statement":"DROP TABLE \"DB2ADMIN\".\"T1\""}]}
		 * 
		 * To retrieve the statement, at first we look over if the length of
		 * items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */
		String expected[] = {"DROP TABLE \"DB2ADMIN\".\"T1\""};

		assertEquals("getDropTable_LUW", expected);

		logger.info("PASSED: drop table executed successfully");

	}
	
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test create unique index DDL
	 * @throws InterruptedException
	 */
	@Test(description = "Test  create unique Index in admin part")
	public void testCreateUniqueIndex() throws InterruptedException {
		logger.info("Running Create Unique Index DDL test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CREATE INDEX \"DB2INST1\".\"IND\" ON \"DB2INST1\".\"TABLE0\" (\"FIELD1\" ASC)"}]}
		 **/

		String expected[] = {
				"CREATE UNIQUE INDEX \"SCHEMA4INDEX\".\"NEW_INDEX1\" ON \"SCHEMA4INDEX\".\"TABLE4INDEX2\" (\"FIELD1\" ASC)"
			};
		assertEquals("getCreateIndex_LUW", expected);

		logger.info("PASSED: create Index DDL is correct");
		
		/**
		 * Verify the DDL execute successfully
		 */
		String sqlStatement = null;
		
		for(int i=0; i<expected.length; i++){
			sqlStatement = expected[i];
			
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
		}
		
		logger.info("PASSED: Create and Alter index DDL executed successfully");
		
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test Create Alias DDL is correct or not
	 */
	@Test(description = "Test the create Alias in admin part")
	public void testCreateAlias() throws InterruptedException {
		logger.info("Running Create Alias DDL test...");
		/**
		 * Response data
		 * {"items":[{
		 * "statement":"CREATE ALIAS \"SCHEMA4ALIAS\".\"ALIASTEST\" FOR \"DB2INST1\".\"TABLE4ALIAS\""}]}
		 */
		String expected[] = {
				"CREATE ALIAS \"SCHEMA4ALIAS\".\"ALIASTEST\" FOR \"SCHEMA4ALIAS\".\"TABLE4ALIAS\""};
		
		assertEquals("getCreateAlias_LUW", expected);
		
		logger.info("PASSED: create Schema DDL is correct");
	
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test Veiw data for  Alias ALIASTEST1
	 */
		@Test(description="Test view data for Alias")
		public void testViewAliasData()throws InterruptedException{
			logger.info("Running View Alias data test...");
			/**
			 * {"cells":[{
			 * "field":"FIELD1","alwaysShown":"true","datatype":"number","applyTooltip":true,"dataType":"number","name":"FIELD1 [INTEGER]","style":"text-align:right","internalDataType":"INTEGER"},
			 * {"field":"FIELD2","alwaysShown":"true","datatype":"number","applyTooltip":true,"dataType":"number","name":"FIELD2 [INTEGER]","style":"text-align:right","internalDataType":"INTEGER"}],
			 * "exceed":false,
			 * "items":[{"FIELD2":"15","FIELD1":"12"}]}
			 */
			String query = alterService.getJSONData("getViewAliasData_LUW");
			JSONObject resObj =alterService.callAlterObjectService(AdminAlterObjectService.BROWSEDATA_COMMAND,query, dbProfile);
			//Get the response Data
			JSONObject responseData =(JSONObject) resObj.get("ResponseData");
			String field2_value = JsonPath.read( responseData, "$.items[0].FIELD2" );
			String field1_value = JsonPath.read( responseData, "$.items[0].FIELD1" );
			Assert.assertEquals(field2_value, "15");
			Assert.assertEquals(field1_value, "12");
			
			logger.info("Veiw Alias data test done and all the data are correct.");
		}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test Execute Create Alias DDL
	 */
	@Test(description="Test Execute Create Alias DDL")
	public void testCreateAliasExecuteDDL(){
		logger.info("Running Create Alias DDL test...");
		
		String ddl = "CREATE ALIAS \"SCHEMA4ALIAS\".\"ALIASTEST\" FOR \"SCHEMA4ALIAS\".\"TABLE4ALIAS\"";
		
		sqlEditorService.runSQLinJDBC(ddl, ";", this.dbProfile,true);
		
		logger.info("PASSED: execute create Alias ddl successfully");
	
		
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test Execute Drop Alias DDL
	 */
	@Test(description="Test Execute Create Alias DDL",dependsOnMethods={"testCreateAliasExecuteDDL"})
	public void testDropAliasExecuteDDL(){
		logger.info("Running Create Alias DDL test...");
		
		String ddl = "DROP ALIAS \"SCHEMA4ALIAS\".\"ALIASTEST\"";
		
		sqlEditorService.runSQLinJDBC(ddl, ";", this.dbProfile,true);
		
		logger.info("PASSED: execute DROP Alias ddl successfully");
	
	}
	
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test Create the System Maintained MQTs  DDL is correct or not
	 */
	@Test(description = "Test the create Schema in admin part")
	public void testCreateSchema() throws InterruptedException {
		logger.info("Running Create Schema DDL test...");
		/**
		 * Response data
		 * {"items":[{"statement":"CREATE SCHEMA \"schema4schema\""}]}
		 */
		String expected[] = {
				"CREATE SCHEMA \"SCHEMA4SCHEMA\""};
		
		assertEquals("getCreateSchema_LUW", expected);
		
		logger.info("PASSED: create Schema DDL is correct");
	
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test Execute create schema DDL 
	 */
	@Test(description = "Test the create Schema in admin part")
	public void testCreateSchemaExecuteDDL() throws InterruptedException {
		logger.info("Running Execute Create Schema DDL test...");
		
		String ddl = "CREATE SCHEMA \"SCHEMA4SCHEMA\"";
		
		sqlEditorService.runSQLinJDBC(ddl, ";", this.dbProfile,true);
		
		logger.info("PASSED: execute create schema ddl successfully");
	
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test the alter Schema DDL is correct or not
	 * */
	@Test(description = "Test the alter Schema action in admin part")
	public void testAlterSchema() throws InterruptedException {

		logger.info("Running Alter Schema...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"items":[{
		 * "statement":"ALTER SCHEMA \"SCHEMA_INDEX\" DATA CAPTURE CHANGES"}]}
		 * 
		 * */
		
		String expected[] = {"ALTER SCHEMA \"SCHEMA4SCHEMA1\" DATA CAPTURE CHANGES"};

		assertEquals("getAlterSchema_LUW", expected);

		logger.info("PASSED: Alter Schema DDL is correct");

	}
	/**@author ybjwang@cn.ibm.com
	 * Test the execute alter schema DDL in admin part
	 * */
	@Test(description = "Test the alter Schema action in admin part")
	public void testAlterSchemaExecuteDDL() throws InterruptedException {
		//Alter Schema is not shown on UI for DB2v9.7
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97)){
								return;
			}
		
		logger.info("Executing Alter Schema DDL...");
		
		String ddl ="ALTER SCHEMA \"SCHEMA4SCHEMA1\" DATA CAPTURE CHANGES";

		sqlEditorService.runSQLinJDBC(ddl, ";", this.dbProfile,true);
		
		logger.info("PASSED: Alter Schema DDL run successfully");

	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test Execute Drop schema 
	 */
	@Test(description = "Test the create Schema in admin part",dependsOnMethods={"testCreateSchemaExecuteDDL"},alwaysRun=true)
	public void testDropSchemaExecuteDDL() throws InterruptedException {
		logger.info("Running Execute Create Schema DDL test...");
		
		String ddl = "DROP SCHEMA \"SCHEMA4SCHEMA\" RESTRICT";
		
		sqlEditorService.runSQLinJDBC(ddl, ";", this.dbProfile,true);
		
		logger.info("PASSED: execute Drop Schema  successfully");
	
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test Create the System Maintained MQTs  DDL is correct or not
	 */
	@Test(description = "Test the Create Storage Group in admin part")
	public void testCreateStorageGroup() throws InterruptedException {
		//Storage Group is not shown on UI part for DB2V9.7
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97)){
													return;
			}
		logger.info("Running Create Storage Group DDL test...");
		/**
		 * Response data
		 * {"items":[{"statement":"CREATE STOGROUP \"NEWSTGRP\" ON '\/tmp'"}]}
		 */
		String expected[] = {
				"CREATE STOGROUP \"NEWSTGRP4STG\" ON \'/tmp\'"};
		
		assertEquals("getCreateStorageGroup_LUW", expected);
		
		logger.info("PASSED: Create Storage Group DDL is correct");
	
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test Create the System Maintained MQTs  DDL is correct or not
	 */
	@Test(description = "Test the Create Storage Group in admin part")
	public void testCreateStorageGroupExecuteDDL() throws InterruptedException {
		//Storage Group is not shown on UI part for DB2V9.7
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97)){
													return;
			}
		logger.info("Executing Create Storage Group DDL test...");
		/**
		 * Response data
		 * {"items":[{"statement":"CREATE STOGROUP \"NEWSTGRP\" ON '\/tmp'"}]}
		 */
		String ddl= "CREATE STOGROUP \"NEWSTGRP4STG\" ON \'/tmp\'";
		
		sqlEditorService.runSQLinJDBC(ddl, ";", this.dbProfile,true);
		
		logger.info("PASSED: Execute Storage Group DDL successfully");
	
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test Create the System Maintained MQTs  DDL is correct or not
	 */
	@Test(description = "Test the Alter Storage Group in admin part")
	public void testAlterStorageGroup() throws InterruptedException {
		//Storage Group is not shown on UI part for DB2V9.7
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97)){
													return;
			}
		logger.info("Running Alter Storage Group DDL test...");
		/**
		 * Response data
		 * {"items":[{
		 * "statement":"ALTER STOGROUP \"NEWSTGRP4STG\" OVERHEAD 7.125 DEVICE READ RATE 150 DATA TAG 1"}]}
		 */
		String expected[] = {
				"ALTER STOGROUP \"NEWSTGRP4STG1\" OVERHEAD 7.125 DEVICE READ RATE 150 DATA TAG 1"};
		
		assertEquals("getAlterStorageGroup_LUW", expected);
		
		logger.info("PASSED: Alter Storage Group DDL is correct");
	
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test Create the System Maintained MQTs  DDL is correct or not
	 */
	@Test(description = "Test the Execute Storage Group DDL")
	public void testAlterStorageGroupExecuteDDL() throws InterruptedException {
		//Storage Group is not shown on UI part for DB2V9.7
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97)){
													return;
			}
		logger.info("Running Alter Storage Group DDL ...");
		/**
		 * Response data
		 * {"items":[{
		 * "statement":"ALTER STOGROUP \"NEWSTGRP4STG\" OVERHEAD 7.125 DEVICE READ RATE 150 DATA TAG 1"}]}
		 */
		String ddl = "ALTER STOGROUP \"NEWSTGRP4STG1\" OVERHEAD 7.125 DEVICE READ RATE 150 DATA TAG 1";
		
		sqlEditorService.runSQLinJDBC(ddl, ";", this.dbProfile,true);	
		
		logger.info("PASSED: Execute Alter Storage Group DDL successfully");
	
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test Create the System Maintained MQTs  DDL is correct or not
	 */
	@Test(description = "Test the Drop Storage Group DDL",dependsOnMethods={"testCreateStorageGroupExecuteDDL"},alwaysRun=true)
	public void testdropStorageGroupExecuteDDL() throws InterruptedException {
		//Storage Group is not shown on UI part for DB2V9.7
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97)){
													return;
			}
		logger.info("Running Drop Storage Group DDL ...");
		/**
		 * Response data
		 * {"items":[{
		 * "statement":"ALTER STOGROUP \"NEWSTGRP4STG\" OVERHEAD 7.125 DEVICE READ RATE 150 DATA TAG 1"}]}
		 */
		String ddl = "DROP STOGROUP \"NEWSTGRP4STG\" RESTRICT";
		
		sqlEditorService.runSQLinJDBC(ddl, ";", this.dbProfile,true);	
		
		logger.info("PASSED: Execute Drop Storage Group DDL successfully");
	
	}
	
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test create a new User Maintained MQTs DDL is correct or not
	 */
	@Test(description = "Test the create MQTs in admin part")
	public void testCreateUserMQTs() throws InterruptedException {
		logger.info("Running User Maintained MQT DDL test...");
		/**
		 * Response data
		 * {"items":[
		 * {"statement":"CREATE TABLE \"SCHEMA4MQT\".\"MQT3\" AS ( SELECT FIELD1 FROM SCHEMA4MQT.TABLE4MQT ) DATA INITIALLY DEFERRED REFRESH DEFERRED MAINTAINED BY SYSTEM"},
		 * {"statement":"SET INTEGRITY FOR \"SCHEMA4MQT\".\"MQT3\" IMMEDIATE CHECKED FULL ACCESS"}]}
		 */
		String expected[] = {
				"CREATE TABLE \"SCHEMA4MQT\".\"MQTTest4User\" AS ( SELECT FIELD1 FROM SCHEMA4MQT.TABLE4MQT ) DATA INITIALLY DEFERRED REFRESH DEFERRED MAINTAINED BY USER",
				"SET INTEGRITY FOR \"SCHEMA4MQT\".\"MQTTest4User\" ALL IMMEDIATE UNCHECKED"};
		
		assertEquals("getCreateUserMQTs_LUW", expected);
		
		logger.info("PASSED: create MQTs DDL is correct");
	
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test Create the System Maintained MQTs  DDL is correct or not
	 */
	@Test(description = "Test the create MQTs in admin part")
	public void testCreateSysMQTs() throws InterruptedException {
		logger.info("Running SYSTEM Maintained MQT DDL test...");
		/**
		 * Response data
		 * {"items":[
		 * {"statement":"CREATE TABLE \"SCHEMA4MQT\".\"MQT3\" AS ( SELECT FIELD1 FROM SCHEMA4MQT.TABLE4MQT ) DATA INITIALLY DEFERRED REFRESH DEFERRED MAINTAINED BY SYSTEM"},
		 * {"statement":"SET INTEGRITY FOR \"SCHEMA4MQT\".\"MQT3\" IMMEDIATE CHECKED FULL ACCESS"}]}
		 */
		String expected[] = {
				"CREATE TABLE \"SCHEMA4MQT\".\"MQTTest4System\" AS ( SELECT FIELD1 FROM SCHEMA4MQT.TABLE4MQT ) DATA INITIALLY DEFERRED REFRESH DEFERRED MAINTAINED BY SYSTEM",
				"SET INTEGRITY FOR \"SCHEMA4MQT\".\"MQTTest4System\" IMMEDIATE CHECKED FULL ACCESS"};
		
		assertEquals("getCreateSysMQTs_LUW", expected);
		
		logger.info("PASSED: create MQTs DDL is correct");
	
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test Create the System Maintained MQTs  DDL can be executed successfully
	 */
	@Test
	public void testCreateSysMQTsExcuteDDL() throws InterruptedException {
		logger.info("Excute Create MQTs DDL test...");
		
		String expected[] = {
				"CREATE TABLE \"SCHEMA4MQT\".\"MQTTest4System\" AS ( SELECT FIELD1 FROM SCHEMA4MQT.TABLE4MQT ) DATA INITIALLY DEFERRED REFRESH DEFERRED MAINTAINED BY SYSTEM",
				"SET INTEGRITY FOR \"SCHEMA4MQT\".\"MQTTest4System\" IMMEDIATE CHECKED FULL ACCESS"};
		
		String sqlStatement = null;
		
		for(int i=0; i<expected.length; i++){
			sqlStatement = expected[i];
			
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
		}
		
		logger.info("PASSED: Create SYSTEM Maintained MQTs DDL executed successfully");
	
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test create a new User Maintained MQTs DDL can be executed successfully nor not.
	 */
	@Test(description="Excute Create User Maintained MQTs DDL test..")
	public void testCreateUserMQTsExcuteDDL() throws InterruptedException {
		logger.info("Excute Create User Maintained MQTs DDL test...");
		
		String expected[] = {
				"CREATE TABLE \"SCHEMA4MQT\".\"MQTTest4User\" AS ( SELECT FIELD1 FROM SCHEMA4MQT.TABLE4MQT ) DATA INITIALLY DEFERRED REFRESH DEFERRED MAINTAINED BY SYSTEM",
				"SET INTEGRITY FOR \"SCHEMA4MQT\".\"MQTTest4User\" IMMEDIATE CHECKED FULL ACCESS"};
		
		String sqlStatement = null;
		
		for(int i=0; i<expected.length; i++){
			sqlStatement = expected[i];
			
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
		}
		
		logger.info("PASSED: Create User Maintained MQTs DDL executed successfully");
	
	}
	
	/**
	 * Author:Wang Yun
	 * Test the drop MQTs DDL is correct in Admin part
	 */
	@Test(description = "Test the drop MQT action in admin part")
	public void testDropMQT() throws InterruptedException{
		
		logger.info("Running Drop MQT...");
		
		String expected[] = {"DROP TABLE \"SCHEMA4MQT\".\"MQTTest4System\"","DROP TABLE \"SCHEMA4MQT\".\"MQTTest4User\""};
		
		assertEquals("getDropMQT_LUW", expected);

		logger.info("PASSED: Alter drop MQT executed successfully");
	}
	
	/**
	 * Author:Wang Yun
	 * Test the drop MQTs DDL execute successfully or not in Admin part
	 */
	@Test(description = "Test the drop MQT action in admin part",dependsOnMethods = {"testCreateSysMQTsExcuteDDL","testCreateUserMQTsExcuteDDL"},alwaysRun=true)
	public void testDropMQTExecuteDDL() throws InterruptedException{
		
		logger.info("Executeing Drop MQT DDL...");
		
		String expected[] = {"DROP TABLE \"SCHEMA4MQT\".\"MQTTest4System\"","DROP TABLE \"SCHEMA4MQT\".\"MQTTest4User\""};
		
		String sqlStatements = null;
		
		for(int i=0; i<expected.length; i++){
			sqlStatements = expected[i];
			
			sqlEditorService.runSQLinJDBC(sqlStatements, ";", this.dbProfile,true);
		}
	}
	
	
	@Test(description = "Test the table batch privileges in admin part")
	public void testTableBatchPrivilegeDefinition() throws InterruptedException {
		logger.info("Running table batch privileges definition test...");
		String query = alterService.getJSONData("getTableBatchPrivilege_LUW");

		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.PRIVILEGE_MODEL_DEFINITION, query, dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "actions" ) );
		JSONArray actions = responseData.getJSONArray( "actions" );
		Assert.assertNotNull( actions );
		
		String[] acts = {"CONTROL", "ALTER", "DELETE", "INDEX", "INSERT", "REFERENCES", "SELECT", "UPDATE"};
		Assert.assertEquals(actions.size(), acts.length);
		for(int i = 0; i < actions.size(); i++){
			Assert.assertEquals(actions.get(i).toString(), acts[i]);
		}

		logger.info("PASSED: table batch privileges definition executed successfully");
	}
	
	@Test(description = "Test the view batch privileges in admin part")
	public void testViewBatchPrivilegeDefinition() throws InterruptedException {
		logger.info("Running view batch privileges definition test...");
		String query = alterService.getJSONData("getViewBatchPrivilege_LUW");

		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.PRIVILEGE_MODEL_DEFINITION, query, dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "actions" ) );
		JSONArray actions = responseData.getJSONArray( "actions" );
		Assert.assertNotNull( actions );
		
		String[] acts = {"CONTROL", "DELETE", "INSERT", "SELECT", "UPDATE"};
		Assert.assertEquals(actions.size(), acts.length);
		for(int i = 0; i < actions.size(); i++){
			Assert.assertEquals(actions.get(i).toString(), acts[i]);
		}

		logger.info("PASSED: view batch privileges definition executed successfully");
	}
	
	@Test(description = "Test the view batch privileges in admin part")
	public void testIndexBatchPrivilegeDefinition() throws InterruptedException {
		logger.info("Running index batch privileges definition test...");
		String query = alterService.getJSONData("getIndexBatchPrivilege_LUW");

		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.PRIVILEGE_MODEL_DEFINITION, query, dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "actions" ) );
		JSONArray actions = responseData.getJSONArray( "actions" );
		Assert.assertNotNull( actions );
		
		String[] acts = {"CONTROL"};
		Assert.assertEquals(actions.size(), acts.length);
		for(int i = 0; i < actions.size(); i++){
			Assert.assertEquals(actions.get(i).toString(), acts[i]);
		}

		logger.info("PASSED: index batch privileges definition executed successfully");
	}
	
	@Test(description = "Test the MQT batch privileges in admin part")
	public void testMQTBatchPrivilegeDefinition() throws InterruptedException {
		logger.info("Running MQT batch privileges definition test...");
		String query = alterService.getJSONData("getMQTBatchPrivilege_LUW");

		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.PRIVILEGE_MODEL_DEFINITION, query, dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "actions" ) );
		JSONArray actions = responseData.getJSONArray( "actions" );
		Assert.assertNotNull( actions );
		
		String[] acts = {"CONTROL", "ALTER", "DELETE", "INDEX", "INSERT", "REFERENCES", "SELECT", "UPDATE"};
		Assert.assertEquals(actions.size(), acts.length);
		for(int i = 0; i < actions.size(); i++){
			Assert.assertEquals(actions.get(i).toString(), acts[i]);
		}

		logger.info("PASSED: mqt batch privileges definition executed successfully");
	}
	
	@Test(description = "Test the Schema batch privileges in admin part")
	public void testSchemaBatchPrivilegeDefinition() throws InterruptedException {
		logger.info("Running Schema batch privileges definition test...");
		String query = alterService.getJSONData("getSchemaBatchPrivilege_LUW");

		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.PRIVILEGE_MODEL_DEFINITION, query, dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "actions" ) );
		JSONArray actions = responseData.getJSONArray( "actions" );
		Assert.assertNotNull( actions );
		
		String[] acts = {"ALTERIN", "CREATEIN", "DROPIN","SCHEMAADM","ACCESSCTRL","DATAACCESS","LOAD","SELECTIN","UPDATEIN","INSERTIN","DELETEIN","EXECUTEIN"};
		Assert.assertEquals(actions.size(), acts.length);
		for(int i = 0; i < actions.size(); i++){
			Assert.assertEquals(actions.get(i).toString(), acts[i]);
		}

		logger.info("PASSED: Schema batch privileges definition executed successfully");
	}
	
	@Test(description = "Test the Table Spaces batch privileges in admin part")
	public void testTableSpacesBatchPrivilegeDefinition() throws InterruptedException {
		logger.info("Running Table Spaces batch privileges definition test...");
		String query = alterService.getJSONData("getTableSpacesBatchPrivilege_LUW");

		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.PRIVILEGE_MODEL_DEFINITION, query, dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "actions" ) );
		JSONArray actions = responseData.getJSONArray( "actions" );
		Assert.assertNotNull( actions );
		
		String[] acts = {"USE"};
		Assert.assertEquals(actions.size(), acts.length);
		for(int i = 0; i < actions.size(); i++){
			Assert.assertEquals(actions.get(i).toString(), acts[i]);
		}

		logger.info("PASSED: Table Spaces batch privileges definition executed successfully");
	}
	
	@Test(description = "Test the Sequence batch privileges in admin part")
	public void testSequenceBatchPrivilegeDefinition() throws InterruptedException {
		logger.info("Running Sequence batch privileges definition test...");
		String query = alterService.getJSONData("getSequenceBatchPrivilege_LUW");

		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.PRIVILEGE_MODEL_DEFINITION, query, dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "actions" ) );
		JSONArray actions = responseData.getJSONArray( "actions" );
		Assert.assertNotNull( actions );
		
		String[] acts = {"ALTER", "USAGE"};
		Assert.assertEquals(actions.size(), acts.length);
		for(int i = 0; i < actions.size(); i++){
			Assert.assertEquals(actions.get(i).toString(), acts[i]);
		}

		logger.info("PASSED: Sequence batch privileges definition executed successfully");
	}
	
	
	//@Test(description = "Test the batch privileges ddl in admin part")
	public void testBatchPrivilegeDDLDefinition() throws InterruptedException {
		logger.info("Running batch privileges ddl test...");
		String query = alterService.getJSONData("getBatchPrivilegeDDL_LUW");

		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.PRIVILEGE_MODEL_COMMAND, query, dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "items" ) );
		JSONArray items = responseData.getJSONArray( "items" );
		Assert.assertNotNull( items );
		
		String[] acts = {"GRANT INDEX, REFERENCES ON TABLE \"db2inst1\".\"abc\" TO USER DB2INST1",
				"GRANT INDEX, REFERENCES ON TABLE \"DB2INST1\".\"NICKNAME_TEST\" TO USER DB2INST1"};
		Assert.assertEquals(items.size(), acts.length);
		String ddl = "";
		for(int i = 0; i < items.size(); i++){
			ddl = JsonPath.read( responseData, "$.items[" + i + "].statement" );
			Assert.assertEquals(ddl, acts[i]);
		}

		logger.info("PASSED: batch privileges ddl executed successfully");
	}
	
	/**
	 * @author cyinmei@cn.ibm.com
	 * This case is to view table's dataL
	 * UI operation, select one table, then click Data to view data in the table
	 * */
	
	@Test(description = "Test view table data in admin part")
	public void testViewTableData() throws InterruptedException {
		
		logger.info("Running view table data...");
		
		String query = alterService.getJSONData("getViewTableData_LUW");

		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.BROWSEDATA_COMMAND, query, dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "items" ) );
		JSONArray items = responseData.getJSONArray( "items" );
		Assert.assertNotNull( items );
		
		//items: [{"COL1":"1"},{"COL1":"2"},{"COL1":"3"}]
		String[] dataResult = {"1","2","3"};
		String dataResponseResult = "";
		for(int i = 0; i < items.size(); i++){
			dataResponseResult = JsonPath.read( responseData, "$.items[" + i + "].COL1" );
			Assert.assertEquals(dataResponseResult, dataResult[i]);
		}

		logger.info("PASSED: view table data executed successfully");
	}
	
	private void testCreateSequence_precondition() {
		// TODO Auto-generated method stub
		dropSequenceEnv(false);
		createSequence();
	}
	private void testSequence_clean() {
		// TODO Auto-generated method stub
		 dropSequenceEnv(true);
	}

	private void dropSequenceEnv(boolean real) {
		// TODO Auto-generated method stub
		List<String> dropList = new ArrayList<String>();
		dropList.add("DROP SEQUENCE \"SEQTEST\".\"SEQ2\"");
		dropList.add("DROP SEQUENCE \"SEQTEST\".\"SEQ3\"");
		dropList.add("DROP SCHEMA SEQTEST RESTRICT");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile,real);
	}
	private void createSequence() {
		// TODO Auto-generated method stub
		String sqlStatement = "CREATE SCHEMA SEQTEST;"+
				"CREATE SEQUENCE \"SEQTEST\".\"SEQ2\" START WITH 1 CACHE 24;"+
				"CREATE SEQUENCE \"SEQTEST\".\"SEQ3\" START WITH 1 CACHE 24;";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile, true);
	}
	/**@author ylwh@cn.ibm.com
	 * Test create sequence function
	 * */
	@Test (description = "Test create sequence function")
	public void testCreateSequence() throws InterruptedException
	{
		logger.info( "Test create sequence ..." );
		/**
		 * Response data
		 * {"items": [{"statement": "CREATE SEQUENCE \"SEQTEST\".\"SEQ1\" START WITH 1 CACHE 24"}]}
		 */
		
		String expected[] = {
				"CREATE SEQUENCE \"SEQTEST\".\"SEQ1\" START WITH 1 CACHE 24"};
		assertEquals("GetLUWEditSeq", expected);

		logger.info("PASSED: create sequence executed successfully");
		

	}
	@Test (description = "Test execute create sequence DDL")
	public void testCreateSequenceExecuteDDL() throws InterruptedException
	{
		logger.info( "Test execute create sequence DDL..." );
		String sql ="CREATE SEQUENCE \"SEQTEST\".\"SEQ1\" START WITH 1 CACHE 24;"+
					"DROP SEQUENCE \"SEQTEST\".\"SEQ1\";";
		sqlEditorService.runSQLinJDBC(sql, ";", this.dbProfile, true);

	}
	/**
	 * Test alter sequence function
	 * */
	@Test (description = "Test alter sequence function")
	public void testAlterSequence() throws InterruptedException
	{
		logger.info( "Test alter sequence ..." );
		/**
		 * Response data
		 * {"items": [{"statement": "ALTER SEQUENCE \"SEQTEST\".\"SEQ3\" INCREMENT BY 2"}]}
		 */
		
		String expected[] = {
				"ALTER SEQUENCE \"SEQTEST\".\"SEQ3\" INCREMENT BY 2"};
		assertEquals("GetLUWAlterSeq", expected);

		logger.info("PASSED: alter sequence executed successfully");

	}
	@Test (description = "Test execute alter sequence DDL")
	public void testAlterSequenceExecuteDDL() throws InterruptedException
	{
		logger.info( "Test execute alter sequence DDL..." );
		String sql="ALTER SEQUENCE \"SEQTEST\".\"SEQ3\" INCREMENT BY 2;";
		sqlEditorService.runSQLinJDBC(sql, ";", this.dbProfile, true);

	}
	/**
	 * Test drop sequence function
	 * */
	@Test (description = "Test drop sequence function")
	public void testDropSequence() throws InterruptedException
	{
		logger.info( "Test drop sequence ..." );
		/**
		 * Response data
		 * {"items": [{"statement": "DROP SEQUENCE \"SEQTEST\".\"SEQ2\""}]}
		 */
		
		String expected[] = {
				"DROP SEQUENCE \"SEQTEST\".\"SEQ2\""};
		assertEquals("GetLUWDropSeq", expected);

		logger.info("PASSED: drop sequence executed successfully");
	}
	@Test (description = "Test execute drop sequence DDL")
	public void testDropSequenceExecuteDDL() throws InterruptedException
	{
		logger.info( "Test execute drop sequence DDL..." );
		String sql="DROP SEQUENCE \"SEQTEST\".\"SEQ2\";"+
				   "CREATE SEQUENCE \"SEQTEST\".\"SEQ2\" START WITH 1 CACHE 24;";
		sqlEditorService.runSQLinJDBC(sql, ";", this.dbProfile, true);
	}
	
	private void testCreateTrigger_precondition() {
		// TODO Auto-generated method stub
		dropTriggerEnv(false);
		dropTrigger_schema(false);
		createTrigger();
	}
	private void testTrigger_clean() {
		// TODO Auto-generated method stub
		dropTriggerEnv(true);
		dropTrigger_schema(true);
	}
	private void dropTriggerEnv(boolean real) {
		// TODO Auto-generated method stub
		List<String> dropList = new ArrayList<String>();
		dropList.add("DROP TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_TABLE2\"");
		dropList.add("DROP TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_VIEW1\"");
		dropList.add("DROP VIEW \"MANTRIGGER\".\"VIEW_T1\"");
		dropList.add("DROP TABLE \"MANTRIGGER\".\"T1\"");		
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile,real);
	}
	private void dropTrigger_schema(boolean real) {
		// TODO Auto-generated method stub
		List<String> dropList = new ArrayList<String>();
		dropList.add("DROP SCHEMA \"MANTRIGGER\" RESTRICT");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile,real);
	}
	private void createTrigger() {
		// TODO Auto-generated method stub
		String sqlStatement = "CREATE SCHEMA \"MANTRIGGER\";"+
				"CREATE TABLE \"MANTRIGGER\".\"T1\"( "+
				"C1  integer not null, "+
				"C2  char(5) not null, "+
				"C3  varchar(5), "+
				"C4  integer not null);"+
				"CREATE VIEW \"MANTRIGGER\".\"VIEW_T1\"(C1,C2,C3) as select C1,C2,C3 from \"MANTRIGGER\".\"T1\";"+
				"CREATE TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_VIEW1\" INSTEAD OF DELETE ON \"MANTRIGGER\".\"VIEW_T1\" FOR EACH ROW select C1 from \"MANTRIGGER\".\"VIEW_T1\";"+
				"CREATE TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_TABLE2\" AFTER UPDATE ON \"MANTRIGGER\".\"T1\" "+
				"REFERENCING OLD TABLE AS T2 NEW TABLE AS T3 FOR EACH STATEMENT select * from \"MANTRIGGER\".\"T1\";";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile, true);
	}
	/**@author ylwh@cn.ibm.com
	 * Test create one trigger on table function
	 * */
	@Test (description = "Test create trigger on table function")
	public void testCreateTriggerOnTable() throws InterruptedException
	{
		logger.info( "Test create trigger1 on table with parameter before,Row and delete ..." );
		/**
		 * Response data
		 * 
		 * {"items": [{"statement": "CREATE TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_TABLE1\" BEFORE DELETE ON \"MANTRIGGER\".\"T1\" REFERENCING OLD AS C3 FOR EACH ROW select * from MANTRIGGER.T1"}]}	
		 */
		
		String expected[] = {
				"CREATE TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_TABLE1\" BEFORE DELETE ON \"MANTRIGGER\".\"T1\" REFERENCING OLD AS C3 FOR EACH ROW select * from MANTRIGGER.T1"};
		assertEquals("GetLUWEditTableTrigger1", expected);

		logger.info("PASSED: create trigger on table executed successfully");;
	}
	@Test (description = "Test execute create trigger DDL")
	public void testCreateTriggerOnTableExecuteDDL() throws InterruptedException
	{
		logger.info("Test execute create trigger DDL");
		String sql="CREATE TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_TABLE1\" BEFORE DELETE ON \"MANTRIGGER\".\"T1\" REFERENCING OLD AS C3 FOR EACH ROW select * from \"MANTRIGGER\".\"T1\";"+
		"DROP TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_TABLE1\";";
		sqlEditorService.runSQLinJDBC(sql, ";", this.dbProfile, true);
	}
	/**
	 * Test create trigger on view function
	 * */
	@Test (description = "Test create trigger on view function")
	public void testCreateTriggerOnView() throws InterruptedException
	{
		logger.info( "Test create trigger on view with parameter instead of ,row and insert ..." );
		/**
		 * Response data
		 * 
		 * {"items": [{"statement": "CREATE TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_VIEW\" INSTEAD OF INSERT ON \"MANTRIGGER\".\"VIEW_T1\" REFERENCING NEW AS A1 NEW TABLE AS T3 FOR EACH ROW select * from \"MANTRIGGER\".\"VIEW_T1\""}]}	
		 */
		
		String expected[] = {
				"CREATE TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_VIEW\" INSTEAD OF INSERT ON \"MANTRIGGER\".\"VIEW_T1\" REFERENCING NEW AS A1 NEW TABLE AS T3 FOR EACH ROW select * from MANTRIGGER.VIEW_T1"};
		assertEquals("GetLUWEditViewTrigger", expected);

		logger.info("PASSED: create trigger on view executed successfully");
	}
	@Test (description = "Test execute trigger DDL on view ")
	public void testCreateTriggerOnViewExecuteDDL() throws InterruptedException
	{
		logger.info( "Test execute trigger on view DDL..." );
		String sql="CREATE TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_VIEW\" INSTEAD OF INSERT ON \"MANTRIGGER\".\"VIEW_T1\" REFERENCING NEW AS A1 NEW TABLE AS T3 FOR EACH ROW select * from \"MANTRIGGER\".\"VIEW_T1\";"+
		"DROP TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_VIEW\";";
		sqlEditorService.runSQLinJDBC(sql, ";", this.dbProfile, true);
	}
	
	/**
	 * Test execute alter trigger on table 
	 * */
	@Test (description = "Test alter trigger on table function")
	public void testAlterTrigger1OnTableExecuteDDL() throws InterruptedException
	{
		logger.info( "Test alter trigger..." );
		String sql="CREATE OR REPLACE TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_TABLE3\" AFTER UPDATE ON \"MANTRIGGER\".\"T1\" REFERENCING OLD "+
		"TABLE AS T2 NEW TABLE AS T3 FOR EACH STATEMENT select C1 from \"MANTRIGGER\".\"T1\";"+
				"DROP TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_TABLE3\";";
		sqlEditorService.runSQLinJDBC(sql, ";", this.dbProfile, true);
	}
	/**
	 * Test drop trigger on view function
	 * */
	@Test (description = "Test drop trigger on view function")
	public void testDropTriggerOnView() throws InterruptedException
	{
		logger.info( "Test drop trigger..." );
		/**
		 * Response data
		 * {"items": [{"statement": "DROP TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_VIEW1\""}]}	
		 */
		
		String expected[] = {
				"DROP TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_VIEW1\""};
		assertEquals("GetLUWDropViewTrigger", expected);

		logger.info("PASSED: create trigger on view executed successfully");
	
	}
	@Test (description = "Test execute drop trigger on view DDL")
	public void testDropTriggerOnViewExecuteDDL() throws InterruptedException
	{
		logger.info( "Test execute drop trigger DDL..." );
		String sql="DROP TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_VIEW1\";"+
		"CREATE TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_VIEW1\" INSTEAD OF DELETE ON \"MANTRIGGER\".\"VIEW_T1\" FOR EACH ROW select C1 from \"MANTRIGGER\".\"VIEW_T1\";";	
		sqlEditorService.runSQLinJDBC(sql, ";", this.dbProfile, true);
	}
	/**
	 * Test  Generate Trigger2 DDL in CLP method on table function
	 * */
	

	@Test (description = "Test Generate Trigger2 DDL on table function")
	public void testTriggerGenerateDDL() throws InterruptedException
	{
		logger.info( "Test Generate Trigger2 DDL method..." );
		
		/**
		 * Response Data
		 * {"items": [{"ddl": "\n\nDROP TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_TABLE2\";\n\nCREATE OR REPLACE TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_TABLE2\" AFTER UPDATE ON \"MANTRIGGER\".\"T1\"\nREFERENCING OLD TABLE AS T2 NEW TABLE AS T3 FOR EACH STATEMENT select *\nfrom \"MANTRIGGER\".\"T1\";"}]}
		 */	
	String query = alterService.getJSONData("GetLUWGenTrigger2DDL");
	
	JSONObject resObj =alterService.callAlterObjectService(AdminAlterObjectService.GENERATE_DDL,query, dbProfile);
	//Get the response data
	JSONObject responseData =(JSONObject) resObj.get("ResponseData");
	
	JSONArray ja = responseData.getJSONArray( "items" );
	
	Assert.assertNotNull( ja );
			
	String ddl = "";
	
	ddl = JsonPath.read( responseData, "$.items[0].ddl" );
	Assert.assertTrue(ddl.contains("DROP TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_TABLE2\";"));
	Assert.assertTrue(ddl.contains("\n\nCREATE OR REPLACE TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_TABLE2\" AFTER UPDATE ON \"MANTRIGGER\".\"T1\"\nREFERENCING OLD TABLE AS T2 NEW TABLE AS T3 FOR EACH STATEMENT select *\nfrom \"MANTRIGGER\".\"T1\";"));
	logger.info("PASSED: generate trigger DDL is correct");	
	}
	/**@author ylwh@cn.ibm.com
	 * Test execute Generate Trigger2 DDL on table function
	 * */
	@Test (description = "Test execute Generate Trigger2 DDL on table ")
	public void testTriggerGenerateDDLExecuteDDL() throws InterruptedException
	{
		logger.info( "Test execute Generate Trigger2 DDL method..." );
		String sql= null;		
		sql= "\n\nDROP TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_TABLE2\";"+ 
			 "\n\nCREATE OR REPLACE TRIGGER \"MANTRIGGER\".\"TRIGGER_T1_TABLE2\" AFTER UPDATE ON \"MANTRIGGER\".\"T1\"\nREFERENCING OLD TABLE AS T2 NEW TABLE AS T3 FOR EACH STATEMENT select *\nfrom \"MANTRIGGER\".\"T1\";";
		sqlEditorService.runSQLinJDBC(sql, ";", this.dbProfile, true);
	}
	//@author ylwh@cn.ibm.com
	private void testCreateView_precondition() {
		// TODO Auto-generated method stub
		dropView(false);
		createView();
	}
	private void createView() {
		// TODO Auto-generated method stub
		List<String> createList = new ArrayList<String>();
		createList.add("CREATE SCHEMA TESTVIEW;");
		createList.add("CREATE TABLE TESTVIEW.T1( "+
					   "C1 INTEGER NOT NULL, "+
					   "C2 CHAR(5), "+
					   "C3 VARCHAR(5) NOT NULL, "+
					   "C4 INTEGER);");
		createList.add("INSERT INTO TESTVIEW.T1 VALUES(111,'11111','abc', 222);");
		createList.add("CREATE VIEW TESTVIEW.VIEW_T1(C1,C2,C4) AS SELECT C1,C2,C4 FROM TESTVIEW.T1;");
		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}
	private void dropView(boolean real) {
		List<String> dropList = new ArrayList<String>();
		dropList.add("DROP VIEW TESTVIEW.VIEW_T1;");
		dropList.add("DROP TABLE TESTVIEW.T1;");
		dropList.add("DROP SCHEMA TESTVIEW RESTRICT;");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, real);
	}
	private void testView_clean() {
		// TODO Auto-generated method stub
		dropView(true);
	}
	/**@author ylwh@cn.ibm.com
	 * Test execute view's function of Generating DDL on table function
	 * */
	@Test (description = "Test execute view's function of Generating DDL on table ")
	public void testViewGenerateDDL() throws InterruptedException
	{
		logger.info( "Running test of execute View Generate DDL method..." );
		String query = alterService.getJSONData("GetLUWGenViewDDL");
		
		JSONObject resObj =alterService.callAlterObjectService(AdminAlterObjectService.GENERATE_DDL,query, dbProfile);
		//Get the response data
		JSONObject responseData =(JSONObject) resObj.get("ResponseData");
		/**
		 * Response Data's DDL:
		 * 	-- This CLP file was created using DB2LOOK Version "10.5" 
		 *  -- Timestamp: Tue 11 Oct 2016 10:19:22 AM CST
		 *	-- Database Name: TPCDS10B       
		 *	-- Database Manager Version: DB2/LINUXX8664 Version 10.5.6 
		 *	-- Database Codepage: 1208
		 *	-- Database Collating Sequence is: IDENTITY
		 *	-- Alternate collating sequence(alt_collate): null
		 *	-- varchar2 compatibility(varchar2_compat): OFF
		 *	   CONNECT TO TPCDS10B;
		 *	----------------------------
		 *	-- DDL Statements for Views
		 *	----------------------------
		 *		SET CURRENT SCHEMA = "DB2INST1";
		 *		SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","DB2INST1";
		 *		DROP VIEW "TESTVIEW"."VIEW_T1";
		 *		CREATE OR REPLACE VIEW TESTVIEW.VIEW_T1(C1,C2,C4) AS SELECT C1,C2,C4 FROM TESTVIEW.T1;
		 *		COMMIT WORK;
		 *		CONNECT RESET;
		 * */	
		JSONArray ja = responseData.getJSONArray( "items" );
		
		Assert.assertNotNull( ja );
				
		String ddl = "";
		
		ddl = JsonPath.read( responseData, "$.items[0].ddl" );
		Assert.assertTrue(ddl.contains("CONNECT TO"));
		Assert.assertTrue(ddl.contains("DROP VIEW \"TESTVIEW\".\"VIEW_T1\";"));
		Assert.assertTrue(ddl.contains("CREATE OR REPLACE VIEW TESTVIEW.VIEW_T1(C1,C2,C4) AS SELECT C1,C2,C4 FROM TESTVIEW.T1;"));
		Assert.assertTrue(ddl.contains("CONNECT RESET"));
		logger.info("PASSED: generate trigger DDL is correct");	
	}
	/**@author ylwh@cn.ibm.com
	 *Test execute DDL of View Generate DDL on table function,Blocked by the method runSQLinCLP.
	 * */
//	@Test (description = "Test execute DDL of View Generate DDL on table ")
//	public void testViewGenerateDDLExecuteDDL() throws InterruptedException
//	{
//		logger.info( "Running test of Generate View DDL execute DDL..." );
//		String ddl="DROP VIEW \"TESTVIEW\".\"VIEW_T1\";"+
//				   "CREATE OR REPLACE VIEW TESTVIEW.VIEW_T1(C1,C2,C4) AS SELECT C1,C2,C4 FROM TESTVIEW.T1;"+
//				   "COMMIT WORK;"+
//				   "CONNECT RESET;";
//		sqlEditorService.runSQLinCLP(ddl, ";",true, this.dbProfile);
//		logger.info( "PASSED: Test of Generate View DDL method execute  success..." );
//	}
	/**@author ylwh@cn.ibm.com
	 * Test view view data function
	 * */
	@Test(description="Administer->Views->select a view->click data button")
	public void testViewData() throws InterruptedException{
		logger.info("Running test of viewing view's data");
		String query = alterService.getJSONData("GetLUWViewData");
		JSONObject resObj = alterService.callAlterObjectService(AdminAlterObjectService.BROWSEDATA_COMMAND, query, dbProfile);
		JSONObject responsedata = (JSONObject)resObj.get("ResponseData");
		logger.info("view view'data response:"+responsedata);
		Assert.assertNotNull(responsedata);
		Assert.assertTrue(responsedata.toString().length()>0);
		Assert.assertTrue(responsedata.containsKey("items"));
		String jc4 = JsonPath.read(responsedata," $.items[0].C4");
		String jc1 = JsonPath.read(responsedata," $.items[0].C1");
		String jc2 = JsonPath.read(responsedata," $.items[0].C2");
		Assert.assertTrue(jc4.equals("222"));
		Assert.assertTrue(jc1.equals("111"));
		Assert.assertTrue(jc2.equals("11111"));
		logger.info("PASSED: view view's data executed successfully");
	}
	//@author ylwh@cn.ibm.com
	private void testCreateClusteredIndex_precondition() {
		// TODO Auto-generated method stub
		dropClusteredIndex(false);
		createClusteredIndex();
	}
	private void testCreateClusteredIndex_clean() {
		// TODO Auto-generated method stub
		dropClusteredIndex(true);
	}
	private void createClusteredIndex(){
		// TODO Auto-generated method stub
		List<String> createList = new ArrayList<String>();
		createList.add("CREATE SCHEMA CLU_SCH;");
		createList.add("CREATE TABLE CLU_SCH.CLU_TAB( "+
					   "ID INTEGER NOT NULL UNIQUE, "+
					   "NAME VARCHAR(10), "+
					   "SEX CHAR(5), "+
					   "TEL INTEGER );");
		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}
	private void dropClusteredIndex(boolean real){
		List<String> dropList = new ArrayList<String>();
		dropList.add("DROP TABLE CLU_SCH.CLU_TAB;");
		dropList.add("DROP SCHEMA CLU_SCH RESTRICT;");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, real);
	}
	/**@author ylwh@cn.ibm.com
	 * Test execute execute creating clustered index function
	 * */
	@Test (description = "Test execute creating clustered index ")
	public void testCreateClusteredIndex() throws InterruptedException
	{
		logger.info( "Running test of creating clustered index method..." );
		String query = alterService.getJSONData("GetLUWCreateClusteredIndex");
		
		JSONObject resObj =alterService.callAlterObjectService(AdminAlterObjectService.ALTER_OBJECT,query, dbProfile);
		//Get the response data
		JSONObject responseData =(JSONObject) resObj.get("ResponseData");
		/**
		 * Response Data's DDL:
		 * {"items":[{"statement":"CREATE UNIQUE INDEX \"CLU_SCH\".\"CLU_INDEX\" ON \"CLU_SCH\".\"CLU_TAB\" (\"ID\" ASC) INCLUDE (\"NAME\") CLUSTER"}]}
		 * */	
		JSONArray ja = responseData.getJSONArray( "items" );
		logger.info("response ddl:"+responseData.toString());
		
		Assert.assertNotNull( ja );
				
		String ddl = "";
		
		ddl = JsonPath.read( responseData, "$.items[0].statement" );
		Assert.assertTrue(ddl.equals("CREATE UNIQUE INDEX \"CLU_SCH\".\"CLU_INDEX\" ON \"CLU_SCH\".\"CLU_TAB\" (\"ID\" ASC) INCLUDE (\"NAME\") CLUSTER"));
		logger.info("PASSED: the case of creating clustered index is correct");	
	}
	/**@author ylwh@cn.ibm.com
	 *Test execute DDL of creating clustered Index function
	 * */
	@Test (description = "Test execute DDL of creating clutered Index function ")
	public void testCreateClusteredIndexExecuteDDL() throws InterruptedException
	{
		logger.info( "Running test of creating clustered Index execute DDL..." );
		String ddl="CREATE UNIQUE INDEX \"CLU_SCH\".\"CLU_INDEX\" ON \"CLU_SCH\".\"CLU_TAB\" (\"ID\" ASC) INCLUDE (\"NAME\") CLUSTER;"+
					"DROP INDEX \"CLU_SCH\".\"CLU_INDEX\";";
		sqlEditorService.runSQLinJDBC(ddl, ";",this.dbProfile, true);
		logger.info( "PASSED: Test of creating clustered Index execute DDL success..." );
	}
}
