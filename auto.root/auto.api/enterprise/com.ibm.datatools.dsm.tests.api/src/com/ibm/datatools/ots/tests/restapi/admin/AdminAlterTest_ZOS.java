package com.ibm.datatools.ots.tests.restapi.admin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.ibm.datatools.ots.tests.restapi.common.AdminAlterObjectService;
import com.jayway.jsonpath.JsonPath;

public class AdminAlterTest_ZOS extends AdminAlterTest
{

	private static final Logger logger = LogManager
			.getLogger(AdminAlterTest_ZOS.class);


	@BeforeClass
	public void beforeTest() {
		// it will be used for alter table add column,alter alter column, alter
		// table properties
		testAlterTableColumn_precondition();
		testAlterTableProperty_precondition();
		testAlterTableConstraint_precondition();
	}

	@AfterClass
	public void afterTest()
	{
		testAlterTableColumn_clean();
		testAlterTableProperty_clean();
		testAlterTableConstraint_clean();
	}

	/**
	 * Copy this method to create a new alter test
	 * */
	// @Test(description="<description>")
	// @Parameters({"dbProfile"})
	@SuppressWarnings("unused")
	private void testFormat() {
		logger.info("Running <test name> test...");

		String queryJSON = alterService.getJSONData("JSON_Data_URL");

		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.ALTER_OBJECT, queryJSON, dbProfile);
		// Get the response Data
		if (resObj != null) {
			JSONObject responseData = (JSONObject) resObj.get("ResponseData");
			Assert.assertNotNull(responseData);
			Assert.assertTrue(responseData.toString().length() > 0);
			/**
			 * Path the response data, for this case, the response data is:
			 * 
			 * 
			 * To retrieve the statement, we are looking for...
			 * $.items[0].statement
			 * */
			String ddl = JsonPath.read(responseData, "<pass expression>");
			Assert.assertEquals(ddl, "expected");

			logger.info("PASSED: <test Name> executed successfully");
		} else {
			Assert.fail("Response data is null");
			logger.warn("ERROR: <test Name> response data is null");
		}
	}

	// create index

	@Test(description = "get create index definition")
	private void testCreateIndexGetDefinition() {
		logger.info("get create index definition...");

		String queryJSON = alterService
				.getJSONData("getCreateIndexGetDefinition_ZOS");

		JSONObject resObj = alterService.callCreateDefinitionService(queryJSON,
				dbProfile);
		// Get the response Data
		if (resObj != null) {
			JSONObject responseData = (JSONObject) resObj.get("ResponseData");
			Assert.assertNotNull(responseData);
			Assert.assertTrue(!"{}".equals(responseData.toString()),
					"responseData is empty!");
			/**
			 * Path the response data, for this case, the response data is:
			 * {"items"
			 * :[{"values":[{"value":"AAAAAAA","label":"AAAAAAA"},{"value"
			 * :"c12op02","label":"c12op02"}],....
			 * "name":"INDSCHEMA","label":"Schema name - index"
			 * ,"valueType":"enum"
			 * ,"defaultValue":{"value":"C12OP02","label":"C12OP02"}},
			 * {"name":"INDNAME"
			 * ,"label":"Index name","valueType":"string","defaultValue"
			 * :{"value"
			 * :"IND","label":"IND"}},{"name":"TABNAME","label":"Table name"
			 * ,"valueType"
			 * :"readOnly"},{"name":"TABSCHEMA","label":"Schema name - table"
			 * ,"valueType":"readOnly"},
			 * {"name":"UNIQUE","label":"Unique","valueType"
			 * :"boolean","defaultValue":{"value":"No","label":"No"}},
			 * {"dependencies"
			 * :[{"condition":[{"rule":"equal","value":"Yes","key"
			 * :"UNIQUE"}],"disable"
			 * :false},{"condition":[{"rule":"equal","value"
			 * :"No","key":"UNIQUE"}
			 * ],"disable":true}],"name":"UNIQUENOTNULL","label"
			 * :"Where not null"
			 * ,"valueType":"boolean","defaultValue":{"value":"No"
			 * ,"label":"No"}},
			 * {"name":"MEMBERS","label":"Key members","valueType":"members"},
			 * {"name"
			 * :"EXPRESSIONMEMBERS","label":"Expression members","valueType"
			 * :"ExpressionMembers"},
			 * {"name":"CLUSTERING","label":"Clustering","valueType"
			 * :"boolean","defaultValue":{"value":"No","label":"No"}},
			 * {"name":"PADDED"
			 * ,"label":"Padded","valueType":"boolean","defaultValue"
			 * :{"value":"No","label":"No"}},
			 * {"name":"CLOSE","label":"Close rule"
			 * ,"valueType":"boolean","defaultValue"
			 * :{"value":"Yes","label":"Yes"}},
			 * {"name":"COPY","label":"Copy","valueType"
			 * :"boolean","defaultValue":{"value":"No","label":"No"}},
			 * {"dependencies"
			 * :[{"condition":[{"rule":"equal","value":"Yes","key"
			 * :"UNIQUE"}],"disable"
			 * :false},{"condition":[{"rule":"equal","value"
			 * :"No","key":"UNIQUE"}
			 * ],"disable":true}],"name":"INCLUDE","label":"Include columns"
			 * ,"valueType":"IncludeMembers"},
			 * {"name":"DEFER","label":"Defer","valueType"
			 * :"boolean","defaultValue":{"value":"No","label":"No"}},
			 * {"name":"DEFINE"
			 * ,"label":"Define","valueType":"boolean","defaultValue"
			 * :{"value":"Yes","label":"Yes"}},
			 * {"name":"PIECESIZE","label":"Piece size","valueType":"integer"},
			 * {"name":"FREEPAGE","label":"Free page","valueType":"integer"},
			 * {"name":"PCTFREE","label":"Percentage of free page ","valueType":
			 * "integer"
			 * },{"values":[{"value":"CHANGED","label":"CHANGED"},{"value"
			 * :"ALL","label":"ALL"},{"value":"NONE","label":"NONE"}],"name":
			 * "GBPCACHE","label":"Group buffer pool cache","valueType":"enum",
			 * "defaultValue":{"value":"CHANGED","label":"CHANGED"}}]}
			 * 
			 * To retrieve the statement, we are looking for...
			 * $.items[0].statement
			 * */

			if (responseData.getJSONArray("items").size() > 0) {
				/*String[] nameList = { "INDSCHEMA", "INDNAME", "TABNAME",
						"TABSCHEMA", "UNIQUE", "UNIQUENOTNULL", "MEMBERS",
						"EXPRESSIONMEMBERS", "CLUSTERING", "PADDED", "CLOSE",
						"COPY", "INCLUDE", "DEFER", "DEFINE", "PIECESIZE",
						"FREEPAGE", "PCTFREE", "GBPCACHE", "INCLUDENULLKEY"};
				String[] typeList = { "enum", "string", "readOnly", "readOnly",
						"boolean", "boolean", "members", "ExpressionMembers",
						"boolean", "boolean", "boolean", "boolean",
						"IncludeMembers", "boolean", "boolean", "integer",
						"integer", "integer", "enum", "boolean"};

				JSONArray ja = (JSONArray) JsonPath.read(responseData,
						"$.items");
				Assert.assertTrue(ja.size() == nameList.length);
				for (int i = 0; i < ja.size(); i++) {
					Assert.assertEquals(
							JsonPath.read(responseData, "$.items[" + i
									+ "].name"), nameList[i]);
					Assert.assertEquals(
							JsonPath.read(responseData, "$.items[" + i
									+ "].valueType"), typeList[i]);
				}*/
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[0].values"));
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[5].dependencies[0].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[12].dependencies[0].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[18].values"));
			}
			logger.info("PASSED: testGetIndexList executed successfully");
		} else {
			Assert.fail("testCreateIndexGetDefinition response data is null");
			logger.warn("ERROR: testCreateIndexGetDefinition response data is null");
		}
	}

	@Test(description = "create index DDL --- tablename")
	private void testCreateIndexDDLWithTableName() {
		logger.info("create index DDL --- tablename...");

		String queryJSON = alterService
				.getJSONData("getCreateIndexGetDDLWithTableName_ZOS");

		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.ALTER_OBJECT, queryJSON, dbProfile);
		// Get the response Data
		if (resObj != null) {
			JSONObject responseData = (JSONObject) resObj.get("ResponseData");
			Assert.assertNotNull(responseData);
			Assert.assertTrue(!"{}".equals(responseData.toString()),
					"responseData is empty!");
			/**
			 * Path the response data, for this case, the response data is:
			 * {"items":[{"statement":
			 * "CREATE INDEX \"PRD\".\"IND\" ON \"PRD\".\"ALERTS\" (\"ACTDESC\" ASC)"}]} To
			 * retrieve the statement, we are looking for...
			 * $.items[0].statement
			 * */

			if (responseData.getJSONArray("items").size() > 0) {

				String statement = JsonPath.read(responseData,
						"$.items[0].statement");
				Assert.assertEquals(statement,
						"CREATE INDEX \"PRD\".\"IND\" ON \"PRD\".\"ALERTS\" (\"ACTDESC\" ASC)");
			}
			logger.info("PASSED: testCreateIndexDDLWithTableName executed successfully");
		} else {
			Assert.fail("testCreateIndexDDLWithTableName response data is null");
			logger.warn("ERROR: testCreateIndexDDLWithTableName response data is null");
		}
	}

	@Test(description = "create index DDL --- other options")
	private void testCreateIndexDDLWithOptions() {
		logger.info("create index DDL --- other options...");

		String queryJSON = alterService
				.getJSONData("getCreateIndexGetDDLWithOptions_ZOS");

		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.ALTER_OBJECT, queryJSON, dbProfile);
		// Get the response Data
		if (resObj != null) {
			JSONObject responseData = (JSONObject) resObj.get("ResponseData");
			Assert.assertNotNull(responseData);
			Assert.assertTrue(!"{}".equals(responseData.toString()),
					"responseData is empty!");
			/**
			 * Path the response data, for this case, the response data is:
			 * {"items":[{"statement":
			 * "CREATE UNIQUE WHERE NOT NULL INDEX \"ICM\".\"IND\" ON \"PRD\".\"ALERTS\" (\"C_ALERT_TYPE\" ASC,\"C_RCD_DEL\" ASC,UPPER(\"C_RCD_DEL\") RANDOM) INCLUDE (\"D_ACTV_DT\") CLUSTER PADDED CLOSE NO COPY YES DEFER YES DEFINE NO PIECESIZE 23 FREEPAGE 23 PCTFREE 23 GBPCACHE ALL"
			 * }]} To retrieve the statement, we are looking for...
			 * $.items[0].statement
			 * */

			if (responseData.getJSONArray("items").size() > 0) {

				String statement = JsonPath.read(responseData,
						"$.items[0].statement");
				Assert.assertEquals(
						statement,
						"CREATE UNIQUE WHERE NOT NULL INDEX \"ICM\".\"IND\" ON \"PRD\".\"ALERTS\" "
								+ "(\"C_ALERT_TYPE\" ASC,\"C_RCD_DEL\" ASC,UPPER(\"C_RCD_DEL\") RANDOM) "
								+ "INCLUDE (\"D_ACTV_DT\") CLUSTER PADDED CLOSE NO COPY YES DEFER YES DEFINE NO "
								+ "PIECESIZE 23 FREEPAGE 23 PCTFREE 23 GBPCACHE ALL");
			}
			logger.info("PASSED: testCreateIndexDDLWithOptions executed successfully");
		} else {
			Assert.fail("testCreateIndexDDLWithOptions response data is null");
			logger.warn("ERROR: testCreateIndexDDLWithOptions response data is null");
		}
	}
	
	
	@Test(description = "get create tablespace partition definition")
	private void testCreateTablespaceGetDefinition() {
		logger.info("get create tablespace partition definition...");

		String queryJSON = alterService
				.getJSONData("getCreateTablespaceGetDefinition_ZOS");

		JSONObject resObj = alterService.callCreateDefinitionService(queryJSON,
				dbProfile);
		
		if (resObj != null) {
			JSONObject responseData = (JSONObject) resObj.get("ResponseData");
			Assert.assertNotNull(responseData);
			Assert.assertTrue(!"{}".equals(responseData.toString()),
					"responseData is empty!");
			
			if (responseData.getJSONArray("items").size() > 0) {
				String[] nameList = {"TBSPACE", "LOB", "DATABASE", "STOGROUPORVCAT", "PQTY", "SQTY", 
						"ERASE", "GBPCACHE", "COMPRESS", "PCTFREE", "FREEPAGE", "TRACKMOD", "DSSIZE", 
						"SEGSIZE", "MEMBER_CLUSTER", "DEFINE", "LOCKSIZE", "CCSID", "MAXROWS", "CLOSERULE", 
						"BPOOL", "LOCKMAX", "LOG"};
				
				String[] labelList = {"Name", "Lob", "Database", "Storage group/VCAT", 
						"Primary space allocation", "Secondary space allocation", "Erase", 
						"Group buffer pool cache", "Compress", "Free page(%)", "Free page", 
						"Track model ", "Maximum size of data set(G)", "Segment size", 
						"Member cluster", "Define", "Lock size", "Encoding scheme", 
						"Max rows/Page", "Close rule", "Buffer pool", "Lock max(pages)", "Log changes"};
				
				String[] valueTypeList = {"string", "boolean", "grid_browser_editor", "radio_browser", 
						"string", "string", "boolean", "enum", "boolean", 
						"string", "string", "boolean", "enum", "enum", 
						"boolean", "boolean", "enum", "enum", "integer", 
						"boolean", "string", "string", "boolean"};
				
				Map<String,String> nameValueMap = new HashMap<String,String>();
				
				nameValueMap.put("TBSPACE", "NEWTBS");
				nameValueMap.put("LOB", "No");
				nameValueMap.put("STOGROUPORVCAT", "None");
				nameValueMap.put("PQTY", "-1");
				nameValueMap.put("SQTY", "-1");
				nameValueMap.put("ERASE", "No");
				nameValueMap.put("GBPCACHE", "CHANGED");
				nameValueMap.put("COMPRESS", "No");
				nameValueMap.put("PCTFREE", "5");
				nameValueMap.put("FREEPAGE", "0");
				nameValueMap.put("TRACKMOD", "Yes");
				nameValueMap.put("DSSIZE", " ");
				nameValueMap.put("SEGSIZE", " ");
				nameValueMap.put("DEFINE", "Yes");
				nameValueMap.put("LOCKSIZE", "ANY");
				nameValueMap.put("CCSID", " ");
				nameValueMap.put("MAXROWS", "255");
				nameValueMap.put("CLOSERULE", "Yes");
				nameValueMap.put("LOG", "Yes");
				
				JSONArray ja = (JSONArray) JsonPath.read(responseData,
						"$.items");
				Assert.assertTrue(ja.size() == nameList.length);
				
				for (int i = 0; i < ja.size(); i++) {
					
					Assert.assertEquals(
							JsonPath.read(responseData, "$.items[" + i
									+ "].name"), nameList[i]);
					
					String name  = JsonPath.read(responseData, "$.items[" + i
							+ "].name");
					
					
					JSONObject valueObject = JsonPath.read(responseData, "$.items[" + i
							+ "].defaultValue");
					
					if(valueObject != null){
						String value = valueObject.getString("value");
						Assert.assertEquals(value, nameValueMap.get(name));
					}
					
					
					Assert.assertEquals(
							JsonPath.read(responseData, "$.items[" + i
									+ "].label"), labelList[i]);
					
					Assert.assertEquals(
							JsonPath.read(responseData, "$.items[" + i
									+ "].valueType"), valueTypeList[i]);
					
				}

				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[3].values"));
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[12].values"));
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[13].values"));
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[17].values"));
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[4].dependencies[0].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[4].dependencies[1].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[4].dependencies[2].condition"),
						"dependencies or condition is null");
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[5].dependencies[0].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[5].dependencies[1].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[5].dependencies[2].condition"),
						"dependencies or condition is null");
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[6].dependencies[0].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[6].dependencies[1].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[6].dependencies[2].condition"),
						"dependencies or condition is null");
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[7].dependencies[0].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[7].dependencies[1].condition"),
						"dependencies or condition is null");
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[8].dependencies[0].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[8].dependencies[1].condition"),
						"dependencies or condition is null");
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[9].dependencies[0].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[9].dependencies[1].condition"),
						"dependencies or condition is null");
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[10].dependencies[0].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[10].dependencies[1].condition"),
						"dependencies or condition is null");
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[11].dependencies[0].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[11].dependencies[1].condition"),
						"dependencies or condition is null");
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[12].dependencies[0].condition"),
						"dependencies or condition is null");
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[13].dependencies[0].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[13].dependencies[1].condition"),
						"dependencies or condition is null");
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[14].dependencies[0].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[14].dependencies[1].condition"),
						"dependencies or condition is null");
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[16].dependencies[0].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[16].dependencies[1].condition"),
						"dependencies or condition is null");
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[17].dependencies[0].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[17].dependencies[1].condition"),
						"dependencies or condition is null");
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[18].dependencies[0].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[18].dependencies[1].condition"),
						"dependencies or condition is null");
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[21].dependencies[0].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[21].dependencies[1].condition"),
						"dependencies or condition is null");
				
				logger.info("PASSED: testCreateTablespaceGetDefinition executed successfully");
			}else{
				Assert.fail("testCreateTablespaceGetDefinition response data is null");
				logger.warn("ERROR: testCreateTablespaceGetDefinition response data is null");
			}
		}
	}
	@Test(description = "get create tablespace definition")
	private void testCreateTablespacePartitionGetDefinition() {
		logger.info("get create tablespace definition...");

		String queryJSON = alterService
				.getJSONData("getCreateTablespacePartitionGetDefinition_ZOS");

		JSONObject resObj = alterService.callCreateDefinitionService(queryJSON,
				dbProfile);
		
		if (resObj != null) {
			JSONObject responseData = (JSONObject) resObj.get("ResponseData");
			
			Assert.assertNotNull(responseData);
			Assert.assertTrue(!"{}".equals(responseData.toString()),
					"responseData is empty!");
			
			if (responseData.getJSONArray("items").size() > 0) {
				String[] nameList = {"PARTITION_NUMBER", "STOGROUPORVCAT", "PQTY", "SQTY", 
						"ERASE", "GBPCACHE", "COMPRESS", "PCTFREE", "FREEPAGE", "TRACKMOD"};
				
				String[] labelList = {"Part number", "Storage group/VCAT", 
						"Primary space allocation", "Secondary space allocation", "Erase", 
						"Group buffer pool cache", "Compress", "Free page(%)", "Free page", 
						"Track model "};
				
				String[] valueTypeList = {"string", "radio_browser", 
						"string", "string", "boolean", "enum", "boolean", 
						"string", "string", "boolean"};
				
				Map<String,String> nameValueMap = new HashMap<String,String>();
				
				nameValueMap.put("STOGROUPORVCAT", "None");
				nameValueMap.put("PQTY", "-1");
				nameValueMap.put("SQTY", "-1");
				nameValueMap.put("ERASE", "No");
				nameValueMap.put("GBPCACHE", "CHANGED");
				nameValueMap.put("COMPRESS", "No");
				nameValueMap.put("PCTFREE", "5");
				nameValueMap.put("FREEPAGE", "0");
				nameValueMap.put("TRACKMOD", "Yes");
				
				JSONArray ja = (JSONArray) JsonPath.read(responseData,
						"$.items");
				Assert.assertTrue(ja.size() == nameList.length);
				
				for (int i = 0; i < ja.size(); i++) {
					
					Assert.assertEquals(
							JsonPath.read(responseData, "$.items[" + i
									+ "].name"), nameList[i]);
					
					String name  = JsonPath.read(responseData, "$.items[" + i
							+ "].name");
					
					
					JSONObject valueObject = JsonPath.read(responseData, "$.items[" + i
							+ "].defaultValue");
					
					if(valueObject != null){
						String value = valueObject.getString("value");
						Assert.assertEquals(value, nameValueMap.get(name));
					}
					
					
					Assert.assertEquals(
							JsonPath.read(responseData, "$.items[" + i
									+ "].label"), labelList[i]);
					
					Assert.assertEquals(
							JsonPath.read(responseData, "$.items[" + i
									+ "].valueType"), valueTypeList[i]);
					
				}

				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[1].values"));
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[5].values"));
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[2].dependencies[0].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[2].dependencies[1].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[2].dependencies[2].condition"),
						"dependencies or condition is null");
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[3].dependencies[0].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[3].dependencies[1].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[3].dependencies[2].condition"),
						"dependencies or condition is null");
				
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[4].dependencies[0].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[4].dependencies[1].condition"),
						"dependencies or condition is null");
				Assert.assertNotNull(JsonPath.read(responseData,
						"$.items[4].dependencies[2].condition"),
						"dependencies or condition is null");
				
				logger.info("PASSED: testCreateTablespaceGetDefinition executed successfully");
			}else{
				Assert.fail("testCreateTablespaceGetDefinition response data is null");
				logger.warn("ERROR: testCreateTablespaceGetDefinition response data is null");
			}
		}
	}
	
	@Test(description = "create segmented tablespace DDL")
	private void testCreateSegmentedTablespaceWithOptions() {
		logger.info("Create segmented tablespace DDL");

		String queryJSON = alterService
				.getJSONData("getCreateSegmentedTablespace_ZOS");

		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.ALTER_OBJECT, queryJSON, dbProfile);
		// Get the response Data
		if (resObj != null) {
			JSONObject responseData = (JSONObject) resObj.get("ResponseData");
			Assert.assertNotNull(responseData);
			Assert.assertTrue(!"{}".equals(responseData.toString()),
					"responseData is empty!");
			
			/**
			 * Path the response data, for this case, the response data is:
			 * {"items":[{"statement":
			 * "CREATE TABLESPACE \"TBSSEG\" USING STOGROUP SYSDEFLT PRIQTY 3 SECQTY 6 ERASE YES GBPCACHE ALL FREEPAGE 10 PCTFREE 10 TRACKMOD NO COMPRESS YES SEGSIZE 4 DEFINE NO LOCKSIZE TABLESPACE CCSID ASCII CLOSE NO BUFFERPOOL BP0 NOT LOGGED IN DSNDB04"
			 * }]}
			 * $.items[0].statement
			 * */
			if (responseData.getJSONArray("items").size() > 0) {

				String statement = JsonPath.read(responseData,
						"$.items[0].statement");
				Assert.assertEquals(
						statement,
						"CREATE TABLESPACE \"TBSSEG\" "
						+ "USING STOGROUP SYSDEFLT "
							+ "PRIQTY 3 "
							+ "SECQTY 6 "
							+ "ERASE YES "
						+ "GBPCACHE ALL "
						+ "FREEPAGE 10 "
						+ "PCTFREE 10 "
						+ "TRACKMOD NO "
						+ "COMPRESS YES "
						+ "SEGSIZE 4 "
						+ "DEFINE NO "
						+ "LOCKSIZE TABLESPACE "
						+ "CCSID ASCII "
						+ "CLOSE NO "
						+ "BUFFERPOOL BP0 "
						+ "NOT LOGGED "
						+ "IN DSNDB04");
			}
			logger.info("PASSED: testCreateTablespacePartitionGetDefinition executed successfully");
		} else {
			Assert.fail("testCreateTablespacePartitionGetDefinition response data is null");
			logger.warn("ERROR: testCreateTablespacePartitionGetDefinition response data is null");
		}
	}
	
	@Test(description = "create Partition-by-growth tablespace DDL")
	private void testCreatPBGTablespaceWithOptions() {
		logger.info("Create Partition-by-growth tablespace DDL");

		String queryJSON = alterService
				.getJSONData("getCreatePBGTablespace_ZOS");

		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.ALTER_OBJECT, queryJSON, dbProfile);
		// Get the response Data
		if (resObj != null) {
			JSONObject responseData = (JSONObject) resObj.get("ResponseData");
			Assert.assertNotNull(responseData);
			Assert.assertTrue(!"{}".equals(responseData.toString()),
					"responseData is empty!");
			
			/**
			 * Path the response data, for this case, the response data is:
			 * {"items":[{
			 * "statement":"CREATE TABLESPACE \"TBSPBG\" USING STOGROUP DSN8GB10 GBPCACHE NONE CCSID UNICODE MAXPARTITIONS 5 NUMPARTS 4 DSSIZE 2G MEMBER CLUSTER"
			 * }]}
			 * $.items[0].statement
			 * */
			if (responseData.getJSONArray("items").size() > 0) {

				String statement = JsonPath.read(responseData,
						"$.items[0].statement");
				Assert.assertEquals(
						statement,
						"CREATE TABLESPACE \"TBSPBG\" "
						+ "USING STOGROUP DSN8GB10 "
						+ "GBPCACHE NONE "
						+ "CCSID UNICODE "
						+ "MAXPARTITIONS 5 "
						+ "NUMPARTS 4 "
						+ "DSSIZE 2G "
						+ "MEMBER CLUSTER");
			}
			logger.info("PASSED: testCreatPBGTablespaceWithOptions executed successfully");
		} else {
			Assert.fail("testCreatPBGTablespaceWithOptions response data is null");
			logger.warn("ERROR: testCreatPBGTablespaceWithOptions response data is null");
		}
	}
	
	
	@Test(description = "create Range-partitioned tablespace DDL")
	private void testCreatPBRTablespaceWithOptions() {
		logger.info("Create Range-partitioned tablespace DDL");

		String queryJSON = alterService
				.getJSONData("getCreatePBRTablespace_ZOS");

		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.ALTER_OBJECT, queryJSON, dbProfile);
		// Get the response Data
		if (resObj != null) {
			JSONObject responseData = (JSONObject) resObj.get("ResponseData");
			Assert.assertNotNull(responseData);
			Assert.assertTrue(!"{}".equals(responseData.toString()),
					"responseData is empty!");
			
			/**
			 * Path the response data, for this case, the response data is:
			 * {"items":[{
			 * "statement":"CREATE TABLESPACE \"TBSPBR\" GBPCACHE ALL NUMPARTS 5 ( PARTITION 1 USING STOGROUP SYSDEFLT PRIQTY 3 SECQTY 6 ERASE YES PCTFREE 10,PARTITION 3 USING STOGROUP DSN8GB10 COMPRESS YES,PARTITION 5 USING VCAT DSNC000 TRACKMOD NO ) IN DSNDB04"
			 * }]}
			 * $.items[0].statement
			 * */
			if (responseData.getJSONArray("items").size() > 0) {

				String statement = JsonPath.read(responseData,
						"$.items[0].statement");
				Assert.assertEquals(
						statement,
						"CREATE TABLESPACE \"TBSPBR\" "
						+ "GBPCACHE ALL "
						+ "NUMPARTS 5 "
						+ "( PARTITION 1 USING STOGROUP SYSDEFLT PRIQTY 3 SECQTY 6 ERASE YES PCTFREE 10,"
						  + "PARTITION 3 USING STOGROUP DSN8GB10 COMPRESS YES,"
						  + "PARTITION 5 USING VCAT DSNC000 TRACKMOD NO ) "
						+ "IN DSNDB04");
			}
			logger.info("PASSED: testCreatPBRTablespaceWithOptions executed successfully");
		} else {
			Assert.fail("testCreatPBRTablespaceWithOptions response data is null");
			logger.warn("ERROR: testCreatPBRTablespaceWithOptions response data is null");
		}
	}
	
	@Test(description = "create Lob tablespace DDL")
	private void testCreatLobTablespaceWithOptions() {
		logger.info("Create Lob tablespace DDL");

		String queryJSON = alterService
				.getJSONData("getCreateLobTablespace_ZOS");

		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.ALTER_OBJECT, queryJSON, dbProfile);
		// Get the response Data
		if (resObj != null) {
			JSONObject responseData = (JSONObject) resObj.get("ResponseData");
			Assert.assertNotNull(responseData);
			Assert.assertTrue(!"{}".equals(responseData.toString()),
					"responseData is empty!");
			
			/**
			 * Path the response data, for this case, the response data is:
			 {"items":[{
			 "statement":"CREATE LOB TABLESPACE \"TBSLOB\" GBPCACHE SYSTEM DEFINE NO LOCKSIZE LOB CLOSE NO DSSIZE 64G NOT LOGGED IN DB1"
			 }]}
			 * $.items[0].statement
			 * */
			if (responseData.getJSONArray("items").size() > 0) {

				String statement = JsonPath.read(responseData,
						"$.items[0].statement");
				Assert.assertEquals(
						statement,
						"CREATE LOB TABLESPACE \"TBSLOB\" "
						+ "GBPCACHE SYSTEM "
						+ "DEFINE NO "
						+ "LOCKSIZE LOB "
						+ "CLOSE NO "
						+ "DSSIZE 64G "
						+ "NOT LOGGED "
						+ "IN DB1");
			}
			logger.info("PASSED: testCreatLobTablespaceWithOptions executed successfully");
		} else {
			Assert.fail("testCreatLobTablespaceWithOptions response data is null");
			logger.warn("ERROR: testCreatLobTablespaceWithOptions response data is null");
		}
	}

	/**
	 * Precondition for Alter table property
	 * */
	public void testAlterTableProperty_precondition()
	{
		sqlEditorService.runSQLinJDBC( "DROP TABLE TESTALT.T2;", ";", this.dbProfile, false );
		sqlEditorService.runSQLinJDBC( "CREATE TABLE TESTALT.T2 (C1 INTEGER);", ";", this.dbProfile, true );
	}

	/**
	 * Precondition for Alter table property
	 * */
	public void testAlterTableProperty_clean()
	{
		sqlEditorService.runSQLinJDBC( "DROP TABLE TESTALT.T2_NEW;", ";", this.dbProfile, false );
	}

	/**
	 * Test the alter table properties function in admin part. include: RENAME
	 * TABLE,DATA CAPTURE,AUDIT,VALIDPROC,APPEND.
	 * 
	 * @throws InterruptedException
	 * 
	 * */
	@Test (description = "Test the alter table properties action in admin part")
	public void testAlterTableProperties() throws InterruptedException
	{

		logger.info( "Running Alter Table properties test..." );

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"items":[{"statement":
		 * "RENAME TABLE \"TABTEST\".\"T1\" TO \"T1_NEW\"",
		 * "ALTER TABLE \"TABTEST\".\"T1_NEW\" DATA CAPTURE CHANGES APPEND YES AUDIT CHANGES VALIDPROC VALPROC"
		 * }]} To retrieve the statement, at first we look over if the length of
		 * items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */
		List<String> expected = new ArrayList<String>();
		expected.add( "RENAME TABLE \"TESTALT\".\"T2\" TO \"T2_NEW\"" );
		expected.add( "ALTER TABLE \"TESTALT\".\"T2_NEW\" DATA CAPTURE CHANGES APPEND YES AUDIT CHANGES VALIDPROC VALPROC" );

		assertEquals( "getAlterTableProperties_ZOS", expected );
		sqlEditorService.runSQLinJDBC( expected, ";", dbProfile, true );
		logger.info( "PASSED: Alter Table  properties executed successfully" );
	}

	/**
	 * Precondition for Alter table constraint
	 * */
	public void testAlterTableConstraint_precondition()
	{
		sqlEditorService.runSQLinJDBC( "DROP TABLE TESTALT.T2;", ";", this.dbProfile, false );
		sqlEditorService.runSQLinJDBC( "CREATE TABLE TESTALT.T2 (C1 INTEGER NOT NULL, C2 VARCHAR(5));", ";", this.dbProfile, true );
		sqlEditorService.runSQLinJDBC( "ALTER TABLE TESTALT.T2 ADD CONSTRAINT CONSTRAINT1 PRIMARY KEY(C1);", ";", this.dbProfile, true );
	}
	
	/**
	 * Precondition for Alter constraint
	 * */
	public void testAlterTableConstraint_clean()
	{
		sqlEditorService.runSQLinJDBC( "DROP TABLE TESTALT.T2;", ";", this.dbProfile, false );
	}
	
	/**
	 * Precondition for Alter table column(add and alter column)
	 * */
	public void testAlterTableColumn_precondition()
	{
		sqlEditorService.runSQLinJDBC( "DROP TABLE TESTALT.T1;", ";", this.dbProfile, false );
		sqlEditorService.runSQLinJDBC( "CREATE TABLE TESTALT.T1 (C1 INTEGER);", ";", this.dbProfile, true );
	}


	/**
	 * Precondition for Alter table add column
	 * */
	public void testAlterTableColumn_clean()
	{
		sqlEditorService.runSQLinJDBC( "DROP TABLE TESTALT.T1;", ";", this.dbProfile, false );
	}

	/**
	 * Test the alter table add column function in admin part.
	 * 
	 * @throws InterruptedException
	 * 
	 * */
	@Test (description = "Test the alter table add column in admin part")
	public void testAlterAddColumn() throws InterruptedException
	{

		logger.info( "Running Alter Table add column test..." );

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"items":[{"statement":
		 * "ALTER TABLE \"TESTALT\".\"T1\" ADD COLUMN \"C2\" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 100 CYCLE CACHE 30 ORDER ) IMPLICITLY HIDDEN ADD COLUMN \"C3\" TIMESTAMP NOT NULL GENERATED ALWAYS FOR EACH ROW ON UPDATE AS ROW CHANGE TIMESTAMP"
		 * }]} To retrieve the statement, at first we look over if the length of
		 * items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */
		String expected[] =
				{
					"ALTER TABLE \"TESTALT\".\"T1\" ADD COLUMN \"C3\" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 100 CYCLE CACHE 30 ORDER ) IMPLICITLY HIDDEN"
				};
		assertEquals( "getAlterTableAddColumn_ZOS", expected );
		sqlEditorService.runSQLinJDBC( expected[ 0 ], ";", dbProfile, true );
		logger.info( "PASSED: Alter Table  add column executed successfully" );
	}

	/**
	 * Test the alter table alter column function in admin part.
	 * 
	 * @throws InterruptedException
	 * 
	 * */
	@Test (description = "Test the alter table alter column in admin part")
	public void testAlterAlterColumn() throws InterruptedException
	{

		logger.info( "Running Alter Table alter column test..." );

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"items":[{"statement":
		 * "ALTER TABLE \"C12OP02\".\"ANNE_T1\" RENAME COLUMN \"C1\" TO \"C2\""
		 * }, {"statement":
		 * "ALTER TABLE \"C12OP02\".\"ANNE_T1\" ALTER COLUMN \"C2\" SET DATA TYPE DECIMAL(13, 2)"
		 * }, {"statement":
		 * "ALTER TABLE \"C12OP02\".\"ANNE_T1\" ALTER COLUMN \"C2\" SET NOT NULL"
		 * }, {"statement":
		 * "ALTER TABLE \"C12OP02\".\"ANNE_T1\" ALTER COLUMN \"C2\" SET DEFAULT 101"
		 * }, {"statement":
		 * "ALTER TABLE \"C12OP02\".\"ANNE_T1\" ALTER COLUMN \"C2\" SET IMPLICITLY HIDDEN"
		 * }]} To retrieve the statement, at first we look over if the length of
		 * items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */
		List<String> expected = new ArrayList<String>();
		expected.add( "ALTER TABLE \"TESTALT\".\"T1\" RENAME COLUMN \"C1\" TO \"C2\"" );
		expected.add( "ALTER TABLE \"TESTALT\".\"T1\" ALTER COLUMN \"C2\" SET DATA TYPE DECIMAL(13,2)" );
		expected.add( "ALTER TABLE \"TESTALT\".\"T1\" ALTER COLUMN \"C2\" SET DEFAULT 10" );
		assertEquals( "getAlterTableAlterColumn_ZOS", expected );
		sqlEditorService.runSQLinJDBC( expected, ";", dbProfile, true );
		logger.info( "PASSED: Alter Table  add column executed successfully" );
	}
		
	
	/**
	 * Test the create table add constraint action in admin part in z/OS
	 * */
	@Test(description = "Test create table add constraint action in admin part in z/OS")
	public void testCreateTableAddConstraint() throws InterruptedException {

		logger.info("Running Create Table add constraint initially...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"items":[{"statement":"CREATE TABLE \"DB2INST1\".\"TABLE\" ( \"NEW_COLUMN1\" CHAR(5), \"NEW_COLUMN2\" INTEGER )"},
		 * {"statement":"ALTER TABLE \"DB2INST1\".\"TABLE\" ADD CONSTRAINT \"NEW_CONSTRAINT1\" PRIMARY KEY (\"NEW_COLUMN1\" )"},
		 * {"statement":"ALTER TABLE \"DB2INST1\".\"TABLE\" ADD CONSTRAINT \"NEW_CONSTRAINT2\" CHECK NOT ENFORCED"},
		 * {"statement":"ALTER TABLE \"DB2INST1\".\"TABLE\" ADD CONSTRAINT \"NEW_CONSTRAINT3\" UNIQUE"},
		 * {"statement":"ALTER TABLE \"DB2INST1\".\"TABLE\" ADD CONSTRAINT \"NEW_CONSTRAINT4\" FOREIGN KEY (\"NEW_COLUMN1\" ) REFERENCES \"DB2INST1\".\"ACT\" NOT ENFORCED"}]}
		 *   
		 * To retrieve the statement, at first we look over if the length of
		 * items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 **/
		String expected[] = {
		"CREATE TABLE \"DB2INST1\".\"TABLE\" ( \"NEW_COLUMN1\" CHAR(5), \"NEW_COLUMN2\" INTEGER )",
		"ALTER TABLE \"DB2INST1\".\"TABLE\" ADD CONSTRAINT \"NEW_CONSTRAINT1\" PRIMARY KEY (\"NEW_COLUMN1\" )",
		"ALTER TABLE \"DB2INST1\".\"TABLE\" ADD CONSTRAINT \"NEW_CONSTRAINT2\" CHECK",
		"ALTER TABLE \"DB2INST1\".\"TABLE\" ADD CONSTRAINT \"NEW_CONSTRAINT3\" UNIQUE",
		"ALTER TABLE \"DB2INST1\".\"TABLE\" ADD CONSTRAINT \"NEW_CONSTRAINT4\" FOREIGN KEY (\"NEW_COLUMN1\" ) REFERENCES \"DB2INST1\".\"ACT\" NOT ENFORCED"};

		assertEquals("getCreateTableAddConstraint_ZOS", expected);

		logger.info("PASSED: Create table add constraint executed successfully");

	}
	
	
	/**
	 * Test the alter table constraints function in admin part in z/OS.
	 * include: DROP CONSTRAINT,  ADD CONSTRAINT
	 * */
	@Test(description = "Test the alter table constraints action in admin part in z/OS")
	public void testAlterTableConstraints() throws InterruptedException {

		logger.info("Running Alter Table constraints test in z/OS...");

		/**
		 * {"items":[{"statement":"ALTER TABLE \"DB2INST1\".\"ACT\" DROP CONSTRAINT \"PK_ACT\""},
		 * {"statement": "ALTER TABLE \"DB2INST1\".\"ACT\" ADD CONSTRAINT \"NEW_CONSTRAINT1\" UNIQUE (\"ACTKWD\" )" }]} 
		 * 
		 * To retrieve the statement, at first we look over if the length of items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */

		String expected[] = {
				"ALTER TABLE \"TESTALT\".\"T2\" DROP CONSTRAINT \"CONSTRAINT1\"",
				"ALTER TABLE \"TESTALT\".\"T2\" ADD CONSTRAINT \"NEW_CONSTRAINT1\" UNIQUE (\"C1\" )"};
		assertEquals("getAlterTableConstraints_ZOS", expected);
		
		logger.info("PASSED: Alter Table constraints executed successfully");
	}
	
	
	/**
	 * Test the drop table function in admin part in z/OS.
	 * 
	 * */
	@Test(description = "Test the drop table function in admin part in z/OS")
	public void testDropTable() throws InterruptedException {

		logger.info("Running drop table test in z/OS...");

		/**
		 * {"items":[{"statement":"DROP TABLE \"C12OP02\".\"A00002\""}]}
		 * 
		 * To retrieve the statement, at first we look over if the length of items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */

		String expected[] = {"DROP TABLE \"C12OP02\".\"A00002\""};
		
		assertEquals("getDropTable_ZOS", expected);
		
		logger.info("PASSED: Drop table executed successfully");
	}
	
	/**
	 * Test the drop tablespace function in admin part in z/OS.
	 * 
	 * */
	@Test(description = "Test the drop tablespace function in admin part in z/OS")
	public void testDropTablespace() throws InterruptedException {

		logger.info("Running drop tablespace test in z/OS...");

		/**
		 * {"items":[{"statement":"DROP TABLESPACE \"DSN00136\".\"A00002\""}]}
		 * 
		 * To retrieve the statement, at first we look over if the length of items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */

		String expected[] = {"DROP TABLESPACE \"DSN00136\".\"A00002\""};
		
		assertEquals("getDropTablespace_ZOS", expected);
		
		logger.info("PASSED: Drop tablespace executed successfully");
	}
	
	/**
	 * Test the drop index function in admin part in z/OS.
	 * 
	 * */
	@Test(description = "Test the drop index function in admin part in z/OS")
	public void testDropIndex() throws InterruptedException {

		logger.info("Running drop index test in z/OS...");

		/**
		 * {"items":[{"statement":"DROP INDEX \"C12OP02\".\"AAAAA0\""}]}
		 * 
		 * To retrieve the statement, at first we look over if the length of items array is correct, then we are looking for the first index of
		 * the items array -> the return is a object, we look for the value of
		 * key "statement", that is $.items[0].statement
		 * */

		String expected[] = {"DROP INDEX \"C12OP02\".\"AAAAA0\""};
		
		assertEquals("getDropIndex_ZOS", expected);
		
		logger.info("PASSED: Drop index executed successfully");
	}
	
}
