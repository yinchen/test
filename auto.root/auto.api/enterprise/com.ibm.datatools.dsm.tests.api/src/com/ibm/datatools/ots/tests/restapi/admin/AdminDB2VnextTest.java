package com.ibm.datatools.ots.tests.restapi.admin;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import org.testng.annotations.Test;


public class AdminDB2VnextTest extends AdminAlterTest
{
	private static final Logger logger = LogManager
			.getLogger(AdminDB2VnextTest.class);
	/**
	 * Test Binary Support For DB2V11
	 */
	@Test(description = "Select one database with version v11.1+, create table, BINARY VARBINARY TYPE are there")
	public void testBinarySupportForDB2V11() {

		logger.info("Running test Binary Support For DB2V11...");

		
		String query = alterService.getJSONData( "getColmnRow" );

		JSONObject resObj =alterService.callCreateDefinitionService(query, dbProfile );

		// Get the response Data
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );
		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "items" ) );
		JSONArray items = responseData.getJSONArray( "items" );
		Assert.assertNotNull( items );
		JSONObject columnDef = null;
		columnDef = (JSONObject) items.get(0);
		Assert.assertNotNull( columnDef );
		Assert.assertTrue(columnDef.containsKey("values"));
		JSONArray columnTypes = columnDef.getJSONArray( "values" );
		Assert.assertTrue(columnDef.toString().contains("\"label\":\"BINARY\""));
		Assert.assertTrue(columnDef.toString().contains("\"label\":\"VARBINARY\""));
		logger.info("PASSED: test Binary Support For DB2V11 successfully");
	}
	
}
