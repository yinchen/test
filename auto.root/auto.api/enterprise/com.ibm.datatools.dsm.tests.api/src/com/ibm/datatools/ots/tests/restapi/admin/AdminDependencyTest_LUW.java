package com.ibm.datatools.ots.tests.restapi.admin;

import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.AdminDependencyService;
import com.ibm.datatools.ots.tests.restapi.common.AdminSQLEditorService;
import com.jayway.jsonpath.JsonPath;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class AdminDependencyTest_LUW extends AdminTestBase
{
	private static final Logger logger = LogManager.getLogger( AdminDependencyTest_LUW.class );
	
	private AdminDependencyService dependencyService = null;
	private AdminSQLEditorService sqlEditorService = null ;
	
	@BeforeClass
	public void beforeTest()
	{
		dependencyService = new AdminDependencyService();
		sqlEditorService = new AdminSQLEditorService() ;
		createObjects_precondition();
	}
	
	@AfterClass
	public void afterTest(){
		dropObjects_clean(true);
	}
	
	private void createObjects_precondition()
	{
		dropObjects_clean(false);
		sqlEditorService.runSQLinJDBC(
				"create bufferpool BUFPOL immediate size 2000 pagesize 4K!"+ //sqlnum.1
		
				"create tablespace DEPTABSP PAGESIZE 4K bufferpool BUFPOL!"+ //sqlnum.2
				
				"create schema TESTDEPENDENCY!"+ // sqlnum.3
				"set current schema TESTDEPENDENCY!"+// sqlnum.4
				
				"create type TESTDEPENDENCY.KILOMETERS as integer with comparisons!"+// sqlnum.5
				
				"create table TESTDEPENDENCY.T1("+// sqlnum.6
				"C1  integer not null,"+
				"C2  char(5) not null,"+
				"C3  varchar(5),"+
				"C4  integer not null,"+
				"C5  TESTDEPENDENCY.KILOMETERS not null)"+ 
				"in DEPTABSP!"+
				
				"insert into TESTDEPENDENCY.T1 values(11,'11111','asf', 123, 111)!"+// sqlnum.7
				
				"Create unique index TESTDEPENDENCY.T1_UN on TESTDEPENDENCY.T1(C1)!"+// sqlnum.8
				
				"alter table TESTDEPENDENCY.T1 add constraint T1_PK primary key(C2)!"+// sqlnum.9
				
				"create view TESTDEPENDENCY.VIEW_T1(c1,c2,c5) as select c1,c2,c5 from TESTDEPENDENCY.T1!"+// sqlnum.10
				
				"create index TESTDEPENDENCY.INDEX_T1 on TESTDEPENDENCY.T1(C5)!"+ // sqlnum.11
				
				"create alias TESTDEPENDENCY.A_T1 for TESTDEPENDENCY.T1!"+// sqlnum.12
				
				"create trigger TESTDEPENDENCY.TRIGGER_T1_ALI "+ // sqlnum.13
				"AFTER INSERT ON TESTDEPENDENCY.T1 "+
				"FOR EACH STATEMENT MODE DB2SQL "+
				"select * from TESTDEPENDENCY.A_T1!"+
				
				"create table TESTDEPENDENCY.MQT_T1 (C1,C4,C5) "+// sqlnum.14
				"as (select C1,C4,C5 from TESTDEPENDENCY.T1) "+
				"data initially deferred refresh immediate "+
				"in DepTabSp!"+
				"set integrity for TESTDEPENDENCY.MQT_T1 immediate checked not incremental!"+ // sqlnum.15
				
				"create sequence TESTDEPENDENCY.SEQ_T1 start with 1 "+ // sqlnum.16
				"increment by 1 "+
				"no maxvalue "+
				"no cycle "+
				"cache 24!"+
				
				"insert into TESTDEPENDENCY.T1 values(next value for TESTDEPENDENCY.SEQ_T1,'sssss','aesf', 1234, 1121)!"+ // sqlnum.17
				
				"create trigger TESTDEPENDENCY.TRIGGER_T1_SEQ "+ // sqlnum.18
				"after insert on TESTDEPENDENCY.T1 "+
				"for each statement mode db2sql "+
				"update TESTDEPENDENCY.T1 set C4=next value for TESTDEPENDENCY.SEQ_T1!"+ 
				
				"create procedure TESTDEPENDENCY.PROCED_T1(in pro1 integer) "+ // sqlnum.19
				"language sql "+
				"specific TESTDEPENDENCY.PROCED_T2 "+
				"P1: BEGIN "+
				"declare A1  TESTDEPENDENCY.KILOMETERS; "+
				"Select C5 into A1 from TESTDEPENDENCY.T1 where C1=pro1; "+
				"END P1!"+
				
				"create function TESTDEPENDENCY.UDF_MQT(f1 integer) "+ // sqlnum.20
				"returns table(f1 integer) "+
				"language sql "+
				"reads sql data "+
				"no external action "+
				"SPECIFIC CHECKVIEW "+
				"return select c1 from TESTDEPENDENCY.MQT_T1 where c1=f1!", "!", this.dbProfile,true);
	}
	
	private void dropObjects_clean(boolean flag)
	{
		sqlEditorService.runSQLinJDBC(
		"Drop function TESTDEPENDENCY.UDF_MQT!"+
		"Drop procedure TESTDEPENDENCY.PROCED_T1!"+
		"Drop sequence TESTDEPENDENCY.SEQ_T1!"+
		"Drop table TESTDEPENDENCY.MQT_T1!"+
		"Drop trigger TESTDEPENDENCY.TRIGGER_T1_ALI!"+
		"Drop trigger TESTDEPENDENCY.TRIGGER_T1_SEQ!"+
		"Drop alias TESTDEPENDENCY.A_T1!"+
		"Drop view TESTDEPENDENCY.VIEW_T1!"+
		"drop index TESTDEPENDENCY.INDEX_T1!"+
		"Drop table TESTDEPENDENCY.T1!"+
		"Drop type TESTDEPENDENCY.KILOMETERS!"+
		"drop schema TESTDEPENDENCY restrict!"+ 
		"Drop tablespace DEPTABSP!"+
		"Drop bufferpool BUFPOL!","!", this.dbProfile,flag) ;
	}
	
	/**
	 * Test retrieve table dependency function
	 * */
	@Test (description = "Test retrieve table dependency function")
	public void testGetTableDependencyL() throws InterruptedException
	{
		logger.info( "Running Retrieve Table dependnecy..." );
		executeTest("GetLUWTableDependency", "GetLUWTableDependency_Result");
	}
	/**
	 * Test retrieve View dependency function
	 * */
	@Test (description = "Test retrieve view dependency function")
	public void testGetViewDependencyL() throws InterruptedException
	{
		logger.info( "Running Retrieve View dependnecy..." );
		executeTest("GetLUWViewDependency", "GetLUWViewDependency_Result");
	}	
	/**
	 * Test retrieve MQT dependency function
	 * */
	@Test (description = "Test retrieve MQT dependency function")
	public void testGetMQTDependencyL() throws InterruptedException
	{
		logger.info( "Running Retrieve MQT dependnecy..." );
		executeTest("GetLUWMQTDependency", "GetLUWMQTDependency_Result");
	}
	/**
	 * Test retrieve Alias dependency function
	 * */
	@Test (description = "Test retrieve Alias dependency function")
	public void testGetAliasDependencyL() throws InterruptedException
	{
		logger.info( "Running Retrieve Alias dependnecy..." );
		executeTest("GetLUWAliasDependency", "GetLUWAliasDependency_Result");
	}
	/**
	 * Test retrieve Stored Procedure dependency function
	 * */
	@Test (description = "Test retrieve Stored Procedure dependency function")
	public void testGetSPDependencyL() throws InterruptedException
	{
		logger.info( "Running Retrieve Stored Procedure dependnecy..." );
		executeTest("GetLUWSPDependency", "GetLUWSPDependency_Result");
	}
	/**
	 * Test retrieve UDF dependency function
	 * */
	@Test (description = "Test retrieve UDF dependency function")
	public void testGetUDFDependencyL() throws InterruptedException
	{
		logger.info( "Running Retrieve UDF dependnecy..." );
		executeTest("GetLUWUDFDependency", "GetLUWUDFDependency_Result");
	}
	/**
	 * Test retrieve Sequence dependency function
	 * */
	@Test (description = "Test retrieve Sequence dependency function")
	public void testGetSequenceDependencyL() throws InterruptedException
	{
		logger.info( "Running Retrieve Sequence dependnecy..." );
		executeTest("GetLUWSequenceDependency", "GetLUWSequenceDependency_Result");
	}
	/**
	 * Test retrieve Constraint dependency function
	 * */
	@Test (description = "Test retrieve Constraint dependency function")
	public void testGetConstraintDependencyL() throws InterruptedException
	{
		logger.info( "Running Retrieve Constraint dependnecy..." );
		executeTest("GetLUWConstraintDependencyPK", "GetLUWConstraintDependencyPK_Result");
	}
	/**
	 * Test retrieve Index dependency function
	 * */
	@Test (description = "Test retrieve Index dependency function")
	public void testGetIndexDependencyL() throws InterruptedException
	{
		logger.info( "Running Retrieve Index dependnecy..." );
		executeTest("GetLUWIndexDependency", "GetLUWIndexDependency_Result");
		//executeTest("GetLUWIndexDependencyPK", "GetLUWIndexDependencyPK_Result");
		//executeTest("GetLUWIndexDependencyUN", "GetLUWIndexDependencyUN_Result");
	}
	/**
	 * Test retrieve Table Space dependency function
	 * */
	@Test (description = "Test retrieve Table Space dependency function")
	public void testGetTablespaceDependencyL() throws InterruptedException
	{
		logger.info( "Running Retrieve Table Space dependnecy..." );
		executeTest("GetLUWTablespaceDependency", "GetLUWTablespaceDependency_Result");
	}
	/**
	 * Test retrieve buffer pool dependency function
	 * */
	@Test (description = "Test retrieve buffer pool dependency function")
	public void testGetBufferpoolDependencyL() throws InterruptedException
	{
		logger.info( "Running Retrieve buffer pool dependnecy..." );
		executeTest("GetLUWBufferpoolDependency", "GetLUWBufferpoolDependency_Result");
	}
	/**
	 * Test retrieve Trigger dependency function
	 * */
	@Test (description = "Test retrieve Trigger dependency function")
	public void testGetTriggerDependencyL() throws InterruptedException
	{
		logger.info( "Running Retrieve Trigger dependnecy..." );
		executeTest("GetLUWTriggerDependencyOnSeq", "GetLUWTriggerDependencyOnSeq_Result");
		//executeTest("GetLUWTriggerDependencyOnAlias", "GetLUWTriggerDependencyOnAlias_Result");
	}
	/**
	 * Test retrieve package dependency function,because the package is generated by system,so first need get the random id.
	 * */
	@Test (description = "Test retrieve Package dependency function")
	public void testGetPackageDependencyL() throws InterruptedException
	{
		logger.info( "Running Retrieve Package dependnecy..." );
		executeTest("GetLUWPackageDependency", "GetLUWPackageDependency_Result");
	}
	/**
	 * Test retrieve UDT dependency function
	 * */
	@Test (description = "Test retrieve UDT dependency function")
	public void testGetUDTDependencyL() throws InterruptedException
	{
		logger.info( "Running Retrieve UDT dependnecy..." );
		executeTest("GetLUWUDTDependency", "GetLUWUDTDependency_Result");
	}
	
	
	private void executeTest(String property, String baseProperty){
		String query = dependencyService.getJSONData( property );
		JSONObject resObj =null;
		if(property.equals("GetLUWPackageDependency")){
			JSONObject[] rdata=sqlEditorService.runSQLinJDBC("select * from syscat.packagedep where pkgschema='TESTDEPENDENCY' and btype='T'!","!", this.dbProfile);
			JSONObject respdata=rdata[0];
			JSONArray ja1= respdata.getJSONArray("data");
			JSONObject jo1 = (JSONObject)ja1.get(0);
			JSONArray ja2=jo1.getJSONArray("items");
			JSONObject jo2=ja2.getJSONObject(0);
			String pkgname =jo2.getString("c1");
			 resObj =
					dependencyService.callDependencyServiceForPackage( AdminDependencyService.GET_DEPENDENCY, query, dbProfile, pkgname);
		}
		else{
			 resObj =
				dependencyService.callDependencyService( AdminDependencyService.GET_DEPENDENCY, query, dbProfile );}
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );
		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		JSONObject propResult = JsonPath.read( responseData, "$" );
		String base = dependencyService.getJSONData( baseProperty );
		JSONObject propBase = JSONObject.fromObject( base );
		logger.info(propResult.toString());
		if(propResult.containsKey("PACKAGE")){
			JSONArray jArray=propResult.getJSONArray("PACKAGE");
			jArray.discard(0);
			jArray.add(0, "{\"up\": true,\"id\": \"TESTDEPENDENCY.P\"}");
		}
		@SuppressWarnings("rawtypes") 
		Iterator its = propBase.entrySet().iterator();
		while ( its.hasNext() )
		{
			@SuppressWarnings("rawtypes") 
			Entry entry = (Entry)its.next();
			String key = (String)entry.getKey();
			if ( propResult.containsKey( key ) )
			{ 			
				JSONArray baseArray = (JSONArray)propBase.get( key );
				JSONArray resultArray = (JSONArray)propResult.get( key );
				Assert.assertTrue(resultArray.containsAll(baseArray));
			}
		}
//		Assert.assertTrue( compareJsons( propBase, propResult ) );
	}
}
