package com.ibm.datatools.ots.tests.restapi.admin;

import java.util.Iterator;
import java.util.Map.Entry;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.AdminDependencyService;
import com.ibm.datatools.ots.tests.restapi.common.AdminSQLEditorService;
import com.jayway.jsonpath.JsonPath;

public class AdminDependencyTest_ZOS extends AdminTestBase
{
	private static final Logger logger = LogManager.getLogger( AdminDependencyTest_ZOS.class );

	private AdminDependencyService dependencyService = null;
	private AdminSQLEditorService sqlEditorService = null ;

	@BeforeClass
	public void beforeTest()
	{
		dependencyService = new AdminDependencyService();
		sqlEditorService = new AdminSQLEditorService() ;
		dropObjects();
		createObjects();
	}
	
	@AfterClass
	public void afterTest(){
		dropObjects();
	}
	
	private void createObjects()
	{
		sqlEditorService.runSQLinJDBC(
				
				"GRANT SYSADM to DEPTEST!" +
				"SET CURRENT SQLID = 'DEPTEST'!" +
				
				"CREATE DATABASE DEP STOGROUP SYSDEFLT CCSID EBCDIC!" +
		
				"CREATE TABLESPACE TBLSP1 " +
				"IN DEP " +
				"USING STOGROUP SYSDEFLT " +
				"PRIQTY -1 " +
				"SECQTY -1 " +
				"CCSID EBCDIC " +
				"LOCKSIZE ANY " +
				"MAXROWS 255!" +
				
				"CREATE SEQUENCE DEPTEST.SEQ START WITH 1 " +
				"INCREMENT BY 1 " +
				"NO MAXVALUE " +
				"NO CYCLE " +
				"CACHE 24!" +
				
				"CREATE TABLE DEPTEST.T1 ( " +
				"C1 CHAR(5) NOT NULL, " +
				"C2 INTEGER NOT NULL, " +
				"C3 VARCHAR(5) NOT NULL, " +
				"C4 CHAR(5)) " +
				"IN DEP.TBLSP1 " +
				"AUDIT NONE " +
				"DATA CAPTURE NONE " +
				"CCSID EBCDIC!" +
				
				"CREATE TABLE DEPTEST.T2 (" +
				"C1 CHAR(5), " +
				"C2 CHAR(5) NOT NULL, " +
				"C3 CHAR(5), " +
				"C4 INTEGER) " +
				"IN DEP.TBLSP1 " +
				"AUDIT NONE " +
				"DATA CAPTURE NONE " +
				"CCSID EBCDIC!" +
				
				"CREATE TABLE DEPTEST.T3 (" +
				"C1 CHAR(5), " +
				"C2 CHAR(5) NOT NULL, " +
				"C3 CHAR(5), " +
				"C4 INTEGER NOT NULL, " +
				"C5 VARCHAR(5) NOT NULL) " +
				"IN DEP.TBLSP1 " +
				"AUDIT NONE " +
				"DATA CAPTURE NONE " +
				"CCSID EBCDIC!" +
				
				"CREATE UNIQUE INDEX DEPTEST.T1_PK ON DEPTEST.T1 (C1)!" +
				"ALTER TABLE DEPTEST.T1 ADD CONSTRAINT T1_PK PRIMARY KEY (C1)!" +
				
				"CREATE UNIQUE INDEX DEPTEST.T1_UN ON DEPTEST.T1 " +
				"(C2 ASC, C3 ASC)" + 
				"NOT CLUSTER " +
				"NOT PADDED	" +
				"CLOSE NO!" +
				"ALTER TABLE DEPTEST.T1 ADD CONSTRAINT T1_UN UNIQUE (C2, C3)!" +
				
				"ALTER TABLE DEPTEST.T2 ADD CONSTRAINT T2_T1_FK FOREIGN KEY " +
				"(C2) REFERENCES DEPTEST.T1 (C1) " +
				"ON DELETE CASCADE!" +
				
				"ALTER TABLE DEPTEST.T3 ADD CONSTRAINT T3_T1_FK FOREIGN KEY " +
				"(C4, C5) REFERENCES DEPTEST.T1 (C2,C3) " +
				"ON DELETE CASCADE!" +
				
				"ALTER TABLE DEPTEST.T1 ADD CONSTRAINT T1_CK CHECK (C2>5)!" +
				
				"CREATE VIEW DEPTEST.V_T (C1, C2) AS SELECT C1, C2 FROM DEPTEST.T1!" +
				
				"CREATE VIEW DEPTEST.V_V (C1, C2) AS SELECT C1, C2 FROM DEPTEST.V_T!" +
				
				"CREATE ALIAS DEPTEST.A_T FOR DEPTEST.T1!" +
				
				"CREATE ALIAS DEPTEST.A_V FOR DEPTEST.V_T!" +
				
				"CREATE TABLE DEPTEST.MQT_T (C3, C4 ) " +
						"AS (SELECT C3, C4 FROM DEPTEST.T1 ) " +
								"DATA INITIALLY DEFERRED REFRESH DEFERRED " +
								"ENABLE QUERY OPTIMIZATION " +
								"MAINTAINED BY SYSTEM " +
								"IN DEP.TBLSP1!" +
								
				"CREATE TABLE DEPTEST.MQT_V (C3, C4 ) " +
				"AS (SELECT C1, C2  FROM DEPTEST.V_T ) " +
				"DATA INITIALLY DEFERRED REFRESH DEFERRED " +
				"ENABLE QUERY OPTIMIZATION " +
				"MAINTAINED BY SYSTEM IN DEP.TBLSP1!" +
								
				"CREATE ALIAS DEPTEST.A_MQT FOR DEPTEST.MQT_T!" +
				
				"CREATE VIEW DEPTEST.V_MQT (C1, C2) AS SELECT C3, C4 FROM DEPTEST.MQT_T!" +
				
				"CREATE TRIGGER DEPTEST.TRIGGER_T_MQT " +
				"AFTER INSERT ON DEPTEST.T1 " +
				"FOR EACH STATEMENT MODE DB2SQL " +
				"NOT SECURED " +
				"SELECT * FROM DEPTEST.MQT_T!" +
				
				"CREATE TRIGGER DEPTEST.TRIGGER_T_A " +
				"AFTER INSERT ON DEPTEST.T1 " +
				"FOR EACH STATEMENT MODE DB2SQL " +
				"NOT SECURED " +
				"SELECT * FROM DEPTEST.A_T!" +
				
				"CREATE TRIGGER DEPTEST.TRIGGER_T_V " +
				"AFTER INSERT ON DEPTEST.T1 " +
				"FOR EACH STATEMENT MODE DB2SQL " +
				"NOT SECURED " +
				"UPDATE DEPTEST.V_T SET C2 = C2 + 1!" +
				
				"CREATE TRIGGER DEPTEST.TRIGGER_SEQ_T " +
				"AFTER INSERT ON DEPTEST.T1 " +
				"FOR EACH STATEMENT MODE DB2SQL " +
				"NOT SECURED " +
				"BEGIN ATOMIC" +
				"	UPDATE DEPTEST.T1 SET C2 = NEXT VALUE FOR DEPTEST.SEQ; " +
				"END!" +
				
				"CREATE PROCEDURE DEPTEST.SP1_T (VARNAME VARCHAR(128), OUT VARCOUNT INTEGER) " +
				"VERSION V1 " +
				"ISOLATION LEVEL CS " +
				"LANGUAGE SQL " +
				"ALLOW DEBUG MODE " +
				"WLM ENVIRONMENT FOR DEBUG MODE WLMENV1	" +
				"P1: BEGIN " +
				"	SELECT COUNT(*) INTO VARCOUNT FROM DEPTEST.T1 " +
				"	WHERE C1 = 'DEPTEST' AND C3 LIKE VARNAME;" +
				"END P1!" +
				
				"CREATE PROCEDURE DEPTEST.SP2_T ( ) " +
				"VERSION V1 " +
				"ISOLATION LEVEL CS " +
				"RESULT SETS 1 " +
				"LANGUAGE SQL " +
				"ALLOW DEBUG MODE " +
				"WLM ENVIRONMENT FOR DEBUG MODE WLMENV1 " +
				"P1: BEGIN " +
				"DECLARE CURSOR1 CURSOR WITH RETURN FOR " +
				"SELECT NAME FROM DEPTEST.T1 WHERE C1 = 'C12OP02' ORDER BY C2; " +
				"OPEN CURSOR1; " +
				"END P1!" +
				
				"CREATE PROCEDURE DEPTEST.SP_A (VARNAME VARCHAR(128), OUT VARCOUNT INTEGER) " +
				"VERSION V1 " +
				"ISOLATION LEVEL CS " +
				"LANGUAGE SQL " +
				"ALLOW DEBUG MODE " +
				"WLM ENVIRONMENT FOR DEBUG MODE WLMENV1 " +
				"P1: BEGIN " +
				"SELECT COUNT(*) INTO VARCOUNT FROM DEPTEST.A_T WHERE C3 = 'DEPTEST'; " +
				"END P1!" +
				
				"CREATE PROCEDURE DEPTEST.SP_V (VARNAME VARCHAR(128), OUT VARCOUNT INTEGER) " +
				"VERSION V1 " +
				"ISOLATION LEVEL CS " +
				"LANGUAGE SQL " +
				"ALLOW DEBUG MODE " +
				"WLM ENVIRONMENT FOR DEBUG MODE WLMENV1 " +
				"P1: BEGIN" +
				"	SELECT COUNT(*) INTO VARCOUNT FROM DEPTEST.V_T WHERE C1 = 'DEPTEST';" +
				"END P1!" +
				
				"CREATE PROCEDURE DEPTEST.SP_MQT (VARNAME VARCHAR(128), OUT VARCOUNT INTEGER) " +
				"VERSION V1 " +
				"ISOLATION LEVEL CS " +
				"LANGUAGE SQL " +
				"ALLOW DEBUG MODE " +
				"WLM ENVIRONMENT FOR DEBUG MODE WLMENV1 " +
				"P1: BEGIN " +
				"	SELECT COUNT(*) INTO VARCOUNT FROM DEPTEST.MQT_T WHERE  " +
				"	C3 = 'DEPTEST'; " +
				"END P1!" +
				
				"CREATE PROCEDURE DEPTEST.SP_SEQ (VARNAME VARCHAR(128), OUT VARCOUNT INTEGER) " +
				"VERSION V1 " +
				"ISOLATION LEVEL CS " +
				"LANGUAGE SQL " +
				"ALLOW DEBUG MODE " +
				"WLM ENVIRONMENT FOR DEBUG MODE WLMENV1 " +
				"P1: BEGIN" +
				"	VALUES NEXT VALUE FOR DEPTEST.SEQ INTO VARCOUNT;" +
				"END P1!" +

				"CREATE PROCEDURE DEPTEST.SP_SP (VARNAME VARCHAR(128), OUT VARCOUNT INTEGER) " +
				"VERSION V1 " +
				"ISOLATION LEVEL CS " +
				"LANGUAGE SQL " +
				"ALLOW DEBUG MODE " +
				"WLM ENVIRONMENT FOR DEBUG MODE WLMENV1 " +
				"P1: BEGIN" +
				"	CALL DEPTEST.SP1_T('s', VARCOUNT);" +
				"END P1!" +
				
				"CREATE FUNCTION DEPTEST.UDF_V (VARNAME VARCHAR(128)) " +
				"RETURNS TABLE (NAME VARCHAR(128)) " +
				"LANGUAGE SQL " +
				"READS SQL DATA " +
				"NO EXTERNAL ACTION " +
				"DETERMINISTIC " +
				"RETURN SELECT C1 FROM DEPTEST.V_T WHERE C1 = 'C12OP02'!" +
				
				"CREATE FUNCTION DEPTEST.UDF_MQT (VARNAME VARCHAR(128)) " +
				"RETURNS TABLE (NAME VARCHAR(128)) " +
				"LANGUAGE SQL " +
				"READS SQL DATA " +
				"NO EXTERNAL ACTION " +
				"DETERMINISTIC " +
				"RETURN SELECT C3 FROM DEPTEST.MQT_T WHERE C3 = VARNAME!" +
				
				"CREATE FUNCTION DEPTEST.UDF_T( ) " +
				"RETURNS INTEGER " +
				"VERSION V1 " +
				"DETERMINISTIC " +
				"NO EXTERNAL ACTION " +
				"WLM ENVIRONMENT FOR DEBUG MODE WLMENV1 " +
				"BEGIN " +
				"	DECLARE VARCOUNT INTEGER; " +
				"	SELECT COUNT(*) INTO VARCOUNT FROM DEPTEST.T1 WHERE C1 = ''; " +
				"	RETURN  VARCOUNT; " +
				"END!" +
				
				"CREATE FUNCTION DEPTEST.UDF_A (VARNAME VARCHAR(128)) " +
				"RETURNS TABLE (NAME VARCHAR(128)) " +
				"LANGUAGE SQL " +
				"READS SQL DATA " +
				"NO EXTERNAL ACTION " +
				"DETERMINISTIC " +
				"RETURN SELECT C3 FROM DEPTEST.A_T WHERE C3 = VARNAME!" +
				
				"CREATE FUNCTION DEPTEST.UDF_UDF( ) " +
				"RETURNS INTEGER " +
				"VERSION V1 " +
				"DETERMINISTIC " +
				"NO EXTERNAL ACTION " +
				"WLM ENVIRONMENT FOR DEBUG MODE WLMENV1 " +
				"BEGIN" +
				"	DECLARE VARCOUNT INTEGER;" +
				"	SELECT COUNT(*) INTO VARCOUNT FROM DEPTEST.T1 WHERE C2 = DEPTEST.UDF_T();" +
				"	RETURN  VARCOUNT;" +
				"END!" +

				"CREATE FUNCTION DEPTEST.UDF_SP( ) " +
				"RETURNS INTEGER " +
				"VERSION V1 " +
				"DETERMINISTIC " +
				"NO EXTERNAL ACTION " +
				"WLM ENVIRONMENT FOR DEBUG MODE WLMENV1 " +
				"BEGIN " +
				"	DECLARE VARCOUNT INTEGER; " +
				"	CALL DEPTEST.SP1_T('s', VARCOUNT); " +
				"RETURN  VARCOUNT;" +
				"END!" +
				
				"CREATE FUNCTION DEPTEST.UDF_SEQ( ) " +
				"RETURNS INTEGER " +
				"VERSION V1 " +
				"DETERMINISTIC " +
				"NO EXTERNAL ACTION " +
				"WLM ENVIRONMENT FOR DEBUG MODE WLMENV1 " +
				"BEGIN " +
				"	DECLARE VARCOUNT INTEGER;" +
				"	VALUES NEXT VALUE FOR DEPTEST.SEQ INTO VARCOUNT;" +
				"	RETURN  VARCOUNT;" +
				"END!" +
				
				"CREATE TABLE DEPTEST.MQT_UDF (NAME) " +
				"AS (SELECT C1 FROM DEPTEST.T1 WHERE C2 = DEPTEST.UDF_T()) " +
				"DATA INITIALLY DEFERRED REFRESH DEFERRED " +
				"ENABLE QUERY OPTIMIZATION " +
				"MAINTAINED BY SYSTEM " +
				"IN DEP.TBLSP1!" +
				
				"CREATE VIEW DEPTEST.V_UDF AS SELECT NAME FROM TABLE (DEPTEST.UDF_MQT('s')) AS T!" +
				
				"CREATE TRIGGER DEPTEST.TRIGGER_UDF_T_T " +
				"AFTER INSERT ON DEPTEST.T1 " +
				"FOR EACH STATEMENT MODE DB2SQL " +
				"NOT SECURED " +
				"BEGIN ATOMIC " +
				"SELECT COUNT(*) FROM DEPTEST.T2 WHERE C2 = DEPTEST.UDF_T();" +
				"END!" +
				
				"CREATE TRIGGER DEPTEST.TRIGGER_SP " +
				"AFTER INSERT ON DEPTEST.T1 " +
				"FOR EACH STATEMENT MODE DB2SQL " +
				"NOT SECURED " +
				"BEGIN ATOMIC" +
				"	CALL DEPTEST.SP2_T();" +
				"END!" +
				
				"CREATE PROCEDURE DEPTEST.SP_UDF_T (VARNAME VARCHAR(128), OUT VARCOUNT INTEGER) " +
				"VERSION V1 " +
				"ISOLATION LEVEL CS " +
				"LANGUAGE SQL " +
				"ALLOW DEBUG MODE " +
				"WLM ENVIRONMENT FOR DEBUG MODE WLMENV1 " +
				"P1: BEGIN" +
				"	SELECT COUNT(*) INTO VARCOUNT FROM DEPTEST.T1 WHERE C2 = DEPTEST.UDF_T();" +
				"END P1!" +
				
				"SET CURRENT SCHEMA = 'DEPTEST'!" +
				
				"CREATE SYNONYM SYNONYM_V FOR DEPTEST.V_T!" +
				
				"CREATE SYNONYM SYNONYM_M FOR DEPTEST.MQT_T!" +
				
				"CREATE SYNONYM SYNONYM_T FOR DEPTEST.T1!" +
				
				"CREATE TRIGGER DEPTEST.TRIGGER_SYNONYM_T " +
				"AFTER INSERT ON DEPTEST.T1 " +
				"FOR EACH STATEMENT MODE DB2SQL " +
				"NOT SECURED " +
				"BEGIN ATOMIC " +
				"SELECT COUNT(*) FROM SYNONYM_T WHERE C2 = 1;" +
				"END!", "!", this.dbProfile) ;
	}
	
	private void dropObjects()
	{
		sqlEditorService.runSQLinJDBC(
				"DROP TRIGGER DEPTEST.TRIGGER_SP;" +
				"DROP FUNCTION DEPTEST.UDF_SP;" +
				"DROP PROCEDURE DEPTEST.SP_SP;" +
				"DROP PROCEDURE DEPTEST.SP_UDF_T;" +
				"DROP PROCEDURE DEPTEST.SP_A;" +
				"DROP PROCEDURE DEPTEST.SP_V;" +
				"DROP PROCEDURE DEPTEST.SP_MQT;" +
				"DROP PROCEDURE DEPTEST.SP1_T;" +
				"DROP PROCEDURE DEPTEST.SP2_T;" +
				"DROP PROCEDURE DEPTEST.SP_SEQ;" +
				"DROP TABLE DEPTEST.MQT_UDF;" +		
				"DROP VIEW DEPTEST.V_UDF;" +
				"DROP VIEW DEPTEST.V_MQT;" +
				"DROP TRIGGER DEPTEST.TRIGGER_UDF_T_T;" +
				"DROP TRIGGER DEPTEST.TRIGGER_SEQ_T;" +
				"DROP FUNCTION DEPTEST.UDF_UDF;" +
				"DROP FUNCTION DEPTEST.UDF_MQT;" +
				"DROP FUNCTION DEPTEST.UDF_SEQ;" +
				"DROP FUNCTION DEPTEST.UDF_V;" +
				"DROP FUNCTION DEPTEST.UDF_T;" +
				"DROP FUNCTION DEPTEST.UDF_A;" +
				"DROP TRIGGER DEPTEST.TRIGGER_SYNONYM_T" +
				"SET CURRENT SCHEMA = 'DEPTEST';" +
				"DROP SYNONYM SYNONYM_V;" +
				"DROP SYNONYM SYNONYM_M;" +
				"DROP SYNONYM SYNONYM_T;" +
				"DROP TRIGGER DEPTEST.TRIGGER_T_A;" +
				"DROP TRIGGER DEPTEST.TRIGGER_T_V;" +
				"DROP TRIGGER DEPTEST.TRIGGER_T_MQT;" +
				"DROP ALIAS DEPTEST.A_T;" +
				"DROP ALIAS DEPTEST.A_V;" +
				"DROP ALIAS DEPTEST.A_MQT;" +
				"DROP TABLE DEPTEST.MQT_V;" +
				"DROP VIEW DEPTEST.V_V;" +
				"DROP VIEW DEPTEST.V_T;" +
				"DROP TABLE DEPTEST.MQT_T;" +
				"DROP SEQUENCE DEPTEST.SEQ;" +
				"DROP TABLE DEPTEST.T1;" +
				"DROP TABLE DEPTEST.T2;" +
				"DROP TABLE DEPTEST.T3;" +
				"DRPO TABLESPACE TBLSP1;" +
				"DROP DATABASE DEP;",";", this.dbProfile) ;
	}

	/**
	 * Test retrieve table dependency function
	 * */
	@Test (description = "Test retrieve table dependency function")
	public void testGetTableDependency() throws InterruptedException
	{
		logger.info( "Running Retrieve Table dependnecy..." );
		executeTest("GetZOSTableDependency", "GetZOSTableDependency_Result");
		executeTest("GetZOSTableDependencyFKOnPK", "GetZOSTableDependencyFKOnPK_Result");
		executeTest("GetZOSTableDependencyFKOnNonPK", "GetZOSTableDependencyFKOnNonPK_Result");
	}
	
	/**
	 * Test retrieve View dependency function
	 * */
	@Test (description = "Test retrieve view dependency function")
	public void testGetViewDependency() throws InterruptedException
	{
		logger.info( "Running Retrieve View dependnecy..." );
		executeTest("GetZOSViewDependency", "GetZOSViewDependency_Result");
		executeTest("GetZOSViewDependencyOnView", "GetZOSViewDependencyOnView_Result");
		executeTest("GetZOSViewDependencyOnUDF", "GetZOSViewDependencyOnUDF_Result");
		executeTest("GetZOSViewDependencyOnMQT", "GetZOSViewDependencyOnMQT_Result");
	}
	
	/**
	 * Test retrieve MQT dependency function
	 * */
	@Test (description = "Test retrieve MQT dependency function")
	public void testGetMQTDependency() throws InterruptedException
	{
		logger.info( "Running Retrieve MQT dependnecy..." );
		executeTest("GetZOSMQTDependency", "GetZOSMQTDependency_Result");
		executeTest("GetZOSMQTDependencyOnView", "GetZOSMQTDependencyOnView_Result");
		executeTest("GetZOSMQTDependencyOnUDF", "GetZOSMQTDependencyOnUDF_Result");
	}
	
	/**
	 * Test retrieve Alias dependency function
	 * */
	@Test (description = "Test retrieve Alias dependency function")
	public void testGetAliasDependency() throws InterruptedException
	{
		logger.info( "Running Retrieve Alias dependnecy..." );
		executeTest("GetZOSAliasDependency", "GetZOSAliasDependency_Result");
		executeTest("GetZOSAliasDependencyOnView", "GetZOSAliasDependencyOnView_Result");
		executeTest("GetZOSAliasDependencyOnMQT", "GetZOSAliasDependencyOnMQT_Result");
	}
	
	/**
	 * Test retrieve Stored Procedure dependency function
	 * */
	@Test (description = "Test retrieve Stored Procedure dependency function")
	public void testGetSPDependency() throws InterruptedException
	{
		logger.info( "Running Retrieve Stored Procedure dependnecy..." );
		executeTest("GetZOSSPDependency", "GetZOSSPDependency_Result");
		executeTest("GetZOSSPDependencyOnAlias", "GetZOSSPDependencyOnAlias_Result");
		executeTest("GetZOSSPDependencyOnMQT", "GetZOSSPDependencyOnMQT_Result");
		executeTest("GetZOSSPDependencyOnView", "GetZOSSPDependencyOnView_Result");
		executeTest("GetZOSSPDependencyOnSeq", "GetZOSSPDependencyOnSeq_Result");
		executeTest("GetZOSSPDependencyOnSP", "GetZOSSPDependencyOnSP_Result");
		executeTest("GetZOSSPDependencyOnUDF", "GetZOSSPDependencyOnUDF_Result");
	}
	
	/**
	 * Test retrieve UDF dependency function
	 * */
	@Test (description = "Test retrieve UDF dependency function")
	public void testGetUDFDependency() throws InterruptedException
	{
		logger.info( "Running Retrieve UDF dependnecy..." );
		executeTest("GetZOSUDFDependency", "GetZOSUDFDependency_Result");
		executeTest("GetZOSUDFDependencyOnAlias", "GetZOSUDFDependencyOnAlias_Result");
		executeTest("GetZOSUDFDependencyOnMQT", "GetZOSUDFDependencyOnMQT_Result");
		executeTest("GetZOSUDFDependencyOnView", "GetZOSUDFDependencyOnView_Result");
		executeTest("GetZOSUDFDependencyOnSeq", "GetZOSUDFDependencyOnSeq_Result");
		executeTest("GetZOSUDFDependencyOnSP", "GetZOSUDFDependencyOnSP_Result");
		executeTest("GetZOSUDFDependencyOnUDF", "GetZOSUDFDependencyOnUDF_Result");
	}
	
	/**
	 * Test retrieve Sequence dependency function
	 * */
	@Test (description = "Test retrieve Sequence dependency function")
	public void testGetSequenceDependency() throws InterruptedException
	{
		logger.info( "Running Retrieve Sequence dependnecy..." );
		executeTest("GetZOSSequenceDependency", "GetZOSSequenceDependency_Result");
	}
	
	/**
	 * Test retrieve Synonym dependency function
	 * */
	@Test (description = "Test retrieve Synonym dependency function")
	public void testGetSynonymDependency() throws InterruptedException
	{
		logger.info( "Running Retrieve Synonym dependnecy..." );
		executeTest("GetZOSSynonymDependency", "GetZOSSynonymDependency_Result");
		executeTest("GetZOSSynonymDependencyOnMQT", "GetZOSSynonymDependencyOnMQT_Result");
		executeTest("GetZOSSynonymDependencyOnView", "GetZOSSynonymDependencyOnView_Result");
	}
	
	/**
	 * Test retrieve Constraint dependency function
	 * */
	@Test (description = "Test retrieve Constraint dependency function")
	public void testGetConstraintDependency() throws InterruptedException
	{
		logger.info( "Running Retrieve Constraint dependnecy..." );
		executeTest("GetZOSConstraintDependencyPK", "GetZOSConstraintDependencyPK_Result");
		executeTest("GetZOSConstraintDependencyFK", "GetZOSConstraintDependencyFK_Result");
		executeTest("GetZOSConstraintDependencyUK", "GetZOSConstraintDependencyUK_Result");
		executeTest("GetZOSConstraintDependencyCheck", "GetZOSConstraintDependencyCheck_Result");
	}
	
	/**
	 * Test retrieve Index dependency function
	 * */
	@Test (description = "Test retrieve Index dependency function")
	public void testGetIndexDependency() throws InterruptedException
	{
		logger.info( "Running Retrieve Index dependnecy..." );
		executeTest("GetZOSIndexDependency", "GetZOSIndexDependency_Result");
	}
	
	/**
	 * Test retrieve Table Space dependency function
	 * */
	@Test (description = "Test retrieve Table Space dependency function")
	public void testGetTablespaceDependency() throws InterruptedException
	{
		logger.info( "Running Retrieve Table Space dependnecy..." );
		executeTest("GetZOSTablespaceDependency", "GetZOSTablespaceDependency_Result");
	}
	
	/**
	 * Test retrieve Database dependency function
	 * */
	@Test (description = "Test retrieve Database dependency function")
	public void testGetDatabaseDependency() throws InterruptedException
	{
		logger.info( "Running Retrieve Database dependnecy..." );
		executeTest("GetZOSDatabaseDependency", "GetZOSDatabaseDependency_Result");
	}
	
	/**
	 * Test retrieve Trigger dependency function
	 * */
	@Test (description = "Test retrieve Trigger dependency function")
	public void testGetTriggerDependency() throws InterruptedException
	{
		logger.info( "Running Retrieve Trigger dependnecy..." );
		executeTest("GetZOSTriggerDependency", "GetZOSTriggerDependency_Result");
		executeTest("GetZOSTriggerDependencyOnMQT", "GetZOSTriggerDependencyOnMQT_Result");
		executeTest("GetZOSTriggerDependencyOnView", "GetZOSTriggerDependencyOnView_Result");
		executeTest("GetZOSTriggerDependencyOnAlias", "GetZOSTriggerDependencyOnAlias_Result");
		executeTest("GetZOSTriggerDependencyOnSeq", "GetZOSTriggerDependencyOnSeq_Result");
		executeTest("GetZOSTriggerDependencyOnSynonym", "GetZOSTriggerDependencyOnSynonym_Result");
		executeTest("GetZOSTriggerDependencyOnSP", "GetZOSTriggerDependencyOnSP_Result");
		
	}
	
	
	private void executeTest(String property, String baseProperty){
		String query = dependencyService.getJSONData( property );
		JSONObject resObj =
				dependencyService.callDependencyService( AdminDependencyService.GET_DEPENDENCY, query, dbProfile );
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );
		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		JSONObject propResult = JsonPath.read( responseData, "$" );
		String base = dependencyService.getJSONData( baseProperty );
		JSONObject propBase = JSONObject.fromObject( base );
		
		Iterator its = propBase.entrySet().iterator();
		while ( its.hasNext() )
		{
			Entry entry = (Entry)its.next();
			String key = (String)entry.getKey();
			if ( propResult.containsKey( key ) )
			{
				JSONArray baseArray = (JSONArray)propBase.get( key );
				JSONArray resultArray = (JSONArray)propResult.get( key );
				Assert.assertTrue(resultArray.containsAll(baseArray));
			}
		}
//		Assert.assertTrue( compareJsons( propBase, propResult ) );
	}
}
