package com.ibm.datatools.ots.tests.restapi.admin;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.AdminAlterObjectService;
import com.ibm.datatools.ots.tests.restapi.common.ConnectionTestService;
import com.jayway.jsonpath.JsonPath;

public class AdminFederationrTest_LUW extends AdminAlterTest
{
	private static final Logger logger = LogManager
			.getLogger(AdminFederationrTest_LUW.class);
	/**
	 * @author ybjwang@cn.ibm.com
	 * some data prepare for Index,MQT,Alias,Schema testing
	 * 
	 */
	@BeforeClass
	public void beforeTest() {
		testcreateDRDAWrapperServerNickname_precondition();
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * clean up the data prepared at before test
	 */
	@AfterClass
	public void afterTest() {	
		testDRDAWrapperServerNickname_clean(true);
	}

	/**
	 * @author huangxue@cn.ibm.com
	 * Objective: This method is to create DRDA wrapper, server, nickname for federation test
	 */
	private void testcreateDRDAWrapperServerNickname_precondition() {
		testDRDAWrapperServerNickname_clean(false);
		ConnectionTestService connTestServices = new ConnectionTestService();
		JSONObject resObj= connTestServices.getConnectionByProfileInJson(this.dbProfile);
		JSONObject responseData =(JSONObject) resObj.get("ResponseData");
		Object connString= responseData.get("response");
		JSONObject connObject = JSONObject.fromObject(connString);
		String dbName =(String) connObject.get("databaseName");
		String dbSchema=(String) connObject.get("user");
		String dbversion=(String)connObject.get("databaseVersion");
		List<String> SQLList = new ArrayList<String>();
		SQLList.add("CREATE TABLE \""+dbSchema+"\".\"HXTABLE\" ( \"NEW_COLUMN1\" CHAR(5))" );
		SQLList.add("INSERT INTO \""+dbSchema+"\".\"HXTABLE\" VALUES \'A\',\'B\',\'C\'" );
		SQLList.add("CREATE WRAPPER \"HXDRDA\" LIBRARY \'libdb2drda.so\' OPTIONS ( ADD DB2_FENCED 'N' )");
		SQLList.add("CREATE SERVER \"HXSERVER\" TYPE DB2/UDB VERSION \'"+dbversion+"\' WRAPPER \"HXDRDA\" AUTHORIZATION \""+dbSchema+"\" PASSWORD \""+this.password+"\" OPTIONS ( ADD DBNAME \'"+dbName+"\' )");
		SQLList.add("CREATE USER MAPPING FOR \""+dbSchema+"\" SERVER \"HXSERVER\" OPTIONS ( ADD REMOTE_AUTHID \'"+dbSchema+"\' , ADD REMOTE_PASSWORD  \'"+this.password+"\' )");
		SQLList.add("CREATE NICKNAME \"HXNICKNAME\" FOR \"HXSERVER\".\""+dbSchema+"\".\"HXTABLE\"");
		sqlEditorService.runSQLinJDBC(SQLList, ";", this.dbProfile, true);
	}
	
		
	/**
	 * @author huangxue@cn.ibm.com
	 * Objective: This method is to drop DRDA wrapper, server, nickname for federation test
	 */
	private void testDRDAWrapperServerNickname_clean(boolean real) {
		ConnectionTestService connTestServices = new ConnectionTestService();
		JSONObject resObj= connTestServices.getConnectionByProfileInJson(this.dbProfile);
		JSONObject responseData =(JSONObject) resObj.get("ResponseData");
		Object connString= responseData.get("response");
		JSONObject connObject = JSONObject.fromObject(connString);
		String dbSchema=(String) connObject.get("user");
				
		List<String> SQLList = new ArrayList<String>();
		SQLList.add("DROP WRAPPER \"HXDRDA\"");
		SQLList.add("DROP TABLE \""+dbSchema+"\".\"HXTABLE\"" );
		sqlEditorService.runSQLinJDBC(SQLList, ";", this.dbProfile, real);
	}
	
	/**
	 * @author huangxue@cn.ibm.com
	 * Objective: Test the Federation Server Connectivity in admin part
	 * Verification points: there is no exception or error in the response data
	 * */
	@Test(description = "Test the Federation Server Connectivity in admin part")
	public void testServerConnectivity() throws InterruptedException {

		logger.info("Running Federation Server Connectivity Testing...");
		String query = "{\"script\":\"SELECT count(*) FROM HXSERVER.sysibm.sysdummy1\",\"type\":\"JDBC\",\"action\":\"RUNSCRIPT\"}";
		JSONObject resObj = alterService.callAlterObjectService(AdminAlterObjectService.EXECUTION_SCRIPT, query, dbProfile);
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		Assert.assertNotNull( responseData );
		if( responseData.containsKey( "exception" )|| responseData.containsKey( "error" )){
			Assert.fail("Federation Server Connectivity is failed");
		}
		String database = (String)responseData.get("database");
		Assert.assertNotNull( database );
		logger.info("PASSED: test federation server connectivity executed successfully");
	}
	
	/**
	 * @author huangxue@cn.ibm.com
	 * Objective: Test Achieve Nickname Data in admin part
	 * Verification points: nickname data should be A,B,C
	 * */
	@Test(description = "Test Achieve Nickname Data in admin part")
	public void testAchieveNicknameData() throws InterruptedException {

		logger.info("Running Achieve Nickname Data Testing...");
		ConnectionTestService connTestServices = new ConnectionTestService();
		JSONObject resObj= connTestServices.getConnectionByProfileInJson(this.dbProfile);
		JSONObject respData =(JSONObject) resObj.get("ResponseData");
		Object connString= respData.get("response");
		JSONObject connObject = JSONObject.fromObject(connString);
		String dbSchema=(String) connObject.get("user");
	
		String query = "{\"schema\":\""+dbSchema.toUpperCase()+"\",\"tableOrg\":\"Nickname\",\"table\":\"HXNICKNAME\",\"maxrows\":\"100\",\"cmd\":\"getAllRows\"}";
		JSONObject result = alterService.callAlterObjectService(AdminAlterObjectService.BROWSEDATA_COMMAND, query, dbProfile);
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		Assert.assertNotNull( responseData );
		JSONObject metaData = JsonPath.read(responseData, "$cells[0]");
		Assert.assertNotNull( metaData );
		String field = metaData.getString("field");
		String internalDataType = metaData.getString("internalDataType");
		Assert.assertEquals(field, "NEW_COLUMN1");
		Assert.assertEquals(internalDataType, "CHAR");
		String items = JsonPath.read(responseData, "$items").toString();
		Assert.assertEquals( items, "[{\"NEW_COLUMN1\":\"A    \"},{\"NEW_COLUMN1\":\"B    \"},{\"NEW_COLUMN1\":\"C    \"}]" );
		
		logger.info("PASSED: Test Achieve Nickname Data executed successfully");
	}
	
	
	/**
	 * @author huangxue@cn.ibm.com
	 * Objective: Test show nickname in SQL Assistant
	 * Verification points: nickname with TYPE: N is in response data
	 * */
	@Test(description = "Test show nickname in SQL Assistant")
	public void testNicknameSQLAssistant() throws InterruptedException {

		logger.info("Running show nickname in SQL Assistant Testing...");
	
		String query = alterService.getJSONData("getSyntaxAssistant_LUW");
		JSONObject result = alterService.callAlterObjectService(AdminAlterObjectService.TYPE_NAVIGATION_URL, query, dbProfile);
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		Assert.assertNotNull( responseData );
		String items = JsonPath.read(responseData, "$items").toString();
		Assert.assertTrue(items.contains("\"TYPE\":\"N\""));
		logger.info("PASSED: Test show nickname in SQL Assistant executed successfully");
	}
	
	/**
	 * @author huangxue@cn.ibm.com
	 * Objective: Test show nickname in Create Alias Assistant
	 * Verification points: nickname with TYPE: N is in response data
	 * */
	@Test(description = "Test show nickname in Create Alias Assistant")
	public void testCreateAliasAssistant() throws InterruptedException {

		logger.info("Running show nickname in Create Alias Assistant Testing...");
	
		String query = alterService.getJSONData("getCreateAliasAssistant_LUW");
		JSONObject result = alterService.callAlterObjectService(AdminAlterObjectService.TYPE_NAVIGATION_URL, query, dbProfile);
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		Assert.assertNotNull( responseData );
		String items = JsonPath.read(responseData, "$items").toString();
		Assert.assertTrue(items.contains("\"TYPE\":\"N\""));
		logger.info("PASSED: Test show nickname in Create Alias Assistant executed successfully");
	}
	
	/**
	 * Test the alter server action in admin part
	 * */
	@Test(description = "Test the alter server action in admin part")
	public void testAlterServer() throws InterruptedException {

		logger.info("Running Alter Server...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * 
		 * */
		
		String expected[] = {"ALTER SERVER \"SERVER\" VERSION \'10.1\' OPTIONS ( ADD DB2_UM_PLUGIN_LANG 'C', ADD DB2_MAX_ASYNC_REQUESTS_PER_QUERY '2', ADD APP_ISOLATION_ENABLE 'Y', ADD CPU_RATIO '2.0', ADD FOLD_ID 'er', SET NUMBER_COMPAT 'Y', ADD DB2_MAXIMAL_PUSHDOWN 'Y', SET VARCHAR2_COMPAT 'Y', ADD IO_RATIO '1.2', SET SAME_DECFLT_ROUNDING 'N', ADD FOLD_PW 'rew', ADD COMM_RATE '3', SET DBNAME 'sampledddd', ADD PLAN_HINTS 'Y', ADD DRIVER_PACKAGE '43', ADD JDBC_LOG 'N', ADD DB2_IUD_ENABLE 'Y', ADD DB2_ONE_REQUEST_PER_CONNECTION 'Y', ADD DB2_UM_PLUGIN 'ee', ADD FED_PROXY_USER 'ser', ADD PUSHDOWN 'N', ADD PASSWORD 'Y', ADD DB2_TWO_PHASE_COMMIT 'Y', ADD COLLATING_SEQUENCE 'Y', ADD OLD_NAME_GEN 'Y', ADD VARCHAR_NO_TRAILING_BLANKS 'wq', SET NO_EMPTY_STRING 'Y', ADD DRIVER_CLASS '434', SET DATE_COMPAT 'Y', ADD XA_OPEN_STRING_OPTIONS 'ew' )"};

		assertEquals("getAlterServer_LUW", expected);

		logger.info("PASSED: Alter server executed successfully");

	}
	
	/**
	 * Test the drop server action in admin part
	 * */
	@Test(description = "Test the drop server action in admin part")
	public void testDropServer() throws InterruptedException {

		logger.info("Running Drop Server...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * 
		 * */
		
		String expected[] = {"DROP SERVER \"SERVER_F\"","DROP SERVER \"SERVER5\""};

		assertEquals("getDropServer_LUW", expected);

		logger.info("PASSED: Drop server executed successfully");

	}

	
	/**
	 * Test the alter UserMapping action in admin part
	 * */
	@Test(description = "Test the alter UserMapping action in admin part")
	public void testAlterUserMapping() throws InterruptedException {

		logger.info("Running Alter UserMapping...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * 
		 * */
		

		
		String expected[] = {"ALTER USER MAPPING FOR \"DB2INST1\" SERVER \"SERVER5\" OPTIONS ( ADD FED_PROXY_USER 'j12op02', SET REMOTE_PASSWORD 'passw0rd', ADD ACCOUNTING_STRING 'connect to sample', ADD USE_TRUSTED_CONTEXT 'Y', SET REMOTE_AUTHID 'J15USER2' )"};

		assertEquals("getAlterUserMapping_LUW", expected);

		logger.info("PASSED: Alter UserMapping executed successfully");

	}
	
	/**
	 * Test the alter UserMapping action in admin part
	 * */
	@Test(description = "Test the drop UserMapping action in admin part")
	public void testDropUserMapping() throws InterruptedException {

		logger.info("Running Drop UserMapping...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * 
		 * */
		

		
		String expected[] = {"DROP USER MAPPING FOR \"DB2INST1\" SERVER \"SERVER5\"","DROP USER MAPPING FOR \"DB2INST1\" SERVER \"SERVER_ORACLE\""};

		assertEquals("getDropUserMapping_LUW", expected);

		logger.info("PASSED: Drop UserMapping executed successfully");

	}
	
	/**
	 * Test the alter Nickname action in admin part
	 * */
	@Test(description = "Test the alter Nickname action in admin part")
	public void testAlterNickname() throws InterruptedException {

		logger.info("Running Alter Nickname...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * 
		 * */
		

		
		String expected[] = {"DROP NICKNAME \"db2inst1\".\"abc\"","CREATE NICKNAME \"IBM_RTMON\".\"abc2\" FOR \"SERVER5\".\"SYSIBM2\".\"SYSTABLES2\""};

		assertEquals("getAlterNickname_LUW", expected);

		logger.info("PASSED: Alter Nickname executed successfully");

	}
	/**
	 * Test the drop Nickname action in admin part
	 * */
	@Test(description = "Test the drop Nickname action in admin part")
	public void testDropNickname() throws InterruptedException {

		logger.info("Running Drop Nickname...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * 
		 * */
		
		String expected[] = {"DROP NICKNAME \"db2inst1\".\"abc\"","DROP NICKNAME \"DB2INST1\".\"NICKNAME_TEST\""};

		assertEquals("getDropNickname_LUW", expected);

		logger.info("PASSED: Drop Nickname executed successfully");

	}
	
	@Test(description = "Test the Server batch privileges in admin part")
	public void testServerBatchPrivilegeDefinition() throws InterruptedException {
		logger.info("Running Sequence batch privileges definition test...");
		String query = alterService.getJSONData("getServerBatchPrivilege_LUW");

		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.PRIVILEGE_MODEL_DEFINITION, query, dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "actions" ) );
		JSONArray actions = responseData.getJSONArray( "actions" );
		Assert.assertNotNull( actions );
		
		String[] acts = {"PASSTHRU"};
		Assert.assertEquals(actions.size(), acts.length);
		for(int i = 0; i < actions.size(); i++){
			Assert.assertEquals(actions.get(i).toString(), acts[i]);
		}

		logger.info("PASSED: Server batch privileges definition executed successfully");
	}
	
	@Test(description = "Test the Nickname batch privileges in admin part")
	public void testNicknameBatchPrivilegeDefinition() throws InterruptedException {
		logger.info("Running Nickname batch privileges definition test...");
		String query = alterService.getJSONData("getNicknameBatchPrivilege_LUW");

		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.PRIVILEGE_MODEL_DEFINITION, query, dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "actions" ) );
		JSONArray actions = responseData.getJSONArray( "actions" );
		Assert.assertNotNull( actions );
		
		String[] acts = {"CONTROL", "ALTER", "DELETE", "INDEX", "INSERT", "REFERENCES", "SELECT", "UPDATE"};
		Assert.assertEquals(actions.size(), acts.length);
		for(int i = 0; i < actions.size(); i++){
			Assert.assertEquals(actions.get(i).toString(), acts[i]);
		}

		logger.info("PASSED: Nickname batch privileges definition executed successfully");
	}
	
	//@Test(description = "Test the batch privileges ddl in admin part")
	public void testBatchPrivilegeDDLDefinition() throws InterruptedException {
		logger.info("Running batch privileges ddl test...");
		String query = alterService.getJSONData("getBatchPrivilegeDDL_LUW");

		JSONObject resObj = alterService.callAlterObjectService(
				AdminAlterObjectService.PRIVILEGE_MODEL_COMMAND, query, dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "items" ) );
		JSONArray items = responseData.getJSONArray( "items" );
		Assert.assertNotNull( items );
		
		String[] acts = {"GRANT INDEX, REFERENCES ON TABLE \"db2inst1\".\"abc\" TO USER DB2INST1",
				"GRANT INDEX, REFERENCES ON TABLE \"DB2INST1\".\"NICKNAME_TEST\" TO USER DB2INST1"};
		Assert.assertEquals(items.size(), acts.length);
		String ddl = "";
		for(int i = 0; i < items.size(); i++){
			ddl = JsonPath.read( responseData, "$.items[" + i + "].statement" );
			Assert.assertEquals(ddl, acts[i]);
		}

		logger.info("PASSED: batch privileges ddl executed successfully");
	}
	
	@Test(description = "Test the create DRDA wrapper DDL in admin part")
	public void testCreateDRDAWrapperDDL() throws InterruptedException {
		logger.info("Running Create DRDA wrapper DDL test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CREATE WRAPPER \"DRDA\" LIBRARY 'libdb2drda.so' OPTIONS ( ADD DB2_FENCED 'N' )"}]}
		 **/

		String expected[] = {"CREATE WRAPPER \"DRDA\" LIBRARY 'libdb2drda.so' OPTIONS ( ADD DB2_FENCED 'N' )"};
		assertEquals("getCreateDRDAWrapperDDL_LUW", expected);

		logger.info("PASSED: create DRDA wrapper DDL executed successfully");
	}
	
	@Test(description = "Test the create ORACLE wrapper DDL in admin part")
	public void testCreateORACLEWrapperDDL() throws InterruptedException {
		logger.info("Running Create ORACLE wrapper DDL test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CREATE WRAPPER \"NET8\" LIBRARY 'libdb2net8.so' OPTIONS ( ADD DB2_FENCED 'N' )"}]}
		 **/

		String expected[] = {"CREATE WRAPPER \"NET8\" LIBRARY 'libdb2net8.so' OPTIONS ( ADD DB2_FENCED 'N' )"};
		assertEquals("getCreateORACLEWrapperDDL_LUW", expected);

		logger.info("PASSED: create ORACLE wrapper DDL executed successfully");
	}
	
	@Test(description = "Test the create Teradata wrapper DDL in admin part")
	public void testCreateTeradataWrapperDDL() throws InterruptedException {
		logger.info("Running Create Teradata wrapper DDL test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CREATE WRAPPER \"TERADATA\" LIBRARY 'libdb2teradata.so' OPTIONS ( ADD DB2_FENCED 'Y' )"}]}
		 **/

		String expected[] = {"CREATE WRAPPER \"TERADATA\" LIBRARY 'libdb2teradata.so' OPTIONS ( ADD DB2_FENCED 'Y' )"};
		assertEquals("getCreateTeradataWrapperDDL_LUW", expected);

		logger.info("PASSED: create Teradata wrapper DDL executed successfully");
	}
	
	@Test(description = "Test the create wrapper with options DDL in admin part")
	public void testCreateWrapperWithOptionsDDL() throws InterruptedException {
		logger.info("Running Create wrapper  with options DDL test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CREATE WRAPPER \"TERADATA\" LIBRARY 'libdb2teradata.so' OPTIONS ( ADD DB2_FENCED 'N', ADD DB2_UM_PLUGIN 'com.ibm.plugin.x', ADD DB2_UM_PLUGIN_LANG 'C' )"}]}
		 **/

		String expected[] = {"CREATE WRAPPER \"TERADATA\" LIBRARY 'libdb2teradata.so' OPTIONS ( ADD DB2_FENCED 'N', ADD DB2_UM_PLUGIN 'com.ibm.plugin.x', ADD DB2_UM_PLUGIN_LANG 'C' )"};
		assertEquals("getCreateWrapperWithOptionsDDL_LUW", expected);

		logger.info("PASSED: create wrapper  with options DDL executed successfully");
	}
	
	@Test(description = "Test the create wrapper option definition in admin part")
	public void testCreateWrapperOptionDefinition() throws InterruptedException {
		logger.info("Running Create wrapper option definition test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"name":"DB2_UM_PLUGIN","label":"User mapping plug-in","valueType":"string","defaultValue":{"value":"","label":""}},
		 * {"values":[{"value":"Java","label":"Java"},{"value":"C","label":"C"}],"name":"DB2_UM_PLUGIN_LANG","label":"Plug-in language","valueType":"enum","defaultValue":{"value":"Java","label":"Java"}}]}
		 **/

		String query = alterService.getJSONData( "getCreateWrapperOptionDefinition_LUW" );

		JSONObject resObj =
				alterService.callCreateDefinitionService(query, dbProfile );

		// Get the response Data
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "items" ) );
		JSONArray items = responseData.getJSONArray( "items" );

		Assert.assertNotNull( items );

		JSONObject jo = null;
		
		jo = (JSONObject) items.get(0);
		Assert.assertTrue(jo.containsKey("defaultValue"));
		Assert.assertEquals(jo.get("valueType"),"string");
		Assert.assertEquals(jo.get("name"),"DB2_UM_PLUGIN");
		
		jo = (JSONObject) items.get(1);
		Assert.assertTrue(jo.containsKey("defaultValue"));
		Assert.assertEquals(jo.get("valueType"),"enum");
		Assert.assertEquals(jo.get("name"),"DB2_UM_PLUGIN_LANG");
		
		logger.info("PASSED: create wrapper definition executed successfully");
	}
	
	@Test(description = "Test the create server definition in admin part")
	public void testCreateServerDefinition() throws InterruptedException {
		logger.info("Running Create server definition test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"values":[{"value":"DRDA","label":"DRDA"}],"name":"WRAPNAME","label":"Wrapper","valueType":"enum","defaultValue":{"value":"DRDA","label":"DRDA"}},
		 * {"dependencies":[{"condition":[{"rule":"equal","value":"DRDA","key":"WRAPNAME"}],"disable":true,"defaultValue":{"value":"DRDA","label":"DRDA"}}],"name":"DATA_SOURCE","label":"The data source of wrapper","valueType":"string","defaultValue":{"value":"DRDA","label":"DRDA"}},
		 * {"values":[{"value":"DB2\/UDB","label":"DB2\/UDB"},{"value":"DB2\/ZOS","label":"DB2\/ZOS"},{"value":"DB2\/ISERIES","label":"DB2\/ISERIES"},{"value":"DB2\/VM","label":"DB2\/VM"}],"dependencies":[{"values":[{"value":"DB2\/UDB","label":"DB2\/UDB"},{"value":"DB2\/ZOS","label":"DB2\/ZOS"},{"value":"DB2\/ISERIES","label":"DB2\/ISERIES"},{"value":"DB2\/VM","label":"DB2\/VM"}],"condition":[{"rule":"equal","value":"DRDA","key":"DATA_SOURCE"}],"disable":false}],"name":"SERVERTYPE","label":"Type","valueType":"enum","defaultValue":{"value":"DB2\/UDB","label":"DB2\/UDB"}},{"values":[{"value":"10.5","label":"10.5"},{"value":"10.1","label":"10.1"},{"value":"9.8","label":"9.8"},{"value":"9.7","label":"9.7"},{"value":"9.5","label":"9.5"},{"value":"9.1","label":"9.1"},{"value":"8.2","label":"8.2"}],"dependencies":[{"values":[{"value":"13","label":"13"},{"value":"12","label":"12"},{"value":"2.6","label":"2.6"},{"value":"2.5","label":"2.5"}],"condition":[{"rule":"equal","value":"TERADATA","key":"SERVERTYPE"}],"disable":false,"defaultValue":{"value":"13","label":"13"}},{"values":[{"value":"11g","label":"11g"},{"value":"11","label":"11"},{"value":"10g","label":"10g"},{"value":"10","label":"10"},{"value":"9i","label":"9i"},{"value":"9","label":"9"},{"value":"8i","label":"8i"},{"value":"8","label":"8"}],"condition":[{"rule":"equal","value":"ORACLE","key":"SERVERTYPE"}],"disable":false,"defaultValue":{"value":"11g","label":"11g"}},{"values":[{"value":"10.5","label":"10.5"},{"value":"10.1","label":"10.1"},{"value":"9.8","label":"9.8"},{"value":"9.7","label":"9.7"},{"value":"9.5","label":"9.5"},{"value":"9.1","label":"9.1"},{"value":"8.2","label":"8.2"}],"condition":[{"rule":"equal","value":"DB2\/UDB","key":"SERVERTYPE"}],"disable":false,"defaultValue":{"value":"10.5","label":"10.5"}},{"values":[{"value":"11.0","label":"11.0"},{"value":"10.0","label":"10.0"},{"value":"9.0","label":"9.0"}],"condition":[{"rule":"equal","value":"DB2\/ZOS","key":"SERVERTYPE"}],"disable":false,"defaultValue":{"value":"11.0","label":"11.0"}},
		 * {"values":[{"value":"10.0","label":"10.0"},{"value":"9.5","label":"9.5"}],"condition":[{"rule":"equal","value":"DB2\/ISERIES","key":"SERVERTYPE"}],"disable":false,"defaultValue":{"value":"10.0","label":"10.0"}},{"values":[{"value":"10.0","label":"10.0"},{"value":"9.7","label":"9.7"}],"condition":[{"rule":"equal","value":"DB2\/VM","key":"SERVERTYPE"}],"disable":false,"defaultValue":{"value":"10.0","label":"10.0"}}],"name":"SERVERVERSION","label":"Version","valueType":"enum","defaultValue":{"value":"10.5","label":"10.5"}},
		 * {"name":"REMOTESERVER_USERID","label":"Remote user ID","valueType":"string"},
		 * {"name":"REMOTESERVER_PASSWORD","label":"Remote password","valueType":"string"}]}
		 * */

		String query = alterService.getJSONData( "getCreateServerDefinition_LUW" );

		JSONObject resObj =
				alterService.callCreateDefinitionService(query, dbProfile );

		// Get the response Data
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "items" ) );
		JSONArray items = responseData.getJSONArray( "items" );

		Assert.assertNotNull( items );

		JSONObject jo = null;
		
		jo = (JSONObject) items.get(0);
		Assert.assertTrue(jo.containsKey("defaultValue"));
		Assert.assertEquals(jo.get("valueType"),"string");
		Assert.assertEquals(jo.get("name"),"SERVERNAME");
		
		jo = (JSONObject) items.get(1);
		Assert.assertTrue(jo.containsKey("defaultValue"));
		Assert.assertEquals(jo.get("valueType"),"enum");
		Assert.assertEquals(jo.get("name"),"WRAPNAME");
		
		jo = (JSONObject) items.get(2);
		Assert.assertEquals(jo.get("valueType"),"string");
		Assert.assertEquals(jo.get("name"),"DATA_SOURCE");
		
		jo = (JSONObject) items.get(3);
		Assert.assertEquals(jo.get("valueType"),"enum");
		Assert.assertEquals(jo.get("name"),"SERVERTYPE");
		
		jo = (JSONObject) items.get(4);
		Assert.assertEquals(jo.get("valueType"),"enum");
		Assert.assertEquals(jo.get("name"),"SERVERVERSION");
		
		jo = (JSONObject) items.get(5);
		Assert.assertEquals(jo.get("valueType"),"validation_text");
		Assert.assertEquals(jo.get("name"),"REMOTESERVER_DATABASE");
		
		jo = (JSONObject) items.get(6);
		Assert.assertEquals(jo.get("valueType"),"validation_text");
		Assert.assertEquals(jo.get("name"),"REMOTESERVER_NODE_NAME");
		
		jo = (JSONObject) items.get(7);
		Assert.assertEquals(jo.get("valueType"),"validation_text");
		Assert.assertEquals(jo.get("name"),"URL");
		
		jo = (JSONObject) items.get(8);
		Assert.assertEquals(jo.get("valueType"),"validation_text");
		Assert.assertEquals(jo.get("name"),"DRIVER_CLASS");
		
		jo = (JSONObject) items.get(9);
		Assert.assertEquals(jo.get("valueType"),"validation_text");
		Assert.assertEquals(jo.get("name"),"REMOTESERVER_USERID");
		
		jo = (JSONObject) items.get(10);
		Assert.assertEquals(jo.get("valueType"),"validation_text");
		Assert.assertEquals(jo.get("name"),"REMOTESERVER_PASSWORD");
		
		jo = (JSONObject) items.get(11);
		Assert.assertEquals(jo.get("valueType"),"enum");
		Assert.assertEquals(jo.get("name"),"SERVERVALUES");

		logger.info("PASSED: create server definition executed successfully");
	}
	
	@Test(description = "Test the create server option definition in admin part")
	public void testCreateServerOptionDefinition() throws InterruptedException {
		logger.info("Running Create server option definition test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * ...
		 * **/

		String query = alterService.getJSONData( "getCreateServerOptionDefinition_LUW" );

		JSONObject resObj =
				alterService.callCreateDefinitionService(query, dbProfile );

		// Get the response Data
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "items" ) );
		JSONArray items = responseData.getJSONArray( "items" );

		Assert.assertNotNull( items );
		//Assert.assertTrue(items.size() == 31);
		List<String> optionList = new ArrayList<String>() {
			private static final long serialVersionUID = 4751681290696952901L;
			{
				add("PUSHDOWN");
				add("CODEPAGE");
				add("DB2_AUTHID_QUOTE_CHAR");
				add("DB2_ID_QUOTE_CHAR");
				add("DB2_TABLE_QUOTE_CHAR");
				add("DATEFORMAT");
				add("TIMEFORMAT");
				add("TIMESTAMPFORMAT");
				add("DB2_TWO_PHASE_COMMIT");
				add("DB2_MAXIMAL_PUSHDOWN");
				add("COLLATING_SEQUENCE");
				//add("DATE_COMPAT");
				add("FED_PROXY_USER");
				add("FOLD_ID");
				add("FOLD_PW");
				add("NO_EMPTY_STRING");
				//add("VARCHAR2_COMPAT");
				add("VARCHAR_NO_TRAILING_BLANKS");
				//add("SAME_DECFLT_ROUNDING");
				//add("NUMBER_COMPAT");
				add("OLD_NAME_GEN");
				add("PASSWORD");
				add("DB2_MAX_ASYNC_REQUESTS_PER_QUERY");
				add("APP_ISOLATION_ENABLE");
				add("DB2_ONE_REQUEST_PER_CONNECTION");
				add("DB2_IUD_ENABLE");
				add("COMM_RATE");
				add("CPU_RATIO");
				add("DB2_UM_PLUGIN");
				add("DB2_UM_PLUGIN_LANG");
				add("IO_RATIO");
				add("PLAN_HINTS");
				add("XA_OPEN_STRING_OPTIONS");
				add("DRIVER_PACKAGE");
				add("JDBC_LOG");
			}
		};
		//if(this.dbProfile.equalsIgnoreCase("DashTx")){
			optionList.add("HOST");
			optionList.add("PORT");
			optionList.add("SERVICE_NAME");
		//}
		for(int i = 0; i < items.size(); i ++){
			String key = ((JSONObject) items.get(0)).get("name").toString();
			Assert.assertTrue(optionList.indexOf(key) >= 0);
		}

		logger.info("PASSED: create server definition executed successfully");
	}
	
	@Test(description = "Test the create server DDL in admin part")
	public void testCreateServerDDL() throws InterruptedException {
		logger.info("Running Create server DDL test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CREATE SERVER \"SERVER1\" TYPE DB2\/VM VERSION '9.7' WRAPPER \"DRDA\" AUTHORIZATION \"db2inst1\" PASSWORD \"passw0rd\" OPTIONS ( ADD DBNAME 'SAMPLE' )"}]}
		 **/

		String expected[] = {"CREATE SERVER \"SERVER1\" TYPE DB2/VM VERSION '9.7' WRAPPER \"DRDA\" AUTHORIZATION \"db2inst1\" PASSWORD \"passw0rd\" OPTIONS ( ADD DBNAME 'SAMPLE' )"};
		assertEquals("getCreateServerDDL_LUW", expected);

		logger.info("PASSED: create server DDL executed successfully");
	}
	
	@Test(description = "Test the create server with options DDL in admin part")
	public void testCreateServerWithOptionsDDL() throws InterruptedException {
		logger.info("Running Create server with options DDL test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CREATE SERVER \"SERVER1\" TYPE DB2\/VM VERSION '9.7' WRAPPER \"DRDA\" AUTHORIZATION \"db2inst1\" PASSWORD \"passw0rd\" OPTIONS ( ADD DBNAME 'SAMPLE', ADD DB2_TWO_PHASE_COMMIT 'Y', ADD DB2_ONE_REQUEST_PER_CONNECTION 'Y', ADD COLLATING_SEQUENCE 'Y', ADD DATE_COMPAT 'Y', ADD APP_ISOLATION_ENABLE 'Y', ADD OLD_NAME_GEN 'Y', ADD DB2_MAXIMAL_PUSHDOWN 'Y', ADD FOLD_ID 'yu', ADD VARCHAR2_COMPAT 'Y', ADD FED_PROXY_USER 'yu', ADD DB2_MAX_ASYNC_REQUESTS_PER_QUERY '15', ADD DB2_IUD_ENABLE 'Y', ADD PASSWORD 'Y', ADD FOLD_PW 'yu' )"}]}
		 **/

		String query = alterService.getJSONData( "getCreateServerWithOptionsDDL_LUW" );

		JSONObject resObj =
				alterService.callAlterObjectService( AdminAlterObjectService.ALTER_OBJECT, query, dbProfile );

		// Get the response Data
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "items" ) );
		JSONArray items = responseData.getJSONArray( "items" );

		Assert.assertNotNull( items );
		String ddl = JsonPath.read( responseData, "$.items[0].statement" );		
		
		Assert.assertTrue(ddl.startsWith("CREATE SERVER \"SERVER1\" TYPE DB2/VM VERSION '9.7' WRAPPER \"DRDA\" AUTHORIZATION \"db2inst1\" PASSWORD \"passw0rd\" OPTIONS"));
		Assert.assertTrue(ddl.contains("ADD DBNAME 'SAMPLE'"));
		Assert.assertTrue(ddl.contains("ADD DB2_MAX_ASYNC_REQUESTS_PER_QUERY '15'"));
		Assert.assertTrue(ddl.contains("ADD APP_ISOLATION_ENABLE 'Y'"));
		Assert.assertTrue(ddl.contains("ADD DB2_IUD_ENABLE 'Y'"));
		Assert.assertTrue(ddl.contains("ADD DB2_ONE_REQUEST_PER_CONNECTION 'Y'"));
		Assert.assertTrue(ddl.contains("ADD FOLD_ID 'yu'"));
		Assert.assertTrue(ddl.contains("ADD DB2_MAXIMAL_PUSHDOWN 'Y'"));
		Assert.assertTrue(ddl.contains("ADD VARCHAR2_COMPAT 'Y'"));
		Assert.assertTrue(ddl.contains("ADD FED_PROXY_USER 'yu'"));
		Assert.assertTrue(ddl.contains("ADD PASSWORD 'Y'"));
		Assert.assertTrue(ddl.contains("ADD DB2_TWO_PHASE_COMMIT 'Y'"));
		
		logger.info("PASSED: create server with options DDL executed successfully");
	}
	
	@Test(description = "Test the create user mapping definition in admin part")
	public void testCreateUserMappingDefinition() throws InterruptedException {
		logger.info("Running Create user mapping definition test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * */

		String query = alterService.getJSONData( "getCreateUserMappingDefinition_LUW" );

		JSONObject resObj =
				alterService.callCreateDefinitionService(query, dbProfile );

		// Get the response Data
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "items" ) );
		JSONArray items = responseData.getJSONArray( "items" );

		Assert.assertNotNull( items );

		JSONObject jo = null;
		
//		jo = (JSONObject) items.get(0);
//		Assert.assertTrue(jo.containsKey("defaultValue"));
//		Assert.assertEquals(jo.get("valueType"),"string");
//		Assert.assertEquals(jo.get("name"),"AUTHID");
//		
//		jo = (JSONObject) items.get(1);
//		Assert.assertTrue(jo.containsKey("defaultValue"));
//		Assert.assertEquals(jo.get("valueType"),"enum");
//		Assert.assertEquals(jo.get("name"),"SERVERNAME");
//
//		
//		jo = (JSONObject) items.get(2);
//		Assert.assertEquals(jo.get("valueType"),"string");
//		Assert.assertEquals(jo.get("name"),"SERVER_TYPE");
//		
//		jo = (JSONObject) items.get(3);
//		Assert.assertEquals(jo.get("valueType"),"string");
//		Assert.assertEquals(jo.get("name"),"REMOTE_AUTHID");
//		
//		jo = (JSONObject) items.get(4);
//		Assert.assertEquals(jo.get("valueType"),"string");
//		Assert.assertEquals(jo.get("name"),"REMOTE_PASSWORD");
		
		logger.info("PASSED: create userMapping definition executed successfully");
	}
	
	@Test(description = "Test the create user mapping option definition in admin part")
	public void testCreateUserMappingOptionDefinition() throws InterruptedException {
		logger.info("Running Create user Mapping option definition test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 **/

		String query = alterService.getJSONData( "getCreateUserMappingOptionDefinition_LUW" );

		JSONObject resObj =
				alterService.callCreateDefinitionService(query, dbProfile );

		// Get the response Data
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "items" ) );
		JSONArray items = responseData.getJSONArray( "items" );

		Assert.assertNotNull( items );

		JSONObject jo = null;
		
		jo = (JSONObject) items.get(0);
		Assert.assertEquals(jo.get("name"),"ACCOUNTING_STRING");
		
		jo = (JSONObject) items.get(1);
		Assert.assertEquals(jo.get("name"),"FED_PROXY_USER");
		
		jo = (JSONObject) items.get(2);
		Assert.assertEquals(jo.get("name"),"USE_TRUSTED_CONTEXT");
		
		logger.info("PASSED: create userMapping definition executed successfully");
	}
	
	@Test(description = "Test the create UserMapping DDL in admin part")
	public void testCreateUserMappingDDL() throws InterruptedException {
		logger.info("Running Create UserMapping DDL test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CREATE USER MAPPING FOR \"db2inst1\" SERVER \"SERVER_DRDA\" OPTIONS ( ADD REMOTE_AUTHID 'db2inst1', ADD REMOTE_PASSWORD 'passw0rd' )"}]}
		 **/

		String expected[] = {"CREATE USER MAPPING FOR \"db2inst1\" SERVER \"SERVER_DRDA\" OPTIONS ( ADD REMOTE_AUTHID 'db2inst1', ADD REMOTE_PASSWORD 'passw0rd' )"};
		assertEquals("getCreatUserMappingDDL_LUW", expected);

		logger.info("PASSED: create UserMapping DDL executed successfully");
	}
	
	@Test(description = "Test the create UserMapping with options DDL in admin part")
	public void testCreateUserMappingWithOptionsDDL() throws InterruptedException {
		logger.info("Running Create UserMapping with options DDL test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CREATE USER MAPPING FOR \"db2inst1\" SERVER \"SERVER_DRDA\" OPTIONS ( ADD REMOTE_AUTHID 'db2inst1', ADD REMOTE_PASSWORD 'passw0rd', ADD FED_PROXY_USER 'sd', ADD ACCOUNTING_STRING 'sd', ADD USE_TRUSTED_CONTEXT 'Y' )"}]}
		 **/

		String expected[] = {"CREATE USER MAPPING FOR \"db2inst1\" SERVER \"SERVER_DRDA\" OPTIONS ( ADD REMOTE_AUTHID 'db2inst1', ADD REMOTE_PASSWORD 'passw0rd', ADD FED_PROXY_USER 'sd', ADD ACCOUNTING_STRING 'sd', ADD USE_TRUSTED_CONTEXT 'Y' )"};
		assertEquals("getCreateUserMappingWithOptionsDDL_LUW", expected);

		logger.info("PASSED: create UserMapping with options DDL executed successfully");
	}
	
	@Test(description = "Test the create nickname definition in admin part")
	public void testCreateNicknameDefinition() throws InterruptedException {
		logger.info("Running Create nickname definition test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * */

		String query = alterService.getJSONData( "getCreateNicknameDefinition_LUW" );

		JSONObject resObj =
				alterService.callCreateDefinitionService(query, dbProfile );

		// Get the response Data
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "items" ) );
		JSONArray items = responseData.getJSONArray( "items" );

		Assert.assertNotNull( items );

		JSONObject jo = null;
		
		jo = (JSONObject) items.get(0);
		Assert.assertTrue(jo.containsKey("values"));
		Assert.assertTrue(jo.containsKey("defaultValue"));
		Assert.assertEquals(jo.get("valueType"),"enum");
		Assert.assertEquals(jo.get("name"),"TABSCHEMA");
		
		jo = (JSONObject) items.get(1);
		Assert.assertTrue(jo.containsKey("defaultValue"));
		Assert.assertEquals(jo.get("valueType"),"string");
		Assert.assertEquals(jo.get("name"),"TABNAME");
		
		jo = (JSONObject) items.get(2);
		Assert.assertTrue(jo.containsKey("defaultValue"));
		Assert.assertEquals(jo.get("valueType"),"enum");
		Assert.assertEquals(jo.get("name"),"SERVERNAME");
		
		jo = (JSONObject) items.get(3);
		Assert.assertTrue(jo.containsKey("defaultValue"));
		Assert.assertEquals(jo.get("valueType"),"string");
		Assert.assertEquals(jo.get("name"),"REMOTE_SCHEMA");
		
		jo = (JSONObject) items.get(4);
		Assert.assertTrue(jo.containsKey("defaultValue"));
		Assert.assertEquals(jo.get("valueType"),"string");
		Assert.assertEquals(jo.get("name"),"REMOTE_TABLE");

		logger.info("PASSED: create nickname definition executed successfully");
	}
	
	@Test(description = "Test the get nickname wizard definition in admin part")
	public void testCreateNicknameWizardDefinition() throws InterruptedException {
		logger.info("Running Create nickname wizard definition test...");

		String query = alterService.getJSONData( "getCreateNicknameWizardDefinition_LUW" );

		JSONObject resObj =
				alterService.callCreateDefinitionService(query, dbProfile );

		// Get the response Data
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "items" ) );
		JSONArray items = responseData.getJSONArray( "items" );

		Assert.assertNotNull( items );
		/*
		JSONObject jo_wrapperValue = null;
		JSONObject jo_server = null;
		JSONObject jo_source = null;
		JSONObject jo_wrapper = null;
		JSONObject jo_OS = null;
		
		//comment it temporally
		
		jo_wrapperValue = (JSONObject) items.get(0);
		
		Assert.assertTrue(jo_wrapperValue.containsKey("values"));
		Assert.assertTrue(jo_wrapperValue.containsKey("defaultValue"));
		Assert.assertEquals(jo_wrapperValue.get("valueType"),"string");
		Assert.assertEquals(jo_wrapperValue.get("name"),"WRAPPERVALUES");
		
		jo_server = (JSONObject) items.get(1);
		Assert.assertTrue(jo_server.containsKey("values"));
		Assert.assertTrue(jo_server.containsKey("defaultValue"));
		Assert.assertEquals(jo_server.get("valueType"),"string");
		Assert.assertEquals(jo_server.get("name"),"REMOTESERVER");
		
		jo_source = (JSONObject) items.get(2);
		Assert.assertTrue(jo_server.containsKey("values"));
		Assert.assertTrue(jo_source.containsKey("defaultValue"));
		Assert.assertEquals(jo_source.get("valueType"),"string");
		Assert.assertEquals(jo_source.get("name"),"REMOTESERVER_SOURCE");
		
		jo_wrapper = (JSONObject) items.get(3);
		Assert.assertTrue(jo_wrapper.containsKey("values"));
		Assert.assertTrue(jo_wrapper.containsKey("defaultValue"));
		Assert.assertEquals(jo_wrapper.get("valueType"),"string");
		Assert.assertEquals(jo_wrapper.get("name"),"ALLWRAPPERS");
		
		jo_OS = (JSONObject) items.get(4);
		Assert.assertTrue(jo_OS.containsKey("defaultValue"));
		Assert.assertEquals(jo_OS.get("valueType"),"readOnly");
		Assert.assertEquals(jo_OS.get("name"),"OS");
		
        jo_OS = (JSONObject) items.get(5);
		Assert.assertTrue(jo_OS.containsKey("defaultValue"));
		Assert.assertEquals(jo_OS.get("valueType"),"string");
		Assert.assertEquals(jo_OS.get("name"),"LOCALUSER");*/

		logger.info("PASSED: create nickname wizard definition executed successfully");
	}
	
	@Test(description = "Test the create server in nicaname wizard DDL in admin part")
	public void testCreateNicknameWizardServerDDL() throws InterruptedException {
		logger.info("Running Create server in nicaname wizard DDL test...");

		String query = alterService.getJSONData( "getCreateNicknameWizardServerDDL_LUW" );

		JSONObject resObj =
				alterService.callAlterObjectService( AdminAlterObjectService.ALTER_OBJECT, query, dbProfile );

		// Get the response Data
		//Since the response statement is dynamically, so we can not use Assert.equal to decide whether it is as accepted
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "items" ) );
		JSONArray ja = responseData.getJSONArray( "items" );

		Assert.assertNotNull( ja );

		logger.info("PASSED: create server in nicaname wizard DDL executed successfully");
	}
	
	
	@Test(description = "Test the create PostgreSQL server in nicaname wizard DDL in admin part")
	public void testCreatePostgreSQLServerDDL() throws InterruptedException {
		logger.info("Running Create PostgreSQL server in nicaname wizard DDL test...");
		
		String query = alterService.getJSONData( "getCreatePostgreSQLServer_LUW" );

		JSONObject resObj =
				alterService.callAlterObjectService( AdminAlterObjectService.ALTER_OBJECT, query, dbProfile );

		// Get the response Data
		//Since the response statement is dynamically, so we can not use Assert.equal to decide whether it is as accepted
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "items" ) );
		JSONArray ja = responseData.getJSONArray( "items" );
		Assert.assertNotNull( ja );
		String statement = JsonPath.read(responseData, "$items[0].statement");
		statement.contains("TYPE POSTGRESQL");
		logger.info("PASSED: create server in nicaname wizard DDL executed successfully");
	}
	
	
	@Test(description = "Test the create MySQL server in nicaname wizard DDL in admin part")
	public void testCreateMySQLServerDDL() throws InterruptedException {
		logger.info("Running Create MySQL server in nicaname wizard DDL test...");
		
		String query = alterService.getJSONData( "getCreateMySQLServer_LUW" );

		JSONObject resObj =
				alterService.callAlterObjectService( AdminAlterObjectService.ALTER_OBJECT, query, dbProfile );

		// Get the response Data
		//Since the response statement is dynamically, so we can not use Assert.equal to decide whether it is as accepted
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "items" ) );
		JSONArray ja = responseData.getJSONArray( "items" );
		Assert.assertNotNull( ja );
		String statement = JsonPath.read(responseData, "$items[0].statement");
		statement.contains("TYPE MYSQL");
		logger.info("PASSED: create server in nicaname wizard DDL executed successfully");
	}
	
	
	@Test(description = "Test the get remote schema layout in nicaname wizard DDL in admin part")
	public void testCreateNicknameWizardSchemaLayoutDDL() throws InterruptedException {
		logger.info("Running get remote schema layout in nicaname wizard DDL test...");

		String query = alterService.getJSONData( "getCreateNicknameSchemalayout_LUW" );

		JSONObject resObj =
				alterService.callAlterObjectService( AdminAlterObjectService.TYPE_NAVIGATION_URL, query, dbProfile );

		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "cells" ) );
		JSONArray ja = responseData.getJSONArray( "cells" );
		Assert.assertTrue(ja.size()==5);
		JSONObject jo = (JSONObject) ja.get(0);
		Assert.assertEquals(jo.get("field"),"CHECKBOX");
		Assert.assertEquals(jo.get("dataType"),"string");
		
		jo = (JSONObject) ja.get(1);
		Assert.assertEquals(jo.get("field"),"TABNAME");
		Assert.assertEquals(jo.get("dataType"),"string");
		
		jo = (JSONObject) ja.get(2);
		Assert.assertEquals(jo.get("field"),"TABSCHEMA");
		Assert.assertEquals(jo.get("dataType"),"string");
		
		jo = (JSONObject) ja.get(3);
		Assert.assertEquals(jo.get("field"),"OWNER");
		Assert.assertEquals(jo.get("dataType"),"string");
		
		jo = (JSONObject) ja.get(4);
		Assert.assertEquals(jo.get("field"),"STEREOTYPE");
		Assert.assertEquals(jo.get("dataType"),"string");

		logger.info("PASSED:get remote schema layout in nicaname wizard DDL executed successfully");
	}
	
	@Test(description = "Test the generate nickname DDL in nicaname wizard DDL in admin part")
	public void testCreateNicknameWizardNicknameDDL() throws InterruptedException {
		logger.info("Running generate nickname DDL in nicaname wizard DDL test...");

		String expected[] = {"CREATE NICKNAME \"IIDEV18\".\"T1\" FOR \"S1\".\"IIDEV18\".\"T1\""};
		assertEquals("getCreateNicknameWizardNicknameDDL_LUW", expected);

		logger.info("PASSED: generate nickname DDL in nicaname wizard DDL executed successfully");
	}
	
	/**
	 * Test the alter wrapper action in admin part
	 * */
	@Test(description = "Test the alter wrapper action in admin part")
	public void testAlterWrapper() throws InterruptedException {

		logger.info("Running Alter Wrapper...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"items":[{"statement":"ALTER WRAPPER DRDA OPTIONS ( SET DB2_FENCED 'Y', ADD DB2_UM_PLUGIN 'com.ibm.plugin' )"}]}
		 * 
		 * */
		

		
		String expected[] = {"ALTER WRAPPER DRDA OPTIONS ( SET DB2_FENCED 'Y', ADD DB2_UM_PLUGIN 'com.ibm.plugin' )"};

		assertEquals("getAlterWrapper_LUW", expected);

		logger.info("PASSED: Alter wrapper executed successfully");

	}
	
	@Test(description = "Test the create Nickname DDL in admin part")
	public void testCreateNicknameDDL() throws InterruptedException {
		logger.info("Running Create Nickname DDL test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CREATE NICKNAME \"DB2INST1\".\"NICKNAME\" FOR \"SERVER_DRDA\".\"db2inst1\".\"TABLE1\""}]}
		 **/

		String expected[] = {"CREATE NICKNAME \"DB2INST1\".\"NICKNAME\" FOR \"SERVER_DRDA\".\"db2inst1\".\"TABLE1\""};
		assertEquals("getCreatNicknameDDL_LUW", expected);

		logger.info("PASSED: create Nickname DDL executed successfully");
	}
	
	@Test(description = "Test the create wrapper definition in admin part")
	public void testCreateWrapperDefinition() throws InterruptedException {
		logger.info("Running Create wrapper definition test...");

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"values":[{"value":"DRDA","label":"DRDA"},{"value":"ORACLE","label":"ORACLE"},{"value":"TERADATA","label":"TERADATA"},{"value":"JDBC","label":"JDBC"}],"name":"DATA_SOURCE","label":"Data source","valueType":"enum","defaultValue":{"value":"DRDA","label":"DRDA"}},
		 * {"validation":{"required":true},"dependencies":[{"condition":[{"rule":"equal","value":"TERADATA","key":"DATA_SOURCE"}],"disable":false,"defaultValue":{"value":"TERADATA","label":"TERADATA"}},{"condition":[{"rule":"equal","value":"ORACLE","key":"DATA_SOURCE"}],"disable":false,"defaultValue":{"value":"NET8","label":"NET8"}},{"condition":[{"rule":"equal","value":"DRDA","key":"DATA_SOURCE"}],"disable":false,"defaultValue":{"value":"DRDA","label":"DRDA"}},{"condition":[{"rule":"equal","value":"JDBC","key":"DATA_SOURCE"}],"disable":false,"defaultValue":{"value":"JDBC","label":"JDBC"}},{"condition":[{"rule":"equal","value":"ODBC","key":"DATA_SOURCE"}],"disable":false,"defaultValue":{"value":"odbc","label":"odbc"}}],"name":"WRAPNAME","label":"Wrapper","valueType":"string","defaultValue":{"value":"DRDA","label":"DRDA"}},
		 * {"dependencies":[{"condition":[{"rule":"equal","value":"TERADATA","key":"DATA_SOURCE"}],"disable":false,"defaultValue":{"value":"Yes","label":"Yes"}},{"condition":[{"rule":"equal","value":"ORACLE","key":"DATA_SOURCE"}],"disable":false,"defaultValue":{"value":"No","label":"No"}},{"condition":[{"rule":"equal","value":"DRDA","key":"DATA_SOURCE"}],"disable":false,"defaultValue":{"value":"No","label":"No"}},{"condition":[{"rule":"equal","value":"JDBC","key":"DATA_SOURCE"}],"disable":false,"defaultValue":{"value":"No","label":"No"}},{"condition":[{"rule":"equal","value":"ODBC","key":"DATA_SOURCE"}],"disable":false,"defaultValue":{"value":"No","label":"No"}}],"name":"DB2_FENCED","label":"DB2 fenced","valueType":"boolean","defaultValue":{"value":"No","label":"No"}},
		 * {"name":"OS","label":"Operating system","valueType":"readOnly","defaultValue":{"value":"Linux","label":""}}]}
		 * */

		String query = alterService.getJSONData( "getCreateWrapperDefinition_LUW" );

		JSONObject resObj =
				alterService.callCreateDefinitionService(query, dbProfile );

		// Get the response Data
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "items" ) );
		JSONArray items = responseData.getJSONArray( "items" );

		Assert.assertNotNull( items );

		JSONObject jo = null;
		
		jo = (JSONObject) items.get(0);
		Assert.assertTrue(jo.containsKey("values"));
		Assert.assertTrue(jo.containsKey("defaultValue"));
		Assert.assertEquals(jo.get("valueType"),"enum");
		Assert.assertEquals(jo.get("name"),"DATA_SOURCE");
		
		jo = (JSONObject) items.get(1);
		Assert.assertTrue(jo.containsKey("dependencies"));
		Assert.assertTrue(jo.containsKey("defaultValue"));
		Assert.assertTrue(jo.containsKey("validation"));
		Assert.assertEquals(jo.get("valueType"),"string");
		Assert.assertEquals(jo.get("name"),"WRAPNAME");
		
		jo = (JSONObject) items.get(2);
		Assert.assertTrue(jo.containsKey("dependencies"));
		Assert.assertTrue(jo.containsKey("defaultValue"));
		Assert.assertEquals(jo.get("valueType"),"boolean");
		Assert.assertEquals(jo.get("name"),"DB2_FENCED");
		
		jo = (JSONObject) items.get(3);
		Assert.assertTrue(jo.containsKey("defaultValue"));
		Assert.assertEquals(jo.get("valueType"),"boolean");
		Assert.assertEquals(jo.get("name"),"DB2_SOURCE_CLIENT_MODE");
		
		jo = (JSONObject) items.get(4);
		Assert.assertTrue(jo.containsKey("defaultValue"));
		Assert.assertEquals(jo.get("valueType"),"validation_text");
		Assert.assertTrue(jo.containsKey("validation"));
		Assert.assertEquals(jo.get("name"),"MODULE");

		jo = (JSONObject) items.get(5);
		Assert.assertTrue(jo.containsKey("defaultValue"));
		Assert.assertEquals(jo.get("valueType"),"readOnly");
		Assert.assertEquals(jo.get("name"),"OS");
		
		jo = (JSONObject) items.get(6);
		Assert.assertEquals(jo.get("valueType"),"enum");
		Assert.assertEquals(jo.get("name"),"WRAPPERVALUES");

		logger.info("PASSED: create wrapper definition executed successfully");
	}
	
	/**
	 * Test the drop wrapper action in admin part
	 * */
	@Test(description = "Test the drop wrapper action in admin part")
	public void testDropWrapper() throws InterruptedException {

		logger.info("Running Drop Wrapper...");

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * */
		

		
		String expected[] = {"DROP WRAPPER \"DRDA1\"","DROP WRAPPER \"NET8\""};

		assertEquals("getDropWrapper_LUW", expected);

		logger.info("PASSED: Alter drop executed successfully");

	}
}
