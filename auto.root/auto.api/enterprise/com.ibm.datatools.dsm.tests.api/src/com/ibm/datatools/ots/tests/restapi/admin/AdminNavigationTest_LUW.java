package com.ibm.datatools.ots.tests.restapi.admin;


import net.sf.json.JSONObject;
import net.sf.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.AdminNavigationService;
import com.ibm.datatools.ots.tests.restapi.common.AdminSQLEditorService;
import com.ibm.datatools.test.utils.AdminConst;
import com.jayway.jsonpath.JsonPath;


public class AdminNavigationTest_LUW extends AdminTestBase{

	private static final Logger logger = LogManager.getLogger(AdminNavigationTest_LUW.class);
	
	private AdminNavigationService navigationService = null ;
	private AdminSQLEditorService sqlEditorService = null ;
	
	@BeforeClass
	public void beforeTest(){
		navigationService = new AdminNavigationService();
		sqlEditorService = new AdminSQLEditorService() ;
		testNavigateTableTao_bf() ;
		testNavigateIndex_precondition();
		testNavigateConstraints_precondition();
		testNavigateStorageGroup_preconditon();
		testNavigateBufferPool_precondition();
		testNavigatePackages_preconditon();
		testNavigateAlias_precondition();
		testNavigateMQT_precondition();
	}
	
	@AfterClass
	public void afterTest(){
		testNavigateTableTao_af() ;
		testNavigateIndex_clean();
		testNavigateConstraints_clean();
		testNaviagateStorageGroup_clean();
		testNavigateBufferPool_clean();
		testNavigatePackages_clean();
		testNavigateAlias_clean();
		testNavigateMQT_clean();
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Prepare data for MQT navigating testing
	 */
	private void testNavigateMQT_precondition(){
		dropMQT_MQT(false);
		dropTable_MQT(false);
		createTable_MQT();
		createMQT_MQT();
		
	}
	private void testNavigateMQT_clean(){
		dropMQT_MQT(false);
		dropTable_MQT(false);
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Prepare data for Alias Navigating testing
	 */
	private void testNavigateAlias_precondition(){
		drop101Tables_Alias(false);
		drop101Alias_Alias(false);
		create101Tables_Alias();
		create101Alias_Alias();
	}
	private void testNavigateAlias_clean(){
		drop101Tables_Alias(true);
		drop101Alias_Alias(true);
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Prepare 100 Packages for navigate Package testing
	 */
	private void testNavigatePackages_preconditon(){
		drop100Packages_Package(false);
		dropTable_Package(false);
		createTable_Package(true);
		create100Packages_Package(true);
	}
	private void testNavigatePackages_clean(){
		drop100Packages_Package(true);
		dropTable_Package(true);
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Prepare BufferPools for navigating bufferpools testing
	 */
	private void testNavigateBufferPool_precondition(){
		drop101BufferPools(false);
		create101BufferPools();
	}
	private void testNavigateBufferPool_clean(){
		drop101BufferPools(true);
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Prepare Storage Group for viewing testing
	 */
	private void testNavigateStorageGroup_preconditon(){
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97)){
			return;}
		dropStorrageGroup_SG(false);
		create100StorageGroup_SG();
	}
	private void testNaviagateStorageGroup_clean(){
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97)){
			return;}
		dropStorrageGroup_SG(true);
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Prepare data for navigate index testing
	 */
	private void testNavigateIndex_precondition(){
		drop101Indexs_Index(false);
		dropTable100Columns_Index(false);
		dropSchema_Index(false);
		createSchema_Index();
		createTable100Columns_Index();
		create101Indexs_Index();
		
	}
	private void testNavigateConstraints_precondition(){
		dropTable4Constraints(false);
		dropSchema4Constraints(false);
		createSchema4Constraints();
		createTable100ColumnsWith101Constraints();
		
	}
	private void testNavigateConstraints_clean(){
		dropTable4Constraints(true);
		dropSchema4Constraints(true);
	}
	private void createTable_MQT() {
		List<String> createList = new ArrayList<String>();

		String fieldsSql = null;
		for (int i = 1; i <= 100; i++) {
			if (i == 1) {
				fieldsSql = "field1 int not null";
			} else {
				fieldsSql = fieldsSql + ",field" + i + " int not null";
			}
		}

		String constraintsSql = null;
		for (int i = 2; i <= 100; i++) {
			if (i == 2) {
				constraintsSql = "CONSTRAINT UNIQUE_CONSTRAINT_TEST" + i + " UNIQUE (field" + i + ")";
			} else {
				constraintsSql = constraintsSql + ", CONSTRAINT UNIQUE_CONSTRAINT_TEST" + i + " UNIQUE (field" + i
						+ ") ";
			}
		}

		createList.add(
		"CREATE TABLE TABLE4MQTTEST (" + fieldsSql + ", CONSTRAINT PRIMARY_CONSTRAINT_TEST PRIMARY KEY (field1), "
				+ constraintsSql + "); ");

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}
	private void dropTable_MQT(boolean real) {
		sqlEditorService.runSQLinJDBC("Drop table TABLE4MQTTEST;", ";", this.dbProfile,real);
	}

	/**
	 * @author ybjwang@cn.ibm.com	
	 * create MQT to veirfy naviagating MQT testing 
	 */
	private void createMQT_MQT() {
		// TODO Auto-generated method stub
		List<String> createList = new ArrayList<String>();

		for (int i = 1; i <= 100; i++) {
			createList.add("CREATE TABLE MQTTEST4MQT" + i + " as (select a.field" + i + " field" + i
					+ "MQT from TABLE4MQTTEST a) DATA INITIALLY DEFERRED REFRESH DEFERRED MAINTAINED BY SYSTEM");
		}

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}
	private void dropMQT_MQT(boolean real) {
		List<String> createList = new ArrayList<String>();

		for (int i = 1; i <= 100; i++) {
			createList.add("Drop table MQTTEST4MQT" + i + ";");
		}

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, real);
	}
	private void create101Tables_Alias() {
		List<String> createList = new ArrayList<String>();

		for (int i = 0; i <= 100; i++) {
			createList.add("CREATE TABLE TABLETEST4ALIAS" + i + "(field1 int);");
		}

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}

	private void create101Alias_Alias() {
		List<String> createList = new ArrayList<String>();

		for (int i = 0; i <= 100; i++) {
			createList.add("CREATE ALIAS ALIAS_T4ALIAS" + i + " FOR TABLETEST4ALIAS" + i + ";");
		}

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}

	private void drop101Tables_Alias(boolean real) {
		List<String> createList = new ArrayList<String>();

		for (int i = 0; i <= 100; i++) {
			createList.add("Drop TABLE TABLETEST4ALIAS" + i + ";");
		}

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, real);
	}

	private void drop101Alias_Alias(boolean real) {
		List<String> createList = new ArrayList<String>();

		for (int i = 0; i <= 100; i++) {
			createList.add("DROP ALIAS ALIAS_T4ALIAS" + i + ";");
		}

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, real);
	}
	private void createTable_Package(boolean real){
		List<String> createList = new ArrayList<String>();
		createList.add("CREATE SCHEMA TESTPACKAGE_SCH@"+
					   "CREATE TABLE TESTPACKAGE_SCH.T1( "+
					   "C1  INTEGER NOT NULL UNIQUE, "+
					   "C2  CHAR(5) NOT NULL, "+
					   "C3  VARCHAR(5), "+
					   "C4  INTEGER NOT NULL)@");
		sqlEditorService.runSQLinJDBC(createList, "@", this.dbProfile, real);
	}
	private void create100Packages_Package(boolean real){
		String sql="";
		String sqlStatement="";
		for(int i=1;i<101;i++){
			sqlStatement="CREATE PROCEDURE TESTPACKAGE_SCH.PROCED_T"+i+"(IN PRO1 INTEGER, "+
						 "OUT  PRO2 VARCHAR(5)) "+
						 "LANGUAGE SQL "+
						 "SPECIFIC  TESTPACKAGE_SCH.PROCED_T"+i+" "+
						 "BEGIN "+
						 "SELECT C3 INTO PRO2 FROM TESTPACKAGE_SCH.T1 WHERE C1=PRO1; "+
						 "END@";
			sql=sql+sqlStatement;
		}
		sqlEditorService.runSQLinJDBC(sql, "@", this.dbProfile,real);
	}
	private void dropTable_Package(boolean real){
		List<String> dropList = new ArrayList<String>();
		dropList.add("DROP TABLE TESTPACKAGE_SCH.T1;");
		dropList.add("DROP SCHEMA TESTPACKAGE_SCH RESTRICT;");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, real);
	}
	private void drop100Packages_Package(boolean real){
		String sql ="";
		String sqlStatement = "";
		for(int i=1;i<101;i++){
			sqlStatement="DROP PROCEDURE TESTPACKAGE_SCH.PROCED_T"+i+";";
			sql=sql+sqlStatement;
		}
		sqlEditorService.runSQLinJDBC(sql, ";", this.dbProfile,real);
	}
	private void createWrapper_Federation(){
		List<String> createList = new ArrayList<String>();
		
		createList.add("CREATE WRAPPER WAPPERDRDA LIBRARY 'libdb2drda.so' OPTIONS ( ADD DB2_FENCED 'N' )");
	
		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile,true);	
		
	}
	private void dropWrapper_Federation(){
		List<String> dropList = new ArrayList<String>();
			dropList.add("DROP WRAPPER WAPPERDRDA");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile,true);	
		
	}
	private void create100StorageGroup_SG(){
		List<String> createList = new ArrayList<String>();
		for(int i=0;i<100;i++){
			createList.add("CREATE STOGROUP STGRP4STGTEST" + i + " ON \'/tmp\'");
		}
		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile,true);	
	}
	private void dropStorrageGroup_SG(boolean real){
		List<String> dropList = new ArrayList<String>();
		for(int i=0;i<100;i++){
			dropList.add("DROP STOGROUP STGRP4STGTEST" +i);
		}
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile,real);
	}
	private void create101BufferPools() {
		List<String> createList = new ArrayList<String>();

		for (int i = 0; i <= 100; i++) {
			createList.add("CREATE BUFFERPOOL BFTEST" + i + ";");
		}

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}

	private void drop101BufferPools(boolean real) {
		List<String> dropList = new ArrayList<String>();
		for (int i = 0; i <= 100; i++) {
			dropList.add("DROP BUFFERPOOL BFTEST" + i + ";");
		}
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, real);
	}
	private void createSchema4Constraints(){
		sqlEditorService.runSQLinJDBC("CREATE SCHEMA SCHEMA4CONSTRAINTS", ";", this.dbProfile,true);	
	}
	public void dropSchema4Constraints(boolean real){
		
		sqlEditorService.runSQLinJDBC("DROP SCHEMA SCHEMA4CONSTRAINTS RESTRICT", ";", this.dbProfile,real);
	}
	public void dropTable4Constraints(boolean real){
		sqlEditorService.runSQLinJDBC("DROP TABLE SCHEMA4CONSTRAINTS.Col100Table4CONS ", ";", this.dbProfile,real);
	}
	private void createTable100ColumnsWith101Constraints() {
		//CREATE TABLE Col100Table4CONS 
		//(FIELD1 INT NOT NULL,FIELD2 INT NOT NULL,
		//CONSTRAINT PRIMARY_CONSTRAINT_TEST PRIMARY KEY (field1), 
		//CONSTRAINT CHECK_CONSTRAINT_TEST CHECK (field2 IN(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)),
		//CONSTRAINT UNIQUE_CONSTRAINT_TEST2  UNIQUE (field2),
		//CONSTRAINT UNIQUE_CONSTRAINT_TEST3 UNIQUE (field3)) ;
		List<String> createList = new ArrayList<String>();

		String fieldsSql = null;
		for (int i = 1; i <= 100; i++) {
			if (i == 1) {
				fieldsSql = "field1 int not null";
			} else {
				fieldsSql = fieldsSql + ",field" + i + " int not null";
			}
		}

		String constraintsSql = null;
		for (int i = 2; i <= 100; i++) {
			if (i == 2) {
				constraintsSql = "CONSTRAINT UNIQUE_CONSTRAINT_TEST" + i + " UNIQUE (field" + i + ")";
			} else {
				constraintsSql = constraintsSql + ", CONSTRAINT UNIQUE_CONSTRAINT_TEST" + i + " UNIQUE (field" + i
						+ ") ";
			}
		}

		createList.add("CREATE TABLE SCHEMA4CONSTRAINTS.Col100Table4CONS (" + fieldsSql
				+ ", CONSTRAINT PRIMARY_CONSTRAINT_TEST PRIMARY KEY (field1), "
				+ "CONSTRAINT CHECK_CONSTRAINT_TEST CHECK (field2 IN(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)), " + constraintsSql
				+ ") ; ");

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Clean all the data that prepared for Index testing
	 */
	private void testNavigateIndex_clean(){
		drop101Indexs_Index(true);
		dropTable100Columns_Index(true);
		dropSchema_Index(true);
	}
	private void createSchema_Index(){
		sqlEditorService.runSQLinJDBC("CREATE SCHEMA SCHEMA4INDEXTEST", ";", this.dbProfile,true);	
	}
	private void dropSchema_Index(boolean real){
		
		sqlEditorService.runSQLinJDBC("DROP SCHEMA SCHEMA4INDEXTEST RESTRICT", ";", this.dbProfile,real);
	}
	private void createTable100Columns_Index() {
		String sqlStatement = null;
		sqlStatement = "create table SCHEMA4INDEXTEST.Col100Table (";
		String fieldsSql = null;
		for (int i = 1; i <= 100; i++) {
			if (i == 1) {
				fieldsSql = "field1 int";
			} else {
				fieldsSql = fieldsSql + ",field" + i + " int";
			}
		}
		sqlStatement = sqlStatement + fieldsSql + ");";

		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
	}

	private void dropTable100Columns_Index(boolean real) {
		sqlEditorService.runSQLinJDBC("Drop table SCHEMA4INDEXTEST.Col100Table;", ";", this.dbProfile,real);
	}

	private void create101Indexs_Index() {
		List<String> createList = new ArrayList<String>();
		createList.add("CREATE INDEX SCHEMA4INDEXTEST.INDTEST0 ON SCHEMA4INDEXTEST.COL100TABLE (FIELD1 ASC,FIELD2 ASC);");

		for (int i = 1; i <= 100; i++) {
			createList.add("CREATE INDEX SCHEMA4INDEXTEST.INDTEST" + i + " ON SCHEMA4INDEXTEST.COL100TABLE (FIELD" + i + " ASC);");
		}

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}

	private void drop101Indexs_Index(boolean real) {
		List<String> dropList = new ArrayList<String>();
		for (int i = 0; i <= 100; i++) {
			dropList.add("DROP INDEX SCHEMA4INDEXTEST.INDTEST" + i + ";");
		}
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, real);
	}

	//************************************* This is a sample of showing how to create env using sql******************************	
	public void testNavigateTableTao_bf(){
		sqlEditorService.runSQLinJDBC("create table TAO (a int,b int);", ";", this.dbProfile) ;
	}
	public void testNavigateTableTao_af(){
		sqlEditorService.runSQLinJDBC("Drop table TAO;", ";", this.dbProfile) ;
	}
	@Test(description="A test case to check whether Table Tao is exists")
	 public void testNavigateTableTao() throws InterruptedException{
		JSONObject[] responseData = sqlEditorService.runSQLinJDBC("Select 1 from SYSIBM.SYSTABLES WHERE NAME='TAO'", ";", this.dbProfile) ;
	 }
	
//*********************************************************************************************************************************	
	
	
	/**
	 * Test the tables layout
	 * */
	@Test(description="Test the retrieve table layout function")
	public void testNavigateTableLayout(){
		
		logger.info("Running Navigate Table layout test...");
		
		String query = navigationService.getJSONData("getNavigateTableLayout");
		JSONObject resObj =navigationService.callNavigationService(AdminNavigationService.TYPE_NAVIGATION_URL,query,dbProfile);
		
		//Get the response Data
		JSONObject responseData =(JSONObject) resObj.get("ResponseData");
		
		/**
		 * {"cells":[
		 *0 	{"fixedPosition":true,"field":"CHECKBOX","dataType":"string","filterable":false,"alwaysShown":"true","name":" ","datatype":"string","internal":true}
		 *1 ,{"fixedPosition":true,"field":"ICON","iconDisplay":"true","dataType":"string","filterable":false,"alwaysShown":"true","itemLabelKey":" ","datatype":"string","internal":true}
		 *2 ,{"shouldFormat":"true","field":"TABNAME","dataType":"string","alwaysShown":"true","name":"Table","applyTooltip":true,"datatype":"string"}
		 *3 ,{"field":"TABSCHEMA","dataType":"string","alwaysShown":"true","name":"Schema","datatype":"string"}
		 *4 ,{"field":"OWNER","dataType":"string","hidden":true,"name":"Owner","datatype":"string"}
		 *5 ,{"field":"TABLEORG","dataType":"string","alwaysShown":"false","name":"Organization","datatype":"string"}
		 *6 ,{"field":"TEMPORALTYPE","dataType":"string","hidden":true,"name":"Temporal type","datatype":"string"}
		 *7 ,{"field":"TBSPACE","dataType":"string","name":"Table space","datatype":"string"}
		 *8 ,{"field":"ROWCOUNT","dataType":"number","alwaysShown":"false","name":"Cardinality","datatype":"number"}
		 *9 ,{"field":"DISTRIBUTION_TYPE","dataType":"string","hidden":true,"name":"Type","datatype":"string"}
		 *10 ,{"field":"CREATE_TIME","dataType":"string","name":"Created","datatype":"string"}
		 *11 ,{"field":"LASTUSED","dataType":"string","dateOnly":"true","name":"Last used","datatype":"string"}
		 *12 ,{"field":"STATS_TIME","dataType":"string","name":"Statistic time","datatype":"string"}
		 *13 ,{"field":"ALTER_TIME","dataType":"string","hidden":true,"name":"Altered","datatype":"string"}
		 *14 ,{"field":"PCTFREE","dataType":"number","alwaysShown":"false","name":"Free(%)","datatype":"number"}
		 *15 ,{"field":"DATACAPTURE","dataType":"boolean","alwaysShown":"false","name":"Data capture","datatype":"boolean"}
		 *16 ,{"fixedPosition":true,"field":"TABTYPE","dataType":"string","filterable":false,"hidden":true,"name":"Table type","datatype":"string","internal":true}
		 *17 ,{"fixedPosition":true,"field":"STEREOTYPE","dataType":"string","filterable":false,"hidden":true,"name":"Stereotype ","datatype":"string","internal":true}
		 *18 ,{"field":"ROW_COMPRESSION","dataType":"string","name":"Compression","datatype":"string"}
		 *19 ,{"field":"ROW_COMPRESSION_PAGE_SAVED","dataType":"number","hidden":true,"alwaysShown":"false","name":"Pages saved","datatype":"number"}
		 *20 ,{"fixedPosition":true,"field":"VERSIONINGTABSCHEMA","dataType":"number","filterable":false,"hidden":true,"name":"Versioning table schema","datatype":"number","internal":true}
		 *21 ,{"field":"VERSIONINGTABNAME","dataType":"number","hidden":true,"name":"Versioning table name","datatype":"number"}
		 *22 ,{"field":"OWNERTYPE","dataType":"string","hidden":true,"name":"Owner type","datatype":"string"}
		 *23 ]}
		 * 
		 * */
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);
		
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[0].field"), "CHECKBOX");
		//Assert.assertEquals(JsonPath.read(responseData, "$.cells[1].field"), "ICON");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[1].field"), "TABNAME"); 
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[1].shouldFormat"), "true");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[2].field"), "TABSCHEMA"); 
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[2].alwaysShown"), "true");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[3].field"), "OWNER");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[3].hidden") + "", "true");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[4].field"), "TABLEORG");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[4].alwaysShown"), "false");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[5].field"), "TEMPORALTYPE");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[6].field"), "TBSPACE");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[7].field"), "ROWCOUNT");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[8].field"), "DISTRIBUTION_TYPE");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[9].field"), "CREATE_TIME");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[10].field"), "LASTUSED");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[11].field"), "STATS_TIME");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[12].field"), "ALTER_TIME");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[13].field"), "PCTFREE");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[14].field"), "DATACAPTURE");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[15].field"), "TABTYPE");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[16].field"), "STEREOTYPE");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[17].field"), "ROW_COMPRESSION");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[18].field"), "ROW_COMPRESSION_PAGE_SAVED");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[19].field"), "VERSIONINGTABSCHEMA");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[20].field"), "VERSIONINGTABNAME");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[21].field"), "OWNERTYPE");
		
		
		logger.info("PassedED: Navigate Table layout test executed successfully");
	}
	
	/**
	 * Test the retrieve the tables function, by changing the maxRow, we expected different rows returned.
	 * */
	@Test(description="Test the retrieve tables function")
	public void testNavigateTable() throws InterruptedException{
		
		logger.info("Running Alter Table Name...");

	
		//The key defined in the properties
		String query = navigationService.getJSONData("getNavigateTable");
		
		JSONObject resObj =navigationService.callNavigationService(AdminNavigationService.TYPE_NAVIGATION_URL,query,dbProfile);
		
		//Get the response Data
		JSONObject responseData =(JSONObject) resObj.get("ResponseData");
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);
		

		/**
		 * Path the response data, for this case, the response data is:
		 * the returned data is all the tables in in the selected database,we need to verify there is table returned back.
		 * JSONPath $..TABNAME means all the TableName in the result JSON
		 * To retrieve the statement, we are looking for the first index of the items array -> the return is a object, we look for the value of key "statement", that is
		 * $.items[0].statement
		 * */
		
		
		//sometimes it will post the error "net.minidev.json.JSONArray cannot be cast to net.sf.json.JSONArray"
		//JSONArray array = JsonPath.read(responseData, "$..TABNAME");				
		JSONArray array = responseData.getJSONArray("items"); 
		
		int firstSize = array.size() ;
		Assert.assertTrue(firstSize > 0 && firstSize <= 100);
		
		if(array.size() != 100){
			
			/**
			 * Further Test
			 * If there exists less than 99 table in the database, we do another test to change the predicate to "predicate":{"maxrows":<less than 99> ...
			 * 
			 * Such as we returned 20 rows, we change the max row to 20-1, then we expected that 19 rows will returned. 
			 * */
			JSONObject jsonObj = JSONObject.fromObject(query);
			
			JSONObject predicates = jsonObj.getJSONObject("predicate");
			Assert.assertNotNull(predicates);
			//We use size - 1 as the new maxRow.
			predicates.put("maxrows", firstSize-1);

			resObj =navigationService.callNavigationService(AdminNavigationService.TYPE_NAVIGATION_URL,jsonObj.toString(),dbProfile);

			//Get the response Data
			responseData =(JSONObject) resObj.get("ResponseData");
			Assert.assertNotNull(responseData);
			Assert.assertTrue(responseData.toString().length() > 0);
			
		
			/**
			 * verify there are size -1 number of row returned.
			 * */
			array = JsonPath.read(responseData, "$..TABNAME");
			int newsize = array.size() ;
			Assert.assertEquals(newsize,firstSize-1);
			
			
			/**
			 * Further Test
			 * 
			 * If there exists less than 99 table in the database, we do another test to change the predicate to "predicate":{"maxrows":<less than 99> ...
			 * 
			 * Such as we returned 20 rows, we change the max row to 101, then we still expect that 20 rows will returned. 
			 * */
			
			jsonObj = JSONObject.fromObject(query);
			
			predicates = jsonObj.getJSONObject("predicate");
			Assert.assertNotNull(predicates);
			//We use size - 1 as the new maxRow.
			predicates.put("maxrows", 101);

			resObj =navigationService.callNavigationService(AdminNavigationService.TYPE_NAVIGATION_URL,jsonObj.toString(),dbProfile);

			//Get the response Data
			responseData =(JSONObject) resObj.get("ResponseData");
			Assert.assertNotNull(responseData);
			Assert.assertTrue(responseData.toString().length() > 0);
			
		
			/**
			 * verify there are size -1 number of row returned.
			 * */
			array = JsonPath.read(responseData, "$..TABNAME");
			Assert.assertEquals(array.size(),firstSize);
		}
		
		logger.info("PassedED: AlterTable Name executed successfully");
		
	}
	
	/**
	 * Test the retrieve the views function, by changing the maxRow, we expected different rows returned.
	 * */
	@Test(description="Test the retrieve views function")
	public void testNavigateView() throws InterruptedException{
		
		logger.info("Running Navigate Views...");

	
		//The key defined in the properties
		String query = navigationService.getJSONData("getNavigateView");
		
		JSONObject resObj =navigationService.callNavigationService(AdminNavigationService.TYPE_NAVIGATION_URL,query,dbProfile);
		
		//Get the response Data
		JSONObject responseData =(JSONObject) resObj.get("ResponseData");
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);		

		
		//sometimes it will post the error "net.minidev.json.JSONArray cannot be cast to net.sf.json.JSONArray"
		//JSONArray array = JsonPath.read(responseData, "$..VIEWNAME");			
		JSONArray array = responseData.getJSONArray("items"); 
		
		
		int firstSize = array.size() ;
		
		int maxrows=JSONObject.fromObject(query).getJSONObject("predicate").getInt("maxrows");
		Assert.assertTrue(firstSize > 0 && firstSize <= maxrows);
		
		if(array.size() != maxrows){
			
			/**
			 * Further Test
			 * If there exists less than maxrows views in the database, we do another test to change the predicate to "predicate":{"maxrows":<less than maxrows> ...
			 * 
			 * Such as we returned 20 rows, we change the max row to 20-1, then we expected that 19 rows will returned. 
			 * */
			JSONObject jsonObj = JSONObject.fromObject(query);
			
			JSONObject predicates = jsonObj.getJSONObject("predicate");
			Assert.assertNotNull(predicates);
			//We use size - 1 as the new maxRow.
			predicates.put("maxrows", firstSize-1);

			resObj =navigationService.callNavigationService(AdminNavigationService.TYPE_NAVIGATION_URL,jsonObj.toString(),dbProfile);

			//Get the response Data
			responseData =(JSONObject) resObj.get("ResponseData");
			Assert.assertNotNull(responseData);
			Assert.assertTrue(responseData.toString().length() > 0);
			
		
			/**
			 * verify there are size -1 number of row returned.
			 * */
			array = JsonPath.read(responseData, "$..VIEWNAME");
			int newsize = array.size() ;
			Assert.assertEquals(newsize,firstSize-1);
			
			
			/**
			 * Further Test
			 * 
			 * If there exists less than 99 views in the database, we do another test to change the predicate to "predicate":{"maxrows":<less than 99> ...
			 * 
			 * Such as we returned 20 rows, we change the max row to 101, then we still expect that 20 rows will returned. 
			 * */
			
			jsonObj = JSONObject.fromObject(query);
			
			predicates = jsonObj.getJSONObject("predicate");
			Assert.assertNotNull(predicates);
			predicates.put("maxrows", firstSize+1);

			resObj =navigationService.callNavigationService(AdminNavigationService.TYPE_NAVIGATION_URL,jsonObj.toString(),dbProfile);

			//Get the response Data
			responseData =(JSONObject) resObj.get("ResponseData");
			Assert.assertNotNull(responseData);
			Assert.assertTrue(responseData.toString().length() > 0);
			
		
			/**
			 * verify there are size -1 number of row returned.
			 * */
			array = JsonPath.read(responseData, "$..VIEWNAME");
			Assert.assertEquals(array.size(),firstSize);
		}
		
		logger.info("PassedED: Navigation View executed successfully");
		
	}
	/**
	 * 
	 * @author ybjwang@cn.ibm.com
	 * Test the navigate 100 index list.
	 */
	@Test(description = "DB has at least 100 indexes open index list with maxrows=100, verify request that retrieves indexes")
	public void testNavigateIndexsList100() throws InterruptedException {

		logger.info("Running Navigate Indexs list...");
		// Get the response Data and verify if thre are 100 data shown
		assertObject100List("getNavigateIndex");
		logger.info("Running Navigate Indexs Passeded");
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test the Index Layout
	 */
	@Test(description="Test Index layout when clicking through Admin--Index")
	public void testNavigateIndexLayout() throws InterruptedException{
		logger.info("Running Navigate Index Layout...");
				// Get the response Data
		JSONObject responseData  = getResponseData("getNavigateIndexLayout");
		Assert.assertNotNull(responseData);
		/**
		 * Response Data for Index Layout
		 * {"cells":[
		 *0	 {"filterable":false,"internal":true,"field":"CHECKBOX","alwaysShown":"true","datatype":"string","dataType":"string","name":" ","fixedPosition":true},
		 *1	 {"field":"INDNAME","alwaysShown":"true","datatype":"string","applyTooltip":true,"dataType":"string","name":"\u7d22\u5f15","shouldFormat":"true"},
		 *2	 {"field":"TABNAME","alwaysShown":"true","datatype":"string","dataType":"string","name":"\u8868\u540d\u79f0","shouldFormat":"true"},
		 *3	 {"field":"TABSCHEMA","alwaysShown":"true","datatype":"string","dataType":"string","name":"\u8868\u6a21\u5f0f"},
		 *4	 {"field":"INDSCHEMA","alwaysShown":"true","datatype":"string","dataType":"string","name":"\u7d22\u5f15\u6a21\u5f0f"},
		 *5	 {"field":"INDEXTYPE","alwaysShown":"true","datatype":"string","dataType":"string","name":"\u7d22\u5f15\u7c7b\u578b"},
		 *6	 {"field":"OWNER","datatype":"string","dataType":"string","name":"\u6240\u6709\u8005"},
		 *7  {"field":"OWNERTYPE","hidden":true,"datatype":"string","dataType":"string","name":"\u6240\u6709\u8005\u7c7b\u578b"},
		 *8	 {"dateOnly":"true","field":"LASTUSED","hidden":true,"datatype":"string","dataType":"string","name":"\u4e0a\u6b21\u4f7f\u7528\u65f6\u95f4"},
		 *9	 {"field":"STATS_TIME","datatype":"string","applyTooltip":true,"dataType":"string","name":"\u7edf\u8ba1\u4fe1\u606f\u65f6\u95f4"},
		 *10 {"filterable":false,"internal":true,"field":"STEREOTYPE","hidden":true,"datatype":"string","dataType":"string","name":"\u6784\u9020\u578b","fixedPosition":true},
		 *11 {"filterable":false,"internal":true,"field":"SYSTEN_GENERATED","hidden":true,"datatype":"string","dataType":"string","name":"\u7cfb\u7edf\u751f\u6210\u7684\u7d22\u5f15","fixedPosition":true}]}

		 */
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[0].field"), "CHECKBOX");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[1].field"), "INDNAME"); 
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[1].shouldFormat"), "true");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[2].field"), "TABNAME"); 
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[2].alwaysShown"), "true");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[3].field"), "TABSCHEMA");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[3].alwaysShown"), "true");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[4].field"), "INDSCHEMA");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[4].alwaysShown"), "true");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[5].field"), "INDEXTYPE");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[6].field"), "OWNER");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[7].field"), "OWNERTYPE");
		Assert.assertTrue((boolean)JsonPath.read(responseData, "$.cells[7].hidden"));
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[8].field"), "LASTUSED");
		Assert.assertTrue((boolean)JsonPath.read(responseData, "$.cells[8].hidden"));
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[9].field"), "STATS_TIME");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[10].field"), "STEREOTYPE");
		Assert.assertTrue((boolean)JsonPath.read(responseData, "$.cells[10].hidden"));
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[11].field"), "SYSTEN_GENERATED");
		Assert.assertTrue((boolean)JsonPath.read(responseData, "$.cells[11].hidden"));	}
	/**
	 * A common Method for get response  data by parsing the json File
	 * @param jsonFile
	 * @return
	 */
	protected JSONObject getResponseData(String jsonFile){
		
		String query = navigationService.getJSONData(jsonFile);

		JSONObject resObj = navigationService.callNavigationService(AdminNavigationService.TYPE_NAVIGATION_URL, query,
				dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		
		return responseData;
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test the Index property.
	 * Because the order of MEMBER is unstable, so we should not use compareProBaseAndProResult in testNavigateIndexProperty
	 */
	@Test(description = "Click an index to open its properties, verify request that returns Properties info")
	public void testNavigateIndexProperty() throws InterruptedException{

		logger.info("Running Navigate Index Property...");

		compareProBaseAndProResultWithOrderKey("getNavigateIndexProperty_Index", "getNavigateIndexProperty_result_Index", new ArrayList<String>(){{add("MEMBER");}});

		logger.info("PassedED: Get the Index's properties executed successfully");
	}
/**
 *  Compare two Object data ,one is from the response data by posting showAllRow command to get one Object property 
 *  the other object's data is from reading the json file defined at getAdminNavigation.proprties
 * @param queryProperties
 * @param propertiesResult
 */
	protected void compareProBaseAndProResult(String queryProperties, String propertiesResult) {

		String query = navigationService.getJSONData(queryProperties);

		JSONObject resObj = navigationService.callNavigationService(AdminNavigationService.TYPE_NAVIGATION_URL, query,
				dbProfile);

		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);

		JSONObject propResult = JsonPath.read(responseData, "$.items[0].data");

		if (propResult == null) {
			propResult = responseData;
		}

		String base = navigationService.getJSONData(propertiesResult);

		JSONObject propBase = JSONObject.fromObject(base);

		Assert.assertTrue(compareJsons(propBase, propResult));
		
	}
	
	/**
	 *  Compare two Object data ,one is from the response data by posting showAllRow command to get one Object property 
	 *  the other object's data is from reading the json file defined at getAdminNavigation.proprties
	 * @param queryProperties
	 * @param propertiesResult
	 * @param keys
	 */
		protected void compareProBaseAndProResultWithOrderKey(String queryProperties, String propertiesResult, List<String> keys) {

			String query = navigationService.getJSONData(queryProperties);

			JSONObject resObj = navigationService.callNavigationService(AdminNavigationService.TYPE_NAVIGATION_URL, query,
					dbProfile);

			JSONObject responseData = (JSONObject) resObj.get("ResponseData");
			Assert.assertNotNull(responseData);
			Assert.assertTrue(responseData.toString().length() > 0);

			JSONObject propResult = JsonPath.read(responseData, "$.items[0].data");

			if (propResult == null) {
				propResult = responseData;
			}

			String base = navigationService.getJSONData(propertiesResult);

			JSONObject propBase = JSONObject.fromObject(base);
			
			for(int i = 0; i < keys.size(); i++){
				String baseKeyValue =  propBase.getString(keys.get(i));
				String[] baseKeyValueArr = null;
				if(baseKeyValue.contains(",")){
					baseKeyValueArr = baseKeyValue.split(",");
					Arrays.sort(baseKeyValueArr);
					propBase.put(keys.get(i), StringUtils.join(baseKeyValueArr, ","));
				}
				
				String resultKeyValue =  propResult.getString(keys.get(i));
				String[] resultKeyValueArr = null;
				if(baseKeyValue.contains(",")){
					resultKeyValueArr = resultKeyValue.split(",");
					Arrays.sort(resultKeyValueArr);
					propResult.put(keys.get(i), StringUtils.join(resultKeyValueArr, ","));
				}
			}

			Assert.assertTrue(compareJsons(propBase, propResult));
			
		}
	
	/**
	 * Test the constraints list.
	 */
	@Test(description = "DB has at least 100 constraints, open constraint list with maxrows=100, verify request that retrieves constraints")
	public void testNavigateConstraintsList100() throws InterruptedException {

		logger.info("Running Navigate Constraints list...");
		// Get the response Data and verify
		assertObject100List("getNavigateConstraint");
		logger.info("Running Navigate Constraints list Passeded");
	}
	
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test the Constrainsts Layout
	 */
	@Test(description="Test The Constrainsts Layout when navigate through Admin-->Constraints")
	public void testNavigateConstraintsLayout() throws InterruptedException{
		
		logger.info("Running Navigate Constraints list...");
		// Get the response Data
		JSONObject responseData = getResponseData("getNavigateConstraintLayout");
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);
		/**
		 * Response data:
		 * {"cells":[
		  0		{"field":"CONSTNAME","alwaysShown":"true","datatype":"string","applyTooltip":true,"dataType":"string","name":"\u7ea6\u675f","shouldFormat":"true"},
		  1		{"field":"TABNAME","alwaysShown":"true","datatype":"string","dataType":"string","name":"\u8868","shouldFormat":"true"},
		  2		{"field":"TABSCHEMA","alwaysShown":"true","datatype":"string","dataType":"string","name":"\u6a21\u5f0f"},
		  3		{"field":"TYPE","alwaysShown":"true","datatype":"string","dataType":"string","name":"\u7c7b\u578b"},
		  4		{"field":"ENFORCED","alwaysShown":"true","datatype":"boolean","dataType":"boolean","name":"\u5f3a\u5236"},
		  5		{"filterable":false,"internal":true,"field":"STEREOTYPE","hidden":true,"datatype":"string","dataType":"string","name":"\u6784\u9020\u578b","fixedPosition":true}]}
		 */
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[0].field"), "CONSTNAME");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[0].alwaysShown"), "true");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[1].field"), "TABNAME"); 
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[1].alwaysShown"), "true");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[2].field"), "TABSCHEMA"); 
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[2].alwaysShown"), "true");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[3].field"), "TYPE");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[3].alwaysShown"), "true");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[4].field"), "ENFORCED");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[4].alwaysShown"), "true");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[5].field"), "STEREOTYPE");
		Assert.assertTrue((boolean)JsonPath.read(responseData, "$.cells[5].hidden"));
		logger.info("Running Navigate 100 Constraints list Passed");
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test the Constrainsts Properties
	 */
	@Test(description="Test The Constrainsts Properties after selecting one Constraints")
	public void testNavigateConstraintsProperties() throws InterruptedException{
		
		logger.info("Running Navigate one Constraints properties...");
		compareProBaseAndProResult("getNavigateConstraintProperty", "getNavigateConstraintProperty_result");

		logger.info("PassedED: Get the Constraints properties executed successfully");
		
	}
	/**
	 * Test the Storage Group list.
	 */
	@Test(description = "DB has at least 100 StorageGroup, open StorageGroup list with maxrows=100, verify request that retrieves StorageGroup")
	public void testNavigateStorageGroupList100() throws InterruptedException {
		//Storage Group is not shown on UI side for DB2V9.7
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97)){
			return;}
		logger.info("Running Navigate StorageGroup list...");
		assertObject100List("getNavigateStorage");
		logger.info("Running Navigate 100 StorageGroup list Passed");
		
	}
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test the StoragGroup Layout
	 */
	@Test(description="Test The StorageGroup Layout when navigate through Admin-->Constraints")
	public void testNavigateStorageGroupLayout() throws InterruptedException{
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97)){
			return;}
		logger.info("Running Navigate StorageGroup list...");
		// Get the response Data
		JSONObject responseData = getResponseData("getNavigateStorageGroupLayout");
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);
		/**
		 * Response data:
		{"cells":[
		0	{"filterable":false,"internal":true,"field":"CHECKBOX","alwaysShown":"true","datatype":"string","dataType":"string","name":" ","fixedPosition":true},
		1	{"field":"SGNAME","alwaysShown":"true","datatype":"string","applyTooltip":true,"dataType":"string","name":"\u5b58\u50a8\u5668\u7ec4","shouldFormat":"true"},
		2	{"field":"OVERHEAD","alwaysShown":"true","datatype":"number","dataType":"number","name":"\u5f00\u9500\uff08\u6beb\u79d2\uff09","style":"text-align:right","sortable":false},
		3	{"field":"DEVICEREADRATE","alwaysShown":"true","datatype":"number","dataType":"number","name":"\u8bbe\u5907\u8bfb\u901f\u7387\uff08MB\/\u79d2\uff09","style":"text-align:right","sortable":false},
		4	{"field":"DATATAG","alwaysShown":"true","datatype":"string","dataType":"string","name":"\u6570\u636e\u6807\u8bb0"},
		5	{"field":"DEFAULTSG","alwaysShown":"true","datatype":"string","dataType":"string","name":"\u7f3a\u7701\u5b58\u50a8\u5668\u7ec4"},
		6	{"field":"CREATE_TIME","alwaysShown":"false","datatype":"string","applyTooltip":true,"dataType":"string","name":"\u521b\u5efa\u65f6\u95f4"}]}
		 */
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[0].field"), "CHECKBOX");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[0].alwaysShown"), "true");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[1].field"), "SGNAME"); 
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[1].alwaysShown"), "true");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[2].field"), "OVERHEAD"); 
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[2].alwaysShown"), "true");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[3].field"), "DEVICEREADRATE");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[3].alwaysShown"), "true");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[4].field"), "DATATAG");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[4].alwaysShown"), "true");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[5].field"), "DEFAULTSG");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[5].alwaysShown"), "true");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[6].field"), "CREATE_TIME");
		Assert.assertEquals(JsonPath.read(responseData, "$.cells[6].alwaysShown"), "false");
		logger.info("Running Navigate  StorageGroup Layout Passed");
	}
	
	/**
	 * @author ybjwang@cn.ibm.com
	 * Test the Constrainsts Properties
	 */
	@Test(description="Test The StorageGroup Properties after selecting one Constraints")
	public void testNavigateStorageGroupProperties() throws InterruptedException{
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97)){
			return;}
		
		logger.info("Running Navigate one StorageGroup properties...");
		compareProBaseAndProResult("getNavigateStorageGroupProperty", "getNavigateStorageGroupProperty_result");

		logger.info("PASSED: Get the StorageGroup properties executed successfully");
		
	}
	/**
	 * A common method to assert one Object List,to verify if the numRow is 100 and 
	 * return the success result Code with Object data.
	 * @param jsonFile
	 */
	private void assertObject100List(String jsonFile){
		
		// Get the response Data
		JSONObject responseData = getResponseData(jsonFile);
		logger.info(responseData);
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);

		Integer numRows = JsonPath.read(responseData, "$.numRows");
		Assert.assertTrue(numRows>=100);
			//Assert.assertEquals(numRows.toString(), "100");
		Assert.assertEquals(JsonPath.read(responseData, "$.resultCode"), "SUCCESS");
		Assert.assertTrue(JsonPath.read(responseData, "$.items").toString().length() >= 100);
	}
	/**
	 * Test the buffer pool list.
	 */
	@Test(description = "DB has at least 100 buffer pools, open buffer pool list with maxrows=100, verify request that retrieves buffer pools")
	public void testNavigateBufferPoolsList100() {

		logger.info("Running Navigate BufferPools list...");

		assertObject100List("getNavigateBufferPool");
		
		logger.info("Running Navigate 100 BufferPools list Passed");
	}
	/**
	 *@author ybjwang@cn.ibm.com
	 * Test one buffer pool propety
	 */
	@Test(description="Test one Buffer Pool property when navigate through Admin-->Storage Objects --> Buffer Pools-->select one")
	public void testNavigateBufferPoolProperty() throws InterruptedException{
		
		logger.info("Running Navigate one buffer pool properties...");
		compareProBaseAndProResult("getNavigateBufferPoolProperty", "getNavigateBufferPoolProperty_result");
		logger.info("PASSED: Get the Buffer Poole properties executed successfully");
		
		}
	
	/**@author ylwh@cn.ibm.com
	 * Test the buffer pool list.
	 */
	@Test(description = "DB has at least 100 Packages, open Package list with maxrows=100, verify request that retrieves packages")
	public void testNavigatePackagesList100() {

		logger.info("Running Navigate Package list...");
		assertObject100List("getNavigatePackage");	
		logger.info("Running Navigate at least 100 Package list Passed");
	}
	/**
	 *@author ylwh@cn.ibm.com
	 * Test one buffer pool propety
	 */
	@Test(description="Test one Package property when navigate through Admin-->Application object --> Package-->select one")
	public void testNavigatePackageProperty() throws InterruptedException{
		
		logger.info("Running Navigate one Package properties...");
		JSONObject[] rdata=sqlEditorService.runSQLinJDBC("select PKGNAME from syscat.packagedep where pkgschema='TESTPACKAGE_SCH' and btype='T'!","!", this.dbProfile);
		String pkgname = JsonPath.read(rdata[0], "$.data[0].items[0].c0") ;

		AssertForPackage("getNavigatePackageProperty",pkgname);
		
		logger.info("PASSED: Get the Buffer Poole properties executed successfully");
		}
	/**
	 *@author ylwh@cn.ibm.com
	 * compare package response data
	 */
	protected void AssertForPackage(String queryProperties, String pkgname) {
		//add pkgname in post
		String query = navigationService.getJSONData(queryProperties);
		JSONObject basejson = JSONObject.fromObject( query );
		JSONObject basepre = basejson.getJSONObject("predicate");
		basepre.put("PKGNAME", pkgname);	
		JSONObject resObj = navigationService.callNavigationService(AdminNavigationService.TYPE_NAVIGATION_URL, basejson.toString(),
				dbProfile);

		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);

		JSONObject propResult = JsonPath.read(responseData, "$.items[0].data");
		
		
		Assert.assertNotNull(propResult);
		Assert.assertTrue(propResult.get("PKGSCHEMA").equals("TESTPACKAGE_SCH"));
		Assert.assertTrue(propResult.get("PKGNAME").equals(pkgname));	
	}
	/**
	 * Test the tables Aliases list
	 */
	@Test(description = "DB has at least 100 aliases, open alias list with maxrows=100, verify request that retrieves aliases")
	public void testNavigateAliasesList100() {

		logger.info("Running getting the Table's aliases list test...");
		assertObject100List("getNavigateAlias");
		logger.info("PASSED: Get the Table's aliases list executed successfully");
	}
	/**
	 *@author ybjwang@cn.ibm.com
	 * Test one Alias propety
	 */
	@Test(description="Test one Alias when navigate through Admin-->Alias-->select one")
	public void testNavigateAliasProperty() throws InterruptedException{
		
		logger.info("Running Navigate one Alias properties...");
		compareProBaseAndProResult("getNavigateAliasProperty", "getNavigateAliasProperty_result");
		logger.info("PASSED: Get the Alias properties executed successfully");
		}
	/**
	 * Test the MQT list.
	 */
	@Test(description = "DB has at least 100 MQTs, open mqt list with maxrows=100, verify request that retrieves mqts")
	public void testNavigateMQTsList100() {

		logger.info("Running Navigate MQTs list...");
		assertObject100List("getNavigateMQT");
		logger.info("PASSED: Get the MQT list executed successfully");
	}
	/**
	 * Test the MQT property.
	 */
	@Test(description = "Click a MQT to open its properties, verify request that returns Properties info")
	public void testNavigateMQTProperty() {

		logger.info("Running Navigate MQT Property...");

		compareProBaseAndProResult("getNavigateMQTProperty", "getNavigateMQTProperty_result");

		logger.info("PASSED: Get the MQT's properties executed successfully");
	}
}
