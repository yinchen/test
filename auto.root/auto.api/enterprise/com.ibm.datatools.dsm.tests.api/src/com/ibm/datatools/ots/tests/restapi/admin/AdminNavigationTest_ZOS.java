package com.ibm.datatools.ots.tests.restapi.admin;

import java.util.List;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.AdminNavigationService;
import com.ibm.datatools.ots.tests.restapi.common.AdminSQLEditorService;
import com.jayway.jsonpath.JsonPath;

public class AdminNavigationTest_ZOS extends AdminTestBase {
	private static final Logger logger = LogManager.getLogger(AdminNavigationTest_ZOS.class);

	private AdminNavigationService navigationService = null;
	private AdminSQLEditorService sqlEditorService = null;

	@BeforeClass
	public void beforeTest() {
		navigationService = new AdminNavigationService();
		sqlEditorService = new AdminSQLEditorService();
		testViewAliasProperties_precondition();
		testViewMQTProperties_precondition();
		testViewAuxiliaryTableProperties_precondition();
		testViewColumnProperties_precondition();
		testViewViewProperties_precondition();
		testViewStorageGroupProperties_precondition();
		testViewSequenceProperties_precondition();
		testViewTableIndexContraintProperties_precondition();
		testViewTablespaceProperties_precondition();
		testViewSynonymProperties_precondition();
		testViewTriggerProperties_precondition();
		testViewStoredProcedureProperties_precondition();
		testViewPackageProperties_precondition();
		testViewUDFProperties_precondition();
		testViewUDTProperties_precondition();
	}

	@AfterClass
	public void afterTest() {
		testViewAliasProperties_clean();
		testViewMQTProperties_clean();
		testViewAuxiliaryTableProperties_clean();
		testViewColumnProperties_clean();
		testViewViewProperties_clean();
		testViewStorageGroupProperties_clean();
		testViewSequenceProperties_clean();
		testViewTableIndexContraintProperties_clean();
		testViewTablespaceProperties_clean();
		testViewSynonymProperties_clean();
		testViewTriggerProperties_clean();
		testViewStoredProcedureProperties_clean();
		testViewPackageProperties_clean();
		testViewUDFProperties_clean();
		testViewUDTProperties_clean();

	}

	
	
	public void setDefaultSchema(){
		List<String> createList = new ArrayList<String>();
		createList.add("GRANT SYSADM TO SYSADM;");
		createList.add("SET CURRENT SQLID ='SYSADM';");
		createList.add("SET CURRENT SCHEMA = 'SYSADM';");
		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, false);
		
	}
	
	/**
	 * Get response data via passed properties.
	 * 
	 * @param queryProperties
	 * @return
	 */
	private void getTestingResult(String queryProperties, String propertiesResult) {

		String query = navigationService.getJSONData(queryProperties);

		JSONObject resObj = navigationService.callNavigationService(AdminNavigationService.TYPE_NAVIGATION_URL, query,
				dbProfile);

		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);

		JSONObject propResult = JsonPath.read(responseData, "$.items[0].data");

		if (propResult == null) {
			propResult = responseData;
		}

		String base = navigationService.getJSONData(propertiesResult);

		JSONObject propBase = JSONObject.fromObject(base);

		Assert.assertTrue(compareJsons(propBase, propResult));

	}
	
	/**
	 * Get response data via passed properties.
	 * 
	 */
	@SuppressWarnings("unchecked")
	private void getTestingResultViewWithOrderKeys(String queryProperties, String propertiesResult, List<String> keys) {

		String query = navigationService.getJSONData(queryProperties);

		JSONObject resObj = navigationService.callNavigationService(AdminNavigationService.TYPE_NAVIGATION_URL, query,
				dbProfile);

		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);

		JSONObject propResult = JsonPath.read(responseData, "$.items[0].data");

		if (propResult == null) {
			propResult = responseData;
		}

		String base = navigationService.getJSONData(propertiesResult);

		JSONObject propBase = JSONObject.fromObject(base);

		for(int i = 0; i < keys.size(); i++){
			Collections.sort(propBase.getJSONArray(keys.get(i)), new Comparator<JSONObject>() {
	            public int compare(JSONObject arg0, JSONObject arg1) {
	            	return arg0.getString("COLNAME").compareTo(arg1.getString("COLNAME"));
	            }
	        });
			
			Collections.sort(propResult.getJSONArray(keys.get(i)), new Comparator<JSONObject>() {
	            public int compare(JSONObject arg0, JSONObject arg1) {
	            	return arg0.getString("COLNAME").compareTo(arg1.getString("COLNAME"));
	            }
	        });
		}
		Assert.assertTrue(compareJsons(propBase, propResult));

	}

	public void testViewTableIndexContraintProperties_precondition() {
		List<String> dropList = new ArrayList<String>();
		dropList.add("ALTER TABLE SYSADM.TB2_FOR_UT DROP RESTRICT ON DROP;");
		dropList.add("DROP DATABASE DBTEST01;");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);

		List<String> grantList = new ArrayList<String>();
		List<String> createList = new ArrayList<String>();

		createList.add("CREATE DATABASE DBTEST01");
		createList.add("CREATE TABLESPACE TSTEST01 IN DBTEST01;");
		createList.add("CREATE TABLESPACE TSTEST02 IN DBTEST01;");
		grantList.add("GRANT SYSADM TO SYSADM;");
		createList.add("SET CURRENT SQLID ='SYSADM';");
		createList.add("SET CURRENT SCHEMA = 'SYSADM';");
		createList.add("CREATE TABLE SYSADM.TB1_FOR_UT " + "(DISTRICT_ID  INT NOT NULL WITH DEFAULT,"
				+ "DISTRICT_SCHOOL_ID  CHAR(10), " + "DISTRICT_NAME  CHAR(20), "
				+ "CONSTRAINT PRIMARY_CONSTRAINT_FOR_UT PRIMARY KEY (DISTRICT_ID), "
				+ "CONSTRAINT CHECK_CONSTRAINT_FOR_UT CHECK (DISTRICT_SCHOOL_ID IN('01', '02', '03', '04', '05', '06', '07', '08', '09', '10'))) "
				+ "IN DBTEST01.TSTEST01; ");
		createList.add(
				"CREATE UNIQUE INDEX SYSADM.INDEX1_FOR_UT ON SYSADM.TB1_FOR_UT (DISTRICT_ID           ASC) BUFFERPOOL BP1; ");
		createList.add("CREATE TABLE TB2_FOR_UT " + "(DISTRICT_ID          INT NOT NULL WITH DEFAULT,"
				+ " DISTRICT_SCHOOL_ID   CHAR(10)," + " DISTRICT_SCHOOL_NUMBER CHAR(30) NOT NULL WITH DEFAULT,"
				+ " CONSTRAINT UNIQUE_CONSTRAINT_FOR_UT" + " UNIQUE  (DISTRICT_SCHOOL_NUMBER),"
				+ " CONSTRAINT FOREIGN_CONSTRAINT_FOR_UT" + " FOREIGN KEY (DISTRICT_ID)"
				+ " REFERENCES SYSADM.TB1_FOR_UT(DISTRICT_ID) " + " ON DELETE RESTRICT)" + " IN DBTEST01.TSTEST02; ");

		sqlEditorService.runSQLinJDBC(grantList, ";", this.dbProfile, false);
		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}

	public void testViewTableIndexContraintProperties_clean() {
		List<String> dropList = new ArrayList<String>();
		dropList.add("ALTER TABLE SYSADM.TB2_FOR_UT DROP RESTRICT ON DROP;");
		dropList.add("DROP DATABASE DBTEST01;");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);
	}

	/**
	 * Test retrieve table properties
	 */
	@Test(description = "Test retrieve table property function")
	public void testViewTableProperties() {

		logger.info("Running View Table Property...");

		getTestingResult("getZOSTableProperty", "getZOSTableProperty_result");

		// Browse columns form table
		getTestingResult("getZOSTableColumn1Property", "getZOSTableColumn1Property_result");
		getTestingResult("getZOSTableColumn2Property", "getZOSTableColumn2Property_result");
		getTestingResult("getZOSTableColumn3Property", "getZOSTableColumn3Property_result");

		logger.info("PASSED: Testing of finding out properties of Table executed successfully");
	}

	/**
	 * Test retrieve index properties
	 */
	@Test(description = "Test retrieve index property function")
	public void testViewIndexProperties() {

		logger.info("Running View Table Property...");

		getTestingResult("getZOSIndexProperty", "getZOSIndexProperty_result");

		// Browse the attributes of keys in index
		getTestingResult("getZOSIndexKeyProperty", "getZOSIndexKeyProperty_result");

		logger.info("PASSED: Testing of finding out properties of Index executed successfully");
	}

	public void testViewTablespaceProperties_precondition() {
		sqlEditorService.runSQLinJDBC("DROP DATABASE DBTESTTS;", ";", this.dbProfile, false);

		List<String> grantList = new ArrayList<String>();
		List<String> createList = new ArrayList<String>();
		grantList.add("GRANT SYSADM TO SYSADM;");
		createList.add("SET CURRENT SQLID ='SYSADM';");
		createList.add("SET CURRENT SCHEMA = 'SYSADM';");
		createList.add("CREATE DATABASE DBTESTTS");
		// For Large tablespace
		createList.add("CREATE TABLESPACE TSLARGE " + "IN DBTESTTS " + "USING STOGROUP SYSDEFLT "
				+ "FREEPAGE 0 PCTFREE 0 " + "GBPCACHE CHANGED " + "TRACKMOD NO " + "LOGGED " + "DSSIZE 1G "
				+ "NUMPARTS 20 " + "SEGSIZE 0 " + "BUFFERPOOL BP1 " + "LOCKSIZE PAGE " + "LOCKMAX 0 " + "CLOSE YES "
				+ "COMPRESS YES " + "CCSID      EBCDIC " + "DEFINE YES " + "MAXROWS 255;");

		// For LOB tablespace
		createList.add("CREATE TABLESPACE TSBLOB " + "IN DBTESTTS " + "LOGGED;");
		createList.add("CREATE TABLE SYSADM.TB_CLOB" + "(P1                   CHAR(4) FOR SBCS DATA NOT NULL,"
				+ " C1                   CHAR(4) FOR SBCS DATA WITH DEFAULT NULL,"
				+ " C2                   TIME NOT NULL," + " C3                   DECIMAL(6, 0) NOT NULL,"
				+ " COL4    CLOB(800)    NOT NULL," + " CONSTRAINT P1" + " PRIMARY KEY (P1))" + "IN DBTESTTS.TSBLOB "
				+ "AUDIT NONE " + "DATA CAPTURE NONE " + "CCSID      EBCDIC " + "NOT VOLATILE; ");
		createList.add("CREATE UNIQUE INDEX SYSADM.IX_CLOB ON SYSADM.TB_CLOB (P1                    ASC)");
		createList.add("CREATE LOB TABLESPACE TSLOB01 IN DBTESTTS NOT LOGGED LOCKSIZE LOB;");
		createList.add("CREATE AUXILIARY TABLE TBLOB01 IN DBTESTTS.TSLOB01 STORES SYSADM.TB_CLOB COLUMN COL4");
		createList.add("CREATE UNIQUE INDEX IXLOB01 ON TBLOB01 ;");

		// For PBG tablespace
		createList.add(
				"CREATE TABLESPACE TSPBG01 IN DBTESTTS MAXPARTITIONS 10 LOCKSIZE ANY LOGGED SEGSIZE 32 MAXROWS 2;");
		createList.add(
				"CREATE TABLE SYSADM.TBPBG01 (PRODUCT_ID INTEGER, QTY INTEGER, STORE VARCHAR(30), DEPT INTEGER,SALE_DATE DATE)IN DBTESTTS.TSPBG01;");

		// For PBR tablespace
		createList.add("CREATE TABLESPACE TSPBR01 IN DBTESTTS NOT LOGGED DSSIZE 4 G " + "NUMPARTS 4" + "(" + " PART 1"
				+ "  USING STOGROUP SYSDEFLT" + "  PRIQTY 720" + "  SECQTY 240" + "  ERASE NO" + "  FREEPAGE 0"
				+ "  PCTFREE 50" + "  GBPCACHE CHANGED" + "  TRACKMOD YES" + "  COMPRESS NO, " + "  PART 4 "
				+ "  USING STOGROUP SYSDEFLT" + "  PRIQTY 720" + "  SECQTY 240" + "  ERASE YES" + "  FREEPAGE 0"
				+ "  PCTFREE 20" + "  GBPCACHE CHANGED" + "  TRACKMOD NO" + "  COMPRESS YES" + ")" + "SEGSIZE 32 "
				+ "BUFFERPOOL BP0 " + "LOCKSIZE TABLESPACE " + "CCSID      UNICODE " + "LOCKPART NO;");
		createList.add("CREATE TABLE SYSADM.TBPBR01" + "(COLCHAR              CHAR(10) FOR SBCS DATA WITH DEFAULT NULL,"
				+ " COLDATE              DATE WITH DEFAULT NULL," + " COLTIME              TIME WITH DEFAULT NULL,"
				+ " COLTIMESTMP          TIMESTAMP (6) WITHOUT TIME ZONE WITH DEFAULT NULL)" + "IN DBTESTTS.TSPBR01 "
				+ "AUDIT NONE " + "DATA CAPTURE NONE " + "NOT VOLATILE " + "APPEND NO  ;");

		createList.add("CREATE INDEX SYSADM.IXPBR01 ON SYSADM.TBPBR01 (COLDATE               ASC);");

		// For regular tablespace with multiple tables
		createList.add("CREATE TABLESPACE TSREGU IN DBTESTTS LOCKSIZE TABLE;");
		createList.add("CREATE TABLE SYSADM.TB_REGULAR01" + "(P1                   CHAR(4) FOR SBCS DATA NOT NULL,"
				+ " C1                   CHAR(4) FOR SBCS DATA WITH DEFAULT NULL,"
				+ " C2                   TIME NOT NULL," + " C3                   DECIMAL(6, 0) NOT NULL,"
				+ "CONSTRAINT P1 PRIMARY KEY (P1))" + "IN DBTESTTS.TSREGU " + "AUDIT NONE " + "DATA CAPTURE NONE "
				+ "CCSID      EBCDIC " + "NOT VOLATILE;");
		createList.add("CREATE TABLE SYSADM.TB_REGULAR02" + "(P1                   CHAR(4) FOR SBCS DATA NOT NULL,"
				+ " C1                   CHAR(4) FOR SBCS DATA WITH DEFAULT NULL,"
				+ " C2                   TIME NOT NULL," + " C3                   DECIMAL(6, 0) NOT NULL,"
				+ "CONSTRAINT P1 PRIMARY KEY (P1))" + "IN DBTESTTS.TSREGU " + "AUDIT NONE " + "DATA CAPTURE NONE "
				+ "CCSID      EBCDIC " + "NOT VOLATILE;");

		// For XML tablespace
		createList.add("CREATE TABLESPACE TSBASEX IN DBTESTTS CCSID ASCII LOCKSIZE ROW;");
		createList.add("CREATE TABLE SYSADM.TBXML01" + "(CUST_ID              VARCHAR(10) WITH DEFAULT NULL,"
				+ " CUST_REF             BIGINT WITH DEFAULT NULL," + " INFO                 XML)"
				+ "IN DBTESTTS.TSBASEX");

		sqlEditorService.runSQLinJDBC(grantList, ";", this.dbProfile, false);
		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}

	public void testViewTablespaceProperties_clean() {
		sqlEditorService.runSQLinJDBC("DROP DATABASE DBTESTTS;", ";", this.dbProfile, false);
	}

	/**
	 * Test retrieval of tablespace properties
	 */
	@Test(description = "Tablespace properties retrieval")
	public void testViewTablespaceProperties() {
		logger.info("Running View Tablespace Property...");
		// Verify attributes of large tablespace without table
		getTestingResult("getZOSTablespaceLargeWithoutTableProperty",
				"getZOSTablespaceLargeWithoutTableProperty_result");

		// Verify attributes of Lob tablespace
		getTestingResult("getZOSTablespaceLOBProperty", "getZOSTablespaceLOBProperty_result");

		// Verify attributes of PBG tablespace
		getTestingResult("getZOSTablespacePBGProperty", "getZOSTablespacePBGProperty_result");

		// Verify attributes of PBR tablespace
		getTestingResult("getZOSTablespacePBRProperty", "getZOSTablespacePBRProperty_result");

		// Verify attributes of regular tablespace with multiple tables
		getTestingResult("getZOSTablespaceRegularWithMultipleProperty",
				"getZOSTablespaceRegularWithMultipleProperty_result");

		// Verify attributes of XML tablespace
		getTestingResult("getZOSTablespaceXMLProperty", "getZOSTablespaceXMLProperty_result");

		// getTestingResult("getZOSTablespacePartProperty",
		// "getZOSTablespacePartProperty_result");

		logger.info("PASSED: Testing of finding out properties of Tablespace executed successfully");
	}

	public void testViewSequenceProperties_precondition() {
		List<String> dropList = new ArrayList<String>();
		dropList.add("DROP SEQUENCE SYSADM.SEADNA01;");
		dropList.add("DROP SEQUENCE ADMF001.SEADNA02;");
		dropList.add("DROP SEQUENCE SYSADM.SEADNA03;");
		dropList.add("DROP SEQUENCE SYSADM.SEADNA04;");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);

		List<String> grantList = new ArrayList<String>();
		List<String> createList = new ArrayList<String>();
		createList.add("SET CURRENT SCHEMA = 'SYSADM';");
		grantList.add("GRANT SYSADM TO ADMF001;");
		createList.add("SET CURRENT SQLID = 'ADMF001';");
		
		createList.add(
				"CREATE SEQUENCE SEADNA01 AS SMALLINT START WITH 1 INCREMENT BY 2 NO MINVALUE MAXVALUE 32767 ORDER;");
		createList.add("COMMENT ON SEQUENCE SEADNA01 IS 'SMALLINT SEQUENCE WITH A LOTS OF ATTRIBUTES';");

		createList.add("CREATE SEQUENCE SEADNA04 AS DECIMAL MINVALUE -30 CYCLE CACHE 20 ORDER;");

		createList.add(
				"CREATE SEQUENCE ADMF001.SEADNA02 AS INTEGER START WITH 100 MINVALUE 1 NO MAXVALUE CYCLE CACHE 5 NO ORDER;");

		createList.add(
				"CREATE SEQUENCE SEADNA03 AS BIGINT START WITH 1010 INCREMENT BY 10 MINVALUE 1 MAXVALUE 922337203685477000 CYCLE NO CACHE ORDER;");
		createList.add(
				"COMMENT ON SEQUENCE SEADNA03 IS 'BIGINT SEQUENCE WITH A LOTS OF ATTRIBUTES AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA';");
		createList.add("ALTER SEQUENCE SYSADM.SEADNA03 RESTART WITH 1000");

		sqlEditorService.runSQLinJDBC(grantList, ";", this.dbProfile, false);
		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}

	public void testViewSequenceProperties_clean() {
		List<String> dropList = new ArrayList<String>();
		dropList.add("DROP SEQUENCE SYSADM.SEADNA01;");
		dropList.add("DROP SEQUENCE ADMF001.SEADNA02;");
		dropList.add("DROP SEQUENCE SYSADM.SEADNA03;");
		dropList.add("DROP SEQUENCE SYSADM.SEADNA04;");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);
	}

	/**
	 * Test retrieval of Sequence properties
	 */
	@Test(description = "Sequence properties retrieval")
	public void testViewSequenceProperties() {

		logger.info("Running retrieving properties of sequence...");
		/*
		 * Verify properties of BigInt sequence
		 */
		getTestingResult("getZOSBigIntSequenceProperties", "getZOSBigIntSequenceProperties_result");

		/*
		 * Verify properties of Decimal sequence
		 */
		getTestingResult("getZOSDecimalSequenceProperties", "getZOSDecimalSequenceProperties_result");

		/*
		 * Verify properties of Integer sequence
		 */
		getTestingResult("getZOSIntegerSequenceProperties", "getZOSIntegerSequenceProperties_result");

		/*
		 * Verify properties of SmallInt sequence
		 */
		getTestingResult("getZOSSmallIntSequenceProperties", "getZOSSmallIntSequenceProperties_result");

		logger.info("PASSED: Testing of finding out properties of a sequence executed successfully");
	}

	public void testViewStorageGroupProperties_precondition() {
		List<String> dropList = new ArrayList<String>();
		dropList.add("Drop STOGROUP STODEFLT;");
		dropList.add("Drop STOGROUP STMDEFLT;");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);
		
		List<String> grantList = new ArrayList<String>();
		List<String> createList = new ArrayList<String>();
		grantList.add("GRANT CREATESG TO SYSADM;");
		createList.add("SET CURRENT SQLID ='SYSADM';");
		createList.add("CREATE STOGROUP STODEFLT VOLUMES(SCR01, SCR02) VCAT DSNC000;");
		createList.add("CREATE STOGROUP STMDEFLT VOLUMES('*') VCAT DSNC000;");
		sqlEditorService.runSQLinJDBC(grantList, ";",this.dbProfile, false);
		sqlEditorService.runSQLinJDBC(createList, ";",this.dbProfile, true);
	}

	public void testViewStorageGroupProperties_clean() {
		List<String> dropList = new ArrayList<String>();
		dropList.add("Drop STOGROUP STODEFLT;");
		dropList.add("Drop STOGROUP STMDEFLT;");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);
	}

	/**
	 * Test retrieval of Storage Group properties
	 */
	@Test(description = "Storage Group properties retrieval")
	public void testViewStorageGroupProperties() {
		logger.info("Running retrieving properties of Storage Group...");
		/*
		 * Verify properties of multiple volumes storage group
		 */
		getTestingResult("getZOSStorageGroupPropertiesWithMultiVolumes",
				"getZOSStorageGroupPropertiesWithMultiVolumes_result");

		/*
		 * Verify properties of single volume storage group
		 */
		getTestingResult("getZOSStorageGroupPropertiesWithSingleVolume",
				"getZOSStorageGroupPropertiesWithSingleVolume_result");

		logger.info("PASSED: Testing of finding out properties of a Storage Group executed successfully");
	}

	/**
	 * Precondition for view alias properties
	 */
	public void testViewAliasProperties_precondition() {
		List<String> dropList = new ArrayList<String>();

		dropList.add("DROP ALIAS C12OP02.ALIAS_TAB4ALIAS;");
		dropList.add("DROP ALIAS C12OP02.ALIAS_MQT_TAB4ALIAS;");
		dropList.add("DROP ALIAS C12OP02.ALIAS_VIEW_TAB4ALIAS;");
		dropList.add("DROP ALIAS C12OP02.ALIAS_SEQ4ALIAS;");
		dropList.add("DROP TABLE C12OP02.MQT_TAB4ALIAS;");
		dropList.add("DROP VIEW C12OP02.VIEW_TAB4ALIAS;");
		dropList.add("DROP TABLE C12OP02.TAB4ALIAS;");
		dropList.add("DROP SEQUENCE C12OP02.SEQ4ALIAS;");

		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);

		List<String> createList = new ArrayList<String>();

		createList.add(
				"CREATE TABLE C12OP02.TAB4ALIAS(\"Column1\" CHAR(5), \"Column2\" CHAR(5)) AUDIT NONE DATA CAPTURE NONE CCSID EBCDIC;");
		createList.add(
				"CREATE TABLE C12OP02.MQT_TAB4ALIAS(\"Column1\", \"Column2\") AS (SELECT * FROM C12OP02.TAB4ALIAS) DATA INITIALLY DEFERRED REFRESH DEFERRED ENABLE QUERY OPTIMIZATION MAINTAINED BY SYSTEM;");
		createList.add(
				"CREATE VIEW C12OP02.VIEW_TAB4ALIAS(\"Column1\", \"Column2\") AS (SELECT * FROM C12OP02.TAB4ALIAS);");
		createList.add("CREATE ALIAS C12OP02.ALIAS_TAB4ALIAS FOR C12OP02.TAB4ALIAS;");
		createList.add("COMMENT ON ALIAS C12OP02.ALIAS_TAB4ALIAS IS \'this is a test\';");
		createList.add("CREATE ALIAS C12OP02.ALIAS_MQT_TAB4ALIAS FOR C12OP02.MQT_TAB4ALIAS;");
		createList.add("COMMENT ON ALIAS C12OP02.ALIAS_MQT_TAB4ALIAS IS \'this is a test\';");
		createList.add("CREATE ALIAS C12OP02.ALIAS_VIEW_TAB4ALIAS FOR C12OP02.VIEW_TAB4ALIAS;");
		createList.add("COMMENT ON ALIAS C12OP02.ALIAS_VIEW_TAB4ALIAS IS \'this is a test\';");
		createList.add(
				"CREATE SEQUENCE C12OP02.SEQ4ALIAS AS INTEGER START WITH 1 INCREMENT BY 1 MINVALUE -2147483648 MAXVALUE 2147483647 NO CYCLE CACHE 20 NO ORDER;");
		createList.add("CREATE ALIAS C12OP02.ALIAS_SEQ4ALIAS FOR C12OP02.SEQ4ALIAS;");
		createList.add("COMMENT ON ALIAS C12OP02.ALIAS_SEQ4ALIAS IS \'this is a test\';");

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);

	}

	/**
	 * Test retrieval of Alias properties
	 */
	@Test(description = "Alias properties retrieval")
	public void testViewAliasProperties() {

		logger.info("Running retrieving properties of alias...");

		// Verify properties of alias which is based on table

		getTestingResult("getZOSAliasBasedOnTableProperty", "getZOSAliasBasedOnTableProperty_result");

		// Verify properties of alias which is based on MQT

		getTestingResult("getZOSAliasBasedOnMQTProperty", "getZOSAliasBasedOnMQTProperty_result");

		// Verify properties of alias which is based on view

		getTestingResult("getZOSAliasBasedOnViewProperty", "getZOSAliasBasedOnViewProperty_result");

		// Verify properties of alias which is based on sequence

		getTestingResult("getZOSAliasBasedOnSequenceProperty", "getZOSAliasBasedOnSequenceProperty_result");

		logger.info("PASSED: Testing of finding out properties of an alias executed successfully");
	}

	/**
	 * Clean environment for alias properties view
	 */
	public void testViewAliasProperties_clean() {
		List<String> dropList = new ArrayList<String>();

		dropList.add("DROP ALIAS C12OP02.ALIAS_TAB4ALIAS;");
		dropList.add("DROP ALIAS C12OP02.ALIAS_MQT_TAB4ALIAS;");
		dropList.add("DROP ALIAS C12OP02.ALIAS_VIEW_TAB4ALIAS;");
		dropList.add("DROP ALIAS C12OP02.ALIAS_SEQ4ALIAS;");
		dropList.add("DROP TABLE C12OP02.MQT_TAB4ALIAS;");
		dropList.add("DROP VIEW C12OP02.VIEW_TAB4ALIAS;");
		dropList.add("DROP TABLE C12OP02.TAB4ALIAS;");
		dropList.add("DROP SEQUENCE C12OP02.SEQ4ALIAS;");

		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);
	}

	/**
	 * Precondition for view auxiliary table properties
	 */
	public void testViewAuxiliaryTableProperties_precondition() {
		List<String> dropList = new ArrayList<String>();

		dropList.add("DROP TABLE C12OP02.AUX_TAB4AUX;");
		dropList.add("DROP TABLE C12OP02.TAB4AUX;");
		dropList.add("DROP TABLESPACE DB4AUX.TS4AUX;");
		dropList.add("DROP TABLESPACE DB4AUX.TS4AUX2;");
		dropList.add("DROP STOGROUP SG4AUX;");
		dropList.add("DROP DATABASE DB4AUX;");

		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);

		List<String> createList = new ArrayList<String>();

		createList.add("CREATE DATABASE DB4AUX CCSID EBCDIC;");
		createList.add("CREATE STOGROUP SG4AUX VOLUMES(\'*\') VCAT DSNC000;");
		createList.add(
				"CREATE TABLESPACE TS4AUX IN DB4AUX USING STOGROUP SG4AUX PRIQTY -1 SECQTY -1 CCSID EBCDIC LOCKSIZE ANY MAXROWS 255;");
		createList.add(
				"CREATE LOB TABLESPACE TS4AUX2 IN DB4AUX USING STOGROUP SG4AUX PRIQTY -1 SECQTY -1 DSSIZE 4 G LOCKSIZE ANY;");
		createList.add(
				"CREATE TABLE C12OP02.TAB4AUX(\"Column1\" CHAR(5), \"Column2\" CLOB(5)) IN DB4AUX.TS4AUX AUDIT NONE DATA CAPTURE NONE CCSID EBCDIC;");
		createList.add(
				"CREATE AUXILIARY TABLE C12OP02.AUX_TAB4AUX IN DB4AUX.TS4AUX2 STORES C12OP02.TAB4AUX COLUMN \"Column2\";");
		createList.add("COMMENT ON TABLE C12OP02.AUX_TAB4AUX IS \'this is auxiliary table\';");

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}

	/**
	 * Test retrieval of Auxiliary table properties
	 */
	@Test(description = "Auxiliary Table properties retrieval")
	public void testViewAuxiliaryTableProperties() {

		logger.info("Running retrieving properties of auxiliary table...");

		// Verify properties of auxiliary table

		getTestingResult("getZOSAuxiliaryTableProperty", "getZOSAuxiliaryTableProperty_result");

		logger.info("PASSED: Testing of finding out properties of an auxiliary table executed successfully");
	}

	/**
	 * Clean environment for auxiliary table properties view
	 */
	public void testViewAuxiliaryTableProperties_clean() {
		List<String> dropList = new ArrayList<String>();

		dropList.add("DROP TABLE C12OP02.AUX_TAB4AUX;");
		dropList.add("DROP TABLE C12OP02.TAB4AUX;");
		dropList.add("DROP TABLESPACE DB4AUX.TS4AUX;");
		dropList.add("DROP TABLESPACE DB4AUX.TS4AUX2;");
		dropList.add("DROP STOGROUP SG4AUX;");
		dropList.add("DROP DATABASE DB4AUX;");

		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);
	}

	/**
	 * Precondition for view Synonym properties
	 */
	public void testViewSynonymProperties_precondition() {
		setDefaultSchema();// set SQL ID before create SYNONYM
		sqlEditorService.runSQLinJDBC("DROP TABLE \"SYSADM\".\"TAB4SYNONYM\";", ";", this.dbProfile, false);
		sqlEditorService.runSQLinJDBC(
				"CREATE TABLE \"SYSADM\".\"TAB4SYNONYM\"(\"Column1\" CHAR(5), \"Column2\" CHAR(5)) AUDIT NONE DATA CAPTURE NONE CCSID EBCDIC;",
				";", this.dbProfile);
		
		List<String> createList = new ArrayList<String>();
		createList.add("GRANT SYSADM TO SYSADM;");
		createList.add("SET CURRENT SQLID ='SYSADM';");
		createList.add("SET CURRENT SCHEMA = 'SYSADM';");
		createList.add("CREATE SYNONYM \"SYNONYM_4TAB\" for \"SYSADM\".\"TAB4SYNONYM\";");//The quotation mark with Synonym name is quite important. 
		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, false);
		
	}
 
	/**
	 * Test retrieval of Synonym properties
	 */
	@Test(description = "Synonym properties retrieval")
	public void testViewSynonymProperties() {
		logger.info("Running View Synonym Property...");
		
		getTestingResult("getZOSSynonymsProperty", "getZOSSynonymsProperty_result");

		logger.info("PASSED: Testing of finding out properties of a synonym executed successfully");
	}

	/**
	 * Clean environment for Synonym properties view
	 */
	public void testViewSynonymProperties_clean() {
		sqlEditorService.runSQLinJDBC("DROP SYNONYM \"SYNONYM_4TAB\";", ";", this.dbProfile);
		sqlEditorService.runSQLinJDBC("DROP TABLE \"SYSADM\".\"TAB4SYNONYM\";", ";", this.dbProfile);

	}

	public void testViewViewProperties_precondition() {

		List<String> dropList = new ArrayList<String>();
		dropList.add("DROP VIEW SYSADM.VWTEST01;");
		dropList.add("DROP VIEW SYSADM.VWTEST02;");
		dropList.add("DROP VIEW SYSADM.VWTEST03;");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);

		List<String> grantList = new ArrayList<String>();
		List<String> createList = new ArrayList<String>();
		grantList.add("GRANT SYSADM TO SYSADM;");
		createList.add("SET CURRENT SQLID = 'SYSADM';");
		createList.add("CREATE VIEW SYSADM.VWTEST01 AS (SELECT * FROM SYSIBM.SYSTABLES);");
		createList.add("CREATE VIEW SYSADM.VWTEST02 AS (SELECT NAME FROM SYSADM.VWTEST01);");
		createList.add(
				"CREATE VIEW SYSADM.VWTEST03 AS (SELECT T1.NAME, T2.NAME AS TSNAME FROM SYSIBM.SYSTABLES T1, SYSIBM.SYSTABLESPACE T2 WHERE T1.TSNAME = T2.NAME );");

		sqlEditorService.runSQLinJDBC(grantList, ";", this.dbProfile, false);
		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}

	public void testViewViewProperties_clean() {

		List<String> dropList = new ArrayList<String>();
		dropList.add("DROP VIEW SYSADM.VWTEST01;");
		dropList.add("DROP VIEW SYSADM.VWTEST02;");
		dropList.add("DROP VIEW SYSADM.VWTEST03;");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);
	}

	@Test(description = "View properties retrieval")
	public void testViewViewProperties() {
		logger.info("Running View View Property...");

		// Verify the properties of a view on just single table
		getTestingResult("getZOSViewOnOneTableProperty", "getZOSViewOnOneTableProperty_result");

		// Verify the properties of a view created on another view
		getTestingResult("getZOSViewOnViewProperty", "getZOSViewOnViewProperty_result");

		// Verify the properties of a view created on multiple tables
		getTestingResult("getZOSViewOnMultipleTablesProperty", "getZOSViewOnMultipleTablesProperty_result");

		// Verify the properties of columns on a view
		getTestingResultViewWithOrderKeys("getZOSViewColumnProperty", "getZOSViewColumnProperty_result", new ArrayList<String>(){{add("items");}});

		logger.info("PASSED: Testing of finding out properties of a view executed successfully");
	}

	/**
	 * Precondition for view Trigger properties
	 */
	public void testViewTriggerProperties_precondition() {
		setDefaultSchema();
		sqlEditorService.runSQLinJDBC("DROP TABLE SYSADM.TAB4TRIGGER;", ";", this.dbProfile, false);
		sqlEditorService.runSQLinJDBC("DROP TABLE SYSADM.TAB_T1;", ";", this.dbProfile, false);
		sqlEditorService.runSQLinJDBC(
				"CREATE TABLE SYSADM.TAB4TRIGGER(\"Column1\" CHAR(5), \"Column2\" INTEGER) AUDIT NONE DATA CAPTURE NONE CCSID EBCDIC;",
				";", this.dbProfile);
		sqlEditorService.runSQLinJDBC(
				"CREATE TABLE SYSADM.TAB_T1(\"Column1\" CHAR(5), \"Column2\" INTEGER) AUDIT NONE DATA CAPTURE NONE CCSID EBCDIC;",
				";", this.dbProfile);
		sqlEditorService.runSQLinJDBC(
				"CREATE TRIGGER SYSADM.TRIGGER_4TAB AFTER INSERT ON SYSADM.TAB4TRIGGER FOR EACH STATEMENT MODE DB2SQL NOT SECURED SELECT * FROM SYSADM.TAB_T1;",
				";", this.dbProfile);

	}

	/**
	 * Test retrieval of Trigger properties
	 */
	@Test(description = "Trigger properties retrieval")
	public void testViewTriggerProperties() {
		logger.info("Running View Trigger Property...");

		getTestingResult("getZOSTriggersProperty", "getZOSTriggersProperty_result");

		logger.info("PASSED: Testing of finding out properties of a trigger executed successfully");
	}

	/**
	 * Clean environment for Trigger properties view
	 */
	public void testViewTriggerProperties_clean() {
		sqlEditorService.runSQLinJDBC("DROP TRIGGER SYSADM.TRIGGER_4TAB;", ";", this.dbProfile);
		sqlEditorService.runSQLinJDBC("DROP TABLE SYSADM.TAB4TRIGGER;", ";", this.dbProfile);
		sqlEditorService.runSQLinJDBC("DROP TABLE SYSADM.TAB_T1;", ";", this.dbProfile);

	}

	/**
	 * Precondition for view MQT properties
	 */
	public void testViewMQTProperties_precondition() {
		List<String> dropList = new ArrayList<String>();
		dropList.add("DROP TABLE C12OP02.MQT_TAB4MQT;");
		dropList.add("DROP TABLE C12OP02.MQT_VIEW_TAB4MQT;");
		dropList.add("DROP VIEW C12OP02.VIEW_TAB4MQT;");
		dropList.add("DROP TABLE C12OP02.TAB4MQT;");
		dropList.add("DROP TABLESPACE DB4MQT.TS4MQT;");
		dropList.add("DROP STOGROUP SG4MQT;");
		dropList.add("DROP DATABASE DB4MQT;");

		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);

		List<String> createList = new ArrayList<String>();
		createList.add("CREATE DATABASE DB4MQT CCSID EBCDIC;");
		createList.add("CREATE STOGROUP SG4MQT VOLUMES(\'*\') VCAT DSNC000;");
		createList.add(
				"CREATE TABLESPACE TS4MQT IN DB4MQT USING STOGROUP SG4MQT PRIQTY -1 SECQTY -1 CCSID EBCDIC LOCKSIZE ANY MAXROWS 255;");
		createList.add(
				"CREATE TABLE C12OP02.TAB4MQT(\"Column1\" CHAR(5), \"Column2\" CHAR(5)) IN DB4MQT.TS4MQT AUDIT NONE DATA CAPTURE NONE CCSID EBCDIC;");
		createList.add(
				"CREATE TABLE C12OP02.MQT_TAB4MQT(\"Column1\", \"Column2\") AS (SELECT * FROM C12OP02.TAB4MQT) DATA INITIALLY DEFERRED REFRESH DEFERRED ENABLE QUERY OPTIMIZATION MAINTAINED BY SYSTEM IN DB4MQT.TS4MQT;");
		createList
				.add("CREATE VIEW C12OP02.VIEW_TAB4MQT(\"Column1\", \"Column2\") AS (SELECT * FROM C12OP02.TAB4MQT);");
		createList.add(
				"CREATE TABLE C12OP02.MQT_VIEW_TAB4MQT(\"Column1\", \"Column2\") AS (SELECT * FROM C12OP02.VIEW_TAB4MQT) DATA INITIALLY DEFERRED REFRESH DEFERRED ENABLE QUERY OPTIMIZATION MAINTAINED BY SYSTEM IN DB4MQT.TS4MQT;");

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);

	}

	/**
	 * Test retrieval of MQT properties
	 */
	@Test(description = "MQT properties retrieval")
	public void testViewMQTProperties() {
		logger.info("Running View MQT Property...");

		getTestingResult("getZOSMQTBasedOnTableProperty", "getZOSMQTBasedOnTableProperty_result");

		getTestingResult("getZOSMQTBasedOnViewProperty", "getZOSMQTBasedOnViewProperty_result");

		logger.info("PASSED: Testing of finding out properties of MQT executed successfully");
	}

	/**
	 * Clean environment for MQT properties view
	 */
	public void testViewMQTProperties_clean() {
		List<String> dropList = new ArrayList<String>();
		dropList.add("DROP TABLE C12OP02.MQT_TAB4MQT;");
		dropList.add("DROP TABLE C12OP02.MQT_VIEW_TAB4MQT;");
		dropList.add("DROP VIEW C12OP02.VIEW_TAB4MQT;");
		dropList.add("DROP TABLE C12OP02.TAB4MQT;");
		dropList.add("DROP TABLESPACE DB4MQT.TS4MQT;");
		dropList.add("DROP STOGROUP SG4MQT;");
		dropList.add("DROP DATABASE DB4MQT;");

		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);
	}

	/**
	 * Test retrieval of constraint properties
	 */
	@Test(description = "Constraint properties retrieval")
	public void testViewConstraintProperties() {
		logger.info("Running View Constraint Property...");

		// Verify attributes of primary constraint
		getTestingResult("getZOSConstraintPrimaryProperty", "getZOSConstraintPrimaryProperty_result");

		// Verify attributes of foreign key constraint
		getTestingResult("getZOSConstraintForeignKeyProperty", "getZOSConstraintForeignKeyProperty_result");

		// Verify attributes of unique constraint
		getTestingResult("getZOSConstraintUniqueProperty", "getZOSConstraintUniqueProperty_result");

		// Verify attributes of check constraint
		getTestingResult("getZOSConstraintCheckProperty", "getZOSConstraintCheckProperty_result");

		logger.info("PASSED: Testing of finding out properties of Constraint executed successfully");
	}

	/**
	 * Precondition for view column properties
	 */
	public void testViewColumnProperties_precondition() {
		List<String> dropList = new ArrayList<String>();
		dropList.add("DROP TABLE C12OP02.TAB4COL;");
		dropList.add("DROP TABLESPACE DB4COL.TS4COL;");
		dropList.add("DROP TABLESPACE DB4COL.TS4COL2;");
		dropList.add("DROP STOGROUP SG4COL;");
		dropList.add("DROP DATABASE DB4COL;");

		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);

		List<String> createList = new ArrayList<String>();
		createList.add("CREATE DATABASE DB4COL CCSID EBCDIC;");
		createList.add("CREATE STOGROUP SG4COL VOLUMES(\'*\') VCAT DSNC000;");
		createList.add(
				"CREATE TABLESPACE TS4COL IN DB4COL USING STOGROUP SG4COL PRIQTY -1 SECQTY -1 CCSID EBCDIC LOCKSIZE ANY MAXROWS 255;");
		createList.add(
				"CREATE TABLESPACE TS4COL2 IN DB4COL USING STOGROUP SG4COL PRIQTY -1 SECQTY -1 CCSID EBCDIC LOCKSIZE ANY MAXROWS 255;");
		createList.add(
				"CREATE TABLE C12OP02.TAB4COL(SYSTEM_START_TIME1 TIMESTAMP(12) NOT NULL GENERATED ALWAYS AS ROW BEGIN, SYSTEM_END_TIME1 TIMESTAMP(12) NOT NULL GENERATED ALWAYS AS ROW END, TRANS_ID1 TIMESTAMP(12) GENERATED ALWAYS AS TRANSACTION START ID, APPLICATION_START_TIME1 TIMESTAMP NOT NULL, APPLICATION_END_TIME1 TIMESTAMP NOT NULL, \"Column1\" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 10 MINVALUE -2147483648 MAXVALUE 2147483647 CYCLE CACHE 40 ORDER ), \"Column2\" CHAR(5) WITH DEFAULT \'a\', PERIOD SYSTEM_TIME(\"SYSTEM_START_TIME1\", \"SYSTEM_END_TIME1\"), PERIOD BUSINESS_TIME(\"APPLICATION_START_TIME1\", \"APPLICATION_END_TIME1\")  ) IN DB4COL.TS4COL AUDIT NONE DATA CAPTURE NONE CCSID EBCDIC;");
		createList.add(
				"CREATE TABLE C12OP02.TAB4COL_HIST(SYSTEM_START_TIME1 TIMESTAMP(12) NOT NULL, SYSTEM_END_TIME1 TIMESTAMP(12) NOT NULL, TRANS_ID1 TIMESTAMP(12), APPLICATION_START_TIME1 TIMESTAMP NOT NULL, APPLICATION_END_TIME1 TIMESTAMP NOT NULL, \"Column1\" INTEGER NOT NULL, \"Column2\" CHAR(5)) IN DB4COL.TS4COL2 AUDIT NONE DATA CAPTURE NONE CCSID EBCDIC;");
		createList.add("ALTER TABLE C12OP02.TAB4COL ADD VERSIONING USE HISTORY TABLE C12OP02.TAB4COL_HIST;");

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}

	/**
	 * Test retrieval of Column properties
	 */
	@Test(description = "Column properties retrieval")
	public void testViewColumnProperties() {
		logger.info("Running View Column Property...");

		// Verify column with temporal attribute, take SYSTEM_START_TIME1 column
		// in bitemporal table as an example
		getTestingResult("getZOSColumnTemporalAttributeProperty", "getZOSColumnTemporalAttributeProperty_result");

		// Verify column with identity attribute, take INTEGER column with
		// START, INCREMENT BY , MINVALUE, MAXVALUE, CYCLE, CACHE, ORDER as an
		// example
		getTestingResult("getZOSColumnIdentityAttributeProperty", "getZOSColumnIdentityAttributeProperty_result");

		// Verify column with general attribute, take CHAR column as an example
		getTestingResult("getZOSColumnGeneralAttributeProperty", "getZOSColumnGeneralAttributeProperty_result");

		logger.info("PASSED: Testing of finding out properties of Column executed successfully");
	}

	/**
	 * Clean environment for column properties view
	 */
	public void testViewColumnProperties_clean() {
		List<String> dropList = new ArrayList<String>();
		dropList.add("DROP TABLE C12OP02.TAB4COL;");
		dropList.add("DROP TABLESPACE DB4COL.TS4COL;");
		dropList.add("DROP TABLESPACE DB4COL.TS4COL2;");
		dropList.add("DROP STOGROUP SG4COL;");
		dropList.add("DROP DATABASE DB4COL;");

		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);
	}

	/**
	 * Precondition for view StoredProcedure properties
	 */
	public void testViewStoredProcedureProperties_precondition() {
		setDefaultSchema();

		sqlEditorService.runSQLinJDBC(
  "CREATE PROCEDURE SYSADM.SP_DSM(IN V1 INTEGER, OUT V2 CHAR(9)) LANGUAGE C "
  + "DETERMINISTIC "
  + "NO SQL "
  + "EXTERNAL NAME SUMMOD "
  + "COLLID SUMCOLL "
  + "ASUTIME LIMIT 900 "
  + "PARAMETER STYLE GENERAL WITH NULLS "
  + "STAY RESIDENT NO "
  + "RUN OPTIONS 'MSGFILE(OUTFILE),RPTSTG(ON),RPTOPTS(ON)' "
  + "WLM ENVIRONMENT PAYROLL "
  + "PROGRAM TYPE MAIN "
  + "SECURITY DB2 "
  + "DYNAMIC RESULT SETS 10 COMMIT ON RETURN NO;",
				";", this.dbProfile);

	}

	/***
	 * Test retrieval of Stored Procedure properties
	 */
	@Test(description = "Stored Procedure properties retrieval")
	public void testViewStoredProcedureProperties() {
		logger.info("Running View Stored Procedure Property...");

		getTestingResult("getZOSStoredProcedureProperty", "getZOSStoredProcedureProperty_result");

		logger.info("PASSED: Testing of finding out properties of a Stored Procedure executed successfully");
	}

	/**
	 * Precondition for view Stored Procedure properties
	 */
	public void testViewStoredProcedureProperties_clean() {
		sqlEditorService.runSQLinJDBC("DROP PROCEDURE SYSADM.SP_DSM;", ";", this.dbProfile, false);
	}

	/**
	 * Precondition for view Package properties
	 */
	public void testViewPackageProperties_precondition() {
    // use the system package
		setDefaultSchema();
		//SYSADM.DSNADMCD
	}

	/**
	 * Test retrieval of Package properties
	 */
	@Test(description = "Package properties retrieval")
	public void testViewPackageProperties() {
		logger.info("Running View Package Property...");

		getTestingResult("getZOSPackageProperty", "getZOSPackageProperty_result");

		logger.info("PASSED: Testing of finding out properties of a Package executed successfully");
	}

	/**
	 * Precondition for view Package properties
	 */
	public void testViewPackageProperties_clean() {
	//no need to clean
	}

	/**
	 * Precondition for view UDF properties
	 */
	public void testViewUDFProperties_precondition() {
		setDefaultSchema();
		sqlEditorService.runSQLinJDBC("DROP FUNCTION SYSADM.DSM_FUNCTION1;", ";", this.dbProfile, false);
		sqlEditorService.runSQLinJDBC("CREATE FUNCTION SYSADM.DSM_FUNCTION1 (VARNAME VARCHAR(128))"
			+"RETURNS TABLE (NAME VARCHAR(128)) "
			+"LANGUAGE SQL "
			+"READS SQL DATA "
			+"NO EXTERNAL ACTION "
			+"DETERMINISTIC "
			+"RETURN SELECT NAME FROM SYSIBM.SYSTABLES WHERE CREATOR = 'SYSADM' AND NAME = VARNAME;",
				";", this.dbProfile);

	}

	/**
	 * Test retrieval of User-Defined Function properties
	 */
	@Test(description = "UDF properties retrieval")
	public void testViewUDFProperties() {
		logger.info("Running View UDF Property...");

		getTestingResult("getZOSUDFProperty", "getZOSUDFProperty_result");

		logger.info("PASSED: Testing of finding out properties of a UDF executed successfully");
	}

	/**
	 * Precondition for view UDF properties
	 */
	public void testViewUDFProperties_clean() {
		setDefaultSchema();
		sqlEditorService.runSQLinJDBC("DROP FUNCTION SYSAMD.DSM_FUNCTION1;", ";", this.dbProfile, false);
	}

	
	/**
	 * Precondition for view UDT properties
	 */
	public void testViewUDTProperties_precondition() {
		setDefaultSchema();
		sqlEditorService.runSQLinJDBC("CREATE TYPE SYSADM.DSM_TYPE AS INTEGER;", ";", this.dbProfile);
	}

	/**
	 * Test retrieval of User-Defined Type properties
	 */
	@Test(description = "UDT properties retrieval")
	public void testViewUDTProperties() {
		logger.info("Running View UDT Property...");

		getTestingResult("getZOSUDTProperty", "getZOSUDTProperty_result");

		logger.info("PASSED: Testing of finding out properties of a UDT executed successfully");
	}

	/**
	 * Precondition for view UDT properties
	 */
	public void testViewUDTProperties_clean() {
		sqlEditorService.runSQLinJDBC("DROP TYPE SYSADM.DSM_TYPE;", ";", this.dbProfile, false);
	}

}
