package com.ibm.datatools.ots.tests.restapi.admin;

import java.util.Iterator;
import java.util.Map.Entry;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.testng.Assert;
import com.ibm.datatools.ots.tests.restapi.common.AdminPrivilegesService;
import com.ibm.datatools.ots.tests.restapi.common.AdminSQLEditorService;
import com.jayway.jsonpath.JsonPath;

public class AdminPrivilegesTest extends AdminTestBase
{
	protected AdminPrivilegesService privilegesService = new AdminPrivilegesService();
	protected AdminSQLEditorService sqlEditorService = new AdminSQLEditorService();
	
	/**
	 * @author hongcbj@cn.ibm.com 
	 * the common function for the Admin Privileges test
	 */
	protected void executeTest(String url, String property, String baseProperty){
		String query = privilegesService.getJSONData( property );
		JSONObject resObj =
				privilegesService.callPrivilegesService( url, query, dbProfile );
		
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );
		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		
		JSONObject propResult = JsonPath.read( responseData, "$" );
		String base = privilegesService.getJSONData( baseProperty );
		JSONObject propBase = JSONObject.fromObject( base );
		
		Assert.assertTrue(compareJsons(propBase, propResult));
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * the common function for the Admin Privileges test
	 */
	protected boolean compareJsons( JSONObject base, JSONObject result ) 
	{
		if ( base.compareTo( result ) == 0 )
		{
			return true;
		}
		Iterator its = base.entrySet().iterator();
		while ( its.hasNext() )
		{
			Entry entry = (Entry)its.next();
			String key = (String)entry.getKey();
			if ( result.containsKey( key ) )
			{
				JSONArray base_ja = base.getJSONArray( key );
				JSONArray result_ja = result.getJSONArray( key );
				
				if (base_ja.equals(result_ja))
				{
					continue;
				}
				else if (key.equalsIgnoreCase( "AUTHIDS" ))
				{
					if (result_ja.contains(base_ja)) continue ;
				}
				else
				{
					Assert.fail( "Suppose value of key " + key + " is " + base_ja + ",but current result is "
							+ result_ja );
					return false;
				}
			}
			else
			{
				Assert.fail( "No Key:" + key );
				return false;
			}

		}
		return true;
	}
}
