package com.ibm.datatools.ots.tests.restapi.admin;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.AdminPrivilegesService;
import com.jayway.jsonpath.JsonPath;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 
 * @author hongcbj@cn.ibm.com
 *
 */
public class AdminPrivilegesTest_LUW extends AdminPrivilegesTest{

	private static final Logger logger = LogManager.getLogger(AdminPrivilegesTest_LUW.class);
	
	@BeforeClass
	public void beforeTest(){
		testAdminPrivileges_precondition();
	}
	
	@AfterClass
	public void afterTest(){
		testAdminPrivileges_clean();
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 */
	public void testAdminPrivileges_precondition(){
		List<String> createList = new ArrayList<String>();
		List<String> dropList = new ArrayList<String>();
		
		//try clean before create
		// drop user
		dropUser(false);
		dropList.add("REVOKE CONNECT ON DATABASE FROM USER \"USER2_PRIVI\"");
		// drop group
		dropGroup(false);
		dropList.add("REVOKE CONNECT ON DATABASE FROM GROUP \"GROUP2_PRIVI\"");
		// drop role
		dropRole(false);
		dropList.add("DROP ROLE \"ROLE2_PRIVI\"");
		// drop view
		dropList.add("DROP VIEW \"SCHEMA_PRIVI\".\"VIEW_PRIVI\";");	
		// drop index
		dropList.add("DROP INDEX \"SCHEMA_PRIVI\".\"INDEX_PRIVI\";");
		// drop table
		dropList.add("DROP TABLE \"SCHEMA_PRIVI\".\"TABLE_PRIVI\";");				
		// drop tablespace
		dropList.add("DROP TABLESPACE \"TABLESPACE_PRIVI\";");		
		// drop schema
		dropList.add("DROP SCHEMA \"SCHEMA_PRIVI\" RESTRICT;");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);
		
		// prepare for test environment
		// create schema
		createList.add("CREATE SCHEMA \"SCHEMA_PRIVI\";");
		
		// create tablespace
		createList.add("CREATE TABLESPACE \"TABLESPACE_PRIVI\";");
		
		// create table
		createList.add("CREATE TABLE \"SCHEMA_PRIVI\".\"TABLE_PRIVI\" ( \"NEW_COLUMN1\" CHAR(5) );");
		
		// create view
		createList.add("CREATE VIEW \"SCHEMA_PRIVI\".\"VIEW_PRIVI\" AS SELECT * FROM \"SCHEMA_PRIVI\".\"TABLE_PRIVI\";");
		
		// create index
		createList.add("CREATE UNIQUE INDEX \"SCHEMA_PRIVI\".\"INDEX_PRIVI\" ON \"SCHEMA_PRIVI\".\"TABLE_PRIVI\" (\"NEW_COLUMN1\" ASC);");
		
		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true) ;
		
		// create role
		createRole();
		
		// create group with privileges
		createGroup();
		
		// create user with privileges
		createUser();	
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 */
	public void testAdminPrivileges_clean(){
		List<String> dropList = new ArrayList<String>();
				
		// drop user
		dropUser(true);
		
		// drop group
		dropGroup(true);
		
		// drop role
		dropRole(true);
		
		// drop view
		dropList.add("DROP VIEW \"SCHEMA_PRIVI\".\"VIEW_PRIVI\";");
		
		// drop index
		dropList.add("DROP INDEX \"SCHEMA_PRIVI\".\"INDEX_PRIVI\";");
		
		// drop table
		dropList.add("DROP TABLE \"SCHEMA_PRIVI\".\"TABLE_PRIVI\";");
				
		// drop tablespace
		dropList.add("DROP TABLESPACE \"TABLESPACE_PRIVI\";");
		
		// drop schema
		dropList.add("DROP SCHEMA \"SCHEMA_PRIVI\" RESTRICT;");
		
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, true) ;
		
	}
	
	public void createUser(){
		List<String> createList = new ArrayList<String>();
		
		createList.add("GRANT CONNECT ON DATABASE TO USER \"USER_PRIVI\";");
		createList.add("GRANT CONTROL ON TABLE \"SCHEMA_PRIVI\".\"TABLE_PRIVI\" TO USER \"USER_PRIVI\";");
		createList.add("GRANT ALTER,DELETE,UPDATE ON TABLE \"SCHEMA_PRIVI\".\"TABLE_PRIVI\" TO USER \"USER_PRIVI\";");
		createList.add("GRANT ALTER,INSERT,SELECT ON TABLE \"SCHEMA_PRIVI\".\"TABLE_PRIVI\" TO USER \"USER_PRIVI\" WITH GRANT OPTION;");
		createList.add("GRANT ALTERIN ON SCHEMA \"SCHEMA_PRIVI\" TO USER \"USER_PRIVI\";");
		createList.add("GRANT CONTROL ON INDEX \"SCHEMA_PRIVI\".\"INDEX_PRIVI\" TO USER \"USER_PRIVI\";");
		
		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true) ;
	}
	
	public void dropUser(boolean type){
		List<String> dropList = new ArrayList<String>();
		
		// drop user
		dropList.add("REVOKE CONTROL ON TABLE \"SCHEMA_PRIVI\".\"TABLE_PRIVI\" FROM USER \"USER_PRIVI\";");
		dropList.add("REVOKE CONTROL ON INDEX \"SCHEMA_PRIVI\".\"INDEX_PRIVI\" FROM USER \"USER_PRIVI\";");
		dropList.add("REVOKE ALTERIN ON SCHEMA \"SCHEMA_PRIVI\" FROM USER \"USER_PRIVI\";");
		dropList.add("REVOKE UPDATE ON TABLE \"SCHEMA_PRIVI\".\"TABLE_PRIVI\" FROM USER \"USER_PRIVI\";");
		dropList.add("REVOKE SELECT ON TABLE \"SCHEMA_PRIVI\".\"TABLE_PRIVI\" FROM USER \"USER_PRIVI\";");
		dropList.add("REVOKE INSERT ON TABLE \"SCHEMA_PRIVI\".\"TABLE_PRIVI\" FROM USER \"USER_PRIVI\";");
		dropList.add("REVOKE DELETE ON TABLE \"SCHEMA_PRIVI\".\"TABLE_PRIVI\" FROM USER \"USER_PRIVI\";");
		dropList.add("REVOKE ALTER ON TABLE \"SCHEMA_PRIVI\".\"TABLE_PRIVI\" FROM USER \"USER_PRIVI\";");
		dropList.add("REVOKE ROLE \"ROLE_PRIVI\" FROM USER \"USER_PRIVI\";");
		dropList.add("REVOKE CONNECT ON DATABASE FROM USER \"USER_PRIVI\"");
		
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, type) ;
	}
	
	public void createGroup(){
		List<String> createList = new ArrayList<String>();
		
		// create group
		createList.add("GRANT CONNECT ON DATABASE TO GROUP \"GROUP_PRIVI\";");
		createList.add("GRANT CONTROL ON TABLE \"SCHEMA_PRIVI\".\"VIEW_PRIVI\" TO GROUP \"GROUP_PRIVI\";");
		createList.add("GRANT DELETE,SELECT ON TABLE \"SCHEMA_PRIVI\".\"VIEW_PRIVI\" TO GROUP \"GROUP_PRIVI\" WITH GRANT OPTION;");
		createList.add("GRANT USE OF TABLESPACE \"TABLESPACE_PRIVI\" TO GROUP \"GROUP_PRIVI\" WITH GRANT OPTION;");
		createList.add("GRANT CONTROL ON INDEX \"SCHEMA_PRIVI\".\"INDEX_PRIVI\" TO GROUP \"GROUP_PRIVI\";");
		
		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true) ;
	}
	
	public void dropGroup(boolean type){
		List<String> dropList = new ArrayList<String>();
		
		// drop group
		dropList.add("REVOKE CONTROL ON TABLE \"SCHEMA_PRIVI\".\"VIEW_PRIVI\" FROM GROUP \"GROUP_PRIVI\";");
		dropList.add("REVOKE USE OF TABLESPACE \"TABLESPACE_PRIVI\" FROM GROUP \"GROUP_PRIVI\";");
		dropList.add("REVOKE CONTROL ON INDEX \"SCHEMA_PRIVI\".\"INDEX_PRIVI\" FROM GROUP \"GROUP_PRIVI\";");
		dropList.add("REVOKE UPDATE ON TABLE \"SCHEMA_PRIVI\".\"VIEW_PRIVI\" FROM GROUP \"GROUP_PRIVI\";");
		dropList.add("REVOKE SELECT ON TABLE \"SCHEMA_PRIVI\".\"VIEW_PRIVI\" FROM GROUP \"GROUP_PRIVI\";");
		dropList.add("REVOKE INSERT ON TABLE \"SCHEMA_PRIVI\".\"VIEW_PRIVI\" FROM GROUP \"GROUP_PRIVI\";");
		dropList.add("REVOKE DELETE ON TABLE \"SCHEMA_PRIVI\".\"VIEW_PRIVI\" FROM GROUP \"GROUP_PRIVI\";");
		dropList.add("REVOKE ROLE \"ROLE_PRIVI\" FROM GROUP \"GROUP_PRIVI\";");
		dropList.add("REVOKE CONNECT ON DATABASE FROM GROUP \"GROUP_PRIVI\";");
		
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, type) ;
	}
	
	public void createRole(){
		List<String> createList = new ArrayList<String>();
		// create role
		createList.add("CREATE ROLE \"ROLE_PRIVI\";");
		createList.add("GRANT ALTERIN ON SCHEMA \"SCHEMA_PRIVI\" TO ROLE \"ROLE_PRIVI\";");
		createList.add("GRANT USE OF TABLESPACE \"TABLESPACE_PRIVI\" TO ROLE \"ROLE_PRIVI\";");
		createList.add("GRANT ROLE \"ROLE_PRIVI\" TO USER \"USER_PRIVI\";");
		createList.add("GRANT ROLE \"ROLE_PRIVI\" TO GROUP \"GROUP_PRIVI\";");
		
		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true) ;
	}
	
	public void dropRole(boolean type){
		List<String> dropList = new ArrayList<String>();
		// drop role
		dropList.add("REVOKE USE OF TABLESPACE \"TABLESPACE_PRIVI\" FROM ROLE \"ROLE_PRIVI\";");
		dropList.add("REVOKE ALTERIN ON SCHEMA \"SCHEMA_PRIVI\" FROM ROLE \"ROLE_PRIVI\";");
		dropList.add("DROP ROLE \"ROLE_PRIVI\";");
		
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, type) ;
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test the view user privileges with Tables object and role function 
	 */
	@Test(description="Test the view user privileges with Tables object and role function")
	public void testViewUserPrivileges(){
		
		logger.info("Running View User Privileges with Tables object and role test...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_DEFINITION_LUW,"getViewUserPrivileges", "getViewUserPrivileges_result");
		
		logger.info("PASSED: View User Privileges with Tables object and role test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test the view group privileges with Views object and role function 
	 */
	@Test(description="Test the view group privileges with Views object and role function")
	public void testViewGroupPrivileges(){
		
		logger.info("Running View Group Privileges with Views object and role test...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_DEFINITION_LUW,"getViewGroupPrivileges", "getViewGroupPrivileges_result");
		
		logger.info("PASSED: View Group Privileges with Views object and role test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test the view role privileges with User and Schema object function 
	 */
	@Test(description="Test the view role privileges with User and Schema object function")
	public void testViewRolePrivileges_User(){
		
		logger.info("Running View role privileges with User and Schema object test...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_DEFINITION_LUW,"getViewRolePrivileges_schema_user", "getViewRolePrivileges_schema_user_result");
		
		logger.info("PASSED: View Role Privileges with User and Schema object test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test the view role privileges with Group and Table Spaces object function 
	 */
	@Test(description="Test the view role privileges with Group and Table Spaces object function")
	public void testViewRolePrivileges_Group(){
		
		logger.info("Running View role privileges with Group and Table Spaces object test...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_DEFINITION_LUW,"getViewRolePrivileges_tablespace_group", "getViewRolePrivileges_tablespace_group_result");
		
		logger.info("PASSED: View Role Privileges with Group and Table Spaces object test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test the view SQL Object privileges with Group and Index object function 
	 */
	@Test(description="Test the view SQL Object privileges with Group and Index object function")
	public void testViewSQLObjectPrivileges_Index_Group(){
		
		logger.info("Running View SQL Object privileges with Group and Index object test...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_DEFINITION_LUW,"getViewSQLObjectPrivileges_index_group", "getViewSQLObjectPrivileges_index_group_result");
		
		logger.info("PASSED: View SQL Object Privileges with Group and Index object test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Administer Privileges Add User function, add a user 'USER2_PRIVI'
	 */
	@Test(description="Test Administer Privileges Add User function")
	public void testAddUser(){
		
		logger.info("Running Administer Privileges Add User ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getAddUser", "getAddUser_result");
		
		logger.info("PASSED: Administer Privileges Add User test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test execute Administer Privileges Add User DDL function 
	 */
	@Test(description="Test execute Administer Privileges Add User DDL function",dependsOnMethods = {"testAddUser"})
	public void testAddUserExecuteDDL(){
		
		logger.info("Running Execute Add User DDL ...");
		
		sqlEditorService.runSQLinJDBC("GRANT CONNECT ON DATABASE TO USER \"USER2_PRIVI\"", ";", this.dbProfile,true) ;
		
		logger.info("PASSED: Execute Add User DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Grant roles 'ROLE_PRIVI' to user 'USER2_PRIVI' function 
	 */
	@Test(description="Test Grant roles to user function",dependsOnMethods = {"testAddUserExecuteDDL"})
	public void testGrantRoleToUser(){
		
		logger.info("Running Grant roles to user ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getGrantRoleToUser", "getGrantRoleToUser_result");
		
		logger.info("PASSED: Grant roles to user test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Grant roles to user DDL function 
	 */
	@Test(description="Test Grant roles to user DDL function",dependsOnMethods = {"testGrantRoleToUser"})
	public void testGrantRoleToUserExecuteDDL(){
		
		logger.info("Running Grant roles to user DDL ...");
		
		List<String> grantList = new ArrayList<String>();
		grantList.add("GRANT ROLE \"ROLE_PRIVI\" TO USER \"USER2_PRIVI\"");
		sqlEditorService.runSQLinJDBC(grantList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Grant roles to user DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Revoke roles 'ROLE_PRIVI' from user 'USER2_PRIVI' function 
	 */
	@Test(description="Test Revoke roles to user function",dependsOnMethods = {"testGrantRoleToUserExecuteDDL"})
	public void testRevokeRoleToUser(){
		
		logger.info("Running Revoke roles to user ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getRevokeRoleToUser", "getRevokeRoleToUser_result");
		
		logger.info("PASSED: Revoke roles to user test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Revoke roles 'ROLE_PRIVI' from user 'USER2_PRIVI' DDL function 
	 */
	@Test(description="Test Revoke roles to user DDL function",dependsOnMethods = {"testRevokeRoleToUser"})
	public void testRevokeRoleToUserExecuteDDL(){
		
		logger.info("Running Revoke roles to user DDL ...");
		
		List<String> revokeList = new ArrayList<String>();
		revokeList.add("REVOKE ROLE \"ROLE_PRIVI\" FROM USER \"USER2_PRIVI\"");
		sqlEditorService.runSQLinJDBC(revokeList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Revoke roles to user DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Grant roles 'ROLE_PRIVI' to user 'USER2_PRIVI' with Admin Options function 
	 */
	@Test(description="Test Grant roles to user with Admin Options function",dependsOnMethods = {"testRevokeRoleToUserExecuteDDL"})
	public void testGrantRoleToUserWithAdminOptions(){
		
		logger.info("Running Grant roles to user with Admin Options ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getGrantRoleToUserWithAdminOptions", "getGrantRoleToUserWithAdminOptions_result");
		
		logger.info("PASSED: Grant roles to user With Admin Options test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Grant roles to user with Admin Options DDL function 
	 */
	@Test(description="Test Grant roles to user with Admin Options DDL function",dependsOnMethods = {"testGrantRoleToUserWithAdminOptions"})
	public void testGrantRoleToUserWithAdminOptionsExecuteDDL(){
		
		logger.info("Running Grant roles to user with Admin Options DDL ...");
		
		List<String> grantList = new ArrayList<String>();
		grantList.add("GRANT ROLE \"ROLE_PRIVI\" TO USER \"USER2_PRIVI\" WITH ADMIN OPTION");
		sqlEditorService.runSQLinJDBC(grantList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Grant roles to user With Admin Options DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Revoke roles 'ROLE_PRIVI' from user 'USER2_PRIVI' with Admin Options function 
	 */
	@Test(description="Test Revoke roles to user with Admin Options function",dependsOnMethods = {"testGrantRoleToUserWithAdminOptionsExecuteDDL"})
	public void testRevokeRoleToUserWithAdminOptions(){
		
		logger.info("Running Revoke roles to user with Admin Options ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getRevokeRoleToUserWithAdminOptions", "getRevokeRoleToUserWithAdminOptions_result");
		
		logger.info("PASSED: Revoke roles to user With Admin Options test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Revoke roles to user with Admin Options DDL function 
	 */
	@Test(description="Test Revoke roles to user with Admin Options DDL function",dependsOnMethods = {"testRevokeRoleToUserWithAdminOptions"})
	public void testRevokeRoleToUserWithAdminOptionsExecuteDDL(){
		
		logger.info("Running Revoke roles to user with Admin Options DDL ...");
		
		List<String> revokeList = new ArrayList<String>();
		revokeList.add("REVOKE ROLE \"ROLE_PRIVI\" FROM USER \"USER2_PRIVI\";");
		revokeList.add("GRANT ROLE \"ROLE_PRIVI\" TO USER \"USER2_PRIVI\"");
		sqlEditorService.runSQLinJDBC(revokeList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Revoke roles to user With Admin Options DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Grant objects privileges to user 'USER2_PRIVI' function 
	 */
	@Test(description="Test Grant objects privileges to user function",dependsOnMethods = {"testRevokeRoleToUserWithAdminOptionsExecuteDDL"})
	public void testGrantObjectsPrivilegesToUser(){
		
		logger.info("Running Grant Object Privileges to user ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getGrantObjectPrivilegesToUser", "getGrantObjectPrivilegesToUser_result");
		
		logger.info("PASSED: Grant object privileges to user test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Grant objects privileges to user 'USER2_PRIVI' DDL function 
	 */
	@Test(description="Test Grant objects privileges to user DDL function",dependsOnMethods = {"testGrantObjectsPrivilegesToUser"})
	public void testGrantObjectsPrivilegesToUserExecuteDDL(){
		
		logger.info("Running Grant Object Privileges to user DDL ...");
		
		List<String> grantList = new ArrayList<String>();
		grantList.add("GRANT SELECT ON TABLE \"SCHEMA_PRIVI\".\"VIEW_PRIVI\" TO USER \"USER2_PRIVI\"");
		sqlEditorService.runSQLinJDBC(grantList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Grant object privileges to user DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Revoke objects privileges to user 'USER2_PRIVI' function 
	 */
	@Test(description="Test Revoke objects privileges to user function",dependsOnMethods = {"testGrantObjectsPrivilegesToUserExecuteDDL"})
	public void testRevokeObjectsPrivilegesToUser(){
		
		logger.info("Running Revoke Object Privileges to user ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getRevokeObjectPrivilegesToUser", "getRevokeObjectPrivilegesToUser_result");
		
		logger.info("PASSED: Grant Revoke privileges to user test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Revoke objects privileges to user 'USER2_PRIVI' DDL function 
	 */
	@Test(description="Test Revoke objects privileges to user DDL function",dependsOnMethods = {"testRevokeObjectsPrivilegesToUser"})
	public void testRevokeObjectsPrivilegesToUserExecuteDDL(){
		
		logger.info("Running Revoke Object Privileges to user DDL ...");
		
		List<String> revokeList = new ArrayList<String>();
		revokeList.add("REVOKE SELECT ON TABLE \"SCHEMA_PRIVI\".\"VIEW_PRIVI\" FROM USER \"USER2_PRIVI\"");
		sqlEditorService.runSQLinJDBC(revokeList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Revoke object privileges to user DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Grant objects privileges to user 'USER2_PRIVI' with grant options function 
	 */
	@Test(description="Test Grant objects privileges to user with grant options function",dependsOnMethods = {"testRevokeObjectsPrivilegesToUserExecuteDDL"})
	public void testGrantObjectsPrivilegesToUserWithGrantOptions(){
		
		logger.info("Running Grant Object Privileges to user with grant options ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getGrantObjectPrivilegesToUserWithGrantOptions", "getGrantObjectPrivilegesToUserWithGrantOptions_result");
		
		logger.info("PASSED: Grant object privileges to user test with grant options executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Grant objects privileges to user 'USER2_PRIVI' with grant options DDL function 
	 */
	@Test(description="Test Grant objects privileges to user with grant options DDL function",dependsOnMethods = {"testGrantObjectsPrivilegesToUserWithGrantOptions"})
	public void testGrantObjectsPrivilegesToUserWithGrantOptionsExecuteDDL(){
		
		logger.info("Running Grant Object Privileges to user with grant options DDL ...");
		
		List<String> grantList = new ArrayList<String>();
		grantList.add("GRANT UPDATE ON TABLE \"SCHEMA_PRIVI\".\"TABLE_PRIVI\" TO USER \"USER2_PRIVI\" WITH GRANT OPTION;");
		sqlEditorService.runSQLinJDBC(grantList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Grant object privileges to user with grant options DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Revoke objects privileges to user 'USER2_PRIVI' with grant options function 
	 */
	@Test(description="Test Revoke objects privileges to user with grant options function",dependsOnMethods = {"testGrantObjectsPrivilegesToUserWithGrantOptionsExecuteDDL"})
	public void testRevokeObjectsPrivilegesToUserWithGrantOptions(){
		
		logger.info("Running Revoke Object Privileges to user with grant options ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getRevokeObjectPrivilegesToUserWithGrantOptions", "getRevokeObjectPrivilegesToUserWithGrantOptions_result");
		
		logger.info("PASSED: Revoke object privileges to user test with grant options executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Revoke objects privileges to user 'USER2_PRIVI' with grant options DDL function 
	 */
	@Test(description="Test Revoke objects privileges to user with grant options DDL function",dependsOnMethods = {"testRevokeObjectsPrivilegesToUserWithGrantOptions"})
	public void testRevokeObjectsPrivilegesToUserWithGrantOptionsExecuteDDL(){
		
		logger.info("Running Revoke Object Privileges to user with grant options DDL ...");
		
		List<String> revokeList = new ArrayList<String>();
		revokeList.add("REVOKE UPDATE ON TABLE \"SCHEMA_PRIVI\".\"TABLE_PRIVI\" FROM USER \"USER2_PRIVI\";");
		revokeList.add("GRANT UPDATE ON TABLE \"SCHEMA_PRIVI\".\"TABLE_PRIVI\" TO USER \"USER2_PRIVI\"");
		sqlEditorService.runSQLinJDBC(revokeList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Revoke object privileges to user with grant options DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Remove User 'USER2_PRIVI' function 
	 */
	@Test(description="Test Remove User function",dependsOnMethods = {"testRevokeObjectsPrivilegesToUserWithGrantOptionsExecuteDDL"})
	public void testRemoveUser(){
		
		logger.info("Running Remove User ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_REMOVE_COMMAND_LUW,"getRemoveUser", "getRemoveUser_result");
		
		logger.info("PASSED: Remove User test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Remove User 'USER2_PRIVI' DDL function 
	 */
	@Test(description="Test Remove User DDL function",dependsOnMethods = {"testRemoveUser"})
	public void testRemoveUserExecuteDDL(){
		
		logger.info("Running Remove User DDL ...");
		
		List<String> dropList = new ArrayList<String>();
		dropList.add("REVOKE UPDATE ON TABLE \"SCHEMA_PRIVI\".\"TABLE_PRIVI\" FROM USER \"USER2_PRIVI\";");
		dropList.add("REVOKE ROLE \"ROLE_PRIVI\" FROM USER \"USER2_PRIVI\";");
		dropList.add("REVOKE CONNECT ON DATABASE FROM USER \"USER2_PRIVI\"");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Remove User DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Administer Privileges Add Group function, add a group 'GROUP2_PRIVI'
	 */
	@Test(description="Test Administer Privileges Add Group function")
	public void testAddGroup(){
		
		logger.info("Running Administer Privileges Add Group ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getAddGroup", "getAddGroup_result");
		
		logger.info("PASSED: Administer Privileges Add Group test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test execute Administer Privileges Add Group DDL function 
	 */
	@Test(description="Test execute Administer Privileges Add Group DDL function",dependsOnMethods = {"testAddGroup"})
	public void testAddGroupExecuteDDL(){
		
		logger.info("Running Execute Add Group DDL ...");
		
		sqlEditorService.runSQLinJDBC("GRANT CONNECT ON DATABASE TO GROUP \"GROUP2_PRIVI\"", ";", this.dbProfile,true) ;
		
		logger.info("PASSED: Execute Add Group DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Grant roles 'ROLE_PRIVI' to group 'GROUP2_PRIVI' function 
	 */
	@Test(description="Test Grant roles to group function",dependsOnMethods = {"testAddGroupExecuteDDL"})
	public void testGrantRoleToGroup(){
		
		logger.info("Running Grant roles to group ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getGrantRoleToGroup", "getGrantRoleToGroup_result");
		
		logger.info("PASSED: Grant roles to group test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Grant roles to Group DDL function 
	 */
	@Test(description="Test Grant roles to Group DDL function",dependsOnMethods = {"testGrantRoleToGroup"})
	public void testGrantRoleToGroupExecuteDDL(){
		
		logger.info("Running Grant roles to Group DDL ...");
		
		List<String> grantList = new ArrayList<String>();
		grantList.add("GRANT ROLE \"ROLE_PRIVI\" TO GROUP \"GROUP2_PRIVI\"");
		sqlEditorService.runSQLinJDBC(grantList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Grant roles to Group DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Revoke roles 'ROLE_PRIVI' from group 'GROUP2_PRIVI' function 
	 */
	@Test(description="Test Revoke roles to group function",dependsOnMethods = {"testGrantRoleToGroupExecuteDDL"})
	public void testRevokeRoleToGroup(){
		
		logger.info("Running Revoke roles to Group ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getRevokeRoleToGroup", "getRevokeRoleToGroup_result");
		
		logger.info("PASSED: Revoke roles to Group test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Revoke roles 'ROLE_PRIVI' from group 'USER2_PRIVI' DDL function 
	 */
	@Test(description="Test Revoke roles to group DDL function",dependsOnMethods = {"testRevokeRoleToGroup"})
	public void testRevokeRoleToGroupExecuteDDL(){
		
		logger.info("Running Revoke roles to Group DDL ...");
		
		List<String> revokeList = new ArrayList<String>();
		revokeList.add("REVOKE ROLE \"ROLE_PRIVI\" FROM GROUP \"GROUP2_PRIVI\"");
		sqlEditorService.runSQLinJDBC(revokeList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Revoke roles to Group DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Grant roles 'ROLE_PRIVI' to from 'GROUP2_PRIVI' with Admin Options function 
	 */
	@Test(description="Test Grant roles to group with Admin Options function",dependsOnMethods = {"testRevokeRoleToGroupExecuteDDL"})
	public void testGrantRoleToGroupWithAdminOptions(){
		
		logger.info("Running Grant roles to Group with Admin Options ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getGrantRoleToGroupWithAdminOptions", "getGrantRoleToGroupWithAdminOptions_result");
		
		logger.info("PASSED: Grant roles to Group With Admin Options test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Grant roles to Group with Admin Options DDL function 
	 */
	@Test(description="Test Grant roles to Group with Admin Options DDL function",dependsOnMethods = {"testGrantRoleToGroupWithAdminOptions"})
	public void testGrantRoleToGroupWithAdminOptionsExecuteDDL(){
		
		logger.info("Running Grant roles to Group with Admin Options DDL ...");
		
		List<String> grantList = new ArrayList<String>();
		grantList.add("GRANT ROLE \"ROLE_PRIVI\" TO GROUP \"GROUP2_PRIVI\" WITH ADMIN OPTION");
		sqlEditorService.runSQLinJDBC(grantList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Grant roles to Group With Admin Options DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Revoke roles 'ROLE_PRIVI' from group 'GROUP2_PRIVI' with Admin Options function 
	 */
	@Test(description="Test Revoke roles to group with Admin Options function",dependsOnMethods = {"testGrantRoleToGroupWithAdminOptionsExecuteDDL"})
	public void testRevokeRoleToGroupWithAdminOptions(){
		
		logger.info("Running Revoke roles to Group with Admin Options ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getRevokeRoleToGroupWithAdminOptions", "getRevokeRoleToGroupWithAdminOptions_result");
		
		logger.info("PASSED: Revoke roles to Group With Admin Options test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Revoke roles to Group with Admin Options DDL function 
	 */
	@Test(description="Test Revoke roles to Group with Admin Options DDL function",dependsOnMethods = {"testRevokeRoleToGroupWithAdminOptions"})
	public void testRevokeRoleToGroupWithAdminOptionsExecuteDDL(){
		
		logger.info("Running Revoke roles to Group with Admin Options DDL ...");
		
		List<String> revokeList = new ArrayList<String>();
		revokeList.add("REVOKE ROLE \"ROLE_PRIVI\" FROM GROUP \"GROUP2_PRIVI\";");
		revokeList.add("GRANT ROLE \"ROLE_PRIVI\" TO GROUP \"GROUP2_PRIVI\"");
		sqlEditorService.runSQLinJDBC(revokeList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Revoke roles to Group With Admin Options DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Grant objects privileges to group 'GROUP2_PRIVI' function 
	 */
	@Test(description="Test Grant objects privileges to group function",dependsOnMethods = {"testRevokeRoleToGroupWithAdminOptionsExecuteDDL"})
	public void testGrantObjectsPrivilegesToGroup(){
		
		logger.info("Running Grant Object Privileges to Group ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getGrantObjectPrivilegesToGroup", "getGrantObjectPrivilegesToGroup_result");
		
		logger.info("PASSED: Grant object privileges to Group test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Grant objects privileges to group 'GROUP2_PRIVI' DDL function 
	 */
	@Test(description="Test Grant objects privileges to Group DDL function",dependsOnMethods = {"testGrantObjectsPrivilegesToGroup"})
	public void testGrantObjectsPrivilegesToGroupExecuteDDL(){
		
		logger.info("Running Grant Object Privileges to Group DDL ...");
		
		List<String> grantList = new ArrayList<String>();
		grantList.add("GRANT CONTROL ON INDEX \"SCHEMA_PRIVI\".\"INDEX_PRIVI\" TO GROUP \"GROUP2_PRIVI\"");
		sqlEditorService.runSQLinJDBC(grantList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Grant object privileges to Group DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Revoke objects privileges to group 'GROUP2_PRIVI' function 
	 */
	@Test(description="Test Revoke objects privileges to group function",dependsOnMethods = {"testGrantObjectsPrivilegesToGroupExecuteDDL"})
	public void testRevokeObjectsPrivilegesToGroup(){
		
		logger.info("Running Revoke Object Privileges to Group ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getRevokeObjectPrivilegesToGroup", "getRevokeObjectPrivilegesToGroup_result");
		
		logger.info("PASSED: Grant Revoke privileges to Group test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Revoke objects privileges to group 'GROUP2_PRIVI' DDL function 
	 */
	@Test(description="Test Revoke objects privileges to group DDL function",dependsOnMethods = {"testRevokeObjectsPrivilegesToGroup"})
	public void testRevokeObjectsPrivilegesToGroupExecuteDDL(){
		
		logger.info("Running Revoke Object Privileges to Group DDL ...");
		
		List<String> revokeList = new ArrayList<String>();
		revokeList.add("REVOKE CONTROL ON INDEX \"SCHEMA_PRIVI\".\"INDEX_PRIVI\" FROM GROUP \"GROUP2_PRIVI\"");
		sqlEditorService.runSQLinJDBC(revokeList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Revoke object privileges to Group DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Grant objects privileges to Group 'GROUP2_PRIVI' with grant options function 
	 */
	@Test(description="Test Grant objects privileges to Group with grant options function",dependsOnMethods = {"testRevokeObjectsPrivilegesToGroupExecuteDDL"})
	public void testGrantObjectsPrivilegesToGroupWithGrantOptions(){
		
		logger.info("Running Grant Object Privileges to Group with grant options ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getGrantObjectPrivilegesToGroupWithGrantOptions", "getGrantObjectPrivilegesToGroupWithGrantOptions_result");
		
		logger.info("PASSED: Grant object privileges to Group test with grant options executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Grant objects privileges to group 'GROUP2_PRIVI' with grant options DDL function 
	 */
	@Test(description="Test Grant objects privileges to group with grant options DDL function",dependsOnMethods = {"testGrantObjectsPrivilegesToGroupWithGrantOptions"})
	public void testGrantObjectsPrivilegesToGroupWithGrantOptionsExecuteDDL(){
		
		logger.info("Running Grant Object Privileges to Group with grant options DDL ...");
		
		List<String> grantList = new ArrayList<String>();
		grantList.add("GRANT USE OF TABLESPACE \"TABLESPACE_PRIVI\" TO GROUP \"GROUP2_PRIVI\" WITH GRANT OPTION");
		sqlEditorService.runSQLinJDBC(grantList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Grant object privileges to Group with grant options DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Revoke objects privileges to group 'GROUP2_PRIVI' with grant options function 
	 */
	@Test(description="Test Revoke objects privileges to group with grant options function",dependsOnMethods = {"testGrantObjectsPrivilegesToGroupWithGrantOptionsExecuteDDL"})
	public void testRevokeObjectsPrivilegesToGroupWithGrantOptions(){
		
		logger.info("Running Revoke Object Privileges to Group with grant options ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getRevokeObjectPrivilegesToGroupWithGrantOptions", "getRevokeObjectPrivilegesToGroupWithGrantOptions_result");
		
		logger.info("PASSED: Revoke object privileges to Group test with grant options executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Revoke objects privileges to group 'GROUP2_PRIVI' with grant options DDL function 
	 */
	@Test(description="Test Revoke objects privileges to group with grant options DDL function",dependsOnMethods = {"testRevokeObjectsPrivilegesToGroupWithGrantOptions"})
	public void testRevokeObjectsPrivilegesToGroupWithGrantOptionsExecuteDDL(){
		
		logger.info("Running Revoke Object Privileges to Group with grant options DDL ...");
		
		List<String> revokeList = new ArrayList<String>();
		revokeList.add("REVOKE USE OF TABLESPACE \"TABLESPACE_PRIVI\" FROM GROUP \"GROUP2_PRIVI\";");
		revokeList.add("GRANT USE OF TABLESPACE \"TABLESPACE_PRIVI\" TO GROUP \"GROUP2_PRIVI\"");
		sqlEditorService.runSQLinJDBC(revokeList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Revoke object privileges to Group with grant options DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Remove Group 'GROUP2_PRIVI' function 
	 */
	@Test(description="Test Remove Group function",dependsOnMethods = {"testRevokeObjectsPrivilegesToGroupWithGrantOptionsExecuteDDL"})
	public void testRemoveGroup(){
		
		logger.info("Running Remove Group ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_REMOVE_COMMAND_LUW,"getRemoveGroup", "getRemoveGroup_result");
		
		logger.info("PASSED: Remove Group test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Remove Group 'GROUP2_PRIVI' DDL function 
	 */
	@Test(description="Test Remove Group DDL function",dependsOnMethods = {"testRemoveGroup"})
	public void testRemoveGroupExecuteDDL(){
		
		logger.info("Running Remove Group DDL ...");
		
		List<String> dropList = new ArrayList<String>();
		dropList.add("REVOKE USE OF TABLESPACE \"TABLESPACE_PRIVI\" FROM GROUP \"GROUP2_PRIVI\";");
		dropList.add("REVOKE ROLE \"ROLE_PRIVI\" FROM GROUP \"GROUP2_PRIVI\";");
		dropList.add("REVOKE CONNECT ON DATABASE FROM GROUP \"GROUP2_PRIVI\"");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Remove Group DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Administer Privileges Add Role function, add a role 'ROLE2_PRIVI'
	 */
	@Test(description="Test Administer Privileges Add Role function")
	public void testAddRole(){
		
		logger.info("Running Administer Privileges Add Role ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getAddRole", "getAddRole_result");
		
		logger.info("PASSED: Administer Privileges Add Role test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test execute Administer Privileges Add Role DDL function 
	 */
	@Test(description="Test execute Administer Privileges Add Role DDL function",dependsOnMethods = {"testAddRole"})
	public void testAddRoleExecuteDDL(){
		
		logger.info("Running Execute Add Role DDL ...");
		
		sqlEditorService.runSQLinJDBC("CREATE ROLE \"ROLE2_PRIVI\"", ";", this.dbProfile,true) ;
		
		logger.info("PASSED: Execute Add Role DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Administer Privileges Grant roles to role function
	 */
	@Test(description="Test Administer Privileges Grant roles to role function",dependsOnMethods = {"testAddRoleExecuteDDL"})
	public void testGrantRoleToRole(){
		
		logger.info("Running Administer Privileges Grant roles to role ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getGrantRoleToRole", "getGrantRoleToRole_result");
		
		logger.info("PASSED: Administer Privileges Grant roles to role test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Administer Privileges Grant roles to role DDL function
	 */
	@Test(description="Test Administer Privileges Grant roles to role DDL function",dependsOnMethods = {"testGrantRoleToRole"})
	public void testGrantRoleToRoleExecuteDDL(){
		
		logger.info("Running Administer Privileges Grant roles to role DDL ...");
		
		sqlEditorService.runSQLinJDBC("GRANT ROLE \"ROLE_PRIVI\" TO ROLE \"ROLE2_PRIVI\"", ";", this.dbProfile,true) ;
		
		logger.info("PASSED: Administer Privileges Grant roles to role DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Administer Privileges Revoke roles to role function - To Do function
	 */
	/*@Test(description="Test Administer Privileges Revoke roles to role function",dependsOnMethods = {"testGrantRoleToRoleExecuteDDL"})
	public void testRevokeRoleToRole(){
		
		logger.info("Running Administer Privileges Revoke roles from role ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getRevokeRoleToRole", "getRevokeRoleToRole_result");
		
		logger.info("PASSED: Administer Privileges Revoke roles from role test executed successfully");
	}*/
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Administer Privileges Revoke roles to role DDL function - To Do function
	 */
	/*@Test(description="Test Administer Privileges Revoke roles to role DDL function",dependsOnMethods = {"testRevokeRoleToRole"})
	public void testRevokeRoleToRoleExecuteDDL(){
		
		logger.info("Running Administer Privileges Revoke roles from role DDL ...");
		
		sqlEditorService.runSQLinJDBC("", ";", this.dbProfile,true) ;
		
		logger.info("PASSED: Administer Privileges Revoke roles from role DDL test executed successfully");
	}*/
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Administer Privileges Grant roles to role with admin options function
	 */
	@Test(description="Test Administer Privileges Grant roles to role with admin options function",dependsOnMethods = {"testGrantRoleToRoleExecuteDDL"})
	public void testGrantRoleToRoleWithAdminOptions(){
		
		logger.info("Running Administer Privileges Grant roles to role with admin options ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getGrantRoleToRoleWithAdminOptions", "getGrantRoleToRoleWithAdminOptions_result");
		
		logger.info("PASSED: Administer Privileges Grant roles to role with admin options test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Administer Privileges Grant roles to role with admin options DDL function
	 */
	@Test(description="Test Administer Privileges Grant roles to role with admin options DDL function",dependsOnMethods = {"testGrantRoleToRoleWithAdminOptions"})
	public void testGrantRoleToRoleWithAdminOptionsExecuteDDL(){
		
		logger.info("Running Administer Privileges Grant roles to role with admin options DDL ...");
		
		List<String> grantList = new ArrayList<String>();
		grantList.add("REVOKE ROLE \"ROLE_PRIVI\" FROM ROLE \"ROLE2_PRIVI\";");
		grantList.add("GRANT ROLE \"ROLE_PRIVI\" TO ROLE \"ROLE2_PRIVI\" WITH ADMIN OPTION");
		
		logger.info("PASSED: Administer Privileges Grant roles to role with admin options DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Administer Privileges Revoke roles to role with admin options function - To Do function
	 */
	/*@Test(description="Test Administer Privileges Revoke roles to role with admin options function",dependsOnMethods = {"testGrantRoleToRoleWithAdminOptionsExecuteDDL"})
	public void testRevokeRoleToRoleWithAdminOptions(){
		
		logger.info("Running Administer Privileges Revoke roles from role ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getRevokeRoleToRoleWithAdminOptions", "getRevokeRoleToRoleWithAdminOptions_result");
		
		logger.info("PASSED: Administer Privileges Revoke roles from role with admin options test executed successfully");
	}*/
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Administer Privileges Revoke roles to role with admin options DDL function - To Do function
	 */
	/*@Test(description="Test Administer Privileges Revoke roles to role with admin options DDL function",dependsOnMethods = {"testRevokeRoleToRoleWithAdminOptions"})
	public void testRevokeRoleToRoleWithAdminOptionExecuteDDL(){
		
		logger.info("Running Administer Privileges Revoke roles from role with admin options DDL ...");
		
		sqlEditorService.runSQLinJDBC("", ";", this.dbProfile,true) ;
		
		logger.info("PASSED: Administer Privileges Revoke roles from role with admin options DDL test executed successfully");
	}*/
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Grant objects privileges to role 'ROLE2_PRIVI' function 
	 */
	@Test(description="Test Grant objects privileges to role function",dependsOnMethods = {"testGrantRoleToRoleWithAdminOptionsExecuteDDL"})
	public void testGrantObjectsPrivilegesToRole(){
		
		logger.info("Running Grant Object Privileges to Role ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getGrantObjectPrivilegesToRole", "getGrantObjectPrivilegesToRole_result");
		
		logger.info("PASSED: Grant object privileges to Role test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Grant objects privileges to role 'ROLE2_PRIVI' DDL function 
	 */
	@Test(description="Test Grant objects privileges to Role DDL function",dependsOnMethods = {"testGrantObjectsPrivilegesToRole"})
	public void testGrantObjectsPrivilegesToRoleExecuteDDL(){
		
		logger.info("Running Grant Object Privileges to Role DDL ...");
		
		List<String> grantList = new ArrayList<String>();
		grantList.add("GRANT INSERT ON TABLE \"SCHEMA_PRIVI\".\"VIEW_PRIVI\" TO ROLE \"ROLE2_PRIVI\"");
		sqlEditorService.runSQLinJDBC(grantList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Grant object privileges to Role DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Revoke objects privileges to role 'ROLE2_PRIVI' function 
	 */
	@Test(description="Test Revoke objects privileges to role function",dependsOnMethods = {"testGrantObjectsPrivilegesToRoleExecuteDDL"})
	public void testRevokeObjectsPrivilegesToRole(){
		
		logger.info("Running Revoke Object Privileges to Role ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getRevokeObjectPrivilegesToRole", "getRevokeObjectPrivilegesToRole_result");
		
		logger.info("PASSED: Grant Revoke privileges to Role test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Revoke objects privileges to role 'ROLE2_PRIVI' DDL function 
	 */
	@Test(description="Test Revoke objects privileges to role DDL function",dependsOnMethods = {"testRevokeObjectsPrivilegesToRole"})
	public void testRevokeObjectsPrivilegesToRoleExecuteDDL(){
		
		logger.info("Running Revoke Object Privileges to Role DDL ...");
		
		List<String> revokeList = new ArrayList<String>();
		revokeList.add("REVOKE INSERT ON TABLE \"SCHEMA_PRIVI\".\"VIEW_PRIVI\" FROM ROLE \"ROLE2_PRIVI\"");
		sqlEditorService.runSQLinJDBC(revokeList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Revoke object privileges to Role DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Grant objects privileges to role 'ROLE2_PRIVI' with grant options function 
	 */
	@Test(description="Test Grant objects privileges to Role with grant options function",dependsOnMethods = {"testRevokeObjectsPrivilegesToRoleExecuteDDL"})
	public void testGrantObjectsPrivilegesToRoleWithGrantOptions(){
		
		logger.info("Running Grant Object Privileges to Role with grant options ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getGrantObjectPrivilegesToRoleWithGrantOptions", "getGrantObjectPrivilegesToRoleWithGrantOptions_result");
		
		logger.info("PASSED: Grant object privileges to Role test with grant options executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Grant objects privileges to role 'ROLE2_PRIVI' with grant options DDL function 
	 */
	@Test(description="Test Grant objects privileges to Role with grant options DDL function",dependsOnMethods = {"testGrantObjectsPrivilegesToRoleWithGrantOptions"})
	public void testGrantObjectsPrivilegesToRoleWithGrantOptionsExecuteDDL(){
		
		logger.info("Running Grant Object Privileges to Role with grant options DDL ...");
		
		List<String> grantList = new ArrayList<String>();
		grantList.add("GRANT ALTER ON TABLE \"SCHEMA_PRIVI\".\"TABLE_PRIVI\" TO ROLE \"ROLE2_PRIVI\" WITH GRANT OPTION");
		sqlEditorService.runSQLinJDBC(grantList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Grant object privileges to Role with grant options DDL test executed successfully");
	}

	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Revoke objects privileges to role 'ROLE2_PRIVI' with grant options function 
	 */
	@Test(description="Test Revoke objects privileges to Role with grant options function",dependsOnMethods = {"testGrantObjectsPrivilegesToRoleWithGrantOptionsExecuteDDL"})
	public void testRevokeObjectsPrivilegesToRoleWithGrantOptions(){
		
		logger.info("Running Revoke Object Privileges to Role with grant options ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_MODEL_COMMAND_LUW,"getRevokeObjectPrivilegesToRoleWithGrantOptions", "getRevokeObjectPrivilegesToRoleWithGrantOptions_result");
		
		logger.info("PASSED: Revoke object privileges to Role test with grant options executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Revoke objects privileges to role 'ROLE2_PRIVI' with grant options DDL function 
	 */
	@Test(description="Test Revoke objects privileges to role with grant options DDL function",dependsOnMethods = {"testRevokeObjectsPrivilegesToRoleWithGrantOptions"})
	public void testRevokeObjectsPrivilegesToRoleWithGrantOptionsExecuteDDL(){
		
		logger.info("Running Revoke Object Privileges to Role with grant options DDL ...");
		
		List<String> revokeList = new ArrayList<String>();
		revokeList.add("REVOKE ALTER ON TABLE \"SCHEMA_PRIVI\".\"TABLE_PRIVI\" FROM ROLE \"ROLE2_PRIVI\";");
		revokeList.add("GRANT ALTER ON TABLE \"SCHEMA_PRIVI\".\"TABLE_PRIVI\" TO ROLE \"ROLE2_PRIVI\"");
		sqlEditorService.runSQLinJDBC(revokeList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Revoke object privileges to Role with grant options DDL test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Remove role 'ROLE2_PRIVI' function 
	 */
	@Test(description="Test Remove Role function",dependsOnMethods = {"testRevokeObjectsPrivilegesToRoleWithGrantOptionsExecuteDDL"})
	public void testRemoveRole(){
		
		logger.info("Running Remove Role ...");
		
		executeTest(AdminPrivilegesService.PRIVILEGE_REMOVE_COMMAND_LUW,"getRemoveRole", "getRemoveRole_result");
		
		logger.info("PASSED: Remove Role test executed successfully");
	}
	
	/**
	 * @author hongcbj@cn.ibm.com
	 * Test Remove role 'ROLE2_PRIVI' DDL function 
	 */
	@Test(description="Test Remove Role DDL function",dependsOnMethods = {"testRemoveRole"})
	public void testRemoveRoleExecuteDDL(){
		
		logger.info("Running Remove Role DDL ...");
		
		List<String> dropList = new ArrayList<String>();
		dropList.add("REVOKE ALTER ON TABLE \"SCHEMA_PRIVI\".\"TABLE_PRIVI\" FROM ROLE \"ROLE2_PRIVI\";");
		dropList.add("REVOKE ROLE \"ROLE_PRIVI\" FROM ROLE \"ROLE2_PRIVI\";");
		dropList.add("DROP ROLE \"ROLE2_PRIVI\"");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, true) ;
		
		logger.info("PASSED: Remove Role DDL test executed successfully");
	}
	
	/**
	 * @author yhjyang@cn.ibm.com
	 * Test get object privileges API
	 */
	@Test(description="Test get object privileges")
	public void testGetObjectPrivileges(){
		/**
		 * {"authIds":["DB2INST1:U","USER_PRIVI:U"],"roles":["SYSTS_ADM:R","SYSTS_USR:R","SYSDEBUG:R","SYSDEBUGPRIVATE:R","ROLE_PRIVI:R","SYSTS_MGR:R"],"groups":["GROUP_PRIVI:G","PUBLIC:G"],"items":[]}
		 */
		logger.info("Running get object privileges ...");
		String query = privilegesService.getJSONData( "getObjectPrivilegesList" );
		JSONObject resObj =
				privilegesService.callPrivilegesService( AdminPrivilegesService.PRIVILEGE_GET_OBJECT_LIST_LUW, query, dbProfile );
		
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );
		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		
		JSONObject propResult = JsonPath.read( responseData, "$" );
		Assert.assertTrue( propResult.containsKey("authIds") );
		Assert.assertTrue( propResult.containsKey("roles") );
		Assert.assertTrue( propResult.containsKey("groups") );
		Assert.assertTrue( propResult.containsKey("items") );
		logger.info("PASSED: get object privileges test executed successfully");
	}
	
	/**
	 * @author yhjyang@cn.ibm.com
	 * Test get DB auth object privileges API
	 */
	@Test(description="Test get DB auth object privileges")
	public void testGetDBAuthObjectPrivileges(){
	
		logger.info("Running get DB auth  object privileges ...");
		String query = privilegesService.getJSONData( "getDBObjectPrivilegesList" );
		JSONObject resObj =
				privilegesService.callPrivilegesService( AdminPrivilegesService.PRIVILEGE_GET_OBJECT_LIST_LUW, query, dbProfile );
		
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );
		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		
		JSONObject propResult = JsonPath.read( responseData, "$" );
		JSONArray items = (JSONArray) responseData.get("items");
		Assert.assertTrue( propResult.containsKey("items") );
		Assert.assertTrue( items.size() > 0 );
		logger.info("PASSED: get DB auth privileges test executed successfully");
	}
	
	/**
	 * @author yhjyang@cn.ibm.com
	 * Test get DB auth object privileges API
	 */
	@Test(description="Test get DB Auth list")
	public void testGetDBAuthList(){
	
		logger.info("Running get DB Auth list ...");
		String query = privilegesService.getJSONData( "getDBAuthList" );
		JSONObject resObj =
				privilegesService.callPrivilegesService( AdminPrivilegesService.PRIVILEGE_MODEL_DEFINITION_LUW, query, dbProfile );
		
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );
		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		
		JSONObject propResult = JsonPath.read( responseData, "$" );
		JSONArray authIds = (JSONArray) responseData.get("authIds");
		Assert.assertTrue( propResult.containsKey("authIds") );
		Assert.assertTrue( authIds.size() > 0 );
		logger.info("PASSED: get DB Auth list test executed successfully");
	}
}
