package com.ibm.datatools.ots.tests.restapi.admin;

import java.util.List;

import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;

import com.ibm.datatools.ots.tests.restapi.common.AdminSQLEditorService;
import com.jayway.jsonpath.JsonPath;

public class AdminSQLEditorTest extends AdminTestBase
{
	protected static final Logger logger = LogManager.getLogger( AdminSQLEditorTest.class );
	public static final String TYPE_JDBC = "JDBC" ;
	public static final String TYPE_CLP = "CLPSSH" ;
	protected AdminSQLEditorService sqlEditorService = null;

	@BeforeClass
	public void beforeTest()
	{
		sqlEditorService = new AdminSQLEditorService();
	}
	
	/**
	 * Run the given SQL in sqleditor
	 * @type JDBC, CLPSSH 
	 * */
	public JSONObject[] runSQLinJDBC(String sqlStatement,String terminator){
		return sqlEditorService.runSQLinJDBC(sqlStatement, terminator, this.dbProfile) ;
	}
	
	public JSONObject[] runSQLinReusedJDBC(String sqlStatement,String terminator){
		return sqlEditorService.runSQLinJDBC(true, sqlStatement, terminator, this.dbProfile) ;
	}

	public JSONObject[] runSQLinJDBC(String sqlStatement,String terminator,String profileName){
		return sqlEditorService.runSQLinJDBC(sqlStatement, terminator, profileName) ;
	}
	
	public JSONObject[] runSQLinJDBC(List<String> sqlStatement,String terminator){
		return sqlEditorService.runSQLinJDBC(sqlStatement, terminator, this.dbProfile) ;
	}
	
	public void runSQLinJDBC(String sqlStatement,String terminator,String profileName,boolean validate){
		 sqlEditorService.runSQLinJDBC(sqlStatement, terminator, profileName,validate) ;
	}
	
	public void runSQLinJDBC( List<String> sqlStatement, String terminator, boolean validate )
	{
		sqlEditorService.runSQLinJDBC( sqlStatement, terminator, this.dbProfile, validate );
	}
	
	public void runSQLinJDBC( String sqlStatement, String terminator, boolean validate )
	{
		sqlEditorService.runSQLinJDBC( sqlStatement, terminator, this.dbProfile, validate );
	}
	/**
	 * Run the given SQL in sqleditor
	 * @type JDBC, CLPSSH 
	 * */
	public JSONObject[] runSQLinCLP(String sqlStatement,String terminator,boolean addConnectTo){
		return sqlEditorService.runSQLinCLP(sqlStatement, terminator, addConnectTo, this.dbProfile);
	}
	
	
	/**
	 * This method provide the function to run a sql statement agaqinst SQL Editor, the sql statement is wrapped in 
	 * JSONObject and stored in getAdminSQLEditor.properties use the key in param as reference key
	 * 
	 * */
	public JSONObject[] runSQLinSQLEditor(String key){
		return sqlEditorService.runSQLinSQLEditor(key, this.dbProfile);
	}
	/**
	 * Assert the response code equals to 200
	 * */
	public void assertResponseCode(JSONObject responseObj){
		this.sqlEditorService.assertResponseCode(responseObj);
	}
	
	protected void executeTest(String property, String baseProperty){
		String query = sqlEditorService.getJSONData( property );
		JSONObject resObj = sqlEditorService.callSQLEditorService( AdminSQLEditorService.SCRIPTS, query, dbProfile );
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );
		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		
		JSONObject propResult = JsonPath.read( responseData, "$" );
		String base = sqlEditorService.getJSONData( baseProperty );
		JSONObject propBase = JSONObject.fromObject( base );
		Assert.assertTrue( compareJsons( propBase, propResult ) );
	}
	
}
