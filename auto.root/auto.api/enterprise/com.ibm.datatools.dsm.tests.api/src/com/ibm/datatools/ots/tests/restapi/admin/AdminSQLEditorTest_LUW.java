package com.ibm.datatools.ots.tests.restapi.admin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.AdminSQLEditorService;
import com.ibm.datatools.test.utils.AdminConst;
import com.jayway.jsonpath.JsonPath;

public class AdminSQLEditorTest_LUW extends AdminSQLEditorTest {

	@BeforeClass
	public void beforeTest() {
		sqlEditorService = new AdminSQLEditorService();
		//This is used to provide precondition for method testFormatSQL(), testSaveReplaceDeleteScript(), testViewDataInSQLResultView()
		//skip the unused pre-condition
		if(!this.isBVT){
			testCreateTable();
		}
	}
	
	@AfterClass
	public void afterTest() {
		if(!this.isBVT){
			testDropTable();
		}

	}
	
	public void testCreateTable() {
		List<String> dropList = new ArrayList<String>();
		dropList.add("DROP TABLE SCH_SQLEDITOR.TAB_SQLEDITOR;");
		dropList.add("DROP SCHEMA SCH_SQLEDITOR RESTRICT;");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);
		
		List<String> createList = new ArrayList<String>();
		createList.add("CREATE SCHEMA SCH_SQLEDITOR;");
		createList.add("CREATE TABLE SCH_SQLEDITOR.TAB_SQLEDITOR(COL1 INT);");
		createList.add("INSERT INTO SCH_SQLEDITOR.TAB_SQLEDITOR VALUES(1),(2),(3);");
		sqlEditorService.runSQLinJDBC(createList, ";",this.dbProfile, true);
		
	}

	public void testDropTable() {
		List<String> dropList = new ArrayList<String>();
		dropList.add("DROP TABLE SCH_SQLEDITOR.TAB_SQLEDITOR;");
		dropList.add("DROP SCHEMA SCH_SQLEDITOR RESTRICT;");
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, true);
		
	}
	
	/**
	 * Test to delete migration "script1" and "script2" to clear environent for regression
	 * */
	@Test (description = "Test to save script", priority = 1)
	public void testDeleteMigrationScript() throws InterruptedException
	{
		logger.info( "get the script list from Saved Queries table ..." );
		//Assert the "Script1 and Script2" are included in script list
		boolean contained = scriptsContained("Script_GetFavorites", "Script_GetFavoritesList_Result", "assertContained");
		if (contained) {
			//Delete the "Script1 and Script2"
			logger.info( "Delete the 'Script1' and Script2'" );
			executeTest("Script_Delete", "Script_Delete_Result");
		}
	}
	
	/**
	 * Sample for Run single sql in sql editor
	 * key=RunSQLTest_SINGLE   defined in getAdminSQLEditor.properties
	 * The value of the key is a JSONObject which exactly same as the one in http request
	 * 
	 * */
	@Test (description = "Test Run single SQL in Editor using JDBC")
	public void testRunSingleSQLInJDBC(){
		String key = "RunSQLTest_SINGLE";
		@SuppressWarnings("unused")
		JSONObject[] result = this.runSQLinSQLEditor(key);
		//If you want have specific verification for the returned data, you can add it here
		//Otherwise, the parent already verified the response code to 200.
	}
	/**
	 * Sample for Run single sql in sql editor
	 * key=RunSQLTest_SINGLE   defined in getAdminSQLEditor.properties
	 * The value of the key is a JSONObject which exactly same as the one in http request
	 * 
	 * */
	@Test (description = "Test Run Multiple SQL in Editor using JDBC")
	public void testRunMultipleSQLInJDBC(){
		String key = "RunSQLTest_MULTIPLE";
		JSONObject[] result = this.runSQLinSQLEditor(key);
		//Verify the result of each statement
		for(int i=0;i<result.length;i++){
			//If you want have specific verification for the returned data, you can add it here
			//Otherwise, the parent already verified the response code to 200.
		}
	}
	/**
	 * Sample for Run single sql in sql editor
	 * key=RunSQLTest_SINGLE   defined in getAdminSQLEditor.properties
	 * The value of the key is a JSONObject which exactly same as the one in http request
	 * 
	 * */
	@Test (description = "Test Run SQL in Editor using CLP")
	public void testRunSQLInCLP(){
		String key = "RunSQLTest_SSH";
		@SuppressWarnings("unused")
		JSONObject[] result = this.runSQLinSQLEditor(key);
		//If you want have specific verification for the returned data, you can add it here
		//Otherwise, the parent already verified the response code to 200.
	}
	@Test (description = "Test hand-writing Run SQL in Editor using JDBC")
	public void testRunSQLInJDBC2(){
		String sql = "select 1 from sysibm.systables;select 2 from sysibm.systables";
		@SuppressWarnings("unused")
		JSONObject[] result = this.runSQLinJDBC(sql, ";");
		//If you want have specific verification for the returned data, you can add it here
		//Otherwise, the parent already verified the response code to 200.
	}
	
	@Test (description = "Test hand-writing Run SQL in Editor using JDBC")
	public void testRunSQLInReusedJDBC(){
		String sql = "set current schema 'SYSIBM'";
		JSONObject[] result1 = this.runSQLinReusedJDBC(sql, ";");
		sql = "select 1 from systables";
		JSONObject[] result = this.runSQLinReusedJDBC(sql, ";");
		JSONObject obj = result[0];
		Assert.assertNotNull(obj.getJSONArray("data"));
	}
	
	@Test (description = "Test hand-writing Run SQL in Editor using CLP")
	public void testRunSQLInCLP2(){
		@SuppressWarnings("unused")
		JSONObject[] result = this.runSQLinCLP("get dbm cfg;!db2ilist;", ";", true);
		//If you want have specific verification for the returned data, you can add it here
		//Otherwise, the parent already verified the response code to 200.
	}
	
	
	//This is the test for beforeTest Sample
	//***********************^^^^^^^^^^^^^^^^^^^^^^^^^^^^^About Are test for reference^^^^^^^^^^^^^^^^^****************************
	
	
	
	/**
	 * Stored procedure test, test run SP with out put params
	 * @author litaocdl
	 * */
	@Test (description = "Test running stored procedure against sql editor")
	public void testRunStoredProcedure(){
		logger.info( "Runing stored procedure test..." );
		
		
		this.runSQLinJDBC("call sysproc.get_dbsize_info(?, ?, ?, -1)",";",true) ;

		
	}
	
	/**
	 * Stored procedure test, test run SP with no params
	 * @author litaocdl
	 * */
	@Test (description = "Test running stored procedure against sql editor")
	public void testRunStoredProcedure1(){
		logger.info( "Runing stored procedure test..." );
		
		this.runSQLinJDBC("create table DSMTAO.ACT (a int, b int)",";",false) ;
		this.runSQLinJDBC("CALL SYSPROC.ADMIN_CMD( ( SELECT 'RUNSTATS ON TABLE DSMTAO.ACT AND INDEXES ALL SET PROFILE'  FROM SYSIBM.SYSDUMMY1 ) );",";",true) ;
		this.runSQLinJDBC("drop table DSMTAO.ACT",";",false) ;	
		
		
		
	}
	
	/**
	 * Stored procedure test, test run SP with input params
	 * This SP is different in 9.7 and 10.1/10.5
	 * @author litaocdl
	 * */
	@Test (description = "Test running stored procedure against sql editor")
	public void testRunStoredProcedure2(){
		logger.info( "Runing stored procedure test..." );
		this.runSQLinJDBC("create table DSMTAO.ACT (a int, b int)",";",false) ;
		if(AdminConst.DB_VERSION_LUW_97.equalsIgnoreCase(this.version)){
			this.runSQLinJDBC("call ADMIN_REVALIDATE_DB_OBJECTS('TABLE','DSMTAO','ACT')",";",true) ;
			
		}else{
			this.runSQLinJDBC("call ADMIN_REVALIDATE_DB_OBJECTS('TABLE','DSMTAO','ACT','y','n')",";",true) ;
			
		}
		this.runSQLinJDBC("drop table DSMTAO.ACT",";",false) ;	
			
	}
	
	/**
	 * Test to save script
	 * */
	@Test (description = "Test to save script", priority = 1)
	public void testSaveScript() throws InterruptedException
	{
		logger.info( "Save script to the Saved Queries table..." );
		executeTest("Script_Save", "Script_Save_Result");
	}

	/**
	 * Test to add script to favorites
	 * */
	@Test (description = "Test to add script to favorites", priority = 2)
	public void testAddScriptToFavorites() throws InterruptedException
	{
		logger.info( "Add script to favorites, it should be listed in Default list in Favorities list..." );
		executeTest("Script_SUB", "Script_SUB_Result");
	}
	
	/**
	 * Test not to select script to favorites
	 * */
	@Test (description = "Test not to select script to favorites", priority = 3)
	public void testNoSelectScriptToFavorites() throws InterruptedException
	{
		logger.info( "No select favorities attributes for script, it should be removed from Favorities list..." );
		executeTest("Script_DeleteScriptFavorites", "Script_DeleteScriptFavorites_Result");
	}
	
	/**
	 * Test to set the category of script as Templates
	 * */
	@Test (description = "Test to set the category of script as Templates", priority = 4 )
	public void testSetCategoryAsTemp() throws InterruptedException
	{
		
		logger.info( "Add script to Favorites, it should be listed in Default list in Favorities list..." );
		executeTest("Script_SUB", "Script_SUB_Result");
		
		logger.info( "Set the category of script as Templates, it should be listed in Templates list in Favorities list..." );
		executeTest("Script_SetCategoryAsTemp", "Script_SetCategoryAsTemp_Result");
		
	}
	
	/**
	 * Test to rename script
	 * */
	@Test (description = "Test to rename script", priority = 5)
	public void testRenameScript() throws InterruptedException
	{
		
		logger.info( "Rename script name in Saved Queries table, its name should be updated in Templates list in Favorities list..." );
		executeTest("Script_RenameScript", "Script_RenameTempScript_Result");
		
	}
	
	/**
	 * Test to delete script
	 * */
	@Test (description = "Test to delete script", priority = 6)
	public void testDeleteScript() throws InterruptedException
	{
		
		logger.info( "Delete script from Saved Queries table to clean environment..." );
		executeTest("Script_RenameScript_Delete", "Script_RenameScript_Delete_Result");
		
		logger.info( "Check script should be removed from Favorities list... " );
		executeTest("Script_GetFavorites", "Script_GetFavorites_Result");
	}
	
	/**
	 * @author cyinmei@cn.ibm.com
	 * This method is to test Format function in SQL editor
	 * Input SQL statement in SQL editor, then click Format button, and verify the back-end request of formatted SQL
	 * 
	 * */
	@Test (description = "Test format function in sql editor")
	public void testFormatSQL(){
		logger.info( "Runing Format SQL test..." );
		String query = sqlEditorService.getJSONData("getSQLEditorFormat");

		JSONObject resObj = sqlEditorService.callSQLEditorService(AdminSQLEditorService.FORMAT_SQL, query, dbProfile);

		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);

		String formattedResult=JsonPath.read(responseData, "$.sql");
		Assert.assertEquals(formattedResult, "SELECT *\n  FROM SCH_SQLEDITOR.TAB_SQLEDITOR");
		
		logger.info("PASSED: Format SQL executed successfully");
			
	}
	
	
	/**
	 * @author cyinmei@cn.ibm.com
	 * 	 * 
	 * This method is used to test Replace functions in SQL editor
	 * Input SQL statements in SQL editor, then click Save button, and verify the back-end request of saved SQL, 
	 * Then select the saved script, and click Replace to open it
	 * Finally, verify Delete script function to clean env 
	 * 	 * */
	@Test (description = "Test replace SQL against sql editor")
	public void testReplaceScript(){
		logger.info( "Runing replace SQL script test..." );
		String query = sqlEditorService.getJSONData("getSQLEditorSaveScript");

		JSONObject resObj = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, query, dbProfile);

		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);	
		
		/**
		*The responseData is like this: {"owner":"admin","result":true,"name":"Script1"}
		*As owner and name value will be changed, so just verify "result":true to check if script is saved successfully
		***/
		Assert.assertTrue(responseData.toString().contains("\"result\":true"));
		
		/**
		* The following is used to test open an existing script, 
		* In SQL result view, select one script, then click "Replace", then verify back-end request
		* The input of replacing one script is like:  cmd: load, name:["Script1"], owner:["admin"]
		* */
		
		String fileName_RES=JsonPath.read(responseData, "$.name");
		String owner_RES=JsonPath.read(responseData, "$.owner");
		
		JSONArray fileName_RE = new JSONArray();
		JSONArray owner_RE = new JSONArray();
		
		fileName_RE.add(fileName_RES);
		owner_RE.add(owner_RES);
		
		JSONObject resObjReplaceInput = new JSONObject()  
        .element("cmd", "load")     
        .element("name", fileName_RE)  
        .element("owner", owner_RE); 
		String replaceInput=resObjReplaceInput.toString();
		JSONObject resObjReplace = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, replaceInput, dbProfile);
		
		JSONObject responseDataReplace = (JSONObject) resObjReplace.get("ResponseData");
		Assert.assertNotNull(responseDataReplace);
		Assert.assertTrue(responseDataReplace.toString().length() > 0);	
        
		int scriptContent = responseDataReplace.toString().indexOf("\"script\":\"SELECT * FROM SCH_SQLEDITOR.TAB_SQLEDITOR\",");
        Assert.assertNotEquals(scriptContent,-1);
		
				
		/**
		* The following is used to delete the script, because this method may be executed many times, so it needs to clean env
		* Otherwise, it will save many scripts in repository DB
		* The input of deleting one script is like:  cmd: delete, name:["Script11"], owner:["admin"]
		* */
		
		String fileName_DE="[\""+JsonPath.read(responseData, "$.name")+"\"]";
		String owner_DE="[\""+JsonPath.read(responseData, "$.owner")+"\"]";
		
		JSONObject resObjDeleteInput = new JSONObject()  
        .element("cmd", "delete")     
        .element("name", fileName_DE)  
        .element("owner", owner_DE); 
		String deleteInput=resObjDeleteInput.toString();
		JSONObject resObjDelete = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, deleteInput, dbProfile);

		JSONObject responseDataDelete = (JSONObject) resObjDelete.get("ResponseData");
		Assert.assertNotNull(responseDataDelete);
		Assert.assertTrue(responseDataDelete.toString().length() > 0);	
		
		Assert.assertEquals(responseDataDelete.toString(), "{\"result\":true}");
			
		logger.info("PASSED: replace SQL script executed successfully");
			
	}
	
	/**
	 * @author fanfei@cn.ibm.com
	 * 	 * 
	 * This method is used to test insert multiple scripts in SQL editor
	 * Input SQL statements in SQL editor, then click Save button, and verify the back-end request of saved SQL, 
	 * Then select the two saved scripts, and click Insert to insert them to editor panel,and verify the back-end request of insert multiple scripts
	 * Finally, verify Delete script function to clean env 
	 * 	 * */
	@Test (description = "Test insert multiple scripts against sql editor")
	public void testInsertMultipleScripts(){
		logger.info( "Runing insert multiple SQL scripts test..." );
		//Save script "TestScript1"
		String query = sqlEditorService.getJSONData("getSQLEditorSaveScriptWithName1");
		JSONObject resObj = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, query, dbProfile);
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);	
		
		//Save script "TestScript2"
		String query2 = sqlEditorService.getJSONData("getSQLEditorSaveScriptWithName2");
		JSONObject resObj2 = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, query2, dbProfile);
		JSONObject responseData2 = (JSONObject) resObj2.get("ResponseData");
		Assert.assertNotNull(responseData2);
		Assert.assertTrue(responseData2.toString().length() > 0);	
		
		//Insert two scripts
		String queryInsert = sqlEditorService.getJSONData("getSQLEditorInsertOrReplaceMultiScripts");
		JSONObject resObjInsert = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, queryInsert, dbProfile);
		JSONObject responseDataInsert = (JSONObject) resObjInsert.get("ResponseData");
		
		JSONArray itemsContent= responseDataInsert.getJSONArray("items");
		
		System.out.println("*****************responseDataInsert = "+responseDataInsert);
		System.out.println("*****************"+JsonPath.read( (JSONObject) itemsContent.get(0), "$.name" ));
		
		System.out.println("*****************"+JsonPath.read( (JSONObject) itemsContent.get(1), "$.name" ));
		//itemsContent: [{"owner":"admin","shared":true,"name":"TestScript1",..},{"owner":"admin","shared":false,"name":"TestScript2",...}]
		String dataResponseResult = "";
		
		for(int i = 0; i < itemsContent.size(); i++){
			int index = JsonPath.read( (JSONObject) itemsContent.get(i), "$.index" );
			dataResponseResult = JsonPath.read( (JSONObject) itemsContent.get(i), "$.name" );
			if (index == 0){
				Assert.assertEquals(dataResponseResult, "TestScript1");
		     } else if (index == 1){
		    	 Assert.assertEquals(dataResponseResult, "TestScript2");
		     }
		}
		
		//Delete the test scripts
		String fileName_DE="[\"TestScript1\",\"TestScript2\"]";
		String owner_DE="["+JsonPath.read(responseData, "$.owner")+"," + JsonPath.read(responseData2, "$.owner") + "]";
		
		JSONObject resObjDeleteInput = new JSONObject()  
        .element("cmd", "delete")     
        .element("name", fileName_DE)  
        .element("owner", owner_DE); 
		String deleteInput=resObjDeleteInput.toString();
		
		JSONObject resObjDelete = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, deleteInput, dbProfile);
		JSONObject responseDataDelete = (JSONObject) resObjDelete.get("ResponseData");
		Assert.assertNotNull(responseDataDelete);
		Assert.assertTrue(responseDataDelete.toString().length() > 0);	
		Assert.assertEquals(responseDataDelete.toString(), "{\"result\":true}");
			
		logger.info("PASSED: Insert multiple scripts executed successfully");
	}
	
	/**
	 * @author fanfei@cn.ibm.com
	 * 	 * 
	 * This method is used to test replace with multiple scripts in SQL editor
	 * Input SQL statements in SQL editor, then click Save button, and verify the back-end request of saved SQL, 
	 * Then select the two saved scripts, and click replace to replace the panel content with them,and verify the back-end request of replace with multiple scripts
	 * Finally, verify Delete script function to clean env 
	 * 	 * */
	@Test (description = "Test insert multiple scripts against sql editor")
	public void testReplaceWithMultipleScripts(){
		logger.info( "Runing replace with multiple SQL scripts test..." );
		//Save script "TestScript1"
		String querySave1 = sqlEditorService.getJSONData("getSQLEditorSaveScriptWithName1");
		JSONObject resObjSave1 = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, querySave1, dbProfile);
		JSONObject responseDataSave1 = (JSONObject) resObjSave1.get("ResponseData");
		Assert.assertNotNull(responseDataSave1);
		Assert.assertTrue(responseDataSave1.toString().length() > 0);	
		
		//Save script "TestScript2"
		String querySave2 = sqlEditorService.getJSONData("getSQLEditorSaveScriptWithName2");
		JSONObject resObjSave2 = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, querySave2, dbProfile);
		JSONObject responseDataSave2 = (JSONObject) resObjSave2.get("ResponseData");
		Assert.assertNotNull(responseDataSave2);
		Assert.assertTrue(responseDataSave2.toString().length() > 0);	
				
		//Insert script "TestScript1"
		String queryInsert2 = sqlEditorService.getJSONData("getSQLEditorInsertScript");
		JSONObject resObjInsert2 = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, queryInsert2, dbProfile);
		JSONObject responseDataInsert2 = (JSONObject) resObjInsert2.get("ResponseData");
				
		JSONArray itemsContentInsert= responseDataInsert2.getJSONArray("items");
		//Verify itemsContent: [{"owner":"admin","shared":true,"name":"TestScript1",..}]
		String dataResponseResult = JsonPath.read((JSONObject) itemsContentInsert.get(0), "$.name" );
		Assert.assertEquals(dataResponseResult, "TestScript1");
		
		//Replace with the "TestScript1" and "TestScript2"
		String queryReplace = sqlEditorService.getJSONData("getSQLEditorInsertOrReplaceMultiScripts");
		JSONObject resObjReplace = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, queryReplace, dbProfile);
		JSONObject responseDataReplace = (JSONObject) resObjReplace.get("ResponseData");
		
		JSONArray itemsContent= responseDataReplace.getJSONArray("items");
		
		//itemsContent: [{"owner":"admin","shared":true,"name":"TestScript1",..},{"owner":"admin","shared":false,"name":"TestScript2",...}]
		for(int i = 0; i < itemsContent.size(); i++){
			int index = JsonPath.read( (JSONObject) itemsContent.get(i), "$.index" );
			dataResponseResult = JsonPath.read( (JSONObject) itemsContent.get(i), "$.name" );
			if (index == 0){
				Assert.assertEquals(dataResponseResult, "TestScript1");
		     } else if (index == 1){
		    	 Assert.assertEquals(dataResponseResult, "TestScript2");
		     }
		}
		
		//Delete the test scripts
		String fileName_DE="[\"TestScript1\",\"TestScript2\"]";
		String owner_DE="["+JsonPath.read((JSONObject) itemsContent.get(0), "$.owner")+"," + JsonPath.read((JSONObject) itemsContent.get(1), "$.owner") + "]";
		
		JSONObject resObjDeleteInput = new JSONObject()  
        .element("cmd", "delete")     
        .element("name", fileName_DE)  
        .element("owner", owner_DE); 
		String deleteInput=resObjDeleteInput.toString();
		
		JSONObject resObjDelete = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, deleteInput, dbProfile);
		JSONObject responseDataDelete = (JSONObject) resObjDelete.get("ResponseData");
		Assert.assertNotNull(responseDataDelete);
		Assert.assertTrue(responseDataDelete.toString().length() > 0);	
		Assert.assertEquals(responseDataDelete.toString(), "{\"result\":true}");
			
		logger.info("PASSED: Replace multiple scripts executed successfully");
	}
	
	/**
	 * @author fanfei@cn.ibm.com
	 * 	 * 
	 * This method is used to test get the script names that owned by current user from saved scripts tab
	 * Input SQL statements in SQL editor, then click Save button, and verify the back-end request of saved SQL, 
	 * Then send the request to get the script names from saved scripts tab,and verify the back-end request could return the correct scripts name
	 * Finally, verify Delete script function to clean env 
	 * 	 * */
	@Test (description = "Test insert multiple scripts against sql editor")
	public void testGetScriptNamesFromSavedScripts(){
		logger.info( "Runing get script names from saved scripts tab test..." );
		//Save script "TestScript1"
		String querySave1 = sqlEditorService.getJSONData("getSQLEditorSaveScriptWithName1");
		JSONObject resObjSave1 = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, querySave1, dbProfile);
		JSONObject responseDataSave1 = (JSONObject) resObjSave1.get("ResponseData");
		Assert.assertNotNull(responseDataSave1);
		Assert.assertTrue(responseDataSave1.toString().length() > 0);	
		
		//Save script "TestScript2"
		String querySave2 = sqlEditorService.getJSONData("getSQLEditorSaveScriptWithName2");
		JSONObject resObjSave2 = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, querySave2, dbProfile);
		JSONObject responseDataSave2 = (JSONObject) resObjSave2.get("ResponseData");
		Assert.assertNotNull(responseDataSave2);
		Assert.assertTrue(responseDataSave2.toString().length() > 0);	
				
		//Get script names of current user owns
		String queryScripts = sqlEditorService.getJSONData("getScriptNamesFromSavedScript");
		JSONObject resObj = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, queryScripts, dbProfile);
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
				
		JSONArray itemsContent= responseData.getJSONArray("items");
		
		//itemsContent: [{"name":"TestScript1", id: "Script1"},{"name":"TestScript2", id:"Script2"}]
		String dataResponseReplaceResult = "";
		String[] scriptName = new String[itemsContent.size()];
		for(int i = 0; i < itemsContent.size(); i++){
			dataResponseReplaceResult = JsonPath.read( (JSONObject) itemsContent.get(i), "$.name" );
			scriptName[i] = dataResponseReplaceResult;
		}
		
		List<String> tempList = Arrays.asList(scriptName);
		Assert.assertTrue(tempList.contains("TestScript1") && tempList.contains("TestScript2"));
		
		//Delete the test scripts
		String fileName_DE="[\"TestScript1\",\"TestScript2\"]";
		String owner_DE="["+JsonPath.read(responseDataSave1, "$.owner")+"," + JsonPath.read(responseDataSave2, "$.owner") + "]";
		
		JSONObject resObjDeleteInput = new JSONObject()  
        .element("cmd", "delete")     
        .element("name", fileName_DE)  
        .element("owner", owner_DE); 
		String deleteInput=resObjDeleteInput.toString();
		
		JSONObject resObjDelete = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, deleteInput, dbProfile);
		JSONObject responseDataDelete = (JSONObject) resObjDelete.get("ResponseData");
		Assert.assertNotNull(responseDataDelete);
		Assert.assertTrue(responseDataDelete.toString().length() > 0);	
		Assert.assertEquals(responseDataDelete.toString(), "{\"result\":true}");
			
		logger.info("PASSED: Get script names from saved scripts tab executed successfully");
	}
	
	/**
	 * @author fanfei@cn.ibm.com
	 * 	 * 
	 * This method is used to test save existed test case to overwrite
	 * Input SQL statements in SQL editor, then click Save button, and verify the back-end request of saved SQL, 
	 * Then update the SQL statements and save again,and verify the back-end request could return the overwritten scripts
	 * Finally, verify Delete script function to clean env 
	 * 	 * */
	@Test (description = "Test insert multiple scripts against sql editor")
	public void testSaveScriptToOverwrite(){
		logger.info( "Runing save exsited test case to overwrite test..." );
		//Save script "TestScript1"
		String querySave1 = sqlEditorService.getJSONData("getSQLEditorSaveScriptWithName1");
		JSONObject resObjSave1 = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, querySave1, dbProfile);
		JSONObject responseDataSave1 = (JSONObject) resObjSave1.get("ResponseData");
		Assert.assertNotNull(responseDataSave1);
		Assert.assertTrue(responseDataSave1.toString().length() > 0);	
		
		//Check the script name is "TestScript1"
		String dataResponseResult = JsonPath.read(responseDataSave1, "$.name" );
		Assert.assertEquals(dataResponseResult, "TestScript1");
		
		//Check the content of "TestScript1" script
		String queryContent1 = sqlEditorService.getJSONData("getScriptContent");
		JSONObject resObj1 = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, queryContent1, dbProfile);
		JSONObject responseData1 = (JSONObject) resObj1.get("ResponseData");
		JSONArray itemsContent1= responseData1.getJSONArray("items");
		
		boolean flag = false;
		//for upgrade, get the first one
		String dataResponseAbbrResult = JsonPath.read( (JSONObject) itemsContent1.get(0), "$.abbr" );
		if(dataResponseAbbrResult.equals("SELECT * FROM SCH_SQLEDITOR.TAB_SQLEDITOR")){
			flag = true;
		}
		else
		{
			//for new install, get the last one
			dataResponseAbbrResult = JsonPath.read( (JSONObject) itemsContent1.get(itemsContent1.size()-1), "$.abbr" );
			if(dataResponseAbbrResult.equals("SELECT * FROM SCH_SQLEDITOR.TAB_SQLEDITOR"));
				flag = true;
		}
		Assert.assertEquals(flag, true);
		//Assert.assertEquals(dataResponseAbbrResult, "SELECT * FROM SCH_SQLEDITOR.TAB_SQLEDITOR");
		
		//Get script names of current user owns
		String queryScripts = sqlEditorService.getJSONData("getScriptNamesFromSavedScript");
		JSONObject resObj = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, queryScripts, dbProfile);
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		JSONArray itemsContent= responseData.getJSONArray("items");
		//itemsContent should include [{"name":"TestScript1", id: "Script1"}]
		String dataResponseReplaceResult = "";
		String[] scriptName = new String[itemsContent.size()];
		for(int i = 0; i < itemsContent.size(); i++){
			dataResponseReplaceResult = JsonPath.read( (JSONObject) itemsContent.get(i), "$.name" );
			scriptName[i] = dataResponseReplaceResult;
		}
		List<String> tempList = Arrays.asList(scriptName);
		Assert.assertTrue(tempList.contains("TestScript1"));
		
		//Save script "TestScript1" again
		String querySave2 = sqlEditorService.getJSONData("getSQLEditorSaveScriptWithName1_1");
		JSONObject resObjSave2 = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, querySave2, dbProfile);
		JSONObject responseDataSave2 = (JSONObject) resObjSave2.get("ResponseData");
		Assert.assertNotNull(responseDataSave2);
		Assert.assertTrue(responseDataSave2.toString().length() > 0);	
		//Check the script name is still "TestScript1"
		String dataResponseResult2 = JsonPath.read(responseDataSave2, "$.name" );
		Assert.assertEquals(dataResponseResult2, "TestScript1");
		
		//Check the content of "TestScript1" script has been updated
		String queryContent2 = sqlEditorService.getJSONData("getScriptContent");
		JSONObject resObj2 = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, queryContent2, dbProfile);
		JSONObject responseData2 = (JSONObject) resObj2.get("ResponseData");
		JSONArray itemsContent2= responseData2.getJSONArray("items");
		
		flag = false;
		//for upgrade, get the first one
		String dataResponseAbbrResult2 = JsonPath.read( (JSONObject) itemsContent2.get(0), "$.abbr" );
		if(dataResponseAbbrResult2.equals("SELECT * FROM SCH_SQLEDITOR.TAB_SQLEDITOR; SELECT * FROM SCH_SQLEDITOR.TAB_SQLEDITOR;")){
			flag = true;
		}
		else
		{
			//for new install, get the last one
			dataResponseAbbrResult2 = JsonPath.read( (JSONObject) itemsContent2.get(itemsContent2.size()-1), "$.abbr" );
			if(dataResponseAbbrResult2.equals("SELECT * FROM SCH_SQLEDITOR.TAB_SQLEDITOR; SELECT * FROM SCH_SQLEDITOR.TAB_SQLEDITOR;"));
				flag = true;
		}
		Assert.assertEquals(flag, true);
		//Assert.assertEquals(dataResponseAbbrResult2, "SELECT * FROM SCH_SQLEDITOR.TAB_SQLEDITOR; SELECT * FROM SCH_SQLEDITOR.TAB_SQLEDITOR;");
		
		//Delete the test scripts
		String fileName_DE="[\"TestScript1\"]";
		String owner_DE="["+JsonPath.read(responseDataSave1, "$.owner")+"]";
		
		JSONObject resObjDeleteInput = new JSONObject()  
        .element("cmd", "delete")     
        .element("name", fileName_DE)  
        .element("owner", owner_DE); 
		String deleteInput=resObjDeleteInput.toString();
		
		JSONObject resObjDelete = sqlEditorService.callSQLEditorService(AdminSQLEditorService.SCRIPTS, deleteInput, dbProfile);
		JSONObject responseDataDelete = (JSONObject) resObjDelete.get("ResponseData");
		Assert.assertNotNull(responseDataDelete);
		Assert.assertTrue(responseDataDelete.toString().length() > 0);	
		Assert.assertEquals(responseDataDelete.toString(), "{\"result\":true}");
			
		logger.info("PASSED: Save exsited test case to overwrite executed successfully");
	}
	
	/**
	 * @author cyinmei@cn.ibm.com
	 * This method is to test Data function in SQL result view
	 * Input SELECT SQL statement in SQL editor, execute it, then click Data in SQL result view to view data
	 * Verify request to retrieve data
	 * 
	 * */
	@Test (description = "Test running view data in sql result view")
	public void testViewDataInSQLResultView(){
		logger.info( "Runing view data in sql result view..." );
		String query = sqlEditorService.getJSONData("getSQLEditorViewData");

		JSONObject resObj = sqlEditorService.callSQLEditorService(AdminSQLEditorService.RUN_SQL, query, dbProfile);

		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);

		String queryId=JsonPath.read(responseData, "$.queryId");
		
		JSONObject resObjExecutionInput = new JSONObject()  
        .element("action", "RUNCOMMAND")     
        .element("type", "JDBC")  
        .element("queryId", queryId); 
		String executionInput=resObjExecutionInput.toString();
		JSONObject resObjExecute = sqlEditorService.callSQLEditorService(AdminSQLEditorService.RUN_SQL, executionInput, dbProfile);
		
		JSONObject responseDataExecute = (JSONObject) resObjExecute.get("ResponseData");
		Assert.assertNotNull(responseDataExecute);
		Assert.assertTrue(responseDataExecute.toString().length() > 0);	
		
		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"index":0,"start":1470114329570,
		 * "data":[{"layout":[{"id":"t1c0","field":"c0","name":"COL1","schema":"SCH_SQLEDITOR","table":"TAB_SQLEDITOR","dataType":"number","style":"text-align:right"}],
		 * "items":[{"c0":"1"},{"c0":"2"},{"c0":"3"}]}],"cost":359}
		 * */
				
		JSONArray dataContent= responseDataExecute.getJSONArray("data");
		JSONArray itemsContent=null;
		for ( int i = 0; i < dataContent.size(); i++ )
		{
			 JSONObject jo = (JSONObject) dataContent.get(i);
			 itemsContent= jo.getJSONArray("items");
			 
		}
		//itemsContent: [{"c0":"1"},{"c0":"2"},{"c0":"3"}]
		String[] dataResult = {"1","2","3"};
		String dataResponseResult = "";
		for(int i = 0; i < itemsContent.size(); i++){
				dataResponseResult = JsonPath.read( (JSONObject) itemsContent.get(i), "$.c0" );
				Assert.assertEquals(dataResponseResult, dataResult[i]);
		}
		
		logger.info("PASSED: View data in sql result view executed successfully");
	
	}
	
	protected boolean scriptsContained(String property, String basePropertyInclude, String containedMsg){
		String query = sqlEditorService.getJSONData( property );
		JSONObject resObj = sqlEditorService.callSQLEditorService( AdminSQLEditorService.SCRIPTS, query, dbProfile );
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );
		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		
		JSONObject propResult = JsonPath.read( responseData, "$" );
		String propResultS = propResult.toString();
		
		String baseInclude = sqlEditorService.getJSONData( basePropertyInclude );
		JSONObject propBaseInclude = JSONObject.fromObject( baseInclude );
		String propBaseIncludeS  = propBaseInclude.toString();
		return assertContained(propBaseIncludeS.substring(propBaseIncludeS.indexOf("items") + 8, propBaseIncludeS.length()-2),
				propResultS.substring(propResultS.indexOf("items") + 8, propResultS.length()));
	}
	
	public boolean assertContained(String expected, String actual) {
		boolean contained;
		try {
			System.out.println("actual = "+ actual + " ****** expected = " + expected);
			if(actual.indexOf(expected)!= -1){
				contained = true;
			}else{			
				contained = false;
			}
			System.out.println("*******contained = "+ contained );
			
		} catch (Throwable e) {
			throw e;
		}
		return contained;
	}	
}
