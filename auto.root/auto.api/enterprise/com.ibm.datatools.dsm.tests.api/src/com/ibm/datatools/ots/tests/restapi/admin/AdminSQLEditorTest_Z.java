package com.ibm.datatools.ots.tests.restapi.admin;

import org.testng.annotations.Test;

public class AdminSQLEditorTest_Z extends AdminSQLEditorTest {
	/**
	 * Stored procedure test
	 * @author litaocdl
	 * */
	@Test (description = "Test running stored procedure against sql editor")
	public void testRunStoredProcedure(){
		logger.info( "Runing stored procedure test..." );
;
		//Run SP with out params
		this.runSQLinJDBC("CALL SYSPROC.GET_SYSTEM_INFO(2,0,en_US,NULL,NULL,?,?)",";",true) ;
		//Run SP with in Params
		this.runSQLinJDBC("call SYSPROC.GET_CONFIG(?,?,null,null,null,?,?);",";",true) ;
		this.runSQLinJDBC("call SYSPROC.GET_CONFIG(2,0,en_US,null,null,?,?);",";",true) ;
		
		
	}
}
