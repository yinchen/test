package com.ibm.datatools.ots.tests.restapi.admin;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.AdminSQLObjectService;
import com.jayway.jsonpath.JsonPath;


public class AdminSQLObjectTest_LUW extends AdminTestBase{

	private static final Logger logger = LogManager.getLogger(AdminSQLObjectTest_LUW.class);
	
	private AdminSQLObjectService soService = null ;
//	private String version = null ;
	
	@BeforeClass
//	@Parameters({"version"})
	public void beforeTest(){
//		this.version=version;
		soService = new AdminSQLObjectService();
	}
	
	private JSONObject getResponseData(String URL,String key,boolean errorLoarding){
		// The key defined in the properties
		String query = soService.getJSONData(key);

		JSONObject resObj=null;
		if(errorLoarding==false){
		 resObj= soService.callSQLObjectService(
				URL, query, dbProfile,version);
		}
		else{
			resObj= soService.callSQLObjectErrorLoardingService(
					URL, query, dbProfile,version);
		}
		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);
		return responseData;
	}
	
	/**
	 * Test case of alter column definition
	 * */
	@Test(description="Test the alter column definition function")
	private void testAlterColumnDefinition(){
		logger.info("Running alter column definition test...");

		JSONObject responseData=getResponseData(AdminSQLObjectService.SQLOBJECT_DEFINITION_ALTER,"getAlterColumnDefinition_LUW",false);
		
		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"items":[{"values":[{"id":"BIGINT","SCALE":"No","value":"BIGINT","label":"BIGINT","LENGTH":"No","SEMANTICLENGTH":"No"},
		 * {"id":"BLOB","defaultLength":1048576,"SCALE":"No","value":"BLOB","label":"BLOB","LENGTH":"Yes","SEMANTICLENGTH":"No"},
		 * {"id":"CHAR","defaultLength":5,"SCALE":"No","value":"CHAR","label":"CHAR","SemanticLengthValues":["OCTETS","CODEUNITS32"],"LENGTH":"Yes","SEMANTICLENGTH":"Yes"},
		 * {"id":"CHAR FOR BIT DATA","defaultLength":1,"SCALE":"No","value":"CHAR FOR BIT DATA","label":"CHAR FOR BIT DATA","LENGTH":"Yes","SEMANTICLENGTH":"No"},
		 * {"id":"CLOB","defaultLength":1048576,"SCALE":"No","value":"CLOB","label":"CLOB","SemanticLengthValues":["OCTETS","CODEUNITS32"],"LENGTH":"Yes","SEMANTICLENGTH":"Yes"},{"id":"DATE","SCALE":"No","value":"DATE","label":"DATE","LENGTH":"No","SEMANTICLENGTH":"No"},{"id":"DBCLOB","defaultLength":1048576,"SCALE":"No","value":"DBCLOB","label":"DBCLOB","SemanticLengthValues":["CODEUNITS16","CODEUNITS32"],"LENGTH":"Yes","SEMANTICLENGTH":"Yes"},{"id":"DECFLOAT","defaultLength":34,"SCALE":"No","value":"DECFLOAT","label":"DECFLOAT","LENGTH":"Yes","SEMANTICLENGTH":"No"},{"id":"DECIMAL","defaultScale":0,"defaultLength":5,"SCALE":"Yes","value":"DECIMAL","label":"DECIMAL","LENGTH":"Yes","SEMANTICLENGTH":"No"},{"id":"DOUBLE","SCALE":"No","value":"DOUBLE","label":"DOUBLE","LENGTH":"No","SEMANTICLENGTH":"No"},{"id":"FLOAT","defaultLength":53,"SCALE":"No","value":"FLOAT","label":"FLOAT","LENGTH":"Yes","SEMANTICLENGTH":"No"},{"id":"GRAPHIC","defaultLength":1,"SCALE":"No","value":"GRAPHIC","label":"GRAPHIC","SemanticLengthValues":["CODEUNITS16","CODEUNITS32"],"LENGTH":"Yes","SEMANTICLENGTH":"Yes"},{"id":"INTEGER","SCALE":"No","value":"INTEGER","label":"INTEGER","LENGTH":"No","SEMANTICLENGTH":"No"},{"id":"LONG VARCHAR","SCALE":"No","value":"LONG VARCHAR","label":"LONG VARCHAR","LENGTH":"No","SEMANTICLENGTH":"No"},{"id":"LONG VARCHAR FOR BIT DATA","SCALE":"No","value":"LONG VARCHAR FOR BIT DATA","label":"LONG VARCHAR FOR BIT DATA","LENGTH":"No","SEMANTICLENGTH":"No"},{"id":"LONG VARGRAPHIC","SCALE":"No","value":"LONG VARGRAPHIC","label":"LONG VARGRAPHIC","LENGTH":"No","SEMANTICLENGTH":"No"},{"id":"NUMBER","defaultScale":0,"defaultLength":0,"SCALE":"Yes","value":"NUMBER","label":"NUMBER","LENGTH":"Yes","SEMANTICLENGTH":"No"},{"id":"NUMERIC","defaultScale":0,"defaultLength":5,"SCALE":"Yes","value":"NUMERIC","label":"NUMERIC","LENGTH":"Yes","SEMANTICLENGTH":"No"},{"id":"REAL","SCALE":"No","value":"REAL","label":"REAL","LENGTH":"No","SEMANTICLENGTH":"No"},{"id":"SMALLINT","SCALE":"No","value":"SMALLINT","label":"SMALLINT","LENGTH":"No","SEMANTICLENGTH":"No"},{"id":"TIME","SCALE":"No","value":"TIME","label":"TIME","LENGTH":"No","SEMANTICLENGTH":"No"},{"id":"TIMESTAMP","defaultLength":6,"SCALE":"No","value":"TIMESTAMP","label":"TIMESTAMP","LENGTH":"Yes","SEMANTICLENGTH":"No"},{"id":"VARCHAR","defaultLength":32,"SCALE":"No","value":"VARCHAR","label":"VARCHAR","SemanticLengthValues":["OCTETS","CODEUNITS32"],"LENGTH":"Yes","SEMANTICLENGTH":"Yes"},{"id":"VARCHAR FOR BIT DATA","defaultLength":1,"SCALE":"No","value":"VARCHAR FOR BIT DATA","label":"VARCHAR FOR BIT DATA","LENGTH":"Yes","SEMANTICLENGTH":"No"},{"id":"VARCHAR2","defaultLength":4096,"SCALE":"No","value":"VARCHAR2","label":"VARCHAR2","LENGTH":"Yes","SEMANTICLENGTH":"No"},{"id":"VARGRAPHIC","defaultLength":32,"SCALE":"No","value":"VARGRAPHIC","label":"VARGRAPHIC","SemanticLengthValues":["CODEUNITS16","CODEUNITS32"],"LENGTH":"Yes","SEMANTICLENGTH":"Yes"},{"id":"XML","SCALE":"No","value":"XML","label":"XML","LENGTH":"No","SEMANTICLENGTH":"No"}],
		 * "name":"TYPENAME",
		 * "label":"Data Type",
		 * "valueType":"enum",
		 * "defaultValue":{"value":"CHAR","label":"CHAR"}},{"name":"COLNAME","label":"Name","valueType":"string","defaultValue":{"value":"NEW_COLUMN","label":"Name"}},{"name":"LENGTH","label":"Length","valueType":"integer","siblings":["TYPENAME","SCALE"],"defaultValue":{"value":"5","label":"5"}},{"name":"INLINE_LENGTH","label":"Inline length","valueType":"integer"},{"name":"SCALE","label":"Scale","valueType":"integer","siblings":["TYPENAME","LENGTH"]},{"name":"NULLS","label":"Nullable","valueType":"boolean","defaultValue":{"value":"Yes","label":"Yes"}},{"name":"DEFAULT","label":"Default value","valueType":"string"},{"name":"PERIOD","label":"Period","valueType":"string"},{"values":[{"value":"SYSTEM","label":"SYSTEM"},{"value":"OFF","label":"OFF"}],"name":"COMPRESS","label":"Compress","valueType":"enum","defaultValue":{"value":"OFF","label":"OFF"}},{"values":[{"value":"IMPLICIT","label":"IMPLICIT"},{"value":"NONE","label":"NONE"}],"name":"HIDDEN","label":"Hidden","valueType":"enum",
		 * "defaultValue":{"value":"NONE","label":"NONE"}},{"values":[{"value":"ALWAYS","label":"ALWAYS"},{"value":"DEFAULT","label":"DEFAULT"},{"value":"NONE","label":"NONE"}],"name":"GENERATED","label":"Generated","valueType":"enum","defaultValue":{"value":"NONE","label":"NONE"}},{"name":"IDENTITY","label":"Identity","valueType":"boolean","defaultValue":{"value":"No","label":"No"}},{"name":"START","label":"Start","valueType":"integer"},{"name":"INCREMENT","label":"Increment by","valueType":"integer"},{"name":"MINVALUE","label":"Minimum","valueType":"integer"},{"name":"MAXVALUE","label":"Maximum","valueType":"integer"},{"name":"ROWCHANGETIMESTAMP","label":"Row change timestamp","valueType":"boolean"},{"name":"CYCLE","label":"Cycle","valueType":"boolean"},{"name":"CACHE","label":"Cache","valueType":"integer"},
		 * {"name":"ORDER","label":"Order","valueType":"boolean"}]}
		 * */
		
		Assert.assertNotNull(JsonPath.read(responseData, "$.items[0].values"));
		Assert.assertEquals("TYPENAME", JsonPath.read(responseData, "$.items[0].name"));
		Assert.assertEquals("Data type", JsonPath.read(responseData, "$.items[0].label"));
		Assert.assertEquals("enum", JsonPath.read(responseData, "$.items[0].valueType"));
		Assert.assertNotNull(JsonPath.read(responseData, "$.items[0].defaultValue"));
		
		Assert.assertEquals("COLNAME", JsonPath.read(responseData, "$.items[1].name"));
		Assert.assertEquals("Name", JsonPath.read(responseData, "$.items[1].label"));
		Assert.assertEquals("string", JsonPath.read(responseData, "$.items[1].valueType"));
		Assert.assertNotNull(JsonPath.read(responseData, "$.items[1].defaultValue"));
		
		Assert.assertEquals("LENGTH", JsonPath.read(responseData, "$.items[2].name"));
		Assert.assertEquals("Length", JsonPath.read(responseData, "$.items[2].label"));
		Assert.assertEquals("number", JsonPath.read(responseData, "$.items[2].valueType"));
		Assert.assertNotNull(JsonPath.read(responseData, "$.items[2].siblings"));
		Assert.assertNotNull(JsonPath.read(responseData, "$.items[2].defaultValue"));
		
		
		logger.info("PASSED: alter column definition executed successfully");
	}
	/**
	 * Test case of alter table definition
	 * */
	@Test(description="Test the alter table definition function")
	private void testAlterTableDefinition(){
		logger.info("Running alter table definition test...");

		JSONObject responseData=getResponseData(AdminSQLObjectService.SQLOBJECT_DEFINITION_ALTER,"getAlterTableDefinition_LUW",false);
		
		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 *{"items":[{"name":"TABNAME","label":"Name","valueType":"string"},{"values":[{"value":"System time period","label":"System time period"},{"value":"Application time period","label":"Application time period"}],"dependencies":[{"condition":[{"rule":"equal","value":"Column","key":"TABLEORG"}],"disable":true},{"condition":[{"rule":"equal","value":"History table","key":"TEMPORALTYPE"}],"disable":true}],"name":"TEMPORALTYPE","label":"Temporal value","valueType":"check","defaultValue":{"value":"None","label":"None"}},{"values":[{"value":" ","label":" "},{"value":"USERSPACE1","label":"USERSPACE1"},{"value":"IBMDB2SAMPLEREL","label":"IBMDB2SAMPLEREL"},{"value":"IBMDB2SAMPLEXML","label":"IBMDB2SAMPLEXML"},{"value":"SYSTOOLSPACE","label":"SYSTOOLSPACE"}],"name":"TBSPACE","label":"Table space","valueType":"enum","siblings":["INDEX_TBSPACE","LONG_TBSPACE"]},{"values":[{"value":" ","label":" "},{"value":"USERSPACE1","label":"USERSPACE1"},{"value":"IBMDB2SAMPLEREL","label":"IBMDB2SAMPLEREL"},{"value":"IBMDB2SAMPLEXML","label":"IBMDB2SAMPLEXML"},{"value":"SYSTOOLSPACE","label":"SYSTOOLSPACE"}],"name":"INDEX_TBSPACE","label":"Index table space","valueType":"enum","siblings":["TBSPACE","LONG_TBSPACE"]},{"values":[{"value":" ","label":" "},{"value":"USERSPACE1","label":"USERSPACE1"},{"value":"IBMDB2SAMPLEREL","label":"IBMDB2SAMPLEREL"},{"value":"IBMDB2SAMPLEXML","label":"IBMDB2SAMPLEXML"},{"value":"SYSTOOLSPACE","label":"SYSTOOLSPACE"}],"name":"LONG_TBSPACE","label":"Long table space","valueType":"enum","siblings":["TBSPACE","INDEX_TBSPACE"]},{"values":[{"value":"Adaptive","label":"Adaptive"},{"value":"Static","label":"Static"},{"value":"Yes","label":"Yes"},{"value":"No","label":"No"}],"name":"ROW_COMPRESSION","label":"Row compression","valueType":"enum","defaultValue":{"value":"No","label":"No"}},{"name":"VALUE_COMPRESSION","label":"Value compression","valueType":"boolean","defaultValue":{"value":"No","label":"No"}},{"values":[{"value":"None","label":"None"},{"value":"Changes","label":"Changes"},{"value":"Include longvar columns","label":"Include longvar columns"}],"name":"DATACAPTURE","label":"Data capture","valueType":"enum","defaultValue":{"value":"None","label":"None"}},{"name":"DROPRULE","label":"Restrict on drop","valueType":"boolean","defaultValue":{"value":"No","label":"No"}},{"values":[{"value":"Yes","label":"Yes"},{"value":"No","label":"No"}],"name":"VOLATILE","children":[{"name":"VOLATILE_CARD","label":"Cardinality","valueType":"boolean","defaultValue":{"value":"false","label":"false"}}],"label":"Volatile","valueType":"enum_boolean","defaultValue":{"value":"No","label":"No"}},{"values":[{"value":"ON","label":"ON"},{"value":"OFF","label":"OFF"}],"name":"APPEND_MODE","label":"Append on model","valueType":"enum","defaultValue":{"value":"OFF","label":"OFF"}},{"name":"PCTFREE","label":"Free(%)","valueType":"integer"},{"values":[{"value":"ROW","label":"ROW"},{"value":"BLOCKINSERT","label":"BLOCKINSERT"},{"value":"TABLE","label":"TABLE"}],"name":"LOCKSIZE","label":"Size of lock","valueType":"enum","defaultValue":{"value":"ROW","label":"ROW"}},{"values":[{"value":"Yes","label":"Yes"},{"value":"No","label":"No"},{"value":"EmptyTable","label":"EmptyTable"}],"name":"NOT_LOGGED_INITIALLY","label":"Not logged","valueType":"enum","defaultValue":{"value":"No","label":"No"}}]}

		 * */
		JSONArray ja=responseData.getJSONArray("items");
		Assert.assertNotNull(ja);
		Assert.assertTrue(ja.size()>0);
		
		Assert.assertEquals("TABNAME", JsonPath.read(responseData, "$.items[0].name"));
		Assert.assertEquals("Name", JsonPath.read(responseData, "$.items[0].label"));
		Assert.assertEquals("string", JsonPath.read(responseData, "$.items[0].valueType"));
		
		//Assert.assertNotNull(JsonPath.read(responseData, "$.items[1].values"));
		
		logger.info("PASSED: alter table definition executed successfully");
	}
	/**
	 * Test case of create definition
	 * */
	@Test(description="Test the create definition function")
	private void testCreateDefinition(){
		logger.info("Running create definition test...");

		JSONObject responseData=getResponseData(AdminSQLObjectService.SQLOBJECT_DEFINITION_CREATE,"getCreateDefinition_LUW",false);
		
		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * */
		Assert.assertNotNull(responseData);
		
		logger.info("PASSED: create definition executed successfully");
	}
	
	/**
	 * Test case of authId loading
	 * */
	@Test(description="Test authId Loading function")
	private void testAuthIdLoading(){
		logger.info("Running authId loading test...");

		JSONObject responseData=getResponseData(AdminSQLObjectService.SQLOBJECT_PRIVILEGE_MODEL,"getAuthIdLoading_LUW",false);
		
		/**
		 * Path the response data, for this case, the response data is:
		 * {"roleMapping":[[null,5,2,0],[null,5,3,0]],
		 * "groupMapping":[[5,-1,"DB2IADM1"]],
		 * "authIds":[["AAA","G"],["PUBLIC","G"],["SYSDEBUG","R"],["SYSDEBUGPRIVATE","R"],["BBB","U"],["DB2INST1","U"]]}
		 * 
		 * */
		
		Assert.assertNotNull(JsonPath.read(responseData,"$.roleMapping"));
		Assert.assertNotNull(JsonPath.read(responseData,"$.groupMapping"));
		Assert.assertNotNull(JsonPath.read(responseData,"$.authIds"));
		logger.info("PASSED: authId loading executed successfully");
	}
	
	/**
	 * Test case of load table 
	 * */
	@Test(description="Test load table function")
	private void testLoadTable(){
		logger.info("Running load table test...");
		JSONObject responseData=getResponseData(AdminSQLObjectService.SQLOBJECT_PRIVILEGE_SQLOBJECT,"getLoadTable_LUW",false);
		
		/**
		 * Path the response data, for this case, the response data is:
		 * {"count":34,"objects":[{"name":"DB2INST1.CL_SCHED"},
		 * {"name":"DB2INST1.DEPARTMENT"},{"name":"DB2INST1.ACT"},{"name":"SYSTOOLS.POLICY"},{"name":"DB2INST1.T_0000"},{"name":"DB2INST1.T_00001"},{"name":"DB2INST1.TEST1"},{"name":"IBM_RTMON.LOCK_UE"},{"name":"DB2INST1.EMPLOYEE"},{"name":"DB2INST1.EMP_PHOTO"},{"name":"DB2INST1.EMP_RESUME"},{"name":"DB2INST1.PROJECT"},{"name":"DB2INST1.PROJACT"},{"name":"DB2INST1.EMPPROJACT"},{"name":"DB2INST1.IN_TRAY"},{"name":"DB2INST1.ORG"},{"name":"DB2INST1.STAFF"},{"name":"DB2INST1.SALES"},{"name":"DB2INST1.STAFFG"},{"name":"DB2INST1.EMPMDC"},{"name":"DB2INST1.PRODUCT"},{"name":"DB2INST1.INVENTORY"},{"name":"DB2INST1.CUSTOMER"},{"name":"DB2INST1.PURCHASEORDER"},{"name":"DB2INST1.CATALOG"},{"name":"DB2INST1.SUPPLIERS"},{"name":"DB2INST1.PRODUCTSUPPLIER"},{"name":"SYSTOOLS.HMON_ATM_INFO"},{"name":"SYSTOOLS.HMON_COLLECTION"},{"name":"\u6211\u7684\u6d4b\u8bd5SCHEMA12345.\u6211\u7684\u8868BUGBASH"},
		 * {"name":"\u6211\u7684\u6d4b\u8bd5SCHEMA12345.\u6211\u7684\u8868BUGBASH2"},{"name":"\u6211\u7684\u6d4b\u8bd5SCHEMA12345.TABLEbugbash"},{"name":"\u6211\u7684\u6d4b\u8bd5SCHEMA12345.TABLE_HIST"},{"name":"IBM_RTMON.THRESH_VIOLATIONS"}],
		 * "actions":["CONTROL","ALTER","DELETE","INDEX","INSERT","REFERENCES","SELECT","UPDATE"]}
		 * */
		Assert.assertNotNull(JsonPath.read(responseData,"$.actions"));
		Assert.assertNotNull(JsonPath.read(responseData,"$.rowCount"));
		Assert.assertNotNull(JsonPath.read(responseData,"$.objects"));
		logger.info("PASSED: load table executed successfully");
	}
	/**
	 * Test case of load view 
	 * */
	@Test(description="Test load view function")
	private void testLoadView(){
		logger.info("Running load view test...");
		JSONObject responseData=getResponseData(AdminSQLObjectService.SQLOBJECT_PRIVILEGE_SQLOBJECT,"getLoadView_LUW",false);
		
		/**
		 * Path the response data, for this case, the response data is:
		 * {"count":34,"objects":[{"name":"DB2INST1.CL_SCHED"},
		 * {"name":"DB2INST1.DEPARTMENT"},{"name":"DB2INST1.ACT"},{"name":"SYSTOOLS.POLICY"},{"name":"DB2INST1.T_0000"},{"name":"DB2INST1.T_00001"},{"name":"DB2INST1.TEST1"},{"name":"IBM_RTMON.LOCK_UE"},{"name":"DB2INST1.EMPLOYEE"},{"name":"DB2INST1.EMP_PHOTO"},{"name":"DB2INST1.EMP_RESUME"},{"name":"DB2INST1.PROJECT"},{"name":"DB2INST1.PROJACT"},{"name":"DB2INST1.EMPPROJACT"},{"name":"DB2INST1.IN_TRAY"},{"name":"DB2INST1.ORG"},{"name":"DB2INST1.STAFF"},{"name":"DB2INST1.SALES"},{"name":"DB2INST1.STAFFG"},{"name":"DB2INST1.EMPMDC"},{"name":"DB2INST1.PRODUCT"},{"name":"DB2INST1.INVENTORY"},{"name":"DB2INST1.CUSTOMER"},{"name":"DB2INST1.PURCHASEORDER"},{"name":"DB2INST1.CATALOG"},{"name":"DB2INST1.SUPPLIERS"},{"name":"DB2INST1.PRODUCTSUPPLIER"},{"name":"SYSTOOLS.HMON_ATM_INFO"},{"name":"SYSTOOLS.HMON_COLLECTION"},{"name":"\u6211\u7684\u6d4b\u8bd5SCHEMA12345.\u6211\u7684\u8868BUGBASH"},
		 * {"name":"\u6211\u7684\u6d4b\u8bd5SCHEMA12345.\u6211\u7684\u8868BUGBASH2"},{"name":"\u6211\u7684\u6d4b\u8bd5SCHEMA12345.TABLEbugbash"},{"name":"\u6211\u7684\u6d4b\u8bd5SCHEMA12345.TABLE_HIST"},{"name":"IBM_RTMON.THRESH_VIOLATIONS"}],"actions":["CONTROL","ALTER","DELETE","INDEX","INSERT","REFERENCES","SELECT","UPDATE"]}
		 * */
		Assert.assertNotNull(JsonPath.read(responseData,"$.actions"));
		Assert.assertNotNull(JsonPath.read(responseData,"$.rowCount"));
		Assert.assertNotNull(JsonPath.read(responseData,"$.objects"));
		logger.info("PASSED: load view executed successfully");
	}
	/**
	 * Test case of load schema 
	 * */
	@Test(description="Test load schema function")
	private void testLoadSchema(){
		logger.info("Running load schema test...");
		JSONObject responseData=getResponseData(AdminSQLObjectService.SQLOBJECT_PRIVILEGE_SQLOBJECT,"getLoadSchema_LUW",false);
		
		/**
		 * Path the response data, for this case, the response data is:
		 * {"count":34,"objects":[{"name":"DB2INST1.CL_SCHED"},
		 * {"name":"DB2INST1.DEPARTMENT"},{"name":"DB2INST1.ACT"},{"name":"SYSTOOLS.POLICY"},{"name":"DB2INST1.T_0000"},{"name":"DB2INST1.T_00001"},{"name":"DB2INST1.TEST1"},{"name":"IBM_RTMON.LOCK_UE"},{"name":"DB2INST1.EMPLOYEE"},{"name":"DB2INST1.EMP_PHOTO"},{"name":"DB2INST1.EMP_RESUME"},{"name":"DB2INST1.PROJECT"},{"name":"DB2INST1.PROJACT"},{"name":"DB2INST1.EMPPROJACT"},{"name":"DB2INST1.IN_TRAY"},{"name":"DB2INST1.ORG"},{"name":"DB2INST1.STAFF"},{"name":"DB2INST1.SALES"},{"name":"DB2INST1.STAFFG"},{"name":"DB2INST1.EMPMDC"},{"name":"DB2INST1.PRODUCT"},{"name":"DB2INST1.INVENTORY"},{"name":"DB2INST1.CUSTOMER"},{"name":"DB2INST1.PURCHASEORDER"},{"name":"DB2INST1.CATALOG"},{"name":"DB2INST1.SUPPLIERS"},{"name":"DB2INST1.PRODUCTSUPPLIER"},{"name":"SYSTOOLS.HMON_ATM_INFO"},{"name":"SYSTOOLS.HMON_COLLECTION"},{"name":"\u6211\u7684\u6d4b\u8bd5SCHEMA12345.\u6211\u7684\u8868BUGBASH"},
		 * {"name":"\u6211\u7684\u6d4b\u8bd5SCHEMA12345.\u6211\u7684\u8868BUGBASH2"},{"name":"\u6211\u7684\u6d4b\u8bd5SCHEMA12345.TABLEbugbash"},{"name":"\u6211\u7684\u6d4b\u8bd5SCHEMA12345.TABLE_HIST"},{"name":"IBM_RTMON.THRESH_VIOLATIONS"}],"actions":["CONTROL","ALTER","DELETE","INDEX","INSERT","REFERENCES","SELECT","UPDATE"]}
		 * */
		Assert.assertNotNull(JsonPath.read(responseData,"$.actions"));
		Assert.assertNotNull(JsonPath.read(responseData,"$.rowCount"));
		Assert.assertNotNull(JsonPath.read(responseData,"$.objects"));
		logger.info("PASSED: load schema executed successfully");
	}
	/**
	 * Test case of load database 
	 * */
	/*@Test(description="Test load database function")
	private void testLoadDatabase(){
		logger.info("Running load database test...");
		JSONObject responseData=getResponseData(AdminSQLObjectService.SQLOBJECT_PRIVILEGE_SQLOBJECT,"getLoadDatabase_LUW",false);
		
		*//**
		 * Path the response data, for this case, the response data is:
		 * {"objects":[{"id":".sample","name":"sample"}],
		 * "actions":["BINDADD","CONNECT","CREATETAB","CREATE_EXTERNAL_ROUTINE","CREATE_NOT_FENCED_ROUTINE","IMPLICIT_SCHEMA","DBADM","LOAD","QUIESCE_CONNECT","SECADM","ACCESSCTRL","DATAACCESS","EXPLAIN","SQLADM","WLMADM"]}
		 * *//*
		Assert.assertNotNull(JsonPath.read(responseData,"$.actions"));
		Assert.assertNotNull(JsonPath.read(responseData,"$.objects"));
		logger.info("PASSED: load database executed successfully");
	}*/
	/**
	 * Test case of load function 
	 * */
	@Test(description="Test load function function")
	private void testLoadFunction(){
		logger.info("Running load function test...");
		JSONObject responseData=getResponseData(AdminSQLObjectService.SQLOBJECT_PRIVILEGE_SQLOBJECT,"getLoadFunction_LUW",false);
		
		/**
		 * Path the response data, for this case, the response data is:
		 * */
		Assert.assertNotNull(JsonPath.read(responseData,"$.actions"));
		Assert.assertNotNull(JsonPath.read(responseData,"$.rowCount"));
		Assert.assertNotNull(JsonPath.read(responseData,"$.objects"));
		logger.info("PASSED: load function executed successfully");
	}
	/**
	 * Test case of load procedure 
	 * */
	@Test(description="Test load procedure function")
	private void testLoadProcedure(){
		logger.info("Running load procedure test...");
		JSONObject responseData=getResponseData(AdminSQLObjectService.SQLOBJECT_PRIVILEGE_SQLOBJECT,"getLoadProcedure_LUW",false);
		
		/**
		 * Path the response data, for this case, the response data is:
		 * */
		Assert.assertNotNull(JsonPath.read(responseData,"$.actions"));
		Assert.assertNotNull(JsonPath.read(responseData,"$.rowCount"));
		Assert.assertNotNull(JsonPath.read(responseData,"$.objects"));
		logger.info("PASSED: load procedure executed successfully");
	}
	
}
