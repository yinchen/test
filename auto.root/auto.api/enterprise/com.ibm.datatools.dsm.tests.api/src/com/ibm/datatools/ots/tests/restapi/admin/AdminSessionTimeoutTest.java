package com.ibm.datatools.ots.tests.restapi.admin;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.ConnectionTestService;

import net.sf.json.JSONObject;

public class AdminSessionTimeoutTest  extends AdminTestBase {

	private static final Logger logger = LogManager
			.getLogger(AdminSessionTimeoutTest.class);
	protected ConnectionTestService connservice = new ConnectionTestService();
	
	@Test(description = "Test session timeout")
	private void testSessionTimeout(){

		logger.info("Thread Do nothing for 2 hours by default");
		try {
			Thread.sleep(7210*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("Send reqeust after 2 hours");
		//Send get connection request,should get 302 errors
		JSONObject resObj =connservice.getConnectionByProfileInJsonWithoutCode(dbProfile);
		int responseCode = (Integer) resObj.get( "ResponseCode" );
		Assert.assertNotNull( responseCode );
		Assert.assertEquals( responseCode,302);
	}
}
