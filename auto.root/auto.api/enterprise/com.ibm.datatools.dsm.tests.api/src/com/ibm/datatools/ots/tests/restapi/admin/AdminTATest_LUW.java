package com.ibm.datatools.ots.tests.restapi.admin;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.AdminTAService;
import com.ibm.datatools.test.utils.AdminConst;
import com.ibm.datatools.ots.tests.restapi.base.CheckMonitorDB;
import com.ibm.datatools.ots.tests.restapi.common.AdminSQLEditorService;
import com.jayway.jsonpath.JsonPath;


public class AdminTATest_LUW extends AdminTestBase{

	private static final Logger logger = LogManager.getLogger(AdminTATest_LUW.class);
	
	private AdminTAService taService = null ;
	
	//private String instanceProfileName = null ;
	//private String hostName = null ;
	
	protected AdminSQLEditorService sqlEditorService = new AdminSQLEditorService();
	
	@BeforeClass
	//@Parameters({"instanceProfile","hostName"})	
	public void beforeTest(){
		taService = new AdminTAService();
		logger.info("taService returned");
		//this.instanceProfileName = instanceProfile ;
		
		//this.hostName=hostName;
		//only testConfigureLoggingManualArchive and testRollForwardDB in BVT suite
		//so skip the unused pre-condition
		if(!this.isBVT){
			testCreateSPRevalidate_precondition();
			testCreateTriggerRevalidate_precondition();
			testCreateUDFRevalidate_precondition();
			if(this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_105)){
				testConvertRowTableToColumnTable_precondition();
			}
			testCreateViewRevalidate_preconditon();
		}
	}
	@AfterClass
	public void afterTest() {
		if(!this.isBVT){
			testCreateSPRevalidate_clean();
			testCreateTriggerRevalidate_clean();
			testCreateUDFRevalidate_clean();
			if(this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_105)){
				testConvertRowTableToColumnTable_clean();
			}
			testCreateViewRevalidate_clean();
		}
	}

	private void testPrecondition(String caseSituation) {
		
		List<String> sqlList = new ArrayList<String>();
		
		if (caseSituation=="RevalidateDBObjects"){
			
			sqlList.add("Drop view SCHEMA4REV.VIEW4REV");
			sqlList.add("Drop table SCHEMA4REV.TABLE4REV");
			sqlList.add("Drop schema SCHEMA4REV RESTRICT");
			sqlEditorService.runSQLinJDBC(sqlList, ";", this.dbProfile,false);
			sqlList.clear();
			sqlList.add("Create schema SCHEMA4REV");
			sqlList.add("Create table SCHEMA4REV.TABLE4REV (col1 char,col2 char,col3 char)");
			sqlList.add("Create view  SCHEMA4REV.VIEW4REV as (select col1 vcol1, col2 vcol2 from SCHEMA4REV.TABLE4REV)");
			sqlList.add("Alter table SCHEMA4REV.TABLE4REV drop column col2");
			sqlEditorService.runSQLinJDBC(sqlList, ";", this.dbProfile,true);
		}
				
	}
	
private void testPreconditionClean(String caseSituation) {
		
		List<String> sqlList = new ArrayList<String>();
		
		if (caseSituation=="RevalidateDBObjects"){
			
			sqlList.add("Drop view SCHEMA4REV.VIEW4REV");
			sqlList.add("Drop table SCHEMA4REV.TABLE4REV");
			sqlList.add("Drop schema SCHEMA4REV RESTRICT");
			sqlEditorService.runSQLinJDBC(sqlList, ";", this.dbProfile,true);
		}
				
	}
	
	private JSONObject getResponseData(String URL,String key,boolean instance){
		// The key defined in the properties
		String query = taService.getJSONData(key);
		
		
		JSONObject resObj=null;
		if(!instance){
		 resObj= taService.callTAService(
				URL, query, dbProfile);
		}
		else resObj= taService.callInstanceTAService(
				URL, query, dbProfile,instanceProfile,hostName);
		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);
		return responseData;
	}
	/**
	 * Copy this method to create a new TA test
	 * */
	//@Test(description="<description>")
	//@Parameters({"dbProfile"})
	@SuppressWarnings("unused")
	private void testFormat(String dbProfile){
		logger.info("Running <test name> test...");

		JSONObject responseData=getResponseData("<URL>","query",false);

		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * 
		 * To retrieve the statement, we are looking for...
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "<pass expression>");
		Assert.assertEquals(ddl, "expected");
		
		logger.info("PASSED: <test Name> executed successfully");
	}
	
	
	/**
	 * Test case of back up database to S3 with existing alias
	 * */
	@Test(description="Test the backup database to S3 with existing alias function")
	private void testBackupDBToS3withExistingAlias(){
		if(this.version != null){
			if(this.compareToVersion(this.version,"V10.5.5") < 0 ){
				return;
			}
		}
		logger.info("Running Back up database to S3  with existing alias...");

		JSONObject responseData=getResponseData(AdminTAService.TA_BACKUP_DATABASE,"getBackupDBToS3withExistingAlias_Cloud",false);
		
		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"items":[{"statement":"CONNECT TO \"sample\""},
		 * {"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
		 * {"statement":"CONNECT RESET"},
		 * {"statement":"DEACTIVATE DATABASE \"sample\""},
		 * {"statement":"BACKUP DATABASE \"sample\" TO DB2REMOTE:\/\/s3User\/imageFolder\/temp EXCLUDE LOGS WITHOUT PROMPTING"},
		 * {"statement":"CONNECT TO \"sample\""},
		 * {"statement":"UNQUIESCE DATABASE"},
		 * {"statement":"CONNECT RESET"}]}
		 * 
		 * To retrieve the statement, we are looking for 0,1,2,3,4
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertTrue(ddl.startsWith("CONNECT TO"));
		ddl = JsonPath.read(responseData, "$.items[1].statement");
		Assert.assertEquals(ddl, "QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS");
		ddl = JsonPath.read(responseData, "$.items[2].statement");
		Assert.assertEquals(ddl, "CONNECT RESET");
		ddl = JsonPath.read(responseData, "$.items[3].statement");
		Assert.assertTrue(ddl.startsWith("DEACTIVATE DATABASE"));
		ddl = JsonPath.read(responseData, "$.items[4].statement");
		Assert.assertTrue(ddl.startsWith("BACKUP DATABASE"));
		Assert.assertTrue(ddl.endsWith("TO DB2REMOTE://s3User/imageFolder/temp EXCLUDE LOGS WITHOUT PROMPTING"));
		ddl = JsonPath.read(responseData, "$.items[5].statement");
		Assert.assertTrue(ddl.startsWith("CONNECT TO"));
		ddl = JsonPath.read(responseData, "$.items[6].statement");
		Assert.assertEquals(ddl, "UNQUIESCE DATABASE");
		ddl = JsonPath.read(responseData, "$.items[7].statement");
		Assert.assertEquals(ddl, "CONNECT RESET");
		
		logger.info("PASSED: back up database to S3 with existing alias executed successfully");
	}
	
	/**
	 * Test case of online backup database to S3 with new alias
	 * */
	@Test(description="Test online backup database to S3 with new alias function")
	private void testOnlineBackupDBToS3withNewAlias(){
		if(this.version != null){
			if(this.compareToVersion(this.version,"V10.5.5") < 0 ){
				return;
			}
		}
		logger.info("Running online Back up database to S3 with new alias...");

		JSONObject responseData=getResponseData(AdminTAService.TA_BACKUP_DATABASE,"getOnlineBackupDBToS3withNewAlias_Cloud",false);
		
		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"items":[{"statement":"CATALOG STORAGE ACCESS ALIAS newS3 VENDOR S3 SERVER s3.amazonaws.com USER AKIAJWZ2OLLLPPYIMTLA PASSWORD 9rfWVUNTILETsrhFjtlCTWFLFKIYDbIs3n3RJy"},
         * {"statement":"BACKUP DATABASE MONDB ONLINE TO DB2REMOTE:\/\/newS3\/imageFolder\/temp EXCLUDE LOGS WITHOUT PROMPTING"}]}
		 * 
		 * To retrieve the statement, we are looking for 0,1
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		Assert.assertEquals(ddl,"CATALOG STORAGE ACCESS ALIAS newS3 VENDOR S3 SERVER s3.amazonaws.com USER AKIAJWZ2OLLLPPYIMTLA PASSWORD 9rfWVUNTILETsrhFjtlCTWFLFKIYDbIs3n3RJy");
		ddl = JsonPath.read(responseData, "$.items[1].statement");
		Assert.assertTrue(ddl.startsWith("BACKUP DATABASE"));
		Assert.assertTrue(ddl.endsWith("ONLINE TO DB2REMOTE://newS3/imageFolder/temp EXCLUDE LOGS WITHOUT PROMPTING"));
		logger.info("PASSED: online Back up database to S3 with new alias executed successfully");
	}
	
	
	/**
	 * Test case of restore to a new DB by using another db's image with new SwiftUser 
	 * */
	@Test(description="Test online backup database to S3 with new alias function")
	private void restoreTONewDBUsingAnotherImageWithNewSwift(){
		if(this.version != null){
			if(this.compareToVersion(this.version,"V10.5.5") < 0 ){
				return;
			}
		}
		logger.info("Running restore to a new DB by using another db's image with new SwiftUser ...");

		JSONObject responseData=getResponseData(AdminTAService.TA_RESTORE_COMMAND,"getRestoreTONewDBUsingAnotherImageWithNewSwift_Cloud",false);
		
		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"items":[{"statement":"CATALOG STORAGE ACCESS ALIAS SwiftUser VENDOR SOFTLAYER SERVER https:\/\/ams01.objectstorage.softlayer.net\/auth\/v1.0 USER IBMOS283639-3:ibm132786 PASSWORD 91ba722a7263c89ad6a5dedf74fbb6c41b4ffc04e23715a256c2680c9613b1ba CONTAINER db2cloud-qa OBJECT backup"},
         * {"statement":"RESTORE DATABASE HOSTQADB FROM DB2REMOTE:\/\/SwiftUser\/db2cloud-qa\/backup  TAKEN AT 20150525123728  INTO NEWDB WITHOUT PROMPTING"}]}
		 * To retrieve the statement, we are looking for 0,1
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		Assert.assertEquals(ddl,"CATALOG STORAGE ACCESS ALIAS SwiftUser VENDOR SOFTLAYER SERVER https://ams01.objectstorage.softlayer.net/auth/v1.0 USER IBMOS283639-3:ibm132786 PASSWORD 91ba722a7263c89ad6a5dedf74fbb6c41b4ffc04e23715a256c2680c9613b1b CONTAINER db2cloud-qa OBJECT backup");
		ddl = JsonPath.read(responseData, "$.items[1].statement");
		Assert.assertEquals(ddl,"RESTORE DATABASE HOSTQADB FROM DB2REMOTE://SwiftUser/db2cloud-qa/backup  TAKEN AT 20150525123728  INTO NEWDB WITHOUT PROMPTING");
		logger.info("PASSED: restore to a new DB by using another db's image with new SwiftUser  executed successfully");
	}
	
	
	
	
	
	/**
	 * Test case of back up database with encrypt option
	 * */
	@Test(description="Test the backup database with encrypt option function")
	private void testBackupDBWithEncryptOption(){
		logger.info("Running Back up database with encrypt option test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_BACKUP_DATABASE,"getBackupDBWithEncryptOption_LUW",false);
		
		/**
		 * Path the response data, for this case, the response data is:
		 * 
		 * {"items":[{"statement":"CONNECT TO \"sample\""},
		 * {"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
		 * {"statement":"CONNECT RESET"},
		 * {"statement":"DEACTIVATE DATABASE \"sample\""},
		 * {"statement":"BACKUP DATABASE \"sample\" TO \"\/home\/db2inst3\" EXCLUDE LOGS WITHOUT PROMPTING"},
		 * {"statement":"CONNECT TO \"sample\""},
		 * {"statement":"UNQUIESCE DATABASE"},
		 * {"statement":"CONNECT RESET"}]}
		 * 
		 * To retrieve the statement, we are looking for 0,1,2,3,4
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertTrue(ddl.startsWith("CONNECT TO"));
		ddl = JsonPath.read(responseData, "$.items[1].statement");
		Assert.assertEquals(ddl, "QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS");
		ddl = JsonPath.read(responseData, "$.items[2].statement");
		Assert.assertEquals(ddl, "CONNECT RESET");
		ddl = JsonPath.read(responseData, "$.items[3].statement");
		Assert.assertTrue(ddl.startsWith("DEACTIVATE DATABASE"));
		ddl = JsonPath.read(responseData, "$.items[4].statement");
		Assert.assertTrue(ddl.startsWith("BACKUP DATABASE"));
		Assert.assertTrue(ddl.endsWith("EXCLUDE LOGS WITHOUT PROMPTING"));
		ddl = JsonPath.read(responseData, "$.items[5].statement");
		Assert.assertTrue(ddl.startsWith("CONNECT TO"));
		ddl = JsonPath.read(responseData, "$.items[6].statement");
		Assert.assertEquals(ddl, "UNQUIESCE DATABASE");
		ddl = JsonPath.read(responseData, "$.items[7].statement");
		Assert.assertEquals(ddl, "CONNECT RESET");
		
		logger.info("PASSED: back up database with encrypt option executed successfully");
	}
	
	/**
	 * Test case of back up database only encrypt lib
	 * */
	@Test(description="Test the backup database only encrypt lib function")
	private void testBackupDBOnlyEncryptLib(){
		logger.info("Running Back up database with encrypt option test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_BACKUP_DATABASE,"getBackupDBOnlyEncryptLib_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CONNECT TO \"sample\""},
		 * {"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
		 * {"statement":"CONNECT RESET"},
		 * {"statement":"DEACTIVATE DATABASE \"sample\""},
		 * {"statement":"BACKUP DATABASE \"sample\" TO \"\/home\/db2inst3\" EXCLUDE LOGS WITHOUT PROMPTING"},
		 * {"statement":"CONNECT TO \"sample\""},
		 * {"statement":"UNQUIESCE DATABASE"},
		 * {"statement":"CONNECT RESET"}]}
		 * 
		 * To retrieve the statement, we are looking for 0,1,2,3,4
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertTrue(ddl.startsWith("CONNECT TO"));
		ddl = JsonPath.read(responseData, "$.items[1].statement");
		Assert.assertEquals(ddl, "QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS");
		ddl = JsonPath.read(responseData, "$.items[2].statement");
		Assert.assertEquals(ddl, "CONNECT RESET");
		ddl = JsonPath.read(responseData, "$.items[3].statement");
		Assert.assertTrue(ddl.startsWith("DEACTIVATE DATABASE"));
		ddl = JsonPath.read(responseData, "$.items[4].statement");
		Assert.assertTrue(ddl.startsWith("BACKUP DATABASE"));
		Assert.assertTrue(ddl.endsWith("EXCLUDE LOGS WITHOUT PROMPTING"));
		ddl = JsonPath.read(responseData, "$.items[5].statement");
		Assert.assertTrue(ddl.startsWith("CONNECT TO"));
		ddl = JsonPath.read(responseData, "$.items[6].statement");
		Assert.assertEquals(ddl, "UNQUIESCE DATABASE");
		ddl = JsonPath.read(responseData, "$.items[7].statement");
		Assert.assertEquals(ddl, "CONNECT RESET");
		
		logger.info("PASSED: back up database only encrypt lib executed successfully");
	}
	
	/**
	 * add by wtaobj@cn.ibm.com
	 * Test case of back up database using TSM and Vendor
	 * perform a full backup to the TSM device with a Vendor options
	 * */
	@Test(description="Test the backup database using TSM and Vendor function")
	private void testBackupDBTsmVendor(){
		logger.info("Running Back up database using TSM and Vendor option test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_BACKUP_DATABASE,"getBackupDBTsmVendor_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CONNECT TO \"sample\""},
		 * {"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
		 * {"statement":"CONNECT RESET"},
		 * {"statement":"DEACTIVATE DATABASE \"sample\""},
		 * {"statement":"BACKUP DATABASE \"sample\" USE TSM OPEN 3 SESSIONS OPTIONS Vendor EXCLUDE LOGS WITHOUT PROMPTING"},
		 * {"statement":"CONNECT TO \"sample\""},
		 * {"statement":"UNQUIESCE DATABASE"},
		 * {"statement":"CONNECT RESET"}]}
		 * 
		 * To retrieve the statement, we are looking for 4
		 * $.items[0].statement
		 * */

		String ddl = JsonPath.read(responseData, "$.items[4].statement");
		//Assert.assertEquals(ddl,"BACKUP DATABASE \"sample\" USE TSM OPEN 3 SESSIONS OPTIONS Vendor EXCLUDE LOGS WITHOUT PROMPTING");
		Assert.assertTrue(ddl.startsWith("BACKUP DATABASE"));
		Assert.assertTrue(ddl.endsWith("USE TSM OPEN 3 SESSIONS OPTIONS Vendor EXCLUDE LOGS WITHOUT PROMPTING"));
		
		logger.info("PASSED: back up database using TSM and Vendor executed successfully");
	}
	
	/**
	 * add by wtaobj@cn.ibm.com
	 * Test case of back up database using XBSA and Vendor
	 * perform a full backup to the XBSA device with a Vendor options
	 * */
	@Test(description="Test the backup database using XBSA and Vendor function")
	private void testBackupDBXBSAVendor(){
		logger.info("Running Back up database using XBSA and Vendor option test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_BACKUP_DATABASE,"getBackupXBSAsmVendor_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CONNECT TO \"sample\""},
		 * {"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
		 * {"statement":"CONNECT RESET"},
		 * {"statement":"DEACTIVATE DATABASE \"sample\""},
		 * {"statement":"BACKUP DATABASE \"sample\" USE XBSA OPEN 4 SESSIONS OPTIONS options-string EXCLUDE LOGS WITHOUT PROMPTING"},
		 * {"statement":"CONNECT TO \"sample\""},
		 * {"statement":"UNQUIESCE DATABASE"},
		 * {"statement":"CONNECT RESET"}]}
		 * 
		 * To retrieve the statement, we are looking for 4
		 * $.items[0].statement
		 * */

		String ddl = JsonPath.read(responseData, "$.items[4].statement");
		//Assert.assertEquals(ddl,"BACKUP DATABASE \"sample\" USE XBSA OPEN 4 SESSIONS OPTIONS options-string EXCLUDE LOGS WITHOUT PROMPTING");
		Assert.assertTrue(ddl.startsWith("BACKUP DATABASE"));
		Assert.assertTrue(ddl.endsWith("USE XBSA OPEN 4 SESSIONS OPTIONS options-string EXCLUDE LOGS WITHOUT PROMPTING"));
		logger.info("PASSED: back up database using XBSA and Vendor executed successfully");
	}
	
	/**
	 * add by wtaobj@cn.ibm.com
	 * Test case of back up database using VendorDDL
	 * perform a full backup to the VendorDDL device
	 * 
	 * comments by wtao July 22 2016,run failed due to Defect 91159.
	 * */
	
/**
	 
	@Test(description="Test the backup database using VendorDDL function")
	private void testBackupDBVendorDDL(){
		logger.info("Running Back up database using VendorDDL option test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_BACKUP_DATABASE,"getBackupDBVendorDDL_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CONNECT TO \"sample\""},
		 * {"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
		 * {"statement":"CONNECT RESET"},
		 * {"statement":"DEACTIVATE DATABASE \"sample\""},
		 * {"statement":"BACKUP DATABASE \"sample\" LOAD \"/home/db2inst1/sqllib/lib32/libcapro.so\" OPEN 0 SESSIONS  EXCLUDE LOGS WITHOUT PROMPTING"},
		 * {"statement":"CONNECT TO \"sample\""},
		 * {"statement":"UNQUIESCE DATABASE"},
		 * {"statement":"CONNECT RESET"}]}
		 * 
		 * To retrieve the statement, we are looking for 4
		 * $.items[0].statement
		 * */

/**
		String ddl = JsonPath.read(responseData, "$.items[4].statement");
		//Assert.assertEquals(ddl,"BACKUP DATABASE \"sample\" LOAD \"/home/db2inst1/sqllib/lib32/libcapro.so\" OPEN 0 SESSIONS  EXCLUDE LOGS WITHOUT PROMPTING");
		Assert.assertTrue(ddl.startsWith("BACKUP DATABASE"));
		Assert.assertTrue(ddl.endsWith("LOAD \"/home/db2inst1/sqllib/lib32/libcapro.so\" OPEN 0 SESSIONS  EXCLUDE LOGS WITHOUT PROMPTING"));
		logger.info("PASSED: back up database using VendorDDL executed successfully");
	}
*/	
	/**
	 * add by wtaobj@cn.ibm.com
	 * Test case of back up database using snapshot
	 * perform a full backup to the snapshot storage
	 * */
	@Test(description="Test the backup database using snapshot function")
	private void testBackupDBSnapshot(){
		logger.info("Running Back up database using snapshot option test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_BACKUP_DATABASE,"getBackupDBSnapshot_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CONNECT TO \"sample\""},
		 * {"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
		 * {"statement":"CONNECT RESET"},
		 * {"statement":"DEACTIVATE DATABASE \"sample\""},
		 * {"statement":"BACKUP DATABASE \"sample\" USE SNAPSHOT EXCLUDE LOGS WITHOUT PROMPTING"},
		 * {"statement":"CONNECT TO \"sample\""},
		 * {"statement":"UNQUIESCE DATABASE"},
		 * {"statement":"CONNECT RESET"}]}
		 * 
		 * To retrieve the statement, we are looking for 4
		 * $.items[0].statement
		 * */

		String ddl = JsonPath.read(responseData, "$.items[4].statement");
		//Assert.assertEquals(ddl,"BACKUP DATABASE \"sample\" USE SNAPSHOT EXCLUDE LOGS WITHOUT PROMPTING");
		Assert.assertTrue(ddl.startsWith("BACKUP DATABASE"));
		Assert.assertTrue(ddl.endsWith("USE SNAPSHOT EXCLUDE LOGS WITHOUT PROMPTING"));
		logger.info("PASSED: back up database using snapshot executed successfully");
	}
		
	/**
	 * add by wtaobj@cn.ibm.com
	 * Test case of back up database compress unquiesce and throttle this utility option
	 * perform a full compress backup with unquiesce and throttle option
	 * */
	@Test(description="Test the backup database using compress,unquiesce and throttle this utility function")
	private void testBackupDBOptions(){
		logger.info("Running Back up database using compress,unquiesce and throttle this utility option test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_BACKUP_DATABASE,"getBackupDBOptions_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"BACKUP DATABASE \"sample\" TO \"\/home\/db2inst1\" COMPRESS UTIL_IMPACT_PRIORITY 56 EXCLUDE LOGS WITHOUT PROMPTING"}]}
		 * 
		 * To retrieve the statement, we are looking for 0
		 * $.items[0].statement
		 * */

		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		//Assert.assertEquals(ddl,"BACKUP DATABASE \"sample\" TO \"/home/db2inst1\" COMPRESS UTIL_IMPACT_PRIORITY 56 EXCLUDE LOGS WITHOUT PROMPTING");
		Assert.assertTrue(ddl.startsWith("BACKUP DATABASE"));
		Assert.assertTrue(ddl.endsWith("TO \"/home/db2inst1\" COMPRESS UTIL_IMPACT_PRIORITY 56 EXCLUDE LOGS WITHOUT PROMPTING"));
		logger.info("PASSED: back up database compress,unquiesce and throttle this utility executed successfully");
	}
	
		
	/**
	 * add by wtaobj@cn.ibm.com
	 * Test case of back up database with performance options
	 * perform a full backup with manually performance tune and optimizing backup images for data deduplication devices
	 * */
	@Test(description="Test the backup database using performance options function")
	private void testBackupDBPerformance(){
		logger.info("Running Back up database using performance options test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_BACKUP_DATABASE,"getBackupDBPerformance_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"BACKUP DATABASE \"sample\" TO \"\/home\/db2inst1\" DEDUP_DEVICE WITH 20 BUFFERS BUFFER 20 PARALLELISM 10 EXCLUDE LOGS WITHOUT PROMPTING"}]}
		 * 
		 * To retrieve the statement, we are looking for 0
		 * $.items[0].statement
		 * */

		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		//Assert.assertEquals(ddl,"BACKUP DATABASE \"sample\" TO \"/home/db2inst1\" DEDUP_DEVICE WITH 20 BUFFERS BUFFER 20 PARALLELISM 10 EXCLUDE LOGS WITHOUT PROMPTING");
		Assert.assertTrue(ddl.startsWith("BACKUP DATABASE"));
		Assert.assertTrue(ddl.endsWith("TO \"/home/db2inst1\" DEDUP_DEVICE WITH 20 BUFFERS BUFFER 20 PARALLELISM 10 EXCLUDE LOGS WITHOUT PROMPTING"));
		logger.info("PASSED: back up database using performance executed successfully");
	}
	
	
	/**
	 * add by wtaobj@cn.ibm.com
	 * Test case of back up database using incremental
	 * perform a incremental backup
	 * */
	@Test(description="Test the backup database using incremental function")
	private void testBackupDBIncremental(){
		logger.info("Running Back up database using incremental option test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_BACKUP_DATABASE,"getBackupDBIncremental_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CONNECT TO TEST"},
		 * {"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
		 * {"statement":"CONNECT RESET"},
		 * {"statement":"DEACTIVATE DATABASE TEST"},
		 * {"statement":"BACKUP DATABASE TEST INCREMENTAL TO \"\/home\/db2inst1\" EXCLUDE LOGS WITHOUT PROMPTING"},
		 * {"statement":"CONNECT TO TEST"},
		 * {"statement":"UNQUIESCE DATABASE"},
		 * {"statement":"CONNECT RESET"}]}
		 * 
		 * To retrieve the statement, we are looking for 4
		 * $.items[0].statement
		 * */

		String ddl = JsonPath.read(responseData, "$.items[4].statement");
		//Assert.assertEquals(ddl,"BACKUP DATABASE \"sample\" USE SNAPSHOT EXCLUDE LOGS WITHOUT PROMPTING");
		Assert.assertTrue(ddl.startsWith("BACKUP DATABASE"));
		Assert.assertTrue(ddl.endsWith("INCREMENTAL TO \"/home/db2inst1\" EXCLUDE LOGS WITHOUT PROMPTING"));
		logger.info("PASSED: back up database using incremental option executed successfully");
	}
	
	/**
	 * add by wtaobj@cn.ibm.com
	 * Test case of back up database using delta
	 * perform a delta backup
	 * */
	@Test(description="Test the backup database using delta function")
	private void testBackupDBDelta(){
		logger.info("Running Back up database using delta option test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_BACKUP_DATABASE,"getBackupDBDelta_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CONNECT TO TEST"},
		 * {"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
		 * {"statement":"CONNECT RESET"},
		 * {"statement":"DEACTIVATE DATABASE TEST"},
		 * {"statement":"BACKUP DATABASE TEST INCREMENTAL DELTA TO \"\/home\/db2inst1\" EXCLUDE LOGS WITHOUT PROMPTING"},
		 * {"statement":"CONNECT TO TEST"},
		 * {"statement":"UNQUIESCE DATABASE"},
		 * {"statement":"CONNECT RESET"}]}
		 * 
		 * To retrieve the statement, we are looking for 4
		 * $.items[0].statement
		 * */

		String ddl = JsonPath.read(responseData, "$.items[4].statement");
		//Assert.assertEquals(ddl,"BACKUP DATABASE \"sample\" USE SNAPSHOT EXCLUDE LOGS WITHOUT PROMPTING");
		Assert.assertTrue(ddl.startsWith("BACKUP DATABASE"));
		Assert.assertTrue(ddl.endsWith("INCREMENTAL DELTA TO \"/home/db2inst1\" EXCLUDE LOGS WITHOUT PROMPTING"));
		logger.info("PASSED: back up database using delta option executed successfully");
	}
	
	/**
	 * Test case of back up database with encrypt option multi master key
	 * */
	
	@Test(description="Test the backup database with encrypt option multi master key function")
	private void testBackupDBWithEncryptOptionMultiMasterKey(){
		logger.info("Running Back up database with encrypt option multi master key test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_BACKUP_DATABASE,"getBackupDBWithEncryptOptionMultiMasterKey_LUW",false);
		
		/**
		 * Path the response data, for this case, the response data is:
		 *
		 * {"items":[{"statement":"CONNECT TO \"sample\""},
		 * {"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
		 * {"statement":"CONNECT RESET"},
		 * {"statement":"DEACTIVATE DATABASE \"sample\""},
		 * {"statement":"BACKUP DATABASE \"sample\" TO \"\/home\/db2inst3\" EXCLUDE LOGS WITHOUT PROMPTING"},
		 * {"statement":"CONNECT TO \"sample\""},
		 * {"statement":"UNQUIESCE DATABASE"},
		 * {"statement":"CONNECT RESET"}]}
		 * 
		 * To retrieve the statement, we are looking for 0,1,2,3,4
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertTrue(ddl.startsWith("CONNECT TO"));
		ddl = JsonPath.read(responseData, "$.items[1].statement");
		Assert.assertEquals(ddl, "QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS");
		ddl = JsonPath.read(responseData, "$.items[2].statement");
		Assert.assertEquals(ddl, "CONNECT RESET");
		ddl = JsonPath.read(responseData, "$.items[3].statement");
		Assert.assertTrue(ddl.startsWith("DEACTIVATE DATABASE"));
		ddl = JsonPath.read(responseData, "$.items[4].statement");
		Assert.assertTrue(ddl.startsWith("BACKUP DATABASE"));
		Assert.assertTrue(ddl.endsWith("EXCLUDE LOGS WITHOUT PROMPTING"));
		ddl = JsonPath.read(responseData, "$.items[5].statement");
		Assert.assertTrue(ddl.startsWith("CONNECT TO"));
		ddl = JsonPath.read(responseData, "$.items[6].statement");
		Assert.assertEquals(ddl, "UNQUIESCE DATABASE");
		ddl = JsonPath.read(responseData, "$.items[7].statement");
		Assert.assertEquals(ddl, "CONNECT RESET");
		
		logger.info("PASSED: back up database with encrypt optionM multi master key executed successfully");
	}
	

	/**
	 * Modify by wtaobj@cn.ibm.com
	 * Test case of createDB
	 */
	
	 @Test(description="Test the createDB function")
	
	  private void testCreateDatabase(){
	 
		
		
		logger.info("Running createDB test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_CREATE_DATABASE,"getCreateDatabase_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *
		 *{"items":[{"statement":"CREATE DATABASE \"mydb\" AUTOMATIC STORAGE YES "}]}
		 * */

		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertEquals(ddl,"CREATE DATABASE \"mydb\" AUTOMATIC STORAGE YES ");
		
		logger.info("PASSED: createDB executed successfully");
	}

	
	/**
	 * add by wtaobj@cn.ibm.com
	 * Test case of creating a new database 
 	 * Label of Detail, 1 specifying name,location,alias and comment; 2 restrict access to system catalog; 3 customize page size, extent size and number of segments
	 * */
	@Test(description="Test the createDB detail options function")
	
	private void testCreateDatabaseDetail(){
		
		
		logger.info("Running createDB detail options test...");
		
		JSONObject responseData=getResponseData(AdminTAService.TA_CREATE_DATABASE,"getCreateDatabaseDetail_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *
		 {"items":[{"statement":"CREATE DATABASE \"mydb\" 
		 AUTOMATIC STORAGE YES 
		 ON '\/home\\\/db2inst1\\\/mydb' 
		 DBPATH ON '\/home\\\/db2inst1\\\/mydb' 
		 ALIAS mydbalias 
		 PAGESIZE 8 K NUMSEGS 50 DFT_EXTENT_SZ 100 
		 RESTRICTIVE  
		 WITH 'that's a test6' "}]}
		 * 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertEquals(ddl,"CREATE DATABASE \"mydb\" AUTOMATIC STORAGE YES ON '/home/db2inst1/mydb' DBPATH ON '/home/db2inst1/mydb' ALIAS mydbalias PAGESIZE 8 K NUMSEGS 50 DFT_EXTENT_SZ 100 RESTRICTIVE  WITH 'that's a test' ");
		logger.info("PASSED: createDB detail options executed successfully");
		
	}
	
	/**
	 * add by wtaobj@cn.ibm.com
	 * Test case of creating a new database 
	 * Label of Locale, 4 specify  Database locale; 5 specify sort character string and language sensitive locale
	**/
	
	@Test(description="Test the createDB locale options function")
	
	private void testCreateDatabaseLocale(){
		
		
		logger.info("Running createDB locale options test...");
		
		JSONObject responseData=getResponseData(AdminTAService.TA_CREATE_DATABASE,"getCreateDatabaseLocale_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *
		 {"items":[{"statement":"CREATE DATABASE \"mydb\" 
		 AUTOMATIC STORAGE YES 
		 USING CODESET ISO-8859-1 
		 TERRITORY AT COLLATE 
		 USING IDENTITY "}]}
		 * 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertEquals(ddl,"CREATE DATABASE \"mydb\" AUTOMATIC STORAGE YES USING CODESET ISO-8859-1 TERRITORY AT COLLATE USING IDENTITY ");
		logger.info("PASSED: createDB locale options executed successfully");
		
	}
	
	/**
	 * add by wtaobj@cn.ibm.com
	 * Test case of creating a new database 
	 * Label of Storage,6 disable manage storage automatically with DB2 and specify uts,cts and tts direcotory.
	**/
	
	@Test(description="Test the createDB Storage options function")
	
	private void testCreateDatabaseStorage(){
		
		
		logger.info("Running createDB Storage options test...");
		
		JSONObject responseData=getResponseData(AdminTAService.TA_CREATE_DATABASE,"getCreateDatabaseStorage_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *
		 {"items":[{"statement":"CREATE DATABASE \"mydb\" 
		 AUTOMATIC STORAGE NO  
		 USER TABLESPACE MANAGED BY SYSTEM USING ( '\/home\/db2inst1' ) NO FILE SYSTEM CACHING  
		 CATALOG TABLESPACE MANAGED BY SYSTEM USING ( '\/home\/db2inst1' ) NO FILE SYSTEM CACHING  
		 TEMPORARY TABLESPACE MANAGED BY SYSTEM USING ( '\/home\/db2inst1' ) NO FILE SYSTEM CACHING "}]}
		 * 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertEquals(ddl,"CREATE DATABASE \"mydb\" AUTOMATIC STORAGE NO  USER TABLESPACE MANAGED BY SYSTEM USING ( '/home/db2inst1' ) NO FILE SYSTEM CACHING  CATALOG TABLESPACE MANAGED BY SYSTEM USING ( '/home/db2inst1' ) NO FILE SYSTEM CACHING  TEMPORARY TABLESPACE MANAGED BY SYSTEM USING ( '/home/db2inst1' ) NO FILE SYSTEM CACHING ");
		logger.info("PASSED: createDB Storage options executed successfully");
		
	}
	
	/**
	 * add by wtaobj@cn.ibm.com
	 * Test case of creating a new database 
	 * Label of User Table Space,database-managed space,table space autoresize,with increase size and Maximun size,table space performance settings
	**/
	
@Test(description="Test the createDB UTS options function")
	
	private void testCreateDatabaseUTS(){
		
		
		logger.info("Running createDB UTS options test...");
		
		JSONObject responseData=getResponseData(AdminTAService.TA_CREATE_DATABASE,"getCreateDatabaseUTS_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *
		 {"items":[{"statement":"CREATE DATABASE \"mydb\" 
		 AUTOMATIC STORAGE YES  
		 USER TABLESPACE MANAGED BY DATABASE USING ( FILE '\/home\/db2inst1' 5120, FILE '\/home\/db2inst122' 5120) 
		 EXTENTSIZE 50 PREFETCHSIZE 1000 OVERHEAD 4000.0 TRANSFERRATE 50000.0 
		 AUTORESIZE YES  
		 INCREASESIZE 20 M 
		 MAXSIZE 30 M"}]}
		 * 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertEquals(ddl,"CREATE DATABASE \"mydb\" AUTOMATIC STORAGE YES  USER TABLESPACE MANAGED BY DATABASE USING ( FILE '/home/db2inst1' 5120, FILE '/home/db2inst122' 5120) EXTENTSIZE 50 PREFETCHSIZE 1000 OVERHEAD 4000.0 TRANSFERRATE 50000.0 AUTORESIZE YES  INCREASESIZE 20 M MAXSIZE 30 M");
		
		logger.info("PASSED: createDB UTS options executed successfully");
		
	}
	
	/**
	 * add by wtaobj@cn.ibm.com
	 * Test case of creating a new database 
	 * Label of Catalog Table Space
	 **/

@Test(description="Test the createDB CTS options function")

	private void testCreateDatabaseCTS(){
	
	
	logger.info("Running createDB CTS options test...");
	JSONObject responseData=getResponseData(AdminTAService.TA_CREATE_DATABASE,"getCreateDatabaseCTS_LUW",true);
	
	/**
	 * Path the response data, for this case, the response data is:
	 *
	 {"items":[{"statement":"CREATE DATABASE \"mydb\" 
	 AUTOMATIC STORAGE YES  
	 CATALOG TABLESPACE MANAGED BY SYSTEM USING ( '\/home\/db2inst1' ) 
	 FILE SYSTEM CACHING "}]}
	 * $.items[0].statement
	 * */
	String ddl = JsonPath.read(responseData, "$.items[0].statement");
	Assert.assertEquals(ddl,"CREATE DATABASE \"mydb\" AUTOMATIC STORAGE YES  CATALOG TABLESPACE MANAGED BY SYSTEM USING ( '/home/db2inst1' ) FILE SYSTEM CACHING " );

	logger.info("PASSED: createDB CTS options executed successfully");
	
}
		
		/**
		 * add by wtaobj@cn.ibm.com
		 * Test case of creating a new database 
		 * Label of Native Encryption
		 **/
		
		@Test(description="Test the createDB Native Encrytion function")
		
		private void testCreateDatabaseNativeEncrytion(){
		
		
		logger.info("Running createDB Native Encrytion options test...");
		
		JSONObject responseData=getResponseData(AdminTAService.TA_CREATE_DATABASE,"getCreateDatabaseNativeEncrytion_LUW",true);
		
		/**
		 * Path the response data, for this case, the response data is:
		 *
		 {"items":[{"statement":"CREATE DATABASE \"endb\" 
		 AUTOMATIC STORAGE YES  
		 ENCRYPT 
		 CIPHER AES 
		 KEY LENGTH 256 
		 MASTER KEY LABEL Mydb2pscLabel "}]}
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		 Assert.assertTrue(ddl.startsWith("CREATE DATABASE "));
		 Assert.assertTrue(ddl.endsWith("AUTOMATIC STORAGE YES  ENCRYPT CIPHER AES KEY LENGTH 256 MASTER KEY LABEL Mydb2pscLabel "));
		logger.info("PASSED: createDB Native Encrytion options executed successfully");
		
		}



		/**
		 * Test case of restoreDB with encrypt option
		 * */
		@Test(description="Test the restoreDB with encrypt option function")
		private void testRestoreDBWithEncryptOption(){
			if(this.version != null){
				if(this.compareToVersion(this.version,"V10.5.5") < 0 ){
					return;
				}
			}
			logger.info("Running restoreDB with encrypt option test...");

			JSONObject responseData=getResponseData(AdminTAService.TA_RESTORE_COMMAND,"getRestoreDBWithEncryptOption_LUW",false);

			/**
			 * Path the response data, for this case, the response data is:
			 *
			 * {"items":[{"statement":"RESTORE DATABASE \"sample\" FROM \"\/home\/db2inst1\" TAKEN AT 20141212132348 WITHOUT PROMPTING"}]}
			 * 
			 * $.items[0].statement
			 * */
			String ddl = JsonPath.read(responseData, "$.items[0].statement");

			Assert.assertTrue(ddl.startsWith("RESTORE DATABASE"));
			Assert.assertTrue(ddl.endsWith("TAKEN AT 20141212132348 ENCRLIB '/home/db2inst1/sqllib/lib/libdb2encr.so' ENCROPTS 'CIPHER=AES:KEY LENGTH=192:MASTER KEY LABEL=mk1' WITHOUT PROMPTING"));

			logger.info("PASSED: restoreDB with encrypt option executed successfully");
		} 
		
		
	
	/**
	 * Test case of restore encrypt image to new none encrypt DB
	 * */
	@Test(description="Test the restore encrypt image to new none encrypt DB function")
	private void testRestoreEncryptImageToNewNoneEncryptDB(){
		logger.info("Running restore encrypt image to new none encrypt DB test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_RESTORE_COMMAND,"getRestoreEncryptImageToNewNoneEncryptDB_LUW",false);
		
		/**
		 * Path the response data, for this case, the response data is:
		 *
		 *{"items":[{"statement":"RESTORE DATABASE \"sample\" FROM \"\/home\/db2inst1\" TAKEN AT 20141212132348 INTO NONENCRP WITHOUT PROMPTING"}]}		 * 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertTrue(ddl.startsWith("RESTORE DATABASE"));
		Assert.assertTrue(ddl.endsWith("TAKEN AT 20141212132348 INTO NONENCRP WITHOUT PROMPTING"));
		
		logger.info("PASSED: restore encrypt image to new none encrypt DB executed successfully");
	}
	
	/**
	 * Test case of restore encrypt image to new encrypt DB
	 * */
	@Test(description="Test the restore encrypt image to new encrypt DB function")
	private void testRestoreEncryptImageToNewEncryptDB(){
		logger.info("Running restore encrypt image to new encrypt DB test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_RESTORE_COMMAND,"getRestoreEncryptImageToNewEncryptDB_LUW",false);
		
		/**
		 * Path the response data, for this case, the response data is:
		 *
		 *{"items":[{"statement":"RESTORE DATABASE \"sample\" FROM \"\/home\/db2inst1\" TAKEN AT 20141212132348 INTO ENCRYPT WITHOUT PROMPTING"}]}
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertTrue(ddl.startsWith("RESTORE DATABASE"));
		Assert.assertTrue(ddl.endsWith("TAKEN AT 20141212132348 INTO ENCRYPT WITHOUT PROMPTING"));
		
		logger.info("PASSED: restore encrypt image to new encrypt DB executed successfully");
	}
	
	/**
	 * Test case of restore encrypt image with encrypt option to new encrypt DB
	 * */
	@Test(description="Test the restore encrypt image with encrypt option to new encrypt DB function")
	private void testRestoreEncryptImageWithEncryptOptionToNewEncryptDB(){
		logger.info("Running restore encrypt image with encrypt option to new encrypt DB test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_RESTORE_COMMAND,"getRestoreEncryptImageWithEncryptOptionToNewEncryptDB_LUW",false);
		
		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"RESTORE DATABASE \"sample\" FROM \"\/home\/db2inst1\" TAKEN AT 20141212132348 INTO ENCRP WITHOUT PROMPTING"}]}
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertTrue(ddl.startsWith("RESTORE DATABASE"));
		Assert.assertTrue(ddl.endsWith("TAKEN AT 20141212132348 INTO ENCRP WITHOUT PROMPTING"));
		
		logger.info("PASSED: restore encrypt image with encrypt option to new encrypt DB executed successfully");
	}
	
	/**
	 * Add by wtaobj@cn.ibm.com
	 * Test case of restore history to DB
	 * */
	@Test(description="Test the restore history to DB function")
	private void testRestoreDBHistory(){
		logger.info("Running restore history to DB test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_RESTORE_COMMAND,"getRestoreDBHistory_LUW",false);
		
		/**
		 * Path the response data, for this case, the response data is:
		 *
		 *{"items":[{"statement":"RESTORE DATABASE \"sample\" HISTORY FILE FROM \"\/home\/db2inst1\" TAKEN AT 20160712160308 WITHOUT PROMPTING"}]}		 * 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		//Assert.assertEquals(ddl,"RESTORE DATABASE \"sample\" HISTORY FILE FROM \"/home/db2inst1\" TAKEN AT 20160712160308 WITHOUT PROMPTING");
		Assert.assertTrue(ddl.startsWith("RESTORE DATABASE"));
		Assert.assertTrue(ddl.endsWith("HISTORY FILE FROM \"/home/db2inst1\" TAKEN AT 20160712160308 WITHOUT PROMPTING"));
		logger.info("PASSED: restore DB history and connection options executed successfully");
	}
	
	/**
	 * Add by wtaobj@cn.ibm.com
	 * Test case of restore DB and redirect tablespace
	 * */
	@Test(description="Test restore DB and redirect tablespace function")
	private void testRestoreDBRedirectTS(){
		logger.info("Running restore DB and redirect tablespace test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_RESTORE_COMMAND,"getRestoreDBRedirectTS_LUW",false);
		
		/**
		 * Path the response data, for this case, the response data is:
		 *
		 * {"items":[{"statement":"RESTORE DATABASE \"db4tsred\" FROM \"\/home\/db2inst1\" TAKEN AT 20160729074354 REDIRECT WITHOUT PROMPTING"},
		 *           {"statement":"SET TABLESPACE CONTAINERS FOR 2 REPLAY ROLLFORWARD CONTAINER OPERATIONS USING ( FILE \"\/home\/db2inst1\/db4tsred\/newuts\" 2000 )"},
		 *           {"statement":"RESTORE DATABASE \"db4tsred\" CONTINUE"}]}	 
		 * $.items[0].statement
		 * $.items[1].statement
		 * $.items[2].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		//Assert.assertEquals(ddl,"RESTORE DATABASE \"sample\" HISTORY FILE FROM \"/home/db2inst1\" TAKEN AT 20160712160308 WITHOUT PROMPTING");
		Assert.assertTrue(ddl.startsWith("RESTORE DATABASE"));
		Assert.assertTrue(ddl.endsWith("FROM \"/home/db2inst1\" TAKEN AT 20160729074354 REDIRECT WITHOUT PROMPTING"));
		ddl = JsonPath.read(responseData, "$.items[1].statement");
		Assert.assertEquals(ddl,"SET TABLESPACE CONTAINERS FOR 2 REPLAY ROLLFORWARD CONTAINER OPERATIONS USING ( FILE \"/home/db2inst1/db4tsred/newuts\" 2000 )");
		ddl = JsonPath.read(responseData, "$.items[2].statement");
		Assert.assertTrue(ddl.startsWith("RESTORE DATABASE"));
		Assert.assertTrue(ddl.endsWith("CONTINUE"));
		logger.info("PASSED: restore DB and redirect tablespace executed successfully");
	}
	
	/**
	 * Add by wtaobj@cn.ibm.com
	 * Test case of restore history and connection options
	 * */
	@Test(description="Test the restore history and connection options function")
	private void testRestoreDBHistoryConnections(){
		logger.info("Running restore history and connection options test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_RESTORE_COMMAND,"getRestoreDBHistoryConnections_LUW",false);
		
		/**
		 * Path the response data, for this case, the response data is:
		 *
		 *{"items":[{"statement":"CONNECT TO \"sample\""},
		 *          {"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
		 *          {"statement":"UNQUIESCE DATABASE"},
		 *          {"statement":"TERMINATE"},
		 *          {"statement":"DEACTIVATE DATABASE \"sample\""},
		 *          {"statement":"RESTORE DATABASE \"sample\" FROM \"\/home\/db2inst1\" TAKEN AT 20160712160308 LOGTARGET \"\/home\/db2inst1\/mydb\" NEWLOGPATH \"\/home\/db2inst1\/mydb\" REPLACE HISTORY FILE WITHOUT PROMPTING"}]}
		 * 
		 * To retrieve the statement, we are looking for 0,1,2,3,4
		 * $.items[0].statement 
		 * $.items[1].statement
		 * $.items[2].statement
		 * $.items[3].statement
		 * $.items[4].statement
		 * $.items[5].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
			Assert.assertTrue(ddl.startsWith("CONNECT TO"));
			ddl = JsonPath.read(responseData, "$.items[1].statement");
			Assert.assertEquals(ddl,"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS");
			ddl = JsonPath.read(responseData, "$.items[2].statement");
			Assert.assertEquals(ddl,"UNQUIESCE DATABASE");
			ddl = JsonPath.read(responseData, "$.items[3].statement");
			Assert.assertEquals(ddl,"TERMINATE");
			ddl = JsonPath.read(responseData, "$.items[4].statement");
			Assert.assertTrue(ddl.startsWith("DEACTIVATE DATABASE"));
			ddl = JsonPath.read(responseData, "$.items[5].statement");		
			Assert.assertTrue(ddl.startsWith("RESTORE DATABASE"));
			Assert.assertTrue(ddl.endsWith("FROM \"/home/db2inst1\" TAKEN AT 20160712160308 LOGTARGET \"/home/db2inst1/mydb\" NEWLOGPATH \"/home/db2inst1/mydb\" REPLACE HISTORY FILE WITHOUT PROMPTING")); 
		logger.info("PASSED: restore encrypt image to new none encrypt DB executed successfully");
	}
	
	/**
	 * Test case of start native encrypt instance
	 * */
	@Test(description="Test the start native encrypt instance function")
	private void testStartNativeEncryptInstance(){
		logger.info("Running start native encrypt instance test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_START_INSTANCES_COMMAND,"getStartNativeEncryptInstance_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"!db2start ADMIN MODE RESTRICTED ACCESS"}]} 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertEquals(ddl, "!db2start ADMIN MODE RESTRICTED ACCESS OPEN KEYSTORE USING Str0ngPassw0rd");
		
		logger.info("PASSED: start native encrypt instance executed successfully");
	}
	
	/**
	 * Add by wtaobj@cn.ibm.com.
	 * Test case of start instance admin mode and user.
	 * */
	@Test(description="Test case of start instance admin mode and user function.")
	private void testStartInstanceAdminModeAndUser(){
		logger.info("Running start instance admin mode and user...");

		JSONObject responseData=getResponseData(AdminTAService.TA_START_INSTANCES_COMMAND,"getStartInstanceAdminModeAndUser_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"!db2start ADMIN MODE USER \"username\""}]} 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertTrue(ddl.startsWith("!db2start ADMIN MODE USER \"username\""));
		
		logger.info("PASSED: start instance admin mode and user executed successfully");
	}
	
	/**
	 * Add by wtaobj@cn.ibm.com.
	 * Test case of start instance admin mode and group.
	 * */
	@Test(description="Test case of start instance admin mode and group function.")
	private void testStartInstanceAdminModeAndGroup(){
		logger.info("Running start instance admin mode and group...");

		JSONObject responseData=getResponseData(AdminTAService.TA_START_INSTANCES_COMMAND,"getStartInstanceAdminModeAndGroup_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"!db2start ADMIN MODE GROUP \"groupname\""}]} 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertTrue(ddl.startsWith("!db2start ADMIN MODE GROUP \"groupname\""));
		
		logger.info("PASSED: start instance admin mode and group executed successfully");
	}
		
	/**
	 * Modify by wtao@cn.ibm.com
	 * Test case of start native encrypt instance no admin mode using password
	 * */
	@Test(description="Test the start native encrypt instance no admin mode using password function")
	private void testStartNativeEncryptInstanceNoAdminModePW(){
		logger.info("Running start native encrypt instance no admin mode using password test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_START_INSTANCES_COMMAND,"getStartNativeEncryptInstanceNoAdminModePW_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"!db2start OPEN KEYSTORE USING ********"}]} 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertEquals(ddl, "!db2start OPEN KEYSTORE USING ********");
		
		logger.info("PASSED: start native encrypt instance no admin mode using password executed successfully");
	}
	
	/**
	 * Test case of start native encrypt key storeFD  instance
	 * */
	@Test(description="Test the start native encrypt key storeFD instance function")
	private void testStartNativeEncryptKeyStoreFDInstance(){
		logger.info("Running start native encrypt key storeFD instance test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_START_INSTANCES_COMMAND,"getStartNativeEncryptKeyStoreFDInstance_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"!db2start ADMIN MODE RESTRICTED ACCESS"}]} 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertTrue(ddl.indexOf("!db2start ADMIN MODE RESTRICTED ACCESS OPEN KEYSTORE PASSARG") >= 0);
		
		logger.info("PASSED: start native encrypt key storeFD instance executed successfully");
	}
	
	/**
	 * Modify by wtaobj@cn.ibm.com
	 * Test case of start native encrypt key storeFD instance no admin mode using filename
	 * */
	@Test(description="Test the start native encrypt key storeFD instance no admin mode using filename function")
	private void testStartNativeEncryptKeyStoreFDInstanceNoAdminModeFN(){
		logger.info("Running start native encrypt key storeFD instance no admin mode using filename test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_START_INSTANCES_COMMAND,"getStartNativeEncryptKeyStoreFDInstanceNoAdminModeFN_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"!db2start OPEN KEYSTORE PASSARG filename:/home/db2inst1/key.sth"}]} 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertEquals(ddl, "!db2start OPEN KEYSTORE PASSARG filename:/home/db2inst1/key.sth");
		
		logger.info("PASSED: start native encrypt key storeFD instance no admin mode using filename executed successfully");
	}
	
	/**
	 * Test case of start native encrypt key storeF name instance
	 * */
	@Test(description="Test the start native encrypt key storeF name instance function")
	private void testStartNativeEncryptKeyStoreFNameInstance(){
		logger.info("Running start native encrypt key storeF name instance test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_START_INSTANCES_COMMAND,"getStartNativeEncryptKeyStoreFNameInstance_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"!db2start ADMIN MODE RESTRICTED ACCESS"}]} 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertTrue(ddl.indexOf("!db2start ADMIN MODE RESTRICTED ACCESS OPEN KEYSTORE PASSARG") >= 0);
		
		logger.info("PASSED: start native encrypt key storeF name instance executed successfully");
	}
	
	/**
	 * Add by wtaobj@cn.ibm.com.
	 * Test case of start instance no admin mode
	 * */
	@Test(description="Test the start instance no admin mode function")
	private void testStartInstanceNoAdminMode(){
		logger.info("Running start instance no admin mode test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_START_INSTANCES_COMMAND,"getStartInstanceNoAdminMode_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"!db2start"}]} 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertEquals(ddl, "!db2start");
		
		logger.info("PASSED: start instance no admin mode executed successfully");
	}
	
	/**
	 * Test case of stop instance mode
	 * */
	@Test(description="Test the stop instance function")
	private void testStopInstance(){
		logger.info("Running stop instance  test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_STOP_INSTANCES_COMMAND,"getStopInstance_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"db2stop FORCE"}]} 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertEquals(ddl, "db2stop FORCE");
		
		logger.info("PASSED: stop instance executed successfully");
	}
	
	/**
	 * Add by wtaobj@cn.ibm.com.
	 * Test case of stop instance with no options
	 * */
	@Test(description="Test the stop instance with no options function")
	private void testStopInstanceNoOptions(){
		logger.info("Running stop instance with no options test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_STOP_INSTANCES_COMMAND,"getStopInstanceNoOpions_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"!db2stop"}]} 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertEquals(ddl, "db2stop");
		
		logger.info("PASSED: stop instance with no options executed successfully");
	}
	
	/**
	 * Test case of quiesce instance roll back no one mode
	 * */
	@Test(description="Test the quiesce instance roll back no one function")
	private void testQuiesceInstanceRollBackNoOne(){
		logger.info("Running stop instance roll back no one test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_QUIESCE_INSTANCES_COMMAND,"getQuiesceInstanceRollBackNoOne_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"QUIESCE INSTANCE \"db2inst1\" RESTRICTED ACCESS IMMEDIATE FORCE CONNECTIONS"}]} 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertTrue(ddl.startsWith("QUIESCE INSTANCE"));
		Assert.assertTrue(ddl.endsWith("RESTRICTED ACCESS IMMEDIATE FORCE CONNECTIONS"));

		logger.info("PASSED: quiesce instance roll back no one executed successfully");
	}
	/**
	 * Test case of quiesce instance roll back user mode
	 * */
	@Test(description="Test the quiesce instance roll back user function")
	private void testQuiesceInstanceRollBackUser(){
		logger.info("Running stop instance roll back user test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_QUIESCE_INSTANCES_COMMAND,"getQuiesceInstanceRollBackUser_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"QUIESCE INSTANCE \"db2inst1\" USER \"db2inst1\" RESTRICTED ACCESS IMMEDIATE FORCE CONNECTIONS"}]} 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertTrue(ddl.startsWith("QUIESCE INSTANCE"));
		Assert.assertTrue(ddl.contains("USER"));
		Assert.assertTrue(ddl.endsWith("RESTRICTED ACCESS IMMEDIATE FORCE CONNECTIONS"));

		logger.info("PASSED: quiesce instance roll back user executed successfully");
	}
	/**
	 * Test case of quiesce instance roll back group mode
	 * */
	@Test(description="Test the quiesce instance roll back group function")
	private void testQuiesceInstanceRollBackGroup(){
		logger.info("Running stop instance roll back group test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_QUIESCE_INSTANCES_COMMAND,"getQuiesceInstanceRollBackGroup_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"QUIESCE INSTANCE \"db2inst1\" GROUP \"group1\" RESTRICTED ACCESS IMMEDIATE FORCE CONNECTIONS"}]} 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertTrue(ddl.startsWith("QUIESCE INSTANCE"));
		Assert.assertTrue(ddl.contains("GROUP \"group1\""));
		Assert.assertTrue(ddl.endsWith("RESTRICTED ACCESS IMMEDIATE FORCE CONNECTIONS"));

		logger.info("PASSED: quiesce instance roll back group executed successfully");
	}
	/**
	 * Test case of quiesce instance defer no one mode
	 * */
	@Test(description="Test the quiesce instance defer no one function")
	private void testQuiesceInstanceDeferNoOne(){
		logger.info("Running stop instance defer no one test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_QUIESCE_INSTANCES_COMMAND,"getQuiesceInstanceDeferNoOne_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"QUIESCE INSTANCE \"db2inst1\" RESTRICTED ACCESS DEFER WITH TIMEOUT 3 FORCE CONNECTIONS"}]} 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertTrue(ddl.startsWith("QUIESCE INSTANCE"));
		Assert.assertTrue(ddl.endsWith("RESTRICTED ACCESS DEFER WITH TIMEOUT 3 FORCE CONNECTIONS"));

		logger.info("PASSED: quiesce instance defer no one executed successfully");
	}
	/**
	 * Test case of quiesce instance defer user mode
	 * */
	@Test(description="Test the quiesce instance defer user function")
	private void testQuiesceInstanceDeferUser(){
		logger.info("Running stop instance defer user test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_QUIESCE_INSTANCES_COMMAND,"getQuiesceInstanceDeferUser_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"QUIESCE INSTANCE "db2inst1" USER "db2inst1" RESTRICTED ACCESS DEFER WITH TIMEOUT 3 FORCE CONNECTIONS"}]} 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertTrue(ddl.startsWith("QUIESCE INSTANCE"));
		Assert.assertTrue(ddl.contains("USER \"db2inst1\""));
		Assert.assertTrue(ddl.endsWith("RESTRICTED ACCESS DEFER WITH TIMEOUT 3 FORCE CONNECTIONS"));

		logger.info("PASSED: quiesce instance defer user executed successfully");
	}
	/**
	 * Test case of quiesce instance defer group mode
	 * */
	@Test(description="Test the quiesce instance defer group function")
	private void testQuiesceInstanceDeferGroup(){
		logger.info("Running stop instance defer group test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_QUIESCE_INSTANCES_COMMAND,"getQuiesceInstanceDeferGroup_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"QUIESCE INSTANCE \"db2inst1\" GROUP \"group1\" RESTRICTED ACCESS IMMEDIATE FORCE CONNECTIONS"}]} 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertTrue(ddl.startsWith("QUIESCE INSTANCE"));
		Assert.assertTrue(ddl.contains("GROUP \"group1\""));
		Assert.assertTrue(ddl.endsWith("RESTRICTED ACCESS DEFER WITH TIMEOUT 3 FORCE CONNECTIONS"));

		logger.info("PASSED: quiesce instance defer group executed successfully");
	}
	
	/**
	 * Test case of unquiesce instance mode
	 * */
	@Test(description="Test the unquiesce instance function")
	private void testUnQuiesceInstance(){
		logger.info("Running stop instance  test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_UNQUIESCE_INSTANCES_COMMAND,"getUnQuiesceInstance_LUW",true);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"UNQUIESCE INSTANCE \"db2inst1\""}]} 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertTrue(ddl.startsWith("UNQUIESCE INSTANCE"));
		
		logger.info("PASSED: unquiesce instance executed successfully");
	}
	
	/**
	 * Add by wtaobj@cn.ibm.com
	 * Test case of quiesce database
	 * */
	@Test(description="Test the quiesce database function")
	private void testQuiesceDatabase(){
		logger.info("Running quiesce database test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_QUIESCE_DATABASE_COMMAND,"getQuiesceDatabase_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"CALL SYSPROC.ADMIN_CMD( 'QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS' )"}]}
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertEquals(ddl,"CALL SYSPROC.ADMIN_CMD( 'QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS' )");
		
		logger.info("PASSED: quiesce database executed successfully");
	}
	
	
	
	/**
	 * Add by wtaobj@cn.ibm.com
	 * Test case of unquiesce database
	 * */
	@Test(description="Test the unquiesce database  function")
	private void testUnquiesceDatabase(){
		logger.info("Running quiesce database  test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_UNQUIESCE_DATABASE_COMMAND,"getUnquiesceDatabase_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"CALL SYSPROC.ADMIN_CMD( 'UNQUIESCE DATABASE' )"}]}
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertEquals(ddl,"CALL SYSPROC.ADMIN_CMD( 'UNQUIESCE DATABASE' )");
		
		logger.info("PASSED: unquiesce database executed successfully");
	}
	
	/**
	 * Test compress table TA
	 * */
	@Test(description="Test the compress table function")
	public void testCompressTable() throws InterruptedException{
		
		logger.info("Running Compress Table TA test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_COMPRESS_TABLE,"getCompressTable_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CONNECT TO \"sample\""}
		 * 			,{"statement":"ALTER TABLE DB2INST1.ACT COMPRESS YES ADAPTIVE ACTIVATE VALUE COMPRESSION"}
		 * 			,{"statement":"REORG TABLE DB2INST1.ACT"}
		 * 			,{"statement":"CONNECT RESET"}]}
		 * 
		 * To retrieve the statement, we are looking for the 0,1,2 index of the items array -> the return is a object, we look for the value of key "statement", that is
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		Assert.assertTrue(ddl.startsWith("CONNECT TO"));//CONNECT TO \"sample\"
		ddl = JsonPath.read(responseData, "$.items[1].statement");
		Assert.assertEquals(ddl, "ALTER TABLE DB2INST1.ACT COMPRESS YES ADAPTIVE ACTIVATE VALUE COMPRESSION");
		ddl = JsonPath.read(responseData, "$.items[2].statement");
		Assert.assertEquals(ddl, "REORG TABLE DB2INST1.ACT");
		ddl = JsonPath.read(responseData, "$.items[3].statement");
		Assert.assertEquals(ddl, "CONNECT RESET");
		
		logger.info("PASSED: Compress table TA executed successfully");
		
	}
	/**
	 * Test activate database TA
	 * */
	@Test(description="Test the activate database function")
	public void testActivateDatabase() throws InterruptedException{
		
		logger.info("Running activate database TA test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_ACTIVATE_DATABASE_COMMAND,"getActivateDatabase_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"ACTIVATE DATABASE \"sample\""}]}
		 * 
		 * To retrieve the statement, we are looking for the 0,1,2 index of the items array -> the return is a object, we look for the value of key "statement", that is
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		Assert.assertTrue(ddl.startsWith("ACTIVATE DATABASE"));
		
		logger.info("PASSED: activate database TA executed successfully");
		
	}
	/**
	 * Test deactivate database TA
	 * */
	@Test(description="Test the deactivate database function")
	public void testDeactivateDatabase() throws InterruptedException{
		
		logger.info("Running activate dedatabase TA test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_DEACTIVATE_DATABASE_COMMAND,"getDeactivateDatabase_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"DEACTIVATE DATABASE \"sample\""}]}
		 * 
		 * To retrieve the statement, we are looking for the 0,1,2 index of the items array -> the return is a object, we look for the value of key "statement", that is
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		Assert.assertTrue(ddl.startsWith("DEACTIVATE DATABASE"));
		
		logger.info("PASSED: deactivate database TA executed successfully");
		
	}
	/**
	 * Test recovery database end of log on default dir TA
	 * */
	@Test(description="Test the recovery database end of log on default dir function")
	public void testRecoveryDBEndOfLogDefaultDir() throws InterruptedException{
		
		logger.info("Running recovery database end of log on default dir TA test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_RECOVERY_DATABASE_COMMAND,"getRecoveryDBEndOfLogDefaultDir_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"CONNECT TO \"sample\""},
		 *{"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
		 *{"statement":"UNQUIESCE DATABASE"},
		 *{"statement":"TERMINATE"},
		 *{"statement":"DEACTIVATE DATABASE \"sample\""},
		 *{"statement":"RECOVER DATABASE \"sample\" TO END OF LOGS USING HISTORY FILE ( \"\/home\/db2inst1\/sqllib\/backup\/\" )"}]}
		 * 
		 * To retrieve the statement, we are looking for the 0,1,2 index of the items array -> the return is a object, we look for the value of key "statement", that is
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		Assert.assertTrue(ddl.startsWith("CONNECT TO"));
		ddl = JsonPath.read(responseData, "$.items[1].statement");
		Assert.assertEquals(ddl, "QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS");
		ddl = JsonPath.read(responseData, "$.items[2].statement");
		Assert.assertEquals(ddl, "UNQUIESCE DATABASE");
		ddl = JsonPath.read(responseData, "$.items[3].statement");
		Assert.assertEquals(ddl, "TERMINATE");
		ddl = JsonPath.read(responseData, "$.items[4].statement");
		Assert.assertTrue(ddl.startsWith("DEACTIVATE DATABASE"));
		ddl = JsonPath.read(responseData, "$.items[5].statement");
		Assert.assertTrue(ddl.startsWith("RECOVER DATABASE"));
		Assert.assertTrue(ddl.contains("TO END OF LOGS USING HISTORY FILE"));
		
		logger.info("PASSED: recovery database end of log on default dir TA executed successfully");
		
	}
	/**
	 * Test recovery database end of log on specify dir TA
	 * */
	@Test(description="Test the recovery database end of log on specify dir function")
	public void testRecoveryDBEndOfLogSpecifyDir() throws InterruptedException{
		
		logger.info("Running recovery database end of log on specify dir TA test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_RECOVERY_DATABASE_COMMAND,"getRecoveryDBEndOfLogSpecifyDir_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CONNECT TO \"sample\""},
		 * {"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
		 * {"statement":"UNQUIESCE DATABASE"},
		 * {"statement":"TERMINATE"},
		 * {"statement":"DEACTIVATE DATABASE \"sample\""},
		 * {"statement":"RECOVER DATABASE \"sample\" TO END OF LOGS USING HISTORY FILE ( \"\/home\/db2inst1\/sqllib\/backup\/\" ) OVERFLOW LOG PATH ( \"\/home\/db2inst1\/sqllib\/backup\" )"}]}
		 * 
		 * To retrieve the statement, we are looking for the 0,1,2 index of the items array -> the return is a object, we look for the value of key "statement", that is
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		Assert.assertTrue(ddl.startsWith("CONNECT TO"));
		ddl = JsonPath.read(responseData, "$.items[1].statement");
		Assert.assertEquals(ddl, "QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS");
		ddl = JsonPath.read(responseData, "$.items[2].statement");
		Assert.assertEquals(ddl, "UNQUIESCE DATABASE");
		ddl = JsonPath.read(responseData, "$.items[3].statement");
		Assert.assertEquals(ddl, "TERMINATE");
		ddl = JsonPath.read(responseData, "$.items[4].statement");
		Assert.assertTrue(ddl.startsWith("DEACTIVATE DATABASE"));
		ddl = JsonPath.read(responseData, "$.items[5].statement");
		Assert.assertTrue(ddl.startsWith("RECOVER DATABASE"));
		Assert.assertTrue(ddl.contains("TO END OF LOGS USING HISTORY FILE"));
		Assert.assertTrue(ddl.contains("OVERFLOW LOG PATH"));
		
		logger.info("PASSED: recovery database end of log on specify dir executed successfully");
		
	}
	/**
	 * Test recovery database server time on default dir TA
	 * */
	@Test(description="Test the recovery database  server time on default dir function")
	public void testRecoveryDBServerTimeDefaultDir() throws InterruptedException{
		
		logger.info("Running recovery database end of server time on default dir TA test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_RECOVERY_DATABASE_COMMAND,"getRecoveryDBServerTimeDefaultDir_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CONNECT TO \"sample\""},
		 * {"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
		 * {"statement":"UNQUIESCE DATABASE"},
		 * {"statement":"TERMINATE"},
		 * {"statement":"DEACTIVATE DATABASE \"sample\""},
		 * {"statement":"RECOVER DATABASE \"sample\" TO USING LOCAL TIME USING HISTORY FILE ( \"\/home\/db2inst1\/sqllib\/backup\/\" )"}]}
		 * To retrieve the statement, we are looking for the 0,1,2 index of the items array -> the return is a object, we look for the value of key "statement", that is
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		Assert.assertTrue(ddl.startsWith("CONNECT TO"));
		ddl = JsonPath.read(responseData, "$.items[1].statement");
		Assert.assertEquals(ddl, "QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS");
		ddl = JsonPath.read(responseData, "$.items[2].statement");
		Assert.assertEquals(ddl, "UNQUIESCE DATABASE");
		ddl = JsonPath.read(responseData, "$.items[3].statement");
		Assert.assertEquals(ddl, "TERMINATE");
		ddl = JsonPath.read(responseData, "$.items[4].statement");
		Assert.assertTrue(ddl.startsWith("DEACTIVATE DATABASE"));
		ddl = JsonPath.read(responseData, "$.items[5].statement");
		Assert.assertTrue(ddl.startsWith("RECOVER DATABASE"));
		Assert.assertTrue(ddl.contains("TO USING LOCAL TIME USING HISTORY FILE"));
		
		logger.info("PASSED: recovery database end of server time on default dir TA executed successfully");
		
	}
	/**
	 * Test recovery database server time on specify dir TA
	 * */
	@Test(description="Test the recovery database server time on specify dir function")
	public void testRecoveryDBServerTimeSpecifyDir() throws InterruptedException{
		
		logger.info("Running recovery database server time on specify dir TA test...");

		JSONObject responseData=getResponseData(AdminTAService.TA_RECOVERY_DATABASE_COMMAND,"getRecoveryDBServerTimeSpecifyDir_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CONNECT TO \"sample\""},
		 * {"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
		 * {"statement":"UNQUIESCE DATABASE"},
		 * {"statement":"TERMINATE"},
		 * {"statement":"DEACTIVATE DATABASE \"sample\""},
		 * {"statement":"RECOVER DATABASE \"sample\" TO 2015-08-21-05.30.00 USING LOCAL TIME USING HISTORY FILE ( \"\/home\/db2inst1\/sqllib\/backup\/\" ) OVERFLOW LOG PATH ( \"\/home\/db2inst1\/sqllib\/backup\" )"}]}
		 * 
		 * To retrieve the statement, we are looking for the 0,1,2 index of the items array -> the return is a object, we look for the value of key "statement", that is
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		Assert.assertTrue(ddl.startsWith("CONNECT TO"));
		ddl = JsonPath.read(responseData, "$.items[1].statement");
		Assert.assertEquals(ddl, "QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS");
		ddl = JsonPath.read(responseData, "$.items[2].statement");
		Assert.assertEquals(ddl, "UNQUIESCE DATABASE");
		ddl = JsonPath.read(responseData, "$.items[3].statement");
		Assert.assertEquals(ddl, "TERMINATE");
		ddl = JsonPath.read(responseData, "$.items[4].statement");
		Assert.assertTrue(ddl.startsWith("DEACTIVATE DATABASE"));
		ddl = JsonPath.read(responseData, "$.items[5].statement");
		Assert.assertTrue(ddl.startsWith("RECOVER DATABASE"));
		Assert.assertTrue(ddl.contains("TO 2015-08-21-05.30.00 USING LOCAL TIME USING HISTORY FILE"));
		Assert.assertTrue(ddl.contains("OVERFLOW LOG PATH"));
		
		logger.info("PASSED: recovery database server time on specify dir TA executed successfully");
		
	}
	/**
	 * Test recovery database  UTC on default dir TA
	 * */
	@Test(description="Test the recovery database  UTC on default dir function")
	public void testRecoveryDBUTCDefaultDir() throws InterruptedException{
	
	logger.info("Running recovery database  UTC on default dir TA test...");

	JSONObject responseData=getResponseData(AdminTAService.TA_RECOVERY_DATABASE_COMMAND,"getRecoveryDBUTCDefaultDir_LUW",false);

	/**
	 * Path the response data, for this case, the response data is:
	 * {"items":[{"statement":"CONNECT TO \"sample\""},
	 * {"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
	 * {"statement":"UNQUIESCE DATABASE"},
	 * {"statement":"TERMINATE"},
	 * {"statement":"DEACTIVATE DATABASE \"sample\""},
	 * {"statement":"RECOVER DATABASE \"sample\" TO 2015-08-21-05.30.00 USING UTC TIME USING HISTORY FILE ( \"\/home\/db2inst1\/sqllib\/backup\/\" ) OVERFLOW LOG PATH ( \"\/home\/db2inst1\/sqllib\/backup\" )"}]}
	 * 
	 * To retrieve the statement, we are looking for the 0,1,2 index of the items array -> the return is a object, we look for the value of key "statement", that is
	 * $.items[0].statement
	 * */
	String ddl = JsonPath.read(responseData, "$.items[0].statement");
	Assert.assertTrue(ddl.startsWith("CONNECT TO"));
	ddl = JsonPath.read(responseData, "$.items[1].statement");
	Assert.assertEquals(ddl, "QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS");
	ddl = JsonPath.read(responseData, "$.items[2].statement");
	Assert.assertEquals(ddl, "UNQUIESCE DATABASE");
	ddl = JsonPath.read(responseData, "$.items[3].statement");
	Assert.assertEquals(ddl, "TERMINATE");
	ddl = JsonPath.read(responseData, "$.items[4].statement");
	Assert.assertTrue(ddl.startsWith("DEACTIVATE DATABASE"));
	ddl = JsonPath.read(responseData, "$.items[5].statement");
	Assert.assertTrue(ddl.startsWith("RECOVER DATABASE"));
	Assert.assertTrue(ddl.contains("TO 2015-08-21-05.30.00 USING UTC TIME USING HISTORY FILE"));
	
	logger.info("PASSED: recovery database  UTC on default dir TA executed successfully");
	
}
	/**
	 * Test recovery database  UTC on specify dir TA
	 * */
	@Test(description="Test the recovery database  UTC on specify dir function")
	public void testRecoveryDBUTCSpecifyDir() throws InterruptedException{
	
	logger.info("Running recovery database  UTC on specify dir TA test...");

	JSONObject responseData=getResponseData(AdminTAService.TA_RECOVERY_DATABASE_COMMAND,"getRecoveryDBUTCSpecifyDir_LUW",false);

	/**
	 * Path the response data, for this case, the response data is:
	 * {"items":[{"statement":"CONNECT TO \"sample\""},
	 * {"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
	 * {"statement":"UNQUIESCE DATABASE"},
	 * {"statement":"TERMINATE"},
	 * {"statement":"DEACTIVATE DATABASE \"sample\""},
	 * {"statement":"RECOVER DATABASE \"sample\" TO 2015-08-21-05.30.00 USING UTC TIME USING HISTORY FILE ( \"\/home\/db2inst1\/sqllib\/backup\/\" ) OVERFLOW LOG PATH ( \"\/home\/db2inst1\/sqllib\/backup\" )"}]}
	 * 
	 * To retrieve the statement, we are looking for the 0,1,2 index of the items array -> the return is a object, we look for the value of key "statement", that is
	 * $.items[0].statement
	 * */
	String ddl = JsonPath.read(responseData, "$.items[0].statement");
	Assert.assertTrue(ddl.startsWith("CONNECT TO"));
	ddl = JsonPath.read(responseData, "$.items[1].statement");
	Assert.assertEquals(ddl, "QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS");
	ddl = JsonPath.read(responseData, "$.items[2].statement");
	Assert.assertEquals(ddl, "UNQUIESCE DATABASE");
	ddl = JsonPath.read(responseData, "$.items[3].statement");
	Assert.assertEquals(ddl, "TERMINATE");
	ddl = JsonPath.read(responseData, "$.items[4].statement");
	Assert.assertTrue(ddl.startsWith("DEACTIVATE DATABASE"));
	ddl = JsonPath.read(responseData, "$.items[5].statement");
	Assert.assertTrue(ddl.startsWith("RECOVER DATABASE"));
	Assert.assertTrue(ddl.contains("TO 2015-08-21-05.30.00 USING UTC TIME USING HISTORY FILE"));
	
	logger.info("PASSED: recovery database  UTC on specify dir TA executed successfully");
}
	/**
	 * Test roll forward DB TA
	 * */
	@Test(description="Test roll forward DB function")
	public void testRollForwardDB() throws InterruptedException{
	
	logger.info("Running roll forward DB TA test...");

	JSONObject responseData=getResponseData(AdminTAService.TA_ROLL_FORWARD_COMMAND,"getRollForwardDB_LUW",false);

	/**
	 * Path the response data, for this case, the response data is:
	 * {"items":[{"statement":"ROLLFORWARD DATABASE \"sample\" STOP"}]}
	 * To retrieve the statement, we are looking for the 0,1,2 index of the items array -> the return is a object, we look for the value of key "statement", that is
	 * $.items[0].statement
	 * */
	String ddl = JsonPath.read(responseData, "$.items[0].statement");
	Assert.assertTrue(ddl.startsWith("ROLLFORWARD DATABASE"));
	Assert.assertTrue(ddl.endsWith("STOP"));
	
	logger.info("PASSED: roll forward DB TA executed successfully");
}
	
	/**
	 * Add by wtaobj@cn.ibm.com
	 * Test roll forward DB to end of log
	 * */
	@Test(description="Test roll forward DB to end of log function")
	public void testRollForwardDBEndOfLog() throws InterruptedException{
	
	logger.info("Running roll forward DB to end of log test...");

	JSONObject responseData=getResponseData(AdminTAService.TA_ROLL_FORWARD_COMMAND,"getRollForwardDBEndOfLog_LUW",false);

	/**
	 * Path the response data, for this case, the response data is:
	 * {"items":[{"statement":"ROLLFORWARD DATABASE \"sample\" TO END OF LOGS"}]}
	 * To retrieve the statement, we are looking for the 0,1,2 index of the items array -> the return is a object, we look for the value of key "statement", that is
	 * $.items[0].statement
	 * */
	String ddl = JsonPath.read(responseData, "$.items[0].statement");
	//Assert.assertEquals(ddl,"ROLLFORWARD DATABASE \"sample\" TO END OF LOGS");
	Assert.assertTrue(ddl.startsWith("ROLLFORWARD DATABASE"));
	Assert.assertTrue(ddl.endsWith("TO END OF LOGS"));
	
	logger.info("PASSED: roll forward DB to end of log executed successfully");
}
	
	/**
	 * Add by wtaobj@cn.ibm.com
	 * Test roll forward DB cancel
	 * */
	@Test(description="Test roll forward DB cancel function")
	public void testRollForwardDBCancel() throws InterruptedException{
	
	logger.info("Running roll forward DB cancel test...");

	JSONObject responseData=getResponseData(AdminTAService.TA_ROLL_FORWARD_COMMAND,"getRollForwardDBCancel_LUW",false);

	/**
	 * Path the response data, for this case, the response data is:
	 * {"items":[{"statement":"ROLLFORWARD DATABASE \"sample\" CANCEL"}]}
	 * To retrieve the statement, we are looking for the 0,1,2 index of the items array -> the return is a object, we look for the value of key "statement", that is
	 * $.items[0].statement
	 * */
	String ddl = JsonPath.read(responseData, "$.items[0].statement");
	
	Assert.assertTrue(ddl.startsWith("ROLLFORWARD DATABASE"));
	Assert.assertTrue(ddl.endsWith("CANCEL"));
	
	logger.info("PASSED: roll forward DB cancel executed successfully");
}
	
	/**
	 * Add by wtaobj@cn.ibm.com
	 * Test roll forward DB complete
	 * */
	@Test(description="Test roll forward DB complete function")
	public void testRollForwardDBComplete() throws InterruptedException{
	
	logger.info("Running roll forward DB complete test...");

	JSONObject responseData=getResponseData(AdminTAService.TA_ROLL_FORWARD_COMMAND,"getRollForwardDBComplete_LUW",false);

	/**
	 * Path the response data, for this case, the response data is:
	 * {"items":[{"statement":"ROLLFORWARD DATABASE \"sample\" COMPLETE"}]}
	 * To retrieve the statement, we are looking for the 0,1,2 index of the items array -> the return is a object, we look for the value of key "statement", that is
	 * $.items[0].statement
	 * */
	String ddl = JsonPath.read(responseData, "$.items[0].statement");
	
	Assert.assertTrue(ddl.startsWith("ROLLFORWARD DATABASE"));
	Assert.assertTrue(ddl.endsWith("COMPLETE"));
	//Assert.assertEquals(ddl,"ROLLFORWARD DATABASE \"sample\" COMPLETE");
	
	logger.info("PASSED: roll forward DB complete executed successfully");
}
	
	/**
	 * Test roll forward table TA
	 * */
	@Test(description="Test the roll forward table function")
	public void testRollForwardTable() throws InterruptedException{
	
	logger.info("Running roll forward table TA test...");

	JSONObject responseData=getResponseData(AdminTAService.TA_ROLL_FORWARD_COMMAND,"getRollForwardTable_LUW",false);

	/**
	 * Path the response data, for this case, the response data is:
	 *{"items":[{"statement":"ROLLFORWARD DATABASE \"sample\" STOP TABLESPACE ( SYSCATSPACE, USERSPACE1, IBMDB2SAMPLEREL ) ONLINE"}]}
	 * To retrieve the statement, we are looking for the 0,1,2 index of the items array -> the return is a object, we look for the value of key "statement", that is
	 * $.items[0].statement
	 * */
	String ddl = JsonPath.read(responseData, "$.items[0].statement");
	Assert.assertTrue(ddl.startsWith("ROLLFORWARD DATABASE"));
	Assert.assertTrue(ddl.endsWith("STOP AND COMPLETE TABLESPACE ( SYSCATSPACE, USERSPACE1, IBMDB2SAMPLEREL ) ONLINE"));
	
	
	logger.info("PASSED: roll forward table TA executed successfully");
}
	/**
	 * Test configure logging with circular type TA
	 * */
	@Test(description="Test the configure logging with circular type function")
	public void testConfigureLoggingCircular() throws InterruptedException{
	
	logger.info("Running configure logging with circular type TA test...");

	JSONObject responseData=getResponseData(AdminTAService.TA_CONFIGURE_LOGGING_COMMAND,"getConfigureLoggingCircular_LUW",false);

	/**
	 * Path the response data, for this case, the response data is:
	 *{"items":[{"statement":"CONNECT TO \"sample\""},
	 *{"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
	 *{"statement":"UNQUIESCE DATABASE"},
	 *{"statement":"CONNECT RESET"},
	 *{"statement":"CONNECT TO \"sample\""},
	 *{"statement":"UPDATE DATABASE CONFIGURATION USING logarchmeth1 OFF logarchmeth2 OFF newlogpath \"\/home\/db2inst1\/db2inst1\/NODE0000\/SQL00001\/LOGSTREAM0000\/\" logprimary 25 logsecond -1 logfilsiz 7900 LOGINDEXBUILD OFF "},
	 *{"statement":"CONNECT RESET"},
	 *{"statement":"DEACTIVATE DATABASE \"sample\""}]}
	 * To retrieve the statement, we are looking for the 0,1,2 index of the items array -> the return is a object, we look for the value of key "statement", that is
	 * $.items[0].statement
	 * */
	String ddl = JsonPath.read(responseData, "$.items[0].statement");
	Assert.assertTrue(ddl.startsWith("CONNECT TO"));
	ddl = JsonPath.read(responseData, "$.items[1].statement");
	Assert.assertEquals(ddl, "QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS");
	ddl = JsonPath.read(responseData, "$.items[2].statement");
	Assert.assertEquals(ddl, "UNQUIESCE DATABASE");
	ddl = JsonPath.read(responseData, "$.items[3].statement");
	Assert.assertEquals(ddl,"CONNECT RESET");
	ddl = JsonPath.read(responseData, "$.items[4].statement");
	Assert.assertTrue(ddl.startsWith("CONNECT TO"));
	ddl = JsonPath.read(responseData, "$.items[5].statement");
	Assert.assertTrue(ddl.startsWith("UPDATE DATABASE CONFIGURATION USING logarchmeth1 OFF logarchmeth2 OFF newlogpath"));
	Assert.assertTrue(ddl.endsWith("logprimary 25 logsecond -1 logfilsiz 7900 LOGINDEXBUILD OFF "));
	ddl = JsonPath.read(responseData, "$.items[6].statement");
	Assert.assertEquals(ddl,"CONNECT RESET");
	ddl = JsonPath.read(responseData, "$.items[7].statement");
	Assert.assertTrue(ddl.startsWith("DEACTIVATE DATABASE"));
	
	logger.info("PASSED: configure logging with circular type TA executed successfully");
}
	
	/**
	 * Add by wtaobj@cn.ibm.com
	 * Test configure logging with choosing number and size of the log files TA
	 * */
	@Test(description="Test the configure logging with choosing number and size of the log files function")
	public void testConfigureLoggingNumberAndSize() throws InterruptedException{
	
	logger.info("Running configure logging with choosing number and size of the log files TA test...");

	JSONObject responseData=getResponseData(AdminTAService.TA_CONFIGURE_LOGGING_COMMAND,"getConfigureLoggingNumberAndSize_LUW",false);

	/**
	 * Path the response data, for this case, the response data is:
	 *{"items":[{"statement":"CONNECT TO \"sample\""},
	 *{"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
	 *{"statement":"UNQUIESCE DATABASE"},
	 *{"statement":"CONNECT RESET"},
	 *{"statement":"CONNECT TO \"sample\""},
	 *{"statement":"UPDATE DATABASE CONFIGURATION USING logarchmeth1 OFF logarchmeth2 OFF logprimary 5 logsecond 10 logfilsiz 2000 LOGINDEXBUILD OFF "},
	 *{"statement":"CONNECT RESET"},
	 *{"statement":"DEACTIVATE DATABASE \"sample\""}]}
	 * To retrieve the statement, we are looking for the 0,1,2 index of the items array -> the return is a object, we look for the value of key "statement", that is
	 * $.items[0].statement
	 * */
	String ddl = JsonPath.read(responseData, "$.items[0].statement");
	Assert.assertTrue(ddl.startsWith("CONNECT TO"));
	ddl = JsonPath.read(responseData, "$.items[1].statement");
	Assert.assertEquals(ddl, "QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS");
	ddl = JsonPath.read(responseData, "$.items[2].statement");
	Assert.assertEquals(ddl, "UNQUIESCE DATABASE");
	ddl = JsonPath.read(responseData, "$.items[3].statement");
	Assert.assertEquals(ddl,"CONNECT RESET");
	ddl = JsonPath.read(responseData, "$.items[4].statement");
	Assert.assertTrue(ddl.startsWith("CONNECT TO"));
	ddl = JsonPath.read(responseData, "$.items[5].statement");
	Assert.assertEquals(ddl,"UPDATE DATABASE CONFIGURATION USING logarchmeth1 OFF logarchmeth2 OFF logprimary 5 logsecond 10 logfilsiz 2000 LOGINDEXBUILD OFF ");
	ddl = JsonPath.read(responseData, "$.items[6].statement");
	Assert.assertEquals(ddl,"CONNECT RESET");
	ddl = JsonPath.read(responseData, "$.items[7].statement");
	Assert.assertTrue(ddl.startsWith("DEACTIVATE DATABASE"));
	
	logger.info("PASSED: configure logging with choosing number and size of the log files TA executed successfully");
}
	
	
	/**
	 * Add by wtaobj@cn.ibm.com
	 * Test configure logging with changing location of the log files TA
	 * */
	@Test(description="Test the configure logging with changing location of the log files function")
	public void testConfigureLoggingLocation() throws InterruptedException{
	
	logger.info("Running configure logging with changing location of the log files TA test...");

	JSONObject responseData=getResponseData(AdminTAService.TA_CONFIGURE_LOGGING_COMMAND,"getConfigureLoggingLocation_LUW",false);

	/**
	 * Path the response data, for this case, the response data is:
	 *{"items":[{"statement":"CONNECT TO \"sample\""},
	 *{"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
	 *{"statement":"UNQUIESCE DATABASE"},
	 *{"statement":"CONNECT RESET"},
	 *{"statement":"CONNECT TO \"sample\""},
	 *{"statement":"UPDATE DATABASE CONFIGURATION USING logarchmeth1 OFF logarchmeth2 OFF newlogpath \"\/home\/db2inst1\/mydb\" mirrorlogpath \"\/home\/db2inst1\/mydb\" logprimary 5 logsecond 10 logfilsiz 2000 LOGINDEXBUILD OFF "},
	 *{"statement":"CONNECT RESET"},
	 *{"statement":"DEACTIVATE DATABASE \"sample\""}]}
	 * To retrieve the statement, we are looking for the 0,1,2 index of the items array -> the return is a object, we look for the value of key "statement", that is
	 * $.items[0].statement
	 * */
	String ddl = JsonPath.read(responseData, "$.items[5].statement");
	Assert.assertEquals(ddl,"UPDATE DATABASE CONFIGURATION USING logarchmeth1 OFF logarchmeth2 OFF newlogpath \"/home/db2inst1/mydb\" mirrorlogpath \"/home/db2inst1/mydb\" logprimary 5 logsecond 10 logfilsiz 2000 LOGINDEXBUILD OFF ");
	
	logger.info("PASSED: configure logging with changing location of the log files TA executed successfully");
}
	
	/**
	 * Test configure logging with manual archive type TA
	 * */
	@Test(description="Test the configure logging with manual archive type function")
	public void testConfigureLoggingManualArchive() throws InterruptedException{
	
	logger.info("Running configure logging with manual archive type TA test...");

	JSONObject responseData=getResponseData(AdminTAService.TA_CONFIGURE_LOGGING_COMMAND,"getConfigureLoggingManualArchive_LUW",false);

	/**
	 * Path the response data, for this case, the response data is:
	 *{"items":[{"statement":"CONNECT TO \"sample\""},
	 *{"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
	 *{"statement":"UNQUIESCE DATABASE"},
	 *{"statement":"CONNECT RESET"},
	 *{"statement":"CONNECT TO \"sample\""},
	 *{"statement":"UPDATE DATABASE CONFIGURATION USING logarchmeth1 LOGRETAIN newlogpath \"\/home\/db2inst1\/db2inst1\/NODE0000\/SQL00001\/LOGSTREAM0000\/\" logprimary 25 logsecond -1 logfilsiz 7900 LOGINDEXBUILD ON "},
	 *{"statement":"CONNECT RESET"},
	 *{"statement":"DEACTIVATE DATABASE \"sample\""}]}
	 * To retrieve the statement, we are looking for the 0,1,2 index of the items array -> the return is a object, we look for the value of key "statement", that is
	 * $.items[0].statement
	 * */
	String ddl = JsonPath.read(responseData, "$.items[0].statement");
	Assert.assertTrue(ddl.startsWith("CONNECT TO"));
	ddl = JsonPath.read(responseData, "$.items[1].statement");
	Assert.assertEquals(ddl, "QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS");
	ddl = JsonPath.read(responseData, "$.items[2].statement");
	Assert.assertEquals(ddl, "UNQUIESCE DATABASE");
	ddl = JsonPath.read(responseData, "$.items[3].statement");
	Assert.assertEquals(ddl,"CONNECT RESET");
	ddl = JsonPath.read(responseData, "$.items[4].statement");
	Assert.assertTrue(ddl.startsWith("CONNECT TO"));
	ddl = JsonPath.read(responseData, "$.items[5].statement");
	Assert.assertTrue(ddl.startsWith("UPDATE DATABASE CONFIGURATION USING logarchmeth1 LOGRETAIN newlogpath"));
	Assert.assertTrue(ddl.endsWith("logprimary 25 logsecond -1 logfilsiz 7900 LOGINDEXBUILD ON "));
	ddl = JsonPath.read(responseData, "$.items[6].statement");
	Assert.assertEquals(ddl,"CONNECT RESET");
	ddl = JsonPath.read(responseData, "$.items[7].statement");
	Assert.assertTrue(ddl.startsWith("DEACTIVATE DATABASE"));
	
	logger.info("PASSED: configure logging with manual archive type TA executed successfully");
}
	
	/**
	 * Test configure logging with routine archive type TA
	 * */
	@Test(description="Test the configure logging with routine archive type function")
	public void testConfigureLoggingRoutineArchive() throws InterruptedException{
	
	logger.info("Running configure logging with routine archive type TA test...");

	JSONObject responseData=getResponseData(AdminTAService.TA_CONFIGURE_LOGGING_COMMAND,"getConfigureLoggingRoutineArchive_LUW",false);

	/**
	 * Path the response data, for this case, the response data is:
	 *{"items":[{"statement":"CONNECT TO \"sample\""},
	 *{"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
	 *{"statement":"UNQUIESCE DATABASE"},
	 *{"statement":"CONNECT RESET"},
	 *{"statement":"CONNECT TO \"sample\""},
	 *{"statement":"UPDATE DATABASE CONFIGURATION USING logarchmeth1 USEREXIT newlogpath \"\/home\/db2inst1\/db2inst1\/NODE0000\/SQL00001\/LOGSTREAM0000\/\" logprimary 25 logsecond -1 logfilsiz 7900 LOGINDEXBUILD ON "},
	 *{"statement":"CONNECT RESET"},
	 *{"statement":"DEACTIVATE DATABASE \"sample\""}]}
	 * To retrieve the statement, we are looking for the 0,1,2 index of the items array -> the return is a object, we look for the value of key "statement", that is
	 * $.items[0].statement
	 * */
	String ddl = JsonPath.read(responseData, "$.items[0].statement");
	Assert.assertTrue(ddl.startsWith("CONNECT TO"));
	ddl = JsonPath.read(responseData, "$.items[1].statement");
	Assert.assertEquals(ddl, "QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS");
	ddl = JsonPath.read(responseData, "$.items[2].statement");
	Assert.assertEquals(ddl, "UNQUIESCE DATABASE");
	ddl = JsonPath.read(responseData, "$.items[3].statement");
	Assert.assertEquals(ddl,"CONNECT RESET");
	ddl = JsonPath.read(responseData, "$.items[4].statement");
	Assert.assertTrue(ddl.startsWith("CONNECT TO"));
	ddl = JsonPath.read(responseData, "$.items[5].statement");
	Assert.assertTrue(ddl.startsWith("UPDATE DATABASE CONFIGURATION USING logarchmeth1 USEREXIT newlogpath"));
	Assert.assertTrue(ddl.contains("logprimary 25 logsecond -1 logfilsiz 7900 LOGINDEXBUILD ON "));
	ddl = JsonPath.read(responseData, "$.items[6].statement");
	Assert.assertEquals(ddl,"CONNECT RESET");
	ddl = JsonPath.read(responseData, "$.items[7].statement");
	Assert.assertTrue(ddl.startsWith("DEACTIVATE DATABASE"));
	
	logger.info("PASSED: configure logging with routine archive type TA executed successfully");
}
	/**
	 * Test configure logging with automatic archive type TA
	 * */
	@Test(description="Test the configure logging with automatic archive type function")
	public void testConfigureLoggingAutomaticArchive() throws InterruptedException{
	
	logger.info("Running configure logging with automatic archive type TA test...");

	JSONObject responseData=getResponseData(AdminTAService.TA_CONFIGURE_LOGGING_COMMAND,"getConfigureLoggingAutomaticArchive_LUW",false);

	/**
	 * Path the response data, for this case, the response data is:
	 *{"items":[{"statement":"CONNECT TO \"sample\""},
	 *{"statement":"QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS"},
	 *{"statement":"UNQUIESCE DATABASE"},
	 *{"statement":"CONNECT RESET"},
	 *{"statement":"CONNECT TO \"sample\""},
	 *{"statement":"UPDATE DATABASE CONFIGURATION USING logarchmeth1 TSM newlogpath \"\/home\/db2inst1\/db2inst1\/NODE0000\/SQL00001\/LOGSTREAM0000\/\" logprimary 25 logsecond -1 logfilsiz 7900 LOGINDEXBUILD ON "},
	 *{"statement":"CONNECT RESET"},
	 *{"statement":"DEACTIVATE DATABASE \"sample\""}]}
	 * To retrieve the statement, we are looking for the 0,1,2 index of the items array -> the return is a object, we look for the value of key "statement", that is
	 * $.items[0].statement
	 * */
	String ddl = JsonPath.read(responseData, "$.items[0].statement");
	Assert.assertTrue(ddl.startsWith("CONNECT TO"));
	ddl = JsonPath.read(responseData, "$.items[1].statement");
	Assert.assertEquals(ddl, "QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS");
	ddl = JsonPath.read(responseData, "$.items[2].statement");
	Assert.assertEquals(ddl, "UNQUIESCE DATABASE");
	ddl = JsonPath.read(responseData, "$.items[3].statement");
	Assert.assertEquals(ddl,"CONNECT RESET");
	ddl = JsonPath.read(responseData, "$.items[4].statement");
	Assert.assertTrue(ddl.startsWith("CONNECT TO"));
	ddl = JsonPath.read(responseData, "$.items[5].statement");
	Assert.assertTrue(ddl.startsWith("UPDATE DATABASE CONFIGURATION USING logarchmeth1 TSM newlogpath"));
	Assert.assertTrue(ddl.contains("logprimary 25 logsecond -1 logfilsiz 7900 LOGINDEXBUILD ON "));
	ddl = JsonPath.read(responseData, "$.items[6].statement");
	Assert.assertEquals(ddl,"CONNECT RESET");
	ddl = JsonPath.read(responseData, "$.items[7].statement");
	Assert.assertTrue(ddl.startsWith("DEACTIVATE DATABASE"));
	
	logger.info("PASSED: configure logging with automatic archive type TA executed successfully");
}
	/**
	 * Add by wtaobj@cn.ibm.com.
	 * Test case of configure BLU acceleration.
	 * */
	@Test(description="Test case of configure BLU acceleration function.")
	private void testConfigureBLU(){
		logger.info("Running configure BLU acceleration...");

		JSONObject responseData=getResponseData(AdminTAService.TA_CONFIGURE_BLU_COMMAND,"getConfigureBLU_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"!db2set DB2_WORKLOAD=ANALYTICS"}]} 
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertEquals(ddl,"!db2set DB2_WORKLOAD=ANALYTICS");
		
		logger.info("PASSED: configure BLU acceleration executed successfully");
	}
	
	/**
	 * Add by wtaobj@cn.ibm.com.
	 * Test case of revalidating DB objects.
	 * */
	@Test(description="Test case of revalidating DB objects function.")
	private void testRevalidateDBObjects(){
		logger.info("Running revalidating DB objects...");
		logger.info("RevalidateDBObjects Precondition");
		testPrecondition("RevalidateDBObjects");

		JSONObject responseData=getResponseData(AdminTAService.TA_REVALIDATE_COMMAND,"getRevalidate_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"CALL SYSPROC.ADMIN_REVALIDATE_DB_OBJECTS()"}]} 
		 */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		Assert.assertEquals(ddl,"CALL SYSPROC.ADMIN_REVALIDATE_DB_OBJECTS()");
		
		logger.info("PASSED: revalidating DB objects executed successfully");
		logger.info("RevalidateDBObjects Precondition Clean");
  	    testPreconditionClean("RevalidateDBObjects");
		
	}
	
	/**
	 * @author cyinmei@cn.ibm.com
	 * Test case of table load data.
	 * */
	@Test(description="Test case of table load data function.")
	private void testLoadTableData(){
		logger.info("Running load table data...");

		JSONObject responseData=getResponseData(AdminTAService.TA_LOAD_TABLE_COMMAND,"getLoadTableData_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CALL SYSPROC.ADMIN_CMD( 'LOAD FROM \"\/home\/db2inst\/test.txt\" OF DEL MESSAGES ON SERVER INSERT INTO SCH_VIEWDATA.TAB_VIEWDATA COPY NO' )"}]}
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertEquals(ddl,"CALL SYSPROC.ADMIN_CMD( 'LOAD FROM \"/home/db2inst1/test.txt\" OF DEL MESSAGES ON SERVER INSERT INTO SCH_VIEWDATA.TAB_VIEWDATA COPY NO' )");
		
		logger.info("PASSED: load table data executed successfully");
	}
	
	/* 
	 * @author cyinmei@cn.ibm.com
	 * Test case of table export data.
	 * 
	 */
	@Test(description="Test case of table export data function.")
	private void testExportTableData(){
		logger.info("Running export table data...");

		JSONObject responseData=getResponseData(AdminTAService.TA_EXPORT_TABLE_COMMAND,"getExportTableData_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 *{"items":[{"statement":"CALL SYSPROC.ADMIN_CMD( 'EXPORT TO \"\/home\/db2inst1\" OF DEL MODIFIED BY XMLNODECLARATION MESSAGES ON SERVER SELECT * FROM SCH_VIEWDATA.TAB_VIEWDATA' )"}]}
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		Assert.assertEquals(ddl,"CALL SYSPROC.ADMIN_CMD( 'EXPORT TO \"/home/db2inst1\" OF DEL MODIFIED BY XMLNODECLARATION MESSAGES ON SERVER SELECT * FROM SCH_VIEWDATA.TAB_VIEWDATA' )");
		logger.info("PASSED: export table data executed successfully");
		
	}
	
	/**
	 * @author cyinmei@cn.ibm.com
	 * Test case of reorganize table.
	 * */
	@Test(description="Test case of reorganize table function.")
	private void testReorganizeTable(){
		logger.info("Running reorganize table ...");

		JSONObject responseData=getResponseData(AdminTAService.TA_REORG_TABLE_COMMAND,"getReorganizeTable_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CALL SYSPROC.ADMIN_CMD( 'REORG TABLE SCH_VIEWDATA.TAB_VIEWDATA' )"}]}
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertEquals(ddl,"CALL SYSPROC.ADMIN_CMD( 'REORG TABLE SCH_VIEWDATA.TAB_VIEWDATA' )");
		
		logger.info("PASSED: reorganize table executed successfully");
	}
	
	/**
	 * @author cyinmei@cn.ibm.com
	 * Test case of runstats table.
	 * */
	@Test(description="Test case of runstats table function.")
	private void testRunstatsTable(){
		logger.info("Running runstats table ...");

		JSONObject responseData=getResponseData(AdminTAService.TA_RUNSTATS_TABLE_COMMAND,"getRunstatsTable_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CALL SYSPROC.ADMIN_CMD( 'RUNSTATS ON TABLE SCH_VIEWDATA.TAB_VIEWDATA ON ALL COLUMNS AND INDEXES ALL SET PROFILE' )"}]}
		 * $.items[0].statement
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertEquals(ddl,"CALL SYSPROC.ADMIN_CMD( 'RUNSTATS ON TABLE SCH_VIEWDATA.TAB_VIEWDATA ON ALL COLUMNS AND INDEXES ALL SET PROFILE' )");
		
		logger.info("PASSED: runstats table executed successfully");
	}
	/**
	 * @author ylwh@cn.ibm.com
	 * The env of test case of convert row table to column table.
	 * */
	private void testConvertRowTableToColumnTable_precondition(){
		dropConRowToColumnTableENV(false);
		createConRowToColumnTableENV(true);
	}
	private void testConvertRowTableToColumnTable_clean(){
		dropConRowToColumnTableENV(true);
	}
	private void createConRowToColumnTableENV(boolean real) {
		// TODO Auto-generated method stub
		List<String> sqlList = new ArrayList<String>();
		sqlList.add("CREATE BUFFERPOOL CON_POL IMMEDIATE SIZE 2000 PAGESIZE 4K;");
		sqlList.add("CREATE TABLESPACE CON_TABSP PAGESIZE 4K BUFFERPOOL CON_POL ;");
		sqlList.add("CREATE SCHEMA CON_SCH;");
		sqlList.add("CREATE TABLE CON_SCH.CON_TAB ( C1 CHAR(5), C2 INTEGER ) IN CON_TABSP ORGANIZE BY ROW;");
		sqlEditorService.runSQLinJDBC(sqlList, ";", this.dbProfile,real);
	}
	private void dropConRowToColumnTableENV(boolean real) {
		// TODO Auto-generated method stub
		List<String> sqlList = new ArrayList<String>();
		sqlList.add("DROP TABLE CON_SCH.CON_TAB;");
		sqlList.add("DROP SCHEMA CON_SCH RESTRICT;");
		sqlList.add("DROP TABLESPACE CON_TABSP;");
		sqlList.add("DROP BUFFERPOOL CON_POL;");
		sqlEditorService.runSQLinJDBC(sqlList, ";", this.dbProfile,real);
	}
	/**
	 * @author cyinmei@cn.ibm.com
	 * Test case of convert row table to column table.
	 * */
	@Test(description="Test case of convert row table to column table function.")
	private void testConvertRowTableToColumnTable(){
		logger.info("Running convert row table to column table ...");
		
		//column-organized table is supported in DB2 v10.5 or above, so for v9.7 and v10.1 DBs just return;
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97) || this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_101) )
		{ return; }

		JSONObject responseData=getResponseData(AdminTAService.TA_CONVERT_TABLE_COMMAND,"getConvertRowTableToColumnTable_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 *	{"items":[{"statement":"CALL SYSPROC.ADMIN_MOVE_TABLE('CON_SCH','CON_TAB','CON_TABSP','CON_TABSP','CON_TABSP','ORGANIZE BY COLUMN','','','','COPY_USE_LOAD, NOT_ENFORCED','INIT')"},
		 * {"statement":"CALL SYSPROC.ADMIN_MOVE_TABLE('CON_SCH','CON_TAB','CON_TABSP','CON_TABSP','CON_TABSP','ORGANIZE BY COLUMN','','','','COPY_USE_LOAD, NOT_ENFORCED','COPY');"},
		 * {"statement":"CALL SYSPROC.ADMIN_MOVE_TABLE('CON_SCH','CON_TAB','CON_TABSP','CON_TABSP','CON_TABSP','ORGANIZE BY COLUMN','','','','COPY_USE_LOAD, NOT_ENFORCED','REPLAY');"},
		 * {"statement":"CALL SYSPROC.ADMIN_CMD( 'BACKUP DATABASE \"encrypdb\" ONLINE TO \"\/home\/db2inst3\" EXCLUDE LOGS WITHOUT PROMPTING' )"},
		 * {"statement":"CALL SYSPROC.ADMIN_MOVE_TABLE('CON_SCH','CON_TAB','CON_TABSP','CON_TABSP','CON_TABSP','ORGANIZE BY COLUMN','','','','COPY_USE_LOAD, NOT_ENFORCED','SWAP')"}]}
			 * */
		String ddl1 = JsonPath.read(responseData, "$.items[0].statement");
		String ddl2 = JsonPath.read(responseData, "$.items[1].statement");
		String ddl3 = JsonPath.read(responseData, "$.items[2].statement");
		String ddl4 = JsonPath.read(responseData, "$.items[3].statement");
		String ddl5 = JsonPath.read(responseData, "$.items[4].statement");

		Assert.assertEquals(ddl1,"CALL SYSPROC.ADMIN_MOVE_TABLE('CON_SCH','CON_TAB','CON_TABSP','CON_TABSP','CON_TABSP','ORGANIZE BY COLUMN','','','','COPY_USE_LOAD, NOT_ENFORCED','INIT')");
		Assert.assertEquals(ddl2,"CALL SYSPROC.ADMIN_MOVE_TABLE('CON_SCH','CON_TAB','CON_TABSP','CON_TABSP','CON_TABSP','ORGANIZE BY COLUMN','','','','COPY_USE_LOAD, NOT_ENFORCED','COPY')");
		Assert.assertEquals(ddl3,"CALL SYSPROC.ADMIN_MOVE_TABLE('CON_SCH','CON_TAB','CON_TABSP','CON_TABSP','CON_TABSP','ORGANIZE BY COLUMN','','','','COPY_USE_LOAD, NOT_ENFORCED','REPLAY')");
		Assert.assertTrue(ddl4.contains("CALL SYSPROC.ADMIN_CMD( 'BACKUP DATABASE"));
		Assert.assertEquals(ddl5,"CALL SYSPROC.ADMIN_MOVE_TABLE('CON_SCH','CON_TAB','CON_TABSP','CON_TABSP','CON_TABSP','ORGANIZE BY COLUMN','','','','COPY_USE_LOAD, NOT_ENFORCED','SWAP')");

		logger.info("PASSED: convert row table to column table executed successfully");
	}
	
	/**
	 * @author cyinmei@cn.ibm.com
	 * Test case of reorganize index on table.
	 * */
	@Test(description="Test case of reorganize index on table function.")
	private void testReorgIndexOnTable(){
		logger.info("Running reorganize index on table ...");

		JSONObject responseData=getResponseData(AdminTAService.TA_REORG_INDEX_COMMAND,"getReorganizeIndex_LUW",false);

		/**
		 * Path the response data, for this case, the response data is:
		 * {"items":[{"statement":"CALL SYSPROC.ADMIN_CMD( 'REORG INDEXES ALL FOR TABLE SCH_REORGINDEX.TAB_REORGINDEX' )"}]}
		 * $.items[0].statement
		 * CREATE TABLE "SCH_REORGINDEX"."TAB_REORGINDEX" ( "NEW_COLUMN1" CHAR(5) );
         * CREATE INDEX "SCH_REORGINDEX"."TAB_REORGINDEX_INDEX1" ON "SCH_REORGINDEX"."TAB_REORGINDEX" ("NEW_COLUMN1" ASC);
		 * */
		String ddl = JsonPath.read(responseData, "$.items[0].statement");
		
		Assert.assertEquals(ddl,"CALL SYSPROC.ADMIN_CMD( 'REORG INDEXES ALL FOR TABLE SCH_REORGINDEX.TAB_REORGINDEX' )");
		
		logger.info("PASSED: reorganize index on table executed successfully");
	}
	/**
	 * @author ylwh@cn.ibm.com
	 * SP revalidate function
	 **/
	private void testCreateSPRevalidate_precondition(){
		dropSPRevalENV(false);
		createSPRevalENV(true);
	}
	private void createSPRevalENV(boolean real) {
		// TODO Auto-generated method stub
		List<String> sqlList = new ArrayList<String>();
		sqlList.add("CREATE SCHEMA TESTSPREVAL!");
		sqlList.add("CREATE TABLE TESTSPREVAL.T1( "+
					 "C1  INTEGER NOT NULL UNIQUE, "+
					 "C2  CHAR(5) NOT NULL, "+
					 "C3  VARCHAR(5), "+
					 "C4  INTEGER NOT NULL)!");
		sqlList.add("CREATE PROCEDURE TESTSPREVAL.PROCED_T1(IN PRO1 INTEGER,OUT  PRO2 VARCHAR(5)) "+
					 "LANGUAGE SQL "+
					 "SPECIFIC  TESTSPREVAL.PROCED_T1 "+
					 "BEGIN "+
					 "SELECT C3 INTO PRO2 FROM TESTSPREVAL.T1 WHERE C1=PRO1; "+
					 "END!");	
		sqlList.add("DROP TABLE TESTSPREVAL.T1!");
		sqlList.add("CREATE TABLE TESTSPREVAL.T1( "+
				 "C1  INTEGER NOT NULL UNIQUE, "+
				 "C2  CHAR(5) NOT NULL, "+
				 "C3  VARCHAR(5), "+
				 "C4  INTEGER NOT NULL)!");
		sqlEditorService.runSQLinJDBC(sqlList, "!", this.dbProfile,real);
	}

	private void testCreateSPRevalidate_clean(){
		dropSPRevalENV(false);
	}
	private void dropSPRevalENV(boolean real) {
		// TODO Auto-generated method stub
		List<String> sqlList = new ArrayList<String>();
		sqlList.add("DROP PROCEDURE TESTSPREVAL.PROCED_T1;");
		sqlList.add("DROP TABLE TESTSPREVAL.T1;");
		sqlList.add("DROP SCHEMA TESTSPREVAL RESTRICT;");	
		sqlEditorService.runSQLinJDBC(sqlList, ";", this.dbProfile,real);
	}
	/**
	 * Test create SP revalidated with table function
	 * */
	@Test(description="Test create SP revalidated with table function")
	public void testCreateSPRevalidate() throws InterruptedException{
	
	logger.info("Running create SP revalidated with table test...");

	JSONObject responseData=getResponseData(AdminTAService.TA_REVALIDATE_COMMAND,"getSPRevalidate_LUW",false);

	/**
	 * Path the response data, for this case, the response data is:
	 * {"items":[{"statement":"CALL SYSPROC.ADMIN_REVALIDATE_DB_OBJECTS('PROCEDURE',NULL,NULL)"}]}
	 * */
	String  ddl = JsonPath.read(responseData, "$.items[0].statement");
	Assert.assertEquals(ddl,"CALL SYSPROC.ADMIN_REVALIDATE_DB_OBJECTS('PROCEDURE',NULL,NULL)");
	logger.info("PASSED: create SP revalidated with table executed successfully");
}
	@Test(description="Test execute SP revalidated DDL function")
	public void testCreateSPRevalidateExecuteDDL() throws InterruptedException
	{
		logger.info( "Test case of executing SP revalidated DDL..." );
		sqlEditorService.runSQLinJDBC("CALL SYSPROC.ADMIN_REVALIDATE_DB_OBJECTS('PROCEDURE',NULL,NULL)", ";", this.dbProfile, true);
		logger.info("PASSED:the ddl of creating SP revalidated with table executed successfully");
	}
	/**
	 * add by ylwh@cn.ibm.com
	 * trigger revalidate function
	 */
	private void testCreateTriggerRevalidate_precondition(){
		dropTriggerRevalENV(false);
		createTriggerRevalENV(true);
	}
	private void createTriggerRevalENV(boolean real) {
		// TODO Auto-generated method stub
		List<String> sqlList = new ArrayList<String>();
		sqlList.add("CREATE SCHEMA  \"TESTTRIGGERREVAL\";");
		sqlList.add("CREATE TABLE \"TESTTRIGGERREVAL\".\"T1\" ( A1 INTEGER NOT NULL, A2 CHAR(5) NOT NULL );");
		sqlList.add("ALTER TABLE \"TESTTRIGGERREVAL\".\"T1\" ADD CONSTRAINT A1_CONS PRIMARY KEY (A1);");	
		sqlList.add("CREATE TABLE \"TESTTRIGGERREVAL\".\"T2\" ( B1 INTEGER NOT NULL, B2 CHAR(5) NOT NULL );");
		sqlList.add("ALTER TABLE \"TESTTRIGGERREVAL\".\"T2\" ADD CONSTRAINT B1_CONS PRIMARY KEY (B1) ;");
		sqlList.add("CREATE TRIGGER \"TESTTRIGGERREVAL\".\"TRIGGERONR\" AFTER DELETE ON \"TESTTRIGGERREVAL\".\"T1\" "+
					"FOR EACH ROW UPDATE \"TESTTRIGGERREVAL\".\"T2\" SET B2=(SELECT A2 FROM \"TESTTRIGGERREVAL\".\"T1\" "+
				    "WHERE \"TESTTRIGGERREVAL\".\"T1\".\"A1\"=\"TESTTRIGGERREVAL\".\"T2\".\"B2\" ); ");
		sqlList.add("DROP TABLE \"TESTTRIGGERREVAL\".\"T2\";");
		sqlList.add("CREATE TABLE \"TESTTRIGGERREVAL\".\"T2\" ( B1 INTEGER NOT NULL, B2 CHAR(5) NOT NULL );");
		sqlEditorService.runSQLinJDBC(sqlList, ";", this.dbProfile,real);
	}

	private void testCreateTriggerRevalidate_clean(){
		dropTriggerRevalENV(false);
	}
	private void dropTriggerRevalENV(boolean real) {
		// TODO Auto-generated method stub
		List<String> sqlList = new ArrayList<String>();
		sqlList.add("DROP TABLE \"TESTTRIGGERREVAL\".\"T1\";");
		sqlList.add("DROP TABLE \"TESTTRIGGERREVAL\".\"T2\";");
		sqlList.add("DROP TRIGGER \"TESTTRIGGERREVAL\".\"TRIGGERONR\";");
		sqlList.add("DROP SCHEMA \"TESTTRIGGERREVAL\" RESTRICT;");	
		sqlEditorService.runSQLinJDBC(sqlList, ";", this.dbProfile,real);
	}
	/**
	 * Test create trigger revalidated with table function
	 * */
	@Test(description="Test create trigger revalidated with table function")
	public void testCreateTriggerRevalidate() throws InterruptedException{
	
	logger.info("Running create trigger revalidated with table test...");

	JSONObject responseData=getResponseData(AdminTAService.TA_REVALIDATE_COMMAND,"getTriggerRevalidate_LUW",false);

	/**
	 * Path the response data, for this case, the response data is:
	 * {"items":[{"statement":"CALL SYSPROC.ADMIN_REVALIDATE_DB_OBJECTS('TRIGGER',NULL,NULL)"}]}
	 * */
	String ddl = JsonPath.read(responseData, "$.items[0].statement");
	Assert.assertEquals(ddl,"CALL SYSPROC.ADMIN_REVALIDATE_DB_OBJECTS('TRIGGER',NULL,NULL)");
	logger.info("PASSED: create trigger revalidated with table executed successfully");
}
	@Test(description="Test execute trigger revalidated DDL function")
	public void testCreateTriggerRevalidateExecuteDDL() throws InterruptedException
	{
		logger.info( "Test case of executing trigger revalidated DDL..." );
		sqlEditorService.runSQLinJDBC("CALL SYSPROC.ADMIN_REVALIDATE_DB_OBJECTS('TRIGGER',NULL,NULL)", ";", this.dbProfile, true);
		logger.info("PASSED:the ddl of creating trigger revalidated with table executed successfully");
	}
	/**
	 * @author ylwh@cn.ibm.com
	 * UDF revalidate function
	 */
	private void testCreateUDFRevalidate_precondition(){
		dropUDFRevalENV(false);
		createUDFRevalENV(true);
	}
	private void createUDFRevalENV(boolean real) {
		// TODO Auto-generated method stub
		List<String> sqlList = new ArrayList<String>();
		sqlList.add("CREATE SCHEMA TESTUDFREVAL;");
		sqlList.add("CREATE TABLE TESTUDFREVAL.T1( A1 INTEGER NOT NULL, A2 CHAR(5) NOT NULL ,A3 INTEGER);");
		sqlList.add("CREATE FUNCTION TESTUDFREVAL.UDFREVAL(F1 CHAR(5)) "+
					"RETURNS TABLE(B1 INTEGER,B2 INTEGER) "+
					"LANGUAGE SQL "+
					"READS SQL DATA "+
					"NO EXTERNAL ACTION "+
					"SPECIFIC UDF1 "+
					"RETURN SELECT A1,A3 FROM TESTUDFREVAL.T1 WHERE A2=F1;");	
		sqlList.add("DROP TABLE TESTUDFREVAL.T1;");
		sqlList.add("CREATE TABLE TESTUDFREVAL.T1( A1 INTEGER NOT NULL, A2 CHAR(5) NOT NULL ,A3 INTEGER);");
		sqlEditorService.runSQLinJDBC(sqlList, ";", this.dbProfile,real);
	}

	private void testCreateUDFRevalidate_clean(){
		dropUDFRevalENV(false);
	}
	private void dropUDFRevalENV(boolean real) {
		// TODO Auto-generated method stub
		List<String> sqlList = new ArrayList<String>();
		sqlList.add("DROP FUNCTION TESTUDFREVAL.UDFREVAL;");
		sqlList.add("DROP TABLE TESTUDFREVAL.T1;");
		sqlList.add("DROP SCHEMA TESTUDFREVAL RESTRICT;");	
		sqlEditorService.runSQLinJDBC(sqlList, ";", this.dbProfile,real);
	}
	/**
	 * Test create UDF revalidated with table function
	 * */
	@Test(description="Test create UDF revalidated with table function")
	public void testCreateUDFRevalidate() throws InterruptedException{
	
	logger.info("Running create UDF revalidated with table test...");

	JSONObject responseData=getResponseData(AdminTAService.TA_REVALIDATE_COMMAND,"getUDFRevalidate_LUW",false);

	/**
	 * Path the response data, for this case, the response data is:
	 * {"items":[{"statement":"CALL SYSPROC.ADMIN_REVALIDATE_DB_OBJECTS('FUNCTION',NULL,NULL)"}]}
	 * */
	String ddl = JsonPath.read(responseData, "$.items[0].statement");
	Assert.assertEquals(ddl,"CALL SYSPROC.ADMIN_REVALIDATE_DB_OBJECTS('FUNCTION',NULL,NULL)");
	logger.info("PASSED: create UDF revalidated with table executed successfully");
}
	@Test(description="Test execute UDF revalidated DDL function")
	public void testCreateUDFRevalidateExecuteDDL() throws InterruptedException
	{
		logger.info( "Test case of executing UDF revalidated DDL..." );
		sqlEditorService.runSQLinJDBC("CALL SYSPROC.ADMIN_REVALIDATE_DB_OBJECTS('FUNCTION',NULL,NULL)", ";", this.dbProfile, true);
		logger.info("PASSED:the ddl of creating UDF revalidated with table executed successfully");
	}
	/**
	 * @author ylwh@cn.ibm.com
	 * view revalidate function
	 */
	public void testCreateViewRevalidate_preconditon(){
		dropViewRevalENV(false);
		createViewRevalENV();
	}
	private void createViewRevalENV() {
		// TODO Auto-generated method stub
		List<String> sqlList = new ArrayList<String>();
		sqlList.add("CREATE SCHEMA TESTVIEWREVAL;");
		sqlList.add("CREATE TABLE TESTVIEWREVAL.T1( "+
					 "C1  INTEGER NOT NULL, "+
					 "C2  CHAR(5) NOT NULL, "+
					 "C3  VARCHAR(5), "+
					 "C4  INTEGER NOT NULL);");
		sqlList.add("CREATE VIEW TESTVIEWREVAL.VIEW_T1(C1,C2,C4) AS SELECT C1,C2,C4 FROM TESTVIEWREVAL.T1;");	
		sqlList.add("DROP TABLE TESTVIEWREVAL.T1;");
		sqlList.add("CREATE TABLE TESTVIEWREVAL.T1( "+
				 "C1  INTEGER NOT NULL, "+
				 "C2  CHAR(5) NOT NULL, "+
				 "C3  VARCHAR(5), "+
				 "C4  INTEGER NOT NULL);");
		sqlEditorService.runSQLinJDBC(sqlList, ";", this.dbProfile,true);
	}
	public void testCreateViewRevalidate_clean(){
		dropViewRevalENV(true);
	}
	public void dropViewRevalENV(boolean real){
		List<String> sqlList = new ArrayList<String>();
		sqlList.add("DROP VIEW TESTVIEWREVAL.VIEW_T1;");
		sqlList.add("DROP TABLE TESTVIEWREVAL.T1;");
		sqlList.add("DROP SCHEMA TESTVIEWREVAL RESTRICT;");
		sqlEditorService.runSQLinJDBC(sqlList, ";", this.dbProfile,real);
	}
	/**
	 * Test create view revalidated with table function
	 * */
	@Test(description="Test create view revalidated with table function")
	public void testCreateViewRevalidate() throws InterruptedException{
	
	logger.info("Running create view revalidated with table test...");

	JSONObject responseData=getResponseData(AdminTAService.TA_REVALIDATE_COMMAND,"getViewRevalidate_LUW",false);

	/**
	 * Path the response data, for this case, the response data is:
	 * {"items":[{"statement":"CALL SYSPROC.ADMIN_REVALIDATE_DB_OBJECTS('VIEW',NULL,NULL)"}]}
	 * */
	String  ddl = JsonPath.read(responseData, "$.items[0].statement");
	Assert.assertEquals(ddl,"CALL SYSPROC.ADMIN_REVALIDATE_DB_OBJECTS('VIEW',NULL,NULL)");
	logger.info("PASSED: create view revalidated with table executed successfully");
}
	@Test(description="Test execute UDF revalidated DDL function")
	public void testCreateViewRevalidateExecuteDDL() throws InterruptedException
	{
		logger.info( "Test case of executing view revalidated DDL..." );
		sqlEditorService.runSQLinJDBC("CALL SYSPROC.ADMIN_REVALIDATE_DB_OBJECTS('VIEW',NULL,NULL)", ";", this.dbProfile, true);
		logger.info("PASSED:the ddl of creating view revalidated with table executed successfully");
	}
}
