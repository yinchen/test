package com.ibm.datatools.ots.tests.restapi.admin;

import java.util.Iterator;
import java.util.Map.Entry;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.ibm.datatools.ots.tests.restapi.base.GetMonitorDBHost;
import com.ibm.datatools.ots.tests.restapi.common.ConnectionTestService;

import net.sf.json.JSONObject;

public class AdminTestBase {

	protected String dbProfile = null ;
	protected String instanceProfile = null ;
	protected String userID = null ;
	protected String password = null ;
	protected String hostName = null ;
	protected String version = null ;
	protected Boolean isBVT = false;

	/**
	 * This is the method we used to initialize the dbProfile name, the dbProfile name will be saved to class variable, and sub-class could use this dbProfilename directly.
	 * Use instanceProfile and password to set credential in repoDB
	 * */
	@BeforeClass
	@Parameters({"dbProfile","version","password","isBVT"})
	public void beforeTest(@Optional("")String dbProfile,
			@Optional("")String version,@Optional("")String password,@Optional("")String isBVT,ITestContext context){
		this.dbProfile = this.getDBProfileName(dbProfile, context);
		this.password = this.getPassword(password, context);
		this.version = this.getVersion(version, context);
		this.isBVT = this.getIsBVT(isBVT, context);
		//the hostName, userID and instanceProfile are retrieved from DSM server connection tab 
		GetMonitorDBHost monitorDBDetail = new GetMonitorDBHost(dbProfile);
		this.userID = monitorDBDetail.getMonitorDBUser();
		this.instanceProfile = this.userID;		
		this.hostName = monitorDBDetail.getMonitorDBHost();
		
		Assert.assertNotNull(this.hostName, "host name can not be null!");
		Assert.assertNotNull(this.userID, "user name can not be null!");
		Assert.assertNotNull(this.password, "password can not be null!");
		//Save the credential info into the repo DB to avoid admin case fail
		setCredentialToDB(this.dbProfile, this.userID, this.password);
	}

	private void setCredentialToDB(String dbProf, String user, String pwd) {
		ConnectionTestService connTestServices = new ConnectionTestService();
		JSONObject result =connTestServices.setAndTestCredential(dbProf, "database", user, pwd, "false");
		// Verify the result code
		String resultCode = result.getString("ResponseCode");
		Assert.assertEquals(resultCode, "200");

		// Verify the result content
		String resultContent = result.getString("ResponseData");
		String expectedResult = connTestServices.getXMLData("testSetAddTestCredentialExpected");
		connTestServices.verifyXMLResult(resultContent, expectedResult);
	}

	/**
	 * Utils method to retrieve the dbProfile name transform in from Params in suite, if a test suite A is referenced by another test
	 * suite B, invoking parent suite B, the dbProfileName defined in B can not transform into A, using this method to solve this problem.  
	 * */
	public String getVersion(String version,ITestContext context){
		String _version = null ;
		if(context.getSuite() != null 
				&& context.getSuite().getXmlSuite() != null
				&& context.getSuite().getXmlSuite().getParentSuite() != null){
			_version = context.getSuite().getXmlSuite().getParentSuite().getParameter("version");
		}
		
		if(_version != null){
			return _version ;
		}else if(version != null && !version.equals("")){
			return version ;
		}else{
			return System.getProperty("version") ;
		}
	}
	
	/**
	 * Utils method to retrieve the dbProfile name transform in from Params in suite, if a test suite A is referenced by another test
	 * suite B, invoking parent suite B, the dbProfileName defined in B can not transform into A, using this method to solve this problem.  
	 * */
	public String getDBProfileName(String dbProfile,ITestContext context){
		String dbProfileName = null ;
		if(context.getSuite() != null 
				&& context.getSuite().getXmlSuite() != null
				&& context.getSuite().getXmlSuite().getParentSuite() != null){
			dbProfileName = context.getSuite().getXmlSuite().getParentSuite().getParameter("dbProfile");
		}
		
		if(dbProfileName != null){
			return dbProfileName ;
		}else if(dbProfile != null && !dbProfile.equals("")){
			return dbProfile ;
		}else{
			return System.getProperty("dbProfile") ;
		}
	}
	
	/**
	 * Utils method to retrieve the userID transform in from Params in suite
	 * */
	public String getUserID(String userID,ITestContext context){
		String _userID = null ;
		if(context.getSuite() != null 
				&& context.getSuite().getXmlSuite() != null
				&& context.getSuite().getXmlSuite().getParentSuite() != null){
			_userID = context.getSuite().getXmlSuite().getParentSuite().getParameter("userID");
		}
		
		if(_userID != null){
			return _userID ;
		}else if(userID != null && !userID.equals("")){
			return userID ;
		}else{
			return System.getProperty("userID") ;
		}
	}
	
	/**
	 * Utils method to retrieve the password transform in from Params in suite
	 * */
	public String getPassword(String password,ITestContext context){
		String _password = null ;
		if(context.getSuite() != null 
				&& context.getSuite().getXmlSuite() != null
				&& context.getSuite().getXmlSuite().getParentSuite() != null){
			_password = context.getSuite().getXmlSuite().getParentSuite().getParameter("password");
		}
		
		if(_password != null){
			return _password ;
		}else if(password != null && !password.equals("")){
			return password ;
		}else{
			return System.getProperty("password") ;
		}
	}

	/**
	 * Utils method to retrieve the instanceProfile name transform in from
	 * Params in suite, if a test suite A is referenced by another test suite B,
	 * invoking parent suite B, the instanceProfile defined in B can not
	 * transform into A, using this method to solve this problem.
	 **/
	public String getInstanceProfile(String instanceProfile,ITestContext context) {
		String _instanceProfile = null;
		if (context.getSuite() != null && context.getSuite().getXmlSuite() != null
				&& context.getSuite().getXmlSuite().getParentSuite() != null) {
			_instanceProfile = context.getSuite().getXmlSuite().getParentSuite().getParameter("instanceProfile");
		}

		if (_instanceProfile != null) {
			return _instanceProfile;
		} else if (instanceProfile != null && !instanceProfile.equals("")) {
			return instanceProfile;
		} else {
			return System.getProperty("instanceProfile");
		}
	}

	/**
	 * Utils method to retrieve the hostName transform in from Params in suite,
	 * if a test suite A is referenced by another test suite B, invoking parent
	 * suite B, the hostName defined in B can not transform into A, using this
	 * method to solve this problem.
	 **/
	public String getHostName(String hostName,ITestContext context) {
		String _hostName = null;
		if (context.getSuite() != null && context.getSuite().getXmlSuite() != null
				&& context.getSuite().getXmlSuite().getParentSuite() != null) {
			_hostName = context.getSuite().getXmlSuite().getParentSuite().getParameter("hostName");
		}

		if (_hostName != null) {
			return _hostName;
		} else if (hostName != null && !hostName.equals("")) {
			return hostName;
		} else {
			return System.getProperty("hostName");
		}
	}

	/**
	 * Utils method to retrieve the isBVT transform in from Params in suite
	 * The isBVT is used to determine whether a pre-condition method should run
	 * Skip unused pre-condition funtion to reduce bvt time
	 * */
	public Boolean getIsBVT(String isBVT, ITestContext context){
		String _isBVTn = null ;
		if(context.getSuite() != null 
				&& context.getSuite().getXmlSuite() != null
				&& context.getSuite().getXmlSuite().getParentSuite() != null){
			_isBVTn = context.getSuite().getXmlSuite().getParentSuite().getParameter("isBVT");
		}
		if (_isBVTn != null) {
			return "true".equalsIgnoreCase(_isBVTn);
		} else if (isBVT != null) {
			return "true".equalsIgnoreCase(isBVT);
		}
		return false;
	}
	
	protected boolean compareJsons( JSONObject base, JSONObject result ) 
	{
		if ( base.compareTo( result ) == 0 )
		{
			return true;
		}
		Iterator its = base.entrySet().iterator();
		while ( its.hasNext() )
		{
			Entry entry = (Entry)its.next();
			String key = (String)entry.getKey();
			if ( result.containsKey( key ) )
			{
				String baseValue = base.get( key ).toString();
				String resultValue = result.get( key ).toString();
				if ( baseValue.equals( resultValue ) )
				{
					continue;
				}
				else if (key.equalsIgnoreCase( "UID" ) || key.equalsIgnoreCase( "PRECISION" ) || key.equalsIgnoreCase( "BIND_TIME" ) || key.equalsIgnoreCase( "REFRESH_TIME" ) || key.equalsIgnoreCase( "ALTER_TIME" ) || key.equalsIgnoreCase( "CREATE_TIME" ) || key.equalsIgnoreCase( "CONSTCREATOR" ) || key.equalsIgnoreCase( "STATS_TIME" ))
				{
					continue;
				}
				else
				{
					Assert.fail( "Suppose value of key " + key + " is " + baseValue + ",but current result is "
							+ resultValue );
					return false;
				}
			}
			else
			{
				Assert.fail( "No Key:" + key );
				return false;
			}

		}
		return true;
	}
	
	protected int compareToVersion(String ver1, String ver2){
		if(ver1 != null && ver1.length() > 0 && ver2 != null && ver2.length() > 0){
			String[] ver1Arr =  ver1.substring(1, ver1.length()).split("\\.");
			String[] ver2Arr =  ver2.substring(1, ver2.length()).split("\\.");
			int ver1Int = 0, ver2Int = 0;
			for(int i = 0; i< ver1Arr.length; i++){
				int ver = Integer.parseInt(ver1Arr[i]);
				ver1Int += ver * Math.pow(10, (ver1Arr.length - i -1));
			}
			for(int i = 0; i< ver2Arr.length; i++){
				int ver = Integer.parseInt(ver2Arr[i]);
				ver2Int += ver * Math.pow(10, (ver2Arr.length - i -1));
			}
			return ver1Int - ver2Int;
		}
		return -1;
	}
}
