package com.ibm.datatools.ots.tests.restapi.admin.fvt;

import org.testng.Assert;

import com.ibm.datatools.ots.tests.restapi.admin.AdminTestBase;
import com.ibm.datatools.ots.tests.restapi.common.AdminNavigationService;
import com.ibm.datatools.ots.tests.restapi.common.AdminSQLEditorService;
import com.jayway.jsonpath.JsonPath;

import net.sf.json.JSONObject;

public class AdminNavigationTest extends AdminTestBase {

	protected AdminNavigationService navigationService = null;
	protected AdminSQLEditorService sqlEditorService = null;

	protected void getTestingResult(String queryProperties, String propertiesResult) {

		String query = navigationService.getJSONData(queryProperties);

		JSONObject resObj = navigationService.callNavigationService(AdminNavigationService.TYPE_NAVIGATION_URL, query,
				dbProfile);

		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);

		JSONObject propResult = JsonPath.read(responseData, "$.items[0].data");

		if (propResult == null) {
			propResult = responseData;
		}

		String base = navigationService.getJSONData(propertiesResult);

		JSONObject propBase = JSONObject.fromObject(base);

		Assert.assertTrue(compareJsons(propBase, propResult));
	}
}
