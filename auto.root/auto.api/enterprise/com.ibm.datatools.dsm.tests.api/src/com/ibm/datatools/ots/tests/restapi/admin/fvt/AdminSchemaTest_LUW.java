package com.ibm.datatools.ots.tests.restapi.admin.fvt;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.AdminNavigationService;
import com.ibm.datatools.ots.tests.restapi.common.AdminSQLEditorService;
import com.jayway.jsonpath.JsonPath;

import net.sf.json.JSONObject;

public class AdminSchemaTest_LUW extends AdminNavigationTest {

	private static final Logger logger = LogManager.getLogger(AdminSchemaTest_LUW.class);

	@BeforeClass
	public void beforeTest() {
		navigationService = new AdminNavigationService();
		sqlEditorService = new AdminSQLEditorService();
		drop101Schemas();
		create101Schemas();
	}

	@AfterClass
	public void afterTest() {
		drop101Schemas();
	}

	private void create101Schemas() {
		List<String> createList = new ArrayList<String>();

		for (int i = 0; i <= 100; i++) {
			createList.add("CREATE SCHEMA SCHEMA_TEST" + i + ";");
		}

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}

	private void drop101Schemas() {
		List<String> createList = new ArrayList<String>();

		for (int i = 0; i <= 100; i++) {
			createList.add("Drop SCHEMA SCHEMA_TEST" + i + " RESTRICT;");
		}

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, false);
	}

	/**
	 * Test the tables Schemas list
	 */
	@Test(description = "DB has at least 100 schemas, open schema list with maxrows=100, verify request that retrieves schemas")
	public void testNavigateSchemasList100() {

		logger.info("Running getting the Table's schema list test...");

		String query = navigationService.getJSONData("getNavigateSchema");

		JSONObject resObj = navigationService.callNavigationService(AdminNavigationService.TYPE_NAVIGATION_URL, query,
				dbProfile);

		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);

		Integer numRows = JsonPath.read(responseData, "$.numRows");
		Assert.assertEquals(numRows.toString(), "100");

		logger.info("PASSED: Get the Table's schemas list executed successfully");
	}

	/**
	 * Test the schema property.
	 */
	@Test(description = "Click schema to open its properties, verify request that returns Properties info")
	public void testNavigateSchemaProperty() {

		logger.info("Running Navigate Schema Property...");

		getTestingResult("getNavigateSchemaProperty", "getNavigateSchemaProperty_result");

		logger.info("PASSED: Get the Schema's properties executed successfully");
	}
}
