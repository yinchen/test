package com.ibm.datatools.ots.tests.restapi.admin.fvt;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.AdminNavigationService;
import com.ibm.datatools.ots.tests.restapi.common.AdminSQLEditorService;
import com.jayway.jsonpath.JsonPath;

import net.sf.json.JSONObject;

public class AdminTableSpaceTest_LUW extends AdminNavigationTest {

	private static final Logger logger = LogManager.getLogger(AdminTableSpaceTest_LUW.class);

	@BeforeClass
	public void beforeTest() {
		navigationService = new AdminNavigationService();
		sqlEditorService = new AdminSQLEditorService();
		drop101TableSpaces();
		create101TableSpaces();
	}

	@AfterClass
	public void afterTest() {
		drop101TableSpaces();
	}

	private void create101TableSpaces() {
		List<String> createList = new ArrayList<String>();

		for (int i = 0; i <= 100; i++) {
			createList.add("CREATE TABLESPACE TABLESPACETEST" + i + ";");
		}

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}

	private void drop101TableSpaces() {
		List<String> dropList = new ArrayList<String>();
		for (int i = 0; i <= 100; i++) {
			dropList.add("DROP TABLESPACE TABLESPACETEST" + i + ";");
		}
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);
	}

	/**
	 * Test the table space list.
	 */
	@Test(description = "DB has at least 100 tablespaces, open tablespace list with maxrows=100, verify request that retrieves tablespaces")
	public void testNavigateTableSpacesList100() {

		logger.info("Running Navigate Table Spaces list...");

		// The key defined in the properties
		String query = navigationService.getJSONData("getNavigateTableSpace");

		JSONObject resObj = navigationService.callNavigationService(AdminNavigationService.TYPE_NAVIGATION_URL, query,
				dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);

		Integer numRows = JsonPath.read(responseData, "$.numRows");
		Assert.assertEquals(numRows.toString(), "100");
	}

	/**
	 * Test the Table space property.
	 */
	@Test(description = "Click tablespace to open it properties, verify request that returns Properties info")
	public void testNavigateTableSpaceProperty() {

		logger.info("Running Navigate Table Space Property...");

		getTestingResult("getNavigateTableSpaceProperty", "getNavigateTableSpaceProperty_result");

		logger.info("PASSED: Get the Table Space's properties executed successfully");
	}
}
