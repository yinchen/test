package com.ibm.datatools.ots.tests.restapi.admin.fvt;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.AdminNavigationService;
import com.ibm.datatools.ots.tests.restapi.common.AdminSQLEditorService;
import com.jayway.jsonpath.JsonPath;

import net.sf.json.JSONObject;

public class AdminTableTest_LUW extends AdminNavigationTest {

	private static final Logger logger = LogManager.getLogger(AdminTableTest_LUW.class);

	@BeforeClass
	public void beforeTest() {
		navigationService = new AdminNavigationService();
		sqlEditorService = new AdminSQLEditorService();
		dropTable100Columns();
		createTable100Columns();
		drop101Tables();
		create101Tables();
	}

	@AfterClass
	public void afterTest() {
		dropTable100Columns();
		drop101Tables();
	}

	/**
	 * Test the table list
	 */
	@Test(description = "Test the tables list DB has at least 100 tables, open table list with maxrows=100, verify request that retrieves table")
	public void testNavigateTableList100() {

		logger.info("Running getting the Table's tables list test...");

		String query = navigationService.getJSONData("getNavigateTable");

		JSONObject resObj = navigationService.callNavigationService(AdminNavigationService.TYPE_NAVIGATION_URL, query,
				dbProfile);

		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);

		Integer numRows = JsonPath.read(responseData, "$.numRows");
		Assert.assertEquals(numRows.toString(), "100");

		logger.info("PASSED: Get the Table's tables list executed successfully");
	}

	/**
	 * Test the table properties
	 */
	@Test(description = "Test the tables properites Click table name to open its properties, verify request that returns response data for Properties page")
	public void testNavigateTableProperties() {

		logger.info("Running getting the Table's properties test...");

		getTestingResult("getNavigateTableProperty", "getNavigateTableProperty_result");

		logger.info("PASSED: Get the Table's properties executed successfully");
	}

	/**
	 * Test the table columns
	 */
	@Test(description = "Test the tables columns Click table name to open it, then click Columns, verify request that returns all columns, table has 100 columns")
	public void testNavigateTableColumns100() {

		logger.info("Running getting the Table's columns test...");

		String query = navigationService.getJSONData("getNavigateTableColumn");

		JSONObject resObj = navigationService.callNavigationService(AdminNavigationService.TYPE_NAVIGATION_URL, query,
				dbProfile);

		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);

		Integer numRows = JsonPath.read(responseData, "$.numRows");
		Assert.assertEquals(numRows.toString(), "100");

		logger.info("PASSED: Get the Table's columns executed successfully");
	}

	private void createTable100Columns() {
		String sqlStatement = null;
		sqlStatement = "create table Col100Table (";
		String fieldsSql = null;
		for (int i = 1; i <= 100; i++) {
			if (i == 1) {
				fieldsSql = "field1 int";
			} else {
				fieldsSql = fieldsSql + ",field" + i + " int";
			}
		}
		sqlStatement = sqlStatement + fieldsSql + ");";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile);
	}

	private void dropTable100Columns() {
		sqlEditorService.runSQLinJDBC("Drop table Col100Table;", ";", this.dbProfile);
	}

	private void create101Tables() {
		List<String> createList = new ArrayList<String>();

		for (int i = 0; i <= 100; i++) {
			createList.add("CREATE TABLE Table" + i + "(field1 int);");
		}

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}

	private void drop101Tables() {
		List<String> createList = new ArrayList<String>();

		for (int i = 0; i <= 100; i++) {
			createList.add("Drop TABLE Table" + i + ";");
		}

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, false);
	}
}
