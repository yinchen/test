package com.ibm.datatools.ots.tests.restapi.admin.fvt;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.AdminNavigationService;
import com.ibm.datatools.ots.tests.restapi.common.AdminSQLEditorService;
import com.ibm.datatools.test.utils.AdminConst;
import com.jayway.jsonpath.JsonPath;

import net.sf.json.JSONObject;

public class AdminTriggerTest_LUW extends AdminNavigationTest {

	private static final Logger logger = LogManager.getLogger(AdminTriggerTest_LUW.class);

	@BeforeClass
	public void beforeTest() {
		navigationService = new AdminNavigationService();
		sqlEditorService = new AdminSQLEditorService();
		dropStaffsTable();
		createStaffsTable();
		drop101Triggers();
		create101Triggers();
	}

	@AfterClass
	public void afterTest() {
		drop101Triggers();
		dropStaffsTable();
	}

	private void create101Triggers() {
		String sql;
		for (int i = 0; i <= 100; i++) {
			sql = "CREATE TRIGGER TRIGGER_" + i
					+ " NO CASCADE BEFORE DELETE ON staff1 REFERENCING OLD AS oldstaff FOR EACH ROW MODE DB2SQL WHEN(oldstaff.job = 'Sales') BEGIN ATOMIC SIGNAL SQLSTATE '75000' ('Sales staff cannot be deleted... see the DO_NOT_DEL_SALES trigger.'); END";
			sqlEditorService.runSQLinJDBC(sql, null, this.dbProfile, true);

		}
	}

	private void drop101Triggers() {
		List<String> createList = new ArrayList<String>();

		for (int i = 0; i <= 100; i++) {
			createList.add("DROP TRIGGER TRIGGER_" + i);
		}

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, false);
	}

	private void createStaffsTable() {
		String sql = "CREATE TABLE STAFF1 (JOB CHAR(5 OCTETS))";
		if (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_97)
				|| (this.version.equalsIgnoreCase(AdminConst.DB_VERSION_LUW_101))) {
			sql = "CREATE TABLE STAFF1 (JOB CHAR(5))";
		}
		sqlEditorService.runSQLinJDBC(sql, null, this.dbProfile, true);
	}

	private void dropStaffsTable() {
		String sql = "DROP TABLE STAFF1";
		sqlEditorService.runSQLinJDBC(sql, ";", this.dbProfile, false);
	}

	/**
	 * Test the trigger
	 */
	@Test(description = "Verify request that retrieves triggers with maxrows=100, and DB has 100 or more than 100 triggers")
	public void testNavigateTriggerList100() {

		logger.info("Running Navigate User Defined type List test...");

		String query = navigationService.getJSONData("getNavigateTrigger");
		JSONObject resObj = navigationService.callNavigationService(AdminNavigationService.TYPE_NAVIGATION_URL, query,
				dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);
		Assert.assertEquals(JsonPath.read(responseData, "resultCode"), "SUCCESS");
		Assert.assertEquals(JsonPath.read(responseData, "numRows").toString(), "100");
		logger.info("PASSED: The retrieve Trigger List layout function test executed successfully");
	}

	/**
	 * Test the Trigger
	 */
	@Test(description = "Verify request that views trigger properties")
	public void testViewTriggerProperties() {
		logger.info("Running View Trigger Property...");

		getTestingResult("getNavigateTriggerProperty", "getNavigateTriggerProperty_result");

		logger.info("PASSED: Testing of finding out properties of a trigger executed successfully");
	}
}
