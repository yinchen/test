package com.ibm.datatools.ots.tests.restapi.admin.fvt;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.AdminNavigationService;
import com.ibm.datatools.ots.tests.restapi.common.AdminSQLEditorService;
import com.jayway.jsonpath.JsonPath;

import net.sf.json.JSONObject;

public class AdminViewTest_LUW extends AdminNavigationTest {

	private static final Logger logger = LogManager.getLogger(AdminViewTest_LUW.class);

	@BeforeClass
	public void beforeTest() {
		navigationService = new AdminNavigationService();
		sqlEditorService = new AdminSQLEditorService();
		dropTable100Columns();
		drop101Views();
		createTable100Columns();
		create101Views();
	}

	@AfterClass
	public void afterTest() {
		dropTable100Columns();
		drop101Views();
	}

	private void createTable100Columns() {
		String sqlStatement = null;
		sqlStatement = "create table Col100Table (";
		String fieldsSql = null;
		for (int i = 1; i <= 100; i++) {
			if (i == 1) {
				fieldsSql = "field1 int";
			} else {
				fieldsSql = fieldsSql + ",field" + i + " int";
			}
		}
		sqlStatement = sqlStatement + fieldsSql + ");";

		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile);
	}

	private void dropTable100Columns() {
		sqlEditorService.runSQLinJDBC("Drop table Col100Table;", ";", this.dbProfile);
	}

	private void create101Views() {
		List<String> createList = new ArrayList<String>();
		createList.add("CREATE VIEW VWTEST0 AS (SELECT * FROM Col100Table);");

		for (int i = 1; i <= 100; i++) {
			createList.add("CREATE VIEW VWTEST" + i + " AS (SELECT FIELD" + i + " FROM VWTEST0);");
		}

		sqlEditorService.runSQLinJDBC(createList, ";", this.dbProfile, true);
	}

	private void drop101Views() {
		List<String> dropList = new ArrayList<String>();
		for (int i = 0; i <= 100; i++) {
			dropList.add("DROP VIEW VWTEST" + i + ";");
		}
		sqlEditorService.runSQLinJDBC(dropList, ";", this.dbProfile, false);
	}

	/**
	 * Test the views list
	 */
	@Test(description = "DB has at least 100 views open view list with maxrows=100, verify request that retrieves views")
	public void testNavigateViewsList100() {

		logger.info("Running Navigate Views list...");

		// The key defined in the properties
		String query = navigationService.getJSONData("getNavigateView");

		JSONObject resObj = navigationService.callNavigationService(AdminNavigationService.TYPE_NAVIGATION_URL, query,
				dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");
		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);

		Integer numRows = JsonPath.read(responseData, "$.numRows");
		Assert.assertEquals(numRows.toString(), "100");
	}

	/**
	 * Test the view property
	 */
	@Test(description = "Click a view to open its properties, verify request that returns Properties info")
	public void testNavigateViewProperty() {

		logger.info("Running Navigate View Property...");

		getTestingResult("getNavigateViewProperty", "getNavigateViewProperty_result");

		logger.info("PASSED: Get the View's properties executed successfully");
	}
}
