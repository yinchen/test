package com.ibm.datatools.ots.tests.restapi.alerts;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckMonitorDB;
import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.common.RestAPIBaseTest;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class TestAlerts {
	
	protected TuningCommandService cs = null;
	private String ssoCookie = null;
	private JSONArray alertsArr = null;
	protected String dbName = null;
	private String repoDBType = null;
	
	@BeforeTest
	public void beforeTest(){
		
		cs = new TuningCommandService();
		RestAPIBaseTest.loginDSM();
		
		//check repo db exist
		String repodbInfo = cs.checkRepoDBExist();
		String repoDBStart = "dataServerType=\"";
		String repoDBEnd = "\" user=";
		repoDBType = repodbInfo.substring(repodbInfo.indexOf(repoDBStart)+repoDBStart.length(), repodbInfo.indexOf(repoDBEnd)).trim();
		System.out.println("Repo db type is : " + repoDBType);
		
		if(!repodbInfo.contains("repDBConnInfo")){
			Assert.fail("No repo db added in this build...");
		}
		
		//check connections exist
		JSONObject connsObj = cs.getAllConns();
		String data = connsObj.getString("data");
		String subData = data.substring(1, data.length()-1);
		JSONArray connsArray = JSONArray.fromObject(subData);
		int connections = connsArray.size() -1;
		if(connections == 0){
			Assert.fail("No connections found in connection list...");
		}
		
		//get SSOCookie to pass the authentication
		String handleSSO = cs.handleSSOahth(Configuration.getProperty("LoginUser"),
											Configuration.getProperty("LoginPassword"));
		String start = "SSOCookie=";
		String end = ";Path=";
				
		ssoCookie = handleSSO.substring(handleSSO.indexOf(start)+start.length(), handleSSO.indexOf(end));
	}
	
	@Test
	public void testViewAlertsAll(){
		
		if(repoDBType.equals("DB2Z")){
			return;
		}
		
		//add in request to alert service as cookie named SSOCookie to pass the authentication
		String alerts = cs.viewAlertsAll(ssoCookie);
		alertsArr = JSONArray.fromObject(alerts);
		int alertsAll = alertsArr.size();
		System.out.println("There are "+alertsAll+" alerts displayed");
	}
	
	@Test
	public void testViewAlertsDetails(){
		
		if(repoDBType.equals("DB2Z")){
			return;
		}
		
		if(alertsArr.size() == 0){
			return;
		}
		
		List<String> idList = new ArrayList<>();
		
		for(int i=0; i<alertsArr.size(); i++){
			JSONObject alertObj = JSONObject.fromObject(alertsArr.get(i));
			String alertsID = alertObj.getString("id");
			idList.add(alertsID);
		}
		
		Random random = new Random();
		int ranNum = random.nextInt(idList.size()-1);
		
		JSONObject obj = cs.viewAlertsDetails(idList.get(ranNum),ssoCookie);
		System.out.println(obj);
		
	}
	
	@Test
	@Parameters("dbProfile")
	public void testViewAlertsFromDatabase(String dbName){
		
		if(repoDBType.equals("DB2Z")){
			return;
		}
		
		this.dbName = dbName;
		Assert.assertEquals(CheckMonitorDB.checkMonitorDBStatus(this.dbName),"success");
		JSONArray arr = cs.viewAlertsFromDatabase("alertsForDatabase?datasource="+dbName+"",ssoCookie);
		System.out.println(arr);
	}
	
	@Test
	public void testGetToken(){
		
		if(repoDBType.equals("DB2Z")){
			return;
		}
		
		String username = Configuration.getProperty("LoginUser");
		String password = Configuration.getProperty("LoginPassword");
		
		System.out.println("Token auth : " +username +":" + password);
		
		String token = cs.getAlertsToken(username,password);
		System.out.println("Alerts Token is : "+token);
		Assert.assertNotNull(token);
	}
	
}
