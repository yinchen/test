package com.ibm.datatools.ots.tests.restapi.base;

import org.testng.Assert;

import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;

public class CheckCredentialForTuning {
	
	public static void checkCredential(String dbName,String username,String password){
		
		TuningCommandService cs = new TuningCommandService();
		String setCredential =  cs.setAndTestCred(dbName, username, password);
		
		String start = "<IsAuthorized>";
		String end = "</IsAuthorized>";
		System.out.println("Set credential result : "+setCredential);
		if(setCredential.contains("ErrorMap")){
			Assert.fail("Set credential for db " +dbName+" failed");
		}
		
		String setCredentialResult = setCredential.substring(setCredential.indexOf(start) + start.length(), 
				setCredential.indexOf(end)).trim();
		System.out.println("Set credential result for db "+dbName+" is " + setCredentialResult);
		
		if(!setCredentialResult.equals("true")){
			Assert.fail("Set Credential failed");
		}
		
	}
	
}
