package com.ibm.datatools.ots.tests.restapi.base;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.RestAPIBaseTest;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


public class CheckDSUtilAllSH {
	
	private static final String beginIndex = "****Normal testing begin......";
	private static final String endIndex = "****Normal testing end.";
	private static final String checkIndex = "Command Run with Failture";
	private static final int connectionAmount = 4;
	private String outputFile = "allScript.out";
	
	TuningCommandService cs;
	Properties p;
	
	@BeforeTest
	public void beforeTest() throws FileNotFoundException, IOException {
		cs = new TuningCommandService();
		RestAPIBaseTest.loginDSM();
	}
	
	@Test
	public void checkLogFile() throws IOException{
		String path = Configuration.locateRootPath() + outputFile;
		
		//Check connection on list
		JSONObject afterExecList = cs.listAllMonitorDB();
		String afterExecListStr = afterExecList.toString();
		String dbName = "databaseName";
		
		int afterExecAmounts = countStr(afterExecListStr,dbName);
		System.out.println("There are " + afterExecAmounts + " connections in connection list after execute dsUtil.sh");
		
		//Delete 4 connections add by dsUtil.sh
		ArrayList<String> connectionList = new ArrayList<>();
		JSONArray array = afterExecList.getJSONArray("items");
		for(int i=0; i<array.size(); i++){
			String name = JSONObject.fromObject(array.get(i)).getString("name");
			connectionList.add(name);
		}
		
		for(int i=0; i<connectionList.size(); i++){
			String dbProfileName = connectionList.get(i);
			JSONObject delResult = cs.delConnections(dbProfileName);
			String resultCode = delResult.getString("resultCode");
			if("success".equals(resultCode)){
				System.out.println("Delete connection ["+dbProfileName+"] succeed...");
			}else{
				System.out.println("Delete connection ["+dbProfileName+"] failed...");
			}
		}
		
		if(afterExecAmounts != connectionAmount){
			Assert.assertEquals(afterExecAmounts, connectionAmount);
		}
		
		String logFile = readDataFormFile(path);
		int index1 = logFile.indexOf(beginIndex);
		int index2 = logFile.indexOf(endIndex);
		logFile = logFile.substring(index1, index2);
		if(logFile.contains(checkIndex)){
			Assert.fail(""+checkIndex+" are found in log file");
		}
	}
	
	public static String readDataFormFile(String fileName) throws IOException{
		try {
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			StringBuffer buffer = new StringBuffer();
			while(reader.ready()){
				buffer.append(reader.readLine() +"\r\n");
			}
			return buffer.toString();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	private static int counter = 0;
	public static int countStr(String str1, String str2) {
        if (str1.indexOf(str2) == -1) {
	        return 0;
        } else if (str1.indexOf(str2) != -1) {
	        counter++;
	        countStr(str1.substring(str1.indexOf(str2) +
                   str2.length()), str2);
               return counter;
        }
	        return 0;
    }
	
}
