package com.ibm.datatools.ots.tests.restapi.base;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.testng.Assert;

import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;

import net.sf.json.JSONObject;

public class CheckJobStatus {
	
	private static final String COMPLETED = "Completed";
	private static final String CANCELLED = "Cancelled";
	private static final String SUCCEEDED = "Succeeded";
	private static final String FAILED = "Failed";
	private static final int RUNJOBTIME = 1800;
	
	public static void checkResponseCode(JSONObject result,int responseCode){
		if(responseCode != 200){
			Assert.fail("FAILURE=> Response code =" + responseCode
					+ " for URL: " + result.get("URL") + ", POSTDATA:"
					+ result.get("PostData"));
		}
	}
	
	
	public static void checkJobStatus(String jobID,long time) throws InterruptedException, FileNotFoundException, IOException{
		TuningCommandService cs = new TuningCommandService();
		JSONObject status = null;
		
		long start = System.currentTimeMillis();
		boolean finished = false;
		int times = 0;
		
		while(!finished){
			Thread.sleep(time);
			status = cs.getJobStatus(jobID);
			System.out.println("Status of job ["+jobID+"] is : "+status);
			if(status == null){
				times++;
				System.out.println("Job is running...");
				while(times > 4){
					String msg = "Submit job ["+jobID+"] failure, it not found in job list !";
					System.out.println(msg);
					Assert.fail(msg);
					return;
				}
			}else if(!(COMPLETED.equals(status.getString("PROGRESS")) || CANCELLED.equals(status.getString("PROGRESS")))){
				System.out.println("Job is running...");
				//Assert.assertEquals(status.getString("STATUS"), "Started");
				long end = System.currentTimeMillis();
				if((end - start)/1000 > RUNJOBTIME){
					System.out.println("Job has run for "+(end - start)/1000 +"seconds");
					Assert.fail("Run job time out");
				}
			}else if ("Started".equals(status.getString("STATUS"))) {
				System.out.println("Job is running...");
			}else if (SUCCEEDED.equals(status.getString("STATUS"))) {
				System.out.println("Job successfully running...");
				Assert.assertEquals(status.getString("STATUS"), "Succeeded");
				finished = true;
			}else if (FAILED.equals(status.getString("STATUS"))) {
				System.out.println("Job running failure...");
				Assert.assertEquals(status.getString("STATUS"), "Failed");
				finished = true;
			}else if ("Canceled".equals(status.get("STATUS"))) {
				Assert.fail("Job cancelled , further operation could be failure");
			}else{
				System.out.println("=========Checks for unexpected issues=========");
				String progress = status.getString("PROGRESS");
				System.out.println("Exceptional progress : "+progress);
				System.out.println("JOB STATUS : "+status.getString("STATUS"));
			}
			
			
		}
		
		Assert.assertEquals(status.getString("PROGRESS"), "Completed");
	}

}




