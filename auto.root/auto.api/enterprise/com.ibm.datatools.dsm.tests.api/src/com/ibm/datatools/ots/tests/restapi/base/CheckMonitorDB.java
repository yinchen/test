package com.ibm.datatools.ots.tests.restapi.base;

import org.testng.Assert;

import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;

import net.sf.json.JSONObject;

public class CheckMonitorDB {
	
	public static String checkMonitorDBStatus(String dbName){
		TuningCommandService cs = new TuningCommandService();
		JSONObject result = cs.getMonitorDBDetails(dbName);
		String resultCode = result.getString("resultCode");
		
		if(result == null || "failure".equals(resultCode)){
			String errorMessage = "No monitor database connection named "+dbName+" found on monitor db list";
			System.out.println("ERROR MESSAGE : " + errorMessage);
			Assert.fail(errorMessage);
		}
		
		String response = result.getString("response");
		JSONObject jo = JSONObject.fromObject(response);
		
		System.out.println("connection details : "+result);
		
		String dataServerType = jo.getString("dataServerType");
		String host = jo.getString("host");
		host = "\"" + host + "\"";
		String port = jo.getString("port");
		String securityMechanism = jo.getString("securityMechanism");
		String user = jo.getString("user");
		String password = jo.getString("password");
		password = "\"" + password + "\"";
		String creator = jo.getString("creator");
		String URL = jo.getString("URL");
		URL = "\"" + URL + "\"";
		//String disableDataCollection = jo.getString("disableDataCollection");
		String operationUser = jo.getString("operationUser");
		String onetimePasswordEnable = jo.getString("onetimePasswordEnable");
		String saveOperationCredentials = jo.getString("saveOperationCredentials");
		//String isMonCredential = jo.getString("isMonCredential");
		
		String profileJSON = "";
		
		if("DB2Z".equals(dataServerType)){
			String location = jo.getString("location");
			profileJSON = "{\"name\":"+dbName+",\"dataServerType\":"+dataServerType+"," +
					"\"location\":"+location+",\"host\":"+host+",\"port\":"+port+","
					//+"\"disableDataCollection\":"+disableDataCollection+","
					+ "\"operationUser\":"+operationUser+","
					+ "\"operationPassword\":"+password+"," 
					+ "\"onetimePasswordEnable\":"+onetimePasswordEnable+"," 
					+ "\"saveOperationCredentials\":"+saveOperationCredentials+"," +
					"\"securityMechanism\":"+securityMechanism+"," +
					"\"creator\":"+creator+"," +
					"\"URL\":"+URL+"," +
					"\"dataServerType\":"+dataServerType+"}";
		}else if("DB2LUW".equals(dataServerType)){
			String dataServerExternalType = jo.getString("dataServerExternalType");
			String databaseName = jo.getString("databaseName");
			profileJSON = "{\"name\":"+dbName+","
					+ "\"dataServerExternalType\":"+dataServerExternalType+","
					+"\"databaseName\":"+databaseName+","
					+ "\"host\":"+host+","
					+ "\"port\":"+port+"," 
					//+ "\"disableDataCollection\":"+disableDataCollection+"," 
					+"\"securityMechanism\":"+securityMechanism+","
					+ "\"operationUser\":"+operationUser+"," 
					+ "\"operationPassword\":"+password+","
					+ "\"onetimePasswordEnable\":"+onetimePasswordEnable+"," 
					+ "\"saveOperationCredentials\":"+saveOperationCredentials+"," 
					+ "\"user\":"+user+"," 
					+"\"password\":"+password+"," 
					+"\"URL\":"+URL+"," 
					+"\"creator\":"+creator+"," 
					//+"\"isMonCredential\":"+isMonCredential+","
					+ "\"dataServerType\":"+dataServerType+"}";
		}
		
		String checkMonitorDBResult = cs.checkMonitorDBConnection(profileJSON);
		
		//String checkMonitorDBResult = result.getString("resultCode");
		
		return checkMonitorDBResult;
		
	}
	
}
