package com.ibm.datatools.ots.tests.restapi.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.TreeSet;

import com.ibm.db2.jcc.DB2SimpleDataSource;


public class Configuration {
	
	private static final String classesPath = "target/classes/";
	private static String prjRootPath = null;	
	
	public static final String allConfigFile = "runtimeconfig.properties";
	private static final String ConfigPath = "target/classes/";
	private static final String PostDataPath = "testData/postData/";
	private static final String ResponseDataPath = "testData/responseData/";
	private static final String uploadFilePath = "testData/postData/uploadFiles/";
	public static final String CommonConfigFile = "config_common.properties";
	public static final String CommonURLFile = "common_urls.properties";
	public static final String RepositoryDBFile = "config/repositorydb.properties";
	public static final String MigrationFile = "testData/postData/migration";
	public static final String TESTOUTPUTFILE = "/reg-output/test-output/";
	public static final String TestBuckets = "config/TestBuckets/";
	public static final String scr = "src/";
	
	private static Properties propAllConfig = null;
	private static Properties propDynamicConfig = null;
	
	public static final String DBConfigPath = "classes/databases/";
	public static final String CDSetupPath = "classes/CDSetup/";
	//public static final String OTSConfigPath = "classes/otsservers/";
	
	
	
	
	
	/**
	 * get a common configuration value specified by key prop
	 * 
	 * @param prop
	 * @return the property value is trimmed before return, if not found, null
	 *         returned
	 */
	public static synchronized String getProperty(String prop) {
		String rootPath = locateRootPath();
		try {
			if (propAllConfig == null) {
				createTempConfigurationFile();

				propAllConfig = openPropertyFile(rootPath + ConfigPath
						+ allConfigFile);
			}

			if (propAllConfig == null)
				return null;

			String value = propAllConfig.getProperty(prop);

			if (value != null){
				String dsmEnv = propAllConfig.getProperty("dsm.env");
				if("dev".equals(dsmEnv)){
					if("LoginURL".equals(prop)){
						return "/dswebcustomauth/handleauth.do";
					}
				}
				//	return value.trim().replace("/console", "");
				//} else {
				//	return value.trim();
				//}
				value = value.trim();
			}
			return value;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * This method return Properties object for configuration file config, if
	 * error, return a Properties object without any properties
	 * 
	 * @param config
	 * @return
	 */
	public static Properties openPropertyFile(String config) {
		Properties p = new Properties();
		
		try {
			InputStream in = new FileInputStream(config);
			p.load(in);
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return p;
	}
	
	/**
	 * Save all properties from CommonConfigFile,CURRENT_CONFIG_MONITORED_DB and
	 * propDynamicConfig into file allConfigFile
	 */
	private synchronized static void createTempConfigurationFile() {
		String fileName = allConfigFile;
		String configPath = getConfigurationPropertyFilePath();
		String saveFile = configPath + fileName;

		Properties propCommonConfig = openPropertyFile(prjRootPath + ConfigPath
				+ CommonConfigFile);

		/*Properties propMonitoredDatabaseConfig = openPropertyFile(prjRootPath
				+ DBConfigPath + getMonitoredDBFileName());*/

		/*Properties propOTSServerConfig = openPropertyFile(prjRootPath
				+ OTSConfigPath + getOTSServerFileName());*/
		
		Properties propCommonURLs = openPropertyFile(prjRootPath + ConfigPath
				+ CommonURLFile);
		
		
		/*Properties[] pss = { propCommonConfig, propMonitoredDatabaseConfig,propCommonURLs,
				propDynamicConfig };*/
		
		Properties[] pss = { propCommonConfig, propCommonURLs,
				propDynamicConfig };
		Properties propAll = mergePropertiesObjectsIntoOne(pss);

		// compose title lines
		String title = "#Temporarily created for real use.\n";
		title += "#This property set is from:\n";
		title += String.format("#      %s\n", CommonConfigFile);
//		title += String.format("#      %s\n", getMonitoredDBFileName());
		title += String.format("#      %s\n", CommonURLFile); 
		title += String.format("#      %s\n", "Dynamically set if any");
		DateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		title += "#Update:" + formater.format(new Date()) + "\n\n";

		boolean ok = savePropertiesObjectToFile(propAll, title, saveFile);
		if (!ok)
			System.out.println("Bootstrap configuration fatal error.");

	}
	
	/**
	 * Just assume this class exist in such a folder:<br>
	 * [ROOT PATH]/classes/[PACKAGE PART OF THE PATH]/, <br>
	 * here this function just return the [ROOT PATH] part of this class
	 * 
	 * <br>
	 * The project maybe run as stand-alone JAVA application or OSGi bundles
	 * started by command line or Eclipse IDE
	 * 
	 * 
	 * @return root path of this project, ended with / or \
	 */
	public static String locateRootPath() {
		if (prjRootPath != null)
			return prjRootPath;

		// If the environment variable PROJECT_HOME is set, just use it as root
		// path
		String projectHome = readProjectRootPath();
		if (projectHome != null) {
			// System.out.println("PROJECT_HOME:" + projectHome);
			prjRootPath = projectHome;
			return projectHome;
		}

		// else guess the root path by parsing the location of this class
		String path = locateRootPathInStandaloneEnv();
		prjRootPath = path;

		System.out.println("PROJECT_ROOT:" + prjRootPath);

		return prjRootPath;
	}
	
	/**
	 * If the environment variable PROJECT_HOME is set, just get it
	 * 
	 * @return
	 */
	private static String readProjectRootPath() {
		final String KEY_PROJECT_HOME = "PROJECT_HOME";
		String VALUE_PROJECT_HOME = System.getenv(KEY_PROJECT_HOME);
		if (VALUE_PROJECT_HOME == null)
			return null;

		if (!(VALUE_PROJECT_HOME.endsWith("/") || (VALUE_PROJECT_HOME
				.endsWith(File.separator))))
			VALUE_PROJECT_HOME += File.separator;

		return VALUE_PROJECT_HOME;
	}

	/**
	 * Please make sure the folder where these classes exist is named "classes"
	 * 
	 * @return this method will never return a null
	 */
	
	private static String locateRootPathInStandaloneEnv() {
		String rootPath = null;

		rootPath = Configuration.class.getResource("").getPath();
		
		String packagePart = Configuration.class.getPackage().getName();
		
		packagePart = packagePart.replace(".", "/");
		
		int intPacakgePathStart = rootPath.indexOf(packagePart);
		rootPath = rootPath.substring(0, intPacakgePathStart);
		
		//now rootPath is the classes folder that contains the packages

		//remove classes folder name part
		final String classesFolderName = classesPath;
		int intBinPathStart = rootPath.indexOf(classesFolderName);		
		rootPath = rootPath.substring(0, intBinPathStart);

		
		//now rootPath is the parent folder of folder classes
		
		return rootPath;
	}
	
	/**
	 * This method save Properties object ps into file file_path, and if
	 * comments lines in head_comments_lines is not null , write the comments
	 * line into the file before writing the Properties object
	 * 
	 * @param ps
	 * @param head_comments_lines
	 * @param file_path
	 * @return
	 */
	public static synchronized boolean savePropertiesObjectToFile(
			Properties ps, String head_comments_lines, String file_path) {
		try {
			// write title lines
			FileWriter fw = new FileWriter(file_path);
			if (head_comments_lines != null)
				fw.write(head_comments_lines);

			if (ps == null)
				return true;

			// write properties
			TreeSet<Object> keys = new TreeSet<Object>(ps.keySet());
			for (Object ko : keys) {
				String key = ko.toString();
				Object vo = ps.get(ko);
				String value = null;
				if (vo != null)
					value = String.valueOf(vo);
				value = value.trim();
				fw.write(key + "=" + value + "\n");

			}
			fw.close();
			return true;
		} catch (IOException e) {
			// in this class, please don't use class Logger class
			e.printStackTrace();
			return false;
		}
	}

	private static Properties mergePropertiesObjectsIntoOne(Properties[] pss) {
		Properties pAll = new Properties();

		if (pss == null)
			return pAll;

		for (int psi = 0; psi < pss.length; psi++) {
			Properties ps = pss[psi];
			if (ps == null)
				continue;

			TreeSet<Object> keys = new TreeSet<Object>(ps.keySet());
			for (Object ko : keys) {
				String key = ko.toString();
				Object vo = ps.get(ko);
				String value = null;
				if (vo != null)
					value = String.valueOf(vo);
				value = value.trim();
				pAll.setProperty(key, value);
			}
		}

		return pAll;
	}
	/**
	 * 
	 * @return the absolute path of config folder, ended with /
	 */
	public static String getConfigurationPropertyFilePath() {
		String path = locateRootPath() + ConfigPath;

		return path;
	}
	
	public static String getDatabaseDirPath() {
		String path = locateRootPath() + DBConfigPath ;

		return path;
	}
	
	public static String getCDSetupPath() {
		String path = locateRootPath() + CDSetupPath ;

		return path;
	}
	
	public static String getPostDataPath() {
		String path = locateRootPath() + PostDataPath ;

		return path;
	}
	

	public static String getResponseDataPath() {
		String path = locateRootPath() + ResponseDataPath ;
		return path;
	}
	
	public static String getuploadFilePath() {
		String path = locateRootPath() + uploadFilePath ;
		return path;
	}
	
	public static String getRepositoryDBFilePath() {
		String path = locateRootPath() + RepositoryDBFile;
		return path;
	}
	
	public static String getMigrationValidateSQLFilePath(String sqlFileName) {
		String path = locateRootPath() + MigrationFile + "/sql/" + sqlFileName;
		return path;
	}
	
	public static String getTestOutputFilePath(){
		String path = locateRootPath() + TESTOUTPUTFILE;
		return path;
	}
	
	public static String getTestBucketsFilePath(){
		String path = locateRootPath() + TestBuckets;
		return path;
	}
	
	public static String getSrcFilePath(){
		String path = locateRootPath() + scr;
		return path;
	}
	
/*	public static String getMonitoredDBFileName() {
		String path = getConfigurationPropertyFilePath()+CommonConfigFile ;
		Properties p =openPropertyFile(path);
		return p.getProperty("MonitoredDatabase");
		
	}
	
	public static String getOTSServerFileName() {
		String path = getConfigurationPropertyFilePath()+CommonConfigFile ;
		Properties p =openPropertyFile(path);
		return p.getProperty("OTSServer");
		
	}
*/	
	public static String getSystemHostname(){
		InetAddress ip ;
		String hostname = null;
		//Get the hostname
		try {
			ip = InetAddress.getLocalHost();
			hostname = ip.getHostName();
			System.out.println("Local IP address : " + ip);
            System.out.println("Local Hostname : " + hostname);
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hostname;
	}
	public static Connection getMonitoredDBConnection() throws SQLException{
		String dbname = Configuration.getProperty("databaseName");
		String server = Configuration.getProperty("host");
		String port = Configuration.getProperty("port");
		String dbuser = Configuration.getProperty("user");
		String dbpwd = Configuration.getProperty("password");
		
		System.out.println("Getting connection to :"+server+","+dbname);
		DB2SimpleDataSource sds = new DB2SimpleDataSource();
		sds.setDatabaseName(dbname);
		sds.setServerName(server);
		sds.setPortNumber(Integer.parseInt(port));
		sds.setUser(dbuser);
		sds.setPassword(dbpwd);
		sds.setAllowNextOnExhaustedResultSet(1); 
		sds.setDriverType(4);
		
		Connection con = sds.getConnection();
		
		return con;
	}
}
