package com.ibm.datatools.ots.tests.restapi.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import net.sf.json.JSONObject;

import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.test.utils.DBEntity;



public class DBUtils {
	
	private static final String DRIVER = "com.ibm.db2.jcc.DB2Driver";
	private String URLSTR = null; 
    private String HOST = null;
    private String USERNAME = null;    
    private String USERPASSWORD = null;    
    private Connection connnection = null;  
    private PreparedStatement preparedStatement = null;  
    private CallableStatement callableStatement = null;     
    private ResultSet resultSet = null;
	
    
    public static String getConfigValue(String key){	
    	Properties prop = new Properties();
    	try {
			prop.load(new FileInputStream(new File(Configuration.getRepositoryDBFilePath())));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop.getProperty(key);
	}
    
    public DBUtils(String hOST, int pORT, String dATABASEANME, String uSERNAME, String uSERPASSWORD) {
		super();
		HOST = hOST;
		USERNAME = uSERNAME;
		USERPASSWORD = uSERPASSWORD;
		URLSTR = "jdbc:db2://"+HOST+":"+String.valueOf(pORT)+"/"+dATABASEANME;
	}
    
    public DBUtils(){
    	
		this(getConfigValue("host"),Integer.parseInt(getConfigValue("port")),
				getConfigValue("dbProfile"),getConfigValue("user"),getConfigValue("password"));
	}
    
	private List<String> loadSQL(String sqlFile, String terminator) throws Exception {    
    	List<String> sqlList = new ArrayList<String>();
	    try {
	         @SuppressWarnings("resource")
	         InputStream sqlFileIn = new FileInputStream(sqlFile);
	 
	         StringBuffer sqlSb = new StringBuffer();
	         byte[] buff = new byte[1024];
	         int byteRead = 0;
	         
	         while ((byteRead = sqlFileIn.read(buff)) != -1) {
	             sqlSb.append(new String(buff, 0, byteRead));
	         }
	         sqlSb.append("\n");
	         
	    	 String[] sqlArr = sqlSb.toString().split("("+terminator+"\r\n)|("+terminator+"\n)");
	    	 
	    	 for (int i = 0; i < sqlArr.length; i++) {
		    	 String sql = sqlArr[i].replaceAll("--.*", "").trim();
		    	 if (!sql.equals("")) {
		    		 sqlList.add(sql);
		    	 }
	    	  }
	    	 return sqlList;
	    	} catch (Exception ex) {
	    	   throw new Exception(ex.getMessage());
	    	}
    }
	
	private void closeAll() throws SQLException {  
        if (resultSet != null) {  
            try {  
                resultSet.close();  
            } catch (SQLException e) {  
                //System.out.println(e.getMessage()); 
                throw new SQLException(e.getMessage());
            }  
        }   
       
        if (preparedStatement != null) {  
            try {  
                preparedStatement.close();  
            } catch (SQLException e) {  
                //System.out.println(e.getMessage()); 
                throw new SQLException(e.getMessage());
            }  
        }           
        
        if (callableStatement != null) {  
            try {  
                callableStatement.close();  
            } catch (SQLException e) {  
                //System.out.println(e.getMessage());  
                throw new SQLException(e.getMessage());
            }  
        }
       
        if (connnection != null) {  
            try {  
                connnection.close();  
            } catch (SQLException e) {  
                //System.out.println(e.getMessage()); 
                throw new SQLException(e.getMessage());
            }  
        }     
    }  
	
	public Connection getConnection() {  
        try { 
            connnection = DriverManager.getConnection(URLSTR, USERNAME,  
                    USERPASSWORD);  
        } catch (SQLException e) {  
            System.out.println(e.getMessage());  
        }  
        return connnection;  
    } 
	
	public void execute(String sqlFile, String terminator) throws Exception {    	 
    	
   	 Statement statement = null;
   	 List<String> sqlList = loadSQL(sqlFile, terminator);
   	 String sql = null;
   	 try {
   		   connnection = this.getConnection();
   		   if (statement == null) {
  				statement = connnection.createStatement();
  				}
   		   connnection.setAutoCommit(false);
   	       for (int i=0; i<sqlList.size();i++) {
   	    	   sql =  sqlList.get(i);
   	    	   statement.execute(sql);
   	       }               
   	       connnection.commit();    	            
   	      } catch (Exception ex) {
		    	     System.out.println("Failed to exectue the SQL script: \""+sql +"\"");
		    	  	 connnection.rollback();
		             throw ex;
	          } finally {
	              closeAll();
	          }
   }
	
	public void executeAlways(String sqlFile, String terminator) throws Exception {    	 

	   	 Statement statement = null;
	   	 List<String> sqlList = loadSQL(sqlFile, terminator);
	   	 String sql = null;
	   	 int count = 0;
	   	 boolean ex = false; 

	   	 connnection = this.getConnection();
	     if (statement == null) {
	    	statement = connnection.createStatement();
		 }
	     connnection.setAutoCommit(true);

	     for (int i=0; i<sqlList.size();i++) {
	    	 try {
	    	   sql =  sqlList.get(i);
	    	   statement.execute(sql);
	    	 } catch(Exception exception) {
	    		 System.out.println("Failed to exectue the SQL script: \""+sql +"\"");
	    		 count++;
	    		 ex = true;
	    		 //throw ex;
	    	 }
	     } 
	   closeAll();
      if(ex)
      {
   	   throw new SQLException("There are "+count+" SQL script failed to exectue...");
      }         	                    

	 }
	

	/*
	 * Execute sql statement for monitor db
	 */
	public static DBEntity getDBConfiguration(String dbProfile){
		
		DBEntity dbEntity = new DBEntity();
		TuningCommandService cs = new TuningCommandService();
		
		JSONObject result = cs.getMonitorDBDetails(dbProfile);
		String response = result.getString("response");
		JSONObject jso = JSONObject.fromObject(response);
		
		
		dbEntity.setHost(jso.getString("host"));
		dbEntity.setPort(jso.getString("port"));
		dbEntity.setUser(jso.getString("user"));
		dbEntity.setPassword(jso.getString("password"));
		dbEntity.setDatabaseName(jso.getString("databaseName"));
		
		return dbEntity;
	}
	
	public static Connection getConnection(String dbProfile, String password) throws Exception {

		DBEntity dbEntity = getDBConfiguration(dbProfile);

		String host = dbEntity.getHost();
		String port = dbEntity.getPort();
		String user = dbEntity.getUser();
		String databaseName = dbEntity.getDatabaseName();

		System.out.println(host + "," + port + "," + user + "," + password);

		try {
			Class.forName(DRIVER);
			Connection conn = DriverManager.getConnection("jdbc:db2://" + host + ":" + port + "/" + databaseName + "",
					"" + user + "", "" + password + "");
			return conn;
		} catch (Exception e) {
			throw e;
		}
	}

	public static void closeConnection(Connection conn) throws SQLException {
		try {
			conn.close();
		} catch (SQLException e) {
			throw e;
		}
	}
	
	public static void main(String args[]) throws Exception{
		/*Connection conn = DBUtils.getConnection("TPCDS","N1cetest");
		
		System.out.println(conn);*/
		
		DBUtils a = new DBUtils();
		try {
			//a.excuteQuery("SELECT 1 FROM IBMPDQ.MANAGED_DATABASE WHERE(1=-1)", null);
			a.executeAlways("C:\\validate.sql", ";");
			//a.executeBatch("C:\\validate.sql");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	public int[] executeBatch(String sql) throws Exception {
		int[] affectedLine = null;
		Statement statement = null;
		try {

			connnection = this.getConnection();

			if (statement == null) {
				statement = connnection.createStatement();
			}

			String[] batchSQLs = sql.trim().split(";");

			connnection.setAutoCommit(false);

			for (int i = 0; i < batchSQLs.length; i++) {
				statement.addBatch(batchSQLs[i]);
			}

			affectedLine = statement.executeBatch();

			connnection.setAutoCommit(true);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeAll();
		}
		return affectedLine;

	}
	
}















