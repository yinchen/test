package com.ibm.datatools.ots.tests.restapi.base;

public class DSMURLUtils {
	
	public static String URL_PREFIX = "" ;
	static{	
		String dsmEdition = Configuration.getProperty("dsm.edition");
		String dsmEnv = Configuration.getProperty("dsm.env");
		String EE_Prefix = Configuration.getProperty("enterprise.URL.PREFIX");
		String CS_Prefix = Configuration.getProperty("console.URL.PREFIX");

		if("build".equals(dsmEnv)){
			switch(dsmEdition) {
				case "bi":
					//In the RestAPIBaseTest, we will add base path "/gateway/default/dsm" for this case
					URL_PREFIX += "/" + EE_Prefix;  					
					break;
				case "enterprise":
					URL_PREFIX += "/" + EE_Prefix;  	
					break;
				case "console":
					URL_PREFIX += "/" + CS_Prefix;  	
					break;
				default:
					URL_PREFIX += "/" + EE_Prefix;  			
			}				
		}
	
	}
}

