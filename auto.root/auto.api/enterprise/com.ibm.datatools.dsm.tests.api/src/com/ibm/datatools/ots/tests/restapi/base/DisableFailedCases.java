package com.ibm.datatools.ots.tests.restapi.base;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ibm.datatools.test.utils.FileModify;


public class DisableFailedCases{
	
	
	public static final List<String> LIST = new ArrayList<String>(Arrays.asList("DSM Admin LUW Test","DSM MONITOR LUW Test","Tuning_TestSuit"));
	
	public static Map<String, List<String>> getFile(){
		
		Map<String, List<String>> map = new HashMap<>();
		
		for(int i=0; i<LIST.size(); i++){
			String path = Configuration.getTestOutputFilePath() + File.separator + LIST.get(i);
			System.out.println("Output file path is : "+path);
			
			File file = new File(path);
			File[] files = file.listFiles();
			for(File f:files){
				if(f.getPath().endsWith(".xml") && !f.getPath().contains("testng-failed.xml")){
					System.out.println("--------------"+f.getName()+"--------------");
					
					List<String> list = getFailedCasesAsList(path +File.separator + f.getName());
					map.put(f.getName(), list);
				}
				
			}
		}
		return map;
		
	}
	
	
	public static List<String> getFailedCasesAsList(String fileName){
		
		List<String> list = new ArrayList<>();
		
		try {
			File xmlFile = new File(fileName);
			   
			  DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			   
			  DocumentBuilder builder = builderFactory.newDocumentBuilder();
			   
			  Document doc = builder.parse(xmlFile);
			   
			  doc.getDocumentElement().normalize();
			   
			  //System.out.println("Root element: "+doc.getDocumentElement().getNodeName());
			   
			  NodeList nList = doc.getElementsByTagName("testcase");
			   
			  for(int i = 0 ; i<nList.getLength();i++){
			   
			  Node node = nList.item(i);
			   
			  //System.out.println("Node name: "+ node.getNodeName());
			  Element ele = (Element)node;
			   
			  if(node.getNodeType() == Element.ELEMENT_NODE){
			   
			  if(ele.getTextContent().contains("java")){
				  list.add(ele.getAttribute("classname") +"." +ele.getAttribute("name"));
			  }
			  }
			}
		} catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
		return list;
		
	}
	
	
	public static List<String> getComponent() throws SAXException, IOException{
		
		Map<String, List<String>> map = getFile();
		List<String> methodNameList = new ArrayList<>();
		for(String key : map.keySet()){
			
			for(int i=0;i<map.get(key).size(); i++){
				String s = map.get(key).get(i);
				System.out.println(s);
				String[] sa = s.split("\\.");
				
				String component = sa[6];   //com.ibm.datatools.ots.tests.restapi.admin.AdminPrivilegesTest_LUW.testRemoveRole
				System.out.println("Component : " + component);
				
				String className = s.substring(0, s.lastIndexOf("."));
				System.out.println("ClassName : " + className);
				
				String methodName = s.substring(s.lastIndexOf(".")+1, s.length());
				System.out.println("MethodName : " + methodName);
				
				String basedDB = key;
				System.out.println("BasedDB : " + basedDB);
				
				
				String scrFilePath = Configuration.getSrcFilePath() + className.replace(".", "/") + ".java";
				File file = new File(scrFilePath);
				InputStreamReader  reader = new InputStreamReader(new FileInputStream(file));
				BufferedReader bufferedReader = new BufferedReader(reader);
				String line = null;
				int lineNum = 0;
				
				while ((line = bufferedReader.readLine()) != null) {
					lineNum++;
					if(line.contains(methodName+"(")){
						System.out.println("Line num : "+lineNum);
						
						System.out.println(line);
						
						if(line.contains("{")){
							methodNameList.add(line);
						}
						System.out.println("----------------------------");
					}
				}
				bufferedReader.close();
			}
		}
		return methodNameList;
	}
	
	
	
	@Test
	public void testDisableFailedCases() throws SAXException, IOException {
		List<String> list = getComponent();
		System.out.println(list.toString());
		
		String codePath = Configuration.getSrcFilePath() + "com/ibm/datatools/ots/tests/restapi/";
		System.out.println(codePath);
		for(int i=0;i<list.size();i++){
			new FileModify(codePath, 
					list.get(i), list.get(i)+"if(true){return;}");
		}
		
	}
	
}
