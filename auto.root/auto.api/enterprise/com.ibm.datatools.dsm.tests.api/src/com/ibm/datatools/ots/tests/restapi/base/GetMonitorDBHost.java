package com.ibm.datatools.ots.tests.restapi.base;


import org.testng.Assert;

import net.sf.json.JSONObject;

import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;

public class GetMonitorDBHost {
	private TuningCommandService tunningSommandService = null;
	JSONObject objResponse = null;
	
	public GetMonitorDBHost(String dbProfile) {
		tunningSommandService = new TuningCommandService();
		JSONObject result = tunningSommandService.getMonitorDBDetails(dbProfile);
		String resultCode = result.getString("resultCode");
		if("failure".equals(resultCode)){
			String message = "No monitor db maned "+dbProfile+" found on db list";
			Assert.fail(message);
		}
		String response = result.getString("response");
		 objResponse = JSONObject.fromObject(response);
	}

	public String getMonitorDBHost(){
		String host = null;
		if(objResponse != null && objResponse.has("host")){
			host = objResponse.getString("host");
		}
		return host;
	}
	public String getMonitorDBUser(){
		String user = null;
		if(objResponse != null && objResponse.has("user")){
			user = objResponse.getString("user");
		}
		return user;
	}
	
}
