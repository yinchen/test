package com.ibm.datatools.ots.tests.restapi.base;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class RepoDBUtils {
	
public static Connection getConnection() throws Exception{
	
	
	String host = null;
	String port = null;
	String user = null;
	String password = null;
	String dbProfile = null;
	
	Properties prop = new Properties();
	
	prop.load(new FileInputStream(new File(Configuration.getRepositoryDBFilePath())));
	
    host = prop.getProperty("host");
    port = prop.getProperty("port");
    user = prop.getProperty("user");
    password = prop.getProperty("password");
    dbProfile = prop.getProperty("dbProfile");
    
		try {
			Class.forName("com.ibm.db2.jcc.DB2Driver");
			Connection conn = DriverManager.getConnection("jdbc:db2://"+host+":"+port+"/"+dbProfile+"", ""+user+"", ""+password+"");
			return conn;
		} catch (Exception e) {
			throw e;
		}
	}

	public static void main(String args[]) throws Exception{
		
		Connection conn = RepoDBUtils.getConnection();
		System.out.println(conn);
	}
	
	

}
