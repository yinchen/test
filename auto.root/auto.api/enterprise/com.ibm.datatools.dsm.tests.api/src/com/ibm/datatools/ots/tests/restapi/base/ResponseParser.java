package com.ibm.datatools.ots.tests.restapi.base;

import org.testng.Assert;

import com.jayway.jsonpath.JsonPath;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class ResponseParser {

	public static JSONObject getTableDetails(JSONObject responseData,String tableName){
		JSONArray array = JSONArray.fromObject(JsonPath.read(responseData, "$data[0].[1].metric.odTableStorage.data[?(@.TABNAME=='"+tableName+"')]"));
		return JsonPath.read(array, "$[0]");
	}
	
	public static JSONArray getLogFileNames(JSONObject responseData){
		JSONArray array = JSONArray.fromObject(JsonPath.read(responseData, "$items[*].name"));
		return array;
	}
	
	public static String getProfileID(JSONObject responseData, String profileName){
		JSONArray array = JSONArray.fromObject(JsonPath.read(responseData, "$result.profileheaders[?(@.name=='"+profileName+"')]"));
		String profileID= JsonPath.read(array, "$[0].id");
		return profileID.trim();
	}
	
	public static String getDBConResultCode(JSONObject responseData){
		
		String resultCode = JsonPath.read(responseData, "$.resultCode");
		return resultCode.trim();
	}
	
	
	 public static JSONArray  getMemoryPoolTypeList(JSONObject responseData){
			
	     	JSONArray array = JSONArray.fromObject(JsonPath.read(responseData, "$.data[0].[1].metric.monGetMemoryPool.data[*].MEMORY_POOL_TYPE"));
	    // 	String   MemoryPoolType = JsonPath.read(array, "$[0]").toString();
	     	System.out.println("List of Memory Pool Type Names"+array);
	     	return array;
	 	 //   System.out.println("List of Memory Pool Type Names"+array);
	    	/*(String str = array.toString();
	    	String delims = "[[,]]";
	    	
	    	String[] tokens = str.split(delims);
	    	String str1 =tokens.toString();
	    	String delims1 ="[[]";
	    	String[] tokens1 = str.split(delims1);
	    	System.out.println("tokens:"+tokens1[0]);*/
	    //	return MemoryPoolType;
	    	
		}
	
	 public static JSONArray getExceptionCaughtVal (JSONObject responseData){
		  
		 
		    String ExpectedVal = "[false]";
		  	JSONArray array = JSONArray.fromObject(JsonPath.read(responseData, "$.data[0].[1].exceptionCaught"));
	     	String ExceptionCaughtVal = JsonPath.read(array, "$[0]").toString();
	     	System.out.println("Exception Caught=="+ExceptionCaughtVal);
	     	String ActualVal = array.toString();
	    	Assert.assertEquals(ActualVal, ExpectedVal);
	     	return array;
	 
	 }
	 
	
	 
	 public static Double getMemoryPoolPercentageMetric(JSONObject responseData){
			
		    String inputString = "0";
	    	JSONArray array = JSONArray.fromObject(JsonPath.read(responseData, "$.data[0].[1].metric.monGetMemoryPool.data[0].USED_PERCENT_TOTAL"));
	        String value =  JsonPath.read(array, "$[0]").toString();
	        double val = Double.parseDouble(value);   
	        int val1 = (int)val;
	    	System.out.println("value:"+val1);
	    	if(val1>=0)
	    	{
	        		System.out.println("Memory Pool Percentage is in valid range");
	    	}
	    	else
	    	{
	    		System.out.println("Memory Pool Percentage is not in valid range");
	    	}
	    	 		return JsonPath.read(array, "$[0]");
		}
	
}

