package com.ibm.datatools.ots.tests.restapi.base;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.testng.Assert;

import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import net.sf.json.JSONObject;

public class SSHCmd {
	
	
	//Default port of db2 is 22
	public static String sshCMD(String ip,String user,String password,String CMD){
		String result="";
		Session session =null;
		ChannelExec openChannel =null;
		
		try {
			JSch jsch = new JSch();
			session = jsch.getSession(user, ip, 22);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.setPassword(password);
			session.connect();
			openChannel = (ChannelExec) session.openChannel("exec");
			openChannel.setCommand(CMD);
			openChannel.connect();  
            InputStream in = openChannel.getInputStream();  
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));  
            String buf = null;
            while ((buf = reader.readLine()) != null) {
            	result += new String(buf.getBytes("gbk"),"UTF-8")+"   \r\n";  
            }  
		} catch (JSchException | IOException e) {
			result +=e.getMessage();
		}finally{
			if(openChannel != null && !openChannel.isClosed()){
				openChannel.disconnect();
			}
			if(session != null&&session.isConnected()){
				session.disconnect();
			}
		}
		return result;
	}
	
	public static String sshCMD(String ip, String user, String password, int port, String CMD) {

		String result = "";
		Session session = null;
		ChannelExec openChannel = null;

		try {
			JSch jsch = new JSch();
			session = jsch.getSession(user, ip, port);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.setPassword(password);
			session.connect();

			openChannel = (ChannelExec) session.openChannel("exec");
			openChannel.setCommand(CMD);
			openChannel.setErrStream(System.err);

			openChannel.connect();

			InputStream in = openChannel.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			String buf = null;
			while ((buf = reader.readLine()) != null) {
				result += new String(buf.getBytes("gbk"), "UTF-8") + "   \r\n";
			}
		} catch (JSchException | IOException e) {
			result += e.getMessage();
		} finally {
			if (openChannel != null && !openChannel.isClosed()) {
				openChannel.disconnect();
			}
			if (session != null && session.isConnected()) {
				session.disconnect();
			}
		}
		return result;
	}

	public static String sshCMD(String dbProfile,String CMD) throws FileNotFoundException, IOException{
		
		String filePath = Configuration.getPostDataPath() + "ConnectionsInfo/" + "connections.properties";
		Properties p = new Properties();
		p.load(new FileInputStream(filePath));
		
		String databaseName = "";
		
		TuningCommandService cs = new TuningCommandService();
		JSONObject monitorDBDetails = cs.getMonitorDBDetails(dbProfile);
		String resultCode = monitorDBDetails.getString("resultCode");
		
		if(monitorDBDetails == null || "failure".equals(resultCode)){
			String errorMessage = "No monitor database connection named ["+dbProfile+"] found in monitor db list";
			System.out.println("ERROR MESSAGE : " + errorMessage);
			Assert.fail(errorMessage);
		}
		
		System.out.println(monitorDBDetails);
		String response = monitorDBDetails.getString("response");
		JSONObject resObj = JSONObject.fromObject(response);
		
		String dbType = resObj.getString("dataServerType");
		if("DB2Z".equals(dbType)){
			databaseName = resObj.getString("location");
		}else if("DB2LUW".equals(dbType)){
			databaseName = resObj.getString("databaseName");
		}
		
		if(!p.containsKey(dbProfile)){
			Assert.fail("No information of db ["+dbProfile+"] found in connections properties file!");
		}
		String password = p.getProperty(dbProfile);
		
		String host = resObj.getString("host");
		String user = resObj.getString("user");

		System.out.println("The host of db2 you want to login is : " + host);
		System.out.println("The username of db2 used : " + user);
		System.out.println("The password of db2 used : " + password);
		
		return sshCMD(host, user, password, CMD);
		
	}
	
	
	
	
	
	
	public static void main(String args[]) throws FileNotFoundException, IOException{
		//String exec = sshCMD("9.111.213.169", "db2inst1", "N1cetest", 22, ". `pwd`/sqllib/db2profile ; db2 list db directory");
		//String exec = sshCMD("9.111.213.169", "db2inst1", "N1cetest",". `pwd`/sqllib/db2profile;db2 list db directory");
		String exec = sshCMD("TPCDS", ". `pwd`/sqllib/db2profile;db2 list db directory;db2 connect to TPCDS;");
		System.out.println(exec);
	}
	
}











