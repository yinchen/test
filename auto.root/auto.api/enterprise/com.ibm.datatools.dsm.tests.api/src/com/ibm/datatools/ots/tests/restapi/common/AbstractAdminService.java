package com.ibm.datatools.ots.tests.restapi.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.sf.json.JSONObject;

import com.ibm.datatools.ots.tests.restapi.admin.AdminNavigationTest_LUW;
import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.test.utils.FileTools;
import com.jayway.restassured.response.Response;


/**
 * @author litaocdl
 * 
 * This class is abstract service class admin server need to extends
 * 
 * @see package com.ibm.datatools.ots.tests.restapi.admin
 * 
 * */
public abstract class AbstractAdminService extends RestAPIBaseTest {
		
	Properties p;
	private static final Logger logger = LogManager.getLogger(AbstractAdminService.class);
	
	public AbstractAdminService(){
		Vector<String> postdata = new Vector<String>();
		/**
		 * Add the Alter properties here, each properties has more than one entry, each entry contains the params which need post to DSM
		 * */
		logger.info("Running AbstractAdminService - before LoadProperties");
		LoadProperties(postdata) ;
		logger.info("Running AbstractAdminService - after LoadProperties");
		p=new Properties();
		
		for(int i=0;i<postdata.size();i++){
			p.putAll(loadJSONPostData(postdata.get(i)));
		}
		logger.info("Running AbstractAdminService - end");
	}
	/**
	 * Load the properties defined in each services into postData Vector.
	 * */
	abstract protected void LoadProperties(Vector<String> postdata) ;
		
	
	public Properties loadJSONPostData(String filename){
		Properties p = new Properties();;
		String filePath = Configuration.getPostDataPath()+"connectionsData/"+filename;
		try{
			p.load(new FileInputStream(filePath));
		}catch(FileNotFoundException e){
			e.printStackTrace();
			throw new IllegalArgumentException("Error when loading file in method loadJSONPostData(), file name is: "+filePath);
		}catch(IOException e){
			e.printStackTrace();
			throw new IllegalArgumentException("Error when loading file in method loadJSONPostData(), file name is: "+filePath);
		}

		return p;
	}
	
	public String getJSONData(String key){
		String value = p.getProperty(key);
		if(value.endsWith(".json")){
			String filePath = Configuration.getPostDataPath()+"jsonData/"+value.trim();
			value = FileTools.readFileAsString(filePath) ;
		}
		return value ;
	}
	
	public Properties getJSONProperties(){
		return this.p;
	}

	protected JSONObject newResult( Response response, String path, Map<String, String> postData )
	{
		JSONObject result = new JSONObject();
		result.accumulate( "URL", path );
		result.accumulate( "PostData", postData );
		result.accumulate( "ResponseCode", response.getStatusCode() );
		result.accumulate( "ResponseData", response.body().asString() );
		return result;
	}
		
}
