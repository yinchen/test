package com.ibm.datatools.ots.tests.restapi.common;

import java.util.Map;
import java.util.Vector;

import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.test.utils.JSONUtils;
import com.jayway.restassured.response.Response;


/**
 * @author litaocdl
 * 
 * This class is the services class for testing the admin Alter function. 
 * the real test class reference this class and do the real verification.
 * 
 * @see package com.ibm.datatools.ots.tests.restapi.admin
 * 
 * */
public class AdminAlterObjectService extends AbstractAdminService {
	
	private static final Logger logger = LogManager.getLogger(AdminAlterObjectService.class);
	/**
	 * URL to handle the Alter Object request
	 * */
	public static final String ALTER_OBJECT = DSMURLUtils.URL_PREFIX + "/adm/core/common/alter/object" ;
	public static final String CREATE_DEFINITION = DSMURLUtils.URL_PREFIX + "/adm/core/common/alter/createDefinition" ;
	public static final String ALTER_DEFINITION = DSMURLUtils.URL_PREFIX + "/adm/core/common/alter/definition" ;
	public static final String PRIVILEGE_MODEL_DEFINITION = DSMURLUtils.URL_PREFIX + "/amd/core/privilege/LUWObjectModel" ;
	public static final String PRIVILEGE_MODEL_COMMAND = DSMURLUtils.URL_PREFIX + "/amd/core/privilege/LUWPrivilegeCommand" ;
	public static final String STATICS_COMMAND = DSMURLUtils.URL_PREFIX + "/adm/core/ta/gen/TARunstatsCommand";
	public static final String COMPRESS_INDEX_COMMAND = DSMURLUtils.URL_PREFIX +"/adm/core/ta/gen/TACompressIndexCommand";
	public static final String GENERATE_DDL = DSMURLUtils.URL_PREFIX +"/adm/core/common/Ddl";
	public static final String REORGNIZE_MQT_COMMAND =DSMURLUtils.URL_PREFIX + "/adm/core/ta/gen/TAReorgTableCommand";
	public static final String BROWSEDATA_COMMAND = DSMURLUtils.URL_PREFIX + "/browsedata/BrowseTableData?cmd=getAlldata";
	public static final String TYPE_NAVIGATION_URL = DSMURLUtils.URL_PREFIX + "/amd/core/explorer/TypesNavigation";
	public static final String COMPRESS_TABLE_COMMAND = DSMURLUtils.URL_PREFIX + "/adm/core/ta/gen/TACompressTableCommand";
	public static final String EXECUTION_SCRIPT = DSMURLUtils.URL_PREFIX + "/adm/core/common/Execution";
	
	/**
	 * Property contained the post form data for Alter function. this properties contains following entry:
	 * 1. getAlterTableName: data area of form data for altering a table name, not include the dbProfileName.
	 * 2. getAlterTable:  data area of form data for altering a table , not include the dbProfileName.
	 * 3. getCreateTableSpace:  data area of form data for creating a table-space, not include the dbProfileName.
	 * 4. <new entry added here>
	 * 
	 * */
	public static final String PROP_GET_ALTER = "getAdminAlter.properties" ;
	
	/**
	 * Load the properties
	 * */	
	protected void LoadProperties(Vector<String> postdata){
		postdata.add(PROP_GET_ALTER) ;
	}
	
	public JSONObject callAlterObjectService(String path,String jsonObj,String dbProfile){

		Map<String,String> postData = JSONUtils.parseJSON2MapString(jsonObj);
		
		Assert.assertNotNull(dbProfile);		
		postData.put("dbProfileName",dbProfile);
		
		logger.info("Calling AlterObjectService with :"+postData);
		
		Response response = post(path,postData,200);
		
		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		
		return result;
	}
	public JSONObject callCreateDefinitionService(String jsonObj,String dbProfile){

		Map<String,String> postData = JSONUtils.parseJSON2MapString(jsonObj);
		
		Assert.assertNotNull(dbProfile);		
		postData.put("dbProfileName",dbProfile);
		
		logger.info("Calling CreateDefinitionService with :"+postData);
		
		Response response = post(CREATE_DEFINITION,postData,200);
		
		JSONObject result = new JSONObject();
		result.accumulate("URL", AdminAlterObjectService.CREATE_DEFINITION);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		
		return result;
	}
	public JSONObject callAlterDefinitionService(String jsonObj,String dbProfile){

		Map<String,String> postData = JSONUtils.parseJSON2MapString(jsonObj);
		
		Assert.assertNotNull(dbProfile);		
		postData.put("dbProfileName",dbProfile);
		
		logger.info("Calling AlterDefinitionService with :"+postData);
		
		Response response = post(ALTER_DEFINITION,postData,200);
		
		JSONObject result = new JSONObject();
		result.accumulate("URL", AdminAlterObjectService.ALTER_DEFINITION);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		
		return result;
	}
	
	/*
	 * Added by ybjwang@cn.ibm.com
	 * Method to generate response data for browse Object data
	 */
	public JSONObject callBrowseDataCommandService(String jsonObj,String dbProfile){

		Map<String,String> postData = JSONUtils.parseJSON2MapString(jsonObj);
		
		Assert.assertNotNull(dbProfile);		
		postData.put("dbProfileName",dbProfile);
				
		logger.info("Calling StaticsCommandService with :"+postData);
		
		Response response = post(GENERATE_DDL,postData,200);
		
		JSONObject result = new JSONObject();
		result.accumulate("URL", AdminAlterObjectService.BROWSEDATA_COMMAND);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		
		return result;
	}
	@Override
	public String getJSONData(String key){
		String value = p.getProperty(key);
		if(value.endsWith(".json")){
			String filePath = Configuration.getPostDataPath()+"jsonData/"+"alter/"+value.trim();
			value = FileTools.readFileAsString(filePath) ;
		}
		return value ;
	}
}
