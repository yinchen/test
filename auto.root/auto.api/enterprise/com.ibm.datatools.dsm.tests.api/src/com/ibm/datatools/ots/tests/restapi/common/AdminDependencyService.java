package com.ibm.datatools.ots.tests.restapi.common;

import java.util.Map;
import java.util.Vector;
import net.sf.json.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.JSONUtils;
import com.jayway.restassured.response.Response;

public class AdminDependencyService extends AbstractAdminService
{

	private static final Logger logger = LogManager.getLogger( AdminDependencyService.class );
	
	/**
	 * URL to handle the dependency request
	 * */
	public static final String GET_DEPENDENCY = DSMURLUtils.URL_PREFIX + "/adm/core/dependency";

	/**
	 * 
	 * 
	 * */
	public static final String PROP_GET_DEPENDENCY = "getAdminDependency.properties";

	/**
	 * Load the properties
	 * */
	protected void LoadProperties( Vector<String> postdata )
	{
		postdata.add( PROP_GET_DEPENDENCY );
	}

	public JSONObject callDependencyService( String path, String jsonObj, String dbProfile )
	{

		Map<String, String> postData = JSONUtils.parseJSON2MapString( jsonObj );

		Assert.assertNotNull( dbProfile );
		postData.put( "dbProfileName", dbProfile );

		logger.info( "Calling DependencyService with :" + postData );
		Response response = post( path, postData, 200 );
		return newResult( response, path, postData );
	}
	/**
	 * add by yangliu,give one more parameter to dependency's postdata of package
	 * 
	 * */
	public JSONObject callDependencyServiceForPackage( String path, String jsonObj, String dbProfile, String pkgname )
	{

		Map<String, String> postData = JSONUtils.parseJSON2MapString( jsonObj );

		Assert.assertNotNull( dbProfile );
		postData.put( "dbProfileName", dbProfile );
		postData.put( "name", pkgname );
		logger.info( "Calling DependencyService with :" + postData );
		Response response = post( path, postData, 200 );
		return newResult( response, path, postData );
	}

}
