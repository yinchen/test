package com.ibm.datatools.ots.tests.restapi.common;

import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.JSONUtils;
import com.jayway.restassured.response.Response;


/**
 * @author litaocdl
 * 
 * This class is the services class for testing the Admin Navigating functions
 * the real test class reference this class and do the real verification.
 * 
 * @see package com.ibm.datatools.ots.tests.restapi.admin
 * 
 * */
public class AdminNavigationService extends AbstractAdminService {
	
	private static final Logger logger = LogManager.getLogger(AdminNavigationService.class);
	/**
	 * URL of type navigation
	 * */
	public static final String TYPE_NAVIGATION_URL = DSMURLUtils.URL_PREFIX + "/amd/core/explorer/TypesNavigation";
	

	/**
	 * Property contained the post form data in TA function. this properties contains following entry:
	 * 1.getNavigateTable: query data to retrieve the result of navigate table.
	 * 2.<for new item, update it here>
	 * 
	 * */
	public static final String PROP_NAVIGATION = "getAdminNavigation.properties" ;
	
	Properties p;
	
	public AdminNavigationService(){}
	/**
	 * Load the properties
	 * */	
	protected void LoadProperties(Vector<String> postdata){
		postdata.add(PROP_NAVIGATION) ;
	}	
	
	public JSONObject callNavigationService(String path,String jsonObj,String dbProfile){

	
		Map<String,String> postData =	JSONUtils.parseJSON2MapString(jsonObj);
		Assert.assertNotNull(dbProfile);		
		postData.put("dbProfileName",dbProfile);
	
		logger.info("Calling NavigationService with :"+postData);
		
		Response response = post(path,postData, 200);
		
		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		
		return result;
	}
	




	
}
