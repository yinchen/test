package com.ibm.datatools.ots.tests.restapi.common;

import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.JSONUtils;
import com.jayway.restassured.response.Response;


/**
 * @author hongcbj@cn.ibm.com
 * 
 * This class is the services class for testing the Admin Privileges functions
 * the real test class reference this class and do the real verification.
 * 
 * */
public class AdminPrivilegesService extends AbstractAdminService {
	
	private static final Logger logger = LogManager.getLogger(AdminPrivilegesService.class);
	/**
	 * URL of type privileges
	 * */
	public static final String PRIVILEGE_MODEL_DEFINITION_LUW = DSMURLUtils.URL_PREFIX + "/amd/core/privilege/LUWPrivilegeModel" ;
	public static final String PRIVILEGE_MODEL_COMMAND_LUW = DSMURLUtils.URL_PREFIX + "/amd/core/privilege/LUWPrivilegeCommand" ;
	public static final String PRIVILEGE_GET_OBJECT_LIST_LUW = DSMURLUtils.URL_PREFIX + "/amd/core/privilege/LUWGetObjectPrivileges" ;
	public static final String PRIVILEGE_REMOVE_COMMAND_LUW = DSMURLUtils.URL_PREFIX + "/amd/core/privilege/LUWGenPrivilegeCommand" ;
	

	/**
	 * Property contained the post form data in AdminPrivileges function 
	 * */
	public static final String PROP_PRIVILEGES = "getAdminPrivileges.properties" ;
	
	Properties p;
	
	public AdminPrivilegesService(){}
	/**
	 * Load the properties
	 * */	
	protected void LoadProperties(Vector<String> postdata){
		postdata.add(PROP_PRIVILEGES) ;
	}	
	
	public JSONObject callPrivilegesService(String path,String jsonObj,String dbProfile){

	
		Map<String,String> postData =	JSONUtils.parseJSON2MapString(jsonObj);
		Assert.assertNotNull(dbProfile);		
		postData.put("dbProfileName",dbProfile);
	
		logger.info("Calling PrivilegesService with :"+postData);
		
		Response response = post(path,postData,200);
		
		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		
		return result;
	}
}
