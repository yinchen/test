package com.ibm.datatools.ots.tests.restapi.common;

import static org.testng.AssertJUnit.assertTrue;

import java.util.List;
import java.util.Map;
import java.util.Vector;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.JSONUtils;
import com.jayway.jsonpath.JsonPath;
import com.jayway.restassured.response.Response;

public class AdminSQLEditorService extends AbstractAdminService
{

	private static final Logger logger = LogManager.getLogger( AdminAlterObjectService.class );
	/**
	 * URL to handle the Alter Object request
	 * */
	public static final String RUN_SQL = DSMURLUtils.URL_PREFIX + "/adm/core/common/Execution";
	
	/**
	 * URL to handle the script request
	 * */
	public static final String SCRIPTS = DSMURLUtils.URL_PREFIX + "/adm/core/editor/Scripts";
	
	public static final String FORMAT_SQL = DSMURLUtils.URL_PREFIX + "/adm/core/editor/Formatter";

	/**
	 * 
	 * 
	 * */
	public static final String PROP_GET_SQLEDITOR = "getAdminSQLEditor.properties";

	/**
	 * Load the properties
	 * */
	protected void LoadProperties( Vector<String> postdata )
	{
		postdata.add( PROP_GET_SQLEDITOR );
	}

	public JSONObject callSQLEditorService( String path, String jsonObj, String dbProfile )
	{

		Map<String, String> postData = JSONUtils.parseJSON2MapString( jsonObj );

		Assert.assertNotNull( dbProfile );
		postData.put( "dbProfileName", dbProfile );

		logger.info( "Calling SQLEditorService with :" + postData );
		Response response = post( path, postData, 200 );
		return newResult( response, path, postData );
	}

	/**
	 * Run the given SQL in sqleditor with new connection
	 * @type JDBC, CLPSSH 
	 * @return the array of response data
	 * */
	public JSONObject[] runSQLinJDBC(String sqlStatement,String terminator,String dbProfileName){
		return runSQLinJDBC(false, sqlStatement, terminator, dbProfileName) ;
	}
	
	/**
	 * Run the given SQL in sqleditor
	 * @type JDBC, CLPSSH 
	 * @return the array of response data
	 * */
	public JSONObject[] runSQLinJDBC(boolean reuseConnection, String sqlStatement,String terminator,String dbProfileName){
		JSONObject jObj;
		if(reuseConnection){
			jObj = JSONUtils.getJSONTemplate4ExecuteInReusedJDBC(sqlStatement, terminator) ;
		}
		else{
			jObj = JSONUtils.getJSONTemplate4ExecuteInJDBC(sqlStatement, terminator) ;
		}
		JSONObject responseData = prepareQuery(jObj.toString(),dbProfileName) ;
		String queryID = JsonPath.read(responseData, "$.queryId") ;
		Assert.assertNotNull(queryID);
		JSONArray position = JsonPath.read(responseData, "$.positions") ;
		int size = position.size() ;
		//Use the queryID to execute the sql in the second request
		JSONObject[] result = new  JSONObject[size] ;
		JSONObject[] runObj = JSONUtils.getJSONTemplate4RunQuery(queryID,size) ;
		for(int i=0;i<size;i++){
			responseData = runQuery(runObj[i].toString(),dbProfileName);
			result[i] = responseData ;	
		}
		return result ;
	}

	/**
	 * run a list of sql
	 */
	public JSONObject[] runSQLinJDBC( List<String> sqlStatements, String terminator, String dbProfileName )
	{
		String combinedSql = "";
		for ( String sql : sqlStatements )
		{
			combinedSql += sql;
			if ( !sql.endsWith( terminator ) )
			{
				combinedSql += ";";
			}
		}
		return runSQLinJDBC( combinedSql, terminator, dbProfileName );
	}

	/**
	 * run a list of sql and validate result
	 */
	public void runSQLinJDBC( List<String> sqlStatements, String terminator, String dbProfileName,
			boolean validateResult )
	{

		JSONObject[] result = runSQLinJDBC( sqlStatements, terminator, dbProfileName );
		if ( validateResult )
		{
			validateRunSQLSucceed( result );
		}
	}

	/**
	 * run sql and validate result
	 */
	public void runSQLinJDBC( String sqlStatement, String terminator, String dbProfileName, boolean validateResult )
	{
		JSONObject[] result = runSQLinJDBC( sqlStatement, terminator, dbProfileName );
		if ( validateResult )
		{
		 validateRunSQLSucceed( result ) ;
		}
	}

	/**
	 * validate whether run sql return error
	 */
	public void validateRunSQLSucceed( JSONObject[] result )
	{
		for ( JSONObject obj : result )
		{
			if ( true == obj.has( "error" ) )
			{
				assertTrue( obj.getString( "exception" ), false );
			}
		}
		
	}

	/**
	 * Run the given SQL in sqleditor
	 * 
	 * @type JDBC, CLPSSH
	 * 
	 *       * @return the array of response data
	 * */
	public JSONObject[] runSQLinCLP(String sqlStatement,String terminator,boolean addConnectTo,String dbProfileName){
		JSONObject jObj = JSONUtils.getJSONTemplate4ExecuteInCLP(sqlStatement, terminator,addConnectTo) ;
		
		JSONObject responseData = prepareQuery(jObj.toString(),dbProfileName) ;
		String queryID = JsonPath.read(responseData, "$.queryId") ;
		Assert.assertNotNull(queryID);
		JSONArray position = JsonPath.read(responseData, "$.positions") ;
		Assert.assertNotNull(position);
		int size = position.size() ;
		//Use the queryID to execute the sql in the second request
		JSONObject[] result = new  JSONObject[size] ;
		JSONObject[] runObj = JSONUtils.getJSONTemplate4RunQuery(queryID,size) ;
		for(int i=0;i<size;i++){
			responseData = runQuery(runObj[i].toString(),dbProfileName);
			result[i] = responseData ;	
		}
		return result ;
	}
	/**
	 * This method provide the function to run a sql statement agaqinst SQL Editor, the sql statement is wrapped in 
	 * JSONObject and stored in getAdminSQLEditor.properties use the key in param as reference key
	 * 
	 * */
	public JSONObject[] runSQLinSQLEditor(String key,String dbProfileName){
		// The key defined in the properties
		String query = this.getJSONData( key );

		JSONObject responseData = prepareQuery(query,dbProfileName) ;
		String queryID = JsonPath.read(responseData, "$.queryId") ;
		Assert.assertNotNull(queryID);
		JSONArray position = JsonPath.read(responseData, "$.positions") ;
		Assert.assertNotNull(position);
		int size = position.size() ;
		//Use the queryID to execute the sql in the second request
		JSONObject[] result = new  JSONObject[size] ;
		JSONObject[] runObj = JSONUtils.getJSONTemplate4RunQuery(queryID,size) ;
		for(int i=0;i<size;i++){
			responseData = runQuery(runObj[i].toString(),dbProfileName);
			result[i] = responseData ;	
		}
		return result ;
	}
	
	public void assertResponseCode(JSONObject responseObj){
		int responseCode = JsonPath.read(responseObj, "$.ResponseCode") ;
		Assert.assertEquals(responseCode, 200);
	}
	
	protected JSONObject prepareQuery(String query,String dbProfile){
		//Get the queryID from first http request
		JSONObject resObj =	this.callSQLEditorService( AdminSQLEditorService.RUN_SQL, query, dbProfile );
		JSONObject responseData =(JSONObject) resObj.get("ResponseData");
		if(responseData.has("error")){
			String error = responseData.get("error").toString();
			assertTrue( responseData.getString( "exception" ), false );
			if("true".equalsIgnoreCase(error)){
				if(responseData.has("exception") && responseData.getJSONObject("exception").has("message")){
					logger.error(responseData.getJSONObject("exception").get("message"));
				}
			}
		}
		Assert.assertNotNull(responseData);	
		assertResponseCode(resObj) ;
		return responseData ;
	}
	protected JSONObject runQuery(String obj,String dbProfile){
		JSONObject resObj =	this.callSQLEditorService( AdminSQLEditorService.RUN_SQL, obj, dbProfile );
		assertResponseCode(resObj) ;
		JSONObject responseData =(JSONObject) resObj.get("ResponseData");
		return responseData ;
	}
}
