package com.ibm.datatools.ots.tests.restapi.common;

import java.util.Map;
import java.util.Vector;

import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.JSONUtils;
import com.jayway.restassured.response.Response;


/**
 * 
 * This class is the services class for testing the SQLObject function. 
 * the real test class reference this class and do the real verification.
 * 
 * @see package com.ibm.datatools.ots.tests.restapi.admin
 * 
 * */
public class AdminSQLObjectService extends AbstractAdminService {

	private static final Logger logger = LogManager.getLogger(AdminSQLObjectService.class);
	public static final String SQLOBJECT_PRIVILEGE_MODEL =DSMURLUtils.URL_PREFIX +  "/amd/core/privilege/LUWPrivilegeModel";
	public static final String SQLOBJECT_PRIVILEGE_SQLOBJECT =DSMURLUtils.URL_PREFIX + "/amd/core/privilege/LUWPrivilegeModel";
	public static final String SQLOBJECT_DEFINITION_ALTER=DSMURLUtils.URL_PREFIX + "/adm/core/common/alter/definition" ;
	public static final String SQLOBJECT_DEFINITION_CREATE=DSMURLUtils.URL_PREFIX + "/adm/core/common/alter/createDefinition";
	
	
	public static final String PROP_GET_SQLOBJECT = "getAdminSQLObject.properties" ;
	
	
	
	public AdminSQLObjectService(){}
	/**
	 * Load the properties
	 * */	
	protected void LoadProperties(Vector<String> postdata){
		postdata.add(PROP_GET_SQLOBJECT) ;
	}
	
	
	public JSONObject callSQLObjectService(String path,String jsonObj,String dbProfile,String version){

	
		Map<String,String> postData =	JSONUtils.parseJSON2MapString(jsonObj);
		
		Assert.assertNotNull(dbProfile);		
		postData.put("dbProfileName",dbProfile);
		postData.put("version",version);
	
		logger.info("Calling SQLObjectService with :"+postData);
		
		Response response = post(path,postData,200);
		
		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		
		return result;
	}
	public JSONObject callSQLObjectErrorLoardingService(String path,String jsonObj,String dbProfile, String version){

		
		Map<String,String> postData =	JSONUtils.parseJSON2MapString(jsonObj);
		
		Assert.assertNotNull(dbProfile);		
		postData.put("dbProfileName","NOTEXISTS");
		postData.put("version",version);
		
		logger.info("Calling SQLObjectService with :"+postData);
		
		Response response = post(path,postData,200);
		
		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		
		return result;
	}
@Override
public String getJSONData(String key){
	String value = p.getProperty(key);
	
	return value ;
}



	
}
