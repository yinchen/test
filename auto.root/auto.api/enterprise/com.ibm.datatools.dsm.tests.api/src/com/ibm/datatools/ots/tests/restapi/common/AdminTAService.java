package com.ibm.datatools.ots.tests.restapi.common;

import java.util.Map;
import java.util.Vector;

import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.test.utils.JSONUtils;
import com.jayway.restassured.response.Response;


/**
 * @author litaocdl
 * 
 * This class is the services class for testing the TA function. 
 * the real test class reference this class and do the real verification.
 * 
 * @see package com.ibm.datatools.ots.tests.restapi.admin
 * 
 * */
public class AdminTAService extends AbstractAdminService {
	
	private static final Logger logger = LogManager.getLogger(AdminTAService.class);
	/**
	 * URL to verify the compress Table Command
	 * */
	public static final String TA_COMPRESS_TABLE = DSMURLUtils.URL_PREFIX + "/adm/core/ta/gen/TACompressTableCommand" ;
	/**
	 * URL to verify the backup database TA
	 * */
	public static final String TA_BACKUP_DATABASE = DSMURLUtils.URL_PREFIX  + "/adm/core/ta/gen/TABackupCommand";
	/**
	 * URL to verify the create database TA
	 * */
	public static final String TA_CREATE_DATABASE = DSMURLUtils.URL_PREFIX  + "/adm/core/ta/gen/TACreateDatabaseCommand";

	/**
	 * URL to verify the restore database TA
	 * */
	public static final String TA_RESTORE_COMMAND = DSMURLUtils.URL_PREFIX  + "/adm/core/ta/gen/TARestoreCommand";
	/**
	 * URL to verify the start instance TA
	 * */
	public static final String TA_START_INSTANCES_COMMAND = DSMURLUtils.URL_PREFIX  +"/adm/core/ta/gen/TAStartInstanceCommand";
	/**
	 * URL to verify the stop instance TA
	 * */
	public static final String TA_STOP_INSTANCES_COMMAND = DSMURLUtils.URL_PREFIX  +"/adm/core/ta/gen/TAStopInstanceCommand";
	/**
	 * URL to verify the quiesce instance TA
	 * */
	public static final String TA_QUIESCE_INSTANCES_COMMAND = DSMURLUtils.URL_PREFIX  +"/adm/core/ta/gen/TAQuiesceInstanceCommand";
	/**
	 * URL to verify the unquiesce instance TA
	 * */
	public static final String TA_UNQUIESCE_INSTANCES_COMMAND = DSMURLUtils.URL_PREFIX  +"/adm/core/ta/gen/TAUnquiesceInstanceCommand";
	/**
	 * URL to verify the activate database TA
	 * */
	public static final String TA_ACTIVATE_DATABASE_COMMAND = DSMURLUtils.URL_PREFIX  +"/adm/core/ta/gen/TAActivateDatabaseCommand";
	/**
	 * URL to verify the deactivate database TA
	 * */
	public static final String TA_DEACTIVATE_DATABASE_COMMAND = DSMURLUtils.URL_PREFIX  +"/adm/core/ta/gen/TADeactivateDatabaseCommand";
	/**
	 * URL to verify the recovery database TA
	 * */
	public static final String TA_RECOVERY_DATABASE_COMMAND = DSMURLUtils.URL_PREFIX  +"/adm/core/ta/gen/TARecoverCommand";
	/**
	 * URL to verify the roll forward TA
	 * */
	public static final String TA_ROLL_FORWARD_COMMAND = DSMURLUtils.URL_PREFIX  +"/adm/core/ta/gen/TARollForwardCommand";
	/**
	 * URL to verify the configure logging TA
	 * */
	public static final String TA_CONFIGURE_LOGGING_COMMAND = DSMURLUtils.URL_PREFIX  +"/adm/core/ta/gen/TAConfigureLoggingCommand";
	
	/**
	 * Property contained the post form data in TA function. this properties contains following entry:
	 * 1.getCompressTable: form data to verify the compress table TA, this form data do not contains the dbProfileName.
	 * 2.getBackupDatabase: form data to verify the backup database TA
	 * 2.<for new item, update it here>
	 * 
	 * */
	public static final String PROP_GET_TA = "getAdminTA.properties" ;
	
	/**
	 * add by wtaobj@cn.ibm.com
	 * URL to configure BLU acceleration
	 * */
	public static final String TA_CONFIGURE_BLU_COMMAND = DSMURLUtils.URL_PREFIX  +"/adm/core/ta/gen/TAConfigureBLUCommand";
	/**
	 * add by wtaobj@cn.ibm.com
	 * URL to quiesce database
	 * */
	public static final String TA_QUIESCE_DATABASE_COMMAND = DSMURLUtils.URL_PREFIX  +"/adm/core/ta/gen/TAQuiesceDatabaseCommand";
	/**
	 * add by wtaobj@cn.ibm.com
	 * URL to unquiesce database
	 * */
	public static final String TA_UNQUIESCE_DATABASE_COMMAND = DSMURLUtils.URL_PREFIX  +"/adm/core/ta/gen/TAUnquiesceDatabaseCommand";
	
	/**
	 * add by wtaobj@cn.ibm.com
	 * URL to revalidate database objects
	 * */
	public static final String TA_REVALIDATE_COMMAND = DSMURLUtils.URL_PREFIX  +"/adm/core/ta/gen/TARevalidateCommand";
	/**
	 * add by cyinmei@cn.ibm.com
	 * URL to export table data
	 * */
	public static final String TA_EXPORT_TABLE_COMMAND = DSMURLUtils.URL_PREFIX  +"/adm/core/ta/gen/TAExportTableCommand";
	
	/**
	 * add by cyinmei@cn.ibm.com
	 * URL to load table data
	 * */
	public static final String TA_LOAD_TABLE_COMMAND = DSMURLUtils.URL_PREFIX  +"/adm/core/ta/gen/TALoadUtilityCommand";
	
	/**
	 * add by cyinmei@cn.ibm.com
	 * URL to reorganize table 
	 * */
	public static final String TA_REORG_TABLE_COMMAND = DSMURLUtils.URL_PREFIX  +"/adm/core/ta/gen/TAReorgTableCommand";
	
	/**
	 * add by cyinmei@cn.ibm.com
	 * URL to runstats table 
	 * */
	
	public static final String TA_RUNSTATS_TABLE_COMMAND = DSMURLUtils.URL_PREFIX  +"/adm/core/ta/gen/TARunstatsCommand";
	
	/**
	 * add by cyinmei@cn.ibm.com
	 * URL to convert row table to column table 
	 * */
	
	public static final String TA_CONVERT_TABLE_COMMAND = DSMURLUtils.URL_PREFIX  +"/adm/core/ta/gen/TAConvertColumnCommand";
	
	/**
	 * add by cyinmei@cn.ibm.com
	 * URL to reorganize index 
	 * */
	public static final String TA_REORG_INDEX_COMMAND = DSMURLUtils.URL_PREFIX  +"/adm/core/ta/gen/TAReorgIndexCommand";
	
	public AdminTAService(){}
	/**
	 * Load the properties
	 * */	
	protected void LoadProperties(Vector<String> postdata){
		postdata.add(PROP_GET_TA) ;
	}
	
	
	public JSONObject callTAService(String path,String jsonObj,String dbProfile){

	
		Map<String,String> postData =	JSONUtils.parseJSON2MapString(jsonObj);		
		Assert.assertNotNull(dbProfile);		
		postData.put("dbProfileName",dbProfile);
	
		logger.info("Calling TAService with :"+postData);		
		Response response = post(path,postData, 200);
		
		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		
		return result;
	}

	/**
	 * using for TA create database and start instance
	 * @param path
	 * @param jsonObj
	 * @param dbProfile
	 * @param flag
	 * @return
	 */
	public JSONObject callInstanceTAService(String path,String jsonObj,String dbProfile,String instanceProfileName,String hostName){

		Map<String,String> postData =	JSONUtils.parseJSON2MapString(jsonObj);
		Assert.assertNotNull(dbProfile);		
		postData.put("databaseProfile",dbProfile); 
		postData.put("dbProfileName",instanceProfileName);
		postData.put("instanceProfileName", instanceProfileName);
		postData.put("hostName", hostName);

			
		logger.info("Calling TAService with :"+postData);
		Response response = post(path,postData, 200);
		
		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		String rererresult=result.toString();
		
		return result;
	}	

	
@Override
public String getJSONData(String key){
	String value = p.getProperty(key);
	if(value.endsWith(".json")){
		String filePath = Configuration.getPostDataPath()+"jsonData/"+"ta/"+value.trim();
		value = FileTools.readFileAsString(filePath) ;
	}
	return value ;
}



	
}
