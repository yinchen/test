package com.ibm.datatools.ots.tests.restapi.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Vector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import net.sf.json.JSONObject;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.jayway.restassured.response.Response;



public class ConnectionService extends RestAPIBaseTest {
	
	private static final Logger logger = LogManager.getLogger(ConnectionService.class);
	
	Properties p;
	
	public ConnectionService() throws FileNotFoundException, IOException{
		Vector<String> postdata = new Vector<String>();
		postdata.add("getConnections.properties");
		postdata.add("getAlertCounts.properties");
		postdata.add("getAlerts.properties");
		postdata.add("getGroups.properties");
		postdata.add("setFavorite.properties");
		postdata.add("getTableStorageRealtime.properties");
		
		p=new Properties();
		
		for(int i=0;i<postdata.size();i++){
			p.putAll(loadJSONPostData(postdata.get(i)));
		}
	}
	
	/*public ConnectionService(String filename) throws FileNotFoundException, IOException{
		p = loadJSONPostData(filename);

	}
	
	
	public ConnectionService(Vector<String> filename) throws FileNotFoundException, IOException{
		p=new Properties();
		
		for(int i=0;i<filename.size();i++){
			p.putAll(loadJSONPostData(filename.get(i)));
			//loadJSONPostData(filename[i]);
		}
		//p = loadJSONPostData(filename);

	}*/
	
	public Properties loadJSONPostData(String filename) throws FileNotFoundException, IOException{
		String filePath = Configuration.getPostDataPath()+"connectionsData/"+filename;
		Properties p = new Properties();
		p.load(new FileInputStream(filePath));
		return p;
	}
	
	public String getJSONData(String key){
		return p.getProperty(key);
	}
	
	public Properties getJSONProperties(){
		return this.p;
	}
	
	public JSONObject callConnectionsDataService(String jsonObj ){
		String postData = formatPostData(jsonObj);
		logger.info("Calling ConnectionsDataService with :"+postData);
		
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("profilesData");
		Response response = post(path,postData,200);

		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		return result;
	}
	

	public String formatPostData(String jsonObj){
		String connectionName = Configuration.getProperty("name");
		String userid = Configuration.getProperty("LoginUser");
		
		JSONObject postData = JSONObject.fromObject(jsonObj);
		
		long endTimestamp = System.currentTimeMillis();
		long startTimestamp= endTimestamp-3600000; //1 Hour Back
		
		if(postData.containsKey("name"))
			postData.put("name", connectionName);
		if(postData.containsKey("startTimestamp"))
			postData.put("startTimestamp", startTimestamp);
		if(postData.containsKey("endTimestamp"))
			postData.put("endTimestamp", endTimestamp);
		if(postData.containsKey("userid"))
			postData.put("userid", userid);
		return postData.toString();
	}
	
}
