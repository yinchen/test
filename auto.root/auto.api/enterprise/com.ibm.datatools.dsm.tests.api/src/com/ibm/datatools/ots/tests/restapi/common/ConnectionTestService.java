package com.ibm.datatools.ots.tests.restapi.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DBUtils;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.test.utils.JSONUtils;
import com.jayway.restassured.response.Response;

import net.sf.json.JSONObject;
import net.sf.json.xml.XMLSerializer;

/**
 * @author litaocdl
 * 
 * For the connection & Security Test services
 * 
 * */
public class ConnectionTestService extends RestAPIBaseTest {
	/**
	 * Database LUW 1054 Information, locates under config/databases folder.
	 * Add more in same format.
	 * */
	public static String DB_FILE_INFO_LUW1054 = Configuration.getDatabaseDirPath()+"db2v1054_luw_1.properties" ;
	
	
	private static final Logger logger = LogManager.getLogger(ConnectionTestService.class);
	public static final String PROP_GET_CONNECTION = "getConnectionTest.properties" ;
	public Properties jsonProperty = null  ;
	public String dbmgrPath;
	public String repositoryMgrPath;
	public String credentialPath = DSMURLUtils.URL_PREFIX + "/userCred/credMgr.do";
	public String commandPath = DSMURLUtils.URL_PREFIX + "/wticli/commandHandler.do";
	public String DBProviderPath = DSMURLUtils.URL_PREFIX + "/dbConnMgt/DBProvider.form";
	public String UserManagementPath = DSMURLUtils.URL_PREFIX + "/dswebservices/UserManagement.do";
	public String LicensePath = DSMURLUtils.URL_PREFIX + "/lic/license.do";
	public String connectionsDataPath;
	private String creator = Configuration.getProperty("LoginUser");
	private JSONObject oriRepoDBInfo = new JSONObject();

	public ConnectionTestService() {
		dbmgrPath = DSMURLUtils.URL_PREFIX + Configuration.getProperty("dbmgr");
		repositoryMgrPath = DSMURLUtils.URL_PREFIX + Configuration.getProperty("repMgt");
		connectionsDataPath = DSMURLUtils.URL_PREFIX + Configuration.getProperty("connectionData");

		Vector<String> postdata = new Vector<String>();
		/**
		 * Add the Alter properties here, each properties has more than one entry, each entry contains the params which need post to DSM
		 * */
		logger.info("Running ConnectionTestService - before LoadProperties");
		LoadProperties(postdata) ;
		logger.info("Running ConnectionTestService - after LoadProperties");
		jsonProperty=new Properties();
		
		for(int i=0;i<postdata.size();i++){
			jsonProperty.putAll(loadJSONPostData(postdata.get(i)));
		}
		logger.info("Running ConnectionTestService - end");
	}

	
	/**
	 * Load the properties
	 * */	
	protected void LoadProperties(Vector<String> postdata){
		postdata.add(PROP_GET_CONNECTION) ;
	}
	
	public Properties loadJSONPostData(String filename){
		Properties p = new Properties();
		String filePath = Configuration.getPostDataPath()+"connectionsData/"+filename;
		try{
			p.load(new FileInputStream(filePath));
		}catch(FileNotFoundException e){
			e.printStackTrace();
			throw new IllegalArgumentException("Error when loading file in method loadJSONPostData(), file name is: "+filePath);
		}catch(IOException e){
			e.printStackTrace();
			throw new IllegalArgumentException("Error when loading file in method loadJSONPostData(), file name is: "+filePath);
		}

		return p;
	}
	
	
	public String getJSONData(String key){
		String value = jsonProperty.getProperty(key);
		if(value.endsWith(".json")){
			String filePath = Configuration.getPostDataPath()+"jsonData/connectionsTest/"+value.trim();
			value = FileTools.readFileAsString(filePath) ;
		}
		return value ;
	}
	
	public String getXMLData(String key) {
		String value = jsonProperty.getProperty(key);
		if (value.endsWith(".xml")) {
			String filePath = Configuration.getPostDataPath() + "xmlData/connectionsTest/" + value.trim();
			value = FileTools.readFileAsString(filePath);
		}
		return value;
	}
	
	protected JSONObject newResult(Response response, String path, Map<String, String> postData) {
		JSONObject result = new JSONObject();
		result.accumulate( "URL", path );
		result.accumulate( "PostData", postData );
		result.accumulate( "ResponseCode", response.getStatusCode() );
		result.accumulate( "ResponseData", response.body().asString() );
		return result;
	}

	/////////////////////////////// APIs from DatabaseProfileManagerHTTPService////////////////////////////
	/**
	 * Add connection using jsonString with showProgress
	 */
	public JSONObject addConnection(String jsonString) {

		Map<String, String> postData = JSONUtils.parseJSON2MapString(jsonString);
		postData.put("cmd", "addProfileInJson");
		postData.put("format", "json");
		postData.put("showProgress", "true");

		return sendMessageAndGetResponseWithPost(dbmgrPath, postData);
	}

	/**
	 * Add connection using jsonString without showProgress
	 */
	public JSONObject addConnectionNoShowProgress(String jsonString) {

		Map<String, String> postData = JSONUtils.parseJSON2MapString(jsonString);
		postData.put("cmd", "addProfileInJson");
		postData.put("format", "json");

		return sendMessageAndGetResponseWithPost(dbmgrPath, postData);
	}
	
	/**
	 * Add connection using given profileName jsonString
	 * 
	 * JsonString is in the format of 
	 * {
         "cmd":"addProfileInJson",
         "dojo.preventCache":"1468822690013",
         "format":"json",	
         "profileJSON":{"name":"ConnectionTest123","dataServerExternalType":"DB2LUW","databaseName":"sample","host":"9.181.24.134","port":50005,"securityMechanism":"3","user":"db2inst5","password":"vm2dsmadmin","creator":"admin","JDBCProperties":"","xtraProps":"","comment":"","URL":"jdbc:db2://9.181.24.134:50005/sample:retrieveMessagesFromServerOnGetMessage=true;securityMechanism=3;","otsEventTablespace":null,"dataServerType":"DB2LUW"},
         "showProgress":"true"
         }

	 * */
	
	public JSONObject addConnection(String jsonString,String profileName){
		//Update the profileName first
		Map<String, String> connectionData = JSONUtils.parseJSON2MapString( jsonString );	
		String profileJSONS = connectionData.get("profileJSON") ;
		JSONObject profileJSON = JSONObject.fromObject(profileJSONS) ;
		if(profileJSON.containsKey("name")){
			profileJSON.remove("name") ;
			profileJSON.accumulate("name", profileName) ;
		}else{
			profileJSON.accumulate("name", profileName) ;
		}
		connectionData.remove("profileJSON");
		connectionData.put("profileJSON", profileJSON.toString());

		Response response = post(dbmgrPath, connectionData, 200);

		int resCode = response.getStatusCode();
		String resData = response.body().asString().trim();
		
		JSONObject result = new JSONObject();
		result.accumulate("ResponseCode", resCode);
		result.accumulate("ResponseData", resData);
		
		//We can do a verification  here, so all consumer of this method do not need to do the verification
		
		return result;
	}

	/**
	 * Test connection using jsonString
	 */
	public JSONObject testConnection(String jsonString) {
		Map<String, String> postData = JSONUtils.parseJSON2MapString(jsonString);
		postData.put("cmd", "testProfileConn");
		postData.put("format", "json");
		postData.put("showProgress", "true");

		return sendMessageAndGetResponseWithPost(dbmgrPath, postData);
	}

	/**
	 * Edit connection using jsonString
	 * cmd: updProfileInJson
	 * format: json
	 */
	public JSONObject editConnection(String jsonString) {

		Map<String, String> postData = JSONUtils.parseJSON2MapString(jsonString);
		postData.put("cmd", "updProfileInJson");
		postData.put("format", "json");

		return sendMessageAndGetResponseWithPost(dbmgrPath, postData);
	}
	
	/**
	 * Edit connection using jsonString
	 */
	public JSONObject getConnectionByProfileInJson(String profileName) {

		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "getProfileInJson");
		postData.put("format", "json");
		postData.put("profileJSON", profileName);

		return sendMessageAndGetResponseWithPost(dbmgrPath, postData);
	}
	/**
	 * get connection using jsonString without post expected status code
	 *  @author ybjwang@cn.ibm.com
	 */
	public JSONObject getConnectionByProfileInJsonWithoutCode(String profileName) {

		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "getProfileInJson");
		postData.put("format", "json");
		postData.put("profileJSON", profileName);

		return sendMessageAndGetResponseWithoutCode(dbmgrPath, postData);
	}
	
	/**
	 * Export connection to file
	 * @author ybjwang@cn.ibm.com
	 */
	public JSONObject exportConnection2Local(String profileName) {

		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "exportDBConnections");
		postData.put("args", "targetExportFile=checkExportEmpty,preview=true");
		String postPath = DSMURLUtils.URL_PREFIX + "/wticli/commandHandler.do";
		return sendMessageAndGetResponseWithPost(postPath, postData);
	}
	/**
	 * Export connection to file
	 * @author ybjwang@cn.ibm.com
	 */
	public JSONObject exportConnection2Server(String profileName) {

		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "exportDBConnections");
		postData.put("args", "targetExportFile=/opt/connection.csv,preview=false,fileLocation=server,exportPassword=true,overwrite=true");
		String postPath = DSMURLUtils.URL_PREFIX + "/wticli/commandHandler.do";
		return sendMessageAndGetResponseWithPost(postPath, postData);
	}
	/**
	 * Migrate HADR connection to Cluster
	 * @author ybjwang@cn.ibm.com
	 */
	public JSONObject migrateHADRConnection(String jsonString,String profileName){
		
		Map<String, String> postData = JSONUtils.parseJSON2MapString(jsonString);
		postData.put("cmd", "migrateCluster");
		postData.put("profileName",profileName);
		return sendMessageAndGetResponseWithPost(dbmgrPath, postData);
	}
	/**
	 * Import Connections from File
	 * @author ybjwang@cn.ibm.com
	 * @return
	 */
	public JSONObject importConnectionsFromFile(){
		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "importDBConnections");
		postData.put("args","dbConnImportFile=/opt/connection.csv,preview=true,duplicateChoice=null,checkImportFile=false");
		postData.put("initImport","true");
		return sendMessageAndGetResponseWithPost(commandPath, postData);
	}
	/**
	 * 
	 */
	public JSONObject importConnectionsFromURL(){
		Map<String, String> postData = new HashMap<String, String>();
		String server = Configuration.getProperty("dsmserver");
		String server_port = Configuration.getProperty("dsmport");
		String dbConnImportFile = server + ":" + server_port;
		postData.put("cmd", "importFromRestClient");
		postData.put("args","dbConnImportFile=" +dbConnImportFile + ",preview=true,duplicateChoice=null,checkImportFile=false");
		postData.put("initImport","true");
		return sendMessageAndGetResponseWithPost(commandPath, postData);
	}
	/**
	 * get all connections
	 */
	/*
	public JSONObject getAllProfiles(){
		
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "getAllProfiles");
		parameters.put("dojo.preventCache",String.valueOf(System.currentTimeMillis()));
		
		Response response = post(dbmgrPath, parameters, 200);
		String responseData = response.body().asString();

		XMLSerializer xmlSerializer = new XMLSerializer();
		JSONObject resData = (JSONObject) xmlSerializer.read( responseData );  

		return resData;

	}
	*/
	
	/**
	 * get all profiles
	 */
	public JSONObject getAllProfiles(String matchPrivilege,String includeTAGS,String includeUsrCred){
		
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "getAllProfiles");
		parameters.put("format", "json");
		parameters.put("matchPrivilege", matchPrivilege);
		parameters.put("userid","admin");
		parameters.put("includeTAGS",includeTAGS);
		parameters.put("includeUsrCred",includeUsrCred);
		parameters.put("dojo.preventCache",String.valueOf(System.currentTimeMillis()));
		
		
		Response response = post(dbmgrPath, parameters, 200);
		String responseData = response.body().asString().trim();
		JSONObject resData=JSONObject.fromObject(responseData);
		return resData;
	}
	

	/**
	 * get one connection
	 * cmd: getProfile
	 * format: the default format: xml
	 */
	public JSONObject getProfile(String profileName) {

		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "getProfile");
		parameters.put("profileName", profileName);

		Response response = post(dbmgrPath, parameters, 200);
		String responseData = response.body().asString();

		XMLSerializer xmlSerializer = new XMLSerializer();
		JSONObject resData = (JSONObject) xmlSerializer.read(responseData);

		return resData;
	}
	
	/**
	 * get one connection
	 * cmd:getProfile
	 * format: json
	 */
	public JSONObject getProfileWithJsonResult(String profileName) {
		JSONObject result = null;
		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "getProfile");
		postData.put("profileName", profileName);
		postData.put("format", "json");

		result = sendMessageAndGetResponseWithPost(dbmgrPath, postData);
			
		return result;
	}

	/**
	 * Remove a database Connection
	 * 
	 * cmd:delProfileInJson
     *format:json
     *profileJSON:300BUG70958
	 * 
	 */
	public JSONObject removeConnection(String profileName) {

		Long beginTime = System.currentTimeMillis();
		Long endTime = System.currentTimeMillis();
		JSONObject result = null;
		while (endTime - beginTime < 20000) {
			Map<String, String> postData = new HashMap<String, String>();
			postData.put("cmd", "delProfileInJson");
			postData.put("format", "json");
			postData.put("profileJSON", profileName);

			result = sendMessageAndGetResponseWithPost(dbmgrPath, postData);
			if (!result.getJSONObject("ResponseData").getString("message").contains("deadlock")) {
				break;
			}
		}
		return result;
	}

	/**
	 * Get All the connection profiles
	 * matchPrivilege:  return the connection profiles which match the privilege provided
           DSWEB.IS_DB_OWNER  : return the connection profile which current user is connection profile owner
           DSWEB.IS_DB_USER:  return the connection profile which current user is connection profile user and owner
           empty value: return all connection profile
	 * minVersionMatch: return the connection profile with min database version, 
	 *     like minVersionMatch=10.5, will only return the connection profile with version later than 10.5
	 * 
	 * cmd:getAllProfiles
	 * format:json
	 * 
	 */
	public JSONObject getAllConnectionProfilesWithJsonResult(String matchPrivilege, String minVersionMatch) {
		JSONObject result = null;
		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "getAllProfiles");
		postData.put("format", "json");
		
		if (matchPrivilege != null) {
			postData.put("matchPrivilege", matchPrivilege);
		}

		if (minVersionMatch != null) {
			postData.put("minVersionMatch", minVersionMatch);
		}

		result = sendMessageAndGetResponseWithPost(dbmgrPath, postData);
			
		return result;
	}
	
	
	/**
	 * Assign tags for profiles
	 */
	public JSONObject batchAssignTags(String assignTags,String removeTags) {

		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "batchAssignTags");
		parameters.put("format", "json");
		parameters.put("assignTags", assignTags);
		parameters.put("removeTags",removeTags);

		return sendMessageAndGetResponseWithPost(dbmgrPath, parameters);
	}
	
	/**
	 * List all filter tags
	 */
	public JSONObject listFilterTags() {

		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "listFilterTags");
		parameters.put("format", "json");
		parameters.put("userid", "admin");

		return sendMessageAndGetResponseWithPost(dbmgrPath, parameters);
	}
	
	
	
	/**
	 * Get All the connection profile names
	 * 
	 * cmd:getDBProfileNames
	 * format:json
	 * 
	 */
	public JSONObject getAllConnectionProfileName() {
		JSONObject result = null;
		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "getDBProfileNames");
		postData.put("format", "json");

		result = sendMessageAndGetResponseWithPost(dbmgrPath, postData);
			
		return result;
	}
	
	/**
	 * update the version and return the VRMF version
	 * 
	 * cmd:updateDatabaseVersionVRMF
	 * format:json
	 * profileName:the profile name
	 * 
	 */
	public JSONObject updateDatabaseVersionVRMF(String profileName) {
		JSONObject result = null;
		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "updateDatabaseVersionVRMF");
		postData.put("format", "json");
		postData.put("profileName", profileName);

		result = sendMessageAndGetResponseWithPost(dbmgrPath, postData);
			
		return result;
	}
	
	/**
	 * update the version and return the VRF version
	 * 
	 * cmd:updateDatabaseVersion
	 * format:json
	 * profileName:the profile name
	 * 
	 */
	public JSONObject updateDatabaseVersion(String profileName) {
		JSONObject result = null;
		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "updateDatabaseVersion");
		postData.put("format", "json");
		postData.put("profileName", profileName);

		result = sendMessageAndGetResponseWithPost(dbmgrPath, postData);
			
		return result;
	}
	
	/**
	 * assign tags to connections
	 */
	public JSONObject assignTagsToConnections(String jsonString) {
		JSONObject result = null;
		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "batchAssignTags");
		postData.put("format", "json");
		postData.put("assignTag", jsonString);  
		postData.put("removeTags", "[]");

		result = sendMessageAndGetResponseWithPost(dbmgrPath, postData);
			
		return result;
	}
	
	/**
	 * remove tags from connection
	 */
	public JSONObject removeTagFromConnection(String jsonString) {
		JSONObject result = null;
		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "batchAssignTags");
		postData.put("format", "json");
		postData.put("assignTag", "[]");  
		postData.put("removeTags", jsonString);

		result = sendMessageAndGetResponseWithPost(dbmgrPath, postData);
			
		return result;
	}
	
	
	//////////////////////////////////// APIs from UserCredentialsHTTPService/////////////////////////////////
	/**
	 * set and test credential
	 */
	
	public JSONObject setAndTestCredential(String resourceID, String namespace, String username, String password,
			String isTransient) {
		
		logger.info("Running setAndTestCredential with: " + resourceID + ":" +username + "/"+password);
		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "setAndTestCred");
		postData.put("resourceID", resourceID);
		postData.put("namespace", namespace);
		postData.put("username", username);
		postData.put("password", password);
		postData.put("isTransient", isTransient);
		
		return sendMessageAndGetResponseWithPost(credentialPath, postData);
	}
	
	public JSONObject setAndTestCredentialwithCredType(String resourceID, String namespace, String username, String password,
			String isTransient, String credType) {
		logger.info("Running setAndTestCredential with: " + resourceID + ":" +username + "/"+password);
		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "setAndTestCred");
		postData.put("resourceID", resourceID);
		postData.put("namespace", namespace);
		postData.put("username", username);
		postData.put("password", password);
		postData.put("isTransient", isTransient);
		postData.put("credType", credType);

		return sendMessageAndGetResponseWithPost(credentialPath, postData);
	}

	/**
	 * get credential
	 */
	public JSONObject getCredential(String resourceID, String namespace) {
		return getCredential(resourceID, namespace, null);
	}

	public JSONObject getCredential(String resourceID, String namespace, String credType) {

		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "getCred");
		postData.put("resourceID", resourceID);
		postData.put("namespace", namespace);
		if (credType != null) {
			postData.put("namespace", namespace);
		}

		return sendMessageAndGetResponseWithPost(credentialPath, postData);
	}

	/**
	 * set credential
	 */
	public JSONObject setCredential(String resourceID, String namespace, String username, String password,
			String isTransient) {

		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "setCred");
		postData.put("resourceID", resourceID);
		postData.put("namespace", namespace);
		postData.put("username", username);
		postData.put("password", password);
		postData.put("isTransient", isTransient);

		return sendMessageAndGetResponseWithPost(credentialPath, postData);
	}

	/**
	 * set credential
	 */
	public JSONObject removeCredential(String resourceID, String namespace) {

		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "removeCred");
		postData.put("resourceID", resourceID);
		postData.put("namespace", namespace);

		return sendMessageAndGetResponseWithPost(credentialPath, postData);
	}

	/**
	 * get Session credential
	 */
	public JSONObject getSessCredential(String resourceID, String namespace) {

		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "getSessCred");
		postData.put("resourceID", resourceID);
		postData.put("namespace", namespace);

		return sendMessageAndGetResponseWithPost(credentialPath, postData);
	}

	/**
	 * get current User credential
	 */
	public JSONObject getCurrUser() {

		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "currUser");

		return sendMessageAndGetResponseWithPost(credentialPath, postData);
		
	}

	//////////////////////////////////// APIs from RepositoryMgrHTTPService/////////////////////////////////
	/**
	 * get repository database information
	 */
	public JSONObject getRepDBInfo() {
		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "getRepDBInfo");

		return sendMessageAndGetResponseWithPost(repositoryMgrPath, postData);
	}

	/**
	 * attach repository database information
	 */
	public JSONObject attachRepDB(String xmlString) {
		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "attachRepDB");
		postData.put("sendRepositorySelectionEvent", "true");
		postData.put("newProfile", xmlString);
		postData.put("showProgress", "true");

		return sendMessageAndGetResponseWithPost(repositoryMgrPath, postData);
	}

	//////////////////////////////////// APIs from ConnectionsRequestHandler/////////////////////////////////
	/**
	 * get connections according to the jsonString parameter
	 */
	public JSONObject getConnections(String jsonString) {
		Map<String, String> postData = JSONUtils.parseJSON2MapString(jsonString);
		postData.put("cmd", "getConnections");

		return sendMessageAndGetResponseWithGet(connectionsDataPath, postData);
	}

	/**
	 * Test the repository whether it is true
	 */
	public JSONObject testRepository() {
		Map<String, String> getData = new HashMap<String, String>();
		getData.put("cmd", "testRepo");

		return sendMessageAndGetResponseWithGet(connectionsDataPath, getData);
	}

	/**
	 * get all connections according to the jsonString parameter
	 */
	public JSONObject getAllConnections(String jsonString) {
		Map<String, String> getData = JSONUtils.parseJSON2MapString(jsonString);
		getData.put("cmd", "getAll");

		return sendMessageAndGetResponseWithGet(connectionsDataPath, getData);
	}
	
   

	/**
	 * switch repository database title: one repositoryDb. This value can be
	 * found from
	 * testData/postData/jsonData/connectionsTest/connectionRepoDB.json
	 */
	public void switchRepoDB(String title) {
		
		String repoDB = getJSONData("connectionRepoDB");
		JSONObject repoDBJson = JSONObject.fromObject(repoDB);
		JSONObject detailJson= repoDBJson.getJSONObject(title);
		
		// Change the current repository db to another one
		String getRepoDBinfo = getXMLData("editRepDBWithClearTextPassword");

		Map<String, String> updatedValues = new HashMap<String, String>();
		updatedValues.put("databaseName", detailJson.getString("dbName"));
		updatedValues.put("host", detailJson.getString("host"));
		updatedValues.put("port", detailJson.getString("port"));
		updatedValues.put("user", detailJson.getString("user"));
		updatedValues.put("password", detailJson.getString("password"));
		updatedValues.put("dataServerType", detailJson.getString("dataServerType"));
		updatedValues.put("creator", creator);

		String newREPODB = replaceXMLUpdatedValuesByNames(getRepoDBinfo, updatedValues);

		JSONObject result = attachRepDB(newREPODB);
		String resultCode = result.getString("ResponseCode");
		Assert.assertEquals(resultCode, "200");

		String responseData = result.getString("ResponseData");
		XMLSerializer xmlSerializer = new XMLSerializer();
		JSONObject responseDataJson = (JSONObject) xmlSerializer.read(responseData);

		String expectedResult = getXMLData("editRepDBExpected");
		JSONObject expectedResultJson = (JSONObject) xmlSerializer.read(expectedResult);

//		verifyResultEditOrGetRepDB(expectedResultJson, responseDataJson, title);
	}

	public void keepRepositoryDBInfo() {
		JSONObject result = getRepDBInfo();
		String responseData = result.getString("ResponseData");
		XMLSerializer xmlSerializer = new XMLSerializer();
		JSONObject responseDataJson = (JSONObject) xmlSerializer.read(responseData);

		String oriRepoPassword = "N1cetest";

		if (responseDataJson.getJSONObject("result").containsKey("repDBConnInfo")) {
			String oriRepoDatabaseName = responseDataJson.getJSONObject("result").getJSONObject("repDBConnInfo")
					.getString("@databaseName");
			String oriRepoPort = responseDataJson.getJSONObject("result").getJSONObject("repDBConnInfo")
					.getString("@port");
			String oriRepoHost = responseDataJson.getJSONObject("result").getJSONObject("repDBConnInfo")
					.getString("@host");
			String oriRepoDataServerType = responseDataJson.getJSONObject("result").getJSONObject("repDBConnInfo")
					.getString("@dataServerType");
			String oriRepoUser = responseDataJson.getJSONObject("result").getJSONObject("repDBConnInfo")
					.getString("@user");
//			String oriRepoDatabaseVersion = responseDataJson.getJSONObject("result").getJSONObject("repDBConnInfo")
//					.getString("@databaseVersion");

			oriRepoDBInfo.put("databaseName", oriRepoDatabaseName);
			oriRepoDBInfo.put("host", oriRepoHost);
			oriRepoDBInfo.put("port", oriRepoPort);
			oriRepoDBInfo.put("dataServerType", oriRepoDataServerType);
			oriRepoDBInfo.put("user", oriRepoUser);
			oriRepoDBInfo.put("password", oriRepoPassword);
//			oriRepoDBInfo.put("version", oriRepoDatabaseVersion);
		}
	}

	public Boolean switchToOrignialRepDB() {
		Map<String, String> oriRepoDBValues = JSONUtils.parseJSON2MapString(oriRepoDBInfo.toString());

		if (!oriRepoDBValues.containsKey("databaseName")) {
			return false;
		}

		oriRepoDBValues.put("creator", creator);

		String getRepoDBinfo = getXMLData("editRepDBWithClearTextPassword");
		String oriREPODB = replaceXMLUpdatedValuesByNames(getRepoDBinfo, oriRepoDBValues);

		JSONObject result = attachRepDB(oriREPODB);
		String resultCode = result.getString("ResponseCode");
		Assert.assertEquals(resultCode, "200");

		String responseData = result.getString("ResponseData");
		XMLSerializer xmlSerializer = new XMLSerializer();
		JSONObject responseDataJson = (JSONObject) xmlSerializer.read(responseData);

		String expectedResult = getXMLData("editRepDBExpected");
		JSONObject expectedResultJson = (JSONObject) xmlSerializer.read(expectedResult);

		return verifyResultEditOrGetRepDB(expectedResultJson, responseDataJson, oriRepoDBInfo);
	}
	
	
	public Boolean switchToConfigRepDB() {
		
		// Change the current repository db to another one
		String getRepoDBinfo = getXMLData("editRepDBWithClearTextPassword");

		Map<String, String> updatedValues = new HashMap<String, String>();
		updatedValues.put("databaseName", DBUtils.getConfigValue("dbProfile"));
		updatedValues.put("host", DBUtils.getConfigValue("host"));
		updatedValues.put("port", DBUtils.getConfigValue("port"));
		updatedValues.put("user", DBUtils.getConfigValue("user"));
		updatedValues.put("password", DBUtils.getConfigValue("password"));
		updatedValues.put("dataServerType", DBUtils.getConfigValue("dbType"));
		updatedValues.put("creator", creator);

		String newREPODB = replaceXMLUpdatedValuesByNames(getRepoDBinfo, updatedValues);
				
		JSONObject result = attachRepDB(newREPODB);
		String resultCode = result.getString("ResponseCode");
		Assert.assertEquals(resultCode, "200");

		return true;
	}

	
	public String replaceConnectionData(String addConnectionParameters, JSONObject databaseInfo) {
		JSONObject addConnectionParametersJson = JSONObject.fromObject(addConnectionParameters);
		String profileJSONS = addConnectionParametersJson.getString("profileJSON");
		JSONObject profileJSON = JSONObject.fromObject(profileJSONS);
		if (databaseInfo.containsKey("dbProfile")) {
			replaceJsonObjectValue(profileJSON, "name", databaseInfo.getString("dbProfile"));
		}
		if (databaseInfo.containsKey("databaseName")) {
			replaceJsonObjectValue(profileJSON, "databaseName", databaseInfo.getString("databaseName"));
		}
		if (databaseInfo.containsKey("hostName")) {
			replaceJsonObjectValue(profileJSON, "host", databaseInfo.getString("hostName"));
		}
		if (databaseInfo.containsKey("port")) {
			replaceJsonObjectValue(profileJSON, "port", databaseInfo.getString("port"));
		}
		if (databaseInfo.containsKey("user")) {
			if (profileJSON.containsKey("user") && (!profileJSON.getString("user").equals(""))) {
				replaceJsonObjectValue(profileJSON, "user", databaseInfo.getString("user"));
			} 
			if (profileJSON.containsKey("operationUser") && (!profileJSON.getString("operationUser").equals(""))) {
				replaceJsonObjectValue(profileJSON, "operationUser", databaseInfo.getString("user"));
			}
		}
		if (databaseInfo.containsKey("password")) {
			if (profileJSON.containsKey("password") && (!profileJSON.getString("password").equals(""))) {
				replaceJsonObjectValue(profileJSON, "password", databaseInfo.getString("password"));
			}
			if (profileJSON.containsKey("operationPassword") && (!profileJSON.getString("operationPassword").equals(""))) {
				replaceJsonObjectValue(profileJSON, "operationPassword", databaseInfo.getString("password"));
			}
		}
		if (databaseInfo.containsKey("dataServerType")) {
			replaceJsonObjectValue(profileJSON, "dataServerType", databaseInfo.getString("dataServerType"));
		}
		if (databaseInfo.containsKey("dataServerExternalType")) {
			replaceJsonObjectValue(profileJSON, "dataServerExternalType",
					databaseInfo.getString("dataServerExternalType"));
		}
		if (databaseInfo.containsKey("securityMechanism")) {
			replaceJsonObjectValue(profileJSON, "securityMechanism", databaseInfo.getString("securityMechanism"));
		}
		if (databaseInfo.containsKey("dsmUser")) {
			replaceJsonObjectValue(profileJSON, "creator", databaseInfo.getString("dsmUser"));
		}
		if (databaseInfo.containsKey("truststoreFileName") && (databaseInfo.containsKey("truststorePassword"))) {
			String truststoreFilePath = uploadFile(databaseInfo.getString("truststoreFileName"));
			profileJSON.put("dsconnmgt_jdbc_sslTrustStoreLocation", truststoreFilePath);
			profileJSON.put("dsconnmgt_jdbc_sslTrustStorePassword", databaseInfo.getString("truststorePassword"));
		}

		replaceURLValue(profileJSON);
		addConnectionParametersJson.remove("profileJSON");
		addConnectionParametersJson.put("profileJSON", profileJSON.toString());
		return addConnectionParametersJson.toString();
	}

	public String replaceConnectionData(String addConnectionParameters, String dbProfile, String databaseName,
			String hostName, String port, String user, String password) {
		JSONObject databaseInfo = new JSONObject();
		databaseInfo.put("dbProfile", dbProfile);
		databaseInfo.put("databaseName", databaseName);
		databaseInfo.put("hostName", hostName);
		databaseInfo.put("port", port);
		databaseInfo.put("user", user);
		databaseInfo.put("password", password);

		return replaceConnectionData(addConnectionParameters, databaseInfo);
	}

	public String getURLFromConnectionData(String connectionData) {
		JSONObject connectionDataJson = JSONObject.fromObject(connectionData);
		String profileJSONS = connectionDataJson.getString("profileJSON");
		JSONObject profileJSON = JSONObject.fromObject(profileJSONS);
		return profileJSON.getString("URL");
	}

	public void replaceJsonObjectValue(JSONObject jsonObject, String key, String newValue) {
		if (jsonObject != null) {
			if (jsonObject.containsKey(key) && (newValue != null)) {
				jsonObject.remove(key);
				jsonObject.accumulate(key, newValue);
			}
		}
	}

	private void replaceURLValue(JSONObject profileJSON) {
		if (profileJSON != null) {
			String host = null;
			String port = null;
			String database = null;
			String url = null;

			if (profileJSON.containsKey("host")) {
				host = profileJSON.getString("host");
			}

			if (profileJSON.containsKey("port")) {
				port = profileJSON.getString("port");
			}

			if (profileJSON.containsKey("databaseName")) {
				database = profileJSON.getString("databaseName");
			} else if (profileJSON.containsKey("location")) {
				database = profileJSON.getString("location");
			}

			if (profileJSON.containsKey("URL")) {
				url = profileJSON.getString("URL");
			}

			if (url != null) {
				int firstColonLocation = url.indexOf(":");
				int lastColonLocation = url.lastIndexOf(":");
				String replaceString = url.substring(firstColonLocation, lastColonLocation);
				String replacedString = ":db2://" + host + ":" + port + "/" + database;
				profileJSON.remove("URL");
				profileJSON.accumulate("URL", url.replace(replaceString, replacedString));

				if (profileJSON.containsKey("securityMechanism")) {
					url = profileJSON.getString("URL");
					firstColonLocation = url.indexOf("securityMechanism=");
					lastColonLocation = url.indexOf(";", firstColonLocation);
					replaceString = url.substring(firstColonLocation, lastColonLocation);
					String securityMechanismInURL = null;
					if (profileJSON.getString("securityMechanism").equals("100")) {
						replacedString = "securityMechanism=3;sslConnection=true";
					} else {
						securityMechanismInURL = profileJSON.getString("securityMechanism");
						replacedString = "securityMechanism=" + securityMechanismInURL;
					}
					profileJSON.remove("URL");
					profileJSON.accumulate("URL", url.replace(replaceString, replacedString));
				}
			}
		}
	}

	private JSONObject sendMessageAndGetResponseWithPost(String path, Map<String, String> postData) {
		Response response = post(path, postData, 200);

		int resCode = response.getStatusCode();
		String resData = response.body().asString().trim();

		JSONObject result = new JSONObject();
		result.accumulate("ResponseCode", resCode);
		result.accumulate("ResponseData", resData);
		return result;
	}
	/**Send reqeust without expected code 200
	 * @author ybjwang@cn.ibm.com
	 * @param path
	 * @param postData
	 * @return
	 */
	private JSONObject sendMessageAndGetResponseWithoutCode(String path, Map<String, String> postData) {
		//This line is different with sendMessageAndGetResponseWithPost
		//Response response = post(path, postData,200);
		Response response = post(path, postData);

		int resCode = response.getStatusCode();
		String resData = response.body().asString().trim();

		JSONObject result = new JSONObject();
		result.accumulate("ResponseCode", resCode);
		result.accumulate("ResponseData", resData);
		return result;
	}

	private JSONObject sendMessageAndGetResponseWithGet(String path, Map<String, String> getData) {
		Response response = get(path, getData, 200);

		int resCode = response.getStatusCode();
		String resData = response.body().asString().trim();

		JSONObject result = new JSONObject();
		result.accumulate("ResponseCode", resCode);
		result.accumulate("ResponseData", resData);
		return result;
	}

	/**
	 * Get the response result code,in connection component, even a failed request will return httpstatus code = 200. we are using resultCode to indicates 
	 * a request success or failed.
	 * success: resultCode=success
	 * failed: resultCode=failure
	 * */
	public String getResponseResultCode(JSONObject result){
		Assert.assertNotNull(result);
		String resData = result.getString("ResponseData") ;
		Map<String, String> resDataMap = JSONUtils.parseJSON2MapString(resData) ;
		return resDataMap.get("resultCode");
	}
	/**
	 * Detail message if request failed.
	 * */
	public String getResponseResultMessage(JSONObject result){
		Assert.assertNotNull(result);
		String resData = result.getString("ResponseData") ;
		Map<String, String> resDataMap = JSONUtils.parseJSON2MapString(resData) ;
		return resDataMap.get("message") ;
	}
	
	/**
	 * Get the response from the RepsonseData
	 */
	public String getResponseResultResponse(JSONObject result) {
		Assert.assertNotNull(result);
		String resData = result.getString("ResponseData");
		Map<String, String> resDataMap = JSONUtils.parseJSON2MapString(resData);
		return resDataMap.get("response");
		
	}
	/**
	 * Get the response from the RepsonseData in XML format especially for export connection
	 */
	public String getExportConnResponseResultResponse(JSONObject result) {
		Assert.assertNotNull(result);
		String resData = result.getString("ResponseData");
		if(resData.contains("CLI20012I")){
			return resData;
		}else{
			int first = resData.indexOf(">");
			String splitData[] = resData.split("response");
			if(splitData !=null){
				
				String responseData = splitData[1].replace(">", "");
				responseData = responseData.replace("<", "");
				responseData = responseData.replace("/", "");
				return responseData;
			}else return "";
		}
	}
		/**
		 * Get the response from the RepsonseData in XML format especially for import connection
		 */
		public String getImportConnResponseResultData(JSONObject result) {
			Assert.assertNotNull(result);
			String resData = result.getString("ResponseData");
			String connectionDef = "ConnectionDefinition";
			String newConnDef = "newconnections";
			String duplicatConnDef = "duplicateconnections";
			Boolean success = false;
			if(resData.contains(connectionDef) && resData.contains(newConnDef)&&resData.contains(duplicatConnDef)){
				success = true;
			}
		
			return success ?   "SUCCESS" :"";
	}
		/**
		 * Get the response from the RepsonseData for import connection from URL
		 */
		public String getImportFromURLResponseData(JSONObject result) {
			Assert.assertNotNull(result);
			String resData = result.getString("ResponseData");		
			return resData;
	}
		/**
		 * 
		 */
		public String getResponseforImportNewAndDuplicateConn(JSONObject result){
			Assert.assertNotNull(result);
			String resData = result.getString("ResponseData");		
			return resData;
		}
	/**
	 * This suits for most request response verification
	 * */
	public void verifyJSONResult(JSONObject result){
		String resultCode = getResponseResultCode(result) ;
		
		if("failure".equalsIgnoreCase(resultCode)){
			String message = this.getResponseResultMessage(result) ;
			Assert.fail(message);
		}
	}

	public void verifyXMLResult(String actualResult, String expectedResult) {
		Document actualDoc = createDocumentFromString(actualResult);
		Document expectedDoc = createDocumentFromString(expectedResult);
		Element actualDocElement = actualDoc.getDocumentElement();
		Element expectedDocElement = expectedDoc.getDocumentElement();
		Boolean compareResult = compareXMLElements(actualDocElement, expectedDocElement);
		Assert.assertTrue(compareResult,
				"The acutal xml result is not the same as the expected xml result: The actual Result is:" + actualResult
						+ ". The expected Result is:" + expectedResult);
	}

	private Boolean compareXMLElements(Element actualDocElement, Element expectedDocElement) {
		if (actualDocElement == null && expectedDocElement == null) {
			return true;
		}
		if ((actualDocElement != null && expectedDocElement == null)
				|| (actualDocElement == null && expectedDocElement != null)) {
			return false;
		}
		if (actualDocElement.getTextContent().equals(expectedDocElement.getTextContent())
				&& actualDocElement.getTagName().equals(expectedDocElement.getTagName())) {
			if (actualDocElement.hasChildNodes() == false && expectedDocElement.hasChildNodes() == false) {
				return true;
			}
			if ((actualDocElement.hasChildNodes() != false && expectedDocElement.hasChildNodes() == false)
					|| (actualDocElement.hasChildNodes() == false && expectedDocElement.hasChildNodes() != false)) {
				return false;
			}
			NodeList actualList = actualDocElement.getChildNodes();
			NodeList expectedList = expectedDocElement.getChildNodes();
			if (actualList.getLength() != expectedList.getLength()) {
				return false;
			}
			for (int i = 0; i < actualList.getLength(); i++) {
				if (!actualList.item(i).isEqualNode(expectedList.item(i))) {
					return false;
				}
			}
			return true;

		} else {
			return false;
		}
	}

	private Document createDocumentFromString(String contents) {
		Document doc1 = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			dbf.setCoalescing(true);
			dbf.setIgnoringElementContentWhitespace(true);
			dbf.setIgnoringComments(true);
			DocumentBuilder db;
			db = dbf.newDocumentBuilder();
			doc1 = db.parse(new InputSource(new StringReader(contents)));
		} catch (ParserConfigurationException e) {
			logger.info("The string can't be tansftered to xml document. e: " + e);
			e.printStackTrace();
		} catch (SAXException e) {
			logger.info("The string can't be tansftered to xml document. e: " + e);
			e.printStackTrace();
		} catch (IOException e) {
			logger.info("The string can't be tansftered to xml document. e: " + e);
			e.printStackTrace();
		}

		return doc1;
	}

	/**
	 * Replace xml content with newValue when meet the condition. 
	 * Like: The old xml content is as below:
	 * <property name="name1" updatedValue="META_DB_CONNECTION_ID"/> 
	 * If updatedValues=("name1", "hello"), then after replace, 
	 * the xml content will be changed to: 
	 * <property name="name1" updatedValue="hello"/>
	 */
	public String replaceXMLUpdatedValuesByNames(String xmlContent, Map<String, String> updatedValues) {
		Assert.assertNotNull(xmlContent);
		Document xmlDocument = createDocumentFromString(xmlContent);
		Element xmlDocElement = xmlDocument.getDocumentElement();

		replaceOneXMLElementUpdatedValueByName(xmlDocElement, updatedValues);
		return element2String(xmlDocElement);
	}

	private void replaceOneXMLElementUpdatedValueByName(Element oneXMLElement, Map<String, String> updatedValues) {
		Assert.assertNotNull(oneXMLElement);
		Assert.assertNotNull(updatedValues);
		Element oneChildElement = null;
		Iterator<Entry<String, String>> updatedValuesIts = updatedValues.entrySet().iterator();
		while (updatedValuesIts.hasNext()) {
			Entry<String, String> updatedValueEntry = updatedValuesIts.next();
			if (oneXMLElement.getAttribute("name").equals(updatedValueEntry.getKey())) {
				oneXMLElement.setAttribute("updatedValue", updatedValueEntry.getValue());
			}
		}

		if (oneXMLElement.hasChildNodes()) {
			NodeList childNodes = oneXMLElement.getChildNodes();
			Node oneChildNode = null;
			for (int i = 0; i < childNodes.getLength(); i++) {
				if (childNodes.item(i) instanceof Element) {
					oneChildNode = childNodes.item(i);
					oneChildElement = (Element) oneChildNode;
					replaceOneXMLElementUpdatedValueByName(oneChildElement, updatedValues);
				}
			}
		}
	}

	private String element2String(Element element) {
		String s = "";
		try {
			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer();
			Source source = new DOMSource(element);
			StringWriter out = new StringWriter();
			Result output = new StreamResult(out);
			transformer.transform(source, output);
			out.flush();
			s = out.toString();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return s.substring(38);
	}

	public void verifyResultEditOrGetRepDB(JSONObject expectedResultJson, JSONObject actualResultJson,
			String expectedRepoDBTitle) {
		String repoDB = getJSONData("connectionRepoDB");
		JSONObject repoDBJson = JSONObject.fromObject(repoDB);
		JSONObject detailJson = repoDBJson.getJSONObject(expectedRepoDBTitle);
		ArrayList<String> ignoreKeyValues = new ArrayList<String>();
		ignoreKeyValues.add("@databaseName");
		ignoreKeyValues.add("@port");
		ignoreKeyValues.add("@host");
		ignoreKeyValues.add("@dataServerType");
		ignoreKeyValues.add("@user");
		ignoreKeyValues.add("@databaseVersion");

//		Assert.assertEquals(JSONUtils.compareJsons(expectedResultJson, actualResultJson, ignoreKeyValues), true);

		String actualDatabaseName = actualResultJson.getJSONObject("result").getJSONObject("repDBConnInfo")
				.getString("@databaseName");
		String actualPort = actualResultJson.getJSONObject("result").getJSONObject("repDBConnInfo").getString("@port");
		String actualHost = actualResultJson.getJSONObject("result").getJSONObject("repDBConnInfo").getString("@host");
		String actualDataServerType = actualResultJson.getJSONObject("result").getJSONObject("repDBConnInfo")
				.getString("@dataServerType");
		String actualUser = actualResultJson.getJSONObject("result").getJSONObject("repDBConnInfo").getString("@user");
		//String actualDatabaseVersion = actualResultJson.getJSONObject("result").getJSONObject("repDBConnInfo")
		//		.getString("@databaseVersion");

		Assert.assertEquals(actualDatabaseName, detailJson.getString("dbName"),
				"The acutualDatabaseName is not the same as the expected databaseName");
		Assert.assertEquals(actualPort, detailJson.getString("port"),
				"The acutualPort is not the same as the expected port");
		Assert.assertEquals(actualHost, detailJson.getString("host"),
				"The actualHost is not the same as the expected host");
		Assert.assertEquals(actualDataServerType, detailJson.getString("dataServerType"),
				"The actualDataServerType is not the same as the expected dataServerType");
		Assert.assertEquals(actualUser, detailJson.getString("user"),
				"The actualUser is not the same as the expected user");
		//Assert.assertEquals(actualDatabaseVersion, detailJson.getString("databaseVersion"),
		//		"The databaseVersion is not the same as the expected databaseVersion");
	}

	public Boolean verifyResultEditOrGetRepDB(JSONObject expectedResultJson, JSONObject actualResultJson,
			JSONObject expectedRepoDBInfo) {
		ArrayList<String> ignoreKeyValues = new ArrayList<String>();
		ignoreKeyValues.add("@databaseName");
		ignoreKeyValues.add("@port");
		ignoreKeyValues.add("@host");
		ignoreKeyValues.add("@dataServerType");
		ignoreKeyValues.add("@user");
		ignoreKeyValues.add("@databaseVersion");

		if (JSONUtils.compareJsons(expectedResultJson, actualResultJson, ignoreKeyValues) != true) {
			return false;
		}

		String actualDatabaseName = actualResultJson.getJSONObject("result").getJSONObject("repDBConnInfo")
				.getString("@databaseName");
		String actualPort = actualResultJson.getJSONObject("result").getJSONObject("repDBConnInfo").getString("@port");
		String actualHost = actualResultJson.getJSONObject("result").getJSONObject("repDBConnInfo").getString("@host");
		String actualDataServerType = actualResultJson.getJSONObject("result").getJSONObject("repDBConnInfo")
				.getString("@dataServerType");
		String actualUser = actualResultJson.getJSONObject("result").getJSONObject("repDBConnInfo").getString("@user");
		String actualDatabaseVersion = actualResultJson.getJSONObject("result").getJSONObject("repDBConnInfo")
				.getString("@databaseVersion");

		if (actualDatabaseName.equals(expectedRepoDBInfo.getString("databaseName"))
				&& actualPort.equals(expectedRepoDBInfo.getString("port"))
				&& actualHost.equals(expectedRepoDBInfo.getString("host"))
				&& actualDataServerType.equals(expectedRepoDBInfo.getString("dataServerType"))
				&& actualUser.equals(expectedRepoDBInfo.getString("user"))
				&& actualDatabaseVersion.equals(expectedRepoDBInfo.getString("version"))) {
			return true;
		} else {
			return false;
		}
	}

	public void testDatabaseConnection(JSONObject acutalDatabaseInfo) {
		// Get the connection data, and then replace some information with the actual database info
		String addConnJsonBody = getJSONData("addConnWithEncryptedUserIDAndPWEncrytedSSData");

		String replacedAddConnJsonBody = replaceConnectionData(addConnJsonBody, acutalDatabaseInfo);

		// Test connection
		JSONObject result = testConnection(replacedAddConnJsonBody);

		// Verify the result code
		String resultCode = getResponseResultCode(result);
		if (!resultCode.equals("success")) {
			logger.error("The database can't be connected. The reponse of testConnection API is:" + result.toString());
		}
		Assert.assertEquals(resultCode, "success", "The database can't be connected. The reason is: "
				+ result.getJSONObject("ResponseData").getString("message"));
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("message"), "");
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("response"), "\"\"");
		logger.info("The database can be connected");
	}
	
	
	
////////////////////////////////////APIs from UserManagement/////////////////////////////////

/**
* Add DSM USer
*/
public JSONObject addDSMUser(String userId, String  password,String userPrivileges) {

	Map<String, String> parameters = new HashMap<String, String>();
	String  userProfile = "{\"first_name\":\"\",\"last_name\":\"\",\"email\":\"\"}";
	parameters.put("cmd", "addUser");
	parameters.put("userid", userId);
	parameters.put("password", password);
	parameters.put("userPrivileges",userPrivileges);
	parameters.put("isAdminAction","true");
	parameters.put("userProfile", userProfile);
	return sendMessageAndGetResponseWithPost(UserManagementPath, parameters);
}


/**
* Edit DSM USer Role
*/
public JSONObject editDSMUser(String userId, String  password, String userPrivileges) {

	Map<String, String> parameters = new HashMap<String, String>();
	String  userProfile = "{\"first_name\":\"\",\"last_name\":\"\",\"email\":\"\"}";
	parameters.put("cmd", "editUser");
	parameters.put("userid", userId);
	parameters.put("oldPassword", "");
	parameters.put("password", password);
	parameters.put("userPrivileges",userPrivileges);
	parameters.put("isAdminAction","true");
	parameters.put("userProfile", userProfile);
	return sendMessageAndGetResponseWithPost(UserManagementPath, parameters);
}


/**
* Edit DSM User DB Privilege
*/
public JSONObject editDSMUserDBPrivilege(String userId, String  dbProfile, String userPrivileges) {

	Map<String, String> parameters = new HashMap<String, String>();
	String  userProfile = "{\"first_name\":\"\",\"last_name\":\"\",\"email\":\"\"}";
	parameters.put("cmd", "editUser");
	parameters.put("userid", userId);
	parameters.put("userPrivileges",userPrivileges);
	parameters.put("isAdminAction","true");
	parameters.put("userProfile", userProfile);
	parameters.put("forSource",dbProfile);
	return sendMessageAndGetResponseWithPost(UserManagementPath, parameters);
}


/**
* Edit DSM User DB Privilege
*/
public JSONObject getImplicitDBPrivileges(String userId, String  dbProfile) {

	Map<String, String> parameters = new HashMap<String, String>();
	parameters.put("cmd", "getImplicitDBPrivileges");
	parameters.put("userid", userId);
	parameters.put("forSource",dbProfile);
	return sendMessageAndGetResponseWithPost(UserManagementPath, parameters);
}


/**
* Get the role of dsm user
*/
public JSONObject getUser(String userId) {

	Map<String, String> parameters = new HashMap<String, String>();
	parameters.put("cmd", "getUser");
	parameters.put("userid", userId);
	return sendMessageAndGetResponseWithPost(UserManagementPath, parameters);
}


/**
* Delete specify DSM USer
*/
public JSONObject delDSMUser(String userId) {

Map<String, String> parameters = new HashMap<String, String>();
parameters.put("cmd", "deleteUser");
parameters.put("userid", userId);


return sendMessageAndGetResponseWithPost(UserManagementPath, parameters);
}


////////////////////////////////////APIs from License/////////////////////////////////

/**
* Get License forDB profile
*/
public JSONObject getDB2License(String profileName) {

Map<String, String> parameters = new HashMap<String, String>();
parameters.put("cmd", "GET_DB2_LICENSE");
parameters.put("profileName",profileName);

return sendMessageAndGetResponseWithPost(LicensePath, parameters);
}


}



