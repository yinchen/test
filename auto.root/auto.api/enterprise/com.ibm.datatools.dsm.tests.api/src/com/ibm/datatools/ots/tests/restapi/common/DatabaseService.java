package com.ibm.datatools.ots.tests.restapi.common;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.xml.XMLSerializer;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.ots.tests.restapi.base.ResponseParser;
import com.jayway.jsonpath.JsonPath;
import com.jayway.restassured.response.Response;


public class DatabaseService extends RestAPIBaseTest{

	public static final String[] propertyKeys = { "name", "dataServerType","databaseName",
		"host", "port", "securityMechanism", "user", "password","xtraProps","comment","URL","creator",
		"dbConnProfVisibility","Truststorelocation","Truststorepassword" };
	
	private static final Logger logger = LogManager.getLogger(DatabaseService.class);
	String path ;
	
	public DatabaseService(){
		path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("dbmgr");
	}

	public JSONObject addDatabaseConnection(String dbName,String host,int port,String user,String pwd){
		
		
		JSONObject jsonObj = new JSONObject();
		jsonObj.accumulate("cmd", "addConnection");
		jsonObj.accumulate("databaseName",dbName);
		jsonObj.accumulate("host",host);
		jsonObj.accumulate("port",port);
		jsonObj.accumulate("user",user);
		jsonObj.accumulate("userid",Configuration.getProperty("LoginUser"));
		jsonObj.accumulate("password",pwd);
		
		Response response =post(DSMURLUtils.URL_PREFIX + Configuration.getProperty("connectionData"),jsonObj.toString(), 200);
		
		
		int resCode =response.getStatusCode();
		String resData = response.body().asString().trim();
		
		JSONObject result = new JSONObject();
		result.accumulate("ResponseCode", resCode);
		result.accumulate("ResponseData", resData);
		return result;
		
		
	}
	
	public JSONObject addSSLDatabaseConnection(String dbName,String host,int port,String user,String pwd,String Trustusr,String Trustpw ){
		
		
		JSONObject jsonObj = new JSONObject();
		jsonObj.accumulate("cmd", "addConnection");
		jsonObj.accumulate("databaseName",dbName);
		jsonObj.accumulate("host",host);
		jsonObj.accumulate("port",port);
		jsonObj.accumulate("user",user);
		jsonObj.accumulate("userid",Configuration.getProperty("LoginUser"));
		jsonObj.accumulate("password",pwd);
		jsonObj.accumulate("Truststorelocation",Trustusr);
		jsonObj.accumulate("Truststorepassword",Trustpw);
		
		Response response =post(DSMURLUtils.URL_PREFIX + Configuration.getProperty("connectionData"),jsonObj.toString(), 200);
		
		
		int resCode =response.getStatusCode();
		String resData = response.body().asString().trim();
		
		JSONObject result = new JSONObject();
		result.accumulate("ResponseCode", resCode);
		result.accumulate("ResponseData", resData);
		return result;
		
		
	}

	public boolean addDatabaseConnection(String pathToDBProfileFile) throws IOException {
		
		Properties p = new Properties();
		p.load(new FileInputStream(pathToDBProfileFile));
		String profileJSON =composeProfileJSON(p);
		logger.info(profileJSON);

		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "addProfileInJson");
		parameters.put("format", "json");
		parameters.put("profileJSON", profileJSON);
		parameters.put("dojo.preventCache",String.valueOf(System.currentTimeMillis()));
		
		Response response = post(path,parameters, 200);

		String responseData = response.body().asString();
		
		logger.debug("Add Connection Response:" + response.body().asString());
		String responseCode = ResponseParser.getDBConResultCode(JSONObject.fromObject(responseData));
		if(responseCode.equalsIgnoreCase("success")){
			logger.info("Database added successfully");
			return true;
		}
		else{
			logger.error("There was an error while adding the database connection ");
			logger.error(response.body().toString());
			return false;
		}

		
	}
	public boolean deleteDatabaseConnection(String profileName){
		logger.info("Deleting profile "+profileName);
		
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "delProfileInJson");
		parameters.put("format", "json");
		parameters.put("profileJSON", profileName);
		parameters.put("dojo.preventCache",String.valueOf(System.currentTimeMillis()));
		
		Response response = post(path,parameters, 200);
		String responseData = response.body().asString();
		
		String responseCode = ResponseParser.getDBConResultCode(JSONObject.fromObject(responseData));
		if(responseCode.equalsIgnoreCase("success")){
			logger.info("Profile "+profileName+" deleted successfully");
			return true;
		}
		else{
			logger.error("There was an error while deleting the profile "+profileName);
			logger.error(response.toString());
			return false;
		}
	}
	
	
	public JSONObject getAllConnections() throws FileNotFoundException, IOException{
		
		ConnectionService cs = new ConnectionService();
		JSONObject resObj =cs.callConnectionsDataService(cs.getJSONData("Get_all_connections"));	
		System.out.println(resObj.get("ResponseData"));
		JSONObject responseData = JSONObject.fromObject(resObj.get("ResponseData"));
		return JSONObject.fromObject(responseData.get("response"));
		
	}
	
	public boolean checkConnectionExists(String host,String port,String databasename) throws FileNotFoundException, IOException{
		boolean connExists =false;
		JSONObject responseData =getAllConnections();
		JSONArray result =JSONArray.fromObject(JsonPath.read(responseData,"$data[0]"));
		for(int i=1;i<result.size();i++){ //first element is always timestamp info-ignoring it
			JSONObject db =(JSONObject) result.get(i);
			String dbhost = (String) db.get("host");
			String dbport = (String) db.get("port");
			String dbname = (String) db.get("databaseName");
			if(dbhost.trim().equalsIgnoreCase(host.trim()) && dbport.trim().equalsIgnoreCase(port.trim()) && dbname.trim().equalsIgnoreCase(databasename.trim())){
				connExists=true;
				break;
			}
			
		}
		return connExists;
	}
	
	public boolean checkConnectionExists(String connName) throws FileNotFoundException, IOException{
		boolean connExists =false;
		JSONObject responseData =getAllConnections();
		Object obj =responseData.get(connName);
		if(obj!=null)
			connExists=true;
		return connExists;
	}
	
	public JSONObject getMonDBInfo(String connName) throws FileNotFoundException, IOException{
		JSONObject responseData = getAllConnections();
		return responseData.getJSONObject(connName);
//		JSONArray result =JSONArray.fromObject(JsonPath.read(responseData,"$data[0]"));
//		JSONObject db = null;
//		for(int i=1;i<result.size();i++){ //first element is always timestamp info-ignoring it
//			db =(JSONObject) result.get(i);
//			String name = (String) db.get("name");
//			if(name.equals(connName)){
//				break;
//			}
//		}
//		return db;
	}
	
//	public Connection getRepoDBConn() throws Exception{
//		Connection repoDBConn = null;
//		JSONObject monDBObj = getMonDBInfo("OTSTEST");
//		if(monDBObj != null){
//			String dbhost = (String) monDBObj.get("host");
//			String dbport = (String) monDBObj.get("port");
//			String dbProfile = "REPODB";
//			String user = "db2inst1";
//			String password = "N1cetest";
//			try {
//				Class.forName("com.ibm.db2.jcc.DB2Driver");
//				repoDBConn = DriverManager.getConnection("jdbc:db2://"+dbhost+":"+dbport+"/"+dbProfile+"", ""+user+"", ""+password+"");
//			} catch (Exception e) {
//				throw e;
//			}
//		}
//		return repoDBConn;
//	}
	
	public void getAllProfiles(){
		
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "getAllProfiles");
		parameters.put("dojo.preventCache",String.valueOf(System.currentTimeMillis()));
		
		Response response = post(path, parameters, 200);
		String responseData = response.body().asString();
		logger.info( responseData );  
		XMLSerializer xmlSerializer = new XMLSerializer();
		JSONObject json = (JSONObject) xmlSerializer.read( responseData );  
		logger.info(json);
	}
	
	
	/**
	 * The value property URL is composed by other property values filled by
	 * user, now just assume the required properties are set in ps by users,
	 * this function just uses these properties to form a URL property and put
	 * it into ps.
	 * 
	 * @param ps
	 */
	private void composePropertyURL(Properties ps) {
		if (ps == null)
			return;

		String tmpl = "jdbc:db2://%s:%s/%s:retrieveMessagesFromServerOnGetMessage=true;securityMechanism=%s;";
		String host = ps.getProperty("host");
		String port = ps.getProperty("port");
		String databaseName = ps.getProperty("databaseName");
		String securityMechanism = ps.getProperty("securityMechanism");
		String URL = String.format(tmpl, host, port, databaseName,
				securityMechanism);
		ps.put("URL", URL);
	}

	private String composeProfileJSON(Properties ps) throws IOException {
		String profileJson="";
		
		JSONObject obj = new JSONObject();
		if (ps != null) {
			composePropertyURL(ps);
			for (String key : propertyKeys) {
				String value = ps.getProperty(key);
				if (value == null)
					value = "";
				obj.put(key, value);
				
			}
			profileJson =obj.toString();
		}
		System.out.println(profileJson);
		return profileJson; 		
	}
	
	public boolean isMonitoringEnabled() throws FileNotFoundException, IOException{
		boolean enabled = false;
		ConnectionService cs = new ConnectionService();
		JSONObject resObj =cs.callConnectionsDataService(cs.getJSONData("get_dbConnInfo"));
		JSONObject responseData =(JSONObject) resObj.get("ResponseData");
		enabled =JsonPath.read(responseData, "$data[0].[1].monitoringOn");
		return enabled;
	}
	
	/*
	public void activateDatabase(String dbProfileName, String dbName){
		String script = "ACTIVATE DATABASE "+dbName+";";
		System.out.println(script);
		String activateDBPath ="/console/adm/core/common/Execution";
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("dbProfileName", dbProfileName);
		parameters.put("id", "commoneditor_editor_SQLEditor_0");
		parameters.put("script", script);
		parameters.put("offset", "0");
		parameters.put("type","CLP");
		parameters.put("terminator", ";");
		parameters.put("failureAction", "CONTINUE");
		parameters.put("successAction", "AUTOCOMMIT");
		parameters.put("rowLimit", "100");
		parameters.put("showRowNumber", "true");
		parameters.put("refreshExplorer", "false");
		parameters.put("currentPath", "");
		parameters.put("currentSchema", "");
		parameters.put("action", "RUNSCRIPT");
		parameters.put("dojo.preventCache", String.valueOf(System.currentTimeMillis()));
		Response response = post(activateDBPath, parameters, 200);
		System.out.println("RESPONSE :::: "+response.body());

	} 
	
	public void main(String[] args){
		DatabaseService ds = new DatabaseService();
		ds.activateDatabase("", "");
		
	}
	*/
		
}
