package com.ibm.datatools.ots.tests.restapi.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import net.sf.json.JSONObject;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DBUtils;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.test.utils.JSONUtils;
import com.jayway.restassured.response.Response;

public class HADRTestService extends RestAPIBaseTest {

	private static final Logger logger = LogManager.getLogger(HADRTestService.class);
	public Properties jsonProperty = null;
	public String dbmgrPath;

	public HADRTestService() throws FileNotFoundException, IOException {
		
		dbmgrPath = DSMURLUtils.URL_PREFIX + Configuration.getProperty("dbmgr");

		Vector<String> postdata = new Vector<String>();
		/**
		 * Add the Alter properties here, each properties has more than one
		 * entry, each entry contains the params which need post to DSM
		 */
		logger.info("Running HADRTestService - before LoadProperties");
		LoadProperties(postdata);
		logger.info("Running HADRTestService - after LoadProperties");

		jsonProperty = new Properties();

		for (int i = 0; i < postdata.size(); i++) {
			jsonProperty.putAll(loadJSONPostData(postdata.get(i)));
		}
		logger.info("Running HADRTestService - end");

	}

	/**
	 * Load the properties
	 */
	protected void LoadProperties(Vector<String> postdata) {
		postdata.add("getHADR.properties");
		postdata.add("getHADRTest.properties");
	}

	public Properties loadJSONPostData(String filename) {
		Properties p = new Properties();
		String filePath = Configuration.getPostDataPath() + "connectionsData/" + filename;
		try {
			p.load(new FileInputStream(filePath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new IllegalArgumentException(
					"Error when loading file in method loadJSONPostData(), file name is: " + filePath);
		} catch (IOException e) {
			e.printStackTrace();
			throw new IllegalArgumentException(
					"Error when loading file in method loadJSONPostData(), file name is: " + filePath);
		}

		return p;
	}

	public String getJSONData(String key) {
		String value = jsonProperty.getProperty(key);
		if (value.endsWith(".json")) {
			String filePath = Configuration.getPostDataPath() + "jsonData/connectionsTest/" + value.trim();
			value = FileTools.readFileAsString(filePath);
		}
		return value;
	}

	public Properties getJSONProperties() {
		return this.jsonProperty;
	}

	/////////////////////////////// APIs from DatabaseProfileManagerHTTPService////////////////////////////
	/**
	 * Add connection using jsonString
	 */
	public JSONObject addConnection(String jsonString) {

		Map<String, String> postData = JSONUtils.parseJSON2MapString(jsonString);
		postData.put("cmd", "addProfileInJson");
		postData.put("format", "json");
		postData.put("showProgress", "true");

		return sendMessageAndGetResponseWithPost(dbmgrPath, postData);
	}
	
	/////////////////////////////// APIs from DatabaseProfileManagerHTTPService////////////////////////////
	/**
	 * Get connection using jsonString
	 */
	public JSONObject callConnectionsDataService(String jsonString ){
		String postData = formatPostData(jsonString);
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("connectionData");
		Response response = post(path,postData,200);

		JSONObject result = new JSONObject();
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		return result;
	}


	public String formatPostData(String jsonObj) {

		String userid = Configuration.getProperty("LoginUser");

		JSONObject postData = JSONObject.fromObject(jsonObj);

		long endTimestamp = System.currentTimeMillis();
		long startTimestamp = endTimestamp - 3600000; // 1 Hour Back

		if (postData.containsKey("startTimestamp"))
			postData.put("startTimestamp", startTimestamp);
		if (postData.containsKey("endTimestamp"))
			postData.put("endTimestamp", endTimestamp);
		if (postData.containsKey("userid"))
			postData.put("userid", userid);
		return postData.toString();
	}

	/**
	 * Remove a database Connection
	 * 
	 * cmd:delProfileInJson format:json profileJSON:300BUG70958
	 * 
	 */
	public JSONObject removeHADRConnection(String profileName) {

		
		Long beginTime = System.currentTimeMillis();
		Long endTime = System.currentTimeMillis();
		JSONObject result = null;
		while (endTime - beginTime < 20000) {
			Map<String, String> postData = new HashMap<String, String>();
			postData.put("cmd", "delProfileInJson");
			postData.put("format", "json");
			postData.put("profileJSON", profileName+"__0");

			result = sendMessageAndGetResponseWithPost(dbmgrPath, postData);
			if(result.isNullObject())
				break;
			if (!result.getJSONObject("ResponseData").getString("message").contains("deadlock")) {
				break;
			}
		}
		return result;
	}
	
	private JSONObject sendMessageAndGetResponseWithPost(String path, Map<String, String> postData) {
		Response response = post(path, postData, 200);

		int resCode = response.getStatusCode();
		String resData = response.body().asString().trim();

		JSONObject result = new JSONObject();
		result.accumulate("ResponseCode", resCode);
		result.accumulate("ResponseData", resData);
		return result;
	}

	/*
	 * Get the response result code,in connection component, even a failed
	 * request will return httpstatus code = 200. we are using resultCode to
	 * indicates a request success or failed. success: resultCode=success
	 * failed: resultCode=failure
	 */
	public String getResponseResultCode(JSONObject result) {
		Assert.assertNotNull(result);
		String resData = result.getString("ResponseData");
		Map<String, String> resDataMap = JSONUtils.parseJSON2MapString(resData);
		return resDataMap.get("resultCode");
	}
	

	
	public JSONObject getResponseData(JSONObject result, String pass) {
		JSONObject jo = JSONObject.fromObject(result);
		String profileName = (String) jo.get("name");
		Connection conn = null;
		
		try {
			conn = DBUtils.getConnection(profileName, pass);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Assert.assertNotNull(conn);
		Assert.assertNotNull(result);
		
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}
		
		return responseData;
	}
	

	
	public String getURLFromConnectionData(String connectionData) {
		JSONObject connectionDataJson = JSONObject.fromObject(connectionData);
		String profileJSONS = connectionDataJson.getString("profileJSON");
		JSONObject profileJSON = JSONObject.fromObject(profileJSONS);
		return profileJSON.getString("URL");
	}
	
	private static void assertResponseFailure(int responseCode, JSONObject result) {
		Assert.fail("FAILURE=> Response code =" + responseCode + " for URL: " + result.get("URL") + ", POSTDATA:"
				+ result.get("PostData"));
	}

	private static void assertExceptionFailure(int responseCode, JSONObject result) {
		Assert.fail("FAILURE:=>ExceptionCaught=true in responseJson for URL: " + result.get("URL") + ", POSTDATA:"
				+ result.get("PostData") + " with RESPONSECODE:" + responseCode + " and RESPONSEDATA:"
				+ result.get("ResponseData"));
	}

	private static boolean isExceptionCaught(JSONObject responseData) {
		return responseData.toString().contains("\"exceptionCaught\":true");
	}


}
