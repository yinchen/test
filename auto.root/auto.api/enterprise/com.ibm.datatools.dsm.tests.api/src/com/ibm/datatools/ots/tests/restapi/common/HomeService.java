package com.ibm.datatools.ots.tests.restapi.common;

import java.util.Map;
import java.util.Vector;

import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.test.utils.JSONUtils;
import com.jayway.restassured.response.Response;


/**
 * @author cyinmei
 * 
 * This class is the services class for testing home page. 
 * the real test class reference this class and do the real verification.
 * 
 * @see package com.ibm.datatools.ots.tests.restapi.home
 * 
 * */
public class HomeService extends AbstractAdminService {
	
	private static final Logger logger = LogManager.getLogger(HomeService.class);
	/**
	 * URL to handle the home request
	 * */
	
	public static final String HOME_REQUEST = DSMURLUtils.URL_PREFIX + "/connectionsData/connectionsData.do";
	
	/**
	 * Property contained the post form data for home page. 
	 * 
	 * */
	
	public static final String PROP_GET_ALTER = "getHomeData.properties" ;
	
	/**
	 * Load the properties
	 * */	
	protected void LoadProperties(Vector<String> postdata){
		postdata.add(PROP_GET_ALTER) ;
	}
	
	public JSONObject callHomeService(String path,String jsonObj,String dbProfile){

		Map<String,String> postData = JSONUtils.parseJSON2MapString(jsonObj);
		
		Assert.assertNotNull(dbProfile);		
		postData.put("dbProfileName",dbProfile);
		
		logger.info("Calling HomeService with :"+postData);
		
		Response response = post(path,postData,200);
		
		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		
		return result;
	}
	
	public JSONObject callHomeService(String path,String jsonObj){

		Map<String,String> postData = JSONUtils.parseJSON2MapString(jsonObj);
			
		logger.info("Calling HomeService with :"+postData);
		
		Response response = post(path,postData,200);
		
		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		
		return result;
	}
	
	
	@Override
	public String getJSONData(String key){
		String value = p.getProperty(key);
		if(value.endsWith(".json")){
			String filePath = Configuration.getPostDataPath()+"jsonData/"+"home/"+value.trim();
			value = FileTools.readFileAsString(filePath) ;
		}
		return value ;
	}
}
