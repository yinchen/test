package com.ibm.datatools.ots.tests.restapi.common;

import java.util.HashMap;
import java.util.Map;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.jayway.restassured.response.Response;

public class LogService extends RestAPIBaseTest{

	public LogService(){
		
	}
	
	public String testLogservice(){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("logservice");
		Map<String, String> parameters = new HashMap<String,String>();
		parameters.put("", "");
		Response response = get(path,parameters, 200);
		
		String result = response.asString().trim();
		return result;
	}
	
}
