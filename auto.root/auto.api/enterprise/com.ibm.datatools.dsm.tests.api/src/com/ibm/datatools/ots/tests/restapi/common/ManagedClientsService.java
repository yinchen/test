package com.ibm.datatools.ots.tests.restapi.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Vector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import net.sf.json.JSONObject;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.jayway.restassured.response.Response;



public class ManagedClientsService extends RestAPIBaseTest {
	
	private static final Logger logger = LogManager.getLogger(ManagedClientsService.class);
	private String connectionName;
	Properties p;
	
	public ManagedClientsService(String dbprofile) throws FileNotFoundException, IOException{
		this.connectionName = dbprofile;
		p=new Properties();
		p.putAll(loadJSONPostData("getManagedClients.properties"));
	}
	
	public Properties loadJSONPostData(String filename) throws FileNotFoundException, IOException{
		String filePath = Configuration.getPostDataPath()+"managedClientsData/"+filename;
		Properties p = new Properties();
		p.load(new FileInputStream(filePath));
		return p;
	}
	
	public String getJSONData(String key){
		return p.getProperty(key);
	}
	
	public Properties getJSONProperties(){
		return this.p;
	}
	
	public JSONObject callManagedClientsService(String jsonObj ){
		JSONObject requestData = JSONObject.fromObject(jsonObj);
		logger.info("Calling Managed Clients Service with :"+requestData.toString());
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("managedClients");
		Response response = get(path,requestData,200);

		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("PostData", requestData.toString());
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		return result;
	}

	public JSONObject formatRequestData(String jsonObj){
		
//		String userid = Configuration.getProperty("LoginUser");
		
		JSONObject requestData = JSONObject.fromObject(jsonObj);
		
//		long endTimestamp = System.currentTimeMillis();
//		long startTimestamp= endTimestamp-3600000; //1 Hour Back
		
//		if(requestData.containsKey("name"))
//			requestData.put("name", connectionName);
//		if(requestData.containsKey("startTimestamp"))
//			requestData.put("startTimestamp", startTimestamp);
//		if(requestData.containsKey("endTimestamp"))
//			requestData.put("endTimestamp", endTimestamp);
//		if(requestData.containsKey("userid"))
//			requestData.put("userid", userid);
		return requestData;
	}
	
}
