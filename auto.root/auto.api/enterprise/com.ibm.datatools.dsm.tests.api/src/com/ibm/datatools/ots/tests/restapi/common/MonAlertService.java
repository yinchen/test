package com.ibm.datatools.ots.tests.restapi.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;





import net.sf.json.JSONObject;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.JSONUtils;
import com.jayway.restassured.response.Response;



public class MonAlertService  extends RestAPIBaseTest {
	
	private static final Logger logger = LogManager.getLogger(MonAlertService.class);
	private String connectionName;
	Properties p;
	
	public MonAlertService(String dbprofile) throws FileNotFoundException,IOException{
		Vector<String> postdata = new Vector<String>();		
	//	postdata.add("getAlerts.properties");
		postdata.add("getAlertsAll.properties");
		p=new Properties();
		this.connectionName = dbprofile;
		for(int i=0;i<postdata.size();i++){
			p.putAll(loadJSONPostData(postdata.get(i)));
		}
	}
	
	public Properties loadJSONPostData(String filename) throws FileNotFoundException, IOException{
		String filePath = Configuration.getPostDataPath()+"connectionsData/"+filename;
		Properties p = new Properties();
		p.load(new FileInputStream(filePath));
		return p;
	}
	
	public String getJSONData(String key){
		return p.getProperty(key);
	}
	
	public Properties getJSONProperties(){
		return this.p;
	}
	
	public JSONObject callMonAlertService(String jsonObj ){
		//String postData = formatPostData(jsonObj);
		Map<String,String> postData = JSONUtils.parseJSON2MapString(jsonObj);
	//	logger.info("Calling MonAlertService with :"+postData);
		
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("listAlerts");
		Response response = post(path,postData,200);

		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
	//	logger.info("That is  :"+response.body().asString());
		return result;
	}
	

	public JSONObject callMonAlertDetailService(String jsonObj, String ID){
		Map<String,String> postData =	JSONUtils.parseJSON2MapString(jsonObj);
		logger.info("Calling one Alert Detail with :"+postData);
		//String connectionName = Configuration.getProperty("name");
		//String path = "/console/healthsnapshot/HealthViewsResultSetDataProvider.form?sqlPropID=forceApplication&cmd=getAllRows&dbProfileName="+connectionName+"&agentID="+ID;
		String path = DSMURLUtils.URL_PREFIX + "/healthsnapshot/AlertDetails.form?cmd=getAlertExtendedDetails&alertID="+ID;
		//http://9.115.71.79:11080/console/healthsnapshot/AlertDetails.form?cmd=getAlertExtendedDetails&alertID=3412
		logger.info("path is"+path);
		Response response = post(path,postData,200);

				JSONObject resultDetail = new JSONObject();
		resultDetail.accumulate("URL", path);
		resultDetail.accumulate("PostData", postData);
		resultDetail.accumulate("ResponseCode", response.getStatusCode());
		resultDetail.accumulate("ResponseData", response.body().asString());
	//	logger.info("Detail is"+response.body().asString());
		return resultDetail;
		

	} 
	
/*	public JSONObject callMonAlertDetailService(String jsonObj ){
		String postData = formatPostData(jsonObj);
		logger.info("Calling MonAlertDetailService with :"+postData);

		String path = Configuration.getProperty("connectionData");
		Response response = post(path,postData,200);

		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		return result;
	}
	*/ 


	public String formatPostData(String jsonObj){
		
		String userid = Configuration.getProperty("LoginUser");
		
		JSONObject postData = JSONObject.fromObject(jsonObj);
		
		long endTimestamp = System.currentTimeMillis();
	//	long startTimestamp = System.currentTimeMillis();
		long startTimestamp = endTimestamp-3600000; 
		//1 Hour Back
		
		if(postData.containsKey("name"))
			postData.put("name", connectionName);
		if(postData.containsKey("startTimestamp"))
			postData.put("startTimestamp", startTimestamp);
		if(postData.containsKey("endTimestamp"))
			postData.put("endTimestamp", endTimestamp);
		if(postData.containsKey("userid"))
			postData.put("userid", userid);
				return postData.toString();
	}
	
}
