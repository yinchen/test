package com.ibm.datatools.ots.tests.restapi.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Vector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import net.sf.json.JSONObject;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.jayway.restassured.response.Response;



public class MonHistoryService extends RestAPIBaseTest {
	
	private static final Logger logger = LogManager.getLogger(MonHistoryService.class);
	private String connectionName;
	Properties p;
	
	public MonHistoryService(String dbprofile) throws FileNotFoundException, IOException{
		Vector<String> postdata = new Vector<String>();		
		postdata.add("getMonHistory.properties");
		this.connectionName = dbprofile;
		p=new Properties();
		
		for(int i=0;i<postdata.size();i++){
			p.putAll(loadJSONPostData(postdata.get(i)));
		}
	}
	
	public Properties loadJSONPostData(String filename) throws FileNotFoundException, IOException{
		String filePath = Configuration.getPostDataPath()+"connectionsData/"+filename;
		Properties p = new Properties();
		p.load(new FileInputStream(filePath));
		return p;
	}
	
	public String getJSONData(String key){
		return p.getProperty(key);
	}
	
	public Properties getJSONProperties(){
		return this.p;
	}
	
	public JSONObject callMonHistoryService(String jsonObj ){
		String postData = formatPostData(jsonObj);
		logger.info("Calling MonHistoryService with :"+postData);
		
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("connectionData");
		Response response = post(path,postData,200);

		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		return result;
	}
	
	public JSONObject callMonHistoryBufferService(String jsonObj ){
		String postData = formatPostData(jsonObj);
		logger.info("Calling MonRealtimeService with :"+postData);
		
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("bufferPoolData");
		Response response = post(path,postData,200);

		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		return result;
	}
	
		
	

	public String formatPostData(String jsonObj){
		
		String userid = Configuration.getProperty("LoginUser");
		
		JSONObject postData = JSONObject.fromObject(jsonObj);
		
		long endTimestamp = System.currentTimeMillis();
		long startTimestamp= endTimestamp-3600000; //1 Hour Back
		
		if(postData.containsKey("connectionName"))
			postData.put("connectionName", connectionName);
		if(postData.containsKey("name"))
			postData.put("name", connectionName);
		if(postData.containsKey("startTimestamp"))
			postData.put("startTimestamp", startTimestamp);
		if(postData.containsKey("endTimestamp"))
			postData.put("endTimestamp", endTimestamp);
		if(postData.containsKey("userid"))
			postData.put("userid", userid);
		return postData.toString();
	}
	
}
