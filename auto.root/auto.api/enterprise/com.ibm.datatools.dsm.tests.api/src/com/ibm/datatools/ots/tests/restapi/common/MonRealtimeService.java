package com.ibm.datatools.ots.tests.restapi.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



import net.sf.json.JSONObject;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.JSONUtils;
import com.jayway.restassured.response.Response;



public class MonRealtimeService extends RestAPIBaseTest {
	
	private static final Logger logger = LogManager.getLogger(MonRealtimeService.class);
	private String connectionName;	
	Properties p;
	
	public MonRealtimeService(String dbprofile) throws FileNotFoundException, IOException{
		this.connectionName = dbprofile;
		Vector<String> postdata = new Vector<String>();		
		postdata.add("getMonRealtime.properties");
		
		p=new Properties();
		
		for(int i=0;i<postdata.size();i++){
			p.putAll(loadJSONPostData(postdata.get(i)));
		}
	}
	
	public Properties loadJSONPostData(String filename) throws FileNotFoundException, IOException{
		String filePath = Configuration.getPostDataPath()+"connectionsData/"+filename;
		Properties p = new Properties();
		p.load(new FileInputStream(filePath));
		return p;
	}
	
	public String getJSONData(String key){
		return p.getProperty(key);
	}
	
	public Properties getJSONProperties(){
		return this.p;
	}
	
	public JSONObject callMonRealtimeService(String jsonObj ){
		String postData = formatPostData(jsonObj);
		logger.info("Calling MonRealtimeService with :"+postData);
		
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("connectionData");
		Response response = post(path,postData,200);

		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		return result;
	}
	
	public JSONObject callMonRealtimeBufferService(String jsonObj ){
		String postData = formatPostData(jsonObj);
		logger.info("Calling MonRealtimeService with :"+postData);
		
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("bufferPoolData");
		Response response = post(path,postData,200);

		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		return result;
	}
	
	public JSONObject callMonRealtimeForce(String jsonObj, String ID){
		Map<String,String> postData =	JSONUtils.parseJSON2MapString(jsonObj);
		logger.info("Calling MonRealtimeForce with :"+postData);
		String path = DSMURLUtils.URL_PREFIX + "/healthsnapshot/HealthViewsResultSetDataProvider.form?sqlPropID=forceApplication&cmd=getAllRows&dbProfileName="+connectionName+"&agentID="+ID;
		logger.info("path is"+path);
		Response response = post(path,postData,200);
		
		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		return result;
	}

	public JSONObject callMonRealtimeCancelActivity(String jsonObj, String AppID, String ActivityID,String UowID){
		Map<String,String> postData =	JSONUtils.parseJSON2MapString(jsonObj);
		logger.info("Calling MonRealtimeCancelActivity with :"+postData);
		String path = DSMURLUtils.URL_PREFIX + "/healthsnapshot/HealthViewsResultSetDataProvider.form?sqlPropID=cancelActivity&cmd=getAllRows&dbProfileName="+connectionName+"&appHandle="+AppID+"&uowID="+UowID+"&activityID="+ActivityID;
		logger.info("path is"+path);
		Response response = post(path,postData,200);
		
		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		return result;
	}
	public JSONObject callMonRealtimeStorage(String jsonObj){
		Map<String,String> postData =	JSONUtils.parseJSON2MapString(jsonObj);
		logger.info("Calling callMonRealtimeStorage with :"+postData);
		String path = DSMURLUtils.URL_PREFIX + "/tablePerfData/tablePerfData.do";
		logger.info("path is"+path);
		Response response = post(path,postData,200);
		
		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("PostData", postData);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		return result;
	}
	
	
	
	public String formatPostData(String jsonObj){
		
		String userid = Configuration.getProperty("LoginUser");
		
		JSONObject postData = JSONObject.fromObject(jsonObj);
		
		long endTimestamp = System.currentTimeMillis();
		long startTimestamp= endTimestamp-3600000; //1 Hour Back
		
		if(postData.containsKey("name"))
			postData.put("name", connectionName);
		if(postData.containsKey("startTimestamp"))
			postData.put("startTimestamp", startTimestamp);
		if(postData.containsKey("endTimestamp"))
			postData.put("endTimestamp", endTimestamp);
		if(postData.containsKey("userid"))
			postData.put("userid", userid);
		return postData.toString();
	}
	
}
