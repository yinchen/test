package com.ibm.datatools.ots.tests.restapi.common;

import java.io.File;
import java.io.FileInputStream;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Map;

import com.jayway.restassured.config.SessionConfig;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.ByteUtil;
import com.ibm.datatools.test.utils.HMACSignatureHelper;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.HttpClientConfig;
import com.jayway.restassured.response.Headers;
import com.jayway.restassured.response.Response;

import net.sf.json.JSONObject;


public class RestAPIBaseTest {

	private static final Logger logger = LogManager.getLogger(RestAPIBaseTest.class);
	private static HashMap<String, String> cookies = new HashMap<String, String>();
	private static boolean loggedIn= false;
	private static String dsmEdition = Configuration.getProperty("dsm.edition");
	private static int DEFAULT_TIMEOUT_IN_MS = 60*5*1000;
	private static String sorigin = null;
	private static final String SOrigin_HEADER_KEY = "X-SOrigin";
	
	public RestAPIBaseTest(){
		RestAssured.baseURI  = Configuration.getProperty("dsmserver");
		RestAssured.port     = Integer.parseInt(Configuration.getProperty("dsmport"));
		RestAssured.config = RestAssured.config().httpClient(new HttpClientConfig()
			    .setParam(ClientPNames.CONN_MANAGER_TIMEOUT, Long.valueOf(DEFAULT_TIMEOUT_IN_MS))  // HttpConnectionManager connection return time
			    .setParam(CoreConnectionPNames.CONNECTION_TIMEOUT, DEFAULT_TIMEOUT_IN_MS) // Remote host connection time
			    .setParam(CoreConnectionPNames.SO_TIMEOUT, DEFAULT_TIMEOUT_IN_MS)  // Remote host response time
			);
		
		if("bi".equals(dsmEdition)){
		    RestAssured.basePath = "/gateway/default/dsm";
		}
		else{
			RestAssured.basePath = "";	
		}
	
	}
	
	public static HashMap<String, String> getCookies() {
		return cookies;
	}
	
	public static void loginDSM(){
		
		System.out.println("--------DSM Config Information------");
		System.out.println("baseURI is: "+RestAssured.baseURI);
		System.out.println("port is: "+RestAssured.port);
		System.out.println("------------------------------------");		
		
		logger.info("Logging in to DSM...");
		logger.info("URL:"+RestAssured.baseURI+":"+RestAssured.port);
		logger.info("basePath:"+RestAssured.basePath);		
		
		//Referer
		String referer = null;
		String loginURL = null;
		if("console".equals(dsmEdition)) {
			referer = RestAssured.baseURI + ":" + RestAssured.port + "/"+ Configuration.getProperty("console.URL.PREFIX") + Configuration.getProperty("Referer");
			loginURL = "/" + Configuration.getProperty("console.URL.PREFIX") + Configuration.getProperty("CS_LoginURL");
		}
		else {
			referer = RestAssured.baseURI + ":" + RestAssured.port + Configuration.getProperty("Referer");
			loginURL = DSMURLUtils.URL_PREFIX + Configuration.getProperty("EE_LoginURL");
		}			
		
		Response response = RestAssured.with().
		parameters(
			"formAction", Configuration.getProperty("Command"),
			"j_username", Configuration.getProperty("LoginUser"),
			"j_password", Configuration.getProperty("LoginPassword")
		     ).		    
			header("Referer", referer).
			//expect().
			//statusCode(200). //TODO:302
			//body(hasXPath("self::loginservice/resultcode[text()='success']")).
			when().relaxedHTTPSValidation().
			post(loginURL);
		
		int statusCode = response.getStatusCode() ;
		if(statusCode == 200 || statusCode == 302){
			RestAPIBaseTest.getCookies().putAll(response.cookies());			
		    logger.info("Login Success!"+ response.asString());
		}else{
			logger.info("Login Failed!"+ response.body().asString());
			Assert.fail("Login Failed!"+ response.body().asString());
		}
		
		//get the SOrigin_HEADER_KEY
		String dsmConfig = DSMURLUtils.URL_PREFIX + Configuration.getProperty("dsmconfig");
		response = RestAssured.with().relaxedHTTPSValidation().cookies(cookies).post(dsmConfig);
		JSONObject config = JSONObject.fromObject(response.body().asString()); 
		
		try {
			sorigin = HMACSignatureHelper.calculateRFC2104HMAC( SOrigin_HEADER_KEY + "/+*+" , config.get("sessionID").toString());
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
		
	public static Response post(String path,String jsonData, int expectedStatusCode){
	
		logger.info("Post path:" + path + ", Data:"+jsonData);
		Response responseData = null;
		
		try {
			if(!loggedIn){
				loginDSM();
				loggedIn=true;
			}
			responseData = RestAssured.with().relaxedHTTPSValidation().cookies(cookies).content(jsonData).
					header(SOrigin_HEADER_KEY, sorigin).
					then().expect().statusCode(expectedStatusCode).when().post(path);

		}catch(Exception e){
				e.printStackTrace();
			}
		
		return responseData;
	}
	
	public static Response post(String path,Map<String, String> parameters, int expectedStatusCode){
		logger.info("Post path:"+ path + ", Data:"+parameters.toString());
		Response responseData = null;
		logger.info("loggedIn: "+ loggedIn);
		
		try {
			if(!loggedIn){
				loginDSM();
				loggedIn=true;
			}
			responseData = RestAssured.with().relaxedHTTPSValidation().cookies(cookies).given().parameters(parameters).
					header(SOrigin_HEADER_KEY, sorigin).
					then().expect().statusCode(expectedStatusCode).when().post(path);
			}catch(Exception e){
				logger.error("Post responsed with errors, and the path is: "+ path + " ,the parameters are: "+parameters.toString()+" the message is: "  + e.getMessage());
				e.printStackTrace();
			}
		//System.out.println("The responseData: \r\n"+ responseData.asString().trim());
		return responseData;
	}
	/**
	 * Post without expectedStatusCode
	 * @param path
	 * @param parameters
	 * @return
	 */
	public static Response post(String path,Map<String, String> parameters){
		logger.info("Post path:"+ path + ", Data:"+parameters.toString());
		Response responseData = null;
		logger.info("loggedIn: "+ loggedIn);
		
		try {
			if(!loggedIn){
				loginDSM();
				loggedIn=true;
			}
			responseData = RestAssured.with().relaxedHTTPSValidation().cookies(cookies).given().
					header(SOrigin_HEADER_KEY, sorigin).
					parameters(parameters).post(path);
			}catch(Exception e){
				logger.error("Post responsed with errors, and the path is: "+ path + " ,the parameters are: "+parameters.toString()+" the message is: "  + e.getMessage());
				e.printStackTrace();
			}
		//System.out.println("The responseData: \r\n"+ responseData.asString().trim());
		return responseData;
	}
	
	public static Response get(String path, Map<String, String> parameters, int expectedStatusCode) {
		logger.info("Get path:"+ path + ", Data:" + parameters.toString());
		Response responseData = null;
		logger.info("loggedIn: " + loggedIn);

		try {
			if (!loggedIn) {
				loginDSM();
				loggedIn = true;
			}

			responseData = RestAssured.with().relaxedHTTPSValidation().cookies(cookies).given().parameters(parameters).
					header(SOrigin_HEADER_KEY, sorigin).
					then().expect().statusCode(expectedStatusCode).when().get(path);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseData;
	}
	
	//get method for DSM alerts tests
	public static Response get(String path, String ssoCookies, int expectedStatusCode) {
		logger.info("Get path:"+ path);
		Response responseData = null;
		logger.info("loggedIn: " + loggedIn);

		try {
			if (!loggedIn) {
				loginDSM();
				loggedIn = true;
			}

			responseData = RestAssured.with().relaxedHTTPSValidation().cookies(cookies).cookies("SSOCookie",ssoCookies).
					header(SOrigin_HEADER_KEY, sorigin).
					given().then().expect().statusCode(expectedStatusCode).when().get(path);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseData;
	}
	
	
	public static Response post(String path, Map<String, String> header, byte[] data, int expectedStatusCode){
		
		logger.info("Post path:"+ path + ", header:"+header.toString());
		Response responseData = null;
		
		try {
			if(!loggedIn){
				loginDSM();
				loggedIn=true;
			}
			
			responseData = RestAssured.with().relaxedHTTPSValidation().cookies(cookies).given().headers(header).
					header(SOrigin_HEADER_KEY, sorigin).
					content(data).then().expect().statusCode(expectedStatusCode).when().post(path);
							
		}catch(Exception e){
				e.printStackTrace();
			}
		
		return responseData;
	}
	
	public static Response postForAlerts(String path, Map<String, String> parameters,Map<String, String> header, int expectedStatusCode){
		
		logger.info("Post path:"+ path + ", header:"+header.toString());
		Response responseData = null;
		
		try {
			if(!loggedIn){
				loginDSM();
				loggedIn=true;
			}
			
			responseData = RestAssured.with().relaxedHTTPSValidation().cookies(cookies).given().headers(header).
					header(SOrigin_HEADER_KEY, sorigin).
					parameters(parameters).then().expect().statusCode(expectedStatusCode).when().post(path);
		}catch(Exception e){
				e.printStackTrace();
			}finally {
				if(responseData == null){
					responseData = RestAssured.with().relaxedHTTPSValidation().cookies(cookies).given().headers(header).parameters(parameters).then().expect().statusCode(500).when().post(path);
					System.out.println("Alerts Token response code : "+responseData.getStatusCode());	
					System.out.println("Alerts Token response body : "+responseData.getBody().asString());
				}
			}
		
		return responseData;
	}
	
public String uploadFile(String fileName) {
		
		final String newLine = "\r\n";
		final String boundaryPrefix = "--";
		
		String HEADERBOUNDARY = "----WebKitFormBoundaryGsokj37Tl567UVfe";
		String BOUNDARY = boundaryPrefix + HEADERBOUNDARY;  //"------WebKitFormBoundaryGsokj37Tl567UVfe"; 		
		String DISPOSITION = "Content-Disposition: form-data; name=\"format\"";
		String FORMAT = "QryStringInHTML";
		 
		StringBuilder sb = new StringBuilder(); 
		 
		sb.append(boundaryPrefix);
		sb.append(BOUNDARY);
		sb.append(newLine);		
		 
		sb.append("Content-Disposition: form-data; name=\"uploadedfile\"; filename=\"" + fileName + "\"" + newLine);
		sb.append("Content-Type: application/octet-stream");
		 
		sb.append(newLine);
		sb.append(newLine);
		 
		byte[] start_data = sb.toString().getBytes();				
		
		FileInputStream sqlFileStream = null;
		byte[] file_data = null;
		
		try {
			
			sqlFileStream = new FileInputStream(new File(Configuration.getuploadFilePath() + fileName));
			file_data = new byte[sqlFileStream.available()];
			sqlFileStream.read(file_data);
			sqlFileStream.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		  
		  
	    byte[] end_data = (newLine + BOUNDARY + newLine + 
	    		  DISPOSITION + newLine + newLine
				  +FORMAT +newLine +
				  BOUNDARY + boundaryPrefix + newLine).getBytes();			 
		
	   Map<String,String> headers = new HashMap<String,String>();
	   headers.put("Content-Type", "multipart/form-data; boundary="+HEADERBOUNDARY);
	
	   String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("tunningUploadSQLFile");

	   Response response = post(path, headers,  ByteUtil.Merge(start_data, file_data, end_data), 200);	
		
	   String htmlString = response.body().asString();
	   //<HTML><body><div id='result'>returnCode=success&amp;message=/data/DSMbuild/ibm-datasrvrmgr/work/upload_ejKHVJ3vXc1470193759330.sql</div></body></HTML>
	   
	   String start="message=";
	   String end="</div>";   
	   
	   return htmlString.substring(htmlString.indexOf(start)+start.length(), htmlString.indexOf(end));	   
		
	}
}
