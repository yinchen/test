package com.ibm.datatools.ots.tests.restapi.common;


import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

public class SendEmailTestBase {
	
	protected String mailSender = null;
	protected String mailRecipients = null;
	protected String mailSubject = null;
	protected String alertIDs = null;
	protected String cmd = null;

	
	protected TuningCommandService cs = null;
	protected Properties p;
	protected final Logger logger = LogManager.getLogger(getClass());
	
	@BeforeClass()
	@Parameters({"mailSender","mailRecipients","mailSubject", "alertIDs", "cmd"})
	public void beforeTest(String mailSender,String mailRecipients,String mailSubject, String alertIDs, String cmd) throws Exception {
		this.mailSender = mailSender;
		this.mailRecipients = mailRecipients;
		this.mailSubject = mailSubject;
		this.alertIDs = alertIDs;
		this.cmd = cmd;
		
		cs = new TuningCommandService();
		RestAPIBaseTest.loginDSM();
	}
}
