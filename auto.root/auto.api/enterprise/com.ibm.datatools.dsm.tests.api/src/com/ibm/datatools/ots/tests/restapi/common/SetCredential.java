package com.ibm.datatools.ots.tests.restapi.common;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckCredentialForTuning;


public class SetCredential {
	
	private String dbProfileName = null;
	private String username = null;
	private String password = null;
	
	protected TuningCommandService cs = null;
	
	@Test
	@Parameters({"dbProfile","username","password"})
	public void checkAndSetCredential(String dbProfileName,String username,String password) throws FileNotFoundException, IOException{
		this.dbProfileName = dbProfileName;
		this.username = username;
		this.password = password;			
		
		DatabaseService databaseService = new DatabaseService();
		RestAPIBaseTest.loginDSM();
		
		boolean connExists =databaseService.checkConnectionExists(this.dbProfileName);
		if(connExists){
			CheckCredentialForTuning.checkCredential(this.dbProfileName, this.username, this.password);
		}
		
		
	}
	
}



