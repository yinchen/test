package com.ibm.datatools.ots.tests.restapi.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.test.utils.JSONUtils;
import com.jayway.restassured.response.Response;

import net.sf.json.JSONObject;

public class TagManagedService extends RestAPIBaseTest{
	public static String DB_FILE_INFO_LUW1054 = Configuration.getDatabaseDirPath()+"db2v1054_luw_1.properties" ;
	
	
	private static final Logger logger = LogManager.getLogger(ConnectionTestService.class);
	public static final String PROP_GET_CONNECTION = "getTagManagedTest.properties" ;
	public Properties jsonProperty = null  ;
	public String repositoryMgrPath;
	public String tagPath = DSMURLUtils.URL_PREFIX + "/tag/tag.do";
	private String creator = Configuration.getProperty("LoginUser");
	private JSONObject oriRepoDBInfo = new JSONObject();

	public TagManagedService() {

		Vector<String> postdata = new Vector<String>();
		logger.info("Running TagManagedService - before LoadProperties");
		LoadProperties(postdata) ;
		logger.info("Running TagManagedService - after LoadProperties");
		jsonProperty=new Properties();
		
		for(int i=0;i<postdata.size();i++){
			jsonProperty.putAll(loadJSONPostData(postdata.get(i)));
		}
		logger.info("Running TagManagedService - end");
	}

	
	/**
	 * Load the properties
	 * */	
	protected void LoadProperties(Vector<String> postdata){
		postdata.add(PROP_GET_CONNECTION) ;
	}
	
	public Properties loadJSONPostData(String filename){
		Properties p = new Properties();
		String filePath = Configuration.getPostDataPath()+"connectionsData/"+filename;
		try{
			p.load(new FileInputStream(filePath));
		}catch(FileNotFoundException e){
			e.printStackTrace();
			throw new IllegalArgumentException("Error when loading file in method loadJSONPostData(), file name is: "+filePath);
		}catch(IOException e){
			e.printStackTrace();
			throw new IllegalArgumentException("Error when loading file in method loadJSONPostData(), file name is: "+filePath);
		}

		return p;
	}
	
	
	public String getJSONData(String key){
		String value = jsonProperty.getProperty(key);
		if(value.endsWith(".json")){
			String filePath = Configuration.getPostDataPath()+"jsonData/tagManagedTest/"+value.trim();
			value = FileTools.readFileAsString(filePath) ;
		}
		return value ;
	}
	
	protected JSONObject newResult(Response response, String path, Map<String, String> postData) {
		JSONObject result = new JSONObject();
		result.accumulate( "URL", path );
		result.accumulate( "PostData", postData );
		result.accumulate( "ResponseCode", response.getStatusCode() );
		result.accumulate( "ResponseData", response.body().asString() );
		return result;
	}

	/////////////////////////////// APIs from TagRepositoryHTTPService////////////////////////////
	/**
	 * Add/Delete/Update tags
	 */
	public JSONObject batchUpdateTags(String jsonString) {
		Map<String, String> postData = new HashMap<String, String>();
		postData.put("changedTagArray", jsonString);
		postData.put("cmd", "batchUpdateTags");
		postData.put("format", "json");

		return sendMessageAndGetResponseWithPost(tagPath, postData);
	}
	
	public JSONObject batchUpdateTagsForDeleteForce(String jsonString) {
		Map<String, String> postData = new HashMap<String, String>();
		postData.put("changedTagArray", jsonString);
		postData.put("cmd", "batchUpdateTags");
		postData.put("format", "json");
		postData.put("force", "true");

		return sendMessageAndGetResponseWithPost(tagPath, postData);
	}

	/**
	 * List tags in Managed tag page
	 */
	public JSONObject listManagedTags(String jsonString) {
		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "listManagedTags");
		postData.put("format", "json");

		return sendMessageAndGetResponseWithPost(tagPath, postData);
	}
	
	private JSONObject sendMessageAndGetResponseWithPost(String path, Map<String, String> postData) {
		Response response = post(path, postData, 200);

		int resCode = response.getStatusCode();
		String resData = response.body().asString().trim();

		JSONObject result = new JSONObject();
		result.accumulate("ResponseCode", resCode);
		result.accumulate("ResponseData", resData);
		return result;
	}
	
	public String getResponseResultCode(JSONObject result){
		Assert.assertNotNull(result);
		String resData = result.getString("ResponseData") ;
		Map<String, String> resDataMap = JSONUtils.parseJSON2MapString(resData) ;
		return resDataMap.get("resultCode");
	}
	
	/**
	 * Detail message if request failed.
	 * */
	public String getResponseResultMessage(JSONObject result){
		Assert.assertNotNull(result);
		String resData = result.getString("ResponseData") ;
		Map<String, String> resDataMap = JSONUtils.parseJSON2MapString(resData) ;
		return resDataMap.get("message") ;
	}
}
