package com.ibm.datatools.ots.tests.restapi.common;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import javax.xml.parsers.ParserConfigurationException;

import org.testng.Assert;
import org.xml.sax.SAXException;

import net.minidev.json.parser.JSONParser;
import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.jayway.restassured.response.Headers;
import com.jayway.restassured.response.Response;

/*
 * The purpose of this class is:
 * 1)inherit from base class RestAPIBaseTest(post login service by default/post test HTTP request)
 * 2)Load JSON post data from file \testData\postData\TuningActivity\xxx.properties, which is passed in from constructor
 * 3)Post HTTP request to call my test HTTP service: queryTunerCommand URL: /console/querytuner/QueryTunerCommand.form
 */
public class TuningCommandService extends RestAPIBaseTest {

	private Properties p;
	private String urlKey;
	
	public TuningCommandService(){
		
	}

	/*public TuningCommandService() throws FileNotFoundException, IOException {
		//this("getAdvisorInfo.properties");
		this("TestQMIntegrationTuneSQL.properties");
	}*/
	

	public TuningCommandService(String file) throws FileNotFoundException,
			IOException {
		this(file, null);
	}

	public TuningCommandService(String file, String urlKey)
			throws FileNotFoundException, IOException {
		Vector<String> postdata = new Vector<String>();
		postdata.add(file);
		p = new Properties();
		for (int i = 0; i < postdata.size(); i++) {
			p.putAll(loadJSONPostData(postdata.get(i)));
		}
		this.urlKey = urlKey;
	}

	public Properties loadJSONPostData(String filename)
			throws FileNotFoundException, IOException {
		String filePath = Configuration.getPostDataPath() + "TuningActivity/"
				+ filename;
		Properties p = new Properties();
		p.load(new FileInputStream(filePath));
		return p;
	}

	public JSONObject callTuningCommandServiceByJson(String inputJson) {
		String path = urlKey == null ? Configuration
				.getProperty("queryTunerCommand") : Configuration
				.getProperty(urlKey);
		Response response = post(path, inputJson, 200);

		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("Input", inputJson);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		return result;
	}

	public JSONObject callTuningCommandServiceByFormData(String input) {
		JSONObject inputParas = JSONObject.fromObject(input);
		Map<String, String> parameters = new HashMap<String, String>();
		for (Object key : inputParas.keySet()) {
			parameters.put(String.valueOf(key),
					String.valueOf(inputParas.get(key)));
		}
		String path = urlKey == null ? Configuration
				.getProperty("queryTunerCommand") : Configuration
				.getProperty(urlKey);
		Response response = post(path, parameters, 200);

		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("Input", input);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		return result;
	}
	
	public String getSQLIDByDBProfile(String dbProfile){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("dbmgr");
		Map<String,String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "getProfileInJson");
		parameters.put("format", "json");
		parameters.put("profileJSON", dbProfile);
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		
		if(!result.getString("resultCode").equals("success")){
			String message = "Verify connection "+dbProfile+" error";
			Assert.fail(message);
		}
		
		String sqlID = null;
		if(result.containsKey("response") && result.getString("response") != null){
			String dbDetails = result.getString("response");
			sqlID = JSONObject.fromObject(dbDetails).getString("user");
		}
		
		return sqlID;
		
	}
	
	public String getDBTypeByDBProfile(String dbProfile){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("dbmgr");
		Map<String,String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "getProfileInJson");
		parameters.put("format", "json");
		parameters.put("profileJSON", dbProfile);
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		
		if(!result.getString("resultCode").equals("success")){
			String message = "Verify connection "+dbProfile+" error";
			Assert.fail(message);
		}
		
		String dbType = null;
		if(result.containsKey("response") && result.getString("response") != null){
			String dbDetails = result.getString("response");
			dbType = JSONObject.fromObject(dbDetails).getString("dataServerType");
		}
		
		return dbType;
		
	}
	
	public JSONObject listAllMonitorDB(){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("dbmgr");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("format", "json");
		parameters.put("includeTAGS", "true");
		parameters.put("matchPrivilege", "DSWEB.IS_DB_USER");
		parameters.put("cmd", "getAllProfiles");
		parameters.put("userid", "admin");
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode",response.getStatusCode());
		return result;
	}
	
	
	
	/**
	 *  Configure db
	 * @param dbName
	 * @return
	 */
	public JSONObject configureTableSpace(String dbName){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerJob");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "retrieveTablespaces");
		parameters.put("monDbConProfile", dbName);
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode",response.getStatusCode());
		return result;
	}
	
	/**
	 * Configure db
	 * @param dbType
	 * @param dbName
	 * @return
	 */
	public JSONObject getConfigStatus(String dbType,String dbName){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerCommand");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "getConfigStatus");
		parameters.put("dataServerType", dbType);
		parameters.put("dbProfileName", dbName);
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode",response.getStatusCode());
		return result;
	}
	
	/**
	 * Configure db
	 * @param dbName
	 * @return
	 */
	public JSONObject configDB(String dbName){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("dbconfig");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("monDbConProfile", dbName);
		parameters.put("actFormData", "{}");
		parameters.put("isOTSLite", "false");
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode",response.getStatusCode());
		return result;
	}
	
	
	/**
	 * Check monitor db connection
	 * @return
	 */
	public String checkMonitorDBConnection(String profileJSON){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("dbmgr");
		Map<String,String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "testProfileConn");
		parameters.put("format", "json");
		parameters.put("profileJSON", profileJSON);
		parameters.put("isMonCredential", "false");
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		String checkMonitorDBResult = result.getString("resultCode");
		return checkMonitorDBResult;
	}
	
	public JSONObject migrateFromQT(String filePath,String dbName,String currentTime,String dbType){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerCommand");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "MigrateFromQT");
		parameters.put("filePath", filePath);
		parameters.put("monDbConProfile", dbName);
		parameters.put("currentTimeStamp", currentTime);
		parameters.put("monDatabaseType", dbType);
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode",response.getStatusCode());
		return result;
	}
	
	public JSONObject getWIAWhatIfJob() {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("QTJobListGridProvider");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "getAllRows");
		parameters.put("providerType", "_BASEDATAPROVIDER_");
		parameters.put("providerTypeID", "default");
		parameters.put("providerInstID", "QTJOBLIST_1oqwt_jobmanager_widget_QTJobListPage_0");
		parameters.put("dataRefresh", "true");
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		JSONArray array = result.getJSONArray("items");
		JSONObject job = null;
		for(int i=0; i<array.size(); i++){
			job = array.getJSONObject(i);
			if(job.containsKey("TUNINGTYPE") && job.getString("TUNINGTYPE").equals("Workload What-If Analysis")
					&&job.containsKey("STATUS")&& job.getString("STATUS").equals("Succeeded")
					&&job.containsKey("PROGRESS")&&job.getString("PROGRESS").equals("Completed")){
				return job;
			}
		}
		return null;
	}
	
	
	
	/**
	 * 
	 * @param dbName
	 * @param workloadid
	 * @param schema
	 * @return
	 */
	public JSONObject getCandidateTableOrganizationList(String dbName,String workloadid,String schema){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("CandidateTableOrganizationList");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("monDbConProfile", dbName);
		parameters.put("workloadid", workloadid);
		parameters.put("schema", "");
		parameters.put("providerType", "_BASEDATAPROVIDER_");
		parameters.put("providerTypeID", "default");
		parameters.put("providerInstID", "default");
		parameters.put("cmd", "getAllRows");
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode",response.getStatusCode());
		return result;
	}
	
	
	
	
	/**
	 * click "Test candidate table organization" in Table organization script page 
	 * of LUW workload job with Table Organization recommendations
	 * 
	 * 
	 * @param dbName					DB2LUW
	 * @param stagingTableSchema		
	 * @param stagingTableWorkloadId
	 * @return
	 */
	public String listSQLStatementOfStagingTable(String dbName,String stagingTableSchema,String stagingTableWorkloadId){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("StatementList");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("workloadid", "");
		parameters.put("monDbConProfile", dbName);
		parameters.put("stagingTableSchema", stagingTableSchema);
		parameters.put("stagingTableWorkloadId", stagingTableWorkloadId);
		parameters.put("captureMaxRowsCount", "100");
		parameters.put("providerType", "_BASEDATAPROVIDER_");
		parameters.put("providerTypeID", "default");
		parameters.put("providerInstID", "QTWorkloadStatementList");
		parameters.put("cmd", "getAllRows");
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		
		String resultCode = result.getString("resultCode");
		String sql = null;
		if("FAILURE".equals(resultCode)){
			System.out.println("Error:No sql statement found");
			Assert.fail("Error:No sql statement found");
		}else{
			JSONArray array = result.getJSONArray("items");
			for(int i=0; i<array.size(); i++){
				JSONObject items = array.getJSONObject(i);
				sql = items.getString("FullStatementText");
			}
		}
		
		return sql;
	}
	
	
	/**
	 * select SQL statement source  FROM  Catalog Plan or Package ->
	 * select capture statements that match criteria for plan
	 * @param dbName
	 * @param sqlCount
	 * @return
	 */
	public JSONObject listCacheStatementForCatalogPlan(String dbName,String sqlCount){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("StatementList");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("workloadid", "");
		parameters.put("monDbConProfile", dbName);
		parameters.put("stagingTableSchema", "");
		parameters.put("stagingTableWorkloadId", "");
		parameters.put("captureMaxRowsCount", sqlCount);
		parameters.put("type", "CATALOG");
		parameters.put("catalogType", "CATALOG.PLAN");
		parameters.put("providerType", "_BASEDATAPROVIDER_");
		parameters.put("providerTypeID", "default");
		parameters.put("providerInstID", "QTWorkloadStatementListCATALOG5PLAN");
		parameters.put("cmd", "getAllRows");
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	/**
	 * select SQL statement source  FROM  Catalog Plan or Package ->
	 * select capture statements that match criteria for packages
	 * @param dbName
	 * @param sqlCount
	 * @return
	 */
	public JSONObject listCacheStatementForCatalogPackage(String dbName,String sqlCount){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("StatementList");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("workloadid", "");
		parameters.put("monDbConProfile", dbName);
		parameters.put("stagingTableSchema", "");
		parameters.put("stagingTableWorkloadId", "");
		parameters.put("captureMaxRowsCount", sqlCount);
		parameters.put("type", "CATALOG");
		parameters.put("catalogType", "CATALOG.PACKAGE");
		parameters.put("providerType", "_BASEDATAPROVIDER_");
		parameters.put("providerTypeID", "default");
		parameters.put("providerInstID", "QTWorkloadStatementListCATALOG5PACKAGE");
		parameters.put("cmd", "getAllRows");
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	public JSONObject listCacheStatementForCatalogPackageWithFilters(String dbName,String sqlCount){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("StatementList");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("monDbConProfile", dbName);
		parameters.put("captureMaxRowsCount", sqlCount);
		parameters.put("type", "CATALOG");
		parameters.put("catalogType", "CATALOG.PACKAGE");
		parameters.put("FILTER_KEYS", "NAME@@=@@AOC5OPKN#@#EXPLAIN@@=@@N");
		parameters.put("cmd", "getAllRows");
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	public JSONObject listCacheStatementForUserDefinedRepository(String dbName,String sqlCount,
			String userDefinedRepoQualifier,String userDefinedRepoName){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("StatementList");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("workloadid", "");
		parameters.put("monDbConProfile", dbName);
		parameters.put("stagingTableSchema", "");
		parameters.put("stagingTableWorkloadId", "");
		parameters.put("userDefinedRepoQualifier", userDefinedRepoQualifier);
		parameters.put("userDefinedRepoName", userDefinedRepoName);
		parameters.put("captureMaxRowsCount", sqlCount);
		parameters.put("providerType", "_BASEDATAPROVIDER_");
		parameters.put("providerTypeID", "default");
		parameters.put("providerInstID", "QTWorkloadStatementListUSER_DEFINED_REPO");
		parameters.put("cmd", "getAllRows");
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	
	
	public String listCacheStatement(String dbName,String sqlCount,String sqlid){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("StatementList");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("workloadid", "");
		parameters.put("monDbConProfile", dbName);
		parameters.put("stagingTableSchema", "");
		parameters.put("stagingTableWorkloadId", "");
		parameters.put("captureMaxRowsCount", sqlCount);
		parameters.put("stmtCacheTableQualifier", sqlid);
		parameters.put("type", "CACHE");
		parameters.put("providerType", "_BASEDATAPROVIDER_");
		parameters.put("providerTypeID", "default");
		parameters.put("providerInstID", "QTWorkloadStatementListCACHE");
		parameters.put("cmd", "getAllRows");
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		//System.out.println(">>"+result);
		String resultCode = result.getString("resultCode");
		String sql = null;
		if("FAILURE".equals(resultCode)){
			System.out.println("Error:No cache in database");
			Assert.fail("No cache in database...");
		}else{
			JSONArray cacheArr = result.getJSONArray("items");
			for(int i=0;i<cacheArr.size();i++){
				JSONObject items = cacheArr.getJSONObject(i);
				sql = items.getString("FullStatementText");
			}
		}
		return sql;
	}
	
	public String selectSQLFromFile(String dbName,String sqlFileName,String schema){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("StatementList");
		String sql = null;
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("workloadid", "");
		parameters.put("monDbConProfile", dbName);
		parameters.put("sqlFileName", sqlFileName);
		parameters.put("stmtDelim", ";");
		parameters.put("schema", schema);
		parameters.put("captureMaxRowsCount", "100");
		parameters.put("providerType", "_BASEDATAPROVIDER_");
		parameters.put("providerTypeID", "default");
		parameters.put("providerInstID", "QTWorkloadStatementList");
		parameters.put("cmd", "getAllRows");
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		//System.out.println(">>"+result);
		JSONArray array = result.getJSONArray("items");
		for(int i=0; i < array.size(); i++){
			JSONObject items = array.getJSONObject(i);
			sql = items.getString("FullStatementText");
		}
		return sql;
	}
	
	public JSONObject getJobByJobID(String jobID) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("QTJobListGridProvider");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "getAllRows");
		parameters.put("providerType", "_BASEDATAPROVIDER_");
		parameters.put("providerTypeID", "default");
		parameters.put("providerInstID", "QTJOBLIST_1oqwt_jobmanager_widget_QTJobListPage_0");
		parameters.put("dataRefresh", "true");
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		JSONArray array = result.getJSONArray("items");
		JSONObject job = null;
		for(int i=0; i<array.size(); i++){
			job = array.getJSONObject(i);
			if(job.containsKey("JOBID") && job.getString("JOBID").equals(jobID)){
				return job;
			}
		}
		return null;
	}
	
	public JSONObject getCandidateIndex(String dbName,String queryNodeID,String queryResultID,String WTI_JOB_ID) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("CandidateIndexList");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "getAllRows");
		parameters.put("indexCmd", "init");
		parameters.put("monDbConProfile", dbName);
		parameters.put("queryNodeID", queryNodeID);
		parameters.put("queryResultID", queryResultID);
		parameters.put("WTI_JOB_ID", WTI_JOB_ID);
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	public JSONObject getSingleJobCandidateIndex(String dbName,String arId,String jobid,String schema) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerCommand");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "RetrieveIndexWhatifcandidate");
		parameters.put("monDbConProfile", dbName);
		parameters.put("arId", arId);
		parameters.put("jobid", jobid);
		parameters.put("schema", schema);
		parameters.put("ddlType", "AL");
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	public JSONObject queryJob(String dbName,String jobID,String workloadID) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerCommand");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "RetrieveIndexWhatifcandidate");
		parameters.put("monDbConProfile", dbName);
		parameters.put("jobid", jobID);
		parameters.put("workloadid", workloadID);
		parameters.put("schema", "SYSADM");
		parameters.put("ddlType", "AL");
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	public String listDatabase(){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("listDatabase");
		Response response = post(path, "", 200);
		String s = response.body().asString();
		return s;
	}
	
	/**
	 *  Add Repository DB
	 * @param newProfile
	 * @return
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 */
	public String addRepoDB(String newProfile) throws ParserConfigurationException, SAXException, IOException{
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("repMgt");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "attachRepDB");
		parameters.put("sendRepositorySelectionEvent", "true");
		parameters.put("newProfile", newProfile);
		parameters.put("showProgress", "true");
		Response response = post(path, parameters, 200);
		
		String result = response.asString().trim();
		return result;
	}
	
	/**
	 * Get db2 license
	 * Check API : GET_DB2_LICENSE
	 * API used : License auto
	 * @param sectionData
	 * @return
	 */
	public JSONObject getDB2License(String dbName) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("GET_DB2_LICENSE");
		path = path + dbName;
		String parameters = "";
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	/**
	 * Check API : CHECK_ENTERPRISE_Z_MON
	 * API used : License auto
	 * @param dbName
	 * @return
	 */
	public JSONObject CHECK_ENTERPRISE_Z_MON(String dbName) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("CHECK_ENTERPRISE_Z_MON");
		path = path + dbName;
		String parameters = "";
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	/**
	 * Check API : CHECK_ENTERPRISE_LUW
	 * API used: License aut
	 * @param monitorDBName
	 * @return
	 */
	public JSONObject CHECK_ENTERPRISE_LUW(String monitorDBName) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("CHECK_ENTERPRISE_LUW");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "CHECK_ENTERPRISE_LUW");
		parameters.put("profileName", monitorDBName);
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	/**
	 * Check API : CHECK_ENTERPRISE_Z_CON
	 * API used: License aut
	 * @param monitorDBName
	 * @return
	 */
	public JSONObject CHECK_ENTERPRISE_Z_CON(String monitorDBName) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("CHECK_ENTERPRISE_Z_CON");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "CHECK_ENTERPRISE_Z_CON");
		parameters.put("profileName", monitorDBName);
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	
	/**
	 * Get monitor db details
	 * @param sectionData
	 * @return
	 */
	public JSONObject getMonitorDBDetails(String monitorDBName) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("dbmgr");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "getProfileInJson");
		parameters.put("format", "json");
		parameters.put("profileJSON", monitorDBName);
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	/**
	 * Check API : ACTIVATE_LICENSE
	 * API used : License auto
	 * @param dbName
	 * @return
	 */
	public JSONObject ACTIVATE_LICENSE(String dbName) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("ACTIVATE_LICENSE");
		path = path + dbName;
		String parameters = "";
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	
	/**
	 *  Add monitor DB
	 * @param sectionData
	 * @return
	 */
	public JSONObject addConnection(String sectionData) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("dbmgr");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "addProfileInJson");
		parameters.put("format", "json");
		parameters.put("profileJSON", sectionData);
		parameters.put("showProgress", "true");
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	public JSONObject addConnection(Properties oqwt) {
		JSONObject saveObject = new JSONObject();
		JSONObject oqwt_SectionData = new JSONObject();
		for (Object key : oqwt.keySet()) {
			oqwt_SectionData.put(String.valueOf(key), oqwt.get(key));
		}
		saveObject.putAll(oqwt_SectionData);

		return addConnection(saveObject.toString());
	}
	
	
	public JSONObject updateConnection(String sectionData) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("dbmgr");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "updProfileInJson");
		parameters.put("format", "json");
		parameters.put("profileJSON", sectionData);
		parameters.put("showProgress", "true");
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	public JSONObject updateConnection(Properties oqwt){
		JSONObject saveObject = new JSONObject();
		JSONObject oqwt_SectionData = new JSONObject();
		for (Object key : oqwt.keySet()) {
			oqwt_SectionData.put(String.valueOf(key), oqwt.get(key));
		}
		saveObject.putAll(oqwt_SectionData);

		return updateConnection(saveObject.toString());
	}

	public JSONObject submitJob(String sectionData) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("jobDetail");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "createAndSaveJob");
		parameters.put("jobComponent", "OQWTComp");
		parameters.put("saveObject", sectionData);
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}

	public JSONObject submitJob(Properties desc, Properties oqwt) {
		JSONObject saveObject = new JSONObject();
		
		JSONArray sectionsArray = new JSONArray();
		JSONObject desc_section = new JSONObject();
		
		desc_section.put("sectionID", "DESC_SECTION");
		JSONObject desc_SectionData = new JSONObject();
		
		for (Object key : desc.keySet()) {
			desc_SectionData.put(String.valueOf(key), desc.get(key));
		}
		desc_section.put("sectionData", desc_SectionData);

		JSONObject oqwt_section = new JSONObject();
		oqwt_section.put("sectionID", "OQWT_SECTION");
		JSONObject oqwt_SectionData = new JSONObject();
		for (Object key : oqwt.keySet()) {
			oqwt_SectionData.put(String.valueOf(key), oqwt.get(key));
		}
		oqwt_section.put("sectionData", oqwt_SectionData);

		sectionsArray.add(0, desc_section);
		sectionsArray.add(1, oqwt_section);

		saveObject.put("JOBID", "0");
		saveObject.put("sections", sectionsArray);

		return submitJob(saveObject.toString());
	}

	public JSONObject runJob(String jobID, String databaseList) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("jobDetail");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "runJobNow");
		parameters.put("dbNameList", databaseList);
		parameters.put("jobID", jobID);
		parameters.put("passwordVal", "");
		parameters.put("userNameVal", "_default");

		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}

	public JSONObject deleteJob(String instID) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("jobDetail");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "deleteExecHistoryAndJob");
		parameters.put("INSTID", instID);

		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	public JSONObject deleteJobs(List<String> instID) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("jobDetail");
		String jobInstIDList = instID.toString().substring(1, instID.toString().length()-1);
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "deleteExecHistoryAndJob");
		parameters.put("INSTID", jobInstIDList.replace(" ", ""));

		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	public JSONObject retuneSingleJob(String dbName,String jobID,String instID,String stmtId){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerCommand");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "retrieveRetuneInfo");
		parameters.put("jobid", jobID);
		parameters.put("jobInstanceId", instID);
		parameters.put("stmtId", stmtId);
		parameters.put("monDbConProfile", dbName);
		
		Response response = post(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	public JSONObject retuneWorkloadJob(String dbName,String jobID,String instID,String stmtId){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerCommand");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "retrieveRetuneInfo");
		parameters.put("jobid", jobID);
		parameters.put("jobInstanceId", instID);
		parameters.put("stmtId", stmtId);
		parameters.put("monDbConProfile", dbName);
		
		Response response = post(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	public JSONObject getSingleJobDetails(String dbName,String jobID,String jobName,String instID,String arId){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerCommand");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "getQuerySummaryReport");
		parameters.put("monDbConProfile", dbName);
		parameters.put("jobid", jobID);
		parameters.put("jobname", jobName);
		parameters.put("jobInstanceId", instID);
		parameters.put("arId", arId);
		parameters.put("tuningCtx", "STANDALONE");
		
		Response response = post(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	
	public JSONObject getWorkloadJobDetails(String dbName,String jobID,String jobName,String instID){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerCommand");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "getWorkloadSummaryReport");
		parameters.put("monDbConProfile", dbName);
		parameters.put("jobid", jobID);
		parameters.put("workloadid", jobName);
		parameters.put("jobInstanceId", instID);
		parameters.put("tuningCtx", "STANDALONE");
		
		Response response = post(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	
	public JSONObject getWorkloadJobWithSameDB(String dbName){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerJobHistory");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "getAllRows");
		parameters.put("providerInstID", "jobMgrComp_historyListGrid");
		parameters.put("providerType", "_BASEDATAPROVIDER_");
		parameters.put("providerTypeID", "default");
		
		Response response = post(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		JSONArray array = result.getJSONArray("items");
		JSONObject job = null;
		String tuningType = "Workload";
		String status = "Succeeded";
		for(int i=0; i<array.size(); i++){
			job = array.getJSONObject(i);
			if(job.containsKey("TUNINGTYPE")&&job.getString("TUNINGTYPE").equals(tuningType)
			&& job.containsKey("STATUS")&&job.getString("STATUS").equals(status)
			&& job.containsKey("DATABASENAME")&& job.getString("DATABASENAME").equals(dbName)){
				return job;
			}
		}
		return null;
	}
	
	public JSONObject getSingleJobWithSameDB(String dbName){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerJobHistory");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "getAllRows");
		parameters.put("providerInstID", "jobMgrComp_historyListGrid");
		parameters.put("providerType", "_BASEDATAPROVIDER_");
		parameters.put("providerTypeID", "default");
		
		Response response = post(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		JSONArray array = result.getJSONArray("items");
		JSONObject job = null;
		String tuningType = "Single-query";
		String status = "Succeeded";
		for(int i=0; i<array.size(); i++){
			job = array.getJSONObject(i);
			if(job.containsKey("TUNINGTYPE")&&job.getString("TUNINGTYPE").equals(tuningType)
			&& job.containsKey("STATUS")&&job.getString("STATUS").equals(status)
			&& job.containsKey("DATABASENAME")&& job.getString("DATABASENAME").equals(dbName)){
				return job;
			}
		}
		return null;
	}
	
	public JSONObject getJob(String tuningType,String status) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerJobHistory");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "getAllRows");
		parameters.put("providerInstID", "jobMgrComp_historyListGrid");
		parameters.put("providerType", "_BASEDATAPROVIDER_");
		parameters.put("providerTypeID", "default");

		Response response = post(path, parameters, 200);
		String json = response.body().asString();
		//System.out.println(json);
		JSONObject result = JSONObject.fromObject(json);
		JSONArray array = result.getJSONArray("items");
		int size = array.size();
		for (int i = 0; i < size; i++) {
			JSONObject job = array.getJSONObject(i);
			if (job.containsKey("TUNINGTYPE")
					&& job.getString("TUNINGTYPE").equals(tuningType)
					&& job.containsKey("STATUS") && job.getString("STATUS").equals(status)) {
				return job;
			}
		}
		return null;
	}
	
	/**
	 *  Choose single job from job list to do capture environment
	 * @param tuningType	Single-query
	 * @param status		succeed
	 * @param dbname		all ZOS db
	 * @return
	 */
	public JSONObject getJobForSingleCapEnv (String tuningType,String status,String dbname) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerJobHistory");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "getAllRows");
		parameters.put("providerInstID", "jobMgrComp_historyListGrid");
		parameters.put("providerType", "_BASEDATAPROVIDER_");
		parameters.put("providerTypeID", "default");

		Response response = post(path, parameters, 200);
		String json = response.body().asString();
		//System.out.println(json);
		JSONObject result = JSONObject.fromObject(json);
		JSONArray array = result.getJSONArray("items");
		int size = array.size();
		for (int i = 0; i < size; i++) {
			JSONObject job = array.getJSONObject(i);
			if (job.containsKey("TUNINGTYPE")
					&& job.getString("TUNINGTYPE").equals(tuningType)
					&& job.containsKey("STATUS") && job.getString("STATUS").equals(status)
					&& job.containsKey("DATABASENAME") && job.getString("DATABASENAME").equals(dbname)) {
				return job;
			}
		}
		return null;
	}

	
	public JSONObject getJobStatus(String jobID) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerJobHistory");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "getAllRows");
		parameters.put("providerInstID", "QTJOBLIST_1oqwt_jobmanager_widget_QTJobListPage_0");
		parameters.put("providerType", "_BASEDATAPROVIDER_");
		parameters.put("providerTypeID", "default");
		parameters.put("dataRefresh", "true");

		Response response = post(path, parameters, 200);
		String json = response.body().asString();
		//System.out.println(json);
		JSONObject result = JSONObject.fromObject(json);
		result.accumulate("ResponseCode", response.getStatusCode());
		JSONArray array = null;
		try {
			array = result.getJSONArray("items");
		} catch (Exception e) {
			System.out.println("Job history is >> " + result);
		}
		int size = array.size();
		for (int i = 0; i < size; i++) {
			JSONObject job = array.getJSONObject(i);
			if (job.containsKey("JOBID")
					&& job.getString("JOBID").equals(jobID)) {
				return job;
			}
		}
		return null;
	}
	
	
	public JSONObject refineWorkloadJob(String dbName,String jobID){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerCommand");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "retrieveRefinedWorkload");
		parameters.put("monDbConProfile", dbName);
		parameters.put("jobid", jobID);
		parameters.put("conditions", "");
		
		Response response = post(path, parameters, 200);
		String json = response.body().asString();
		JSONObject result = JSONObject.fromObject(json);
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	
	
	public String handshake() {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("handshake");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("APIidentifier", "2");
		parameters.put("DDLversion", "2");
		parameters.put("APIversion", "2");
		parameters.put("providerID", "CQM");
		parameters.put("providerVersion", "4.1");
		Response response = post(path, parameters, 200);
		String s = response.body().asString();
		return s;
	}
	
	public JSONObject tuneSQL(String dbType,String dbName,String url,String user,String password,String sqlStatement,String schema) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("tunesql");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("monitorDBType", dbType);
		parameters.put("monitorDBURL", url);
		parameters.put("APIidentifier", "APIidentifier");
		parameters.put("DDLversion", "DDLversion");
		parameters.put("APIversion", "APIversion");
		parameters.put("providerID", "CQM");
		parameters.put("providerVersion", "providerVersion");
		parameters.put("monitorDBConnectionProfileName", dbName);
		parameters.put("monitorDBUserName", user);
		parameters.put("monitorDBPassword", password);
		parameters.put("sqlStatement", sqlStatement);
		parameters.put("sqlSchema", schema);

		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	public JSONObject tuneworkload(String dbType,String url,String dbName,String user,String password,String schema) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("tuneworkload");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("APIidentifier", "QueryTuner");
		parameters.put("DDLversion", "2");
		parameters.put("APIversion", "2");
		parameters.put("providerID", "CQM");
		parameters.put("providerVersion", "4.1");
		parameters.put("monitorDBType", dbType);
		parameters.put("monitorDBURL", url);
		parameters.put("monitorDBConnectionProfileName", dbName);
		parameters.put("monitorDBUserName", user);
		parameters.put("monitorDBPassword", password);
		parameters.put("stagingTableDBType", dbType);
		parameters.put("stagingTableDBURL", url);
		parameters.put("stagingTableDBConnectionProfileName", dbName);
		parameters.put("stagingTableDBUserName", user);
		parameters.put("stagingTableDBPassword", password);
		parameters.put("stagingTableSchema", schema);
		parameters.put("workloadId", "1");
		parameters.put("workloadNamePrefix", "WL");
		parameters.put("deleteStagingDataOnSuccessfulCapture", "NO");

		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	/**add by Ma Wei
	 * getcompareJob
	 * 
	*/
	public JSONObject getcompareJob() {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("QTJobListGridProvider");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "getAllRows");
		parameters.put("providerType", "_BASEDATAPROVIDER_");
		parameters.put("providerTypeID", "default");
		parameters.put("providerInstID", "QTJOBLIST_1oqwt_jobmanager_widget_QTJobListPage_0");
		parameters.put("dataRefresh", "true");
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		JSONArray array = result.getJSONArray("items");
		JSONObject job = null;
		for(int i=0; i<array.size(); i++){
			job = array.getJSONObject(i);
			if(job.containsKey("TUNINGTYPE") && job.getString("TUNINGTYPE").equals("Workload Access Plan Comparison")
					&&job.containsKey("STATUS")&& job.getString("STATUS").equals("Succeeded")
					&&job.containsKey("PROGRESS")&&job.getString("PROGRESS").equals("Completed")){
				return job;
			}
		}
		return null;
	}
	
	
	/**
	 * add by Ma wei
	 * getWorkloadcompareDetails
	 */
	
	public JSONObject getWorkloadcompareDetails(String dbName,String jobID,String jobName,String instID){

		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerCommand");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "retrieveWAPCInfo");
		parameters.put("monDbConProfile", dbName);
		parameters.put("jobid", jobID);
		parameters.put("workloadid", jobName);
		parameters.put("jobInstanceId", instID);
		parameters.put("tuningCtx", "STANDALONE");
		
		Response response = post(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	/**
	 * add by Ma wei
	 * getwhatifresult
	 */
	public JSONObject getwhatifresult(String dbName,String jobID,String jobName,String instID){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerCommand");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "retrieveIndexImpactAnalysisInfo");
		parameters.put("monDbConProfile", dbName);
		parameters.put("jobid", jobID);
		parameters.put("workloadid", jobName);
		parameters.put("jobInstanceId", instID);
		parameters.put("tuningCtx", "STANDALONE");
		
		Response response = post(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	/**
	 * add by Ma wei
	 * getZOSwhatifresult
	 */
	public JSONObject getZOSwhatifresult(String dbName,String jobID,String jobName,String instID){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerCommand");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "RetrieveZosWhatIfAnalysisInfo");
		parameters.put("monDbConProfile", dbName);
		parameters.put("jobid", jobID);
		parameters.put("workloadid", jobName);
		parameters.put("jobInstanceId", instID);
		parameters.put("tuningCtx", "STANDALONE");
		
		Response response = post(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}

	/**
	 * add by Jun Qing
	 * getHostVariablesGridProvider
	 */
	public JSONObject getHostVariablesGridProvider(String dbName,String instID){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("HostVariablesGridProvider");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "getAllRows");
		parameters.put("monDbConProfile", dbName);		
		parameters.put("providerInstID", "oqwt_jobmanager_widget_HostVariableDialog");
		parameters.put("providerType", "_BASEDATAPROVIDER_");
		parameters.put("providerTypeID", "default");
		parameters.put("instID", instID);
		Response response = post(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		String resData = response.body().asString().trim();
		result.accumulate("ResponseData", resData);
		return result;
	}	
	
	/**
	 * add by Jun Qing
	 * getHostVariableSetAndCount
	 */
	public JSONObject getHostVariableSetAndCount(String dbName,String instID){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerCommand");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "getHostVariableSetAndCount");
		parameters.put("monDbConProfile", dbName);		
		parameters.put("instID", instID);
		Response response = post(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		String resData = response.body().asString().trim();
		result.accumulate("ResponseData", resData);
		return result;
	}	
	
	/**
	 * add by Jun Qing
	 * getRetrieveEO
	 */
	public JSONObject getRetrieveEO(String dbName,String jobID,String workloadid,String instID, String arId){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerCommand");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "retrieveEO");
		parameters.put("monDbConProfile", dbName);		
		parameters.put("jobid", jobID);
		parameters.put("workloadid", workloadid);
		parameters.put("jobInstanceId", instID);
		parameters.put("arId", arId);
		parameters.put("tuningCtx", "STANDALONE");
		Response response = post(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		String resData = response.body().asString().trim();
		result.accumulate("ResponseData", resData);
		return result;
	}	
	
	/**
	 * add by Jun Qing
	 * RetrieveEOAPGAndCompare
	 */
	public JSONObject getRetrieveEOAPGAndCompare(String dbName,String schema, String query_no, String sql_statement, String param4SET_PLAN_HINT, String isCompare, String arId){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerCommand");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "RetrieveEOAPGAndCompare");
		parameters.put("monDbConProfile", dbName);		
		parameters.put("query_no", query_no);
		parameters.put("sql_statement", sql_statement);
		parameters.put("schema", schema);
		parameters.put("param4SET_PLAN_HINT", param4SET_PLAN_HINT);
		parameters.put("isCompare", isCompare);
		if(isCompare.equals("true")){
			parameters.put("arId", arId);
		}
		Response response = post(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		String resData = response.body().asString().trim();
		result.accumulate("ResponseData", resData);
		return result;
	}	
	
	public Properties getJSONProperties() {
		return this.p;
	}
	
	/**
	 * Add by Yin Chen
	 * @param userID
	 * @param password
	 * @return
	 */
	public JSONObject createUser(String userID,String password){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("createUser");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("userid", userID);
		parameters.put("userProfile", "{\"first_name\":\"\",\"last_name\":\"\",\"email\":\"\"}");
		parameters.put("userPrivileges", "[{\"hasPrivilege\":\"true\",\"privilege\":\"DSWEBSECURITY.CANADMINISTER\"},{\"hasPrivilege\":\"false\",\"privilege\":\"DSWEBSECURITY.CANVIEW\"}]");
		parameters.put("isAdminAction", "true");
		parameters.put("oldPassword", "");
		parameters.put("password", password);
		Response response = post(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		String resData = response.body().asString().trim();
		result.accumulate("ResponseData", resData);
		return result;
	}
	
	/**
	 * Add by Yin chen
	 * @return
	 */
	public JSONObject getUsers(){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("getUsers");
		Map<String,String> parameters = new HashMap<String,String>();
		Response response = post(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		String resData = response.body().asString().trim();
		result.accumulate("ResponseData", resData);
		return result;
	}
	
	/**
	 * Add by Yin chen
	 * @return
	 */
	public JSONObject deleteUser(String userID){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("deleteUser");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("userid", userID);
		Response response = post(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		String resData = response.body().asString().trim();
		result.accumulate("ResponseData", resData);
		return result;
	}
	
	/**
	 * Add by yin chen
	 * @param connectionFileName
	 * @return
	 */
	public JSONObject initImportDBConnections(String connectionFileName){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("importDBConnection");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "importDBConnections");
		parameters.put("args", "dbConnImportFile="+connectionFileName+",preview=true,duplicateChoice=null,checkImportFile=false");
		parameters.put("initImport", "true");
		Response response = post(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		String resData = response.body().asString().trim();
		result.accumulate("ResponseData", resData);
		return result;
	}
	
	/**
	 * Add by yin chen
	 * @param connectionFileName
	 * @return
	 */
	public JSONObject startImport(String connectionFileName){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("importDBConnection");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "importDBConnections");
		parameters.put("args", "dbConnImportFile="+connectionFileName+",preview=false,duplicateChoice=update,checkImportFile=false");
		Response response = post(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		String resData = response.body().asString().trim();
		result.accumulate("ResponseData", resData);
		return result;
	}
	
	public JSONObject parseRunSQL(String sql,String dbName){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("parse");
		Map<String, String> parameters = new HashMap<String,String>();
		parameters.put("script", sql);
		parameters.put("terminator", ";");
		parameters.put("debug", "true");
		parameters.put("type", "QT");
		parameters.put("dbProfileName", dbName);
		Response response = post(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		String resData = response.body().asString().trim();
		result.accumulate("ResponseData", resData);
		return result;
	}
	
	public JSONObject explainRunSQL(String dbName,String sql,String schame,String sqlid){
		
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("explain");
		
		String jsonData = "{profileName:"+dbName+",sql:"+sql+",schema:"+schame+",sqlid:"+sqlid+"}";
		
		Response response = post(path,jsonData, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		String resData = response.body().asString().trim();
		result.accumulate("ResponseData", resData);
		return result;
	}
	
	public List<JSONObject> getQMTuneSQLJobs(String QMJobName){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerJobHistory");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "getAllRows");
		parameters.put("providerInstID", "QTJOBLIST_1oqwt_jobmanager_widget_QTJobListPage_0");
		parameters.put("providerType", "_BASEDATAPROVIDER_");
		parameters.put("providerTypeID", "default");
		parameters.put("dataRefresh", "true");

		Response response = post(path, parameters, 200);
		String json = response.body().asString();
		JSONObject result = JSONObject.fromObject(json);
		result.accumulate("ResponseCode", response.getStatusCode());
		
		JSONArray array = null;
		try {
			array = result.getJSONArray("items");
		} catch (Exception e) {
			System.out.println("Job history is >> " + result);
		}
		
		int size = array.size();
		List<JSONObject> jobList = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			JSONObject job = array.getJSONObject(i);
			if (job.containsKey("QUERYNAME")
					&& job.getString("QUERYNAME").contains(QMJobName)) {
				System.out.println("QM Tune SQL Jobs : "+job);
				jobList.add(job);
			}
		}
		return jobList;
	}
	
	
	public List<JSONObject> getQMWorkloadJobs(String QMJobName){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerJobHistory");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "getAllRows");
		parameters.put("providerInstID", "QTJOBLIST_1oqwt_jobmanager_widget_QTJobListPage_0");
		parameters.put("providerType", "_BASEDATAPROVIDER_");
		parameters.put("providerTypeID", "default");
		parameters.put("dataRefresh", "true");

		Response response = post(path, parameters, 200);
		String json = response.body().asString();
		JSONObject result = JSONObject.fromObject(json);
		result.accumulate("ResponseCode", response.getStatusCode());
		
		JSONArray array = null;
		try {
			array = result.getJSONArray("items");
		} catch (Exception e) {
			System.out.println("Job history is >> " + result);
		}
		
		int size = array.size();
		List<JSONObject> jobList = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			JSONObject job = array.getJSONObject(i);
			if (job.containsKey("JOBNAME")
					&& job.getString("JOBNAME").contains(QMJobName)) {
				System.out.println("QM Workload Jobs : "+job);
				jobList.add(job);
			}
		}
		return jobList;
	}
	
	public JSONObject getJobByJobName(String jobName) {
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("QTJobListGridProvider");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cmd", "getAllRows");
		parameters.put("providerType", "_BASEDATAPROVIDER_");
		parameters.put("providerTypeID", "default");
		parameters.put("providerInstID", "QTJOBLIST_1oqwt_jobmanager_widget_QTJobListPage_0");
		parameters.put("dataRefresh", "true");
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		JSONArray array = result.getJSONArray("items");
		JSONObject job = null;
		for(int i=0; i<array.size(); i++){
			job = array.getJSONObject(i);
			if(job.containsKey("JOBNAME") && job.getString("JOBNAME").equals(jobName)){
				return job;
			}
		}
		return null;
	}
	
	public String setAndTestCred(String dbName,String username,String password){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("setAndTestCred");
		Map<String, String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "setAndTestCred");
		parameters.put("resourceID", dbName);
		parameters.put("namespace", "database");
		parameters.put("username", username);
		parameters.put("password", password);
		parameters.put("isTransient", "false");
		parameters.put("isOneTime", "false");
		
		Response response = post(path, parameters, 200);
		String result = response.asString().trim();
		return result;
	}
	
	public JSONObject delConnections(String dbProfileName){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("dbmgr");
		Map<String, String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "delProfileInJson");
		parameters.put("format", "json");
		parameters.put("profileJSON", dbProfileName);
		
		Response response = post(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		String resData = response.body().asString().trim();
		result.accumulate("ResponseData", resData);
		return result;
	}
	
	public JSONObject retrieveDBVersion(String dbName){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("retrieveDBVersion");
		path = path + dbName;
		Map<String, String> parameters = new HashMap<String,String>();
		parameters.put("", "");
		Response response = get(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	public String checkRepoDBExist(){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("repMgt");
		Map<String, String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "getRepDBInfo");
		Response response = post(path,parameters, 200);
		
		String result = response.asString().trim();
		return result;
	} 
	
	public String viewAlertsAll(String ssoCookies){
		String path = Configuration.getProperty("viewAlertsAll");
		Response response = get(path, ssoCookies,200);
		
		String result = response.asString().trim();
		return result;
	}
	
	public String handleSSOahth(String username,String password){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("handleSSOahth");
		path = path + "j_username="+username+"&j_password="+password+"&formAction=login&service=/alerts";
		Map<String, String> parameters = new HashMap<String,String>();
		Response response = post(path,parameters, 302);
		
		Headers header = response.getHeaders();
		return header.toString();
	}
	
	public JSONObject viewAlertsDetails(String index,String ssoCookie){
		String path = Configuration.getProperty("viewAlertsAll");
		path += index;
		Response response = get(path,ssoCookie,200);
		
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	public JSONArray viewAlertsFromDatabase(String dbName,String ssoCookie){
		String path = Configuration.getProperty("viewAlertsAll");
		path += dbName;
		Response response = get(path,ssoCookie,200);
		
		String result = response.body().asString();
		JSONArray arr = JSONArray.fromObject(result);
		return arr;
	}
	
	public String downloadDDL(){
		String path = Configuration.getProperty("downloadDDL");
		Map<String, String> parameters = new HashMap<String,String>();
		parameters.put("", "");
		Response response = get(path,parameters, 200);
		
		String result = response.asString().trim();
		return result;
	}
	
	public String getAlertsToken(String username,String password){
		String path = Configuration.getProperty("alertsToken");
		Map<String, String> parameters = new HashMap<String,String>();
		parameters.put("username", username);
		parameters.put("password", password);
		
		Map<String, String> headers = new HashMap<String,String>();
		headers.put("Content-Type", "application/x-www-form-urlencoded");
		
		Response response = postForAlerts(path,parameters, headers,200);
		//Response response = post(path,parameters,200);
		String result = response.asString().trim();
		return result;
	}
	
	public JSONObject getAllFilters(Map<String,String> parameters){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerCommand");
		parameters.put("cmd", "manageCaptureFilters");
		parameters.put("subCMD", "getAllFilters");
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode",response.getStatusCode());
		return result;
	}
	
	public JSONObject addFilter(Map<String,String> parameters){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerCommand");
		
		parameters.put("cmd", "manageCaptureFilters");
		parameters.put("subCMD", "addFilter");
		
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode",response.getStatusCode());
	
		return result;
	}
	
	public JSONObject deleteFilter(Map<String,String> parameters){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerCommand");
		
		parameters.put("cmd", "manageCaptureFilters");
		parameters.put("subCMD", "deleteFilter");
		
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode",response.getStatusCode());
	
		return result;
	}
	
	public JSONObject editFilter(Map<String,String> parameters){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerCommand");
		
		parameters.put("cmd", "manageCaptureFilters");
		parameters.put("subCMD", "editFilter");
		
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode",response.getStatusCode());
	
		return result;
	}
	
	public JSONObject callTuningServiceAPI(String url, Map<String, String> parameters) {
		Response response = post(url, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());

		return result;
	}
	
	public JSONObject callTuningServiceAPI(String url, String parameters) {
		Response response = post(url, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());

		return result;
	}
	
	public String callTuningServiceAPIbyGet(String url, Map<String, String> parameters) {
		Response response = get(url, parameters, 200);
		String result  = response.body().asString();

		return result;
	}
	
	public JSONObject getImpactAnalysisresult(String dbName,String jobID,String jobName,String instID){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerCommand");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "retrieveIndexImpactAnalysisInfo");
		parameters.put("monDbConProfile", dbName);
		parameters.put("jobid", jobID);
		parameters.put("jobname", jobName);
		parameters.put("jobInstanceId", instID);
		parameters.put("tuningCtx", "STANDALONE");
		
		Response response = post(path,parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		
		return result;
	}
	
	public JSONObject getAllConns(){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("getallconns");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("", "");
		Response response = get(path,parameters, 200);
		
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		
		return result;
	}
	
	public JSONObject listComparePackageVersions(Properties params){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("GetPackageVersions");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "getAllRows");
		parameters.put("source", "package");
		parameters.put("monDbConProfile", params.getProperty("monDbConProfile"));
		parameters.put("collId", params.getProperty("collId"));
		parameters.put("packageName", params.getProperty("packageName"));
		parameters.put("stmtNoI", params.getProperty("stmtNoI"));
		parameters.put("stmtText", params.getProperty("stmtText"));
		parameters.put("sectNoI", params.getProperty("sectNoI"));

		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	
	public JSONObject getComparePackageResult(Map params){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerCommand");
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("cmd", "comparePkgAccessPlans");
		parameters.put("captureSource", "package");
		parameters.put("monDBProfile", params.get("monDbConProfile").toString());
		parameters.put("stmt1", params.get("stmt1").toString());
		parameters.put("stmt2", params.get("stmt2").toString());
		
		Response response = post(path, parameters, 200);
		JSONObject result = JSONObject.fromObject(response.body().asString());
		result.accumulate("ResponseCode", response.getStatusCode());
		return result;
	}
	

	
}
