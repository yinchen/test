package com.ibm.datatools.ots.tests.restapi.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.FileTools;
import com.jayway.restassured.response.Response;

/*
 * The purpose of this class is:
 * 1)inherit from base class RestAPIBaseTest(post login service by default/post test HTTP request)
 * 2)Load JSON post data from file \testData\postData\TuningActivity\getAdvisorInfo.properties,such as:
 * getWiaDDL={"cmd":"getWiaDDL","jobid":"1431566449694","monDbConProfile":"CSDEC006"}
 * 
 * 3)Post HTTP request to call my test HTTP service: /console/querytuner/QueryTunerJob.form
 */
public class TuningService extends RestAPIBaseTest{

	private static final Logger logger = LogManager.getLogger(TuningService.class);
	public static final String PROP_GET_TUNING = "getTuningJson.properties" ;
	public String repositoryMgrPath;
	Properties p;
	
	public TuningService() throws FileNotFoundException, IOException{
		repositoryMgrPath = DSMURLUtils.URL_PREFIX + Configuration.getProperty("repMgt");
		Vector<String> postdata = new Vector<String>();
		postdata.add("getAdvisorInfo.properties");
		//TODO: add other properties using the same URL
		LoadProperties(postdata) ;
		p=new Properties();
		
		for(int i=0;i<postdata.size();i++){
			p.putAll(loadJSONPostData(postdata.get(i)));
		}

	}
	
	public Properties loadJSONPostData(String filename) throws FileNotFoundException, IOException{
		String filePath = Configuration.getPostDataPath()+"TuningActivity/"+filename;
		Properties p = new Properties();
		p.load(new FileInputStream(filePath));
		return p;
	}
	
	public JSONObject callTuningService(JSONObject jsonObj){
		String path = DSMURLUtils.URL_PREFIX + Configuration.getProperty("queryTunerJob");
		logger.info("Calling TuningService with :"+jsonObj.toString());
	    Map<String, String> parameters = toMap(jsonObj);
	    
		Response response = post(path,parameters, 200);
		JSONObject result = new JSONObject();
		result.accumulate("URL", path);
		result.accumulate("ResponseCode", response.getStatusCode());
		result.accumulate("ResponseData", response.body().asString());
		return result;
	}
	
	/** 
     * Convert jsonObject to Map
     * 
     * @param jsonObject jsonObject
     * @return Map
     */ 
    @SuppressWarnings("unchecked") 
    public static Map<String, String> toMap(JSONObject jsonObject) 
    { 
        Map<String, String> result = new HashMap<String, String>(); 
        Iterator<String> iterator = jsonObject.keys(); 
        String key = null; 
        String value = null; 
        while (iterator.hasNext()) 
        { 
            key = iterator.next(); 
            value = jsonObject.getString(key); 
            result.put(key, value); 
        } 
        return result; 
    } 
	
	public Properties getJSONProperties(){
		return this.p;
	}
	
	
	/**
	 * add by Jun Qing
	 * Load the properties
	 * */	
	protected void LoadProperties(Vector<String> postdata){
		postdata.add(PROP_GET_TUNING) ;
	}
	
	/**
	 * add by Jun Qing
	 * get JSON data
	 * @param key
	 * @return
	 */
	public String getJSONData(String key){
		String value = p.getProperty(key);
		if(value.endsWith(".json")){
			String filePath = Configuration.getPostDataPath()+"jsonData/Tuning/"+value.trim();
			value = FileTools.readFileAsString(filePath) ;
		}
		return value ;
	}
	
	/**
	 * add by Jun Qing
	 * Get the response from the RepsonseData
	 */
	public String getResponseResult(JSONObject result) {
		Assert.assertNotNull(result);
		String resData = result.getString("ResponseData");
		return resData;
	}
	
	
	public String getXMLData(String key) {
		String value = p.getProperty(key);
		if (value.endsWith(".xml")) {
			String filePath = Configuration.getPostDataPath() + "xmlData/Tuning/" + value.trim();
			value = FileTools.readFileAsString(filePath);
		}
		return value;
	}
	
	
	/**
	 * attach repository database information
	 */
	public JSONObject attachRepDB(String xmlString) {
		Map<String, String> postData = new HashMap<String, String>();
		postData.put("cmd", "attachRepDB");
		postData.put("sendRepositorySelectionEvent", "true");
		postData.put("newProfile", xmlString);
		postData.put("showProgress", "true");

		return sendMessageAndGetResponseWithPost(repositoryMgrPath, postData);
	}
	
	private JSONObject sendMessageAndGetResponseWithPost(String path, Map<String, String> postData) {
		Response response = post(path, postData, 200);

		int resCode = response.getStatusCode();
		String resData = response.body().asString().trim();

		JSONObject result = new JSONObject();
		result.accumulate("ResponseCode", resCode);
		result.accumulate("ResponseData", resData);
		return result;
	}
	
	public void switchRepoDB(String title) {
	
		String getRepoDBinfo = getXMLData(title);

		JSONObject result = attachRepDB(getRepoDBinfo);
		String resultCode = result.getString("ResponseCode");
		Assert.assertEquals(resultCode, "200");
	}
	
}
