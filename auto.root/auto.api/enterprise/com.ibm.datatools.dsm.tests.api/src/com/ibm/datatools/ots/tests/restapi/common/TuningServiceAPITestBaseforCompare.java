package com.ibm.datatools.ots.tests.restapi.common;


import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

public class TuningServiceAPITestBaseforCompare {
	
	protected String dbName = null;
	protected String schema = null;
	protected String username = null;
	protected String password = null;
	
	protected String dbPort = null;
	protected String dbType = null;
	protected String dbHost = null;
	protected String sqlid = null;
	protected String workloadname = null;
	
	protected String dbName2 = null;
	protected String schema2 = null;
	protected String username2 = null;
	protected String password2 = null;
	
	protected String dbPort2 = null;
	protected String dbHost2 = null;
	protected String sqlid2 = null;	
	protected String workloadname2 = null;
	
	protected TuningCommandService cs = null;
	protected Properties p;
	protected final Logger logger = LogManager.getLogger(getClass());
	
	@BeforeClass()
	@Parameters({"dbProfile","schema","username","password", "dbPort", "dbType", "dbHost", "sqlid", "workloadname","dbProfile2","schema2","username2","password2", "dbPort2",  "dbHost2", "sqlid2", "workloadname2"})
	public void beforeTest(String dbName,String schema,String username,String password, String dbPort, String dbType, String dbHost, String sqlid, String workloadname, String dbName2,String schema2,String username2,String password2, String dbPort2, String dbHost2, String sqlid2, String workloadname2) throws Exception {
		this.dbName = dbName;
		this.schema = schema;
		this.username = username;
		this.password = password;
		
		this.dbPort = dbPort;
		this.dbType = dbType;
		this.dbHost = dbHost;
		this.sqlid = sqlid;
		this.workloadname = workloadname;
		
		this.dbName2 = dbName2;
		this.schema2 = schema2;
		this.username2 = username2;
		this.password2 = password2;
		
		this.dbPort2 = dbPort2;
		this.dbHost2 = dbHost2;
		this.sqlid2 = sqlid2;
		this.workloadname2 = workloadname2;
		
		cs = new TuningCommandService();
		RestAPIBaseTest.loginDSM();
	}
}
