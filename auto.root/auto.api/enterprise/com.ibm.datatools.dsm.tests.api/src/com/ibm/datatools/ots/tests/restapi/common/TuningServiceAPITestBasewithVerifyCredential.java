package com.ibm.datatools.ots.tests.restapi.common;


import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.ibm.datatools.ots.tests.restapi.base.CheckCredentialForTuning;

public class TuningServiceAPITestBasewithVerifyCredential {
	
	protected String dbName = null;
	protected String schema = null;
	protected String username = null;
	protected String password = null;
	
	protected String dbPort = null;
	protected String dbType = null;
	protected String dbHost = null;
	protected String sqlid = null;
	protected String workloadname = null;
	protected String workloadname2 = null;
	
	protected TuningCommandService cs = null;
	protected Properties p;
	protected final Logger logger = LogManager.getLogger(getClass());
	
	@BeforeClass()
	@Parameters({"dbProfile","schema","username","password", "dbPort", "dbType", "dbHost", "sqlid", "workloadname", "workloadname2"})
	public void beforeTest(String dbName,String schema,String username,String password, String dbPort, String dbType, String dbHost, String sqlid, String workloadname, String workloadname2) throws Exception {
		this.dbName = dbName;
		this.schema = schema;
		this.username = username;
		this.password = password;
		
		this.dbPort = dbPort;
		this.dbType = dbType;
		this.dbHost = dbHost;
		this.sqlid = sqlid;
		this.workloadname = workloadname;
		this.workloadname2 = workloadname2;
		
		cs = new TuningCommandService();
		RestAPIBaseTest.loginDSM();
		CheckCredentialForTuning.checkCredential(this.dbName, this.username, this.password);
	}
}
