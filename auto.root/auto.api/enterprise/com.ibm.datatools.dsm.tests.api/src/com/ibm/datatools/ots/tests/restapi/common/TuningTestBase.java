package com.ibm.datatools.ots.tests.restapi.common;


import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.ibm.datatools.ots.tests.restapi.base.CheckCredentialForTuning;
import com.ibm.datatools.ots.tests.restapi.base.CheckMonitorDB;

public class TuningTestBase {
	
	protected String dbName = null;
	protected String schema = null;
	protected String username = null;
	protected String password = null;
	protected TuningCommandService cs = null;
	protected Properties p;
	protected final Logger logger = LogManager.getLogger(getClass());
	
	@BeforeClass()
	@Parameters({"dbProfile","schema","username","password"})
	public void beforeTest(String dbName,String schema,String username,String password) throws Exception {
		this.dbName = dbName;
		this.schema = schema;
		this.username = username;
		this.password = password;
		
		cs = new TuningCommandService();
		RestAPIBaseTest.loginDSM();
		Assert.assertEquals(CheckMonitorDB.checkMonitorDBStatus(this.dbName),"success");
		CheckCredentialForTuning.checkCredential(this.dbName, this.username, this.password);
	}
}
