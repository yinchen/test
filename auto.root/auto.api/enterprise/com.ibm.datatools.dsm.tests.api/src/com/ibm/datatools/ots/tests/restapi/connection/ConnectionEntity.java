package com.ibm.datatools.ots.tests.restapi.connection;

public class ConnectionEntity {
	
	private String dbProfileName;
	private String dbType;
	private String dbName;
	private String host;
	private String port;
	private String jdbcSecurity;
	private String userID;
	private String password;
	private String location;
	
	public String getDbProfileName() {
		return dbProfileName;
	}
	public void setDbProfileName(String dbProfileName) {
		this.dbProfileName = dbProfileName;
	}
	public String getDbType() {
		return dbType;
	}
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getJdbcSecurity() {
		return jdbcSecurity;
	}
	public void setJdbcSecurity(String jdbcSecurity) {
		this.jdbcSecurity = jdbcSecurity;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	
	public ConnectionEntity(String dbProfileName, String dbType, String dbName, String host, String port,
			String jdbcSecurity, String userID, String password,String location) {
		super();
		this.dbProfileName = dbProfileName;
		this.dbType = dbType;
		this.dbName = dbName;
		this.host = host;
		this.port = port;
		this.jdbcSecurity = jdbcSecurity;
		this.userID = userID;
		this.password = password;
		this.location = location;
	}
	
	
}
