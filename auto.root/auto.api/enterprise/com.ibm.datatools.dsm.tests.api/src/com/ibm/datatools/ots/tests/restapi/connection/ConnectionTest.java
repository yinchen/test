package com.ibm.datatools.ots.tests.restapi.connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.ConnectionTestService;
import com.ibm.datatools.ots.tests.restapi.common.RestAPIBaseTest;
import com.ibm.datatools.test.utils.JSONUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.xml.XMLSerializer;

public class ConnectionTest extends ConnectionTestBase {
	private static final Logger logger = LogManager.getLogger(ConnectionTest.class);

	private ConnectionTestService connTestServices = null;

	@BeforeClass
	public void beforeTest(){
		connTestServices = new ConnectionTestService();
		logger.info("connection Test returned -- ConnectionTest");
		
		//If can not login DSM, it will skip the following testing
		RestAPIBaseTest.loginDSM();

		//Before run test cases, test connection. If return success, then start to run test cases. Or skip.
//		connTestServices.testDatabaseConnection(databaseInfo);
	}

	@AfterClass
	public void afterTest() {
		connTestServices.removeConnection(dbProfile);
	}

	@BeforeMethod
	public void beforeMethod() {
		// If the db connection has existed, then delete this db connection
		try {
			logger.info("Sleep 5s ...");
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			logger.error("Sleep 5s failed!");
			e.printStackTrace();
		}
		connTestServices.removeConnection(dbProfile);
	}
	/**
	 * Test to add a simple database Connection and then edit it, and then remove it.
	 * */
	@Test(description="Test to add a database connection with securityMechanism=3")
	public void testAddDeleteConnection(){
		logger.info("Run test case testAddDeleteConnection:");
		
		//Test to add a connection with given profielName
		String simpleConnection = connTestServices.getJSONData("addSimpleConnection") ;
		String replacedSimpleConnection = connTestServices.replaceConnectionData(simpleConnection,
				dbProfile, databaseName, hostName, port, user, password);
		
		// Get the URL info as the expected URL information
		url = connTestServices.getURLFromConnectionData(replacedSimpleConnection);
		
		JSONObject result = connTestServices.addConnection(replacedSimpleConnection) ;

		//Verify
		String resultCode = connTestServices.getResponseResultCode(result) ;
		
		if("failure".equalsIgnoreCase(resultCode)){
			String message = connTestServices.getResponseResultMessage(result) ;
		
			if(!message.startsWith("DSW03101E")){
				//Because this connectionProfile already exists, this does not mean this case failed.
				Assert.fail(message);
			}
			System.out.println(message + "testAddDeleteConnection" );
		}
		//Remove the connection and verify
		
		result = connTestServices.removeConnection(dbProfile) ;
		connTestServices.verifyJSONResult(result);
	}
	
	/**
	 * Test to add a database Connection with operationUser
	 * */
	@Test(description="Test to add a database connection with operationUser")
	public void testAddConnectionWithOperationUser(){
		logger.info("Run test case testAddConnectionWithOperationUser:");
		
		// Test to add a connection with given profielName
		String addConnectionWithOperationUser = connTestServices.getJSONData("addConnectionWithOperationUser") ;
		String replacedSimpleConnection = connTestServices.replaceConnectionData(addConnectionWithOperationUser,
				dbProfile, databaseName, hostName, port, user, password);
		
		// Get the URL info as the expected URL information
		url = connTestServices.getURLFromConnectionData(replacedSimpleConnection);
		
		JSONObject result = connTestServices.addConnectionNoShowProgress(replacedSimpleConnection) ;
		
		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");

		// Verify the result response
		String resultResponse = connTestServices.getResponseResultResponse(result);
		String addConnectionWithOperationUserExcepted = connTestServices.getJSONData("addConnectionWithOperationUserExpected");
		securityMechanism = "3";
		verifyResponseResultWhenConnectDB(resultResponse, addConnectionWithOperationUserExcepted);
	}
	
	/**
	 * Test to add a database Connection with operationUser
	 * */
	@Test(description="Test to add a database connection with operationUser")
	public void testAddConnectionWithOperationUserSaveToRepoDB(){
		logger.info("Run test case testAddConnectionWithOperationUserSaveToRepoDB:");
		
		// Test to add a connection with given profielName
		String addConnectionWithOperationUser = connTestServices.getJSONData("addConnectionWithOperationUserSaveToRepoDB") ;
		String replacedSimpleConnection = connTestServices.replaceConnectionData(addConnectionWithOperationUser,
				dbProfile, databaseName, hostName, port, user, password);
		
		// Get the URL info as the expected URL information
		url = connTestServices.getURLFromConnectionData(replacedSimpleConnection);
		
		JSONObject result = connTestServices.addConnectionNoShowProgress(replacedSimpleConnection) ;
		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");

		// Verify the result response
		String resultResponse = connTestServices.getResponseResultResponse(result);
		String addConnectionWithOperationUserSaveToRepoDBExpected = connTestServices.getJSONData("addConnectionWithOperationUserSaveToRepoDBExpected");
		securityMechanism = "3";
		verifyResponseResultWhenConnectDB(resultResponse, addConnectionWithOperationUserSaveToRepoDBExpected);
	}
	
	/**
	 * Test to add a database Connection with Data Collection User
	 * */
	@Test(description="Test to add a database connection with Data Collection User")
	public void testAddConnectionWithDataCollectionUser(){
		logger.info("Run test case testAddConnectionWithDataCollectionUser:");
		
		// Test to add a connection with given profielName
		String addConnectionWithDataCollectionUser = connTestServices.getJSONData("addConnectionWithDataCollectionUser") ;
		String replacedSimpleConnection = connTestServices.replaceConnectionData(addConnectionWithDataCollectionUser,
				dbProfile, databaseName, hostName, port, user, password);
		
		// Get the URL info as the expected URL information
		url = connTestServices.getURLFromConnectionData(replacedSimpleConnection);
		
		JSONObject result = connTestServices.addConnectionNoShowProgress(replacedSimpleConnection) ;
		
		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");

		// Verify the result response
		String resultResponse = connTestServices.getResponseResultResponse(result);
		String addConnectionWithDataCollectionExcepted = connTestServices.getJSONData("addConnectionWithDataCollectionUserExpected");
		securityMechanism = "3";
		verifyResponseResultWhenConnectDB(resultResponse, addConnectionWithDataCollectionExcepted);
	}
	
	/**
	 * Test to add a database Connection with Operation User and Data Collection User
	 * */
	@Test(description="Test to add a database connection with Operation User and Data Collection User")
	public void testAddConnectionWithOperationUserDataCollectionUser(){
		logger.info("Run test case testAddConnectionWithOperationUserDataCollectionUser:");
		
		// Test to add a connection with given profielName
		String addConnectionWithTwoUsers = connTestServices.getJSONData("addConnectionWithTwoUsers") ;
		String replacedSimpleConnection = connTestServices.replaceConnectionData(addConnectionWithTwoUsers,
				dbProfile, databaseName, hostName, port, user, password);
		
		// Get the URL info as the expected URL information
		url = connTestServices.getURLFromConnectionData(replacedSimpleConnection);
		
		JSONObject result = connTestServices.addConnectionNoShowProgress(replacedSimpleConnection) ;
		
		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");

		// Verify the result response
		String resultResponse = connTestServices.getResponseResultResponse(result);
		String addConnectionWithTwoUsersExpected = connTestServices.getJSONData("addConnectionWithTwoUsersExpected");
		securityMechanism = "3";
		verifyResponseResultWhenConnectDB(resultResponse, addConnectionWithTwoUsersExpected);
	}
	
	/**
	 * Test to update DatabaseVersionVRMF and update DatabaseVersion
	 * */
	@Test(description="Test to update DatabaseVersionVRMF and update DatabaseVersion")
	public void testUpdateDatabaseVersionAndUpdateDatabaseVersionVRMF(){
		logger.info("Run test case testUpdateDatabaseVersionVRMF:");
		
		// Test to add a connection with given profielName
		String addConnectionWithOperationUser = connTestServices.getJSONData("addConnectionWithOperationUserSaveToRepoDB") ;
		String replacedSimpleConnection = connTestServices.replaceConnectionData(addConnectionWithOperationUser,
				dbProfile, databaseName, hostName, port, user, password);
		
		// Get the URL info as the expected URL information
		url = connTestServices.getURLFromConnectionData(replacedSimpleConnection);
		
		JSONObject result = connTestServices.addConnectionNoShowProgress(replacedSimpleConnection) ;
		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");

		JSONObject  updateResult1 = connTestServices.updateDatabaseVersionVRMF(dbProfile);
		// Verify the result response
		String updateResultResponse1 = connTestServices.getResponseResultResponse(updateResult1);
		Assert.assertTrue(updateResultResponse1.startsWith(version.substring(1)));
		
		JSONObject  updateResult2 = connTestServices.updateDatabaseVersion(dbProfile);
		// Verify the result response
		String updateResultResponse2 = connTestServices.getResponseResultResponse(updateResult2);
		Assert.assertEquals(updateResultResponse2, version.substring(1));
	}
	
	/**
	 * Test to add and get a database Connection with advanced JDBC properties.
	 */
	@Test(description = "Test to add and get Connection with advanced JDBC properties")
	public void testAddAndGetConnectionWithAdJDBCPro() {
		logger.info("Run test case testAddAndGetConnectionWithAdJDBCPro:");

		// Get the connection data, and then replace some information with the
		// actual database info
		String addConnectionWithAdJDBCPro = connTestServices.getJSONData("addConnectionWithAdJDBCPro");
		String replacedAddConnectionWithAdJDBCPro = connTestServices.replaceConnectionData(addConnectionWithAdJDBCPro,
				dbProfile, databaseName, hostName, port, user, password);

		// Get the URL info as the expected URL information
		url = connTestServices.getURLFromConnectionData(replacedAddConnectionWithAdJDBCPro);

		JSONObject result = connTestServices.addConnection(replacedAddConnectionWithAdJDBCPro);

		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");

		// Verify the result response
		String resultResponse = connTestServices.getResponseResultResponse(result);
		String addConnectionWithAdJDBCProExpected = connTestServices.getJSONData("addConnectionWithAdJDBCProExpected");
		securityMechanism = "3";
		verifyResponseResultWhenConnectDB(resultResponse, addConnectionWithAdJDBCProExpected);

		// Get the profile with xml result
		result = connTestServices.getProfile(dbProfile);
		
		// Verify the result
		verifyResponseResultWithXmlResponse(result, "getConnectionWithAdJDBCProExpected");
		
		// Get the profile with json result
		result = connTestServices.getProfileWithJsonResult(dbProfile);

		// Verify the result
		verifyResultForGetProfile(result, "getConnectionWithAdJDBCProJsonExpected", null, false);
	}

	/**
	 * Test to add and get a database Connection with User ID and encrypted
	 * password.
	 */
	@Test(description = "Test to add and get Connection with User ID and encrypted password. The encryption algorithm is DES.")
	public void testAddAndGetConWithUserIDAndEncrytedPassword() {
		logger.info("Run test case testAddAndGetConWithUserIDAndEncrytedPassword:");

		// Get the connection data, and then replace some information with the
		// actual database info
		String addConnWithUserIDAndEncrytedPassword = connTestServices
				.getJSONData("addConnWithUserIDAndEncrytedPassword");
		String replacedAddConnWithUserIDAndEncrytedPassword = connTestServices.replaceConnectionData(
				addConnWithUserIDAndEncrytedPassword, dbProfile, databaseName, hostName, port, user, password);

		// Get the URL info as the expected URL information
		url = connTestServices.getURLFromConnectionData(replacedAddConnWithUserIDAndEncrytedPassword);

		JSONObject result = connTestServices.addConnection(replacedAddConnWithUserIDAndEncrytedPassword);

		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");

		// Verify the result response
		String resultResponse = connTestServices.getResponseResultResponse(result);
		String addConnWithUserIDAndEncrytedPasswordExpected = connTestServices
				.getJSONData("addConnWithUserIDAndEncrytedPasswordExpected");

		securityMechanism = "7";
		verifyResponseResultWhenConnectDB(resultResponse, addConnWithUserIDAndEncrytedPasswordExpected);

		result = connTestServices.getConnectionByProfileInJson(dbProfile);
		resultResponse = connTestServices.getResponseResultResponse(result);

		verifyResponseResultWhenConnectDB(resultResponse, addConnWithUserIDAndEncrytedPasswordExpected);
	}

	/**
	 * Test to add a database Connection with encrypted User ID and password.
	 */
	@Test(description = "Test to add Connection with encrypted User ID and password. The encryption algorithm is AES.")
	public void testAddConWithEncryptedUserIDAndPassword() {
		logger.info("Run test case testAddConWithEncryptedUserIDAndPassword:");

		// Get the connection data, and then replace some information with the
		// actual database info
		String addConnWithEncryptedUserIDAndPassword = connTestServices
				.getJSONData("addConnWithEncryptedUserIDAndPassword");
		String replacedAddConnWithEncryptedUserIDAndPassword = connTestServices.replaceConnectionData(
				addConnWithEncryptedUserIDAndPassword, dbProfile, databaseName, hostName, port, user, password);

		// Get the URL info as the expected URL information
		url = connTestServices.getURLFromConnectionData(replacedAddConnWithEncryptedUserIDAndPassword);

		// Add connection
		JSONObject result = connTestServices.addConnection(replacedAddConnWithEncryptedUserIDAndPassword);
		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");

		// Verify the result response
		String resultResponse = connTestServices.getResponseResultResponse(result);
		String addConnWithEncryptedUserIDAndPasswordExpected = connTestServices
				.getJSONData("addConnWithEncryptedUserIDAndPasswordExpected");

		securityMechanism = "9";
		verifyResponseResultWhenNotConnectDB(resultResponse, addConnWithEncryptedUserIDAndPasswordExpected);
	}

	/**
	 * Test and add a database Connection with encrypted User ID and password,
	 * and encrypted security-sensitive data.
	 */
	@Test(description = "Test and add Connection with Encrypted User ID and password, and encrypted security-sensitive data. The encryption algorithm is DES.")
	public void testAndAddConWithEncryptedUserIDAndPWEncrytedSSData() {
		logger.info("Run test case testAndAddConWithEncryptedUserIDAndPWEncrytedSSData:");

		// Get the connection data, and then replace some information with the
		// actual database info
		String addConnWithEncryptedUserIDAndPWEncrytedSSData = connTestServices
				.getJSONData("addConnWithEncryptedUserIDAndPWEncrytedSSData");
		String replacedAddConnWithEncryptedUserIDAndPWEncrytedSSData = connTestServices.replaceConnectionData(
				addConnWithEncryptedUserIDAndPWEncrytedSSData, dbProfile, databaseName, hostName, port, user, password);

		// Get the URL info as the expected URL information
		url = connTestServices.getURLFromConnectionData(replacedAddConnWithEncryptedUserIDAndPWEncrytedSSData);

		// Test connection
		JSONObject result = connTestServices.testConnection(replacedAddConnWithEncryptedUserIDAndPWEncrytedSSData);
		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("message"), "");
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("response"), "\"\"");

		result = connTestServices.addConnection(replacedAddConnWithEncryptedUserIDAndPWEncrytedSSData);

		// Verify the result code
		resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");

		// Verify the result response
		String resultResponse = connTestServices.getResponseResultResponse(result);
		String addConnWithEncryptedUserIDAndPWEncrytedSSDataExpected = connTestServices
				.getJSONData("addConnWithEncryptedUserIDAndPWEncrytedSSDataExpected");

		securityMechanism = "13";
		verifyResponseResultWhenConnectDB(resultResponse, addConnWithEncryptedUserIDAndPWEncrytedSSDataExpected);
	}

	/**
	 * Test to add a database Connection with user ID only.
	 */
	@Test(description = "Test to add Connection with user ID only")
	public void testAddConWithUserIDOnly() {
		logger.info("Run test case testAddConWithUserIDOnly:");

		// Get the connection data, and then replace some information with the
		// actual database info
		String addConnWithUserIDOnly = connTestServices.getJSONData("addConnWithUserIDOnly");
		String replacedAddConnWithUserIDOnly = connTestServices.replaceConnectionData(addConnWithUserIDOnly, dbProfile,
				databaseName, hostName, port, user, null);

		// Get the URL info as the expected URL information
		url = connTestServices.getURLFromConnectionData(replacedAddConnWithUserIDOnly);

		JSONObject result = connTestServices.addConnection(replacedAddConnWithUserIDOnly);

		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");

		// Verify the result response
		String resultResponse = connTestServices.getResponseResultResponse(result);
		String addConnWithUserIDOnlyExpected = connTestServices.getJSONData("addConnWithUserIDOnlyExpected");

		securityMechanism = "4";
		verifyResponseResultWhenConnectDB(resultResponse, addConnWithUserIDOnlyExpected);
	}

	/**
	 * Test to add a database Connection with SSL Connectivity.
	 */
	@Test(description = "Test to add Connection with SSL Connectivity")
	public void testAddConWithSSLConnectivity() {
		logger.info("Run test case testAddConWithSSLConnectivity:");

		// Get the connection data, and then replace some information with the
		// actual database info
		String addConnWithSSLConnectivity = connTestServices.getJSONData("addConnWithSSLConnectivity");
		String replacedAddConnWithSSLConnectivity = connTestServices.replaceConnectionData(addConnWithSSLConnectivity,
				databaseInfo);

		// Get the URL info as the expected URL information
		url = connTestServices.getURLFromConnectionData(replacedAddConnWithSSLConnectivity);

		JSONObject result = connTestServices.addConnection(replacedAddConnWithSSLConnectivity);

		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");

		// Verify the result response
		String resultResponse = connTestServices.getResponseResultResponse(result);
		String addConnWithSSLConnectivityExpected = connTestServices.getJSONData("addConnWithSSLConnectivityExpected");

		securityMechanism = "100";
		verifyResponseResultWhenConnectDB(resultResponse, addConnWithSSLConnectivityExpected);
	}

	/**
	 * Test to add a database Connection with the error DataServerType. The db
	 * info is LUW related, but select Data server type 'DB for Z/OS'.
	 */
	@Test(description = "Test to add Connection with the error DataServerType")
	public void testAddConnectionWithErrDataServerType() {
		logger.info("Run test case testAddConnectionWithErrDataServerType:");

		// Get the connection data
		String addConnectionWithErrDataServerType = connTestServices.getJSONData("addConnectionWithErrDataServerType");

		// Change the dataServerType to DB2Z and change the location to the
		// actual database name.
		JSONObject addConnectionWithErrDataServerTypeJson = JSONObject.fromObject(addConnectionWithErrDataServerType);
		String profileJSONS = addConnectionWithErrDataServerTypeJson.getString("profileJSON");
		JSONObject profileJSON = JSONObject.fromObject(profileJSONS);
		profileJSON.remove("dataServerType");
		profileJSON.put("dataServerType", "DB2Z");
		profileJSON.put("location", databaseName);
		addConnectionWithErrDataServerTypeJson.remove("profileJSON");
		addConnectionWithErrDataServerTypeJson.put("profileJSON", profileJSON);
		addConnectionWithErrDataServerType = addConnectionWithErrDataServerTypeJson.toString();

		// Replace some information with the actual database info
		String replacedAddConnectionWithErrDataServerType = connTestServices.replaceConnectionData(
				addConnectionWithErrDataServerType, dbProfile, databaseName, hostName, port, user, password);

		// Get the URL info as the expected URL information
		url = connTestServices.getURLFromConnectionData(replacedAddConnectionWithErrDataServerType);

		JSONObject result = connTestServices.addConnection(replacedAddConnectionWithErrDataServerType);

		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");

		// Verify the result response
		String resultResponse = connTestServices.getResponseResultResponse(result);
		String addConnectionWithErrDataServerTypeExpected = connTestServices
				.getJSONData("addConnectionWithErrDataServerTypeExpected");
		securityMechanism = "3";
		verifyResponseResultWhenNotConnectDBForZ(resultResponse, addConnectionWithErrDataServerTypeExpected);
	}
	
	
	/**
	 * Test to add and get a database Connection with advanced JDBC properties.
	 */
	@Test(description = "Test to add and get HADR Connections with advanced JDBC properties")
    public void testAddAndGetHADRConnectionsWithJDBCPro() {
		// Get the connection data, and then replace some information with the
		// actual database info
		String addConnectionWithAdJDBCPro = connTestServices.getJSONData("addHADRConnectionWithJDBCPro");
		
		// Get the URL info as the expected URL information
		url = connTestServices.getURLFromConnectionData(addConnectionWithAdJDBCPro);
		
		JSONObject result = connTestServices.addConnection(addConnectionWithAdJDBCPro);
		
		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");
		
		// Verify the result response
		String resultResponse = connTestServices.getResponseResultResponse(result);
		
		//Verify primary and standby dbs.
		verifyResponseResultWhenConnectHADRDBs(addConnectionWithAdJDBCPro, resultResponse);
	}
	
	/**
	 * Test to  list All Filter Tags
	 * */
	@Test(description="Test to list  all  filter tags  when click 'Filter by Tag' icon in Manage Connections page.")
	public void testListFilterTags(){
		logger.info("Run test case for test list filder Tags:  testListFilterTags");
		
		
		JSONObject result = connTestServices.listFilterTags();
		
		// Verify the result code
		String  resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("message"), "");
	
		}
	
	
	private void verifyResponseResultWhenConnectHADRDBs(String addConnectionWithAdJDBCPro, String resultResponse){
           JSONObject addDBInfo = JSONObject.fromObject(addConnectionWithAdJDBCPro).getJSONObject("profileJSON");
		JSONObject actualResult = JSONObject.fromObject(resultResponse);
		Assert.assertTrue(actualResult.getString("name").contains(addDBInfo.getString("name")));
		Assert.assertEquals(addDBInfo.getString("databaseName"), actualResult.getString("databaseName"));
		Assert.assertEquals(addDBInfo.getString("port"), actualResult.getString("port"));
		Assert.assertNotNull(actualResult.getString("OSType"));
		Assert.assertEquals(addDBInfo.getString("host"), actualResult.getString("host"));
		Assert.assertNotNull(actualResult.getString("dataServerType"));
		Assert.assertTrue(actualResult.getString("hadr_result").contains(addDBInfo.getString("name")));

	}
	
	/**
	 * Test to add and get a database Connection with advanced JDBC properties.
	 * @author jiahoup@cn.ibm.com
	 * Date:20161103
	 */
	@Test(description = "Test to add and get Purescale-HADR Connections with advanced JDBC properties")
    public void testAddAndGetPureScaleHADRConnectionsWithJDBCPro() {
		// Get the connection data, and then replace some information with the
		// actual database info
		String addConnectionWithAdJDBCPro = connTestServices.getJSONData("addPureScaleHADRConnectionWithJDBCPro");
		
		// Get the URL info as the expected URL information
		url = connTestServices.getURLFromConnectionData(addConnectionWithAdJDBCPro);
		
		JSONObject result = connTestServices.addConnection(addConnectionWithAdJDBCPro);
		
		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");
		
		// Verify the result response
		String resultResponse = connTestServices.getResponseResultResponse(result);
		
		//Verify primary and standby dbs.
		verifyResponseResultWhenConnectPureScaleHADRDBs(addConnectionWithAdJDBCPro, resultResponse);
	}
	
	
	
	
	
	/**
	 * Test to add a database Connection with operationUser
	 * */
	@Test(description="Test to add,delete,get version,get db license and delete db connection for dash connection with monitorUser")
	public void testAddEditGetDBVersionandLicenseDelDashDB(){
		logger.info("Run test case testAddEditGetDBVersionandLicenseDelDashDB: testAddEditGetDBVersionandLicenseDelDashDB");
		
		// Test to add a connection with given profielName
		String addConnectionforDashDB = connTestServices.getJSONData("addConnectionforDashDB") ;
		String replacedSimpleConnection = connTestServices.replaceConnectionData(addConnectionforDashDB,
				dbProfile, databaseName, hostName, port, user, password);
		
		// Get the URL info as the expected URL information
		//url = connTestServices.getURLFromConnectionData(replacedSimpleConnection);
		
		JSONObject result = connTestServices.addConnectionNoShowProgress(replacedSimpleConnection) ;
		
		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");

		// Verify the result response for Add connection
		String resultResponse = connTestServices.getResponseResultResponse(result);
		String addConnectionforDashDBExcepted = connTestServices.getJSONData("addConnectionforDashDBExpected");
		securityMechanism = "3";
		verifyResponseResultWhenConnectDashDB(resultResponse, addConnectionforDashDBExcepted);
	
		

		//editConnection
		result=connTestServices.editConnection(replacedSimpleConnection);
		// Verify the result code
		resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");
		
		
	   //Update DB Version
		
		JSONObject  updateVersionResult = connTestServices.updateDatabaseVersion(dbProfile);
		// Verify the result response
		String updateVersionResponse = connTestServices.getResponseResultResponse(updateVersionResult);
		Assert.assertEquals(updateVersionResponse, version.substring(1));
		
		//Get DB2 License
		JSONObject licenseResult = connTestServices.getDB2License(dbProfile);
		// Verify the result response
		JSONObject  db2license = JSONObject.fromObject(licenseResult.getString("ResponseData"));
		Assert.assertEquals(db2license.getString("db2license"), "DASHDB");
		//Remove the connection and verify
		
		result = connTestServices.removeConnection(dbProfile) ;
		connTestServices.verifyJSONResult(result);
		
	
	}
	
	/**
	 * Test to verifyResponseResult when connect purescale-hadr dbs
	 * @author jiahoup@cn.ibm.com
	 * date:20161103
	 */
	private void verifyResponseResultWhenConnectPureScaleHADRDBs(String addConnectionWithAdJDBCPro, String resultResponse){
        JSONObject addDBInfo = JSONObject.fromObject(addConnectionWithAdJDBCPro).getJSONObject("profileJSON");
		JSONObject actualResult = JSONObject.fromObject(resultResponse);
		Assert.assertTrue(actualResult.getString("name").contains(addDBInfo.getString("name")));
		Assert.assertEquals(addDBInfo.getString("databaseName"), actualResult.getString("databaseName"));
		Assert.assertEquals(addDBInfo.getString("port"), actualResult.getString("port"));
		Assert.assertNotNull(actualResult.getString("OSType"));
		Assert.assertEquals(addDBInfo.getString("host"), actualResult.getString("host"));
		Assert.assertNotNull(actualResult.getString("dataServerType"));
		Assert.assertTrue(actualResult.getString("hadr_result").contains(addDBInfo.getString("name")));

	}


	private void verifyResponseResultWithXmlResponse(JSONObject actualResult, String expectedXMLResult) {
		String expectedResult = connTestServices.getXMLData(expectedXMLResult);
		XMLSerializer xmlSerializer = new XMLSerializer();
		JSONObject expectedResultJson = (JSONObject) xmlSerializer.read(expectedResult);
		Map<String, String> replacedKeyValues = new HashMap<String, String>();
		replacedKeyValues.put("databaseName", databaseName);
		replacedKeyValues.put("dataServerType", dataServerType);
		replacedKeyValues.put("databaseVersion", version.substring(1));
		replacedKeyValues.put("URL", url);
		if (dataServerType.equals("DB2LUW")) {
			replacedKeyValues.put("capability", "DSM_ENTERPRISE_LUW");
		}
		replacedKeyValues.put("DB2Instance", instanceProfile);
		replacedKeyValues.put("host", hostName);
		if (dataServerExternalType == null) {
			replacedKeyValues.put("dataServerExternalType", dataServerType);
		} else {
			replacedKeyValues.put("dataServerExternalType", dataServerExternalType);
		}

		replacedKeyValues.put("port", port);
		replacedKeyValues.put("name", dbProfile);
		replacedKeyValues.put("user", user);

		// The following parameters can't be compared
		replacedKeyValues.put("dbUUID", getValueFromJson(actualResult, "dbUUID"));
		replacedKeyValues.put(".dsrcprop.", getValueFromJson(actualResult, ".dsrcprop."));
		replacedKeyValues.put("lastUpdatedTimeStamp", getValueFromJson(actualResult, "lastUpdatedTimeStamp"));
		replacedKeyValues.put("TimeZoneDiff", getValueFromJson(actualResult, "TimeZoneDiff"));
		replacedKeyValues.put("creator", getValueFromJson(actualResult, "creator"));
		replacedKeyValues.put("TimeZone", getValueFromJson(actualResult, "TimeZone"));
		replacedKeyValues.put("OSType", getValueFromJson(actualResult, "OSType"));

		replaceResultJson(expectedResultJson, replacedKeyValues);

		Assert.assertTrue(JSONUtils.compareJsons(expectedResultJson, actualResult));
	}

	private void replaceResultJson(JSONObject expectedResultJson, Map<String, String> replacedKeyValues) {
		JSONArray response = (JSONArray) expectedResultJson.getJSONArray("response").get(0);
		for (int i = 0; i < response.size(); i++) {
			String key = response.getJSONObject(i).getString("@name");
			Iterator its = replacedKeyValues.entrySet().iterator();
			while (its.hasNext()) {
				Entry entry = (Entry) its.next();
				String entryKey = (String) entry.getKey();
				String entryValue = (String) entry.getValue();
				if (entryKey.equals(key)) {
					response.getJSONObject(i).remove("@currentValue");
					response.getJSONObject(i).accumulate("@currentValue", entryValue);
				}
			}
		}
	}

	private String getValueFromJson(Object resultJson, String nameValue) {
		JSONArray response = null;
		
		if (resultJson instanceof JSONArray) {
			response = (JSONArray) resultJson;
		} else {
			response = (JSONArray)((JSONObject) resultJson).getJSONArray("response").get(0);
		}
		for (int i = 0; i < response.size(); i++) {
			String key = response.getJSONObject(i).getString("@name");
			String value = response.getJSONObject(i).getString("@currentValue");

			if (key.equals(nameValue)) {
				return value;
			}
		}
		return null;
	}
	
	private void verifyResultForGetProfile(JSONObject result, String expectedResult, ArrayList<String> ignoreKeyValues, Boolean ignoreNumValues) {
		// Verify the result code
		String resultCode = result.getString("ResponseCode");
		Assert.assertEquals(resultCode, "200");
		
		//Verify the ResponseData part:
		String resultCodeInResponseData = result.getJSONObject("ResponseData").getString("resultCode");
		Assert.assertEquals(resultCodeInResponseData, "success");
		
		String messageInResponseData = result.getJSONObject("ResponseData").getString("message");
		Assert.assertEquals(messageInResponseData, "");
		
		String HADRResultInResponseData = result.getJSONObject("ResponseData").getString("HADRResult");
		Assert.assertEquals(HADRResultInResponseData, "");
	    	
		// Verify the result response
		String repsone = result.getJSONObject("ResponseData").getString("response");
		XMLSerializer xmlSerializer = new XMLSerializer();
		JSONArray resData = (JSONArray) xmlSerializer.read(repsone);
		
		String resultExpected = connTestServices.getJSONData(expectedResult);
		String repsoneExpected = JSONObject.fromObject(resultExpected).getString("response");
		JSONArray resultExpectedData = (JSONArray) xmlSerializer.read(repsoneExpected);
		
		verifyResponseResultWithXmlResponseForGetProfile(resData, resultExpectedData);
	}
	
	private void verifyResponseResultWithXmlResponseForGetProfile(JSONArray actualResult, JSONArray expectedXMLResult) {
		Map<String, String> replacedKeyValues = new HashMap<String, String>();
		replacedKeyValues.put("databaseName", databaseName);
		replacedKeyValues.put("dataServerType", dataServerType);
		replacedKeyValues.put("databaseVersion", version.substring(1));
		replacedKeyValues.put("URL", url);
		if (dataServerType.equals("DB2LUW")) {
			replacedKeyValues.put("capability", "DSM_ENTERPRISE_LUW");
		}
		replacedKeyValues.put("DB2Instance", instanceProfile);
		replacedKeyValues.put("host", hostName);
		if (dataServerExternalType == null) {
			replacedKeyValues.put("dataServerExternalType", dataServerType);
		} else {
			replacedKeyValues.put("dataServerExternalType", dataServerExternalType);
		}

		replacedKeyValues.put("port", port);
		replacedKeyValues.put("name", dbProfile);
		replacedKeyValues.put("user", user);

		// The following parameters can't be compared
		replacedKeyValues.put("dbUUID", getValueFromJson(actualResult, "dbUUID"));
		replacedKeyValues.put(".dsrcprop.", getValueFromJson(actualResult, ".dsrcprop."));
		replacedKeyValues.put("lastUpdatedTimeStamp", getValueFromJson(actualResult, "lastUpdatedTimeStamp"));
		replacedKeyValues.put("TimeZoneDiff", getValueFromJson(actualResult, "TimeZoneDiff"));
		replacedKeyValues.put("creator", getValueFromJson(actualResult, "creator"));
		replacedKeyValues.put("TimeZone", getValueFromJson(actualResult, "TimeZone"));
		replacedKeyValues.put("OSType", getValueFromJson(actualResult, "OSType"));

		replaceResultJsonForGetProfile(expectedXMLResult, replacedKeyValues);

		Assert.assertTrue(JSONUtils.compareJsonArrays(expectedXMLResult, actualResult));
	}
	
	private void replaceResultJsonForGetProfile(JSONArray expectedResultJson, Map<String, String> replacedKeyValues) {
		JSONArray response = expectedResultJson;
		for (int i = 0; i < response.size(); i++) {
			String key = response.getJSONObject(i).getString("@name");
			Iterator its = replacedKeyValues.entrySet().iterator();
			while (its.hasNext()) {
				Entry entry = (Entry) its.next();
				String entryKey = (String) entry.getKey();
				String entryValue = (String) entry.getValue();
				if (entryKey.equals(key)) {
					response.getJSONObject(i).remove("@currentValue");
					response.getJSONObject(i).accumulate("@currentValue", entryValue);
				}
			}
		}
	}
}
