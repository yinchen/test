package com.ibm.datatools.ots.tests.restapi.connection;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.test.utils.JSONUtils;

import net.sf.json.JSONObject;

public class ConnectionTestBase {

	protected String dbProfile = null;
	protected String instanceProfile = null;
	protected String hostName = null;
	protected String version = null;
	protected String databaseName = null;
	protected String port = null;
	protected String user = null;
	protected String password = null;
	protected String securityMechanism = null;
	protected String dataServerType = null;
	protected String url = null;
	protected String dataServerExternalType = null;
	protected String truststoreFileName = null;
	protected String truststorePassword = null;
	protected String dsmNewUserName = null;
	protected String dsmNewUserPassword = null;
	protected String dsmUser = Configuration.getProperty("LoginUser");
	protected JSONObject databaseInfo = new JSONObject();

	/**
	 * This is the method we used to initialize the dbProfile name, the
	 * instanceProfile and so on. The dbProfile name will be saved to class
	 * variable, and sub-class could use this dbProfilename directly.
	 * 
	 */
	@BeforeClass
	@Parameters({ "dbProfile", "instanceProfile", "hostName", "version", "databaseName", "port", "user", "password",
			"securityMechanism", "dataServerType", "dataServerExternalType", "truststoreFileName",
			"truststorePassword", "dsmNewUserName","dsmNewUserPassword"})
	public void beforeTest(@Optional("") String dbProfile, @Optional("") String instanceProfile,
			@Optional("") String hostName, @Optional("") String version, @Optional("") String databaseName,
			@Optional("") String port, @Optional("") String user, @Optional("") String password,
			@Optional("") String securityMechanism, @Optional("") String dataServerType,
			@Optional("") String dataServerExternalType, @Optional("") String truststoreFileName,
			@Optional("") String truststorePassword, @Optional("") String dsmNewUserName, 
			@Optional("") String dsmNewUserPassword,ITestContext context) {
		this.dbProfile = this.getDBParameterValue(dbProfile, context, "dbProfile");
		this.instanceProfile = this.getDBParameterValue(instanceProfile, context, "instanceProfile");
		this.hostName = this.getDBParameterValue(hostName, context, "hostName");
		this.version = this.getDBParameterValue(version, context, "version");
		this.databaseName = this.getDBParameterValue(databaseName, context, "databaseName");
		this.port = this.getDBParameterValue(port, context, "port");
		this.user = this.getDBParameterValue(user, context, "user");
		this.password = this.getDBParameterValue(password, context, "password");
		this.securityMechanism = this.getDBParameterValue(securityMechanism, context, "securityMechanism");
		this.dataServerType = this.getDBParameterValue(dataServerType, context, "dataServerType");
		this.dataServerExternalType = this.getDBParameterValue(dataServerExternalType, context,
				"dataServerExternalType");
		this.truststoreFileName = this.getDBParameterValue(truststoreFileName, context, "truststoreFileName");
		this.truststorePassword = this.getDBParameterValue(truststorePassword, context, "truststorePassword");
		this.dsmNewUserName = this.getDBParameterValue(dsmNewUserName, context, "dsmNewUserName");
		this.dsmNewUserPassword = this.getDBParameterValue(dsmNewUserPassword, context, "dsmNewUserPassword");
		databaseInfo.put("dbProfile", this.dbProfile);
		databaseInfo.put("databaseName", this.databaseName);
		databaseInfo.put("hostName", this.hostName);
		databaseInfo.put("port", this.port);
		databaseInfo.put("user", this.user);
		databaseInfo.put("password", this.password);
		databaseInfo.put("dataServerType", this.dataServerType);
		databaseInfo.put("dataServerExternalType", this.dataServerExternalType);
		databaseInfo.put("securityMechanism", this.securityMechanism);
		databaseInfo.put("dsmUser", this.dsmUser);
		databaseInfo.put("truststoreFileName", this.truststoreFileName);
		databaseInfo.put("truststorePassword", this.truststorePassword);
	}

	/**
	 * Utils method to retrieve the parameter(like: dbProfile name) transform in
	 * from Params in suite, if a test suite A is referenced by another test
	 * suite B, invoking parent suite B, the _parameter defined in B can not
	 * transform into A, using this method to solve this problem.
	 */
	private String getDBParameterValue(String parameter, ITestContext context, String parameterName) {
		String _parameter = null;
		if (context.getSuite() != null && context.getSuite().getXmlSuite() != null
				&& context.getSuite().getXmlSuite().getParentSuite() != null) {
			_parameter = context.getSuite().getXmlSuite().getParentSuite().getParameter(parameterName);
		}

		if (_parameter != null) {
			return _parameter;
		} else if (parameter != null && !parameter.equals("")) {
			return parameter;
		} else {
			return System.getProperty(parameterName);
		}
	}

	protected void verifyResponseResultWhenConnectDB(String resultResponse, String expectedResponse) {
		JSONObject expectedResult = JSONObject.fromObject(expectedResponse);
		JSONObject actualResult = JSONObject.fromObject(resultResponse);
		ArrayList<String> key = new ArrayList();
		key.add("dbUUID");
		key.add("DB2Instance");
		key.add("additional2");
		key.add(".dsrcprop.");
		key.add("lastUpdatedTimeStamp");
		key.add("OSType");
		key.add("databaseName");
		key.add("dataServerType");
		key.add("databaseVersion");
		key.add("URL");
		key.add("host");
		key.add("dataServerExternalType");
		key.add("securityMechanism");
		key.add("port");
		key.add("user");
		key.add("name");
		key.add("TimeZoneDiff");
		key.add("Timezone");
		key.add("creator");
		key.add("dsconnmgt_jdbc_sslTrustStorePassword");
		key.add("JDBCProperties");
		key.add("dsconnmgt_jdbc_sslTrustStoreLocation");

		Assert.assertTrue(JSONUtils.compareJsons(expectedResult, actualResult, key));

		String actualDatabaseName = actualResult.getString("databaseName");
		String actualDataServerType = actualResult.getString("dataServerType");

		String actualURL = actualResult.getString("URL");

		String actualhost = actualResult.getString("host");
		String actualDataServerExternalType = actualResult.getString("dataServerExternalType");
		String actualSecurityMechanism = actualResult.getString("securityMechanism");
		String actualPort = actualResult.getString("port");
		String actualUser = "";
		if(actualResult.containsKey("user") && actualResult.get("user") != null) {
			actualUser = actualResult.getString("user");
		}
		String actualName = actualResult.getString("name");

		Assert.assertEquals(actualDatabaseName, databaseName);
		Assert.assertEquals(actualDataServerType, dataServerType);
		Assert.assertEquals(actualURL, url);
		Assert.assertEquals(actualhost, hostName);
		Assert.assertEquals(actualSecurityMechanism, securityMechanism);
		Assert.assertEquals(actualPort, port);
		if (expectedResult.containsKey("user") && (!expectedResult.getString("user").equals(""))) {
			Assert.assertEquals(actualUser, user);
		}

		Assert.assertEquals(actualName, dbProfile);

		//for BI, dataServerExternalType is not same with dataServerType. For enterprise, same.
		if (dataServerExternalType!=null){
			Assert.assertEquals(actualDataServerExternalType, dataServerExternalType);
		}else{
			Assert.assertEquals(actualDataServerExternalType, dataServerType);
		}
		
		//when edit connection with wrong info, no databaseVersion and DB2Instance in response data.
		if (!expectedResult.getString("databaseName").equalsIgnoreCase("aaaa")){
			String actualDatabaseVersion = actualResult.getString("databaseVersion");
			Assert.assertEquals(actualDatabaseVersion, version.substring(1));
			
			String actualDB2Instance = actualResult.getString("DB2Instance");
			Assert.assertEquals(actualDB2Instance, instanceProfile);
		}
	}

	protected void verifyResponseResultWhenNotConnectDB(String resultResponse, String expectedResponse) {
		JSONObject expectedResult = JSONObject.fromObject(expectedResponse);
		JSONObject actualResult = JSONObject.fromObject(resultResponse);
		ArrayList<String> key = new ArrayList();
		key.add("dbUUID");
		key.add(".dsrcprop.");
		key.add("lastUpdatedTimeStamp");
		key.add("databaseName");
		key.add("dataServerType");
		key.add("URL");
		key.add("host");
		key.add("dataServerExternalType");
		key.add("securityMechanism");
		key.add("port");
		key.add("user");
		key.add("name");
		key.add("creator");

		Assert.assertTrue(JSONUtils.compareJsons(expectedResult, actualResult, key));

		String actualDatabaseName = actualResult.getString("databaseName");
		String actualDataServerType = actualResult.getString("dataServerType");

		String actualURL = actualResult.getString("URL");

		String actualhost = actualResult.getString("host");
		String actualDataServerExternalType = actualResult.getString("dataServerExternalType");
		String actualSecurityMechanism = actualResult.getString("securityMechanism");
		String actualPort = actualResult.getString("port");
		String actualUser = actualResult.getString("user");
		String actualName = actualResult.getString("name");

		Assert.assertEquals(actualDatabaseName, databaseName);
		Assert.assertEquals(actualDataServerType, dataServerType);
		Assert.assertEquals(actualURL, url);
		Assert.assertEquals(actualhost, hostName);
		Assert.assertEquals(actualSecurityMechanism, securityMechanism);
		Assert.assertEquals(actualPort, port);
		Assert.assertEquals(actualUser, user);
		Assert.assertEquals(actualName, dbProfile);

		// for BI, dataServerExternalType is not same with dataServerType. For
		// enterprise, same.
		if (dataServerExternalType != null) {
			Assert.assertEquals(actualDataServerExternalType, dataServerExternalType);
		} else {
			Assert.assertEquals(actualDataServerExternalType, dataServerType);
		}
	}
	
	protected void verifyResponseResultWhenNotConnectDBForZ(String resultResponse, String expectedResponse) {
		JSONObject expectedResult = JSONObject.fromObject(expectedResponse);
		JSONObject actualResult = JSONObject.fromObject(resultResponse);
		ArrayList<String> key = new ArrayList();
		key.add("dbUUID");
		key.add(".dsrcprop.");
		key.add("lastUpdatedTimeStamp");
		key.add("databaseName");
		key.add("dataServerType");
		key.add("URL");
		key.add("host");
		key.add("dataServerExternalType");
		key.add("securityMechanism");
		key.add("port");
		key.add("user");
		key.add("name");
		key.add("creator");

		Assert.assertTrue(JSONUtils.compareJsons(expectedResult, actualResult, key));

		String actualDataServerType = actualResult.getString("dataServerType");

		String actualURL = actualResult.getString("URL");
		String actualhost = actualResult.getString("host");
		String actualDataServerExternalType = actualResult.getString("dataServerExternalType");
		String actualSecurityMechanism = actualResult.getString("securityMechanism");
		String actualPort = actualResult.getString("port");
		String actualUser = actualResult.getString("user");
		String actualName = actualResult.getString("name");

		Assert.assertEquals(actualDataServerType, "DB2Z");
		Assert.assertEquals(actualURL, url);
		Assert.assertEquals(actualhost, hostName);
		Assert.assertEquals(actualSecurityMechanism, securityMechanism);
		Assert.assertEquals(actualPort, port);
		Assert.assertEquals(actualUser, user);
		Assert.assertEquals(actualName, dbProfile);

		// for BI, dataServerExternalType is not same with dataServerType. For
		// enterprise, same.
		if (dataServerExternalType != null) {
			Assert.assertEquals(actualDataServerExternalType, dataServerExternalType);
		} else {
			Assert.assertEquals(actualDataServerExternalType, "DB2Z");
		}
	}
	
	
	
	protected void verifyResponseResultWhenConnectDashDB(String resultResponse, String expectedResponse) {
		JSONObject expectedResult = JSONObject.fromObject(expectedResponse);
		JSONObject actualResult = JSONObject.fromObject(resultResponse);
		ArrayList<String> key = new ArrayList();
		key.add("dbUUID");
		key.add("DB2Instance");
		key.add("additional2");
		key.add(".dsrcprop.");
		key.add("lastUpdatedTimeStamp");
		key.add("OSType");
		key.add("databaseName");
		key.add("dataServerType");
		key.add("databaseVersion");
		key.add("URL");
		key.add("host");
		key.add("dataServerExternalType");
		key.add("securityMechanism");
		key.add("port");
		key.add("user");
		key.add("name");
		key.add("TimeZoneDiff");
		key.add("Timezone");
		key.add("creator");
		key.add("dsconnmgt_jdbc_sslTrustStorePassword");
		key.add("JDBCProperties");
		key.add("dsconnmgt_jdbc_sslTrustStoreLocation");
		key.add("databaseVersion_VRMF");
		Assert.assertTrue(JSONUtils.compareJsons(expectedResult, actualResult, key));

		String actualDatabaseName = actualResult.getString("databaseName");
		String actualDataServerType = actualResult.getString("dataServerType");

		String actualURL = actualResult.getString("URL");

		String actualhost = actualResult.getString("host");
		String actualDataServerExternalType = actualResult.getString("dataServerExternalType");
		String actualSecurityMechanism = actualResult.getString("securityMechanism");
		String actualPort = actualResult.getString("port");
		String actualUser = "";
		if(actualResult.containsKey("user") && actualResult.get("user") != null) {
			actualUser = actualResult.getString("user");
		}
		String actualName = actualResult.getString("name");

		Assert.assertEquals(actualDatabaseName, databaseName);
		Assert.assertEquals(actualDataServerType, dataServerType);
		Assert.assertEquals(actualhost, hostName);
		Assert.assertEquals(actualSecurityMechanism, securityMechanism);
		Assert.assertEquals(actualPort, port);
		if (expectedResult.containsKey("user") && (!expectedResult.getString("user").equals(""))) {
			Assert.assertEquals(actualUser, user);
		}

		Assert.assertEquals(actualName, dbProfile);

		//for BI, dataServerExternalType is not same with dataServerType. For enterprise, same.
		if (dataServerExternalType!=null){
			Assert.assertEquals(actualDataServerExternalType, dataServerExternalType);
		}else{
			Assert.assertEquals(actualDataServerExternalType, dataServerType);
		}
		
		//when edit connection with wrong info, no databaseVersion and DB2Instance in response data.
		if (!expectedResult.getString("databaseName").equalsIgnoreCase("aaaa")){
			String actualDatabaseVersion = actualResult.getString("databaseVersion");
			Assert.assertEquals(actualDatabaseVersion, version.substring(1));
		}
	}
}




