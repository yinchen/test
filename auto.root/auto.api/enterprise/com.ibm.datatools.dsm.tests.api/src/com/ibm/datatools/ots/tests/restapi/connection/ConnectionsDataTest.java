package com.ibm.datatools.ots.tests.restapi.connection;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.common.ConnectionTestService;
import com.ibm.datatools.test.utils.JSONUtils;

import net.sf.json.JSONObject;

public class ConnectionsDataTest extends ConnectionTestBase {
	private static final Logger logger = LogManager.getLogger(ConnectionsDataTest.class);

	private ConnectionTestService connTestServices = null;
	private String userid = null;

	@BeforeClass
	public void beforeTest() {
		connTestServices = new ConnectionTestService();
		logger.info("connections Data Test returned");
		userid = Configuration.getProperty("LoginUser");
		connTestServices.keepRepositoryDBInfo();
		connTestServices.switchRepoDB("connectionDataTest");
	}

	@AfterClass
	public void afterTest() {
		if (connTestServices.switchToOrignialRepDB() == false) {
			logger.warn(
					"Switch to the orignial repository database failed. So will switch to a temporary repository database.");
			connTestServices.switchRepoDB("personalRepDB");
		}
	}

	/**
	 * Test to get all connections which contains some monitoring connections
	 * and some blackout connections and so on.
	 */
	@Test(description = "Test to get all connects without metric info by getAll API")
	public void testGetAllConnectionsWithoutMetricInfo() {
		logger.info("Run connectionsData test case testGetAllConnectionsWithoutMetricInfo:");
		
		String getAllConnectionsWithoutMetricInfo = connTestServices.getJSONData("getAllConnectionsWithoutMetricInfo");
		JSONObject jsonString = JSONObject.fromObject(getAllConnectionsWithoutMetricInfo);
		connTestServices.replaceJsonObjectValue(jsonString, "userid", userid);

		JSONObject result = connTestServices.getAllConnections(jsonString.toString());
		verifyResult(result, "getAllConnectionsWithoutMetricInfoExpected", null);
	}

	/**
	 * Test to get all connections which contains some monitoring connections
	 * and some blackout connections and so on.
	 */
	@Test(description = "Test to get all connects by getConnections API")
	public void testGetAllConnections() {
		logger.info("Run connectionsData test case testGetAllConnections:");
		
		String getConnectionsWithMetricInfo = connTestServices.getJSONData("getConnectionsWithMetricInfo");
		JSONObject jsonString = JSONObject.fromObject(getConnectionsWithMetricInfo);
		connTestServices.replaceJsonObjectValue(jsonString, "userid", userid);

		JSONObject result = connTestServices.getConnections(jsonString.toString());

		ArrayList<String> ignoreKeyValues = new ArrayList();
		ignoreKeyValues.add("exceptionCaught");
		ignoreKeyValues.add("errorType");

		verifyResult(result, "getConnectionsWithMetricInfoExpected", ignoreKeyValues);
	}

	/**
	 * Test the Repository status whether it is true.
	 */
	@Test(description = "Test the Repository status")
	public void testRepositoryStatus() {
		logger.info("Run connectionsData test case testRepositoryStatus:");
		
		JSONObject result = connTestServices.testRepository();
		Assert.assertEquals(result.getJSONObject("ResponseData").getBoolean("isActive"), true,
				"The repositoyr's isActive is not true!");
	}

	private void verifyResult(JSONObject result, String expectedResult, ArrayList<String> ignoreKeyValues) {
		// Verify the result code
		String resultCode = result.getString("ResponseCode");
		Assert.assertEquals(resultCode, "200");

		// Verify the result response
		String resultResponse = result.getString("ResponseData");
		String resultExpected = connTestServices.getJSONData(expectedResult);
		Assert.assertEquals(JSONUtils.compareJsons(JSONObject.fromObject(resultExpected),
				JSONObject.fromObject(resultResponse), ignoreKeyValues, true), true);
	}
}