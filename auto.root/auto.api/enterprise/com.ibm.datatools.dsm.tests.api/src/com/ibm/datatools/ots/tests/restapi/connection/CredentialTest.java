package com.ibm.datatools.ots.tests.restapi.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.ConnectionTestService;

import net.sf.json.JSONObject;

public class CredentialTest extends ConnectionTestBase {
	private static final Logger logger = LogManager.getLogger(CredentialTest.class);

	private ConnectionTestService connTestServices = null;

	@BeforeClass
	public void beforeTest() {
		connTestServices = new ConnectionTestService();
		logger.info("Credential Test returned");
		connTestServices.testDatabaseConnection(databaseInfo);
		addDatabaseConnection();
	}

	@AfterClass
	public void afterTest() {
		connTestServices.removeConnection(dbProfile);
	}

	/**
	 * Test: after adding connection, get credential->set and test credential
	 * with true isTransient -> get credential again.
	 */
	@Test(description = "Test setting and testing the credential for a database with true isTransient, get credential")
	public void testSetTestAndGetCredentialWhenIsTransientTrue() {
		logger.info("Run test case testSetTestAndGetCredentialWhenIsTransientTrue:");
		
		connTestServices.removeCredential(dbProfile, "database");
		getCredentialBeforeSetCredential();

		// Set and Test the credential
		JSONObject result = connTestServices.setAndTestCredential(dbProfile, "database", user, password, "true");

		// Verify the result code
		String resultCode = result.getString("ResponseCode");
		Assert.assertEquals(resultCode, "200");

		// Verify the result content
		String resultContent = result.getString("ResponseData");
		String expectedResult = connTestServices.getXMLData("testSetAddTestCredentialExpected");
		connTestServices.verifyXMLResult(resultContent, expectedResult);

		// Get the credential again
		getCredentialAfterSetCredentialIsTransientTrue();
	}

	/**
	 * Test: after adding connection, get credential->set and test credential
	 * with false isTransient -> get credential again.
	 */
	@Test(description = "Test setting and testing the credential for a database with false isTransient, get credential")
	public void testSetTestAndGetCredentialWhenIsTransientFalse() {
		logger.info("Run test case testSetTestAndGetCredentialWhenIsTransientFalse:");
		
		connTestServices.removeCredential(dbProfile, "database");
		getCredentialBeforeSetCredential();

		// Set and Test the credential
		JSONObject result = connTestServices.setAndTestCredential(dbProfile, "database", user, password, "false");

		// Verify the result code
		String resultCode = result.getString("ResponseCode");
		Assert.assertEquals(resultCode, "200");

		// Verify the result content
		String resultContent = result.getString("ResponseData");
		String expectedResult = connTestServices.getXMLData("testSetAddTestCredentialExpected");
		connTestServices.verifyXMLResult(resultContent, expectedResult);

		// Get the credential again
		getCredentialAfterSetCredentialIsTransientFalse();
		
		
	}
	
	
	/**
	 * Test: after adding connection, get credential->set and test credential
	 * with false isTransient -> get credential again.
	 */
	@Test(description = "Test setting and testing the credential for a database with false isTransient, get then set wrong cred")
	public void testSetTestMonitorAndUserCred() {
		logger.info("Run test case testSetTestMonitorAndsetWrongCred:");
		
		connTestServices.removeCredential(dbProfile, "database");
		getCredentialBeforeSetCredential();

		// Set and Test the credential
		JSONObject result = connTestServices.setAndTestCredentialwithCredType(dbProfile, "database", user, password, "false","monitor");

		// Verify the result code
		String resultCode = result.getString("ResponseCode");
		Assert.assertEquals(resultCode, "200");

		// Verify the result content
		String resultContent = result.getString("ResponseData");
		String expectedResult = connTestServices.getXMLData("testSetAddTestCredentialExpected");
		connTestServices.verifyXMLResult(resultContent, expectedResult);

		// set user Cred
		
		result = connTestServices.setCredential(dbProfile,"dbProfile", user, password, "false");
		// Verify the result code
		resultCode = result.getString("ResponseCode");
		Assert.assertEquals(resultCode, "200");

		// Verify the result content
		resultContent = result.getString("ResponseData");
		expectedResult = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + "\n\n<Response>\n<Result>\n"
				+ "success" +"\n</Result>\n</Response>";
		connTestServices.verifyXMLResult(resultContent, expectedResult);
		
		//getSessCred
		result = connTestServices.getSessCredential(dbProfile, "database");
		// Verify the result code
		resultCode = result.getString("ResponseCode");
		Assert.assertEquals(resultCode, "200");
		
	}

	/**
	 * Test: get current user
	 */
	@Test(description = "Test get current user name")
	public void testGetCurrentUser() {
		logger.info("Run test case testGetCurrentUser:");
		// Set and Test the credential
		JSONObject result = connTestServices.getCurrUser();
		// Verify the result code
		String resultCode = result.getString("ResponseCode");
		Assert.assertEquals(resultCode, "200");
		// Verify the result content
		String resultContent = result.getString("ResponseData");
		String expectedResult = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + "\n\n<Response>\n<Result>\n"
						+ this.dsmUser +"\n</Result>\n</Response>";
		connTestServices.verifyXMLResult(resultContent, expectedResult);
	}

	private void getCredentialBeforeSetCredential() {
		// Get the credential
		JSONObject result = connTestServices.getCredential(dbProfile, "database");

		// Verify the result code
		String resultCode = result.getString("ResponseCode");
		Assert.assertEquals(resultCode, "200");

		// Verify the result content
		String resultContent = result.getString("ResponseData");
		String expectedResult = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + "<UserCredential>\n<Username>\n"
				+ null + "\n</Username>\n<Password>\n**********\n</Password>\n<isPersistent>\n"+"false\n</isPersistent>\n<isOneTime>\n"
				+"false\n</isOneTime>\n</UserCredential>\n";
		connTestServices.verifyXMLResult(resultContent, expectedResult);
	}

	private void getCredentialAfterSetCredentialIsTransientFalse() {
		// Get the credential again
		JSONObject result = connTestServices.getCredential(dbProfile, "database");

		// Verify the result code
		String resultCode = result.getString("ResponseCode");
		Assert.assertEquals(resultCode, "200");

		// Verify the result content
		String resultContent = result.getString("ResponseData");
		String expectedResult = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + "<UserCredential>\n<Username>\n"
				+ user + "\n</Username>\n<Password>\n**********\n</Password>\n<isPersistent>\n"+"true\n</isPersistent>\n<isOneTime>\n"
				+"false\n</isOneTime>\n</UserCredential>\n";
		
		
		connTestServices.verifyXMLResult(resultContent, expectedResult);
	}
	
	
	private void getCredentialAfterSetCredentialIsTransientTrue() {
		// Get the credential again
		JSONObject result = connTestServices.getCredential(dbProfile, "database");

		// Verify the result code
		String resultCode = result.getString("ResponseCode");
		Assert.assertEquals(resultCode, "200");

		// Verify the result content
		String resultContent = result.getString("ResponseData");
		String expectedResult = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + "<UserCredential>\n<Username>\n"
				+ user + "\n</Username>\n<Password>\n**********\n</Password>\n<isPersistent>\n"+"false\n</isPersistent>\n<isOneTime>\n"
				+"false\n</isOneTime>\n</UserCredential>\n";
		connTestServices.verifyXMLResult(resultContent, expectedResult);
	}

	private void addDatabaseConnection() {
		// If the db connection has existed, then delete this db connection
		connTestServices.removeConnection(dbProfile);

		// Get the connection data, and then replace some information with the
		// actual database info
		String addConnectionWithAdJDBCPro = connTestServices.getJSONData("addConnectionWithAdJDBCPro");
		String replacedAddConnectionWithAdJDBCPro = connTestServices.replaceConnectionData(addConnectionWithAdJDBCPro,
				dbProfile, databaseName, hostName, port, user, password);

		JSONObject result = connTestServices.addConnection(replacedAddConnectionWithAdJDBCPro);

		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");
	}
}
