package com.ibm.datatools.ots.tests.restapi.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.ConnectionTestService;

import net.sf.json.JSONObject;

public class EditConnectionTest extends ConnectionTestBase {
	private static final Logger logger = LogManager.getLogger(EditConnectionTest.class);
	
	private ConnectionTestService connTestServices = null ;
	private JSONObject resultAdd = null;
	private String addConnectionforEdit = null;
	
	@BeforeMethod
	public void beforeMethod() {
		connTestServices = new ConnectionTestService();
		logger.info("connection Test returned");
		if (!dbProfile.equals("INF_LUW_WRONG_INFO")) {
	//		connTestServices.testDatabaseConnection(databaseInfo);
		}
		
		connTestServices.removeConnection(dbProfile);
	}
	
	private void addConnectionforEdit()
	{
		connTestServices.removeConnection(dbProfile);

		if(dbProfile.contains("BIGSQL")==true){
			addConnectionforEdit = connTestServices.getJSONData("addConnectionforEditBI") ;
		}else{
			addConnectionforEdit = connTestServices.getJSONData("addConnectionforEdit") ;
		}

		JSONObject addConnectionforEditJson = JSONObject.fromObject(addConnectionforEdit);
		String profileJSONS = addConnectionforEditJson.getString("profileJSON");
		JSONObject profileJSON = JSONObject.fromObject(profileJSONS);
		profileJSON.put("name", dbProfile);
		addConnectionforEditJson.remove("profileJSON");
		addConnectionforEditJson.put("profileJSON", profileJSON);
		String addConnectionforEditwithNewprofilename=addConnectionforEditJson.toString();
		resultAdd = connTestServices.addConnection(addConnectionforEditwithNewprofilename) ;
	}

	/**
	 * For edit connection without test connection, then the wrong info also can be updated.
	 */
	@Test(description = "Test to add Connection then edit it without test condition")
	public void testEditConnectionwithWrongInfo() {
		logger.info("Run test case testEditConnectionwithWrongInfo:");
		
		addConnectionforEdit();
		
		//wrong databaseName
		String databaseName = "aaaa";
		
		// Replace information with the database info
		String editConnection = connTestServices.replaceConnectionData(
				addConnectionforEdit, dbProfile, databaseName, hostName, port, user, password);
		//edit connection
		JSONObject result = connTestServices.editConnection(editConnection);
		url = connTestServices.getURLFromConnectionData(editConnection);

		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");

		// Verify the result response
		String resultResponse = connTestServices.getResponseResultResponse(result);
		
		String editConnectionwithWronginfoExpected = connTestServices.getJSONData("editConnectionwithWronginfoExpected");
		verifyResponseResultWhenConnectDB(resultResponse, editConnectionwithWronginfoExpected);
	}

	/**
	 * For edit connection with test connection pass, then the correct info can be updated.
	 */
	@Test(description = "Test to add Connection then edit it with test condition")
	public void testEditConnectionwithTestConnection() {
		logger.info("Run test case testEditConnectionwithTestConnection:");
		
		addConnectionforEdit();
		
		// Replace information with the database info
		String editConnection = connTestServices.replaceConnectionData(
				addConnectionforEdit, dbProfile, databaseName, hostName, port, user, password);
		
		//run test connection to test edit info
		JSONObject resultTestConnection = connTestServices.testConnection(editConnection);
		String resultCodeTestConnection = connTestServices.getResponseResultCode(resultTestConnection);
		Assert.assertEquals(resultCodeTestConnection, "success");

		//edit connection
		JSONObject result = connTestServices.editConnection(editConnection);
		url = connTestServices.getURLFromConnectionData(editConnection);
		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");

		// Verify the result response
		String resultResponse = connTestServices.getResponseResultResponse(result);
		if(dbProfile.contains("BIGSQL")==true){
			String editConnectionwithTestConnectionExpectedBI = connTestServices.getJSONData("editConnectionwithTestConnectionExpectedBI");
			verifyResponseResultWhenConnectDB(resultResponse, editConnectionwithTestConnectionExpectedBI);

		}else{
			String editConnectionwithTestConnectionExpected = connTestServices.getJSONData("editConnectionwithTestConnectionExpected");
			verifyResponseResultWhenConnectDB(resultResponse, editConnectionwithTestConnectionExpected);

		}
			
	}

	/**
	 * Test to add Connection then edit it to only use Operation Credential.
	 */
	@Test(description = "Test to add Connection then edit it to use Operation Credential/Data Collection Credential")
	public void testEditConnectionToOperationOrDataCollectionCredential() {
		//Add a connection which only use Data Collection Credential
		addConnectionWithDataCollectionUser();
		
		//Edit the connection to only use Operation Credential
		logger.info("Run testEditConnectionToOperationCredential:");
		testEditConnection("editConnectionToOperationCredential");
		
		//Edit the connection to only use Data Collection Credential
		logger.info("Run testEditConnectionToDataCollectionCredential:");
		testEditConnection("editConnectionToDataCollectionCredential");
		
		//Edit the connection to only use Operation Credential and save to Repo DB
		logger.info("Run testEditConnectionToOperationCredentialSaveToRepoDB:");
		testEditConnection("editConnectionToOperationCredentialSaveToRepoDB");
		
		//Edit the connection to use Operation Credential and Data Collection Credential
		logger.info("Run testEditConnectionToTwoCredentials:");
		testEditConnection("editConnectionToTwoCredentials");
	}
	
	private void testEditConnection(String editConnectionProfileJson) {
		String editConnectionDraft = connTestServices.getJSONData(editConnectionProfileJson) ;

		// Replace information with the database info
		String editConnection = connTestServices.replaceConnectionData(
				editConnectionDraft, dbProfile, databaseName, hostName, port, user, password);

		// Edit connection
		JSONObject result = connTestServices.editConnection(editConnection);
		url = connTestServices.getURLFromConnectionData(editConnection);
		
		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");

		// Verify the result response
		String resultResponse = connTestServices.getResponseResultResponse(result);
		String editConnectionExpected = connTestServices.getJSONData(editConnectionProfileJson + "Expected");
		verifyResponseResultWhenConnectDB(resultResponse, editConnectionExpected);
	}
	
	private void addConnectionWithDataCollectionUser(){
		String addConnectionWithDataCollectionUser = connTestServices.getJSONData("addConnectionWithDataCollectionUser") ;
		String replacedSimpleConnection = connTestServices.replaceConnectionData(addConnectionWithDataCollectionUser,
				dbProfile, databaseName, hostName, port, user, password);
		
		// Get the URL info as the expected URL information
		url = connTestServices.getURLFromConnectionData(replacedSimpleConnection);
		
		JSONObject result = connTestServices.addConnectionNoShowProgress(replacedSimpleConnection) ;
		
		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");
	}
	
}
