package com.ibm.datatools.ots.tests.restapi.connection;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.ConnectionTestService;

import net.sf.json.JSONObject;

/**
 * 
 * @author ybjwang@cn.ibm.com
 *
 */
public class ExportConnectionTest extends ConnectionTestBase {

	private ConnectionTestService connTestServices = null;
	
	@BeforeClass
	public void beforeTest() {
		connTestServices = new ConnectionTestService();
		
	}

	@Test(description = "Test export connections to local file")
	public void testExportConnection2Local(){
		JSONObject resObj= connTestServices.exportConnection2Local(this.dbProfile);	

		String resultResponse = connTestServices.getExportConnResponseResultResponse(resObj);
		
		Assert.assertTrue(resultResponse.equalsIgnoreCase("SUCCESS"));
		
	}
	@Test(description = "Test export connections to server file")
	public void testExportConnection2Server(){
		JSONObject resObj= connTestServices.exportConnection2Server(this.dbProfile);	

		String resultResponse = connTestServices.getExportConnResponseResultResponse(resObj);
		
		String respone = "CLI20012I: The database connections were exported.";
		Assert.assertEquals(resultResponse, respone);
		
	}
}
