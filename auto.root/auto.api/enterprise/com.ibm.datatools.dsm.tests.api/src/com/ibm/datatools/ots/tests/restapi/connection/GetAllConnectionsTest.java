package com.ibm.datatools.ots.tests.restapi.connection;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.ConnectionTestService;
import com.ibm.datatools.test.utils.JSONUtils;

import net.sf.json.JSONObject;

public class GetAllConnectionsTest extends ConnectionTestBase {
	private static final Logger logger = LogManager.getLogger(GetAllConnectionsTest.class);
	private ConnectionTestService connTestServices = null;

	@BeforeClass
	public void beforeTest(){
		connTestServices = new ConnectionTestService();
		logger.info("connection Test returned -- GetAllConnectionsTest");
		//connTestServices.keepRepositoryDBInfo();
		connTestServices.switchRepoDB("connectionDataTest");
	}

	@AfterClass
	public void afterTest() {
		/*
		if (connTestServices.switchToOrignialRepDB() == false) {
			connTestServices.switchRepoDB("personalRepDB");
		}
		*/
		connTestServices.switchToConfigRepDB();
	}

	
	
	/**
	 * Test to get all connections which the version is greater than 10.
	 */
	@Test(description = "Test to get all connection profiles with dbmgt/dbmgr.do getAllProfiles API")
	public void testGetAllConnectionsProfilesWithJsonResultVersionGT10() {
		logger.info("Run GetAllConnectionsTest test case testGetAllConnectionsProfilesWithJsonResultVersionGT10:");

		JSONObject result = connTestServices.getAllConnectionProfilesWithJsonResult("DSWEB.IS_DB_USER", "10.0");
		verifyResponseResult(result, "getAllConnectionProfilesWithJsonResultVersionGT10Expected", null, false);
	}
	
	
	@Test(description = "Test to get all connections with dbmgt/dbmgr.do getAllProfiles API")
	public void testGetAllProfilesWithTagsIsDBUser() {
		logger.info("Run getAllProfiles test case: testGetAllProfilesWithTagsIsDBUser");
		String matchPrivilege="DSWEB.IS_DB_USER";
		String  includeTAGS="true";
		JSONObject result = connTestServices.getAllProfiles(matchPrivilege,includeTAGS,"");
		String resCode = result.get("resultCode").toString();
		Assert.assertEquals(resCode, "success");
		String getAllConnectionsWithTagsExpected = connTestServices.getJSONData("getAllConnectionWithTagsResultExpected");
		JSONObject expectedResult = JSONObject.fromObject(getAllConnectionsWithTagsExpected);
		ArrayList<String> key = new ArrayList();
		key.add("Timezone");
		key.add(".dsrcprop.");
		key.add("lastUpdatedTimeStamp");	
		String resultResponse = result.getString("response");	
		Assert.assertTrue(JSONUtils.compareJsons(expectedResult, JSONObject.fromObject(resultResponse), key));
	}
	
	
	
	@Test(description = "Test to get all connections with dbmgt/dbmgr.do getAllProfiles API")
	public void testGetAllProfilesWithTagsIsDBOwner() {
		logger.info("Run getAllProfiles test case: getAllProfilesWithTagsIsDBOwner");
		
		String matchPrivilege="DSWEB.IS_DB_OWNER";
		String  includeTAGS="true";
		JSONObject result = connTestServices.getAllProfiles(matchPrivilege,includeTAGS,"");
		String resCode = result.get("resultCode").toString();
		Assert.assertEquals(resCode, "success");
		String getAllConnectionsWithTagsExpected = connTestServices.getJSONData("getAllConnectionWithTagsResultExpected");
		JSONObject expectedResult = JSONObject.fromObject(getAllConnectionsWithTagsExpected);
		ArrayList<String> key = new ArrayList();
		key.add("Timezone");
		key.add(".dsrcprop.");
		key.add("lastUpdatedTimeStamp");	
		String resultResponse = result.getString("response");	
		Assert.assertTrue(JSONUtils.compareJsons(expectedResult, JSONObject.fromObject(resultResponse), key));
	}
	
	
	@Test(description = "Test to get all connections with dbmgt/dbmgr.do getAllProfiles API")
	public void testGetAllProfilesWithTagsIsDBUserIsUSerCred() {
		logger.info("Run getAllProfiles test case: testGetAllProfilesWithTagsIsDBUser");
		
		String matchPrivilege="DSWEB.IS_DB_USER";
		String includeTAGS="true";
		String  includeUsrCred="true";
		JSONObject result = connTestServices.getAllProfiles(matchPrivilege,includeTAGS,includeUsrCred);
		String resCode = result.get("resultCode").toString();
		Assert.assertEquals(resCode, "success");
		String getAllConnectionsWithTagsExpected = connTestServices.getJSONData("getAllConnectionWithTagsUserCredResultExpected");
		JSONObject expectedResult = JSONObject.fromObject(getAllConnectionsWithTagsExpected);
		ArrayList<String> key = new ArrayList();
		key.add("Timezone");
		key.add(".dsrcprop.");
		key.add("lastUpdatedTimeStamp");	
		String resultResponse = result.getString("response");	
		Assert.assertTrue(JSONUtils.compareJsons(expectedResult, JSONObject.fromObject(resultResponse), key));
	}
	
	
	@Test(description = "Test to get all connections with dbmgt/dbmgr.do getAllProfiles API")
	public void testGetAllProfilesWithTags() {
		logger.info("Run getAllProfiles test case: testGetAllProfilesWithTags");

		String matchPrivilege="";
		String  includeTAGS="true";
		
		JSONObject result = connTestServices.getAllProfiles(matchPrivilege,includeTAGS,"");
		String resCode = result.get("resultCode").toString();
		Assert.assertEquals(resCode, "success");
		String getAllConnectionsWithTagsExpected = connTestServices.getJSONData("getAllConnectionWithTagsResultExpected");
		JSONObject expectedResult = JSONObject.fromObject(getAllConnectionsWithTagsExpected);
		ArrayList<String> key = new ArrayList();
		key.add("Timezone");
		key.add(".dsrcprop.");
		key.add("lastUpdatedTimeStamp");
		
		String resultResponse = result.getString("response");	
		Assert.assertTrue(JSONUtils.compareJsons(expectedResult, JSONObject.fromObject(resultResponse), key));
	}
	
	@Test(description = "Test to get all connections with dbmgt/dbmgr.do UpdateDatabaseVersionVRMF API")
	public void testUpdateDatabaseVersionVRMF() {
		logger.info("Run UpdateDatabaseVersionVRMF test case: UpdateDatabaseVersionVRMF");
		String profileName="conn3";
		JSONObject result = connTestServices.updateDatabaseVersionVRMF(profileName);
		verifyResult(result,"updateDatabaseVersionVRMFExpected",null,false);	
	}
	
	@Test(description = "Test to get all connections with dbmgt/dbmgr.do UpdateDatabaseVersionVRMF API")
	public void testUpdateDatabaseVersion() {
		logger.info("Run UpdateDatabaseVersion test case: UpdateDatabaseVersion");
		String profileName="conn3";
		JSONObject result = connTestServices.updateDatabaseVersion(profileName);
		verifyResult(result,"updateDatabaseVersionExpected",null,false);	
	}
	

	/**
	 * Test to get all connections' names which contains some monitoring connections
	 * and some blackout connections and so on.
	 */
	@Test(description = "Test to get all connection profile names")
	public void testGetAllConnectionsProfileNames() {
		logger.info("Run GetAllConnectionsTest test case testGetAllConnectionsProfileNames:");

		JSONObject result = connTestServices.getAllConnectionProfileName();
		
		// Verify the result code
		String resultCode = result.getString("ResponseCode");
		Assert.assertEquals(resultCode, "200");

		// Verify the result response
		String resultResponse = result.getString("ResponseData");
		String response = JSONObject.fromObject(resultResponse).getString("response");
		String resultExpected = "[{\"profileName\":\"conn1\"},{\"profileName\":\"conn2\"},{\"profileName\":\"conn3\"},{\"profileName\":\"conn4\"},{\"profileName\":\"conn5\"},{\"profileName\":\"conn6\"},{\"profileName\":\"conn7\"},{\"profileName\":\"zostest\"}]";
		Assert.assertEquals(response,resultExpected, "The actual result is not the same as the expected result. The actual result is:"+response + "/nThe expected result is:" + resultExpected);
	}

	@Test(description = "Test to batch assign tags Favorite for profile conn2 and conn3")
	public void testBatchAssignTags() {
		logger.info("Run batch assign tags for profiles test case: testBatchAssignTags");
		String assignTags = "["+connTestServices
				.getJSONData("assignTags")+"]";
		JSONObject result = connTestServices.batchAssignTags(assignTags,"[]");
		
		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("message"), "");
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("response"), "\"\"");

	
	}
	private void verifyResult(JSONObject result, String expectedResult, ArrayList<String> ignoreKeyValues, Boolean ignoreNumValues) {
		// Verify the result code
		String resultCode = result.getString("ResponseCode");
		Assert.assertEquals(resultCode, "200");

		// Verify the result response
		String resultResponse = result.getString("ResponseData");
		String resultExpected = connTestServices.getJSONData(expectedResult);
		List<String> arrayKeyList = new ArrayList<String>();
		Assert.assertEquals(JSONUtils.compareJsons(JSONObject.fromObject(resultExpected),
				JSONObject.fromObject(resultResponse), ignoreKeyValues, ignoreNumValues), true);
	}
	
	private void verifyResponseResult(JSONObject result, String expectedResult, ArrayList<String> ignoreKeyValues, Boolean ignoreNumValues) {
		// Verify the result code
		String resultCode = result.getString("ResponseCode");
		Assert.assertEquals(resultCode, "200");

		// Verify the result response
		String resultResponse = result.getString("ResponseData");
		String response = JSONObject.fromObject(resultResponse).getString("response");
		String resultExpected = connTestServices.getJSONData(expectedResult);
		Assert.assertEquals(JSONUtils.compareJsons(JSONObject.fromObject(resultExpected),
				JSONObject.fromObject(response), ignoreKeyValues, ignoreNumValues), true);
	}
}
