package com.ibm.datatools.ots.tests.restapi.connection;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.ConnectionTestService;

import net.sf.json.JSONObject;

/**
 * 
 * @author ybjwang@cn.ibm.com
 *
 */
public class ImportConnectionTest extends ConnectionTestBase {

	private ConnectionTestService connTestServices = null;
	
	@BeforeClass
	public void beforeTest() {
		connTestServices = new ConnectionTestService();
		
	}
	/**
 * Test import connections from server/local file
 */
	@Test(description = "Test import connections from server file")
	public void testImportConnectionFromFile(){
		JSONObject resObj= connTestServices.importConnectionsFromFile();	

		String resultResponse = connTestServices.getImportConnResponseResultData(resObj);
		
		Assert.assertTrue(resultResponse.equalsIgnoreCase("SUCCESS"));
		
	}
	/**
	 * Test import connections from url
	 * The reponse data is preview
	 */
		@Test(description = "Test import connections from server file")
		public void testImportConnectionFromURL(){
			JSONObject resObj= connTestServices.importConnectionsFromURL();	

			String resultResponse = connTestServices.getImportFromURLResponseData(resObj);
			
			Assert.assertTrue(resultResponse.equalsIgnoreCase("preview"));
			
		}

}
