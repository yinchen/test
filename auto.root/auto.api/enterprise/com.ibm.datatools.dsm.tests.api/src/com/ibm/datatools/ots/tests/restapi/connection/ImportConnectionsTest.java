package com.ibm.datatools.ots.tests.restapi.connection;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.common.RestAPIBaseTest;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class ImportConnectionsTest {
	
	TuningCommandService cs;
	Properties p;
	
	@BeforeTest
	public void beforeTest() throws FileNotFoundException, IOException {
		cs = new TuningCommandService();
		RestAPIBaseTest.loginDSM();
	}
	
	@Test
	public void importDBConnections() throws InterruptedException{
		
		String connectionsFile = Configuration.getProperty("import.connections.platform");
		System.out.println("Import connections file is : "+connectionsFile);
		
		//delete all connections in DSM connection list
		//when do test for migration_luw,do NOT delete 
		
		if(!connectionsFile.equals("migration_luw.csv")){
			JSONObject connsObj = cs.getAllConns();
			String data = connsObj.getString("data");
			String subData = data.substring(1, data.length()-1);
			JSONArray connsArray = JSONArray.fromObject(subData);
			int connsBeforeImport = connsArray.size() -1;
			System.out.println("There are "+connsBeforeImport+" connections before import");
			
			for(int i=1;i<connsArray.size();i++){
				JSONObject conn = JSONObject.fromObject(connsArray.get(i));
				String dbProfileName = conn.getString("name");
				JSONObject delResult = cs.delConnections(dbProfileName);
				String resultCode = delResult.getString("resultCode");
				if(resultCode.equals("success")){
					System.out.println("Delete connections [" +dbProfileName +"] succeed");
				}else {
					System.out.println("Delete connections [" +dbProfileName +"] failed");
				}
			}
		}
		
		/**
		 * Upload dbconnections file to server
		 */
		String connectionFileName = cs.uploadFile(connectionsFile);
		
		/**
		 * Init import
		 */
		boolean isInitImportSuccess = false;
		int initImportTimes = 0;
		int maxInitTimes = 3;
		while(!isInitImportSuccess){
			initImportTimes++;
			JSONObject initImportResult = cs.initImportDBConnections(connectionFileName);
			System.out.println(initImportResult);
			if("success".equals(initImportResult.getString("code"))){
				isInitImportSuccess = true;
			}
			if(initImportTimes > maxInitTimes){
				Assert.fail("Init import connection for "+initImportTimes+",but still failed");
			}
		}
		System.out.println("Init import times : "+initImportTimes);
		
		/**
		 * Import
		 */
		
		boolean isImportSuccess = false;
		int importTimes = 0;
		int maxImportTimes = 3;
		while(!isImportSuccess){
			importTimes++;
			JSONObject importResult = cs.startImport(connectionFileName);
			System.out.println("Import result "+importResult);
			if("success".equals(importResult.getString("code"))){
				isImportSuccess = true;
			}
			
			if(importTimes > maxImportTimes){
				Assert.fail("Import connection for "+importTimes+",but still failed");
			}
			
		}
		
		System.out.println("Import times : "+importTimes);
		
		JSONObject afterImport = cs.getAllConns();
		String afterImportData = afterImport.getString("data");
		String subDataAfter = afterImportData.substring(1, afterImportData.length()-1);
		JSONArray connsArrayAfterImport = JSONArray.fromObject(subDataAfter);
		int connsAfterImport = connsArrayAfterImport.size() -1;
		System.out.println("There are "+connsAfterImport+" connections after import");
		
		//wait 20 seconds for db information synchronization
		Thread.sleep(20000L);
		
		//retrieve db version
		Map<String, String> dbMap = new HashMap<String,String>();
		Map<String, String> emptyVersionDB = new HashMap<String,String>();
		for(int i=1; i<connsArrayAfterImport.size(); i++){
			JSONObject item = connsArrayAfterImport.getJSONObject(i);
			String dbType = item.getString("dataServerExternalType");
			String dbName = null;
			if(dbType.equals("DB2Z")){
				dbName = item.getString("location");
			}
			
			dbName = item.getString("name");
			String dbVersion = item.getString("databaseVersion");
			if(dbVersion.isEmpty()){
				emptyVersionDB.put(dbName, dbVersion);
			}
			dbMap.put(dbName, dbVersion);
		}
		
		System.out.println(dbMap);
		System.out.println(emptyVersionDB);
		
		for(Map.Entry<String, String> entry : emptyVersionDB.entrySet()){
			String dbName = entry.getKey();
			JSONObject retrieveResult = cs.retrieveDBVersion(dbName);
			String db2License = retrieveResult.getString("db2license");
			System.out.println("Retrieve db version of ["+dbName+"] is " +db2License);
		}
		
		
	}
}
	





