package com.ibm.datatools.ots.tests.restapi.connection;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.ConnectionTestService;

import net.sf.json.JSONObject;
/**
 * 
 * @author ybjwang@cn.ibm.com
 *
 */
public class MigrateHADRConnectionTest extends ConnectionTestBase {

	private ConnectionTestService connTestServices = null;
	private String connectionJson= "addMigrateHADRConnectionWithJDBCPro";
	private String migrateCommand ="";
	private String addConnectionforEdit = null;
	private String editConnectionString="";
	private String databaseName = "puredb";
	private String name="testMigrate";

	
	@BeforeClass
	public void beforeTest() {
		
		connTestServices = new ConnectionTestService();
		//if the connection exists.
		connTestServices.removeConnection(name);
		//add a connection
		//edit this connection to an HADR environment
		String newConnection = connTestServices.getJSONData(connectionJson) ;
		JSONObject result  = connTestServices.addConnection(newConnection);
		// Verify the result code
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertEquals(resultCode, "success");
		editConnectionInfo();
		
	}
	private void editConnectionInfo(){
		addConnectionforEdit = connTestServices.getJSONData(connectionJson) ;
		
		
		editConnectionString = connTestServices.replaceConnectionData(
				addConnectionforEdit, name,databaseName, hostName, port, user, password);
		
		JSONObject result = connTestServices.editConnection(editConnectionString);
		String resultCode = connTestServices.getResponseResultCode(result);
		Assert.assertTrue(resultCode.equalsIgnoreCase("SUCCESS"));
	}
	@Test(description= "Test Migrate")
	public void testMigrateHADRConnection(){
		connTestServices.migrateHADRConnection(editConnectionString,name);
	}
	@AfterClass
	public void afterTest(){
		//delete this connection
		connTestServices.removeConnection(name);
	}
	
	
	
}
