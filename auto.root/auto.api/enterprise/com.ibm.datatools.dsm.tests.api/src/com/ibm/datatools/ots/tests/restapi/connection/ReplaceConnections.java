package com.ibm.datatools.ots.tests.restapi.connection;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.RestAPIBaseTest;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;

import net.sf.json.JSONObject;

public class ReplaceConnections {
	
	private String dbProfileName = null;
	private String dbName = null;
	private String host = null;
	private String port = null;
	private String username = null;
	private String password = null;

	TuningCommandService cs;
	Properties p;
	private final Logger logger = LogManager.getLogger(getClass());
	
	@BeforeTest
	@Parameters({"dbProfileName","dbName","host","port","username","password"})
	public void beforeTest(String dbProfileName,String dbName,String host,String port,String username,String password) throws FileNotFoundException, IOException {
		
		this.dbProfileName = dbProfileName;
		this.dbName = dbName;
		this.host = host;
		this.port = port;
		this.username = username;
		this.password = password;
		
		cs = new TuningCommandService();
		RestAPIBaseTest.loginDSM();
	}
	
	@Test
	public void replaceConns(){
		JSONObject delObj = cs.delConnections(dbProfileName);
		System.out.println("Delete connection result : "+delObj);
		
		String resultCode = delObj.getString("resultCode");
		if(resultCode.equals("success")){
			System.out.println("Delete connection ["+ dbProfileName +"] succeed");
		}else {
			System.out.println("Delete connection ["+ dbProfileName +"] failed");
		}
		
		logger.info("Add Connections For BVT Test...");
		
		String database = cs.listDatabase();
		if(database.contains(dbProfileName)){
			System.out.println("MESSAGE:The connection with same name ["+dbProfileName+"] already exist, please use an another name");
			return;
		}
		
		JSONObject result = cs.addConnection(enableAllDesc(dbProfileName, dbName, host, port, username, password));
		String responseCode = (String) result.get("resultCode");
		if("failure".equals(responseCode)){
			Assert.fail("Add database connection ["+dbProfileName+"] failed...");
		}else {
			Assert.assertEquals(result.getString("resultCode"), "success");
		}
	}
	
	
	public Properties enableAllDesc(String dbProfileName,String dbName,String host,String port,String user,
			String password){
		Properties desc = new Properties();
		desc.put("name", dbProfileName);
		desc.put("dataServerExternalType", "DB2LUW");
		desc.put("databaseName", dbName);
		desc.put("host", host);
		desc.put("port", port);
		desc.put("disableDataCollection", false);
		desc.put("securityMechanism", "3");
		desc.put("operationUser", user);
		desc.put("operationPassword", password);
		desc.put("onetimePasswordEnable", false);
		desc.put("saveOperationCredentials", true);
		desc.put("user", user);
		desc.put("password", password);
		desc.put("URL", "jdbc:db2://"+host+":"+port+"/"+dbName+":retrieveMessagesFromServerOnGetMessage=true;securityMechanism=3;");
		desc.put("creator","admin");
		desc.put("dataServerType", "DB2LUW");
		return desc;
	}
	
}
