package com.ibm.datatools.ots.tests.restapi.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.ConnectionTestService;

import net.sf.json.JSONObject;
import net.sf.json.xml.XMLSerializer;

public class RepositoryDBTest extends ConnectionTestBase {
	private static final Logger logger = LogManager.getLogger(RepositoryDBTest.class);

	private ConnectionTestService connTestServices = null;

	@BeforeClass
	public void beforeTest() {
		connTestServices = new ConnectionTestService();
		logger.info("connection Test returned -- RepositoryDBTest");
		connTestServices.keepRepositoryDBInfo();
	}

	@AfterClass
	public void afterTest() {
		if (connTestServices.switchToOrignialRepDB() == false) {
			connTestServices.switchRepoDB("personalRepDB");
		}
	}

	/**
	 * Test: edit the current repository db to a new db with 'Clear text
	 * password' JDBC security -> get the repository db
	 */
	@Test(description = "Edit and get the repository db with 'Clear text password' JDBC security")
	public void editAndGetRepDBWithClearTextPassword() {
		logger.info("Run test case editAndGetRepDBWithClearTextPassword:");
		
		connTestServices.switchRepoDB("connectionDataTest");
		JSONObject result = connTestServices.getRepDBInfo();
		String responseData = result.getString("ResponseData");
		XMLSerializer xmlSerializer = new XMLSerializer();
		JSONObject responseDataJson = (JSONObject) xmlSerializer.read(responseData);

		String expectedResult = connTestServices.getXMLData("getRepDBExpected");
		JSONObject expectedResultJson = (JSONObject) xmlSerializer.read(expectedResult);

		connTestServices.verifyResultEditOrGetRepDB(expectedResultJson, responseDataJson, "connectionDataTest");
	}
}
