package com.ibm.datatools.ots.tests.restapi.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.ConnectionTestService;

import net.sf.json.JSONObject;

public class SecurityTest extends ConnectionTestBase {
	private static final Logger logger = LogManager.getLogger(SecurityTest.class);

	private ConnectionTestService connTestServices = null;
	private JSONObject resultAdd = null;
	private String addConnectionforEdit = null;
	
	@BeforeClass
	public void beforeTest() {
		connTestServices = new ConnectionTestService();
		logger.info("Security Test start");
		connTestServices.delDSMUser(dsmNewUserName);
		connTestServices.removeConnection(dbProfile);
	}

	@AfterClass
	public void afterTest() {
		connTestServices.delDSMUser(user);
	}

	
	
	private void addConnectionforEdit()
	{
		connTestServices.removeConnection(dbProfile);

		if(dbProfile.contains("BIGSQL")==true){
			addConnectionforEdit = connTestServices.getJSONData("addConnectionforEditBI") ;
		}else{
			addConnectionforEdit = connTestServices.getJSONData("addConnectionforEdit") ;
		}

		JSONObject addConnectionforEditJson = JSONObject.fromObject(addConnectionforEdit);
		String profileJSONS = addConnectionforEditJson.getString("profileJSON");
		JSONObject profileJSON = JSONObject.fromObject(profileJSONS);
		profileJSON.put("name", dbProfile);
		addConnectionforEditJson.remove("profileJSON");
		addConnectionforEditJson.put("profileJSON", profileJSON);
		String addConnectionforEditwithNewprofilename=addConnectionforEditJson.toString();
		resultAdd = connTestServices.addConnection(addConnectionforEditwithNewprofilename) ;
	}

	/**
	 * Test: after admin user and then delete it
	 * 
	 */
	@Test(description = "Add admin  user and then delete it")
	public void testAddGetUSerRoleandDeleteDSMUser() {
		logger.info("Run test case test add a DSM user and then delete it: testAddandDeleteDSMUser");
		
		String userPrivileges = connTestServices.getJSONData("addDsmUserAdmin");
		
		// Set and Test the credential
		JSONObject result = connTestServices.addDSMUser(dsmNewUserName, dsmNewUserPassword, userPrivileges);

		
		// Verify the add user result code
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("resultcode"), "success");
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("message"), "");
		
		//get the role of user
		
		result = connTestServices.getUser(dsmNewUserName);
		
		// Verify the delete user result code
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("resultcode"), "success");
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("message"), "");	
		
		//Delete  dsm  user
		result = connTestServices.delDSMUser(dsmNewUserName);
		
		// Verify the delete user result code
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("resultcode"), "success");
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("message"), "");		
		
	}
	
	
	/**
	 * Test:Add normal user, and the edit it as admin user,  finally delete it
	 * 
	 */
	@Test(description = "Add normal user, and the edit it as admin user")
	public void testAddEditUserRole() {
		logger.info("Run test case test add a DSM user,change user's role as admin: testAddEditUserRole");
		
		String userPrivileges = connTestServices.getJSONData("addDsmUser");
		
		// Add a normal dsm user
		JSONObject result = connTestServices.addDSMUser(dsmNewUserName, dsmNewUserPassword, userPrivileges);

		
		// Verify the add user result code
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("resultcode"), "success");
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("message"), "");
		
		
		//Edit a normal user as a admin user
		String AdminPrivileges = connTestServices.getJSONData("addDsmUserAdmin");
		result = connTestServices.editDSMUser(dsmNewUserName, dsmNewUserPassword, AdminPrivileges);
		
		// Verify the edit user result code
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("resultcode"), "success");
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("message"), "");		
				
		result = connTestServices.delDSMUser(dsmNewUserName);
			
	}
	
	
	/**
	 * Test: First Add a DB connection, and then  add a user, and give the user for DB privilege
	 * 
	 * 
	 */
	@Test(description = " Add a DB connection, and then  add a user, and give the user for DB privilege")
	public void testAddEditandGetUserDBPrivilege() {
		logger.info("Run test case to add a DSM user, and give the user for DB privilege: testAddEditUserDBPriviege");
		
		addConnectionforEdit();
	
		// Add a normal dsm user
		String userPrivileges = connTestServices.getJSONData("addDsmUser");
		JSONObject result = connTestServices.addDSMUser(dsmNewUserName, dsmNewUserPassword, userPrivileges);

		
		// Verify the add user result code
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("resultcode"), "success");
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("message"), "");
		
		
		//Edit user's db privilege
		String userDBPrivileges = connTestServices.getJSONData("editUserDBPrivilege");
		result = connTestServices.editDSMUserDBPrivilege(dsmNewUserName, dbProfile, userDBPrivileges);
		
		// Verify the edit user result code
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("resultcode"), "success");
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("message"), "");		
		
		
		// Get the implicit privilege of a user,
		result=connTestServices.getImplicitDBPrivileges(dsmNewUserName, dbProfile);
		
		// Verify the  result for  getImplicitDBPrivileges
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("resultcode"), "success");
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("message"), "");		
				
		//Delete dsm user
		result = connTestServices.delDSMUser(dsmNewUserName);;
		
		// Verify the delete user result code
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("resultcode"), "success");
		Assert.assertEquals(result.getJSONObject("ResponseData").getString("message"), "");		
		
	}

	
}
