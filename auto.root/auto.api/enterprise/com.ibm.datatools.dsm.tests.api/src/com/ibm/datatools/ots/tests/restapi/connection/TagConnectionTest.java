package com.ibm.datatools.ots.tests.restapi.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.ConnectionTestService;
import com.ibm.datatools.ots.tests.restapi.common.RestAPIBaseTest;
import com.ibm.datatools.ots.tests.restapi.common.TagManagedService;

import net.sf.json.JSONObject;

public class TagConnectionTest  extends ConnectionTestBase {
	private static final Logger logger = LogManager.getLogger(TagConnectionTest.class);

	private ConnectionTestService connTestServices = null;
	private TagManagedService tagManagedService = null;

	@BeforeClass
	public void beforeTest(){
		connTestServices = new ConnectionTestService();
		tagManagedService = new TagManagedService();
		logger.info("connection Test returned -- TagConnectionTest");
		
		//Before run test cases, test connection. If return success, then start to run test cases. Or skip.
		connTestServices.testDatabaseConnection(databaseInfo);
		
		//If can not login DSM, it will skip the following testing
		RestAPIBaseTest.loginDSM();
		addASampleConnection();
		deleteTags();
		addTags();
	}

	@AfterClass
	public void afterTest() {
		connTestServices.removeConnection(dbProfile);
		deleteTags();
	}

	@BeforeMethod
	public void beforeMethod() {
		// If the db connection has existed, then delete this db connection
		try {
			logger.info("Sleep 5s ...");
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			logger.error("Sleep 5s failed!");
			e.printStackTrace();
		}
		connTestServices.removeConnection(dbProfile);
	}
	/**
	 * Test to assign a tag to a connection
	 * */
	@Test(description="Test assign a tag to a connection")
	public void testAssignATagToAConnection(){
		logger.info("Run test case testAssignATagToAConnection:");
		String assignTags = "[{\"name\":\"publicTag1\",\"connections\":[\"" + dbProfile + "\"]}]";
		
		JSONObject result = connTestServices.assignTagsToConnections(assignTags);

		//Verify
		String resultCode = connTestServices.getResponseResultCode(result) ;
		Assert.assertEquals(resultCode, "success");
	}
	
	private void addASampleConnection(){
		//add a connection with given profielName
		String simpleConnection = connTestServices.getJSONData("addSimpleConnection") ;
		String replacedSimpleConnection = connTestServices.replaceConnectionData(simpleConnection,
				dbProfile, databaseName, hostName, port, user, password);
		
		// Get the URL info as the expected URL information
		url = connTestServices.getURLFromConnectionData(replacedSimpleConnection);
		
		JSONObject result = connTestServices.addConnection(replacedSimpleConnection) ;

		//Verify
		String resultCode = connTestServices.getResponseResultCode(result) ;
		
		if("failure".equalsIgnoreCase(resultCode)){
			String message = connTestServices.getResponseResultMessage(result) ;
		
			if(!message.startsWith("DSW03101E")){
				//Because this connectionProfile already exists, this does not mean this case failed.
				Assert.fail(message);
			}
			System.out.println(message + "testAddDeleteConnection" );
		}
	}
	
	
	/**
	 * Add a public manual tag
	 * */
	private void addTags(){
		String addAPublicTag = tagManagedService.getJSONData("addAPublicTag") ;		
		JSONObject result = tagManagedService.batchUpdateTags(addAPublicTag);

		//Verify
		String resultCode = tagManagedService.getResponseResultCode(result) ;
		Assert.assertEquals(resultCode, "success");
	}
	
	/**
	 * Delete a public manual tag
	 * */
	private void deleteTags(){
		String deleteAPublicTag = tagManagedService.getJSONData("deleteAPublicTag") ;
		tagManagedService.batchUpdateTagsForDeleteForce(deleteAPublicTag);
	}
}
