package com.ibm.datatools.ots.tests.restapi.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.RestAPIBaseTest;
import com.ibm.datatools.ots.tests.restapi.common.TagManagedService;

import net.sf.json.JSONObject;


public class TagManagedTest {
	private static final Logger logger = LogManager.getLogger(ConnectionTest.class);

	private TagManagedService tagManagedServices = null;

	@BeforeClass
	public void beforeTest(){
		tagManagedServices = new TagManagedService();
		logger.info("Tag Test returned -- TagManagedTest");
		
		//If can not login DSM, it will skip the following testing
		RestAPIBaseTest.loginDSM();
	}

	@AfterClass
	public void afterTest() {
	}

	@BeforeMethod
	public void beforeMethod() {
	}
	
	/**
	 * Test to add a public manual tag
	 * */
	@Test(description="Test to add a public manual tag")
	public void testAddAPublicManualTag(){
		logger.info("Run test case testAddAPublicManualTag:");
		
		String addAPublicTag = tagManagedServices.getJSONData("addAPublicTag") ;
		
		JSONObject result = tagManagedServices.batchUpdateTags(addAPublicTag);

		//Verify
		String resultCode = tagManagedServices.getResponseResultCode(result) ;
		Assert.assertEquals(resultCode, "success");
	}
	
	/**
	 * Test to delete a public manual tag
	 * */
	@Test(description="Test to delete a public manual tag")
	public void testDeleteAPublicManualTag(){
		logger.info("Run test case testDeleteAPublicManualTag:");
		
		String deleteAPublicTag = tagManagedServices.getJSONData("deleteAPublicTag") ;
		
		JSONObject result = tagManagedServices.batchUpdateTags(deleteAPublicTag);
		
		//Verify
		String resultCode = tagManagedServices.getResponseResultCode(result) ;
		Assert.assertEquals(resultCode, "success");
	}
}
