package com.ibm.datatools.ots.tests.restapi.connection;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckMonitorDB;
import com.ibm.datatools.ots.tests.restapi.common.RestAPIBaseTest;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;

public class TestActivateLicense {
	TuningCommandService cs;
	Properties p;
	
	String dbName = null;
	
	@Parameters("dbProfile")
	@BeforeTest
	public void beforeTest(String monitorDBName) throws FileNotFoundException, IOException {
		cs = new TuningCommandService();
		RestAPIBaseTest.loginDSM();
		
		dbName = monitorDBName;
		Assert.assertEquals(CheckMonitorDB.checkMonitorDBStatus(dbName), "success");
	}
	
	
	
	@Test
	public void testActivateLicense(){
		
		JSONObject monDBDetails = cs.getMonitorDBDetails(dbName);
		if(monDBDetails == null){
			String errorMessage = ""+dbName+" do not exist in DB connection list";
			Assert.fail(errorMessage);
		}else{
			String response = monDBDetails.getString("response");
			JSONObject json = JSONObject.fromObject(response);
			String dataServerType = json.getString("dataServerType");
			if("DB2LUW".equals(dataServerType)){
				return;
			}
		}
		
		
		JSONObject result = cs.ACTIVATE_LICENSE(dbName);
		
		if(result == null){
			String errorMessage = "ERROR MESSAGE: ACTIVATE_LICENSE result is null";
			System.out.println(errorMessage);
			Assert.fail(errorMessage);
		}else {
			String activateResult = result.getString("result");
			String alreadyActed = result.getString("alreadyActed");
			if("true".equals(activateResult) && "no".equals(alreadyActed)){
				String message = "License of DB "+dbName+" successfully activated";
				System.out.println("MESSAGE : "+message);
				Assert.assertEquals("true", activateResult, message);
			}else if("false".equals(activateResult) && "yes".equals(alreadyActed)){
				String message = "License of DB "+dbName+" is already activated";
				System.out.println("MESSAGE : "+message);
				Assert.assertEquals("false", activateResult, message);
			}
		}
		
	}
}



















