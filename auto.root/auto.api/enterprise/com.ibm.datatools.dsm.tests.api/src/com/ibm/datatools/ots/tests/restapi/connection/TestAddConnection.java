package com.ibm.datatools.ots.tests.restapi.connection;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.ConnectionTestService;
import com.ibm.datatools.ots.tests.restapi.common.RestAPIBaseTest;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.test.utils.FileTools;
public class TestAddConnection {

	private final Logger logger = LogManager.getLogger(getClass());
	ConnectionTestService ct;
	TuningCommandService cs;
	Properties p;
	
	
	@BeforeTest
	public void beforeTest() throws FileNotFoundException, IOException {
		ct = new ConnectionTestService();
		cs = new TuningCommandService();
		RestAPIBaseTest.loginDSM();
	}
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() {
		p = ct.loadJSONPostData("AddConnection.properties");
		return FileTools.readProperties(p);
	}
	
	@DataProvider(name = "host")
	public Object[][] readHost() throws FileNotFoundException{
		return FileTools.readPropertiesFile("testData/postData/connectionsData/TestAddRepoDB.properties");
	}
	
	static String host = "";
	
	@Test(dataProvider = "host")
	public static void getHost(Object key,Object inputPara){
		JSONObject obj = JSONObject.fromObject(inputPara);
		host = obj.getString("host");
	}
	
	
	@Test(dataProvider = "testdata")
	public void testAddConnections(Object key,Object inputPara) throws InterruptedException, FileNotFoundException, IOException {
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		String name = obj.getString("name");
		String dbType = obj.getString("dbType");
		
		String monitorHost = null;
		if(obj.get("host") != null){
			monitorHost = obj.getString("host");
			System.out.println(monitorHost);
		}else{
			monitorHost = host;
			System.out.println(monitorHost);
		}
		
		String port = obj.getString("port");
		String user = obj.getString("user");
		String password = obj.getString("password");
		String database = cs.listDatabase();
		if(database.contains(name)){
			System.out.println("MESSAGE:The database with same name already exist, please use an another name");
			return;
		}
		if("DB2LUW".equals(dbType)){
			String dbName = obj.getString("dbName");
			JSONObject result = cs.addConnection(luwDesc(name,dbType,dbName,monitorHost,port,user,password));
			String responseCode = (String) result.get("resultCode");
			if("failure".equals(responseCode)){
				Assert.fail("Add database connection failed...");
			}else {
				Assert.assertEquals(result.getString("resultCode"), "success");
			}
		}else{
			String location = obj.getString("location");
			JSONObject result = cs.addConnection(zosDesc(name,dbType,location,monitorHost,port,user,password));
			String responseCode = (String) result.get("resultCode");
			if("failure".equals(responseCode)){
				Assert.fail("Add database connection failed...");
			}else {
				Assert.assertEquals(result.getString("resultCode"), "success");
			}
		}
		
	}
	public Properties luwDesc(String name,String dbType,String dbName,String monitorHost,String port,String user,
			String password) {
		Properties desc = new Properties();
		desc.put("name", name);
		desc.put("dataServerExternalType", dbType);
		desc.put("databaseName", dbName);
		desc.put("host", monitorHost);
		desc.put("port", port);
		desc.put("securityMechanism", "3");
		desc.put("user", user);
		desc.put("operationUser", user);
		desc.put("operationPassword", password);
		desc.put("password", password);
		desc.put("saveOperationCredentials", true);
		desc.put("disableDataCollection", false);
		desc.put("onetimePasswordEnable", false);
		desc.put("creator", "admin");
		desc.put("URL", "jdbc:db2://"+monitorHost+":"+port+"/"+dbName+":retrieveMessagesFromServerOnGetMessage=true;securityMechanism=3;");
		desc.put("dataServerType", dbType);
		return desc;
	}
	
	public Properties zosDesc(String name,String dbType,String location,String monitorHost,String port,
			String user,String password) {
		Properties desc = new Properties();
		desc.put("name", name);
		desc.put("dataServerType", dbType);
		desc.put("location", location);
		desc.put("host", monitorHost);
		desc.put("port", port);
		desc.put("securityMechanism", "3");
		desc.put("user", user);
		desc.put("password", password);
		desc.put("creator", "admin");
		desc.put("JDBCProperties", "");
		desc.put("xtraProps", "");
		desc.put("comment", "");
		desc.put("URL", "jdbc:db2://"+monitorHost+":"+port+"/"+location+":emulateParameterMetaDataForZCalls=1;retrieveMessagesFromServerOnGetMessage=true;securityMechanism=3;");
		return desc;
	}
}































