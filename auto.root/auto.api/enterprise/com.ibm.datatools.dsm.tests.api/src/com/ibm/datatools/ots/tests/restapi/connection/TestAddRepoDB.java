package com.ibm.datatools.ots.tests.restapi.connection;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;

import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.ibm.datatools.ots.tests.restapi.common.ConnectionTestService;
import com.ibm.datatools.ots.tests.restapi.common.RestAPIBaseTest;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.test.utils.FileTools;

public class TestAddRepoDB {
	private final Logger logger = LogManager.getLogger(getClass());
	TuningCommandService cs;
	ConnectionTestService ct;
	Properties p;
	
	private static final int tryAddRepoDBTimes = 2;
	
	@BeforeTest
	public void beforeTest() throws FileNotFoundException, IOException {
		cs = new TuningCommandService();
		ct = new ConnectionTestService();
		RestAPIBaseTest.loginDSM();
	}
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() {
		p = ct.loadJSONPostData("TestAddRepoDB.properties");
		return FileTools.readProperties(p);
	}
	
	@Test(dataProvider = "testdata")
	public void testAddRepoDB(Object key,Object inputPara) throws ParserConfigurationException, SAXException, IOException{
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		
		String hostNumber = obj.getString("host");
		String repodbName = obj.getString("repodbName");
		String host = "\"" + hostNumber + "\"";
		repodbName = "\"" + repodbName + "\"";
		System.out.println("RepoDB host >>"+host);
		System.out.println("RepoDB name >>"+repodbName);
		
		String newProfile = "<connmgt:ConnectionProfile   xmi:version=\"2.0\"     xmlns:xmi=\"http://www.omg.org/XMI\"  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"  xmlns:connmgt=\"com.ibm.datatools.db.connmgt\">" +
				"<property name=\"name\"  updatedValue=\"META_DB_CONNECTION_ID\"/>" +
				"<property name=\"dataServerExternalType\"  updatedValue=\"DB2LUW\"/>" +
				"<property name=\"databaseName\"  updatedValue="+repodbName+"/>" +
				"<property name=\"host\"  updatedValue="+host+" />" +
				"<property name=\"port\"  updatedValue=\"65000\"/>" +
				"<property name=\"securityMechanism\"  updatedValue=\"3\"/>" +
				"<property name=\"user\"  updatedValue=\"db2inst1\"/>" +
				"<property name=\"password\"  updatedValue=\"N1cetest\"/>" +
				"<property name=\"creator\"  updatedValue=\"admin\"/>" +
				"<property name=\"dataServerType\"  updatedValue=\"DB2LUW\"/>" +
				"</connmgt:ConnectionProfile>";
		
		System.out.println(">>"+newProfile);
		
		for(int i=1; i<=tryAddRepoDBTimes; i++){
			System.out.println("Try to add repository database for "+i+" time...");
			String result = cs.addRepoDB(newProfile);
			System.out.println("Add repodb result >> "+result);
			String resultCode = result.substring(result.indexOf("resultcode") + 11, result.indexOf("/resultcode")-1);
			System.out.println("Add repodb resultCode >> "+resultCode);
			if("success".equals(resultCode)){
				System.out.println("Add repodb succeed...");
				break;
			}
			System.out.println("Add repodb for "+i+" time failed...");
		}
		
	}
}
























