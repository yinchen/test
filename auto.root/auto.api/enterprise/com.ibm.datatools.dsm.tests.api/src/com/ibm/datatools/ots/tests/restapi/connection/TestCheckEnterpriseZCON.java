package com.ibm.datatools.ots.tests.restapi.connection;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.ots.tests.restapi.base.CheckMonitorDB;
import com.ibm.datatools.ots.tests.restapi.common.RestAPIBaseTest;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;

public class TestCheckEnterpriseZCON {
	TuningCommandService cs;
	Properties p;
	
	String dbName = null;
	
	@Parameters("dbProfile")
	@BeforeTest
	public void beforeTest(String monitorDBName) throws FileNotFoundException, IOException {
		cs = new TuningCommandService();
		RestAPIBaseTest.loginDSM();
		
		dbName = monitorDBName;
		Assert.assertEquals(CheckMonitorDB.checkMonitorDBStatus(dbName), "success");
	}
	
	@Test
	public void verifyEnterpriseZCON(){
		
		JSONObject result = cs.CHECK_ENTERPRISE_Z_CON(dbName);
		System.out.println(">>"+result);
		
		if(result == null){
			String errorMessage = "ERROR MESAGE : CHECK_ENTERPRISE_Z_CON result is null";
			Assert.fail(errorMessage);
		}
		
		int responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		
		String licenseResult = result.getString("result");
		if("false".equals(licenseResult)){
			String message = " License of DB "+dbName+" is not ENTERPRISE_Z_CON";
			Assert.fail(message);
		}else if("true".equals(licenseResult)){
			Assert.assertTrue("true".equals(licenseResult));
		}
	}
}





















