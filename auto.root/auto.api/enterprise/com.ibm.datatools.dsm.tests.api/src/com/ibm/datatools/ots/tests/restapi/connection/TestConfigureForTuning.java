package com.ibm.datatools.ots.tests.restapi.connection;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckCredentialForTuning;
import com.ibm.datatools.ots.tests.restapi.base.CheckMonitorDB;
import com.ibm.datatools.ots.tests.restapi.common.RestAPIBaseTest;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;

public class TestConfigureForTuning {
	private final Logger logger = LogManager.getLogger(getClass());
	TuningCommandService cs;
	Properties p;
	
	String dbName = null;
	String dbType = null;
	String username = null;
	String password = null;
	
	@Parameters({"dbProfile","dbType","username","password"})
	@BeforeTest()
	public void beforeTest(String dbName,String dbType,String username,String password) throws FileNotFoundException, IOException {
		cs = new TuningCommandService();
		RestAPIBaseTest.loginDSM();
		this.dbName = dbName;
		this.dbType = dbType;
		this.username = username;
		this.password = password;
		
		Assert.assertEquals(CheckMonitorDB.checkMonitorDBStatus(dbName),"success","Verify the database "+dbName+" failure");
	}
	
	@Test
	public void testConfigMonitorDB(){
		
		//Check db user credential
		CheckCredentialForTuning.checkCredential(this.dbName, this.username, this.password);
		
		
		JSONObject result = cs.configureTableSpace(dbName);
		String tableSpace = result.getString("tablespaceNames");
		String s = tableSpace.substring(1, tableSpace.length() -1);
		JSONObject objTableSpace = JSONObject.fromObject(s);
		String data = objTableSpace.getString("data");
		if(data.equals("error")){
			String message = "Failed to retrieve table space.";
			Assert.fail("ERROR MESSAGE : "+message);
			return;
		}
		
		JSONObject configStatusResult = cs.getConfigStatus(dbType, dbName);
		String canTune = configStatusResult.getString("CANTUNE");
		System.out.println(canTune);
		
		if("true".equals(canTune)){
			String message = "Database "+dbName+" already configured";
			logger.info(message);
			return;
		}
		
		JSONObject configDBResult = cs.configDB(dbName);
		String dbconfigstatus = configDBResult.getString("dbconfigstatus");
		if(!"success".equals(dbconfigstatus)){
			String message = "Configure database "+dbName+" failed";
			Assert.fail("ERROR MESSAGE :"+message);
			return;
		}else{
			String message = configDBResult.getString("message");
			logger.info(message);
		}
		
	}
}

























