package com.ibm.datatools.ots.tests.restapi.connection;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.ots.tests.restapi.base.CheckMonitorDB;
import com.ibm.datatools.ots.tests.restapi.common.ConnectionTestService;
import com.ibm.datatools.ots.tests.restapi.common.RestAPIBaseTest;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;

public class TestGetDB2License {
	TuningCommandService cs;
	ConnectionTestService ct;
	Properties p;
	
	String dbName = null;
	@Parameters("dbProfile")
	@BeforeTest
	public void beforeTest(String monitorDBName) throws FileNotFoundException, IOException {
		cs = new TuningCommandService();
		ct = new ConnectionTestService();
		RestAPIBaseTest.loginDSM();
		
		dbName = monitorDBName;
		Assert.assertEquals(CheckMonitorDB.checkMonitorDBStatus(dbName), "success");
	}
	
	
	@Test
	public void testVerifyDB2License(){
		
		String db2License = null;
		p = ct.loadJSONPostData("TestGetDB2License.properties");
		
		db2License = p.getProperty(dbName);
		if(db2License == null){
			String message = "NO license info of db ["+dbName+"] found in properties file.";
			System.out.println("Error message : "+message);
			Assert.fail(message);
		}
		
		JSONObject obj = JSONObject.fromObject(db2License);
		
		String checkLicense = obj.getString("checkLicense");
		String checkCapabilities = obj.getString("checkCapabilities");
		
		String checkCapas[] = checkCapabilities.split(",");
		List<String> checkCapasList = Arrays.asList(checkCapas);
		
		JSONObject result = cs.getDB2License(dbName);
		System.out.println("License info of db "+dbName+" is >>> "+result);
		
		if(result == null){
			String message = "ERROR MESSAGE : GET_DB2_LICENSE result is null";
			Assert.fail(message);
		}
		
		int responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		
		String db2license = result.getString("db2license");
		String capabilities = result.getString("capabilities");
		capabilities = capabilities.replaceAll("\"", "");
		String capa = capabilities.substring(1, capabilities.length()-1);
		
		String capas[] = capa.split(",");
		List<String> capasList = Arrays.asList(capas);
		boolean capasCheckResult = compare(capasList, checkCapasList);
		
		
		if(!db2license.equals(checkLicense) && !capasCheckResult){
			String errorMessage = "ERROR MESSAGE : db2license and capabilities of DB "+dbName+" do not matches";
			Assert.fail(errorMessage);
		}else if(db2license.equals(checkLicense) && !capasCheckResult){
			String errorMessage = "ERROR MESSAGE : db2license of DB "+dbName+" matches but capabilities do not matches";
			Assert.fail(errorMessage);
		}else if(!checkLicense.contains(db2license) && capasCheckResult){
			String errorMessage = "ERROR MESSAGE : capabilities of DB "+dbName+" matches but db2license do not matches";
			Assert.fail(errorMessage);
		}else if(db2license.equals(checkLicense) && capasCheckResult){
			Assert.assertTrue(db2license.equals(checkLicense) && capasCheckResult);
		}
		
	}
	
	public static <T extends Comparable<T>> boolean compare(List<T> a, List<T> b) {
		  if(a.size() != b.size())
		    return false;
		  Collections.sort(a);
		  Collections.sort(b);
		  for(int i=0;i<a.size();i++){
		    if(!a.get(i).equals(b.get(i)))
		      return false;
		  }
		  return true;
		} 
	
}

































