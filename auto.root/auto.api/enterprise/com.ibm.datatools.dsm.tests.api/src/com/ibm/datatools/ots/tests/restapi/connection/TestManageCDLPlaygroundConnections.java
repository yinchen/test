package com.ibm.datatools.ots.tests.restapi.connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.test.utils.CryptUtils;
import com.ibm.datatools.test.utils.ReadeCsvUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


public class TestManageCDLPlaygroundConnections {

	private static String whiteListFilePath = Configuration.getuploadFilePath();
	private static String whiteListConnectionsFileName = "SVLPlaygroundConnections.csv";
	
	private static final int DBPROFILENAMECOL = 0;
	private static final int DBTYPECOL =1;
	private static final int DBNAMECOL = 2;
	private static final int HOSTCOL = 4;
	private static final int PORTCOL = 5;
	private static final int USERCOL = 6;
	private static final int PASSWORDCOL = 7;
	private static final int JDBCSECURITYCOL = 23;
	private static final int LOCATIONCOL = 9;
	
	private static ReadeCsvUtil util;
	TuningCommandService cs;
	
	
	public static int getWhiteListConnections() throws Exception{
		util = new ReadeCsvUtil(whiteListFilePath + whiteListConnectionsFileName);
		
		return util.getRowNum() - 1;
	}
	
	
	public static List<String> whiteListDBProfileNameList() throws Exception{
		util = new ReadeCsvUtil(whiteListFilePath + whiteListConnectionsFileName);
		String conntions = util.getCol(DBPROFILENAMECOL);
		String[] conntionsArray = conntions.split(",");
		List<String> whiteListConnectionsList = new ArrayList<>();
		for(int i=1;i<conntionsArray.length;i++){
			whiteListConnectionsList.add(conntionsArray[i].trim());
		}
		return whiteListConnectionsList;
	}
	
	public static Map<String, ConnectionEntity> getWhiteListConnEneity() throws Exception{
		ReadeCsvUtil whiteListCSVUtil = new ReadeCsvUtil(whiteListFilePath + whiteListConnectionsFileName);
		
		Map<String, ConnectionEntity> connectionsEntity = new HashMap<String,ConnectionEntity>();
		
		String dbProfileName;
		String dbType;      
		String dbName;     
		String host;        
		String port;        
		String jdbcSecurity;
		String userID;      
		String password;    
		String location;    
		
		for(int i=1;i<whiteListCSVUtil.getRowNum();i++){
			 String[] dbPara = whiteListCSVUtil.getRow(i).split(",");
			 
			 dbProfileName = dbPara[DBPROFILENAMECOL].trim();
			 dbType = dbPara[DBTYPECOL].trim();
			 dbName = dbPara[DBNAMECOL].trim();
			 host = dbPara[HOSTCOL].trim();
			 port = dbPara[PORTCOL].trim();
			 jdbcSecurity = dbPara[JDBCSECURITYCOL].trim();
			 userID = dbPara[USERCOL].trim();
			 password = dbPara[PASSWORDCOL].trim();
			 location = dbPara[LOCATIONCOL].trim();
			
			ConnectionEntity connections = new ConnectionEntity(dbProfileName, dbType, dbName, 
					host, port, jdbcSecurity, userID, password,location);
			connectionsEntity.put(dbProfileName, connections);
			
		}
		return connectionsEntity;
	}
	
	
	@Test
	public void test() throws Exception{
		
		String dbProfileName = null;
		String dbType = null;
		String dbName = null;
		String host = null;
		String port = null;
		String userID = null;
		String password = null;
		String jdbcSecurity = null;
		String location = null;
		
		cs = new TuningCommandService();
		JSONObject connectionsResult = cs.getAllConns();
		String data = connectionsResult.getString("data");
		String subData = data.substring(1, data.length()-1);
		JSONArray connsArray = JSONArray.fromObject(subData);
		
		Map<String, ConnectionEntity> connsEntity = getWhiteListConnEneity();
		
		int currentBuildConnections = connsArray.size() -1;
		int whiteListConnections = getWhiteListConnections();
		System.out.println("White List connections amount is : " + whiteListConnections);
		System.out.println("Current build Connections amount is : " + currentBuildConnections);
		
		//Get current build connections dbProfileName (By RestAPI)
		List<String> currentBuildConnectionsList = new ArrayList<>();
		for(int i=1; i<connsArray.size(); i++){
			JSONObject connsObj = JSONObject.fromObject(connsArray.get(i));
			String name = connsObj.getString("name");
			currentBuildConnectionsList.add(name);
		}
		
		System.out.println("Connections name of white list is : " + whiteListDBProfileNameList().toString());
		System.out.println("Connections name of current build is : " + currentBuildConnectionsList.toString());
		
		
		/*
		 * Compare the connections of current build and white list
		 * List <whiteListNoConns> is the connections that not contains in white list which should be deleted. 
		 */
		ArrayList<String> whiteListNoConns = new ArrayList<>();
		for(int i=0; i<currentBuildConnectionsList.size();i++){
			if(!whiteListDBProfileNameList().contains(currentBuildConnectionsList.get(i))){
				whiteListNoConns.add(currentBuildConnectionsList.get(i));
			}
		}
		System.out.println("Connections NOT contains in white list is : "+whiteListNoConns.toString());
		System.out.println("===================Delete connections from current build which NOT contains in white list===================");
		
		//Delete whiteListNoConns
		
		for(int i=0;i<whiteListNoConns.size();i++){
			dbProfileName = whiteListNoConns.get(i);
			JSONObject delConnsResult = cs.delConnections(dbProfileName);
			String resultCode = delConnsResult.getString("resultCode");
			if("success".equals(resultCode)){
				System.out.println("Delete connection "+dbProfileName+" succeed...");
			}else{
				System.out.println("Delete connection ["+dbProfileName+"] failed...");
			}
		}
		
		
		
		/*
		 * Compare the connections of current build and white list
		 * List <cdlNoConns> is the connections that NOT contains in CDL Playground which should be added
		 */
		
		ArrayList<String> currentBuildNoConns = new ArrayList<>();
		for(int i=0;i<whiteListDBProfileNameList().size();i++){
			if(!currentBuildConnectionsList.contains(whiteListDBProfileNameList().get(i))){
				currentBuildNoConns.add(whiteListDBProfileNameList().get(i));
			}
		}
		System.out.println("Connections NOT contains in current build is : "+currentBuildNoConns.toString());
		System.out.println("===================Add connections to current build===================");
		
		for(int i=0;i<currentBuildNoConns.size();i++){
			dbProfileName = connsEntity.get(currentBuildNoConns.get(i)).getDbProfileName();
			dbType = connsEntity.get(currentBuildNoConns.get(i)).getDbType();
			dbName = connsEntity.get(currentBuildNoConns.get(i)).getDbName();
			host = connsEntity.get(currentBuildNoConns.get(i)).getHost();
			port = connsEntity.get(currentBuildNoConns.get(i)).getPort();
			userID = connsEntity.get(currentBuildNoConns.get(i)).getUserID();
			password = CryptUtils.decryptString(connsEntity.get(currentBuildNoConns.get(i)).getPassword());
			jdbcSecurity = connsEntity.get(currentBuildNoConns.get(i)).getJdbcSecurity();
			location = connsEntity.get(currentBuildNoConns.get(i)).getLocation();
			
			if(dbName.isEmpty()){
				JSONObject addConnResult = cs.addConnection(zosDesc(dbProfileName, dbType, location, host, port, userID, password, jdbcSecurity));
				String responseCode = (String) addConnResult.get("resultCode");
				if("failure".equals(responseCode)){
					System.out.println("Add connection ["+dbProfileName+"] failed...");
				}else {
					System.out.println("Add connection ["+dbProfileName+"] succeed...");
				}
			}else{
				JSONObject addConnResult = cs.addConnection(luwDesc(dbProfileName, dbType, dbName, host, port, userID, password,jdbcSecurity));
				String responseCode = (String) addConnResult.get("resultCode");
				if("failure".equals(responseCode)){
					System.out.println("Add connection ["+dbProfileName+"] failed...");
				}else{
					System.out.println("Add connection ["+dbProfileName+"] succeed...");
				}
			}
		}
		
		
		//Get connections with same dbProfileName between current build and white list
		ArrayList<String> sameConns = new ArrayList<>();
		for(String s : whiteListDBProfileNameList()){
			for(String s1 : currentBuildConnectionsList){
				if(s.equals(s1)){
					sameConns.add(s);
				}
			}
		}
		
		System.out.println("===================Update connections in current build which with same dbProfileName of white list===================");
		for(int i=0;i<sameConns.size();i++){
			dbProfileName = connsEntity.get(sameConns.get(i)).getDbProfileName();
			dbType = connsEntity.get(sameConns.get(i)).getDbType();
			dbName = connsEntity.get(sameConns.get(i)).getDbName();
			host = connsEntity.get(sameConns.get(i)).getHost();
			port = connsEntity.get(sameConns.get(i)).getPort();
			userID = connsEntity.get(sameConns.get(i)).getUserID();
			password = CryptUtils.decryptString(connsEntity.get(sameConns.get(i)).getPassword());
			jdbcSecurity = connsEntity.get(sameConns.get(i)).getJdbcSecurity();
			location = connsEntity.get(sameConns.get(i)).getLocation();
			
			
			
			if(dbName.isEmpty()){
				JSONObject addConnResult = cs.updateConnection(zosDesc(dbProfileName, dbType, location, host, port, userID, password, jdbcSecurity));
				String responseCode = (String) addConnResult.get("resultCode");
				if("failure".equals(responseCode)){
					System.out.println("Update connection ["+dbProfileName+"] failed...");
				}else {
					System.out.println("Update connection ["+dbProfileName+"] succeed...");
				}
			}else{
				JSONObject addConnResult = cs.updateConnection(luwDesc(dbProfileName, dbType, dbName, host, port, userID, password,jdbcSecurity));
				String responseCode = (String) addConnResult.get("resultCode");
				if("failure".equals(responseCode)){
					System.out.println("Update connection ["+dbProfileName+"] failed...");
				}else{
					System.out.println("Update connection ["+dbProfileName+"] succeed...");
				}
			}
		}	
		
	}
	
	public static String getUncontain(List<String> cdl, List<String> svl){
		String s = null;
        for(String str1 : cdl){
            if(!svl.contains(str1.trim())){
                System.out.println("White List not have these connections ==>" + str1);
            }
        }
        
        for(String str2 : svl){
            if(!cdl.contains(str2.trim())){
                System.out.println("Current build not have these connections==>" + str2);
            }
        }
		return s;
    }
	
	
	public Properties luwDesc(String name,String dbType,String dbName,String monitorHost,String port,String user,
			String password,String jdbcSecurity) {
		Properties desc = new Properties();
		desc.put("name", name);
		desc.put("dataServerExternalType", dbType);
		desc.put("databaseName", dbName);
		desc.put("host", monitorHost);
		desc.put("port", port);
		desc.put("securityMechanism", jdbcSecurity);
		desc.put("user", user);
		desc.put("password", password);
		desc.put("creator", "admin");
		desc.put("JDBCProperties", "");
		desc.put("xtraProps", "");
		desc.put("comment", "");
		desc.put("URL", "jdbc:db2://"+monitorHost+":"+port+"/"+dbName+":retrieveMessagesFromServerOnGetMessage=true;securityMechanism="+jdbcSecurity+";");
		desc.put("dataServerType", dbType);
		return desc;
	}
	
	public Properties zosDesc(String name,String dbType,String location,String monitorHost,String port,
			String user,String password,String jdbcSecurity) {
		Properties desc = new Properties();
		desc.put("name", name);
		desc.put("dataServerType", dbType);
		desc.put("location", location);
		desc.put("host", monitorHost);
		desc.put("port", port);
		desc.put("securityMechanism", jdbcSecurity);
		desc.put("user", user);
		desc.put("password", password);
		desc.put("creator", "admin");
		desc.put("JDBCProperties", "");
		desc.put("xtraProps", "");
		desc.put("comment", "");
		desc.put("URL", "jdbc:db2://"+monitorHost+":"+port+"/"+location+":emulateParameterMetaDataForZCalls=1;retrieveMessagesFromServerOnGetMessage=true;securityMechanism="+jdbcSecurity+";");
		return desc;
	}
}











