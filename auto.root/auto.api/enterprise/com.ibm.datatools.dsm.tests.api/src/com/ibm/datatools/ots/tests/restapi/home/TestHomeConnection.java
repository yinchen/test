package com.ibm.datatools.ots.tests.restapi.home;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.HomeService;

import net.sf.json.JSONObject;

public class TestHomeConnection {
	
	private static final Logger logger = LogManager
			.getLogger(TestHomeConnection.class);
	protected HomeService homeService = new HomeService();
	
	
	/**
	 * Test get connections with default metrics in home page.
	 * */
	@Test(description = "Test get connection with default metrics in home page")
	public void testGetConnectionWithDefaultMetrics() throws InterruptedException {

		logger.info("Running get connection with default metrics in home page test...");

		/**
		 * Response data: {"data":[1]}
		 */

		// The input defined in the properties
		String query = homeService.getJSONData("getConnectionWithDefaultMetricsInput");

		JSONObject resObj = homeService.callHomeService(HomeService.HOME_REQUEST, query);
		
		int responseCode = (Integer)resObj.get("ResponseCode");
		if ( responseCode != 200 )
		{
			Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+resObj.get("URL")+", POSTDATA:"+resObj.get("PostData"));
		}

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);
		Assert.assertTrue(responseData.containsKey("data"));

		logger.info("PASSED: get connection with default metrics in home page executed successfully");
	}
	
	/**
	 * Test get connections with all metrics in home page.
	 * */
	@Test(description = "Test get connection with all metrics in home page")
	public void testGetConnectionWithAllMetrics() throws InterruptedException {

		logger.info("Running get connection with all metrics in home page test...");

		/**
		 * Response data: {"data":[1]}
		 */

		// The input defined in the properties
		String query = homeService.getJSONData("getConnectionWithAllMetricsInput");

		JSONObject resObj = homeService.callHomeService(HomeService.HOME_REQUEST, query);
		
		int responseCode = (Integer)resObj.get("ResponseCode");
		if ( responseCode != 200 )
		{
			Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+resObj.get("URL")+", POSTDATA:"+resObj.get("PostData"));
		}

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);
		Assert.assertTrue(responseData.containsKey("data"));

		logger.info("PASSED: get connection with all metrics in home page executed successfully");
	}
	
	/**
	 * Test Monitor- Database switch for dashDB Local connection in home page.
	 * */
	@Test(description = "Test Monitor- Database switch for dashDB Local connection in home page")
	public void testdashDBLocalSwitchMonitorDatabase() throws InterruptedException {

		logger.info("Running Monitor- Database switch for dashDB Local connection in home page test...");

		/**
		 * input:
		 * 
         * activity_id:"overview"
         * cmd:"loginRemoteBluhelix"
         * name:"dashDBLocalCon"
         * task_id:"ocm.singleDBView.task.id"  
         *   
		 * Response data: {"isBluhelix":true,"url":"https:\/\/9.30.33.162:8443\/console\/dswebconfig?dswebSSOToken=\/YnRB5RaW-svz1rt-RcP66z6\/3547120715290187"}
		 */

		// The input defined in the properties
		String query = homeService.getJSONData("getdashDBLocalSwitchMonitorDatabaseInput");

		JSONObject resObj = homeService.callHomeService(HomeService.HOME_REQUEST, query);
		
		int responseCode = (Integer)resObj.get("ResponseCode");
		if ( responseCode != 200 )
		{
			Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+resObj.get("URL")+", POSTDATA:"+resObj.get("PostData"));
		}

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);
		Assert.assertTrue(responseData.containsKey("isBluhelix"));
		Assert.assertTrue(responseData.containsKey("url"));

		logger.info("PASSED: Monitor- Database switch for dashDB Local connection in home page executed successfully");
	}
	
	/**
	 * Test Administer - Tables switch for dashDB Local connection in home page.
	 * */
	@Test(description = "Test Administer - Tables switch for dashDB Local connection in home page")
	public void testdashDBLocalSwitchAdministerTables() throws InterruptedException {

		logger.info("Running Administer - Tables switch for dashDB Local connection in home page test...");

		/**
		 * input:
		 * activity_id:"overview"
         * cmd:"loginRemoteBluhelix"
         * name:"dashDBLocalCon"
         * task_id:"DatabaseAdministration.Tables.id"
         *   
		 * Response data: {"isBluhelix":true,"url":"https:\/\/9.30.33.162:8443\/console\/dswebconfig?dswebSSOToken=\/Otp7e6s-5PeFawj6giD1jyn\/3547187372819983"}
		 */

		// The input defined in the properties
		String query = homeService.getJSONData("getdashDBLocalSwitchAdministerTablesInput");

		JSONObject resObj = homeService.callHomeService(HomeService.HOME_REQUEST, query);
		
		int responseCode = (Integer)resObj.get("ResponseCode");
		if ( responseCode != 200 )
		{
			Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+resObj.get("URL")+", POSTDATA:"+resObj.get("PostData"));
		}

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);
		Assert.assertTrue(responseData.containsKey("isBluhelix"));
		Assert.assertTrue(responseData.containsKey("url"));

		logger.info("PASSED: Administer - Tables switch for dashDB Local or Sailfish connection in home page executed successfully");
	}
	
	/**
	 * Test Run SQL switch for dashDB Local connection in home page.
	 * */
	@Test(description = "Test Run SQL switch for dashDB Local connection in home page")
	public void testdashDBLocalSwitchRunSQL() throws InterruptedException {

		logger.info("Running Run SQL switch for dashDB Local connection in home page test...");

		/**
		 * input:
		 * 
         * activity_id:"overview"
         * cmd:"loginRemoteBluhelix"
         * name:"dashDBLocalCon"
         * task_id:"DatabaseAdministration.SQLEditor.id"  
         *   
		 * Response data: {"isBluhelix":true,"url":"https:\/\/9.30.33.162:8443\/console\/dswebconfig?dswebSSOToken=\/gaOkcFZp1fS7fmr_byuhpxV\/3547405708492206"}
		 */

		// The input defined in the properties
		String query = homeService.getJSONData("getdashDBLocalSwitchRunSQLInput");

		JSONObject resObj = homeService.callHomeService(HomeService.HOME_REQUEST, query);
		
		int responseCode = (Integer)resObj.get("ResponseCode");
		if ( responseCode != 200 )
		{
			Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+resObj.get("URL")+", POSTDATA:"+resObj.get("PostData"));
		}

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);
		Assert.assertTrue(responseData.containsKey("isBluhelix"));
		Assert.assertTrue(responseData.containsKey("url"));

		logger.info("PASSED: Run SQL switch for dashDB Local connection in home page executed successfully");
	}
	
	/**
	 * Test Monitor- Database switch for Sailfish connection in home page.
	 * */
	@Test(description = "Test Monitor- Database switch for Sailfish connection in home page")
	public void testSailfishSwitchMonitorDatabase() throws InterruptedException {

		logger.info("Running Monitor- Database switch for Sailfish connection in home page test...");

		/**
		 * input:
		 * 
         * activity_id:"overview"
         * cmd:"loginRemoteBluhelix"
         * name:"SailfishCon"
         * task_id:"ocm.singleDBView.task.id"  
         *   
		 * Response data: {"isBluhelix":true,"url":"https:\/\/9.30.33.163:8443\/console\/dswebconfig?dswebSSOToken=\/YnRB5RaW-svz1rt-RcP66z6\/3547120715290187"}
		 */

		// The input defined in the properties
		String query = homeService.getJSONData("getSailfishSwitchMonitorDatabaseInput");

		JSONObject resObj = homeService.callHomeService(HomeService.HOME_REQUEST, query);
		
		int responseCode = (Integer)resObj.get("ResponseCode");
		if ( responseCode != 200 )
		{
			Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+resObj.get("URL")+", POSTDATA:"+resObj.get("PostData"));
		}

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);
		Assert.assertTrue(responseData.containsKey("isBluhelix"));
		Assert.assertTrue(responseData.containsKey("url"));

		logger.info("PASSED: Monitor- Database switch for Sailfish connection in home page executed successfully");
	}
	
	/**
	 * Test Administer - Tables switch for Sailfish connection in home page.
	 * */
	@Test(description = "Test Administer - Tables switch for Sailfish connection in home page")
	public void testSailfishSwitchAdministerTables() throws InterruptedException {

		logger.info("Running Administer - Tables switch for Sailfish connection in home page test...");

		/**
		 * input:
		 * activity_id:"overview"
         * cmd:"loginRemoteBluhelix"
         * name:"SailfishCon"
         * task_id:"DatabaseAdministration.Tables.id"
         *   
		 * Response data: {"isBluhelix":true,"url":"https:\/\/9.30.33.163:8443\/console\/dswebconfig?dswebSSOToken=\/Otp7e6s-5PeFawj6giD1jyn\/3547187372819983"}
		 */

		// The input defined in the properties
		String query = homeService.getJSONData("getSailfishSwitchAdministerTablesInput");

		JSONObject resObj = homeService.callHomeService(HomeService.HOME_REQUEST, query);
		
		int responseCode = (Integer)resObj.get("ResponseCode");
		if ( responseCode != 200 )
		{
			Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+resObj.get("URL")+", POSTDATA:"+resObj.get("PostData"));
		}

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);
		Assert.assertTrue(responseData.containsKey("isBluhelix"));
		Assert.assertTrue(responseData.containsKey("url"));

		logger.info("PASSED: Administer - Tables switch for Sailfish connection in home page executed successfully");
	}
	
	/**
	 * Test Run SQL switch for Sailfish connection in home page.
	 * */
	@Test(description = "Test Run SQL switch for Sailfish connection in home page")
	public void testSailfishSwitchRunSQL() throws InterruptedException {

		logger.info("Running Run SQL switch for Sailfish connection in home page test...");

		/**
		 * input:
		 * 
         * activity_id:"overview"
         * cmd:"loginRemoteBluhelix"
         * name:"SailfishCon"
         * task_id:"DatabaseAdministration.SQLEditor.id"  
         *   
		 * Response data: {"isBluhelix":true,"url":"https:\/\/9.30.33.163:8443\/console\/dswebconfig?dswebSSOToken=\/gaOkcFZp1fS7fmr_byuhpxV\/3547405708492206"}
		 */

		// The input defined in the properties
		String query = homeService.getJSONData("getSailfishSwitchRunSQLInput");

		JSONObject resObj = homeService.callHomeService(HomeService.HOME_REQUEST, query);
		
		int responseCode = (Integer)resObj.get("ResponseCode");
		if ( responseCode != 200 )
		{
			Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+resObj.get("URL")+", POSTDATA:"+resObj.get("PostData"));
		}

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);
		Assert.assertTrue(responseData.containsKey("isBluhelix"));
		Assert.assertTrue(responseData.containsKey("url"));

		logger.info("PASSED: Run SQL switch for Sailfish connection in home page executed successfully");
	}
}
