package com.ibm.datatools.ots.tests.restapi.home;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.AdminSQLEditorService;
import com.ibm.datatools.ots.tests.restapi.common.HomeService;
import com.ibm.datatools.ots.tests.restapi.monitor.MonitorTestBase;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class TestHomeFavorite extends MonitorTestBase 
{
	//This file is used to test API for Favorite in home page 
		//including add favorite, remove favorite
		
		private static final Logger logger = LogManager
				.getLogger(TestHomeFavorite.class);
		
		protected AdminSQLEditorService sqlEditorService = new AdminSQLEditorService();
		protected HomeService homeService = new HomeService();
		
		@BeforeClass
		public void beforeTest() {
	    //Pre-condition 
			testAddFavorite_precondition();
			testRemoveFavorite_precondition();
		}
		
		@AfterClass
		public void afterTest() {
		//Clean env	
			testAddFavorite_clean();
			
		}
		
		private void testAddFavorite_precondition(){
			
			String sqlStatement = "DELETE FROM IBMPDQ.DASHBOARD_PROPERTIES WHERE CATEGORY=ENTO AND CREATOR='admin' AND PROPERTY_NAME=\'isFavorite\' AND PROPERTY_VALUE=\'true\' AND WTINAME=OTSTEST";
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,false);
		}
		
		private void testRemoveFavorite_precondition(){
			String sqlStatement = "DELETE FROM IBMPDQ.DASHBOARD_PROPERTIES WHERE CATEGORY=ENTO AND CREATOR='admin' AND PROPERTY_NAME=\'isFavorite\' AND PROPERTY_VALUE=\'true\' AND WTINAME=TPCDS";
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,false);	
			
			sqlStatement="INSERT INTO IBMPDQ.DASHBOARD_PROPERTIES(CATEGORY, CREATOR, PROPERTY_NAME, PROPERTY_VALUE, WTINAME) VALUES(\'ENTO\', \'admin\', \'isFavorite\', \'true\', \'TPCDS\')";
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);		
		}
		
		private void testAddFavorite_clean() {
			String sqlStatement = "DELETE FROM IBMPDQ.DASHBOARD_PROPERTIES WHERE CATEGORY=\'ENTO\' AND CREATOR='admin' AND PROPERTY_NAME=\'isFavorite\' AND PROPERTY_VALUE=\'true\' AND WTINAME=\'OTSTEST\'";
			sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);

		}
		
		/**
		 * Test add favorite in home page.
		 * */
		@Test(description = "Test add favorite in home page")
		public void testAddFavorite() throws InterruptedException {

			logger.info("Running add favorite in home page test...");

			/**
			 * Response data: {"data":[1]}
			 */

			// The input defined in the properties
			String query = homeService.getJSONData("addFavoriteInput");

			JSONObject resObj = homeService.callHomeService(HomeService.HOME_REQUEST, query, dbProfile);

			// Get the response Data
			JSONObject responseData = (JSONObject) resObj.get("ResponseData");

			Assert.assertNotNull(responseData);
			Assert.assertTrue(responseData.toString().length() > 0);
			Assert.assertTrue(responseData.containsKey("data"));
			JSONArray ja = responseData.getJSONArray("data");
			Assert.assertTrue((ja.getInt(0) == 1));

			logger.info("PASSED: add favorite in home page executed successfully");
		}
		
		/**
		 * Test remove favorite in home page.
		 * */
		@Test(description = "Test remove favorite in home page")
		public void testRemoveFavorite() throws InterruptedException {

			logger.info("Running remove favorite in home page test...");

			/**
			 * Response data: {"data":[1]}
			 */
			
			// The input defined in the properties
			String query = homeService.getJSONData("removeFavoriteInput");

			JSONObject resObj = homeService.callHomeService(HomeService.HOME_REQUEST, query, dbProfile);

			// Get the response Data
			JSONObject responseData = (JSONObject) resObj.get("ResponseData");

			Assert.assertNotNull(responseData);
			Assert.assertTrue(responseData.toString().length() > 0);
			Assert.assertTrue(responseData.containsKey("data"));
			JSONArray ja = responseData.getJSONArray("data");
			Assert.assertTrue((ja.getInt(0) == 1));

			logger.info("PASSED: remove favorite in home page executed successfully");
		}

}
