package com.ibm.datatools.ots.tests.restapi.home;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.AdminSQLEditorService;
import com.ibm.datatools.ots.tests.restapi.common.HomeService;
import com.ibm.datatools.ots.tests.restapi.monitor.MonitorTestBase;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class TestHomeView extends MonitorTestBase
{
	//This file is used to test API for view in home page 
	//including list view, create view with different combined settings, drop view
	
	private static final Logger logger = LogManager
			.getLogger(TestHomeView.class);
	
	protected AdminSQLEditorService sqlEditorService = new AdminSQLEditorService();
	protected HomeService homeService = new HomeService();
	
	@BeforeClass
	public void beforeTest() {
    //Pre-condition 
		testCreateViewWithDefaultSetting_precondition();
		testCreateViewWithCustomizedSetting_precondition();
		testDropView_precondition();
		testListView_precondition();
	}
	
	@AfterClass
	public void afterTest() {
	//Clean env	
		testCreateViewWithDefaultSetting_clean();
		testCreateViewWithCustomizedSetting_clean();
		testListView_clean();
		
	}
	
	private void testCreateViewWithDefaultSetting_precondition(){
		
		String sqlStatement = "DELETE FROM IBMPDQ.DASHBOARD_PROPERTIES WHERE CATEGORY=\'ento_view\' AND CREATOR='admin' AND PROPERTY_NAME=\'DEFAULTSETTINGVIEW\'";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,false);
	}
	
	private void testCreateViewWithCustomizedSetting_precondition(){
		String sqlStatement = "DELETE FROM IBMPDQ.DASHBOARD_PROPERTIES WHERE CATEGORY=\'ento_view\' AND CREATOR='admin' AND PROPERTY_NAME=\'CUSTOMIZEDSETTINGVIEW\'";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,false);	
	}
	
	private void testDropView_precondition(){
		String sqlStatement = "DELETE FROM IBMPDQ.DASHBOARD_PROPERTIES WHERE CATEGORY=\'ento_view\' AND CREATOR='admin' AND PROPERTY_NAME=\'DROPVIEW\'";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,false);
		
		sqlStatement = "insert into IBMPDQ.DASHBOARD_PROPERTIES(WTINAME, CREATOR, CATEGORY, PROPERTY_NAME,PROPERTY_VALUE)"
				+ " VALUES('test','admin', 'ento_view', 'DROPVIEW','{\"sortLabel\":\"Alert severity\",\"groupByPickerVal\":\"none\","
				+ "\"sortType\":\"DESC\",\"sortId\":\"alerts\",\"showCriticalFilterON\":\"all\",\"filterText\":\"\","
				+ "\"metrics\":[\"metric.throughputAll.data.TOTAL_APP_COMMITS\",\"metric.throughputAll.data.AVG_ACTIVITY_TIME\",\"metric.throughputSystem.data.CPU_PERCENT\","
				+ "\"metric.throughputSystem.data.DB2_CPU_PERCENT\",\"metric.throughputAll.data.LOGICAL_READS\",\"metric.memDbTotalUsed.data.MEMORY_POOL_USED_GB\"],"
				+ "\"sortTips\":\"alert severity within each group.\"}')";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile,true);
		
	}
	
	private void testListView_precondition() {
		
		List<String> ddlList = new ArrayList<String>();

		ddlList.add("DELETE FROM IBMPDQ.DASHBOARD_PROPERTIES WHERE CATEGORY=\'ento_view\' AND CREATOR='admin' AND PROPERTY_NAME=\'LISTVIEW1\'");
		ddlList.add("DELETE FROM IBMPDQ.DASHBOARD_PROPERTIES WHERE CATEGORY=\'ento_view\' AND CREATOR='admin' AND PROPERTY_NAME=\'LISTVIEW2\'");
		ddlList.add("DELETE FROM IBMPDQ.DASHBOARD_PROPERTIES WHERE CATEGORY=\'ento_view\' AND CREATOR='admin' AND PROPERTY_NAME=\'LISTVIEW3\'");
					
		sqlEditorService.runSQLinJDBC(ddlList, ";", this.dbProfile, false);
		
		
		ddlList.add("insert into IBMPDQ.DASHBOARD_PROPERTIES(WTINAME, CREATOR, CATEGORY, PROPERTY_NAME,PROPERTY_VALUE)"
				+ " VALUES('test','admin', 'ento_view', 'LISTVIEW1','{\"sortLabel\":\"Alert severity\",\"groupByPickerVal\":\"none\","
				+ "\"sortType\":\"DESC\",\"sortId\":\"alerts\",\"showCriticalFilterON\":\"all\",\"filterText\":\"\","
				+ "\"metrics\":[\"metric.throughputAll.data.TOTAL_APP_COMMITS\",\"metric.throughputAll.data.AVG_ACTIVITY_TIME\",\"metric.throughputSystem.data.CPU_PERCENT\","
				+ "\"metric.throughputSystem.data.DB2_CPU_PERCENT\",\"metric.throughputAll.data.LOGICAL_READS\",\"metric.memDbTotalUsed.data.MEMORY_POOL_USED_GB\"],"
				+ "\"sortTips\":\"alert severity within each group.\"}')");
		
		ddlList.add("insert into IBMPDQ.DASHBOARD_PROPERTIES(WTINAME, CREATOR, CATEGORY, PROPERTY_NAME,PROPERTY_VALUE)"
				+ " VALUES('test','admin', 'ento_view', 'LISTVIEW2','{\"sortLabel\":\"Alert severity\",\"groupByPickerVal\":\"none\","
				+ "\"sortType\":\"DESC\",\"sortId\":\"alerts\",\"showCriticalFilterON\":\"all\",\"filterText\":\"\","
				+ "\"metrics\":[\"metric.throughputAll.data.TOTAL_APP_COMMITS\",\"metric.throughputAll.data.AVG_ACTIVITY_TIME\",\"metric.throughputSystem.data.CPU_PERCENT\","
				+ "\"metric.throughputSystem.data.DB2_CPU_PERCENT\",\"metric.throughputAll.data.LOGICAL_READS\",\"metric.memDbTotalUsed.data.MEMORY_POOL_USED_GB\"],"
				+ "\"sortTips\":\"alert severity within each group.\"}')");
		
		ddlList.add("insert into IBMPDQ.DASHBOARD_PROPERTIES(WTINAME, CREATOR, CATEGORY, PROPERTY_NAME,PROPERTY_VALUE)"
				+ " VALUES('test','admin', 'ento_view', 'LISTVIEW3','{\"sortLabel\":\"Alert severity\",\"groupByPickerVal\":\"none\","
				+ "\"sortType\":\"DESC\",\"sortId\":\"alerts\",\"showCriticalFilterON\":\"all\",\"filterText\":\"\","
				+ "\"metrics\":[\"metric.throughputAll.data.TOTAL_APP_COMMITS\",\"metric.throughputAll.data.AVG_ACTIVITY_TIME\",\"metric.throughputSystem.data.CPU_PERCENT\","
				+ "\"metric.throughputSystem.data.DB2_CPU_PERCENT\",\"metric.throughputAll.data.LOGICAL_READS\",\"metric.memDbTotalUsed.data.MEMORY_POOL_USED_GB\"],"
				+ "\"sortTips\":\"alert severity within each group.\"}')");

		sqlEditorService.runSQLinJDBC(ddlList, ";", this.dbProfile, true);
	}

	private void testCreateViewWithDefaultSetting_clean() {
		String sqlStatement = "DELETE FROM IBMPDQ.DASHBOARD_PROPERTIES WHERE CATEGORY=\'ento_view\' AND CREATOR='admin' AND PROPERTY_NAME=\'DEFAULTSETTINGVIEW\'";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile, true);

	}
	
	private void testCreateViewWithCustomizedSetting_clean(){
		String sqlStatement = "DELETE FROM IBMPDQ.DASHBOARD_PROPERTIES WHERE CATEGORY=\'ento_view\' AND CREATOR='admin' AND PROPERTY_NAME=\'CUSTOMIZEDSETTINGVIEW\'";
		sqlEditorService.runSQLinJDBC(sqlStatement, ";", this.dbProfile, true);	
	}
	
    private void testListView_clean(){
    	
    	List<String> ddlList = new ArrayList<String>();

		ddlList.add("DELETE FROM IBMPDQ.DASHBOARD_PROPERTIES WHERE CATEGORY=\'ento_view\' AND CREATOR='admin' AND PROPERTY_NAME=\'LISTVIEW1\'");
		ddlList.add("DELETE FROM IBMPDQ.DASHBOARD_PROPERTIES WHERE CATEGORY=\'ento_view\' AND CREATOR='admin' AND PROPERTY_NAME=\'LISTVIEW2\'");
		ddlList.add("DELETE FROM IBMPDQ.DASHBOARD_PROPERTIES WHERE CATEGORY=\'ento_view\' AND CREATOR='admin' AND PROPERTY_NAME=\'LISTVIEW3\'");
					
		sqlEditorService.runSQLinJDBC(ddlList, ";", this.dbProfile, true);	
	}
	
	/**
	 * Test create view with default setting in home page.
	 * */
	@Test(description = "Test create view with default setting in home page")
	public void testCreateViewWithDefaultSetting() throws InterruptedException {

		logger.info("Running create view with default setting in home page test...");

		/**
		 * Response data: {"data":[1]}
		 */

		// The input defined in the properties
		String query = homeService.getJSONData("getCreateViewWithDefaultSettingInput");

		JSONObject resObj = homeService.callHomeService(HomeService.HOME_REQUEST, query, dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);
		Assert.assertTrue(responseData.containsKey("data"));
		JSONArray ja = responseData.getJSONArray("data");
		Assert.assertTrue((ja.getInt(0) == 1));

		logger.info("PASSED: create view with default setting in home page executed successfully");
	}
	
	/**
	 * Test create view with customized setting in home page.
	 * */
	@Test(description = "Test create view with customized setting in home page")
	public void testCreateViewWithCustomizedSetting() throws InterruptedException {

		logger.info("Running create view with customized setting in home page test...");

		/**
		 * Response data: {"data":[1]}
		 */
		
		// The input defined in the properties
		String query = homeService.getJSONData("getCreateViewWithCustomizedSettingInput");

		JSONObject resObj = homeService.callHomeService(HomeService.HOME_REQUEST, query, dbProfile);

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);
		Assert.assertTrue(responseData.containsKey("data"));
		JSONArray ja = responseData.getJSONArray("data");
		Assert.assertTrue((ja.getInt(0) == 1));

		logger.info("PASSED: create view with customized setting in home page executed successfully");
	}
	
	
	/**
	 * Test drop view in home page.
	 * */
	@Test(description = "Test create view with default setting in home page")
	public void testDropView() throws InterruptedException {

		logger.info("Running drop view in home page test...");

		/**
		 * Drop view input:
		 * {category:"ento_view",
           cmd:"deleteProperty",
           propertyName:"DROPVIEW",
           userid:"admin"}
		 * 
		 * The response data is:{"data":[1]}
		 * */

		// The input defined in the properties
		String query = homeService.getJSONData( "getDropView" );

		JSONObject resObj =
				   homeService.callHomeService( HomeService.HOME_REQUEST, query, dbProfile );

		// Get the response Data
		JSONObject responseData = (JSONObject)resObj.get( "ResponseData" );

		Assert.assertNotNull( responseData );
		Assert.assertTrue( responseData.toString().length() > 0 );
		Assert.assertTrue( responseData.containsKey( "data" ) );
		JSONArray ja = responseData.getJSONArray( "data" );
		Assert.assertTrue( (ja.getInt(0) == 1) );
				
		logger.info("PASSED: drop view in home page executed successfully");
	}
	
	/**
	 * Test list view in home page.
	 * */
	@Test(description = "Test list view in home page")
	public void testListView() throws InterruptedException {

		logger.info("Running list view in home page test...");

		/**
		 * List view input:
		 * {category:"ento_view",
           cmd:"getProperty",
           userid:"admin"}
		 * 
		 * The response data is:{"data":[1]}
		 * {"data":[[
		 * {"propertyName":"LISTVIEW1","name":"test","propertyValue":{"sortLabel":"Alert severity","groupByPickerVal":"none","sortType":"DESC","sortId":"alerts",
		 * "showCriticalFilterON":"all","filterText":"","metrics":["metric.throughputAll.data.TOTAL_APP_COMMITS","metric.throughputAll.data.AVG_ACTIVITY_TIME",
		 * "metric.throughputSystem.data.CPU_PERCENT","metric.throughputSystem.data.DB2_CPU_PERCENT","metric.throughputAll.data.LOGICAL_READS",
		 * "metric.memDbTotalUsed.data.MEMORY_POOL_USED_GB"],"sortTips":"alert severity within each group."},"category":"ento_view","userid":"admin",
		 * "timestamp":"2017-06-28 03:19:15.953673","LISTVIEW1":{"sortLabel":"Alert severity","groupByPickerVal":"none","sortType":"DESC","sortId":"alerts",
		 * "showCriticalFilterON":"all","filterText":"","metrics":["metric.throughputAll.data.TOTAL_APP_COMMITS","metric.throughputAll.data.AVG_ACTIVITY_TIME",
		 * "metric.throughputSystem.data.CPU_PERCENT","metric.throughputSystem.data.DB2_CPU_PERCENT","metric.throughputAll.data.LOGICAL_READS",
		 * "metric.memDbTotalUsed.data.MEMORY_POOL_USED_GB"],"sortTips":"alert severity within each group."}},
		 * 
		 * {"propertyName":"LISTVIEW2","name":"test","propertyValue":{"sortLabel":"Alert severity","groupByPickerVal":"none","sortType":"DESC","sortId":"alerts",
		 * "showCriticalFilterON":"all","filterText":"","metrics":["metric.throughputAll.data.TOTAL_APP_COMMITS","metric.throughputAll.data.AVG_ACTIVITY_TIME",
		 * "metric.throughputSystem.data.CPU_PERCENT","metric.throughputSystem.data.DB2_CPU_PERCENT","metric.throughputAll.data.LOGICAL_READS",
		 * "metric.memDbTotalUsed.data.MEMORY_POOL_USED_GB"],"sortTips":"alert severity within each group."},"category":"ento_view","userid":"admin",
		 * "LISTVIEW2":{"sortLabel":"Alert severity","groupByPickerVal":"none","sortType":"DESC","sortId":"alerts","showCriticalFilterON":"all","filterText":"",
		 * "metrics":["metric.throughputAll.data.TOTAL_APP_COMMITS","metric.throughputAll.data.AVG_ACTIVITY_TIME","metric.throughputSystem.data.CPU_PERCENT",
		 * "metric.throughputSystem.data.DB2_CPU_PERCENT","metric.throughputAll.data.LOGICAL_READS","metric.memDbTotalUsed.data.MEMORY_POOL_USED_GB"],
		 * "sortTips":"alert severity within each group."},"timestamp":"2017-06-28 03:19:16.013293"},
		 * 
		 * {"LISTVIEW3":{"sortLabel":"Alert severity","groupByPickerVal":"none","sortType":"DESC","sortId":"alerts","showCriticalFilterON":"all","filterText":"",
		 * "metrics":["metric.throughputAll.data.TOTAL_APP_COMMITS","metric.throughputAll.data.AVG_ACTIVITY_TIME","metric.throughputSystem.data.CPU_PERCENT",
		 * "metric.throughputSystem.data.DB2_CPU_PERCENT","metric.throughputAll.data.LOGICAL_READS","metric.memDbTotalUsed.data.MEMORY_POOL_USED_GB"],
		 * "sortTips":"alert severity within each group."},"propertyName":"LISTVIEW3","name":"test","propertyValue":{"sortLabel":"Alert severity",
		 * "groupByPickerVal":"none","sortType":"DESC","sortId":"alerts","showCriticalFilterON":"all","filterText":"",
		 * "metrics":["metric.throughputAll.data.TOTAL_APP_COMMITS","metric.throughputAll.data.AVG_ACTIVITY_TIME","metric.throughputSystem.data.CPU_PERCENT",
		 * "metric.throughputSystem.data.DB2_CPU_PERCENT","metric.throughputAll.data.LOGICAL_READS","metric.memDbTotalUsed.data.MEMORY_POOL_USED_GB"],
		 * "sortTips":"alert severity within each group."},"category":"ento_view","userid":"admin","timestamp":"2017-06-28 03:19:16.096853"}]]}

		 * 
		 * */

		// The input defined in the properties, create 3 views, and check listing them
		String query = homeService.getJSONData( "getListView" );

		JSONObject resObj =
				   homeService.callHomeService( HomeService.HOME_REQUEST, query, dbProfile );

		// Get the response Data
		JSONObject responseData = (JSONObject) resObj.get("ResponseData");

		Assert.assertNotNull(responseData);
		Assert.assertTrue(responseData.toString().length() > 0);
		Assert.assertTrue(responseData.containsKey("data"));
		JSONArray ja = responseData.getJSONArray("data");
		JSONArray jaa = ja.getJSONArray(0);
		int i = 0;
		for (int j = 0; j < jaa.size(); j++) 
		{
			if (jaa.getString(j).contains("LISTVIEW1") || jaa.getString(j).contains("LISTVIEW2")
					|| jaa.getString(j).contains("LISTVIEW3")) {
				i++;
			}
		}
		Assert.assertTrue((i == 3));

		logger.info("PASSED: list view in home page executed successfully");
	}


}
