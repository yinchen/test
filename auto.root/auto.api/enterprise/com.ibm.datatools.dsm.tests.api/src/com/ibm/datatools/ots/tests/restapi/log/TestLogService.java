package com.ibm.datatools.ots.tests.restapi.log;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.LogService;
import com.ibm.datatools.ots.tests.restapi.common.RestAPIBaseTest;

public class TestLogService {
	
	LogService logService = null;
	
	@BeforeTest
	public void beforeTest(){
		logService = new LogService();
		RestAPIBaseTest.loginDSM();
	}
	
	@Test
	public void testLog(){
		String log = logService.testLogservice();
		String subLog = log.substring(0, 100);
		System.out.println("Part of log : "+subLog);
		if(!log.contains("Admin")){
			Assert.fail("Check log failure");
		}
	}
	
}
