package com.ibm.datatools.ots.tests.restapi.migration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Properties;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DBUtils;
import com.ibm.datatools.ots.tests.restapi.common.RestAPIBaseTest;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;

public class DownloadzosMigDDL {
	
protected TuningCommandService cs = null;
	
	@BeforeTest()
	public void beforeTest(){
		
		cs = new TuningCommandService();
		RestAPIBaseTest.loginDSM();
	}
	
	@Test()
	public void testMirZOS() throws Exception{
		String s = cs.downloadDDL();
		File file = new File(Configuration.getMigrationValidateSQLFilePath("zosmigration.sql"));
		
		PrintStream ps = new PrintStream(new FileOutputStream(file));
		ps.print(s);
		
		DBUtils dbUtils = new DBUtils();
		
		String path = Configuration.getRepositoryDBFilePath();
		Properties p = new Properties();
		p.load(new FileInputStream(path));
		
		String selectForMasterSQL = Configuration.getMigrationValidateSQLFilePath("zosmigration.sql");
		dbUtils.executeAlways(selectForMasterSQL, "#");
		
	}
	
}
