package com.ibm.datatools.ots.tests.restapi.migration;

import java.io.FileInputStream;
import java.util.Properties;

import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DBUtils;

public class TestValidateSQL {
	
	@Test
	public void testValidateSQL() throws Exception{
		
		DBUtils dbUtils = new DBUtils();
		
		String path = Configuration.getRepositoryDBFilePath();
		Properties p = new Properties();
		p.load(new FileInputStream(path));
		String dbType = p.getProperty("dbType");
		
		System.out.println("Run validate sql in db2 "+ dbType);
		
		if("DB2LUW".equals(dbType)){
			String sqlFileName = Configuration.getMigrationValidateSQLFilePath("validate.sql");
			dbUtils.executeAlways(sqlFileName, "#");
		}else if("ZOS".equals(dbType)){
			String sqlFileName = Configuration.getMigrationValidateSQLFilePath("validateForZOS.sql");
			dbUtils.executeAlways(sqlFileName, "#");
		}
		
		String selectForMasterSQL = Configuration.getMigrationValidateSQLFilePath("Select_for_master.sql");
		dbUtils.execute(selectForMasterSQL, ";");
		
	}
}
