package com.ibm.datatools.ots.tests.restapi.monitor;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.MonAlertService;
import com.ibm.datatools.test.utils.FileTools;
import com.jayway.jsonpath.JsonPath;
//import com.jayway.restassured.RestAssured;
//import com.jayway.restassured.response.Response;

public class MonAlertTest extends MonitorTestBase
{
	private final Logger logger = LogManager.getLogger(getClass());	
	
	private MonAlertService ms;

	@DataProvider(name="testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException{
	
		ms = new MonAlertService(this.dbProfile);
		Properties p = ms.getJSONProperties();
		return FileTools.readProperties(p);
	
	}

	/**
	    *@author wangzq@cn.ibm.com
	    * The case of check alerts(all Open/Opened/Closed/Opened and Closed) responseCode 200 and exception
	**/	
	
	@Test(dataProvider = "testdata")
	public void testMonAlertService(Object key ,Object jsonObj)
	{
		logger.info("Running monalert backend query ..."); 
		
		JSONObject result = ms.callMonAlertService(jsonObj.toString());
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));
		}
			
		JSONObject responseData =JSONObject.fromObject(result.get("ResponseData"));
		boolean exceptionCaught = responseData.toString().contains("\"exceptionCaught\":true");
		if ( exceptionCaught )
		{
			assertExceptionFailure(responseCode, result);
		}
		
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));	
	}
/**
    *@author wangzq@cn.ibm.com
    * The case of check alert detail exception
**/	
	@Test
	public void testAlertDetailException() throws FileNotFoundException, IOException, InterruptedException
	{	
        logger.info("Running monalert detail backend query ..."); 
        ms = new MonAlertService(this.dbProfile);
		JSONObject resultDetail = ms.callMonAlertService(ms.getJSONData("ListAlertSummary"));
       // JSONObject result = ms.callMonAlertService(ms.getJSONData("ListAlertSummary"));
		int responseCode = (Integer)resultDetail.get("ResponseCode");
		if ( responseCode != 200 )
		{
			Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+resultDetail.get("URL")+", POSTDATA:"+resultDetail.get("PostData"));
		}
		
		JSONObject responseData =JSONObject.fromObject(resultDetail.get("ResponseData"));

		boolean exceptionCaught = responseData.toString().contains("\"exceptionCaught\":true");
		if ( exceptionCaught )
		{
			assertExceptionFailure(responseCode, resultDetail);
		}
		
		JSONArray metricData = JsonPath.read(responseData, "$items");
		//	logger.info("Get the first alert"+ metricData);
			
		if (metricData.isEmpty())
		{
			//Assert.fail("The alerts: There is no any alert");
			logger.info ("There is not any alerts now");
		}	
		    
		if ( !metricData.isEmpty() )
		{
			
			JSONObject Data1 = (JSONObject)metricData.get(0); 
			logger.info("Get the first alert"+ Data1);
			
				if ( !Data1.isEmpty() )
		    {
				String dataId = Data1.getString("id");
				if (dataId.isEmpty()){
					Assert.fail("FAILURE=> The alert is invalid: Alert id is null");
				}
				if ( !dataId.isEmpty() )
				{
					//JSONObject alertid = (JSONObject) dataId.get(0);				
					String id = Data1.getString("id");
				    String Jsondata="{\"cmd\":\"getAlertExtendedDetails\",\"alertID\":\""+id+"\"}";
				    resultDetail = ms.callMonAlertDetailService(Jsondata,id);
					
				    logger.info("That is Jsondata"+ resultDetail);
				}
				
		     }	
		}

	}

	/**
	    *@author wangzq@cn.ibm.com
	    * The case of check one alert detail valid(including Alert severity/alert_type/alert severity)
	**/	
	@Test
	public void testAlertDetailValid() throws FileNotFoundException, IOException, InterruptedException
	{	
        logger.info("Running monalert detail valid ..."); 
        ms = new MonAlertService(this.dbProfile);
		JSONObject resultDetail = ms.callMonAlertService(ms.getJSONData("ListAlertSummary"));
       // JSONObject result = ms.callMonAlertService(ms.getJSONData("ListAlertSummary"));
		int responseCode = (Integer)resultDetail.get("ResponseCode");
		if ( responseCode != 200 )
		{
			Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+resultDetail.get("URL")+", POSTDATA:"+resultDetail.get("PostData"));
		}
		
		JSONObject responseData =JSONObject.fromObject(resultDetail.get("ResponseData"));

		boolean exceptionCaught = responseData.toString().contains("\"exceptionCaught\":true");
		if ( exceptionCaught )
		{
			assertExceptionFailure(responseCode, resultDetail);
		}
		
		JSONArray metricData = JsonPath.read(responseData, "$items");
		//	logger.info("Get the first alert"+ metricData);
			
		if (metricData.isEmpty())
		{
			//Assert.fail("The alerts: There is no any alert");
			logger.info ("There is not any alerts now");
		}	
		    
		if ( !metricData.isEmpty() )
		{
			
			JSONObject Data1 = (JSONObject)metricData.get(0); 
			logger.info("Get the first alert"+ Data1);
			
				if ( !Data1.isEmpty() )
		    {
				String dataId = Data1.getString("id");
				if (dataId.isEmpty()){
					Assert.fail("FAILURE=> The alert is invalid: Alert id is null");
				}
				if ( !dataId.isEmpty() )
				{
					String id = Data1.getString("id");
				    String Jsondata="{\"cmd\":\"getAlertExtendedDetails\",\"alertID\":\""+id+"\"}";
				    resultDetail = ms.callMonAlertDetailService(Jsondata,id);
					
				    logger.info("That is Jsondata"+ resultDetail);
				 String responseDetailData =(String) resultDetail.get("ResponseData");
				 
				 JSONObject json = JSONObject.fromObject(responseDetailData);
				 JSONObject metricData1 = JsonPath.read(json, "$alert");
				//    logger.info("YXYXYX"+metricData1);
				 				 
				    String category = metricData1.getString("category");
				    
				    if ( category.isEmpty() ){
						 
						 Assert.fail("FAILURE=> The alert's category is invalid: Alert category is null");
					 }
				    if ( !category.isEmpty() ){
				    	logger.info("let's check the alert type whether reasonable");
				    	String alertType = metricData1.getString("type");
				    //	logger.info ("AlertType:  "+type);
				    	if ( alertType.isEmpty() ){
							 
							 Assert.fail("FAILURE=> The alert's type is invalid: Alert type is null");
						 }
				    	if (!alertType.isEmpty() ){
				    		logger.info("let's check the alert Severity whether reasonable");
				    		String alertSeverity = metricData1.getString("severity");
				    		logger.info("AlertSeverity =  "+alertSeverity);
				    		if ( alertSeverity.isEmpty() ){
				    			Assert.fail("FAILURE=> The alert's severity is invalid: the severity is null");				    			
				    		}
				    		if (!alertSeverity.isEmpty() ){
				    			logger.info("The alert with severity/alert-type/category");
				    		}
				    	}
				     
				    	
				    }
				   // logger.info ("This is category"+category);

	   
	   
				    
			   }  
			 }				
		    }	
		  }	
	/**
	    *@author wangzq@cn.ibm.com
	    * The case of check one alert's severity valid(eg:alert-type is "Table space container utilization ")
	**/	
/*	@Test
	public void testAlertSeverityValid() throws FileNotFoundException, IOException, InterruptedException
	{	
		  logger.info("Running monalert severity valid ..."); 
	        MonAlertService ms=new MonAlertService();
			JSONObject resultDetail = ms.callMonAlertService(ms.getJSONData("ListAlertSummary"));
	       	int responseCode = (Integer)resultDetail.get("ResponseCode");
			if ( responseCode != 200 )
			{
				Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+resultDetail.get("URL")+", POSTDATA:"+resultDetail.get("PostData"));
			}
			
			JSONObject responseData =JSONObject.fromObject(resultDetail.get("ResponseData"));

			boolean exceptionCaught = responseData.toString().contains("\"exceptionCaught\":true");
			if ( exceptionCaught )
			{
				assertExceptionFailure(responseCode, resultDetail);
			}
			
			JSONArray metricData = JsonPath.read(responseData, "$items");
			//	logger.info("Get the first alert"+ metricData);
				
			if (metricData.isEmpty())
			{
				//Assert.fail("The alerts: There is no any alert");
				logger.info ("There is not any alerts now");
			}	
			    
			if ( !metricData.isEmpty() )
			{
				
				JSONObject Data2 = (JSONObject)metricData.get(1); 
				logger.info("Get the 2nd alert"+ Data2);
				
					if ( !Data2.isEmpty() )
			    {
					String dataId = Data2.getString("id");
					if (dataId.isEmpty()){
						Assert.fail("FAILURE=> The alert is invalid: Alert id is null");
					}
					if ( !dataId.isEmpty() )
					{
						String id = Data2.getString("id");
					    String Jsondata="{\"cmd\":\"getAlertExtendedDetails\",\"alertID\":\""+id+"\"}";
					    resultDetail = ms.callMonAlertDetailService(Jsondata,id);
						
					    logger.info("That is Jsondata"+ resultDetail);
					 String responseDetailData =(String) resultDetail.get("ResponseData");
					 
					 JSONObject json = JSONObject.fromObject(responseDetailData);
					 JSONObject metricData1 = JsonPath.read(json, "$alertDetails.[0]");
			        //logger.info("The alertDetails is  "+ metricData1);
			         String alertType = metricData1.getString("Alert type");
			         
					 logger.info("alertType: "+ alertType);
					// if ("Table space container utilization".equals(alertType))
					 if	(alertType.equals("Table space container utilization "))
	
					   {
						 logger.info("The alert type match");							 
					   }
					 /**
					 Get File Used Size String & File Total Size String
					  **/
//					 String fsUsedsizeStr = metricData1.getString("File system used size");
//					 String fsTotalsizeStr = metricData1.getString("File system total size");
//					 int lengthfsUsedsizeStr = fsUsedsizeStr.length();
//					 int lengthfsTotalsizeStr = fsTotalsizeStr.length();
//					 int lengthUsed = lengthfsUsedsizeStr -3;
//					 int lengthTotal = lengthfsTotalsizeStr -3;
//					 String fsUsedsize = fsUsedsizeStr.substring(0,lengthUsed);
//					 String fsTotalsize = fsTotalsizeStr.substring(0,lengthTotal);
					 
//					 long fsUsedsizeInt = Integer.parseInt(fsUsedsize);
//					 long fsTotalsizeInt = Integer.parseInt(fsTotalsize);
	
//					 float fsUsedPercent = fsUsedsizeInt/fsTotalsizeInt;
//					if (fsUsedPercent >0.95 )
/*					{
						String alertSeverity =metricData1.getString("Alert severity") ;
						//logger.info("SSSSSSSSSS:"+alertSeverity);
						if(alertSeverity.equals("Critical"))
						{
							Assert.fail("FAILURE=> The alert severity is invalid");
						}
					 } 
					 
				 
					// long fsUsedsize = fsUsedsizeStr.
					// float fsUsedPercent= fsUsedsize*100/fsTotalsize;
					 logger.info("File System Used size:"+fsUsedsizeStr);
					 logger.info("File System Total size:"+fsTotalsizeStr);
					//logger.info("File System Used Percent:"+fsUsedPercent);
					 logger.info("Length"+lengthfsUsedsizeStr);
					 logger.info("hahaha"+fsUsedsize);
					 logger.info("hehehe"+fsTotalsize);
					 logger.info("fsUsedsizeInt:"+ fsUsedsizeInt );
					 logger.info("fsTotalsizeInt = "+ fsTotalsizeInt);
					 logger.info("FileSystemUsedPercent "+fsUsedPercent);
					 
	          
					} 
			    }
			}
	}  */
	
  	
/*	@Test
	public void testAlertDbAvailSev() throws FileNotFoundException, IOException, InterruptedException
	{	
		  logger.info("Running monalert severity valid ..."); 
	        MonAlertService ms=new MonAlertService();
			JSONObject resultDetail = ms.callMonAlertService(ms.getJSONData("ListAlertSummary"));
			logger.info("ZQresult is"+resultDetail);
	       	int responseCode = (Integer)resultDetail.get("ResponseCode");
			if ( responseCode != 200 )
			{
				Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+resultDetail.get("URL")+", POSTDATA:"+resultDetail.get("PostData"));
			}
			
			JSONObject responseData =JSONObject.fromObject(resultDetail.get("ResponseData"));

			boolean exceptionCaught = responseData.toString().contains("\"exceptionCaught\":true");
			if ( exceptionCaught )
			{
				assertExceptionFailure(responseCode, resultDetail);
			}
			JSONArray metricData = JsonPath.read(responseData, "$items");
			logger.info("Get the first alert"+ metricData);
			
			if (metricData.isEmpty())
			{
			//Assert.fail("The alerts: There is no any alert");
			logger.info ("There is not any alerts now");
			}	
			if ( !metricData.isEmpty() )
			{
				
			JSONObject Data2 = (JSONObject)metricData.get(9); 
			logger.info("Get the 2nd alert"+ Data2);
				
			    if ( !Data2.isEmpty() )
			    {
					String dataId = Data2.getString("id");
					if (dataId.isEmpty()){
						Assert.fail("FAILURE=> The alert is invalid: Alert id is null");
					}
					if ( !dataId.isEmpty() )
					{
						String id = Data2.getString("id");
					    String Jsondata="{\"cmd\":\"getAlertExtendedDetails\",\"alertID\":\""+id+"\"}";
					    resultDetail = ms.callMonAlertDetailService(Jsondata,id);
						
					    logger.info("That is Jsondata"+ resultDetail);
					 String responseDetailData =(String) resultDetail.get("ResponseData");
					 
					 JSONObject json = JSONObject.fromObject(responseDetailData);
					 JSONObject metricData1 = JsonPath.read(json, "$alertDetails.[0]");
			         logger.info("The alertDetails is  "+ metricData1);
			         String alertType = metricData1.getString("Alert type");
			         
					 logger.info("alertType: "+ alertType);
					// if ("Table space container utilization".equals(alertType))
					 if	(alertType.equals("Database availability "))
	
					   {
						 logger.info("The alert type match");							 
					   }
					 String alertStatus = metricData1.getString("Status");
					 logger.info("alertStatus: "+ alertStatus);
					 String alertSeverity = metricData1.getString("Severity");
					 logger.info("alertSeverity: "+ alertSeverity);
					 if (alertStatus.equals("UNREACHABLE "))
					 {
						 if (!alertSeverity.equals("Critical"))
						 {
						    Assert.fail("FAILURE=> The alert severity not match with Status");
					
						 }
					  }	
					 
					}
			    }
			}   
	}	*/
	
/*	private static void assertResponseFailure(int responseCode, JSONObject result)
	{
		Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));
	}
*/	
	private static void assertExceptionFailure(int responseCode, JSONObject result)
	{
		Assert.fail("FAILURE:=>ExceptionCaught=true in responseJson for URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData")+" with RESPONSECODE:"+responseCode+" and RESPONSEDATA:"+result.get("ResponseData"));		
	}
	
/*	private static boolean isExceptionCaught(JSONObject responseData)  
	{
		return responseData.toString().contains("\"exceptionCaught\":true");
	}

*/
}