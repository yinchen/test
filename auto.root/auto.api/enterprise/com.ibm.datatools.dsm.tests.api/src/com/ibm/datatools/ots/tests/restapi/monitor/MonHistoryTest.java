package com.ibm.datatools.ots.tests.restapi.monitor;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.common.ConnectionService;
import com.ibm.datatools.ots.tests.restapi.common.DatabaseService;
import com.ibm.datatools.ots.tests.restapi.common.MonHistoryService;
import com.ibm.datatools.ots.tests.restapi.common.MonLiteService;
import com.ibm.datatools.test.utils.FileTools;
import com.jayway.jsonpath.JsonPath;

public class MonHistoryTest extends MonitorTestBase
{
	private final Logger logger = LogManager.getLogger(getClass());	
	private MonHistoryService ms;
	private DatabaseService databaseService = new DatabaseService();;

	@DataProvider(name="testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException{
	
		ms = new MonHistoryService(this.dbProfile);
		Properties p = ms.getJSONProperties();
		return FileTools.readProperties(p);
	
	}
	
	/**
	 * Add by Sun Yu Ping
	 * Test get PackageCache History System
	     */
	
	@Test
	public void TestPackageCacheHistorySystem() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Package Cache History ..."); 
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_history_Package_Cache_System_Statements"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0]");
		if ( metricData.isEmpty() )
			Assert.fail("No result of metricData  is got" );
		else
		{
			JSONArray resultArray = metricData.getJSONArray("results");
			if (resultArray.isEmpty())
				Assert.fail("No result of results is got" );
			logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));
		}
										
	}
	
	/**
	 * Add by huangxue
	 * Test monitor history mode
		 **/
	@Test(dataProvider = "testdata")
	public void testMonHistory(Object key ,Object jsonObj) throws FileNotFoundException, IOException{
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		JSONObject result;
		logger.info("Running monitor history and alert backend query ..."); 
		if(jsonObj.toString().contains("metricMode")&&jsonObj.toString().contains("HISTORICAL")){   //add by miao hai
			result = ms.callMonHistoryBufferService(jsonObj.toString());
		}else{
		 result = ms.callMonHistoryService(jsonObj.toString());
		}
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");	
		
		boolean exceptionCaught = responseData.toString().contains("\"exceptionCaught\":true");
		if ( exceptionCaught )
		{
			assertExceptionFailure(responseCode, result);
		}
		
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));	
	}

	
	/**
	 * Add by huangxue
	 * Test view connection Statistics
	     */
	
	@Test
	public void TestConnectionStatistics() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Connection Statistics ..."); 
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_Connection_Statistics"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.sessions");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );
		else
			{
			JSONArray resultArray = metricData.getJSONArray("data");
			if ( resultArray.isEmpty() )
				Assert.fail("No result is got" );
			}				
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}
	
	/**
	 * Add by miao hai
	 * Test view buffer pool
	     */
	
	@Test 
	public void TestBufferPoolHistory() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Connection Statistics ..."); 
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryBufferService(ms.getJSONData("Get_history_bufferPool"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}

	

	/**
	 * Add by huangxue
	 * Test get throughputAll
	     */
	
	@Test
	public void TestgetThroughputAll() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Connection Statistics ..."); 
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_history_throughputAll"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.throughputAll");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );	
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}

	
	/**
	 * Add by huangxue
	 * Test get throughputSystem
	     */
	
	@Test
	public void TestgetThroughputSystem() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Connection Statistics ..."); 
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_history_throughputSystem"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.throughputSystem");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );	
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}

	
	/**
	 * Add by huangxue
	 * Test view Database Memory
	     */
	
	
	@Test
	public void TestDatabaseMemory() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Connection Statistics ..."); 
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_Database_Memory"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.monGetMemoryPool");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );
		else
			{
			JSONArray resultArray = metricData.getJSONArray("data");
			if ( resultArray.isEmpty() )
				Assert.fail("No result is got" );
			}				
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));													
	}


	/**
	 * Add by huangxue
	 * Test view Instance Memory
	     */
	
	@Test
	public void TestInstanceMemory() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Connection Statistics ..."); 
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_Instance_Memory"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.instMemSet");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );
		else
			{
			JSONArray resultArray = metricData.getJSONArray("data");
			if ( resultArray.isEmpty() )
				Assert.fail("No result is got" );
			}				
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));													
	}
	

	/**
	 * Add by huangxue
	 * Test view IO BufferPool
	     */
	
	@Test
	public void TestIOBufferPool() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Connection Statistics ..."); 
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_IO_BufferPool"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData1 = JsonPath.read(responseData, "$data[0].[1].metric.monGetBufferpool");
		if ( metricData1.isEmpty() )
			Assert.fail("No result is got" );
		else
			{
			JSONArray resultArray1 = metricData1.getJSONArray("data");
			if ( resultArray1.isEmpty() )
				Assert.fail("No result is got" );
			}
		
		JSONObject metricData2 = JsonPath.read(responseData, "$data[0].[1].metric.monGetTablespace");
		if ( metricData2.isEmpty() )
			Assert.fail("No result is got" );
		else
			{
			JSONArray resultArray2 = metricData2.getJSONArray("data");
			if ( resultArray2.isEmpty() )
				Assert.fail("No result is got" );
			}
		
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));													
	}
	
	
	/**
	 * Add by huangxue
	 * Test view Logging Performance
	     */
	
	@Test
	public void TestLoggingPerformance() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Connection Statistics ..."); 
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_Logging_Performance"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.monGetTransactionLog");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );
		else
			{
			JSONArray resultArray = metricData.getJSONArray("data");
			if ( resultArray.isEmpty() )
				Assert.fail("No result is got" );
			}				
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));													
	}

		
	/**
	 * Add by huangxue
	 * Test view Table Space
	     */
	
	@Test
	public void TestGetTablespace() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Connection Statistics ..."); 
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_Storage_Tablespace"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.monGetTablespace");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );
		else
			{
			JSONArray resultArray = metricData.getJSONArray("data");
			if ( resultArray.isEmpty() )
				Assert.fail("No result is got" );
			}				
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));													
	}


	/**
	 * Add by huangxue
	 * Test view details of Blocking Waiting Connections
	     */
	
	@Test
	public void TestBlockingWaitingConnectionsDetails() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Blocking Waiting Connections ..."); 
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_Blocking_and_Waiting_Connections"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");

		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		JSONObject metricData = JsonPath.read(responseData, "$data[0]");			
		if ( !metricData.isEmpty() )
		{
			JSONArray resultArray = metricData.getJSONArray("results");
			if ( !resultArray.isEmpty() )
			{
				JSONObject indi = (JSONObject) resultArray.get(0);
				Integer HOLDER_APPLICATION_HANDLE = indi.getInt("HOLDER_APPLICATION_HANDLE");
				Integer WAITER_APPLICATION_HANDLE = indi.getInt("WAITER_APPLICATION_HANDLE");
				
				if ( HOLDER_APPLICATION_HANDLE == null || WAITER_APPLICATION_HANDLE == null)
				{
					Assert.fail("Application handle is invalid: null" );
				}	
				
				logger.info("Get the detail of Blocking Connections  ... "); 		
				String cmd_getConnectionLastExecutableIdSQL = "{\"cmd\":\"getConnectionLastExecutableIdSQL\",\"id\":\"" + HOLDER_APPLICATION_HANDLE + "\",\"name\":\"OTSTEST\"}";
				result = ms.callMonHistoryService(cmd_getConnectionLastExecutableIdSQL);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				Assert.assertNotNull(responseData);

				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				
				JSONObject metricData1 = JsonPath.read(responseData, "$data[0]");

				if ( metricData1.isEmpty() )
					Assert.fail("No result is got" );
				else
					{
					Object result1 = metricData1.get("STMT_TEXT");
					if ( result1.toString()=="")
						Assert.fail("No result is got" );
					}	
				
				logger.info("Get the detail of Waiting Connections  ... "); 		
				String cmd_getpodMonCurSqlDetail = "{\"cmd\":\"getConnections\",\"parm_1\":\"" + WAITER_APPLICATION_HANDLE + "\",\"parm_2\":\"" + WAITER_APPLICATION_HANDLE +"\",\"parm_3\":\"" + WAITER_APPLICATION_HANDLE +"\",\"name\":\"OTSTEST\",\"metricOnDemand\": \"podMonCurSqlDetail\"}";
				result = ms.callMonHistoryService(cmd_getpodMonCurSqlDetail);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				Assert.assertNotNull(responseData);
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				
				JSONObject metricData2 = JsonPath.read(responseData, "$data[0].[1]");
				if ( metricData2.isEmpty() )
					Assert.fail("No result is got" );	
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
			}
		}									
	}

	/**
	 * Add by huangxue
	 * Test view details of a Individual Statement
	     */
	
	@Test
	public void TestIndividualStatDetails() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query IndividualStat and get connection's application handle ..."); 
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_Individual_StatHistory"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		JSONObject metricData = JsonPath.read(responseData, "$data[0]");			
		if ( !metricData.isEmpty() )
		{
			JSONArray resultArray = metricData.getJSONArray("results");
			if ( !resultArray.isEmpty() )
			{
				JSONObject indi = (JSONObject) resultArray.get(0);
				String applHandle = indi.getString("APPL_ID");
				Integer uow_id = indi.getInt("UOW_ID");
				Integer activity_id = indi.getInt("ACTIVITY_ID");
				String sql_hash_id = indi.getString("SQL_HASH_ID");
				
				if ( applHandle == null )
				{
					Assert.fail("Application handle is invalid: null" );
				}	
				
				logger.info("Running query getIndividualStatHistoryId ... "); 		
				String cmd_getIndividualStatHistoryId = "{\"cmd\":\"getIndividualStatHistoryId\",\"activity_id\":\"" + activity_id + "\",\"name\":\"OTSTEST\",\"appl_id\":\""+applHandle+"\",\"id\":\""+sql_hash_id+ "\",\"uow_id\":\""+uow_id+"\",\"startTimestamp\":\"1388563200000\",\"endTimestamp\":\"1451635200000\"}";
				result = ms.callMonHistoryService(cmd_getIndividualStatHistoryId);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
			}
		}									
	}


	/**
	 * Add by huangxue
	 * Test view details of Throughput by Connection
	     */
	
	@Test
	public void TestThroughputByConnectionDetails() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Throughput ..."); 
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_Workload_Throughput"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.throughputConnection");			
		if ( !metricData.isEmpty() )
		{
			JSONArray resultArray = metricData.getJSONArray("data");
			if ( !resultArray.isEmpty() )
			{
				JSONObject indi = (JSONObject) resultArray.get(0);
				Integer applHandleavg = indi.getInt("APPLICATION_HANDLE_AVG");
				Integer applHandlemax = indi.getInt("APPLICATION_HANDLE_MAX");
				
				if ( applHandleavg == null )
				{
					Assert.fail("Application handle is invalid: null" );
				}	
				
				logger.info("Running query Throughput By Connection Details ... "); 		
				String cmd_getThroughputDetails = "{\"cmd\":\"getConnections\",\"name\":\"OTSTEST\",\"metricHistory_match\":\"throughputConnection/APPLICATION_HANDLE EQU "+applHandleavg+"\",\"metricHistory_allrows\":\"true\",\"startTimestamp\":\"1388563200000\",\"endTimestamp\":\"1451635200000\",\"metricHistory\": \"throughputConnection\"}";
				result = ms.callMonHistoryService(cmd_getThroughputDetails);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				Assert.assertNotNull(responseData);
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				JSONObject metricData1 = JsonPath.read(responseData, "$data[0].[1].metric.throughputConnection");
				if ( metricData1.isEmpty() )
					Assert.fail("No result is got" );
				else
					{
					JSONArray resultArray1 = metricData1.getJSONArray("histSeries");
					if ( resultArray1.isEmpty() )
						Assert.fail("No result is got" );
					}		
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
			}
		}									
	}

	
	/**
	 * Add by huangxue
	 * Test view details of Throughput by database member
	     */
	
	@Test
	public void TestThroughputByMemberDetails() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Throughput ..."); 
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_Workload_Throughput"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.throughputMember");			
		if ( !metricData.isEmpty() )
		{
			JSONArray resultArray = metricData.getJSONArray("data");
			if ( !resultArray.isEmpty() )
			{
				JSONObject indi = (JSONObject) resultArray.get(0);
				Integer memberAvg = indi.getInt("MEMBER_AVG");
				Integer memberMax = indi.getInt("MEMBER_MAX");
				
				if ( memberAvg == null )
				{
					Assert.fail("Member handle is invalid: null" );
				}	
				
				logger.info("Running query Throughput By Member Details ... "); 		
				String cmd_getThroughputDetails = "{\"cmd\":\"getConnections\",\"name\":\"OTSTEST\",\"metricHistory_match\":\"throughputMember/MEMBER EQU "+memberAvg+"\",\"metricHistory_allrows\":\"true\",\"startTimestamp\":\"1388563200000\",\"endTimestamp\":\"1451635200000\",\"metricHistory\": \"throughputMember\"}";
				result = ms.callMonHistoryService(cmd_getThroughputDetails);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				Assert.assertNotNull(responseData);
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				JSONObject metricData1 = JsonPath.read(responseData, "$data[0].[1].metric.throughputMember");
				if ( metricData1.isEmpty() )
					Assert.fail("No result is got" );
				else
					{
					JSONArray resultArray1 = metricData1.getJSONArray("histSeries");
					if ( resultArray1.isEmpty() )
						Assert.fail("No result is got" );
					}		
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
			}
		}									
	}


	/**
	 * Add by huangxue
	 * Test view details of Throughput by workload
	     */
	
	@Test
	public void TestThroughputByWorkloadDetails() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Throughput ..."); 	
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_Workload_Throughput"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.throughputWorkload");			
		if ( !metricData.isEmpty() )
		{
			JSONArray resultArray = metricData.getJSONArray("data");
			if ( !resultArray.isEmpty() )
			{
				JSONObject indi = (JSONObject) resultArray.get(0);
				String workloadNameAvg = indi.getString("WORKLOAD_NAME_AVG");
				String workloadNameMax = indi.getString("WORKLOAD_NAME_MAX");
				
				if ( workloadNameAvg == null )
				{
					Assert.fail("Workload handle is invalid: null" );
				}	
				
				logger.info("Running query Throughput By workload Details ... "); 		
				String cmd_getThroughputDetails = "{\"cmd\":\"getConnections\",\"name\":\"OTSTEST\",\"metricHistory_match\":\"throughputWorkload/WORKLOAD_NAME EQU "+workloadNameAvg+"\",\"metricHistory_allrows\":\"true\",\"startTimestamp\":\"1388563200000\",\"endTimestamp\":\"1451635200000\",\"metricHistory\": \"throughputWorkload\"}";
				result = ms.callMonHistoryService(cmd_getThroughputDetails);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				Assert.assertNotNull(responseData);
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				JSONObject metricData1 = JsonPath.read(responseData, "$data[0].[1].metric.throughputWorkload");
				if ( metricData1.isEmpty() )
					Assert.fail("No result is got" );
				else
					{
					JSONArray resultArray1 = metricData1.getJSONArray("histSeries");
					if ( resultArray1.isEmpty() )
						Assert.fail("No result is got" );
					}		
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
			}
		}									
	}

	
	/**
	 * Add by huangxue
	 * Test view details of Throughput by Serviceclass
	     */
	
	@Test
	public void TestThroughputByServiceclassDetails() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Throughput ..."); 	
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_Workload_Throughput"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.throughputServiceclass");			
		if ( !metricData.isEmpty() )
		{
			JSONArray resultArray = metricData.getJSONArray("data");
			if ( !resultArray.isEmpty() )
			{
				JSONObject indi = (JSONObject) resultArray.get(0);
				String subclassNameAvg = indi.getString("SERVICE_SUBCLASS_NAME_AVG");
				String subclassNameMax = indi.getString("SERVICE_SUBCLASS_NAME_MAX");
				String superclassNameAvg = indi.getString("SERVICE_SUPERCLASS_NAME_AVG");
				String superclassNameMax = indi.getString("SERVICE_SUPERCLASS_NAME_MAX");
				
				if ( subclassNameAvg == null ||superclassNameAvg == null )
				{
					Assert.fail("Workload handle is invalid: null" );
				}	
				
				logger.info("Running query Throughput By workload Details ... "); 		
				String cmd_getThroughputDetails = "{\"cmd\":\"getConnections\",\"name\":\"OTSTEST\",\"metricHistory_match\":\"throughputServiceclass/SERVICE_SUPERCLASS_NAME EQU "+superclassNameAvg+"/SERVICE_SUBCLASS_NAME EQU "+subclassNameAvg+"\",\"metricHistory_allrows\":\"true\",\"startTimestamp\":\"1388563200000\",\"endTimestamp\":\"1451635200000\",\"metricHistory\": \"throughputServiceclass\"}";
				result = ms.callMonHistoryService(cmd_getThroughputDetails);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				Assert.assertNotNull(responseData);
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				JSONObject metricData1 = JsonPath.read(responseData, "$data[0].[1].metric.throughputServiceclass");
				if ( metricData1.isEmpty() )
					Assert.fail("No result is got" );
				else
					{
					JSONArray resultArray1 = metricData1.getJSONArray("histSeries");
					if ( resultArray1.isEmpty() )
						Assert.fail("No result is got" );
					}		
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
			}
		}									
	}

	
	/**
	 * Add by huangxue
	 * Test view Throughput by Connection
	     */
	
	public void TestThroughputByConnection() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Connection Statistics ..."); 
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_Workload_Throughput"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.throughputConnection");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );
		else
			{
			JSONArray resultArray = metricData.getJSONArray("data");
			if ( resultArray.isEmpty() )
				Assert.fail("No result is got" );
			}				
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));												
	}

	
	/**
	 * Add by huangxue
	 * Test view Throughput by database member
	     */
	
	public void TestThroughputByMember() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Connection Statistics ..."); 
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_Workload_Throughput"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.throughputMember");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );
		else
			{
			JSONArray resultArray = metricData.getJSONArray("data");
			if ( resultArray.isEmpty() )
				Assert.fail("No result is got" );
			}				
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}


	/**
	 * Add by huangxue
	 * Test view Throughput by workload
	     */
	
	public void TestThroughputByWorkload() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Connection Statistics ..."); 
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_Workload_Throughput"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.throughputWorkload");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );
		else
			{
			JSONArray resultArray = metricData.getJSONArray("data");
			if ( resultArray.isEmpty() )
				Assert.fail("No result is got" );
			}				
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}

	
	/**
	 * Add by huangxue
	 * Test view Throughput by Serviceclass
	     */
	
	public void TestThroughputByServiceclass() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Connection Statistics ..."); 
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_Workload_Throughput"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.throughputServiceclass");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );
		else
			{
			JSONArray resultArray = metricData.getJSONArray("data");
			if ( resultArray.isEmpty() )
				Assert.fail("No result is got" );
			}				
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}


	/**
	 * Add by huangxue
	 * Test view details of Package Cache
	     */
	@Test
	public void TestPackageCacheDetails() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Package Cache ..."); 
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_top_NN_sql_statements"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		JSONObject metricData = JsonPath.read(responseData, "$data[0]");			
		if ( !metricData.isEmpty() )
		{
			JSONArray resultArray = metricData.getJSONArray("results");
			if ( !resultArray.isEmpty() )
			{
				JSONObject indi = (JSONObject) resultArray.get(0);
				String sqlHashID = indi.getString("SQL_HASH_ID");

				if ( sqlHashID == null )
				{
					Assert.fail("SQL ID is invalid: null" );
				}	
				
				logger.info("Running query Package Cache Details ... "); 		
				String cmd_getPackageCacheDetails = "{\"cmd\":\"getTopNNHashId\",\"name\":\"OTSTEST\",\"id\":\""+sqlHashID+"\",\"startTimestamp\":\"1388563200000\",\"endTimestamp\":\"1451635200000\"}";
				result = ms.callMonHistoryService(cmd_getPackageCacheDetails);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				Assert.assertNotNull(responseData);
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				JSONObject metricData1 = JsonPath.read(responseData, "$data[0]");
				if ( metricData1.isEmpty() )
					Assert.fail("No result is got" );

				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
			}
		}									
	}


	/**
	 * Add by huangxue
	 * Test view details of Table Space
	     */
	
	@Test
	public void TestTableSpaceDetails() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Table Space ..."); 
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_Storage_Tablespace"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.monGetTablespace");			
		if ( !metricData.isEmpty() )
		{
			JSONArray resultArray = metricData.getJSONArray("data");
			if ( !resultArray.isEmpty() )
			{
				JSONObject indi = (JSONObject) resultArray.get(0);
				String TBSP_NameAVG = indi.getString("TBSP_NAME_AVG");
				String TBSP_NameMAX = indi.getString("TBSP_NAME_MAX");

				if ( TBSP_NameAVG == null )
				{
					Assert.fail("Table Space Name is invalid: null" );
				}	
				
				logger.info("Running query Table Space Details ... "); 		
				String cmd_getTableSpaceDetails = "{\"cmd\":\"getConnections\",\"name\":\"OTSTEST\",\"metricHistory_match\":\"monGetTablespace/TBSP_NAME EQU "+TBSP_NameAVG+"\",\"startTimestamp\":\"1388563200000\",\"endTimestamp\":\"1451635200000\",\"metricHistory\": \"monGetTablespace\"}";
				result = ms.callMonHistoryService(cmd_getTableSpaceDetails);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				Assert.assertNotNull(responseData);
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				JSONObject metricData1 = JsonPath.read(responseData, "$data[0].[1].metric.monGetTablespace");
				if ( metricData1.isEmpty() )
					Assert.fail("No result is got" );
				else
					{
					JSONArray resultArray1 = metricData1.getJSONArray("histSeries");
					if ( resultArray1.isEmpty() )
						Assert.fail("No result is got" );
					}		
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
			}
		}									
	}


	/**
	 * Add by huangxue
	 * Test view details of alert
	     */
	
	@Test
	public void TestAlertDetails() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query All Alert ..."); 	
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_All_Alert"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		JSONArray alertData = JsonPath.read(responseData, "$data[0]");			
		if ( !alertData.isEmpty() )
		{
			JSONObject indi = (JSONObject) alertData.get(0);
			logger.info("information is" + indi.toString());
			String id = indi.getString("id");
				
			if ( id == null )
			{
				Assert.fail("Alert ID is invalid: null" );
			}	
				
			logger.info("Running query alert Details ... "); 		
			String cmd_getAlertDetails = "{\"cmd\":\"getAlert\",\"id\":\""+id+"\"}";
			result = ms.callMonHistoryService(cmd_getAlertDetails);
			responseCode = (Integer)result.get("ResponseCode");
			if ( responseCode != 200 )
			{
				assertResponseFailure(responseCode, result);
			}
			// Check for exception in the response
			responseData = (JSONObject) result.get("ResponseData");
			Assert.assertNotNull(responseData);
			if ( isExceptionCaught(responseData) )
			{			
				assertExceptionFailure(responseCode, result);
			}	
			JSONArray metricData1 = JsonPath.read(responseData, "$data[0]");
			logger.info("information1 is" + metricData1.toString());
			if ( metricData1.isEmpty() )
				Assert.fail("No result is got" );	
				
			logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
			
		}									
	}
	
	
	/**
	 * Add by huangxue
	 * Test Overview in History
	     */
	
	@Test
	public void TestOverview() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Overview in History ..."); 	
		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_Overview_History"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		JSONArray metricData = JsonPath.read(responseData, "$data[0].[1].metric.graphs");
		logger.info("this is result" + metricData.toString());
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );
		else
			{
			JSONArray Overviewresult = (JSONArray) metricData.get(0);;
			logger.info("this is result" + Overviewresult.toString());
			if ( Overviewresult.isEmpty() )
				Assert.fail("No result is got" );
			}				
			logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));																	
	}


	private static void assertResponseFailure(int responseCode, JSONObject result)
	{
		Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));
	}
	
	private static void assertExceptionFailure(int responseCode, JSONObject result)
	{
		Assert.fail("FAILURE:=>ExceptionCaught=true in responseJson for URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData")+" with RESPONSECODE:"+responseCode+" and RESPONSEDATA:"+result.get("ResponseData"));		
	}
	
	private static boolean isExceptionCaught(JSONObject responseData)  
	{
		return responseData.toString().contains("\"exceptionCaught\":true");
	}
}
