package com.ibm.datatools.ots.tests.restapi.monitor;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.MonLiteService;
import com.ibm.datatools.test.utils.FileTools;
import com.jayway.jsonpath.JsonPath;

public class MonLiteTest extends MonitorTestBase
{
	private final Logger logger = LogManager.getLogger(getClass());	
	private MonLiteService ms;

	
	@DataProvider(name="testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException{
	
		ms = new MonLiteService(this.dbProfile);
		Properties p = ms.getJSONProperties();
		return FileTools.readProperties(p);
	
	}
	
	@Test(dataProvider = "testdata")
	public void testMonLiteService(Object key ,Object jsonObj)
	{
		logger.info("Running monlite backend query ..."); 
		
		JSONObject result = ms.callMonLiteService(jsonObj.toString());
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");		
		boolean exceptionCaught = responseData.toString().contains("\"exceptionCaught\":true");
		if ( exceptionCaught )
		{
			assertExceptionFailure(responseCode, result);
		}
		
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));	
	}
	
	@Test
	public void ValidateHadrData() throws FileNotFoundException, IOException, InterruptedException
	{				
		logger.info("Validate query odHadrRoleSummary output ..."); 
		
		ms = new MonLiteService(this.dbProfile);
		JSONObject result = ms.callMonLiteService(ms.getJSONData("metricOnDemand_odHadrRoleSummary"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odHadrRoleSummary");
		// metricData is empty if the monitored db is not a HADR system.
		// We validate data only when the metricData is available from running on a HADR system.
		if ( !metricData.isEmpty() )
		{
			JSONArray dataArray = metricData.getJSONArray("data");
			for ( int i = 0; i < dataArray.size(); i++ )
			{
				JSONObject hadrRole = (JSONObject) dataArray.get(i);
				// Validate HADR_ROLE
				String hadr_role = hadrRole.getString("HADR_ROLE");
				if ( !hadr_role.equalsIgnoreCase("primary") &&  
						!hadr_role.equalsIgnoreCase("standby") )
				{
					Assert.fail("HADR_ROLE " + hadr_role + " is invalid");
				}	
				// Validate HADR_SYNCMODE
				String hadr_syncmode = hadrRole.getString("HADR_SYNCMODE");
				if ( !hadr_syncmode.equalsIgnoreCase("Asynchronous") &&  
						!hadr_syncmode.equalsIgnoreCase("Synchronous") &&
						!hadr_syncmode.equalsIgnoreCase("Near synchronous") && 
						!hadr_syncmode.equalsIgnoreCase("Super asynchronous") )
				{
					Assert.fail("HADR_SYNCMODE " + hadr_syncmode + " is invalid");
				}	
			}
		}			
		
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));			
	}
	
	// URL parms {"cmd":"getBufferpoolDpfRealtimeFallbackPod","parm_1":"BP1","name":"OTSTEST","metricOnDemandDelta":"getBufferpoolDpfRealtimeFallbackPod"}
	// can be used to trigger running monGetBufferpoolDpf query. When UI check box "Collect data for dpf and purescale ..." is NOT selected (monGetBufferpoolDpf data
	// is not collected), then podMonGetBufferpoolDpf query is next run to get the data. To make sure podMonGetBufferpoolDpf is tested no matter what UI check box setting is,
	// this test manually calls {"cmd":"getConnections","parm_1":bp_name","name":"OTSTEST","metricOnDemandDelta":"podMonGetBufferpoolDpf"} to trigger running 
	// podMonGetBufferpoolDpf query. So this test covers query monGetBufferpoolDpf and podMonGetBufferpoolDpf.
	@Test(enabled = false)
	public void TestBufferpoolDpf() throws FileNotFoundException, IOException, InterruptedException
	{				
		logger.info("Run query monGetBufferpool and get bufferpool name ..."); 
		
		ms = new MonLiteService(this.dbProfile);
		JSONObject result = ms.callMonLiteService(ms.getJSONData("metricRealtime_monGetBufferpool"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}			
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.monGetBufferpool");			
		if ( !metricData.isEmpty() )
		{
			JSONArray dataArray = metricData.getJSONArray("data");
			if ( !dataArray.isEmpty() )
			{
				JSONObject bufferpool = (JSONObject) dataArray.get(0);
				String bp_name = bufferpool.getString("BP_NAME");
				if ( bp_name == null )
				{
					Assert.fail("Bufferpool name is invalid: null" );
				}	
				
				logger.info("Running command getBufferpoolDpfRealtimeFallbackPod ... "); 
				
				// {"cmd":"getBufferpoolDpfRealtimeFallbackPod","parm_1":bp_name,"name":"OTSTEST","metricOnDemandDelta":"getBufferpoolDpfRealtimeFallbackPod"}				
				String cmd_realtimeFallbackPod = "{\"cmd\":\"getBufferpoolDpfRealtimeFallbackPod\",\"parm_1\":\"" + bp_name + "\",\"name\":\"OTSTEST\",\"metricOnDemandDelta\":\"getBufferpoolDpfRealtimeFallbackPod\"}";
				result = ms.callMonLiteService(cmd_realtimeFallbackPod);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}		
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));			
				
				logger.info("Running query podMonGetBufferpoolDpf ... ");
				
				// {"cmd":"getConnections","parm_1":bp_name","name":"OTSTEST","metricOnDemandDelta":"podMonGetBufferpoolDpf"}
				String cmd_podMonGetBufferpoolDpf="{\"cmd\":\"getConnections\",\"parm_1\":\"" + bp_name + "\",\"name\":\"OTSTEST\",\"metricOnDemandDelta\":\"podMonGetBufferpoolDpf\"}";
				result = ms.callMonLiteService(cmd_podMonGetBufferpoolDpf);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));
			}
		}								
	}
	
	@Test
	public void TestPodThroughputConnectionDpf() throws FileNotFoundException, IOException, InterruptedException
	{				
		logger.info("Run query throughputConnection and get application handle"); 
		
		ms = new MonLiteService(this.dbProfile);
		JSONObject result = ms.callMonLiteService(ms.getJSONData("metricRealtime_throughputConnection"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.throughputConnection");			
		if ( !metricData.isEmpty() )
		{
			JSONArray dataArray = metricData.getJSONArray("data");
			if ( !dataArray.isEmpty() )
			{
				JSONObject throughputConn = (JSONObject) dataArray.get(0);
				String applHandle = throughputConn.getString("APPLICATION_HANDLE");
				if ( applHandle == null )
				{
					Assert.fail("Application handle is invalid: null" );
				}	
				
				logger.info("Running query podThroughputConnectionDpf ... "); 
				
				// {"cmd":"getConnections","parm_1":applHandle,"name":"OTSTEST","metricOnDemandDelta":"podThroughputConnectionDpf"}				
				String cmd_podThroughputConnectionDpf = "{\"cmd\":\"getConnections\",\"parm_1\":\"" + applHandle + "\",\"name\":\"OTSTEST\",\"metricOnDemandDelta\":\"podThroughputConnectionDpf\"}";
				result = ms.callMonLiteService(cmd_podThroughputConnectionDpf);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));
			}
		}									
	}
	
	@Test
	public void TestPodThroughputServiceclassDpf() throws FileNotFoundException, IOException, InterruptedException
	{				
		logger.info("Run query throughputServiceclass and get service superclass name, service subclass name ..."); 
		
		ms = new MonLiteService(this.dbProfile);
		JSONObject result = ms.callMonLiteService(ms.getJSONData("metricRealtime_throughputServiceclass"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.throughputServiceclass");			
		if ( !metricData.isEmpty() )
		{
			JSONArray dataArray = metricData.getJSONArray("data");
			if ( !dataArray.isEmpty() )
			{				
				JSONObject throughputServiceclass = (JSONObject) dataArray.get(0);
				String serviceSuperClass = throughputServiceclass.getString("SERVICE_SUPERCLASS_NAME");
				String serviceSubClass = throughputServiceclass.getString("SERVICE_SUBCLASS_NAME");
				if ( serviceSuperClass == null )
				{
					Assert.fail("Service superclass name is invalid: null" );
				}	
				if ( serviceSubClass == null )
				{
					Assert.fail("Service subclass name is invalid: null" );
				}
				
				logger.info("Running query podThroughputServiceclassDpf ... "); 
				
				// {"cmd":"getConnections","parm_1":serviceSuperClass,"parm_2":serviceSubClass,"name":"OTSTEST","metricOnDemandDelta":"podThroughputServiceclassDpf"}				
				String cmd_podThroughputServiceclassDpf = "{\"cmd\":\"getConnections\",\"parm_1\":\"" + serviceSuperClass + "\",\"parm_2\":\"" + serviceSubClass + "\",\"name\":\"OTSTEST\",\"metricOnDemandDelta\":\"podThroughputServiceclassDpf\"}";
				result = ms.callMonLiteService(cmd_podThroughputServiceclassDpf);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));
			}
		}									
	}
	
	@Test
	public void TestPodThroughputWorkloadDpf() throws FileNotFoundException, IOException, InterruptedException
	{				
		logger.info("Run query throughputWorkload and get workload name ..."); 
		
		ms = new MonLiteService(this.dbProfile);
		JSONObject result = ms.callMonLiteService(ms.getJSONData("metricRealtime_throughputWorkload"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.throughputWorkload");			
		if ( !metricData.isEmpty() )
		{
			JSONArray dataArray = metricData.getJSONArray("data");
			if ( !dataArray.isEmpty() )
			{
				JSONObject throughputWorkload = (JSONObject) dataArray.get(0);
				String wlName = throughputWorkload.getString("WORKLOAD_NAME");
				if ( wlName == null )
				{
					Assert.fail("workload name is invalid: null" );
				}	
				
				logger.info("Running query podThroughputWorkloadDpf ... "); 
				
				// {"cmd":"getConnections","parm_1":wlName,"name":"OTSTEST","metricOnDemandDelta":"podThroughputWorkloadDpf"}				
				String cmd_podThroughputWorkloadDpf = "{\"cmd\":\"getConnections\",\"parm_1\":\"" + wlName + "\",\"name\":\"OTSTEST\",\"metricOnDemandDelta\":\"podThroughputWorkloadDpf\"}";
				result = ms.callMonLiteService(cmd_podThroughputWorkloadDpf);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));
			}
		}									
	}
	
	@Test
	public void TestPodInfSqlDetail() throws FileNotFoundException, IOException, InterruptedException
	{				
		logger.info("Run query odInfSql and get in-flight statement's application handle, uow_id, activity_id ..."); 
		
		ms = new MonLiteService(this.dbProfile);
		JSONObject result = ms.callMonLiteService(ms.getJSONData("metricOnDemand_odInfSql"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odInfSql");			
		if ( !metricData.isEmpty() )
		{
			JSONArray dataArray = metricData.getJSONArray("data");
			if ( !dataArray.isEmpty() )
			{
				JSONObject infStmt = (JSONObject) dataArray.get(0);
				String applHandle = infStmt.getString("APPLICATION_HANDLE");
				Integer uow_id = infStmt.getInt("UOW_ID");
				Integer activity_id = infStmt.getInt("ACTIVITY_ID");
				if ( applHandle == null )
				{
					Assert.fail("Application handle is invalid: null" );
				}	
				
				logger.info("Running query podInfSqlDetail ... "); 
				
				// {"cmd":"getConnections","parm_1":applHandle,"parm_2":uow_id,"parm_3":activity_id,"name":"OTSTEST","metricOnDemand":"podInfSqlDetail"}				
				String cmd_podInfSqlDetail = "{\"cmd\":\"getConnections\",\"parm_1\":\"" + applHandle + "\",\"parm_2\":\"" + uow_id + "\",\"parm_3\":\"" + activity_id + "\",\"name\":\"OTSTEST\",\"metricOnDemand\":\"podInfSqlDetail\"}";
				result = ms.callMonLiteService(cmd_podInfSqlDetail);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
			}
		}		
	}
	
	@Test
	public void TestPodInfSqlText() throws FileNotFoundException, IOException, InterruptedException
	{				
		logger.info("Run query odInfSql and get in-flight statement's application handle ..."); 
		
		ms = new MonLiteService(this.dbProfile);
		JSONObject result = ms.callMonLiteService(ms.getJSONData("metricOnDemand_odInfSql"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}				
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odInfSql");			
		if ( !metricData.isEmpty() )
		{
			JSONArray dataArray = metricData.getJSONArray("data");
			if ( !dataArray.isEmpty() )
			{
				JSONObject infStmt = (JSONObject) dataArray.get(0);
				String applHandle = infStmt.getString("APPLICATION_HANDLE");
				if ( applHandle == null )
				{
					Assert.fail("Application handle is invalid: null" );
				}	
				
				logger.info("Running query podInfSqlText ... "); 
				
				// {"cmd":"getConnections","parm_1":applHandle,"name":"OTSTEST","metricOnDemand":"podInfSqlText"}				
				String cmd_podInfSqlText = "{\"cmd\":\"getConnections\",\"parm_1\":\"" + applHandle + "\",\"name\":\"OTSTEST\",\"metricOnDemand\":\"podInfSqlText\"}";
				result = ms.callMonLiteService(cmd_podInfSqlText);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
			}
		}					
	}
	
	@Test
	public void TestPodMonCurSqlDetail() throws FileNotFoundException, IOException, InterruptedException
	{				
		logger.info("Run query odInfSql and get in-flight statement's application handle ..."); 
		
		ms = new MonLiteService(this.dbProfile);
		JSONObject result = ms.callMonLiteService(ms.getJSONData("metricOnDemand_odInfSql"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odInfSql");			
		if ( !metricData.isEmpty() )
		{
			JSONArray dataArray = metricData.getJSONArray("data");
			if ( !dataArray.isEmpty() )
			{
				JSONObject infStmt = (JSONObject) dataArray.get(0);
				String applHandle = infStmt.getString("APPLICATION_HANDLE");
				if ( applHandle == null )
				{
					Assert.fail("Application handle is invalid: null" );
				}	
				
				logger.info("Running query podMonCurSqlDetail ... "); 
				
				// {"cmd":"getConnections","parm_1":applHandle,"parm_2":applHandle,"parm_3":applHandle,"name":"OTSTEST","metricOnDemand":"podMonCurSqlDetail"}				
				String cmd_podMonCurSqlDetail = "{\"cmd\":\"getConnections\",\"parm_1\":\"" + applHandle + "\",\"parm_2\":\"" + applHandle + "\",\"parm_3\":\"" + applHandle + "\",\"name\":\"OTSTEST\",\"metricOnDemand\":\"podMonCurSqlDetail\"}";
				result = ms.callMonLiteService(cmd_podMonCurSqlDetail);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
			}
		}									
	}
	
	@Test
	public void TestPodMonConnectionDetails() throws FileNotFoundException, IOException, InterruptedException
	{				
		logger.info("Run query sessions and get connection's application handle ..."); 
		
		ms = new MonLiteService(this.dbProfile);
		JSONObject result = ms.callMonLiteService(ms.getJSONData("metricRealtime_sessions"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.sessions");			
		if ( !metricData.isEmpty() )
		{
			JSONArray dataArray = metricData.getJSONArray("data");
			if ( !dataArray.isEmpty() )
			{
				JSONObject conn = (JSONObject) dataArray.get(0);
				String applHandle = conn.getString("APPLICATION_HANDLE");
				if ( applHandle == null )
				{
					Assert.fail("Application handle is invalid: null" );
				}	
				
				logger.info("Running query podMonConnectionDetails ... "); 
				
				// {"cmd":"getConnections","parm_1":applHandle,"name":"OTSTEST","metricOnDemand":"podMonConnectionDetails"}				
				String cmd_podMonConnectionDetail = "{\"cmd\":\"getConnections\",\"parm_1\":\"" + applHandle + "\",\"name\":\"OTSTEST\",\"metricOnDemand\":\"podMonConnectionDetails\"}";
				result = ms.callMonLiteService(cmd_podMonConnectionDetail);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
			}
		}									
	}
	
	@Test
	public void TestPodMonConnectionDetailsDpf() throws FileNotFoundException, IOException, InterruptedException
	{				
		logger.info("Run query throughputConnection and get application handle"); 
		
		ms = new MonLiteService(this.dbProfile);
		JSONObject result = ms.callMonLiteService(ms.getJSONData("metricRealtime_throughputConnection"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.throughputConnection");			
		if ( !metricData.isEmpty() )
		{
			JSONArray dataArray = metricData.getJSONArray("data");
			if ( !dataArray.isEmpty() )
			{
				JSONObject throughputConn = (JSONObject) dataArray.get(0);
				String applHandle = throughputConn.getString("APPLICATION_HANDLE");
				if ( applHandle == null )
				{
					Assert.fail("Application handle is invalid: null" );
				}	
				
				logger.info("Running query podThroughputConnectionDpf ... "); 
				
				// {"cmd":"getConnections","parm_1":applHandle,"name":"OTSTEST","metricOnDemandDelta":"podThroughputConnectionDpf"}				
				String cmd_podThroughputConnectionDpf = "{\"cmd\":\"getConnections\",\"parm_1\":\"" + applHandle + "\",\"name\":\"OTSTEST\",\"metricOnDemandDelta\":\"podThroughputConnectionDpf\"}";
				result = ms.callMonLiteService(cmd_podThroughputConnectionDpf);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}						
				metricData = JsonPath.read(responseData, "$data[0].[1].metric.podThroughputConnectionDpf");			
				if ( !metricData.isEmpty() )
				{
					dataArray = metricData.getJSONArray("data");
					if ( !dataArray.isEmpty() )
					{
						JSONObject throughputConnMember = (JSONObject) dataArray.get(0);
						applHandle = throughputConnMember.getString("APPLICATION_HANDLE");
						Integer member = throughputConnMember.getInt("MEMBER");
						if ( applHandle == null )
						{
							Assert.fail("Application handle is invalid: null" );
						}
						if ( member == null )
						{
							Assert.fail("Member is invalid: null" );
						}
						
						logger.info("Running query podMonConnectionDetailsDpf ... "); 
						
						// {"cmd":"getConnections","parm_1":applHandle,"parm_2":member,"name":"OTSTEST","metricOnDemand":"podMonConnectionDetailsDpf"}				
						String cmd_podMonConnectionDetailsDpf = "{\"cmd\":\"getConnections\",\"parm_1\":\"" + applHandle + "\",\"parm_2\":\"" + member + "\",\"name\":\"OTSTEST\",\"metricOnDemand\":\"podMonConnectionDetailsDpf\"}";
						result = ms.callMonLiteService(cmd_podMonConnectionDetailsDpf);
						responseCode = (Integer)result.get("ResponseCode");
						if ( responseCode != 200 )
						{
							assertResponseFailure(responseCode, result);
						}
						// Check for exception in the response
						responseData = (JSONObject) result.get("ResponseData");
						if ( isExceptionCaught(responseData) )
						{			
							assertExceptionFailure(responseCode, result);
						}	
						
						logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));						
					}
				}		
			}
		}									
	}
	
	@Test
	public void TestPodMonGetMemoryPoolDpf() throws FileNotFoundException, IOException, InterruptedException
	{				
		logger.info("Run query monGetMemoryPool and get memory pool type ..."); 
		
		ms = new MonLiteService(this.dbProfile);
		JSONObject result = ms.callMonLiteService(ms.getJSONData("metricRealtime_monGetMemoryPool"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.monGetMemoryPool");			
		if ( !metricData.isEmpty() )
		{
			JSONArray dataArray = metricData.getJSONArray("data");
			if ( !dataArray.isEmpty() )
			{
				JSONObject conn = (JSONObject) dataArray.get(0);
				String memPoolType = conn.getString("MEMORY_POOL_TYPE");
				if ( memPoolType == null )
				{
					Assert.fail("Memory pool type is invalid: null" );
				}	
				
				logger.info("Running query podMonGetMemoryPoolDpf ... "); 
				
				// {"cmd":"getConnections","parm_1":memPoolType,"name":"OTSTEST","metricOnDemand":"podMonGetMemoryPoolDpf"}				
				String cmd_podMonGetMemoryPoolDpf = "{\"cmd\":\"getConnections\",\"parm_1\":\"" + memPoolType + "\",\"name\":\"OTSTEST\",\"metricOnDemand\":\"podMonGetMemoryPoolDpf\"}";
				result = ms.callMonLiteService(cmd_podMonGetMemoryPoolDpf);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
			}
		}									
	}
	
	@Test
	public void TestPodInstMemSetDpf() throws FileNotFoundException, IOException, InterruptedException
	{				
		logger.info("Run query instMemSet and get instance memory set's host name, db name, memory set type ..."); 
		
		ms = new MonLiteService(this.dbProfile);
		JSONObject result = ms.callMonLiteService(ms.getJSONData("metricRealtime_instMemSet"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.instMemSet");			
		if ( !metricData.isEmpty() )
		{
			JSONArray dataArray = metricData.getJSONArray("data");
			if ( !dataArray.isEmpty() )
			{
				JSONObject memSet = (JSONObject) dataArray.get(0);
				String hostName = memSet.getString("HOST_NAME");
				String dbName = memSet.getString("DB_NAME");
				String memSetType = memSet.getString("MEMORY_SET_TYPE");
				if ( hostName == null )
				{
					Assert.fail("Host name is invalid: null" );
				}
				if ( dbName == null )
				{
					Assert.fail("DB name is invalid: null" );
				}
				if ( memSetType == null )
				{
					Assert.fail("Memor set type is invalid: null" );
				}
				
				logger.info("Running query podInstMemSetDpf ... "); 
				
				// {"cmd":"getConnections","parm_1":hostName,"parm_2":dbName,"parm_3":memSetType,"name":"OTSTEST","metricOnDemand":"podInstMemSetDpf"}				
				String cmd_podInstMemSetDpf = "{\"cmd\":\"getConnections\",\"parm_1\":\"" + hostName + "\",\"parm_2\":\"" + dbName + "\",\"parm_3\":\"" + memSetType + "\",\"name\":\"OTSTEST\",\"metricOnDemand\":\"podInstMemSetDpf\"}";
				result = ms.callMonLiteService(cmd_podInstMemSetDpf);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
			}
		}									
	}
	
	@Test
	public void TestPodTableStorageDpf() throws FileNotFoundException, IOException, InterruptedException
	{				
		logger.info("Run query odTableStorage and get table schema and name ..."); 
		
		ms = new MonLiteService(this.dbProfile);
		JSONObject result = ms.callMonLiteService(ms.getJSONData("metricOnDemand_odTableStorage"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odTableStorage");			
		if ( !metricData.isEmpty() )
		{
			JSONArray dataArray = metricData.getJSONArray("data");
			if ( !dataArray.isEmpty() )
			{
				JSONObject table = (JSONObject) dataArray.get(0);
				String tabSchema = table.getString("TABSCHEMA");
				String tabName = table.getString("TABNAME");
				if ( tabSchema == null || tabName == null )
				{
					Assert.fail("Table name is invalid: null" );
				}	
				
				logger.info("Running query podTableStorageDpf ... "); 
				
				// {"cmd":"getConnections","parm_1":tabSchema,"parm_2":tabName,"name":"OTSTEST","metricOnDemandDelta":"podTableStorageDpf"}				
				String cmd_podTableStorageDpf = "{\"cmd\":\"getConnections\",\"parm_1\":\"" + tabSchema + "\",\"parm_2\":\"" + tabName + "\",\"name\":\"OTSTEST\",\"metricOnDemandDelta\":\"podTableStorageDpf\"}";
				result = ms.callMonLiteService(cmd_podTableStorageDpf);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));		
				
			}
		}									
	}
	
	// URL parms {"cmd":"getTablespaceDpfRealtimeFallbackPod","parm_1":"TS1","name":"OTSTEST","metricOnDemandDelta":"getTablespaceDpfRealtimeFallbackPod"}
	// can be used to trigger running monGetTablespaceDpf query. When UI check box "Collect data for dpf and purescale ..." is NOT selected (monGetTablespaceDpf data
	// is not collected), then podMonGetTablespaceDpf query is next run to get the data. To make sure podMonGetTablespaceDpf is tested no matter what UI check box setting is,
	// this test manually calls {"cmd":"getConnections","parm_1":tsName","name":"OTSTEST","metricOnDemandDelta":"podMonGetTablespaceDpf"} to trigger running 
	// podMonGetTablespaceDpf query. So this test covers query monGetTablespaceDpf and podMonGetTablespaceDpf.
	@Test(enabled = false)
	public void TestTablespaceDpf() throws FileNotFoundException, IOException, InterruptedException
	{				
		logger.info("Run query monGetTablespace and get tablespace name ..."); 
		
		ms = new MonLiteService(this.dbProfile);
		JSONObject result = ms.callMonLiteService(ms.getJSONData("metricRealtime_monGetTablespace"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}			
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.monGetTablespace");			
		if ( !metricData.isEmpty() )
		{
			JSONArray dataArray = metricData.getJSONArray("data");
			if ( !dataArray.isEmpty() )
			{
				JSONObject tableSpace = (JSONObject) dataArray.get(0);
				String tsName = tableSpace.getString("TBSP_NAME");
				if ( tsName == null )
				{
					Assert.fail("Tablespace name is invalid: null" );
				}	
				
				logger.info("Running command getTablespaceDpfRealtimeFallbackPod ... "); 
				
				// {"cmd":"getTablespaceDpfRealtimeFallbackPod","parm_1":tsName,"name":"OTSTEST","metricOnDemandDelta":"getTablespaceDpfRealtimeFallbackPod"}				
				String cmd_realtimeFallbackPod = "{\"cmd\":\"getTablespaceDpfRealtimeFallbackPod\",\"parm_1\":\"" + tsName + "\",\"name\":\"OTSTEST\",\"metricOnDemandDelta\":\"getTablespaceDpfRealtimeFallbackPod\"}";
				result = ms.callMonLiteService(cmd_realtimeFallbackPod);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}		
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));			
				
				logger.info("Running query podMonGetTablespaceDpf ... ");
				
				// {"cmd":"getConnections","parm_1":tsName","name":"OTSTEST","metricOnDemandDelta":"podMonGetTablespaceDpf"}
				String cmd_podMonGetTablespaceDpf="{\"cmd\":\"getConnections\",\"parm_1\":\"" + tsName + "\",\"name\":\"OTSTEST\",\"metricOnDemandDelta\":\"podMonGetTablespaceDpf\"}";
				result = ms.callMonLiteService(cmd_podMonGetTablespaceDpf);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));
			}
		}								
	}
	
	@Test
	public void TestPodTbspStorageDpf() throws FileNotFoundException, IOException, InterruptedException
	{				
		logger.info("Run query odTbspStorage and get tablespace name ..."); 
		
		ms = new MonLiteService(this.dbProfile);
		JSONObject result = ms.callMonLiteService(ms.getJSONData("metricOnDemand_odTbspStorage"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odTbspStorage");			
		if ( !metricData.isEmpty() )
		{
			JSONArray dataArray = metricData.getJSONArray("data");
			if ( !dataArray.isEmpty() )
			{
				JSONObject tablespace = (JSONObject) dataArray.get(0);
				String tspName = tablespace.getString("TBSP_NAME");
				if ( tspName == null )
				{
					Assert.fail("Tablespace name is invalid: null" );
				}	
				
				logger.info("Running query podTbspStorageDpf ... "); 
				
				// {"cmd":"getConnections","parm_1":tspName,"name":"OTSTEST","metricOnDemandDelta":"podTbspStorageDpf"}				
				String cmd_podTbspStorageDpf = "{\"cmd\":\"getConnections\",\"parm_1\":\"" + tspName + "\",\"name\":\"OTSTEST\",\"metricOnDemandDelta\":\"podTbspStorageDpf\"}";
				result = ms.callMonLiteService(cmd_podTbspStorageDpf);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
			}
		}									
	}
	
	private static void assertResponseFailure(int responseCode, JSONObject result)
	{
		Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));
	}
	
	private static void assertExceptionFailure(int responseCode, JSONObject result)
	{
		Assert.fail("FAILURE:=>ExceptionCaught=true in responseJson for URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData")+" with RESPONSECODE:"+responseCode+" and RESPONSEDATA:"+result.get("ResponseData"));		
	}
	
	private static boolean isExceptionCaught(JSONObject responseData)  
	{
		return responseData.toString().contains("\"exceptionCaught\":true");
	}
}
