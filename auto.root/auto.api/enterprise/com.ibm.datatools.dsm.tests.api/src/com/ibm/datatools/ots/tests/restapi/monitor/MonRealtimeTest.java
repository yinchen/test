package com.ibm.datatools.ots.tests.restapi.monitor;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.common.ConnectionService;
import com.ibm.datatools.ots.tests.restapi.common.DatabaseService;
import com.ibm.datatools.ots.tests.restapi.common.MonHistoryService;
import com.ibm.datatools.ots.tests.restapi.common.MonLiteService;
import com.ibm.datatools.ots.tests.restapi.common.MonRealtimeService;
import com.ibm.datatools.test.utils.FileTools;
import com.jayway.jsonpath.JsonPath;

public class MonRealtimeTest extends MonitorTestBase
{
	private final Logger logger = LogManager.getLogger(getClass());	
	private MonRealtimeService ms;
	private DatabaseService databaseService = new DatabaseService();;

	@DataProvider(name="testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException{
	
		ms = new MonRealtimeService(this.dbProfile);
		Properties p = ms.getJSONProperties();
		return FileTools.readProperties(p);	
	}
	
	/**
	 * Add by huangxue
	 * Test monitor realtime mode
	    */
	@Test(dataProvider = "testdata")
	public void testMonRealtime(Object key ,Object jsonObj) throws FileNotFoundException, IOException
	{
		
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		JSONObject result;
		logger.info("Running monitor history and alert backend query ..."); 
		if(jsonObj.toString().contains("metricMode")&&jsonObj.toString().contains("REALTIME")){   //add by miao hai
			result = ms.callMonRealtimeBufferService(jsonObj.toString());
		}else{
			 result = ms.callMonRealtimeService(jsonObj.toString());
		}
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");	
		
		boolean exceptionCaught = responseData.toString().contains("\"exceptionCaught\":true");
		if ( exceptionCaught )
		{
			assertExceptionFailure(responseCode, result);
		}
		
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));	
	}


	/**
	 * Add by huangxue
	 * Test get In-Light statement
	     */
	
	@Test
	public void TestgetInLight() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query In-Light statement ..."); 
		ms = new MonRealtimeService(this.dbProfile);
		JSONObject result = ms.callMonRealtimeService(ms.getJSONData("Get_In_Flight_with_System_Statement"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odInfSql");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );	
		else
		{
		JSONArray resultArray = metricData.getJSONArray("data");
		if ( resultArray.isEmpty() )
			Assert.fail("No result is got" );
		}	
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}
	
	/**
	 * Add by Sun Yu Ping
	 * Test get Inflight Executions
		     */
	
	@Test
	public void TestGetInflightExecutions() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query  Inflight Executions ..."); 
		ms = new MonRealtimeService(this.dbProfile);
		JSONObject result = ms.callMonRealtimeService(ms.getJSONData("Get_Inflight_Executions_with_System_Statement"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odInfSql");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );	
		else
		{
		JSONArray resultArray = metricData.getJSONArray("data");
		if ( resultArray.isEmpty() )
			Assert.fail("No result is got" );
		}	
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}
	
	/**
	 * Add by Sun Yu Ping
	 * Test get Stored Procedures
		     */
	
	@Test
	public void TestGetStoredProcedures() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query  Stored Procedures ..."); 
		ms = new MonRealtimeService(this.dbProfile);
		JSONObject result = ms.callMonRealtimeService(ms.getJSONData("Get_Stored_Procedures"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
//      no exceptionCaught 
//		boolean exceptionCaught = JsonPath.read(responseData, "$data[0].[1].exceptionCaught");
//		if ( exceptionCaught )
//		{
//			String exceptionMsg =  JsonPath.read(responseData, "$data[0].[1].exceptionMsg");
//			Assert.fail("Get_Stored_Procedures exception caught: " + exceptionMsg );	
//		}
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}
	
	/**
	 * Add by miaohai
	 * Test get Buffer Pool
	     */
	
	@Test
	public void TestgetBufferPool() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query In-Light statement ..."); 
		ms = new MonRealtimeService(this.dbProfile);
		JSONObject result = ms.callMonRealtimeBufferService(ms.getJSONData("Get_BufferPoolRealtime"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}

	
	/**
	 * Add by huangxue
	 * Test cancel activities of a In-Light statement
	     */
	
	@Test
	public void TestCancelActivities() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query In-Light statement ..."); 
		ms = new MonRealtimeService(this.dbProfile);
		JSONObject result = ms.callMonRealtimeService(ms.getJSONData("Get_In_Flight_with_System_Statement"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odInfSql");
		if ( !metricData.isEmpty() )
		{
			JSONArray dataArray = metricData.getJSONArray("data");
			if ( !dataArray.isEmpty() )
			{
				JSONObject conn = (JSONObject) dataArray.get(0);
				String applHandle = conn.getString("APPLICATION_HANDLE");
				String activityID = conn.getString("ACTIVITY_ID");
				String uowID= conn.getString("UOW_ID");
				
				if ( applHandle == null || activityID == null || uowID == null)
				{
					Assert.fail("Handle is invalid: null" );
				}	
				
				logger.info("Cancel activities of a In-Light statement ... "); 
					
				String Jsondata="{\"__dummyKey\": \"__dummyValue\"}";
				result = ms.callMonRealtimeCancelActivity(Jsondata,applHandle,uowID,activityID);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				//responseData = (JSONObject) result.get("ResponseData");
				//Assert.assertEquals(responseData.get("resultCode"),"SUCCESS");
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
			}
		}							
	}

	/**
	 * Add by huangxue
	 * Test force a In-Light statement
		     */
	
	@Test
	public void TestforceStatement() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query In-Light statement ..."); 
		ms = new MonRealtimeService(this.dbProfile);
		JSONObject result = ms.callMonRealtimeService(ms.getJSONData("Get_In_Flight_with_System_Statement"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odInfSql");
		if ( !metricData.isEmpty() )
		{
			JSONArray dataArray = metricData.getJSONArray("data");
			if ( !dataArray.isEmpty() )
			{
				JSONObject conn = (JSONObject) dataArray.get(0);
				String applHandle = conn.getString("APPLICATION_HANDLE");
				if ( applHandle == null )
				{
					Assert.fail("Application handle is invalid: null" );
				}	
				
				logger.info("Force the application ... "); 
					
				String Jsondata="{\"__dummyKey\": \"__dummyValue\"}";
				result = ms.callMonRealtimeForce(Jsondata,applHandle);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				Assert.assertEquals(responseData.get("resultCode"),"SUCCESS");
				
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
			}
		}							
	}

	
	/**
	 * Add by huangxue
	 * Test get Package Cache by NumExec
		     */
	
	@Test
	public void TestgetPackageCacheNumExec() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query  Package Cache ..."); 
		ms = new MonRealtimeService(this.dbProfile);
		JSONObject result = ms.callMonRealtimeService(ms.getJSONData("Get_PackageCache_NumExec_with_System_Statement"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odMonGetPackageCacheByNumExec");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );	
		else
		{
		JSONArray resultArray = metricData.getJSONArray("data");
		if ( resultArray.isEmpty() )
			Assert.fail("No result is got" );
		}	
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}

	/**
	 * Add by huangxue
	 * Test get Package Cache by ActTime
		     */
	
	@Test
	public void TestgetPackageCacheActTime() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query  Package Cache ..."); 
		ms = new MonRealtimeService(this.dbProfile);
		JSONObject result = ms.callMonRealtimeService(ms.getJSONData("Get_PackageCache_ActTime_with_System_Statement"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odMonGetPackageCacheByActTime");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );	
		else
		{
		JSONArray resultArray = metricData.getJSONArray("data");
		if ( resultArray.isEmpty() )
			Assert.fail("No result is got" );
		}	
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}
	

	/**
	 * Add by huangxue
	 * Test get Package Cache by RowsRead
		     */
	
	@Test
	public void TestgetPackageCacheRowsRead() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query  Package Cache ..."); 
		ms = new MonRealtimeService(this.dbProfile);
		JSONObject result = ms.callMonRealtimeService(ms.getJSONData("Get_PackageCache_RowsRead_with_System_Statement"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odMonGetPackageCacheByRowsRead");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );	
		else
		{
		JSONArray resultArray = metricData.getJSONArray("data");
		if ( resultArray.isEmpty() )
			Assert.fail("No result is got" );
		}	
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}

	
	/**
	 * Add by huangxue
	 * Test get Package Cache by RowsReturned
		     */
	
	@Test
	public void TestgetPackageCacheRowsReturned() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query  Package Cache ..."); 
		ms = new MonRealtimeService(this.dbProfile);
		JSONObject result = ms.callMonRealtimeService(ms.getJSONData("Get_PackageCache_RowsReturned_with_System_Statement"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odMonGetPackageCacheByRowsReturned");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );	
		else
		{
		JSONArray resultArray = metricData.getJSONArray("data");
		if ( resultArray.isEmpty() )
			Assert.fail("No result is got" );
		}	
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}

	/**
	 * Add by huangxue
	 * Test get Package Cache by TotalSorts
		     */
	
	@Test
	public void TestgetPackageCacheTotalSorts() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query  Package Cache ..."); 
		ms = new MonRealtimeService(this.dbProfile);
		JSONObject result = ms.callMonRealtimeService(ms.getJSONData("Get_PackageCache_TotalSorts_with_System_Statement"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odMonGetPackageCacheByTotalSorts");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );	
		else
		{
		JSONArray resultArray = metricData.getJSONArray("data");
		if ( resultArray.isEmpty() )
			Assert.fail("No result is got" );
		}	
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}

	/**
	 * Add by huangxue
	 * Test get Package Cache by LockWaitTime
		     */
	
	@Test
	public void TestgetPackageCacheLockWaitTime() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query  Package Cache ..."); 
		ms = new MonRealtimeService(this.dbProfile);
		JSONObject result = ms.callMonRealtimeService(ms.getJSONData("Get_PackageCache_LockWaitTime_with_System_Statement"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odMonGetPackageCacheByLockWaitTime");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );	
		else
		{
		JSONArray resultArray = metricData.getJSONArray("data");
		if ( resultArray.isEmpty() )
			Assert.fail("No result is got" );
		}	
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}

	/**
	 * Add by huangxue
	 * Test get Package Cache by PoolReadTime
		     */
	
	@Test
	public void TestgetPackageCachePoolReadTime() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query  Package Cache ..."); 
		ms = new MonRealtimeService(this.dbProfile);
		JSONObject result = ms.callMonRealtimeService(ms.getJSONData("Get_PackageCache_PoolReadTime_with_System_Statement"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odMonGetPackageCacheByPoolReadTime");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );	
		else
		{
		JSONArray resultArray = metricData.getJSONArray("data");
		if ( resultArray.isEmpty() )
			Assert.fail("No result is got" );
		}	
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}

	/**
	 * Add by huangxue
	 * Test get Package Cache by ActTime
	     */	
	
	@Test
	public void TestgetPackageCacheTotalActTime() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query  Package Cache ..."); 
		ms = new MonRealtimeService(this.dbProfile);
		JSONObject result = ms.callMonRealtimeService(ms.getJSONData("Get_PackageCache_ActTime_with_System_Statement_Total"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odMonGetPackageCacheByTotalActTime");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );	
		else
		{
		JSONArray resultArray = metricData.getJSONArray("data");
		if ( resultArray.isEmpty() )
			Assert.fail("No result is got" );
		}	
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}

	
	/**
	 * Add by huangxue
	 * Test get Package Cache by RowsRead
		     */
	
	@Test
	public void TestgetPackageCacheTotalRowsRead() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query  Package Cache ..."); 
		ms = new MonRealtimeService(this.dbProfile);
		JSONObject result = ms.callMonRealtimeService(ms.getJSONData("Get_PackageCache_RowsRead_with_System_Statement_Total"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odMonGetPackageCacheByTotalRowsRead");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );	
		else
		{
		JSONArray resultArray = metricData.getJSONArray("data");
		if ( resultArray.isEmpty() )
			Assert.fail("No result is got" );
		}	
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}

	
	/**
	 * Add by huangxue
	 * Test get Package Cache by RowsReturned
		     */
	
	@Test
	public void TestgetPackageCacheTotalRowsReturned() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query  Package Cache ..."); 
		ms = new MonRealtimeService(this.dbProfile);
		JSONObject result = ms.callMonRealtimeService(ms.getJSONData("Get_PackageCache_RowsReturned_with_System_Statement_Total"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odMonGetPackageCacheByTotalRowsReturned");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );	
		else
		{
		JSONArray resultArray = metricData.getJSONArray("data");
		if ( resultArray.isEmpty() )
			Assert.fail("No result is got" );
		}	
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}

	/**
	 * Add by huangxue
	 * Test get Package Cache by TotalSorts
		     */
	
	@Test
	public void TestgetPackageCacheTotalTotalSorts() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query  Package Cache..."); 
		ms = new MonRealtimeService(this.dbProfile);
		JSONObject result = ms.callMonRealtimeService(ms.getJSONData("Get_PackageCache_TotalSorts_with_System_Statement_Total"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odMonGetPackageCacheByTotalTotalSorts");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );	
		else
		{
		JSONArray resultArray = metricData.getJSONArray("data");
		if ( resultArray.isEmpty() )
			Assert.fail("No result is got" );
		}	
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}

	/**
	 * Add by huangxue
	 * Test get Package Cache by LockWaitTime
		     */
	
	@Test
	public void TestgetPackageCacheTotalLockWaitTime() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query  Package Cache ..."); 
		ms = new MonRealtimeService(this.dbProfile);
		JSONObject result = ms.callMonRealtimeService(ms.getJSONData("Get_PackageCache_LockWaitTime_with_System_Statement_Total"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odMonGetPackageCacheByTotalLockWaitTime");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );	
		else
		{
		JSONArray resultArray = metricData.getJSONArray("data");
		if ( resultArray.isEmpty() )
			Assert.fail("No result is got" );
		}	
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}

	/**
	 * Add by huangxue
	 * Test get Package Cache by PoolReadTime
		     */
	
	@Test
	public void TestgetPackageCacheTotalPoolReadTime() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query  Package Cache ..."); 
		ms = new MonRealtimeService(this.dbProfile);
		JSONObject result = ms.callMonRealtimeService(ms.getJSONData("Get_PackageCache_PoolReadTime_with_System_Statement_Total"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odMonGetPackageCacheByTotalPoolReadTime");
		if ( metricData.isEmpty() )
			Assert.fail("No result is got" );	
		else
		{
		JSONArray resultArray = metricData.getJSONArray("data");
		if ( resultArray.isEmpty() )
			Assert.fail("No result is got" );
		}	
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
										
	}

	/**
	 * Add by huangxue
	 * Test Detail of Package Cache
		     */
	
	@Test
	public void TestgetDetailofPackageCache() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Package Cache ..."); 
		ms = new MonRealtimeService(this.dbProfile);
		JSONObject result = ms.callMonRealtimeService(ms.getJSONData("Get_PackageCache_NumExec_with_System_Statement"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odMonGetPackageCacheByNumExec");
		if ( !metricData.isEmpty() )
		{
			JSONArray resultArray = metricData.getJSONArray("data");
			if ( !resultArray.isEmpty() )
			{
				JSONObject indi = (JSONObject) resultArray.get(0);
				String executableID= indi.getString("EXECUTABLE_ID");
				
				if ( executableID == null )
				{
					Assert.fail("Executable ID is invalid: null" );
				}	
				
				logger.info("Running query  Package Cache Details ... "); 		
				String cmd_getPackageCacheDetails = "{\"cmd\":\"odMonGetPackageCacheFullSQL\",\"name\":\"OTSTEST\",\"odMonGetPackageCacheFullSQL\":\""+executableID+"\"}";
				result = ms.callMonRealtimeService(cmd_getPackageCacheDetails);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				Assert.assertNotNull(responseData);
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				JSONObject SQLData1 = JsonPath.read(responseData, "$data");		
				Assert.assertNotNull(SQLData1.get("SQL_TEXT"));
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
			}
		}	
	}

	/**
	 * Add by huangxue
	 * Test Detail of Blocking Connection
		     */
	
	@Test
	public void TestgetDetailofBlockingConnection() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Blocking Connection ..."); 
		ms = new MonRealtimeService(this.dbProfile);
		JSONObject result = ms.callMonRealtimeService(ms.getJSONData("Get_getBlockersWaitersRealtime"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.odLockBlockers");
		if ( !metricData.isEmpty() &&  metricData.containsKey("data"))
		{
			JSONArray resultArray = metricData.getJSONArray("data");
			if ( !resultArray.isEmpty() )
			{
				JSONObject indi = (JSONObject) resultArray.get(0);
				Integer AppID= indi.getInt("BLOCKER_HOLDER_APPLICATION_HANDLE");
				
				if ( AppID == null )
				{
					Assert.fail("Application ID is invalid: null" );
				}	
				
				logger.info("Running query SQL Details ... "); 		
				String cmd_getSQLDetails = "{\"cmd\": \"getConnectionLastExecutableIdSQL\",\"name\": \"repo\",\"id\":\""+AppID+"\"}";	
				result = ms.callMonRealtimeService(cmd_getSQLDetails);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				Assert.assertNotNull(responseData);
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				JSONObject DetailData = JsonPath.read(responseData, "$data[0]");
				Assert.assertNotNull(DetailData);
								
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
			}
		}	
	}

	
	/**
	 * Add by huangxue
	 * Test Detail of Connection Statistics
		     */
	
	@Test
	public void TestgetDetailofConnectionStatistics() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Run query Connection Statistics ..."); 
		ms = new MonRealtimeService(this.dbProfile);
		String cmd_getConnectionStatistics ="{\"cmd\": \"getConnections\",\"metricRealtime\": \"sessions\",\"name\": \"repo\"}";
		JSONObject result = ms.callMonRealtimeService(cmd_getConnectionStatistics);
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.sessions");
		if ( !metricData.isEmpty() )
		{
			JSONArray resultArray = metricData.getJSONArray("data");
			if ( !resultArray.isEmpty() )
			{
				JSONObject indi = (JSONObject) resultArray.get(0);
				Integer AppID= indi.getInt("APPLICATION_HANDLE");
				
				if ( AppID == null )
				{
					Assert.fail("Application ID is invalid: null" );
				}	
				
				logger.info("Running query Connection Details ... "); 		
				String cmd_getConnectionDetails = "{\"cmd\": \"getConnections\",\"inc_connprop\": \"false\", \"metricOnDemand\": \"podMonConnectionDetails\",\"name\": \"repo\",\"parm_1\":\""+AppID+"\"}";
				//String cmd_getCurSqlDetail = "{\"cmd\": \"getConnections\",\"inc_connprop\": \"false\", \"metricOnDemand\": \"podMonCurSqlDetail\",\"name\": \"repo\",\"parm_1\":"+AppID+"\",\"parm_2\":"+AppID+"\",\"parm_3\":"+AppID+"\"}"; Defect 91342
				//String cmd_getInfSqlText = "{\"cmd\": \"getConnections\", \"metricOnDemand\": \"podInfSqlText\",\"name\": \"repo\",\"parm_1\":"+AppID+"\"}";Defect 91342
				
				result = ms.callMonRealtimeService(cmd_getConnectionDetails);
				responseCode = (Integer)result.get("ResponseCode");
				if ( responseCode != 200 )
				{
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				Assert.assertNotNull(responseData);
				if ( isExceptionCaught(responseData) )
				{			
					assertExceptionFailure(responseCode, result);
				}	
				JSONObject DetailData = JsonPath.read(responseData, "$data[0].[1].metric.podMonConnectionDetails");
				Assert.assertNotNull(DetailData);
				Assert.assertNotNull(DetailData.getJSONArray("data"));
								
				logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));				
			}
		}	
	}

	
	/**
	 * Add by huangxue
	 * Test rearch locked object
	     */
	
	@Test
	public void TestSearchLockedObject() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Rearch Locked Object..."); 
		ms = new MonRealtimeService(this.dbProfile);
		String cmd_rearchLockedObject ="{\"cmd\": \"getConnections\",\"metricOnDemandDelta\": \"podObjectAllLocks\",\"metricOnDemandDelta_calcSUM\": \"podObjectAllLocks/WAITER_TOTAL_WAIT_TIME\",\"name\": \"repo\",\"parm_1\": \"%eqre%\",\"parm_2\": \"%%\"}";
		JSONObject result = ms.callMonRealtimeService(cmd_rearchLockedObject);
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric");
		Assert.assertNotNull(metricData);
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));					
	}

	
	/**
	 * Add by huangxue
	 * Test view workload:Throughput Summary, Database Time Spent,Operating System Time Spent,Units of Work (UOW)
    */
	
	@Test
	public void TestViewWorkload() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("View workload..."); 
		ms = new MonRealtimeService(this.dbProfile);
		String cmd_getThroughputSummary ="{\"cmd\": \"getConnections\",\"metricRealtime\": \"throughputConnection,throughputMember,throughputWorkload,throughputServiceclass\",\"name\": \"repo\"}";
		String cmd_getUOW ="{\"cmd\": \"getConnections\",\"metricOnDemand\": \"odMonGetUnitOfWork\",\"name\": \"repo\"}";
		String cmd_getthroughputAll ="{\"cmd\": \"getConnections\",\"metricRealtime\": \"throughputAll\",\"name\": \"repo\"}";
		String cmd_getthroughputSystem ="{\"cmd\": \"getConnections\",\"metricRealtime\": \"throughputSystem\",\"name\": \"repo\"}";
		
		JSONObject result_ThroughputSummary = ms.callMonRealtimeService(cmd_getThroughputSummary);
		int responseCode = (Integer)result_ThroughputSummary.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result_ThroughputSummary);
		}	
		// Check for exception in the response
		JSONObject responseData_ThroughputSummary = (JSONObject) result_ThroughputSummary.get("ResponseData");
		
		Assert.assertNotNull(responseData_ThroughputSummary);
		
		if ( isExceptionCaught(responseData_ThroughputSummary) )
		{			
			assertExceptionFailure(responseCode, result_ThroughputSummary);
		}	
		
		JSONObject metricData__ThroughputSummary = JsonPath.read(responseData_ThroughputSummary, "$data[0].[1].metric.throughputConnection");
		Assert.assertNotNull(metricData__ThroughputSummary);
		Assert.assertNotNull(metricData__ThroughputSummary.get("data"));
		metricData__ThroughputSummary = JsonPath.read(responseData_ThroughputSummary, "$data[0].[1].metric.throughputMember");
		Assert.assertNotNull(metricData__ThroughputSummary);
		Assert.assertNotNull(metricData__ThroughputSummary.get("data"));
		metricData__ThroughputSummary = JsonPath.read(responseData_ThroughputSummary, "$data[0].[1].metric.throughputServiceclass");
		Assert.assertNotNull(metricData__ThroughputSummary);
		Assert.assertNotNull(metricData__ThroughputSummary.get("data"));
		metricData__ThroughputSummary = JsonPath.read(responseData_ThroughputSummary, "$data[0].[1].metric.throughputWorkload");
		Assert.assertNotNull(metricData__ThroughputSummary);
		Assert.assertNotNull(metricData__ThroughputSummary.get("data"));
		
	
		JSONObject result_UOW = ms.callMonRealtimeService(cmd_getUOW);
		 responseCode = (Integer)result_UOW.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result_UOW);
		}	
		// Check for exception in the response
		JSONObject responseData_UOW = (JSONObject) result_UOW.get("ResponseData");
		
		Assert.assertNotNull(responseData_UOW);
		
		if ( isExceptionCaught(responseData_UOW) )
		{			
			assertExceptionFailure(responseCode, result_UOW);
		}	
		JSONObject metricData_UOW = JsonPath.read(responseData_UOW, "$data[0].[1].metric.odMonGetUnitOfWork");
		Assert.assertNotNull(metricData_UOW);
		Assert.assertNotNull(metricData_UOW.get("data"));
		
	
		JSONObject result_throughputAll = ms.callMonRealtimeService(cmd_getthroughputAll);
		 responseCode = (Integer)result_throughputAll.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result_throughputAll);
		}	
		// Check for exception in the response
		JSONObject responseData_throughputAl = (JSONObject) result_throughputAll.get("ResponseData");
		
		Assert.assertNotNull(responseData_throughputAl);
		
		if ( isExceptionCaught(responseData_throughputAl) )
		{			
			assertExceptionFailure(responseCode, result_throughputAll);
		}	
		JSONObject metricData_throughputAll = JsonPath.read(responseData_throughputAl, "$data[0].[1].metric.throughputAll");
		Assert.assertNotNull(metricData_throughputAll);
		Assert.assertNotNull(metricData_throughputAll.get("data"));


		JSONObject result_throughputSystem = ms.callMonRealtimeService(cmd_getthroughputSystem);
		 responseCode = (Integer)result_throughputSystem.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result_throughputSystem);
		}	
		// Check for exception in the response
		JSONObject responseData_throughputSystem = (JSONObject) result_throughputSystem.get("ResponseData");
		
		Assert.assertNotNull(responseData_throughputSystem);
		
		if ( isExceptionCaught(responseData_throughputSystem) )
		{			
			assertExceptionFailure(responseCode, result_throughputSystem);
		}	
		JSONObject metricData_throughputSystem = JsonPath.read(responseData_throughputSystem, "$data[0].[1].metric.throughputSystem");
		Assert.assertNotNull(metricData_throughputSystem);
		Assert.assertNotNull(metricData_throughputSystem.get("data"));
		
		logger.info("SUCCESS to get workload  ");					
	}
	
	
	/**
	 * Add by huangxue
	 * Test view memory
	*/
	
	@Test
	public void TestViewMemory() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("View Memory..."); 
		ms = new MonRealtimeService(this.dbProfile);
		String cmd_GetMemoryPool ="{\"cmd\": \"getConnections\",\"metricRealtime\": \"monGetMemoryPool\",\"name\": \"repo\"}";
		String cmd_instMemSet ="{\"cmd\": \"getConnections\",\"metricRealtime\": \"instMemSet\",\"name\": \"repo\"}";

		JSONObject result = ms.callMonRealtimeService(cmd_GetMemoryPool);
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.monGetMemoryPool");
		Assert.assertNotNull(metricData);
		Assert.assertNotNull(metricData.get("data"));
		
		result = ms.callMonRealtimeService(cmd_instMemSet);
		 responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		 responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		 metricData = JsonPath.read(responseData, "$data[0].[1].metric.instMemSet");
		Assert.assertNotNull(metricData);
		Assert.assertNotNull(metricData.get("data"));
		
		
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));					
	}
	
	/**
	 * Add by huangxue
	 * Test view IO
    */
	@Test
	public void TestViewIO() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("View IO..."); 
		ms = new MonRealtimeService(this.dbProfile);
		String cmd_GetBufferpool ="{\"cmd\": \"getConnections\",\"metricRealtime\": \"monGetBufferpool\",\"name\": \"repo\"}";
		String cmd_GetTablespace ="{\"cmd\": \"getConnections\",\"metricRealtime\": \"monGetTablespace\",\"name\": \"repo\"}";
		String cmd_GetTransactionLog ="{\"cmd\": \"getConnections\",\"metricRealtime\": \"monGetTransactionLog,throughputAll\",\"name\": \"repo\"}";

		JSONObject result = ms.callMonRealtimeService(cmd_GetBufferpool);
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.monGetBufferpool");
		Assert.assertNotNull(metricData);
		Assert.assertNotNull(metricData.get("data"));
		
		result = ms.callMonRealtimeService(cmd_GetTablespace);
		 responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		 responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		 metricData = JsonPath.read(responseData, "$data[0].[1].metric.monGetTablespace");
		Assert.assertNotNull(metricData);
		Assert.assertNotNull(metricData.get("data"));
		
		result = ms.callMonRealtimeService(cmd_GetTransactionLog);
		 responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		 responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		 metricData = JsonPath.read(responseData, "$data[0].[1].metric.monGetTransactionLog");
		Assert.assertNotNull(metricData);
		Assert.assertNotNull(metricData.get("data"));
		
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));					
	}

	
	/**
	 * Add by huangxue
	 * Test get Table Performance by Schema
     */
	
	@Test
	public void TestGetTablePerformance_Schema() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("View get Table Performance by Schema..."); 
		ms = new MonRealtimeService(this.dbProfile);
		String cmd_GetTablePerformance_Schema ="{\"cmd\": \"getTableStorageRealtime\",\"incSchemaOnly\": \"true\",\"name\": \"repo\"}";
		JSONObject result = ms.callMonRealtimeService(cmd_GetTablePerformance_Schema);
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0]");
		logger.info("xx"+metricData);
		logger.info("xx"+metricData.getJSONArray("results"));
		Assert.assertNotNull(metricData);
		Assert.assertNotNull(metricData.getJSONArray("results"));	
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));					
	}

	/**
	 * Add by huangxue
	 * Test get Table Performance by table
     */
	
	@Test
	public void TestGetTablePerformance_Table() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("View get Table Performance by Table..."); 
		ms = new MonRealtimeService(this.dbProfile);
		String cmd_GetTablePerformance_Table ="{\"cmd\":\"getTableStorageRealtime\",\"name\":\"OTSTEST\",\"incSchemaOnly\": \"false\",\"includeSysTables\":\"true\"}";
		JSONObject result = ms.callMonRealtimeService(cmd_GetTablePerformance_Table);
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0]");
		Assert.assertNotNull(metricData);
		Assert.assertNotNull(metricData.getJSONArray("results"));	
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));					
	}
	
	/**
	 * Add by jiahouping
	 * Test get Table Performance Show System Tables
     */
	@Test
    public void TestGetTablePerformance_showSystemTables() throws FileNotFoundException, IOException, InterruptedException
    {
    	logger.info("Checking if the connection exists...");
    	boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("View Get Table Performance show System Tables");
		ms = new MonRealtimeService(this.dbProfile);
		String cmd_GetTablePerformance_showSystemTables="{\"cmd\":\"getTableStorageRealtime\",\"includeSysTables\":\"true\", \"name\":\"OTSTEST\"}";
		JSONObject result = ms.callMonRealtimeStorage(cmd_GetTablePerformance_showSystemTables);
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	

		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
				
		Assert.assertNotNull(responseData);
				
	    if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}
	    
	    JSONObject metricData = JsonPath.read(responseData, "$data[0]");
		Assert.assertNotNull(metricData);
		Assert.assertNotNull(metricData.getJSONArray("results"));	
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));		
    	
    }
	
	
	/**
	 * Add by huangxue
	 * Test get Overview
     */
	
	/*@Test
	public void TestGetOverview() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("View get Overview..."); 
		ms = new MonRealtimeService(this.dbProfile);
		String cmd_GetOverview ="{\"cmd\": \"getOverviewMetrics\", \"name\": \"mondb1_new\"}";
		JSONObject result = ms.callMonRealtimeService(cmd_GetOverview);
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		
		Assert.assertNotNull(responseData);
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}	
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.databaseTimeBreakdownMetrics.[0]");
		Assert.assertNotNull(metricData);
		Assert.assertNotNull(metricData.getJSONArray("details"));
		
		 metricData = JsonPath.read(responseData, "$data[0].[1].metric.databaseTimeBreakdownMetrics.[0]");
		Assert.assertNotNull(metricData);
		Assert.assertNotNull(metricData.getJSONArray("display"));
		
		metricData = JsonPath.read(responseData, "$data[0].[1].metric.keyMetrics.[0]");
		Assert.assertNotNull(metricData);
		Assert.assertNotNull(metricData.getJSONArray("details"));
		
		 metricData = JsonPath.read(responseData, "$data[0].[1].metric.keyMetrics.[0]");
		Assert.assertNotNull(metricData);
		Assert.assertNotNull(metricData.getJSONArray("display"));
		
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));					
	}*/

	/**
	 * Add by huangxue
	 * Test view homepage
     */
	
	@Test
	public void TestViewHomePage() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("View Home..."); 
		ms = new MonRealtimeService(this.dbProfile);
		String cmd_ViewHome ="{\"cmd\": \"getConnections\",\"agg\": \"AVG/SSD/TREND/MAX/MIN\", \"alert\": \"open\", \"incStorageMetrics\": \"true\", \"inc_favorite\": \"true\",\"inc_group\": \"true\",\"metric\": \"throughputSystem/CPU_PERCENT,throughputAll/TOTAL_APP_COMMITS/AVG_ACTIVITY_TIME/LOGICAL_READS/DEADLOCKS/LOCK_TIMEOUTS,memDbTotalUsed/MEMORY_POOL_USED_GB,dbSummarySessions/NUM_ACTIVE_SESSIONS\",\"metricAgg\": \"throughputSystem/CPU_PERCENT,throughputAll/TOTAL_APP_COMMITS/AVG_ACTIVITY_TIME/LOGICAL_READS/DEADLOCKS/LOCK_TIMEOUTS,memDbTotalUsed/MEMORY_POOL_USED_GB,dbSummarySessions/NUM_ACTIVE_SESSIONS\", \"userid\": \"admin\"}";
		JSONObject result = ms.callMonRealtimeService(cmd_ViewHome);
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");		
		Assert.assertNotNull(responseData);
		JSONObject monitoringconn = JsonPath.read(responseData, "$data[0].[3]");
		if (!monitoringconn.containsKey("monitoringState"))
		{
		    JSONObject metricData = JsonPath.read(responseData, "$data[0].[3].metric.dbSummarySessions");
		    Assert.assertNotNull(metricData);
		    Assert.assertNotNull(metricData.getJSONArray("data"));
		
		    metricData = JsonPath.read(responseData, "$data[0].[3].metric.memDbTotalUsed");
		    Assert.assertNotNull(metricData);
		    Assert.assertNotNull(metricData.getJSONArray("data"));
		
		    metricData = JsonPath.read(responseData, "$data[0].[3].metric.throughputAll");
		    Assert.assertNotNull(metricData);
		    Assert.assertNotNull(metricData.getJSONArray("data"));
		}
		
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));					
	}

	/**
	 * Add by wzhhuabj
	 * Test baseline
     */
	
	@Test
	public void TestGetBaseline() throws FileNotFoundException, IOException, InterruptedException
	{	
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
		
		logger.info("Get baseline..."); 
		ms = new MonRealtimeService(this.dbProfile);
		String cmd_get_baseline = "{\"cmd\": \"getUserBaselines\", \"name\": \"test\", \"userid\": \"admin\"}";
		JSONObject result = ms.callMonRealtimeService(cmd_get_baseline);
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}	
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");		
		Assert.assertNotNull(responseData);
		
		JSONArray metricData = (JSONArray) JsonPath.read(responseData, "$data");
		Assert.assertNotNull(metricData);
		
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));					
	}

	
	private static void assertResponseFailure(int responseCode, JSONObject result)
	{
		Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));
	}
	
	private static void assertExceptionFailure(int responseCode, JSONObject result)
	{
		Assert.fail("FAILURE:=>ExceptionCaught=true in responseJson for URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData")+" with RESPONSECODE:"+responseCode+" and RESPONSEDATA:"+result.get("ResponseData"));		
	}
	
	private static boolean isExceptionCaught(JSONObject responseData)  
	{
		return responseData.toString().contains("\"exceptionCaught\":true");
	}
}
