package com.ibm.datatools.ots.tests.restapi.monitor;

import static org.testng.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DBUtils;
import com.ibm.datatools.ots.tests.restapi.base.RepoDBUtils;
import com.ibm.datatools.ots.tests.restapi.common.MonHistoryService;
import com.ibm.datatools.test.utils.DBEntity;
import com.ibm.datatools.test.utils.FileTools;
import com.jayway.jsonpath.JsonPath;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/*
 * Test workload performance data, insert fake data to repository table 
 * then test if monitor wl feature could handle these data well.
 * 
 * Fake data:
create table ibm_rtmon_data."throughputAllWL"
(
dbconn_int			integer not null,			
collected			timestamp not null,			
WORKLOAD_ID 	int, 							
WORKLOAD_NAME	VARCHAR(128),					
TOTAL_CPU_TIME_WL		bigint, 				1
CPU_USER				bigint,					2
CPU_SYSTEM				bigint,					3
CPU_IOWAIT				bigint,					4
CPU_IDLE				bigint,					5
logical_reads			bigint, 				6  --3600
total_act_time			bigint,					7
act_aborted_total			bigint,				8
act_completed_total		bigint, 				9
total_app_commits			bigint, 			10 	--3600
total_sorts				bigint,					11
total_compilations		bigint, 				12
deadlocks				bigint,					13
sort_overflows			bigint,					14	
total_section_proc_time	bigint, 				15  --20 must bigger than total_section_sort_proc_time 
total_routine_user_code_proc_time bigint,		16
total_section_sort_proc_time	bigint,			17
lock_wait_time	bigint,							18
cf_wait_time	bigint,							19
pool_read_time	bigint,							20
pool_write_time	bigint,							21
direct_read_time	bigint,						22
direct_write_time	bigint,						23
total_compile_proc_time	bigint,					24
total_implicit_compile_proc_time	bigint,		25
total_load_proc_time	bigint,					26
total_reorg_proc_time	bigint,					27
total_runstats_proc_time	bigint,				28
total_commit_proc_time	bigint,					29
total_rollback_proc_time	bigint,				30
total_connect_request_proc_time	bigint,			31
agent_wait_time	bigint,							32
wlm_queue_time_total	bigint,					33
log_buffer_wait_time	bigint,					34
log_disk_wait_time	bigint,						35
tcpip_wait_time	bigint,							36
ipc_wait_time	bigint,							37
ida_wait_time	bigint,							38
audit_subsystem_wait_time	bigint,				39
audit_file_write_wait_time	bigint,				40
diaglog_write_wait_time	bigint,					41
evmon_wait_time	bigint,							42
total_extended_latch_wait_time	bigint,			43
prefetch_wait_time	bigint,						44
comm_exit_wait_time	bigint,						45
reclaim_wait_time	bigint,						46
spacemappage_reclaim_wait_time	bigint,			47
fed_wait_time		bigint,						48
lock_wait_time_global	bigint,					49
total_backup_proc_time	bigint,					50
total_index_build_proc_time	bigint				51
);


*/




public class MonWLTest extends MonitorTestBase {
	private final Logger logger = LogManager.getLogger(getClass());
	private MonHistoryService ms =null;
	
	
	
private static void insertExample(int connId, Timestamp ts, int wlid,String wlName, int i) throws Throwable {
		
		Connection con =null;
		try {
			con = RepoDBUtils.getConnection();

			PreparedStatement ps = con.prepareStatement("insert into ibm_rtmon_data.\"throughputAllWL\" "
					+ "( dbconn_int   , collected   , WORKLOAD_ID  ,  WORKLOAD_NAME ,"
					+ " TOTAL_CPU_TIME_WL  ,  CPU_USER    , CPU_SYSTEM    , CPU_IOWAIT    , CPU_IDLE    , "
					+ " logical_reads   ,  total_act_time   , act_aborted_total   , act_completed_total  ,  total_app_commits   , "
					+ " total_sorts    , total_compilations  ,  deadlocks    , sort_overflows   , total_section_proc_time , "
					+ " TOTAL_RTN_USER_CODE_PROC_TM , total_section_sort_proc_time , lock_wait_time , cf_wait_time , pool_read_time ,"
					+ " pool_write_time , direct_read_time , direct_write_time , total_compile_proc_time , total_implicit_compile_proc_tm ,"
					+ " total_load_proc_time , total_reorg_proc_time , total_runstats_proc_time , total_commit_proc_time , total_rollback_proc_time ,"
					+ " total_connect_request_proc_tm  , agent_wait_time , wlm_queue_time_total , log_buffer_wait_time , log_disk_wait_time ,"
					+ " tcpip_wait_time , ipc_wait_time , ida_wait_time , audit_subsystem_wait_time , audit_file_write_wait_time ,"
					+ " diaglog_write_wait_time , evmon_wait_time , total_extended_latch_wait_time , prefetch_wait_time , comm_exit_wait_time ,"
					+ " reclaim_wait_time , spacemappage_reclaim_wait_time , fed_wait_time  , lock_wait_time_global , total_backup_proc_time ,"
					+ " total_index_build_proc_time ) values(?, ?, "
					+ "?, ?, ?, ?, ?,   ?, ?, ?, ?, ?, "
					+ "?, ?, ?, ?, ?,   ?, ?, ?, ?, ?, "
					+ "?, ?, ?, ?, ?,   ?, ?, ?, ?, ?, "
					+ "?, ?, ?, ?, ?,   ?, ?, ?, ?, ?, "
					+ "?, ?, ?, ?, ?,   ?, ?, ?, ?, ?, "
					+ "?, ?, ?  )");
			
			
			ps.setInt(1, connId);
			ps.setTimestamp(2, ts);
			ps.setInt(3, wlid);
			ps.setString(4, wlName);
			ps.setInt(1 + 4, (int) (TOTAL_CPU_TIME_WL * i));
			ps.setInt(2 + 4, (int) (CPU_USER * i));
			ps.setInt(3 + 4, (int) (CPU_SYSTEM * i));
			ps.setInt(4 + 4, (int) (CPU_IOWAIT * i));
			ps.setInt(5 + 4, (int) (CPU_IDLE * i));
			ps.setInt(6 + 4, (int) (logical_reads * i));
			ps.setInt(7 + 4, (int) (total_act_time * i));
			ps.setInt(8 + 4, (int) (act_aborted_total * i));
			ps.setInt(9 + 4, (int) (act_completed_total * i));
			ps.setInt(10 + 4, (int) (total_app_commits * i));
			ps.setInt(11 + 4, (int) (total_sorts * i));
			ps.setInt(12 + 4, (int) (total_compilations * i));
			ps.setInt(13 + 4, (int) (deadlocks * i));
			ps.setInt(14 + 4, (int) (sort_overflows * i));
			ps.setInt(15 + 4, (int) (total_section_proc_time * i)); 
			ps.setInt(16 + 4, (int) (total_routine_user_code_proc_time * i));
			ps.setInt(17 + 4, (int) (total_section_sort_proc_time * i));
			ps.setInt(18 + 4, (int) (lock_wait_time * i));  //lock_wait_time > lock_wait_time_global 49
			ps.setInt(19 + 4, (int) (cf_wait_time * i));
			ps.setInt(20 + 4, (int) (pool_read_time * i));
			ps.setInt(21 + 4, (int) (pool_write_time * i));
			ps.setInt(22 + 4, (int) (direct_read_time * i));
			ps.setInt(23 + 4, (int) (direct_write_time* i));
			ps.setInt(24 + 4, (int) (total_compile_proc_time * i));
			ps.setInt(25 + 4, (int) (total_implicit_compile_proc_time * i));
			ps.setInt(26 + 4, (int) (total_load_proc_time * i));
			ps.setInt(27 + 4, (int) (total_reorg_proc_time * i));
			ps.setInt(28 + 4, (int) (total_runstats_proc_time * i));
			ps.setInt(29 + 4, (int) (total_commit_proc_time * i));
			ps.setInt(30 + 4, (int) (total_rollback_proc_time * i));
			ps.setInt(31 + 4, (int) (total_connect_request_proc_time * i));
			ps.setInt(32 + 4, (int) (agent_wait_time * i));
			ps.setInt(33 + 4, (int) (wlm_queue_time_total * i));
			ps.setInt(34 + 4, (int) (log_buffer_wait_time * i));
			ps.setInt(35 + 4, (int) (log_disk_wait_time * i));
			ps.setInt(36 + 4, (int) (tcpip_wait_time * i));
			ps.setInt(37 + 4, (int) (ipc_wait_time * i));
			ps.setInt(38 + 4, (int) (ida_wait_time * i));
			ps.setInt(39 + 4, (int) (audit_subsystem_wait_time * i));
			ps.setInt(40 + 4, (int) (audit_file_write_wait_time * i));
			ps.setInt(41 + 4, (int) (diaglog_write_wait_time * i));
			ps.setInt(42 + 4, (int) (evmon_wait_time * i));
			ps.setInt(43 + 4, (int) (total_extended_latch_wait_time * i));
			ps.setInt(44 + 4, (int) (prefetch_wait_time * i));
			ps.setInt(45 + 4, (int) (comm_exit_wait_time * i));
			ps.setInt(46 + 4, (int) (reclaim_wait_time* i));
			ps.setInt(47 + 4, (int) (spacemappage_reclaim_wait_time * i));
			ps.setInt(48 + 4, (int) (fed_wait_time * i));
			ps.setInt(49 + 4, (int) (lock_wait_time_global * i));
			ps.setInt(50 + 4, (int) (total_backup_proc_time * i));
			ps.setInt(51 + 4, (int) (total_index_build_proc_time * i));



			int rs = ps.executeUpdate();

			ps.close();
			
			System.out.println("insert finished");
		} catch (Exception err) {
			err.printStackTrace(System.out);
		}finally{
			con.close();
		}
	}
	
	
	static final double TOTAL_CPU_TIME_WL = 1;
	static final double CPU_USER = 2;
	static final double CPU_SYSTEM = 3;
	static final double CPU_IOWAIT = 4;
	static final double CPU_IDLE = 5;
	static final double logical_reads = 3600; //6
	static final double total_act_time = 7;
	static final double act_aborted_total = 8;
	static final double act_completed_total = 9;
	static final double total_app_commits = 3600; //10
	static final double total_sorts = 11;
	static final double total_compilations = 12;
	static final double deadlocks = 13;
	static final double sort_overflows = 14;
	static final double total_section_proc_time = 20;  //total_section_proc_time >total_section_sort_proc_time
	static final double total_routine_user_code_proc_time = 16;
	static final double total_section_sort_proc_time = 17;
	static final double lock_wait_time = 59;
	static final double cf_wait_time = 19;
	static final double pool_read_time = 20;
	static final double pool_write_time = 21;
	static final double direct_read_time = 22;
	static final double direct_write_time = 23;
	static final double total_compile_proc_time = 24;
	static final double total_implicit_compile_proc_time = 25;
	static final double total_load_proc_time = 26;
	static final double total_reorg_proc_time = 27;
	static final double total_runstats_proc_time = 28;
	static final double total_commit_proc_time = 29;
	static final double total_rollback_proc_time = 30;
	static final double total_connect_request_proc_time = 31;
	static final double agent_wait_time = 32;
	static final double wlm_queue_time_total = 33;
	static final double log_buffer_wait_time = 34;
	static final double log_disk_wait_time = 35;
	static final double tcpip_wait_time = 36;
	static final double ipc_wait_time = 37;
	static final double ida_wait_time = 38;
	static final double audit_subsystem_wait_time = 39;
	static final double audit_file_write_wait_time = 40;
	static final double diaglog_write_wait_time = 41;
	static final double evmon_wait_time = 42;
	static final double total_extended_latch_wait_time = 43;
	static final double prefetch_wait_time = 44;
	static final double comm_exit_wait_time = 45;
	static final double reclaim_wait_time = 46;
	static final double spacemappage_reclaim_wait_time = 47;
	static final double fed_wait_time = 48;
	static final double lock_wait_time_global = 49;
	static final double total_backup_proc_time = 50;
	static final double total_index_build_proc_time = 51;
	
	static final double sqlexecutiontime = // 36
			total_section_proc_time // 20
			+ total_routine_user_code_proc_time;// 16
	
	static final double iotime  = //86
			pool_read_time  //20 
			+pool_write_time  //21 
			+ direct_read_time //22 
			+  direct_write_time ; //23 
	
	static final double otherprocessingtime =//220 
			total_compile_proc_time //24
			+total_implicit_compile_proc_time  //25
			+ total_load_proc_time //26 
			+total_reorg_proc_time  //27 
			+total_runstats_proc_time //28 
			+total_commit_proc_time //29 
			+total_rollback_proc_time //30 
			+total_connect_request_proc_time; //31 
	
	static final double otherwaittime =//680
			agent_wait_time //32 
			+wlm_queue_time_total //33
			+log_buffer_wait_time //34
			+log_disk_wait_time //35
			+tcpip_wait_time //36 
			+ipc_wait_time //37 
			+ida_wait_time  //38
			+audit_subsystem_wait_time  //39
			+audit_file_write_wait_time  //40
			+diaglog_write_wait_time  //41
			+evmon_wait_time  //42
			+total_extended_latch_wait_time  //43
			+prefetch_wait_time  //44
			+comm_exit_wait_time //45
			+reclaim_wait_time  //46
			+spacemappage_reclaim_wait_time //47 
			+fed_wait_time ; //48 

	static final double total_time =  //1100  
			sqlexecutiontime //36 
			+iotime  //86 
			+  otherprocessingtime //220 
			+  otherwaittime  //680
			+  lock_wait_time  //59 
			+ cf_wait_time; //19
	
	
	Map<String, MetricUnit> metricUnitMap=null;

	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {

		Properties p = ms.getJSONProperties();
		return FileTools.readProperties(p);

	}



	@BeforeClass
	@Parameters({"dbProfile"})
	public void beforeTest2(@Optional("")String dbProfile) throws Throwable{
		
		System.out.println("# init test data");
		
		ms = new MonHistoryService(dbProfile){
			
			public String formatPostData(String jsonObj){
				
				String userid = Configuration.getProperty("LoginUser");
				
				JSONObject postData = JSONObject.fromObject(jsonObj);
				
				if(postData.containsKey("name"))
					postData.put("name", MonWLTest.this.dbProfile);
				if(postData.containsKey("startTimestamp"))
					postData.put("startTimestamp", TimePoints.getInst().requestStart.getTime());
				if(postData.containsKey("endTimestamp"))
					postData.put("endTimestamp", TimePoints.getInst().requestEnd.getTime());
				if(postData.containsKey("userid"))
					postData.put("userid", userid);
				postData.put("wlid", 1);
				return postData.toString();
			}
			
		};

		
		
		DBEntity dbEntity=DBUtils.getDBConfiguration(this.dbProfile);
		
		
		dropData();
		insertData();
		
		testWLResult();
	}
	

	public void testWLResult() throws Throwable {
		
		logger.info("Run query testWLResult");
		
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_history_throughputAllWL"));
		int responseCode = (Integer) result.get("ResponseCode");
		if (responseCode != 200) {
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if (isExceptionCaught(responseData)) {
			assertExceptionFailure(responseCode, result);
		}
		
		System.out.println("# reponse data:\t");
		System.out.println(responseData);
		JSONArray metricData = JsonPath.read(responseData, "$data[0].[1].metric.graphs[0]");
		
		metricUnitMap=new HashMap<String, MonWLTest.MetricUnit>();
		
		loadTestResult(metricData);
		
		JSONArray metricDataBreakdown = JsonPath.read(responseData, "$data[0].[1].metric.graphs[1]");
		loadBreakdownTestResult(metricDataBreakdown);
	}
	
	
	@Test
	public void testAdWlmSystemResource() throws Throwable{
		testWLMStatsResult("adWlmSystemResource");
	}
	
	@Test
	public void testAdWlmServiceSubclassStats() throws Throwable{
		testWLMStatsResult("adWlmServiceSubclassStats");
	}
	
	@Test
	public void testAdWlmMonWorkloads() throws Throwable{
		testWLMStatsResult("adWlmMonWorkloads");
	}
	
	@Test
	public void testAdWlmServiceSubclass() throws Throwable{
		testWLMStatsResult("adWlmServiceSubclass");
	}
	
	@Test
	public void testAdWlmWorkloadStats() throws Throwable{
		testWLMStatsResult("adWlmWorkloadStats");
	}
	
	//	@Test
	public void testAdWlmQueueStats() throws Throwable{
		testWLMStatsResult("adWlmQueueStats");
	}

	
	
	protected void testWLMStatsResult(String queryId) throws Throwable {
		
		logger.info("Run query testWLMStatsResult");
		
		JSONObject result = ms.callMonHistoryService(ms.getJSONData(queryId));
		int responseCode = (Integer) result.get("ResponseCode");
		if (responseCode != 200) {
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if (isExceptionCaught(responseData)) {
			assertExceptionFailure(responseCode, result);
		}
		
		System.out.println("# reponse data:\t");
		System.out.println(responseData);
		
			
		JSONArray metricData = JsonPath.read(responseData, "$data[0][1].metric." + queryId + ".data");
		assertTrue(metricData!=null, queryId + " doesn't return data.");
		assertTrue(metricData.size()>0, queryId + " doesn't return data.");
		
	}


	private void loadTestResult(JSONArray metricData) {
		for (int i = 0; i < metricData.size(); i++) {
			JSONObject obj=(JSONObject) metricData.get(i);
			
			MetricUnit mu=new MetricUnit();
			String name=JsonPath.read(obj, "$name");
			mu.max=JsonPath.read(obj, "$data[0].data[0].max");
			mu.min=JsonPath.read(obj, "$data[0].data[0].min");
			mu.avg=JsonPath.read(obj, "$data[0].data[0].avg");
			
			metricUnitMap.put(name, mu);
		}
	}
	
	private void loadBreakdownTestResult(JSONArray metricData) {
		for (int i = 0; i < metricData.size(); i++) {
			JSONObject obj=(JSONObject) metricData.get(i);
			
			MetricUnit mu=new MetricUnit();
			String name=JsonPath.read(obj, "$name");
			mu.max=JsonPath.read(obj, "$data[0].max");
			mu.min=JsonPath.read(obj, "$data[0].min");
			mu.avg=JsonPath.read(obj, "$data[0].avg");
			
			metricUnitMap.put(name, mu);
		}
	}
	
	@Test
	public void testWLDB2CPU(){
		MetricUnit mu=metricUnitMap.get("_databaseWLCPU");
		
		double wlDB2Cpu = 100 * (TOTAL_CPU_TIME_WL) / (CPU_USER + CPU_SYSTEM + CPU_IOWAIT + CPU_IDLE);
		validateResults(mu, wlDB2Cpu);
		
	}
	
	
	@Test
	public void testLogicReads(){
		MetricUnit mu=metricUnitMap.get("_logicRead");
		assertEquals(mu.min, 60);
		assertEquals(mu.avg, 120);
		assertEquals(mu.max, 180);
		
	}
	
	@Test
	public void testActivityTime(){
		MetricUnit mu=metricUnitMap.get("_avgActivityTime");
		
		double value = (total_act_time)/ (act_aborted_total+act_completed_total);
		
		validateResults(mu, value);
	}
	
	
	
	@Test
	public void testTrxThoughput(){
		MetricUnit mu=metricUnitMap.get("_trxThoughput");
		
		assertEquals(mu.min, total_app_commits/3600); //per hour
		assertEquals(mu.avg, total_app_commits/3600*2);
		assertEquals(mu.max, total_app_commits/3600*3);
	}
	
	@Test
	public void testTotalSorts(){
		MetricUnit mu=metricUnitMap.get("_total_sorts");
		
		assertEquals(mu.min, total_sorts/60.0); //per min
		assertEquals(mu.avg, total_sorts/60.0*2);
		assertEquals(mu.max, total_sorts/60.0*3);
	}
	
	@Test
	public void testTotalCompilations(){
		MetricUnit mu=metricUnitMap.get("_total_compilations");
		
		assertEquals(mu.min, total_compilations/60.0); //per min
		assertEquals(mu.avg, total_compilations/60.0*2);
		assertEquals(mu.max, total_compilations/60.0*3);
	}
	
	@Test
	public void testFailureActivityRate() {
		MetricUnit mu = metricUnitMap.get("_failureActivityRate");

		double value = 100 * (act_aborted_total) / (act_aborted_total + act_completed_total);

		validateResults(mu, value);
	}
	
	
	@Test
	public void testTOTAL_SECTION_PROC_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("TOTAL_SECTION_PROC_TIME_PERCENT");

		double value = 100	* (total_section_proc_time-total_section_sort_proc_time)/ (total_time);

		validateResults(mu, value);
	}
	
	
	@Test
	public void testTOTAL_SECTION_SORT_PROC_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("TOTAL_SECTION_SORT_PROC_TIME_PERCENT");

		double value = 100	* (total_section_proc_time)/ (total_time);

		validateResults(mu, value);
	}
	
	@Test
	public void testTOTAL_ROUTINE_USER_CODE_PROC_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("TOTAL_ROUTINE_USER_CODE_PROC_TIME_PERCENT");
		
		double value = 100	* (total_routine_user_code_proc_time)/ (total_time);
		
		validateResults(mu, value);
	}
	

	@Test
	public void testSQLEXECUTIONTIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("SQLEXECUTIONTIME_PERCENT");
		
		double value = 100	* (sqlexecutiontime)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testPOOL_READ_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("POOL_READ_TIME_PERCENT");
		
		double value = 100	* (pool_read_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	
	
	@Test
	public void testPOOL_WRITE_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("POOL_WRITE_TIME_PERCENT");
		
		double value = 100	* (pool_write_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	
	@Test
	public void testDIRECT_READ_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("DIRECT_READ_TIME_PERCENT");
		
		double value = 100	* (direct_read_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testDIRECT_WRITE_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("DIRECT_WRITE_TIME_PERCENT");
		
		double value = 100	* (direct_write_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	
	@Test
	public void testIOTIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("IOTIME_PERCENT");
		
		double value = 100	* (iotime)/ (total_time);
		
		validateResults(mu, value);
	}
	
	
	@Test
	public void testLOCK_WAIT_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("LOCK_WAIT_TIME_PERCENT");
		
		double  value = 100	* (lock_wait_time-lock_wait_time_global)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testLOCKWAITTIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("LOCKWAITTIME_PERCENT");
		
		double value = 100	* (lock_wait_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testTOTAL_COMPILE_PROC_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("TOTAL_COMPILE_PROC_TIME_PERCENT");
		
		double value = 100	* (total_compile_proc_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	
	@Test
	public void testTOTAL_IMPLICIT_COMPILE_PROC_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("TOTAL_IMPLICIT_COMPILE_PROC_TIME_PERCENT");
		
		double value = 100	* (total_implicit_compile_proc_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	
	@Test
	public void testTOTAL_LOAD_PROC_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("TOTAL_LOAD_PROC_TIME_PERCENT");
		
		double value = 100	* (total_load_proc_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testTOTAL_REORG_PROC_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("TOTAL_REORG_PROC_TIME_PERCENT");
		
		double value = 100	* (total_reorg_proc_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testTOTAL_RUNSTATS_PROC_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("TOTAL_RUNSTATS_PROC_TIME_PERCENT");
		
		double value = 100	* (total_runstats_proc_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testTOTAL_CONNECT_REQUEST_PROC_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("TOTAL_CONNECT_REQUEST_PROC_TIME_PERCENT");
		
		double value = 100	* (total_connect_request_proc_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testTOTAL_COMMIT_PROC_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("TOTAL_COMMIT_PROC_TIME_PERCENT");
		
		double value = 100	* (total_commit_proc_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testTOTAL_ROLLBACK_PROC_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("TOTAL_ROLLBACK_PROC_TIME_PERCENT");
		
		double value = 100	* (total_rollback_proc_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	
	@Test
	public void testTOTAL_BACKUP_PROC_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("TOTAL_BACKUP_PROC_TIME_PERCENT");
		
		double value = 100	* (total_backup_proc_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	
	@Test
	public void testTOTAL_INDEX_BUILD_PROC_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("TOTAL_INDEX_BUILD_PROC_TIME_PERCENT");
		
		double value = 100	* (total_index_build_proc_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testOTHERPROCESSINGTIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("OTHERPROCESSINGTIME_PERCENT");
		
		double value = 100	* (otherprocessingtime)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testAGENT_WAIT_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("AGENT_WAIT_TIME_PERCENT");
		
		double value = 100	* (agent_wait_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testWLM_QUEUE_TIME_TOTAL_PERCENT() {
		MetricUnit mu = metricUnitMap.get("WLM_QUEUE_TIME_TOTAL_PERCENT");
		
		double value = 100	* (wlm_queue_time_total)/ (total_time);
		
		validateResults(mu, value);
	}
	

	@Test
	public void testLOG_BUFFER_WAIT_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("LOG_BUFFER_WAIT_TIME_PERCENT");
		
		double value = 100	* (log_buffer_wait_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testLOG_DISK_WAIT_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("LOG_DISK_WAIT_TIME_PERCENT");
		
		double value = 100	* (log_disk_wait_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	
	@Test
	public void testTCPIP_WAIT_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("TCPIP_WAIT_TIME_PERCENT");
		
		double value = 100	* (tcpip_wait_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testIPC_WAIT_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("IPC_WAIT_TIME_PERCENT");
		
		double value = 100	* (ipc_wait_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testAUDIT_SUBSYSTEM_WAIT_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("AUDIT_SUBSYSTEM_WAIT_TIME_PERCENT");
		
		double value = 100	* (audit_subsystem_wait_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testAUDIT_FILE_WRITE_WAIT_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("AUDIT_FILE_WRITE_WAIT_TIME_PERCENT");
		
		double value = 100	* (audit_file_write_wait_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testDIAGLOG_WRITE_WAIT_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("DIAGLOG_WRITE_WAIT_TIME_PERCENT");
		
		double value = 100	* (diaglog_write_wait_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testEVMON_WAIT_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("EVMON_WAIT_TIME_PERCENT");
		
		double value = 100	* (evmon_wait_time)/ (total_time);
		
		validateResults(mu, value);
	}


	@Test
	public void testTOTAL_EXTENDED_LATCH_WAIT_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("TOTAL_EXTENDED_LATCH_WAIT_TIME_PERCENT");
		
		double value = 100	* (total_extended_latch_wait_time)/ (total_time);
		
		validateResults(mu, value);
	}


	@Test
	public void testPREFETCH_WAIT_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("PREFETCH_WAIT_TIME_PERCENT");
		
		double value = 100	* (prefetch_wait_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testCOMM_EXIT_WAIT_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("COMM_EXIT_WAIT_TIME_PERCENT");
		
		double value = 100	* (comm_exit_wait_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testIDA_WAIT_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("IDA_WAIT_TIME_PERCENT");
		
		double value = 100	* (ida_wait_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testFED_WAIT_TIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("FED_WAIT_TIME_PERCENT");
		
		double value = 100	* (fed_wait_time)/ (total_time);
		
		validateResults(mu, value);
	}
	
	@Test
	public void testOTHERWAITTIME_PERCENT() {
		MetricUnit mu = metricUnitMap.get("OTHERWAITTIME_PERCENT");
		
		double value = 100	* (otherwaittime)/ (total_time);
		
		validateResults(mu, value);
	}



	private void assertEquals(Number value, Number target) {
		Assert.assertTrue(Math.abs(value.doubleValue()/target.doubleValue())<1.1);
	}
	
	static class MetricUnit{
		
		Number max, min, avg;
		
	}

	private void validateResults(MetricUnit mu, double value) {
//		assertEquals(mu.min, value); 
//		assertEquals(mu.avg, value);
//		assertEquals(mu.max, value);
		
		Assert.assertTrue(mu.min!=null);
		Assert.assertTrue(mu.avg!=null); 
		Assert.assertTrue(mu.max!=null); 
		
	}
	
	
	
	
	
	public void TestPodThroughputConnectionDpf() throws FileNotFoundException, IOException, InterruptedException {
		logger.info("Run query throughputConnection and get application handle");

		ms = new MonHistoryService(this.dbProfile);
		JSONObject result = ms.callMonHistoryService(ms.getJSONData("Get_history_throughputAllWL"));
		int responseCode = (Integer) result.get("ResponseCode");
		if (responseCode != 200) {
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if (isExceptionCaught(responseData)) {
			assertExceptionFailure(responseCode, result);
		}
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.throughputConnection");
		if (!metricData.isEmpty()) {
			JSONArray dataArray = metricData.getJSONArray("data");
			if (!dataArray.isEmpty()) {
				JSONObject throughputConn = (JSONObject) dataArray.get(0);
				String applHandle = throughputConn.getString("APPLICATION_HANDLE");
				if (applHandle == null) {
					Assert.fail("Application handle is invalid: null");
				}

				logger.info("Running query podThroughputConnectionDpf ... ");

				// {"cmd":"getConnections","parm_1":applHandle,"name":"OTSTEST","metricOnDemandDelta":"podThroughputConnectionDpf"}
				String cmd_podThroughputConnectionDpf = "{\"cmd\":\"getConnections\",\"parm_1\":\"" + applHandle
						+ "\",\"name\":\"OTSTEST\",\"metricOnDemandDelta\":\"podThroughputConnectionDpf\"}";
				result = ms.callMonHistoryService(cmd_podThroughputConnectionDpf);
				responseCode = (Integer) result.get("ResponseCode");
				if (responseCode != 200) {
					assertResponseFailure(responseCode, result);
				}
				// Check for exception in the response
				responseData = (JSONObject) result.get("ResponseData");
				if (isExceptionCaught(responseData)) {
					assertExceptionFailure(responseCode, result);
				}

				logger.info("SUCCESS=> URL: " + result.get("URL") + ", POSTDATA:" + result.get("PostData"));
			}
		}
	}
	
	
	static class TimePoints {

		private TimePoints() {

		}

		static TimePoints timePoints = null;

		public static TimePoints getInst() {
			if (timePoints == null) {
				timePoints = new TimePoints();
			}
			return timePoints;
		}

		Timestamp dataPointTs1;
		Timestamp dataPointTs2;
		Timestamp dataPointTs3;
		Timestamp dataPointTs4;

		Timestamp dataPointStart;
		Timestamp dataPointEnd;
		
		Timestamp requestStart;
		Timestamp requestEnd;

		int hour = 10;
		int day = 10;

		{

			LocalDateTime testDay = LocalDateTime.now().minus(40, ChronoUnit.WEEKS);

			dataPointTs1 = Timestamp.valueOf(testDay);
			dataPointTs2 = Timestamp.valueOf(testDay.plus(1, ChronoUnit.HOURS));
			dataPointTs3 = Timestamp.valueOf(testDay.plus(2, ChronoUnit.HOURS));
			
			dataPointTs4 = Timestamp.valueOf(testDay.plus(3, ChronoUnit.HOURS));
			

			dataPointStart = Timestamp.valueOf(testDay.minus(7, ChronoUnit.DAYS));
			dataPointEnd = Timestamp.valueOf(testDay.plus(7, ChronoUnit.DAYS));
			
			requestStart = Timestamp.valueOf(testDay.minus(5, ChronoUnit.DAYS));
			requestEnd = Timestamp.valueOf(testDay.plus(5, ChronoUnit.DAYS));

			System.out.println("#workload test data start time:" + dataPointStart.getTime() + "\t" + dataPointStart);
			System.out.println("#workload test data end time:" + dataPointEnd.getTime() + "\t" + dataPointEnd);

		}

	}
	
	private static void dropData() throws Throwable {
		Connection con = null;
		try {
			con = RepoDBUtils.getConnection();

			TimePoints tp = TimePoints.getInst();

			PreparedStatement ps = con.prepareStatement(
					"delete from  ibm_rtmon_data.\"throughputAllWL\" where collected<=? and collected>=?  ");
			ps.setTimestamp(1, tp.dataPointEnd);
			ps.setTimestamp(2, tp.dataPointStart);
			int rs = ps.executeUpdate();

			ps.close();

			System.out.println("drop finished");
		} catch (Exception err) {
			err.printStackTrace(System.out);
		} finally {
			con.close();
		}

	}
	
	private  int getDBConnID(String profileName) throws Throwable {
		Connection con = null;
		
		int dbId=-1;
		try {
			con = RepoDBUtils.getConnection();


			PreparedStatement ps = con.prepareStatement(
					"select dbconn_int from  IBM_RTMON_METADATA.RTMON_MAP_DBCONN where DBCONN_ID=?  ");
			ps.setString(1, profileName);
			
			ResultSet rs = ps.executeQuery();
			
			rs.next();
			dbId=rs.getInt(1);

			ps.close();

			System.out.println("drop finished");
		} catch (Exception err) {
			err.printStackTrace(System.out);
		} finally {
			con.close();
		}
		return dbId;

	}

	private  void insertData() throws Throwable {

		TimePoints tp = TimePoints.getInst();
		
		int dbId=getDBConnID(this.dbProfile);
		
		Assert.assertTrue(dbId>=0);

		insertExample(dbId, tp.dataPointTs1, 1, "wl_1", 0);
		insertExample(dbId, tp.dataPointTs1, 2, "wl_2", 0);
		// insertExample(1,tp.dataPointTs1, 3,"wl_3", 0) ;

		// 1-0= 1

		insertExample(dbId, tp.dataPointTs2, 1, "wl_1", 1);
		insertExample(dbId, tp.dataPointTs2, 2, "wl_2", 1);
		// insertExample(1,tp.dataPointTs2, 3,"wl_3", 1) ;

		// 2-1= 1

		insertExample(dbId, tp.dataPointTs3, 1, "wl_1", 2);
		insertExample(dbId, tp.dataPointTs3, 2, "wl_2", 2);
		// insertExample(1,tp.dataPointTs3, 3,"wl_3", 2) ;

		// 5-2= 3

		insertExample(dbId, tp.dataPointTs4, 1, "wl_1", 5);
		insertExample(dbId, tp.dataPointTs4, 2, "wl_2", 5);
		// insertExample(1,tp.dataPointTs4, 3,"wl_3", 5) ;

	}
	
	



	private static void assertResponseFailure(int responseCode, JSONObject result) {
		Assert.fail("FAILURE=> Response code =" + responseCode + " for URL: " + result.get("URL") + ", POSTDATA:"
				+ result.get("PostData"));
	}

	private static void assertExceptionFailure(int responseCode, JSONObject result) {
		Assert.fail("FAILURE:=>ExceptionCaught=true in responseJson for URL: " + result.get("URL") + ", POSTDATA:"
				+ result.get("PostData") + " with RESPONSECODE:" + responseCode + " and RESPONSEDATA:"
				+ result.get("ResponseData"));
	}

	private static boolean isExceptionCaught(JSONObject responseData) {
		return responseData.toString().contains("\"exceptionCaught\":true");
	}
}
