package com.ibm.datatools.ots.tests.restapi.monitor;


import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.ibm.datatools.ots.tests.restapi.common.DatabaseService;
import com.ibm.datatools.ots.tests.restapi.common.MonRealtimeService;

public class MonitorTestBase {
	
	protected String dbProfile = null ;
	private final Logger logger = LogManager.getLogger(getClass());	
	private MonRealtimeService ms;
	private DatabaseService databaseService = new DatabaseService();
	
	@BeforeClass
	@Parameters({"dbProfile"})
	public void beforeTest(@Optional("")String dbProfile)throws FileNotFoundException, IOException{
		this.dbProfile = dbProfile;
		Assert.assertNotNull(this.dbProfile, "dbProfile can not be null!");
		
		logger.info("Checking if the connection exists..."); 
		boolean connExists =databaseService.checkConnectionExists(this.dbProfile);
		if(!connExists){
			Assert.fail("FAILURE=> Database Connection is not existed");
		}
	}

}
