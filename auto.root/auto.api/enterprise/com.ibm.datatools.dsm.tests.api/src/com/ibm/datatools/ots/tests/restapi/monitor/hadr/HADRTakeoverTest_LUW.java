package com.ibm.datatools.ots.tests.restapi.monitor.hadr;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.SSHCmd;
import com.ibm.datatools.ots.tests.restapi.common.HADRTestService;
import com.jayway.jsonpath.JsonPath;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class HADRTakeoverTest_LUW extends HADRTestBase {

	private HADRTestService hadrTestServices = null;
	private String connectionJson = "addPureScaleHADRConnectionWithJDBCPro";
	private String hadrprimaryJason = "metricOnDemand_hadrStatePureScalePrimarySummary";
	private String hadrstandbyJason = "metricOnDemand_hadrStatePureScaleStandbySummary";

	private String instanceName = "db2inst1";
	private String hadrprimaryIP = "9.111.139.183";
	private String hadrreplayIP = "9.111.141.196";
	private String instancePW = "n1cetest1";

	private String databaseName = "vmpuredb";
	private String name = "testTakeover";
	private static final Logger logger = LogManager.getLogger(HADRTakeoverTest_LUW.class);


	/**
	 * @author jiahoup@cn.ibm.com some data prepare for db2 takeover on
	 *         hadr-purescale testing
	 * 
	 */
	@BeforeTest
	public void beforeTest() throws FileNotFoundException, IOException {

		hadrTestServices = new HADRTestService();
		// if the connection exists.
		// hadrTestServices.removeHADRConnection(name);
		//
		// // add a purescale hadr connection
		// // edit this connection to an HADR environment
		// String newConnection = hadrTestServices.getJSONData(connectionJson);
		//
		// JSONObject result = hadrTestServices.addConnection(newConnection);
		//
		// // Verify the result code
		// String resultCode = hadrTestServices.getResponseResultCode(result);
		// Assert.assertEquals(resultCode, "success");

	}

	@Test(description = "Test Hadr-purescale Takeover is correct or not")
	public void testTakeoverHADR() throws InterruptedException, FileNotFoundException, IOException {
		logger.info("Before PureScale HADR Takeover ...");
		JSONObject beforeMetricData = getMetricDataforPureScaleHADR(hadrprimaryJason, false);
		Assert.assertNotNull(beforeMetricData);
		DB2TakeoverExecuteSSHCMD(databaseName, hadrreplayIP);
		
		Thread.sleep(5*60*1000);
		JSONObject afterMetricData = getMetricDataforPureScaleHADR(hadrstandbyJason, true);
//     	for(int i=0;i<4;i++)
//		{
//			if(afterMetricData.isNullObject()){
//				Thread.sleep(4*60*1000);
//				afterMetricData = getMetricDataforPureScaleHADR(hadrstandbyJason, true);
//			}
//		}
		
		Assert.assertNotNull(afterMetricData);
		
		String primaryhost =  beforeMetricData.getString("PRIMARY_MEMBER_HOST");
		logger.info("Original HADR Primary mebmer is " + primaryhost);
	    String standbyhost= afterMetricData.getString("STANDBY_MEMBER_HOST");
	    logger.info("Takeover HADR Standby mebmer is " + standbyhost);
		// Validate HADR_ROLE
		
		if(primaryhost.isEmpty()||!primaryhost.equalsIgnoreCase(standbyhost))
		{
			Assert.fail("Takeover HADR not successfully !!!");
		}

		logger.info("Takeover from original primary site to restore system ...");
		DB2TakeoverExecuteSSHCMD(databaseName, hadrprimaryIP);
			
		
	}

	@AfterClass
	public void afterTest() {
		// delete this connection
		// hadrTestServices.removeHADRConnection(name);
	}

	public JSONObject getMetricDataforPureScaleHADR(String hadrJason, boolean isTakeover)
			throws FileNotFoundException, IOException, InterruptedException {
		String currentConnection = hadrTestServices.getJSONData(hadrJason);

		JSONObject result = hadrTestServices.callConnectionsDataService(currentConnection);
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		JSONObject metricData;
		
		metricData = JsonPath.read(responseData, "$data[0].[1].metric.hadrStatePureScaleSummary.data[1]");
			// metricData is empty if the monitored db is not a HADR system.
		// We validate data only when the metricData is available from running
		// on a HADR system.
		if (!metricData.isEmpty()) {
			return metricData;
		} else
			return null;

	}

	/**
	 * run db2 takeover hadr in ddl *
	 * 
	 * @author jiahoup@cn.ibm.com date: 20161104
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public void DB2TakeoverExecuteSSHCMD(String dbname, String ip)
			throws InterruptedException, FileNotFoundException, IOException {

		logger.info("Running DB2 takeover sshcmd...");
		/**
		 * Response data DB20000I The TAKEOVER HADR ON DATABASE command
		 * completed successfully.
		 */
		String ddl = "db2 takeover hadr on db " + dbname;

		logger.info("running " + ddl);

		// String exec = SSHCmd.sshCMD(dbProfile, ddl);
		// String exec = SSHCmd.sshCMD(ip, instanceName,instancePW, 22, "db2
		// list db directory");
		String exec = SSHCmd.sshCMD(ip, instanceName, instancePW, 22, ddl);
		System.out.println(exec);

		logger.info("PASSED: Exectue takeover successfully");

	}

}
