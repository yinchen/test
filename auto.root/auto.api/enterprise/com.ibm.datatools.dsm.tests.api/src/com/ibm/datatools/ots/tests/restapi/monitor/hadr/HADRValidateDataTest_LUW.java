package com.ibm.datatools.ots.tests.restapi.monitor.hadr;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.DBUtils;
import com.ibm.datatools.ots.tests.restapi.common.HADRService;
import com.ibm.datatools.test.utils.FileTools;
import com.jayway.jsonpath.JsonPath;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class HADRValidateDataTest_LUW {
	
	private final Logger logger = LogManager.getLogger(getClass());	
	
	private HADRService hadrService = null;
	
	
	@BeforeTest
	public void beforeTest() throws FileNotFoundException, IOException {
		hadrService = new HADRService();
		String json = hadrService.getJSONData("metricOnDemand_hadrStateSummary");
		JSONObject jo = JSONObject.fromObject(json);
		String profileName = (String) jo.get("name");
		Connection conn = null;
		
		try {
			conn = DBUtils.getConnection(profileName, "N1cetest");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Assert.assertNotNull(conn);
	}
	
	
	@DataProvider(name="testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException{
	
		hadrService = new HADRService();
		Properties p = hadrService.getJSONProperties();
		return FileTools.readProperties(p);
	
	}
	
	
	@Test(dataProvider = "testdata")
	public void testMonLiteService(Object key ,Object jsonObj)
	{
		logger.info("Running monlite backend query ..."); 
		
		JSONObject result = hadrService.callHADRService(jsonObj.toString());
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");		
		boolean exceptionCaught = responseData.toString().contains("\"exceptionCaught\":true");
		if ( exceptionCaught )
		{
			assertExceptionFailure(responseCode, result);
		}
		
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));	
	}
	
	
	
	@Test
	public void ValidateNonPureScaleHadrData() throws FileNotFoundException, IOException, InterruptedException
	{				
		logger.info("Validate query hadrStateSummary output ..."); 
		
		HADRService hadrService = new HADRService();
		JSONObject result =  null;
		JSONObject responseData = null;
		int responseCode = -1;
		
		//will try the request multiple times
		for(int i = 0, maxRetryTime = 2 ; i< maxRetryTime; i++){
			result = hadrService.callHADRService(hadrService.getJSONData("metricOnDemand_hadrStateSummary"));
			responseCode = (Integer)result.get("ResponseCode");
			if ( responseCode != 200 )
			{
				assertResponseFailure(responseCode, result);
			}
			// Check for exception in the response
			responseData = (JSONObject) result.get("ResponseData");			
			if(!isExceptionCaught(responseData))
				break;			
		}		
		
		
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}
		
		
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.hadrStateSummary");
		// metricData is empty if the monitored db is not a HADR system.
		// We validate data only when the metricData is available from running on a HADR system.
		if ( !metricData.isEmpty() )
		{
			JSONArray dbsDataArray = metricData.getJSONArray("data");
			for ( int i = 0; i < dbsDataArray.size(); i++ )
			{
				JSONObject dataArray = (JSONObject) dbsDataArray.get(i);
				// Validate HADR_ROLE
				String hadr_role = dataArray.getString("HADR_ROLE");
				if ( !hadr_role.equalsIgnoreCase("primary") &&  
						!hadr_role.equalsIgnoreCase("standby") )
				{
					Assert.fail("HADR_ROLE " + hadr_role + " is invalid");
				}	
				// Validate HADR_SYNCMODE
				String hadr_syncmode = dataArray.getString("HADR_SYNCMODE");
				if ( !hadr_syncmode.equalsIgnoreCase("Asynchronous") &&  
						!hadr_syncmode.equalsIgnoreCase("Synchronous") &&
						!hadr_syncmode.equalsIgnoreCase("Near synchronous") && 
						!hadr_syncmode.equalsIgnoreCase("Super asynchronous") )
				{
					Assert.fail("HADR_SYNCMODE " + hadr_syncmode + " is invalid");
				}	
			}
		}			
		
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));			
	}

	
	@Test
	public void ValidatePureScaleHadrData() throws FileNotFoundException, IOException, InterruptedException
	{				
		logger.info("Validate query hadrStatePureScaleSummary output ..."); 
		
		HADRService hadrService = new HADRService();
		JSONObject result = hadrService.callHADRService(hadrService.getJSONData("metricOnDemand_hadrStatePureScaleSummary"));
		int responseCode = (Integer)result.get("ResponseCode");
		if ( responseCode != 200 )
		{
			assertResponseFailure(responseCode, result);
		}
		// Check for exception in the response
		JSONObject responseData = (JSONObject) result.get("ResponseData");
		if ( isExceptionCaught(responseData) )
		{			
			assertExceptionFailure(responseCode, result);
		}
		JSONObject metricData = JsonPath.read(responseData, "$data[0].[1].metric.hadrStatePureScaleSummary");
		// metricData is empty if the monitored db is not a HADR system.
		// We validate data only when the metricData is available from running on a HADR system.
		if ( !metricData.isEmpty() )
		{
			JSONArray dbsDataArray = metricData.getJSONArray("data");
			for ( int i = 0; i < dbsDataArray.size(); i++ )
			{
				JSONObject dataArray = (JSONObject) dbsDataArray.get(i);
				// Validate HADR_ROLE
				String hadr_role = dataArray.getString("HADR_ROLE");
				if ( !hadr_role.equalsIgnoreCase("primary") &&  
						!hadr_role.equalsIgnoreCase("standby") )
				{
					Assert.fail("HADR_ROLE: " + hadr_role + " is invalid");
				}
				// Validate hadr_connect_status
				String hadr_connect_status = dataArray.getString("HADR_CONNECT_STATUS");
				if ( !hadr_connect_status.equalsIgnoreCase("Connected") &&  
						!hadr_connect_status.equalsIgnoreCase("Congested") &&
						!hadr_connect_status.equalsIgnoreCase("Disconnected") )
				{
					Assert.fail("HADR_CONNECT_STATUS: " + hadr_connect_status + " is invalid");
				}
				// Validate hadr_state
				String hadr_state = dataArray.getString("HADR_STATE");
				if ( !hadr_state.equalsIgnoreCase("Disconnected") &&  
						!hadr_state.equalsIgnoreCase("Local catchup") &&
						!hadr_state.equalsIgnoreCase("Remote catchup pending") &&
						!hadr_state.equalsIgnoreCase("Remote catchup") &&
						!hadr_state.equalsIgnoreCase("Peer") &&
						!hadr_state.equalsIgnoreCase("Disconnected peer") )
				{
					Assert.fail("HADR_STATE: " + hadr_state + " is invalid");
				}
							
				// Validate HADR_SYNCMODE
				String hadr_syncmode = dataArray.getString("HADR_SYNCMODE");
				if ( !hadr_syncmode.equalsIgnoreCase("Asynchronous") &&  
						!hadr_syncmode.equalsIgnoreCase("Synchronous") &&
						!hadr_syncmode.equalsIgnoreCase("Near synchronous") && 
						!hadr_syncmode.equalsIgnoreCase("Super asynchronous") )
				{
					Assert.fail("HADR_SYNCMODE " + hadr_syncmode + " is invalid");
				}
			
			}
		}			
		
		logger.info("SUCCESS=> URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));			
	}
	
	
	private static void assertResponseFailure(int responseCode, JSONObject result)
	{
		Assert.fail("FAILURE=> Response code ="+responseCode+" for URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData"));
	}
	
	private static void assertExceptionFailure(int responseCode, JSONObject result)
	{
		Assert.fail("FAILURE:=>ExceptionCaught=true in responseJson for URL: "+result.get("URL")+", POSTDATA:"+result.get("PostData")+" with RESPONSECODE:"+responseCode+" and RESPONSEDATA:"+result.get("ResponseData"));		
	}
	
	private static boolean isExceptionCaught(JSONObject responseData)  
	{
		return responseData.toString().contains("\"exceptionCaught\":true");
	}
  
}
