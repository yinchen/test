package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;


/**
 * Case Type : ZOS automation case
 * Case number in wiki : 22
 * Case description : Optimize->Start Tuning->select SQL statement source: Catalog Plan or Package->
 * 					  set Maximum number of statements to capture->select capture statements that match criteria for plans->
 * 					  set filters->click Next->click Tune All Statements->set Job name->set SQLID->set Explain options-->
 * 					  click Done->Click Retune ->set options->click Done-> verify all advisors result
 * @author yinchen
 *
 */
public class TestCaptureFromCatalogPlanRetune extends TuningTestBase{
	
	@Test
	public void testRunWorkloadJob() throws FileNotFoundException, InterruptedException, IOException{
		String random = String.valueOf(System.currentTimeMillis());
		String sqlid = cs.getSQLIDByDBProfile(dbName);
		String sqlCount = "10"; //Set sql statement to capture
		String dbType = cs.getDBTypeByDBProfile(dbName);
		
		String jobName = "Workload" + random.substring(9, 13) + "-ZOS22";
		
		/**
		 * Submit job
		 */
		JSONObject result = cs.submitJob(genDescSection(jobName,random, dbName), genOQWTSection(random, sqlid, sqlCount, dbType, dbName));
		int responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		
		/**
		 * Run job
		 */
		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
		result = cs.runJob(jobId, dbName);
		
		responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String resultCode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultCode));
		
		CheckJobStatus.checkJobStatus(jobId, 8000L);
		
		
		/**
		 * Retune
		 */
		JSONObject jobForRetune = cs.getJobByJobID(jobId);
		String instID = jobForRetune.getString("INSTID");
		String stmtId = jobForRetune.getString("SQLSTMTID");
		JSONObject retuneJob = cs.retuneSingleJob(dbName, jobId, instID, stmtId);
		
		String wccJobID = retuneJob.getString("workloadid");
		String schema = retuneJob.getString("schema");
		result = cs.submitJob(gDescSection(jobName, dbName), 
				gOQWTSection(random, schema, wccJobID, sqlid, dbName, dbType));
		
		responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		/*
		 * Run retune job
		 */
		String retuneJobID = result.getString("jobid");
		Assert.assertTrue(retuneJobID != null);
		result = cs.runJob(retuneJobID, dbName);
		
		responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		resultCode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultCode));
		CheckJobStatus.checkJobStatus(retuneJobID, 8000L);
		/**
		 *Ma Wei
		 *Verify WSA
		 *Verify WIA
		*/
	
	    JSONObject resultDetails = cs.getJobByJobID(jobId);
		JSONObject jso = cs.getWorkloadJobDetails(dbName,  result.getString("jobid"), "Workload" + random.substring(8, 12) + "-ByRestAPI",resultDetails.getString("INSTID"));
		
		try {
			Object wsa_recommendation =  jso.get("wsaDDLs");
			System.out.println("Here is WSA RECOMMENDATIONS : " + wsa_recommendation.toString());
			Assert.assertTrue(wsa_recommendation.toString().contains("RUNSTATS"), "Verify the wsa can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify WSA error*******");
		}
		

		try {
			Object WIA_recommendation =  jso.get("wiaDDLsCreate");
			System.out.println("Here is WIA RECOMMENDATIONS : " + WIA_recommendation.toString());
			Assert.assertTrue( WIA_recommendation.toString().contains("INDEX"), "Verify the WIA can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify WIA error*******");
		}
		
		
		/*
		 * Delete job when check verification point successfully
		 */
		JSONObject retuneJobResult = cs.getJobByJobID(retuneJobID);
		String jobInstID1 = resultDetails.getString("INSTID");
		String jobInstID2 = retuneJobResult.getString("INSTID");
		List<String> jobInstIDList = new ArrayList<String>();
		jobInstIDList.add(jobInstID1);
		jobInstIDList.add(jobInstID2);
		JSONObject delJobStatus = cs.deleteJobs(jobInstIDList);
		String delResultCode = delJobStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
		
	}
	
	public Properties genDescSection(String jobName,String random,String dbName) {
		Properties desc = new Properties();
		desc.put("jobname", jobName);
		desc.put("jobtype", "querytunerjobs");
		desc.put("schedenabled", "0");
		desc.put("monDbConProfile", dbName);
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}

	public Properties genOQWTSection(String random,String sqlid,String sqlCount,String dbType, String dbName) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("tuningType", "WORKLOAD");
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("workloadName", "Workload" + random.substring(9, 13));
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("reExplainValCheck", true);
		oqwt_SectionData.put("wsaValCheck", true);
		oqwt_SectionData.put("wiaValCheck", true);
		oqwt_SectionData.put("wtaaValCheck", false);
		oqwt_SectionData.put("wtaaModelSelect", "modeling");
		oqwt_SectionData.put("curPathValue", "");
		oqwt_SectionData.put("curDegreeValue", "ANY");
		oqwt_SectionData.put("curRefAgeValue", "ANY");
		oqwt_SectionData.put("curMQTValue", "ALL");
		oqwt_SectionData.put("curBusiTimeValue", "NULL");
		oqwt_SectionData.put("curSysTimeValue", "NULL");
		oqwt_SectionData.put("curGetArchiveValue", "Y");
		oqwt_SectionData.put("SQLID", sqlid);
		oqwt_SectionData.put("wccJobID", "Workload#" + random);
		oqwt_SectionData.put("wccJobStatus", "Running");
		oqwt_SectionData.put("captureMaxRowsCount", sqlCount);
		oqwt_SectionData.put("type", "CATALOG");
		oqwt_SectionData.put("catalogType", "CATALOG.PLAN");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		oqwt_SectionData.put("monitoredDbType", dbType);
		return oqwt_SectionData;
	}
	
	public Properties gDescSection(String jobName,String dbName) {
		Properties desc = new Properties();
		desc.put("jobname", "Retune_" + jobName);
		desc.put("jobType", "querytunerjobs");
		desc.put("schedenabled", 0);
		desc.put("mondbconprofile", dbName);
		desc.put("jobcreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}
	
	public Properties gOQWTSection(String random,String schema,String wccJobID,String sqlid,String dbName,String dbType) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", "true");
		oqwt_SectionData.put("tuningType", "WORKLOAD");
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("workloadName", "Workload_" + random.substring(9, 13));
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("reExplainValCheck", true);
		oqwt_SectionData.put("wsaValCheck", true);
		oqwt_SectionData.put("wiaValCheck", true);
		oqwt_SectionData.put("wtaaValCheck", false);
		oqwt_SectionData.put("wtaaModelSelect", "modeling");
		oqwt_SectionData.put("curPathValue", "\"SYSIBM\",\"SYSFUN\",\"SYSPROC\",\"SYSIBMADM\",\"C01D02\"");
		
		oqwt_SectionData.put("curDegreeValue", "1");
		oqwt_SectionData.put("curRefAgeValue", "0");
		oqwt_SectionData.put("curMQTValue", "SYSTEM");
		oqwt_SectionData.put("curBusiTimeValue", "NULL");
		oqwt_SectionData.put("curSysTimeValue", "NULL");
		oqwt_SectionData.put("curGetArchiveValue", "N");
		oqwt_SectionData.put("SQLID", sqlid);
		oqwt_SectionData.put("wccJobID", wccJobID);
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("queryText", "");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		oqwt_SectionData.put("monitoredDbType", dbType);
		return oqwt_SectionData;
	}	
	
	
}




























