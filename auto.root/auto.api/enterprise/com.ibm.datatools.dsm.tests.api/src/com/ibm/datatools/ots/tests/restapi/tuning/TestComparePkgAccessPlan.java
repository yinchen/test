package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class TestComparePkgAccessPlan  extends TuningTestBase{
	
	@Test
	public void TestComparePkgAccessPlan() throws InterruptedException, FileNotFoundException, IOException {
		JSONObject result = cs.listCacheStatementForCatalogPackageWithFilters(dbName, "1");
		int numRows = (Integer)result.get("numRows");
		
		JSONArray items = result.getJSONArray("items");
		int itemsSize = items.size();
		
		if(numRows == 0 || itemsSize == 0){
			String errorMessage = "No SQL statement found in calalog package of DB "+dbName+" , " +
								  "please use another db connection";
			Assert.fail(errorMessage);
			return;
		}
		
		//prepare select record
		JSONObject item = items.getJSONObject(0);
		Properties params = new Properties();
		params.put("monDbConProfile", dbName);
		params.put("collId", item.getString("COLLID"));
		params.put("packageName", item.getString("NAME"));
		params.put("stmtNoI", item.getString("STMTNOI"));
		params.put("stmtText", item.getString("FullStatementText"));
		params.put("sectNoI", item.getString("SECTNOI"));
		
		JSONObject getVersionsResult = cs.listComparePackageVersions(params);
		String resultCode = getVersionsResult.getString("resultCode");
		int versionsNumRows = (Integer)getVersionsResult.get("numRows");
		JSONArray versionsItems = getVersionsResult.getJSONArray("items");
		if("SUCCESS".equals(resultCode) && versionsNumRows>0){
			Map newMap = new HashMap<String,Object>();
			newMap.put("monDbConProfile", dbName);
			int j = 1;
			for(int i = 0; i< versionsItems.size(); i++){
			    Map itemMap = (Map) versionsItems.get(i);
			    if( "Y".equals(itemMap.get("explain")) && j < 3){
			    		if(newMap.containsKey("stmt1")){
			    			newMap.put("stmt2", itemMap);
			    		}else {
			    			newMap.put("stmt1", itemMap);
			    		}
			   		j++;
			    } else if(j == 2) {
			    		break;
			    }
			}
			if(j < 2){
				String errorMessage = "There are no two versions with explain info to do the comparision";
				Assert.fail(errorMessage);
				return;
			}
			JSONObject getCompareResult = cs.getComparePackageResult(newMap);
			String compareResultCode = getCompareResult.getString("resultCode");
			String sessionID = getCompareResult.getString("sessionID");
			if( "SUCCESS".equals(compareResultCode) && sessionID != null) {
				System.out.println("Compare package versions successfully.");
			} else {
				Assert.fail("Failed to generate access plan.");
				return;
			}
			
		} else{
			String errorMessage = "Get compared package versions error";
			Assert.fail(errorMessage);
			return;
		}
	}
}
