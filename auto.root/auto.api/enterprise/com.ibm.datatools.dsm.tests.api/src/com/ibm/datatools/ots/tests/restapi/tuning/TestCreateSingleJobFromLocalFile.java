package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;


/**
 * Case Type : ZOS and LUW automation case
 * Case numbei in wiki : ZOS(2) LUW(2)
 * Case description for ZOS : Optimize->Start Tuning->select SQL statement source: Local File->
 * 					          set Maximum number of statements to capture->set Local file name->set SQL statement delimiter->set schema->
 * 					          click Next->select one statement->click View Full Statement button->click Tune selected statement->
 * 					  		  set Explain options->click Done->verify all advisors result(Report, APG, SA, IA).
 * 
 * Case description for LUW : Optimize->Start Tuning->select SQL statement source: Local File->
 * 							  set Maximum number of statements to capture->set Local file name->set SQL statement delimiter->set schema->
 * 							  click Next->select one statement->click Tune selected statement->click Done->verify all advisors result(Report, APG, SA, IA).
 * @author yinchen
 *
 */
public class TestCreateSingleJobFromLocalFile extends TuningTestBase{
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("TestCreateSingleJobFromLocalFile.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	@Test(dataProvider = "testdata")
	public void testCreateSingleJob(Object key,Object inputPara) throws FileNotFoundException, InterruptedException, IOException{
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		String random = String.valueOf(System.currentTimeMillis());
		
		String dbType = cs.getDBTypeByDBProfile(dbName);
		String sqlid = cs.getSQLIDByDBProfile(dbName);
		
		//get the sql path from DSM server
		String sqlFileName = cs.uploadFile(obj.getString("sqlFileName"));
		/**
		 *   Select a SQL from file
		 */
			
		String queryText = cs.selectSQLFromFile(dbName, sqlFileName, schema);
		System.out.println(queryText);
		
		JSONObject result = cs.submitJob(genDescSection(random, dbName), 
				genOQWTSection(random, queryText, schema, dbType, sqlid,dbName));
		
		int responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		
		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
		
		result = cs.runJob(jobId, dbName);
		responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		
		String resultCode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultCode));
		
		CheckJobStatus.checkJobStatus(jobId, 5000L);
		
		/*
		 * Delete job 
		 */
		JSONObject delJob = cs.getJobByJobID(jobId);
		JSONObject delStatus = cs.deleteJob(delJob.getString("INSTID"));
		String delResultCode = delStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
	}
	
	public Properties genDescSection(String random,String dbName) {
		Properties desc = new Properties();
		desc.put("jobname", "Query" + random.substring(8, 12) + "-ZOS2");
		desc.put("jobtype", "querytunerjobs");
		desc.put("schedenabled", "0");
		desc.put("monDbConProfile", dbName);
		desc.put("schedenabled", 1);
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}

	public Properties genOQWTSection(String random, String queryText,
			String schema, String dbType, String sqlid,String dbName) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("queryid", "");
		oqwt_SectionData.put("tuningType", "SQL_BASED");
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("jobName", "Query" + random.substring(9, random.length()));
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("sqlid", sqlid);
		oqwt_SectionData.put("source", "LOCAL_FILE");
		oqwt_SectionData.put("degree", "ANY");
		oqwt_SectionData.put("hint", "");
		oqwt_SectionData.put("refreshAge", "ANY");
		oqwt_SectionData.put("MQT", "ALL");
		oqwt_SectionData.put("systemTime", "NULL");
		oqwt_SectionData.put("businessTime", "NULL");
		oqwt_SectionData.put("getArchive", "Y");
		oqwt_SectionData.put("returnAllStats", "OFF");
		oqwt_SectionData.put("queryName", "Query" + random.substring(9, random.length()));
		oqwt_SectionData.put("resultName", " ");
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("reExplainValCheck", true);
		oqwt_SectionData.put("apgValCheck", true);
		oqwt_SectionData.put("formatQueryValCheck", true);
		oqwt_SectionData.put("iaValCheck", true);
		oqwt_SectionData.put("saValCheck", true);
		oqwt_SectionData.put("queryText", queryText);
		oqwt_SectionData.put("hashID", "");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		oqwt_SectionData.put("monitoredDbType", dbType);
		return oqwt_SectionData;
	}
	
}






































