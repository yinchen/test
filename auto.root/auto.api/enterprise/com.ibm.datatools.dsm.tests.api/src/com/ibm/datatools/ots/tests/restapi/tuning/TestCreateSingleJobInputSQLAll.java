package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;


/**
 * Case Type : ZOS automation case & LUW automation case
 * Case number in wiki: ZOS(1)   &  LUW(1)
 * Case description of ZOS: Optimize->Start Tuning->select SQL statement source: Input SQL Statement-> 
 * 					  		set schema->set SQLID->input SQL text->set Explain options->select collect distribution statistics->
 * 					  		set statement name->set result name->select all advisors and tools->
 * 					  		click Done->verify all advisors result(Report, APG, SA, IA).
 * Case description of LUW: Optimize->Start Tuning->select SQL statement source: Input SQL Statement->
 * 							set schema->input SQL text->set Statement name->set Result name->
 * 							click Done->verify all advisors result(Report, APG, SA, IA).
 * 
 * 
 * @author yinchen
 *
 */
public class TestCreateSingleJobInputSQLAll extends TuningTestBase{
	

	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		// return multiple JSON post properties
		cs = new TuningCommandService("TestCreateSingleJobInputSQLAll.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);

	}

	@Test(dataProvider = "testdata")
	public void testRunSingleQueryJob(Object key, Object inputPara) throws InterruptedException, FileNotFoundException, IOException {
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		String queryText = obj.getString("queryText");
		
		String sqlid = cs.getSQLIDByDBProfile(dbName);
		String dbType = cs.getDBTypeByDBProfile(dbName);
		
		String random = String.valueOf(System.currentTimeMillis());
		JSONObject result = cs.submitJob(genDescSection(random), genOQWTSection(random, queryText,sqlid,dbName,dbType));
		
		int responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
			
		result = cs.runJob(jobId, dbName);
		responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String resultcode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultcode));
			
		CheckJobStatus.checkJobStatus(jobId, 5000L);
		
		/**
		 *Ma Wei
		 *Verify SA
		 */
		
		JSONObject resultDetails = cs.getJobByJobID(jobId);
		JSONObject jso = cs.getSingleJobDetails(dbName,  result.getString("jobid"), "Query" + random.substring(8, 12) + "-ZOS1",resultDetails.getString("INSTID"), resultDetails.getString("RESULTID"));
		
		try {
			Object sa_recommendation =  jso.get("SA_RECOMMENDATIONS");
			System.out.println("Here is SA RECOMMENDATIONS : " + sa_recommendation.toString());
			Assert.assertTrue(sa_recommendation.toString().contains("RUNSTATS"), "Verify the sa can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify SA error*******");
		}
		
		
		/*
		 * Delete job when check verification point successfully
		 */
		JSONObject delStatus = cs.deleteJob(resultDetails.getString("INSTID"));
		String delResultCode = delStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
		
	}
		


	public Properties genDescSection(String random) {
		Properties desc = new Properties();
		desc.put("jobname", "Query" + random.substring(8, 12) + "-ZOS1");
		desc.put("jobtype", "querytunerjobs");
		desc.put("schedenabled", 1);
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("DBLOGINUSER", "db2inst1");
		return desc;
	}

	public Properties genOQWTSection(String random, String queryText,String sqlid,String dbName,String dbType) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("queryid", "");
		oqwt_SectionData.put("tuningType", "SQL_BASED");
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("jobName", "Query_" + random + "-Result_" + random);
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("sqlid", sqlid);
		oqwt_SectionData.put("degree", "ANY");
		oqwt_SectionData.put("hint", "");
		oqwt_SectionData.put("refreshAge", "ANY");
		oqwt_SectionData.put("MQT", "ALL");
		oqwt_SectionData.put("systemTime", "NULL");
		oqwt_SectionData.put("businessTime", "NULL");
		oqwt_SectionData.put("getArchive", "Y");
		oqwt_SectionData.put("returnAllStats", "OFF");
		oqwt_SectionData.put("queryName", "Query_" + random);
		oqwt_SectionData.put("resultName", "Result_" + random);
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("reExplainValCheck", true);
		oqwt_SectionData.put("apgValCheck", true);
		oqwt_SectionData.put("formatQueryValCheck", true);
		oqwt_SectionData.put("iaValCheck", true);
		oqwt_SectionData.put("saValCheck", true);
		oqwt_SectionData.put("queryText", queryText);
		oqwt_SectionData.put("hashID", "");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		oqwt_SectionData.put("monitoredDbType", dbType);
		return oqwt_SectionData;
	}
}










