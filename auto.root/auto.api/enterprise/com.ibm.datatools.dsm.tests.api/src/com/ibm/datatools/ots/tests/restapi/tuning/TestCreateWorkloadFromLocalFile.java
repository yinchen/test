package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;

/**
 * Case Type : Auto case for DB2LUW
 * Case number on wiki : 6
 * Case description :   Optimize->Start Tuning->select SQL statement source: Local File->
 * 						set Maximum number of statements to capture->set Local file name->
 * 						set SQL statement delimiter->set schema->click Next->
 * 						select Tune all Statements-> set Job name-> select Statistics, Statistical views->
 * 						click Done->verify advisors result
 * @author yinchen
 *
 */
public class TestCreateWorkloadFromLocalFile extends TuningTestBase{
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("TestCreateWorkloadFromLocalFile.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	@Test(dataProvider = "testdata")
	public void testCreateWorkloadJob(Object key,Object inputPara) throws FileNotFoundException, InterruptedException, IOException{
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		String random = String.valueOf(System.currentTimeMillis());		
		String jobName = "Workload" +random.substring(9, 13) +"-LUW6";
		String wccJobID = jobName +"#" + random;
		
		String dbType = cs.getDBTypeByDBProfile(dbName);
		String sqlid = cs.getSQLIDByDBProfile(dbName);
		
		boolean wsa = obj.getBoolean("wsa");
		boolean wia = obj.getBoolean("wia");
		boolean wto = obj.getBoolean("wto");
		boolean wsva = obj.getBoolean("wsva");
		boolean wda = obj.getBoolean("wda");
		//get the sql path from DSM server
		String sqlFileName = cs.uploadFile(obj.getString("sqlFileName"));
		
		JSONObject result = cs.submitJob(genDescSection(jobName, dbName), 
				genOQWTSection(jobName, sqlFileName, schema, dbType, dbName, wccJobID, sqlid, wsa, wia, wto, wsva, wda));
		int responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		
		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
		
		result = cs.runJob(jobId, dbName);
		responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String resultCode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultCode));
		
		CheckJobStatus.checkJobStatus(jobId, 10000L);
		
		/**
		 *Ma Wei
		 *Verify WSA
		 *Verify WSVA
		*/
	
	    JSONObject resultDetails = cs.getJobByJobID(jobId);
	 		    
		JSONObject jso = cs.getWorkloadJobDetails(dbName, result.getString("jobid"),jobName,resultDetails.getString("INSTID"));
		
		try {
			Object wsa_recommendation =  jso.get("wsaDDLs");
			System.out.println("Here is WSA RECOMMENDATIONS : " + wsa_recommendation.toString());
			Assert.assertTrue(wsa_recommendation.toString().contains("RUNSTATS"), "Verify the wsa can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify WSA error*******");
		}
		

		try {
			Object WSVA_recommendation =  jso.get("wsvaDDLs");
			System.out.println("Here is WSVA RECOMMENDATIONS : "+WSVA_recommendation.toString());
			Assert.assertTrue( WSVA_recommendation.toString().contains("CREATE VIEW"), "Verify the WSVA can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify WSVA error*******");
		}
		
		
		
		/*
		 * Delete job when check verification point successfully
		 */
		JSONObject delStatus = cs.deleteJob(resultDetails.getString("INSTID"));
		String delResultCode = delStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
	}
	
	public Properties genDescSection(String jobName,String dbName) {
		Properties desc = new Properties();
		desc.put("jobname", jobName);
		desc.put("jobType", "querytunerjobs");
		desc.put("schedenabled", 0);
		desc.put("mondbconprofile", dbName);
		desc.put("jobcreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}
	
	public Properties genOQWTSection(String jobName, String sqlFileName,
			String schema, String dbType,String dbName,String wccJobID,
			String sqlid,boolean wsa,boolean wia,boolean wto,boolean wsva,boolean wda) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("tuningType", "WORKLOAD");
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("workloadName", jobName);
		oqwt_SectionData.put("reExplainWorkloadValCheck", true);
		oqwt_SectionData.put("wsaValCheck", wsa);
		oqwt_SectionData.put("wiaValCheck", wia);
		oqwt_SectionData.put("wtoValCheck", wto);
		oqwt_SectionData.put("wsvaValCheck", wsva);
		oqwt_SectionData.put("wdaValCheck", wda);
		oqwt_SectionData.put("SQLID", sqlid);
		oqwt_SectionData.put("wccJobID", wccJobID);
		oqwt_SectionData.put("wccJobStatus", "Running");
		oqwt_SectionData.put("sqlFileName", sqlFileName);
		oqwt_SectionData.put("stmtDelimiter", ";");
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("stmtDelim", ";");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbType", dbType);
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		return oqwt_SectionData;
	}
	
}
