package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;

public class TestCreateWorkloadJob extends TuningTestBase{

	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("TuneWorkloadJob.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	@Test(dataProvider = "testdata")
	public void testCreateWorkloadJob(Object key,Object inputPara) throws InterruptedException, FileNotFoundException, IOException {
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		String random = String.valueOf(System.currentTimeMillis());
		String wccJobID = "workload#" + random;
		
		String sqlid = cs.getSQLIDByDBProfile(dbName);
		String dbType = cs.getDBTypeByDBProfile(dbName);
		
		//get the sql path from DSM server
		String sqlFileName = cs.uploadFile(obj.getString("sqlFileName"));
		
		JSONObject result = cs.submitJob(genDescSection(random,dbName), 
				genOQWTSection(random,sqlFileName ,schema,dbType,dbName,wccJobID,sqlid));
		
		int responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
			
		result = cs.runJob(jobId, dbName);
		responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String resultcode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultcode));
		
		CheckJobStatus.checkJobStatus(jobId, 15000L);
		
		/**
		 *Ma Wei
		 *Verify WSA
		*/
	
	    JSONObject resultDetails = cs.getJobByJobID(jobId);
		JSONObject jso = cs.getWorkloadJobDetails(dbName,  result.getString("jobid"), "Workload" + random.substring(8, 12),resultDetails.getString("INSTID"));
		
		try {
			Object wsa_recommendation =  jso.get("wsaDDLs");
			System.out.println("Here is WSA RECOMMENDATIONS : " + wsa_recommendation.toString());
			Assert.assertTrue(wsa_recommendation.toString().contains("RUNSTATS TABLESPACE"), "Verify the wsa can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify WSA error*******");
		}
		
		try {
			Object wapa_recommendation =  jso.get("WAPA_STATEMENT");
			System.out.println("Here is WAPA RECOMMENDATIONS SUMMARY : " + wapa_recommendation.toString());
			Assert.assertTrue(wapa_recommendation.toString().contains("\"ExplainStatus\":\"Yes\""), "Verify the wapa can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify WSA error*******");
		}
	
		/*
		 * Delete job when check verification point successfully
		 */
		JSONObject delStatus = cs.deleteJob(resultDetails.getString("INSTID"));
		String delResultCode = delStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
	}
	
	public Properties genDescSection(String random,String dbName) {
		Properties desc = new Properties();
		desc.put("jobname", "Workload" + random.substring(8, 12));
		desc.put("jobType", "querytunerjobs");
		desc.put("schedenabled", 0);
		desc.put("mondbconprofile", dbName);
		desc.put("jobcreator", "admin");
		desc.put("jobid", "0");
		desc.put("jobdesc", "");
		desc.put("dbreqforjob", "1");
		return desc;
	}
	
	public Properties genOQWTSection(String random, String sqlFileName,
			String schema, String dbType,String dbName,String wccJobID,String sqlid) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("tuningType", "WORKLOAD");
		oqwt_SectionData.put("wccJobStatus", "Running");
		oqwt_SectionData.put("sqlFileName", sqlFileName);
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("workloadName", "Workload_" + random);
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("wtaaValCheck", false);
		oqwt_SectionData.put("wsaValCheck", true);
		oqwt_SectionData.put("curDegreeValue", "ANY");
		oqwt_SectionData.put("reExplainWorkloadValCheck", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("curRefAgeValue", "ANY");
		oqwt_SectionData.put("stmtDelimiter", ";");
		oqwt_SectionData.put("wtaaModelSelect", "modeling");
		oqwt_SectionData.put("wccJobID", wccJobID);
		oqwt_SectionData.put("dbconfigured", true);
		oqwt_SectionData.put("curMQTValue", "ALL");
		oqwt_SectionData.put("curSysTimeValue", "NULL");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("curBusiTimeValue", "NULL");
		oqwt_SectionData.put("curPathValue", "");
		oqwt_SectionData.put("curGetArchiveValue", "Y");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("wiaValCheck", true);
		oqwt_SectionData.put("wapaValCheck", true);
		oqwt_SectionData.put("SQLID", sqlid);
		//oqwt_SectionData.put("monitoredDbName", dbName);
		oqwt_SectionData.put("monitoredDbType", dbType);
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		return oqwt_SectionData;
	}

}































