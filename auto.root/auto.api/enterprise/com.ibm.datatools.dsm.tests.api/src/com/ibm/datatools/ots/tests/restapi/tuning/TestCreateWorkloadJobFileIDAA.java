package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;

/**
 * Case Type : ZOS automation case
 * Case number on wiki : 9
 * Case description : Optimize->Start Tuning->select SQL statement source: Local File->
 * 					  set Maximum number of statements to capture->set Local file name->set SQL statement delimiter->set schema->
 * 					  click Next->click Tune All Statements->set Job name->set SQLID->set Explain options->
 * 					  select IDAA with using a virtual accelerator->set accelerator->click Done->verify advisors result
 * @author yinchen
 *
 */
public class TestCreateWorkloadJobFileIDAA extends TuningTestBase{

	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("TestCreateWorkloadJobFileIDAA.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	@Test(dataProvider = "testdata")
	public void testCreateWorkloadJob(Object key,Object inputPara) throws InterruptedException, FileNotFoundException, IOException {
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		String wtaaAccelName = obj.getString("wtaaAccelName");
		//get the sql path from DSM server
		String sqlFileName = cs.uploadFile(obj.getString("sqlFileName"));
		
		String dbType = cs.getDBTypeByDBProfile(dbName);
		String sqlid = cs.getSQLIDByDBProfile(dbName);
		
		String random = String.valueOf(System.currentTimeMillis());
		JSONObject result = cs.submitJob(genDescSection(random,dbName), 
				genOQWTSection(random,sqlFileName,schema,dbName,dbType,sqlid,wtaaAccelName));
		
		int responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
			
		result = cs.runJob(jobId, dbName);
		responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String resultcode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultcode));
		
		CheckJobStatus.checkJobStatus(jobId, 7000L);
		
		/**
		 *Ma Wei
		 *Verify WSA,IDAA
		 */
	
	    JSONObject resultDetails = cs.getJobByJobID(jobId);
		JSONObject jso = cs.getWorkloadJobDetails(dbName,  result.getString("jobid"), "Workload" + random.substring(8, 12) + "-ZOS9",resultDetails.getString("INSTID"));
		
		//Check virtual accelerator status
		String acceleratorNotExist = "The virtual accelerator "+wtaaAccelName+" could not be located on the connected subsystem.";
		String acceletarotNotStarted = "The virtual accelerator "+wtaaAccelName+" is not running.";
		
		Object jobExecLogObj =null;
		try {
			jobExecLogObj = jso.get("jobExecLog");
		} catch (Exception e) {
		}
		
		System.out.println("jobExecLogObj >>> "+jobExecLogObj);
		
		if(jobExecLogObj != null){
			if(jobExecLogObj.toString().contains(acceleratorNotExist)){
				System.out.println("Accelerator "+wtaaAccelName+" was not exist");
				throw new SkipException("Accelerator "+wtaaAccelName+" was not exist");
			}
			
			if(jobExecLogObj.toString().contains(acceletarotNotStarted)){
				System.out.println("Accelerator "+wtaaAccelName+" was not started");
				throw new SkipException("Accelerator "+wtaaAccelName+" was not started");
			}
		}
		
		
		try {
			Object wsa_recommendation =  jso.get("wsaDDLs");
			System.out.println("Here is WSA RECOMMENDATIONS : " + wsa_recommendation.toString());
			Assert.assertTrue(wsa_recommendation.toString().contains("RUNSTATS TABLESPACE"), "Verify the wsa can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify WSA error*******");
		}
		

		try {
			Object idaa_recommendation =  jso.get("wtaaTableSpec");
			System.out.println("Here is IADD RECOMMENDATIONS : " + idaa_recommendation.toString());
			Assert.assertTrue( idaa_recommendation.toString().contains("TPCDS"), "Verify the IDAA can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify IDAA error*******");
		}
		
		
		/*
		 * Delete job when check verification point successfully
		 */
		JSONObject delStatus = cs.deleteJob(resultDetails.getString("INSTID"));
		String delResultCode = delStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
	}
	
	public Properties genDescSection(String random,String dbName ) {
		Properties desc = new Properties();
		desc.put("jobname", "Workload" + random.substring(8, 12) + "-ZOS9");
		desc.put("jobType", "querytunerjobs");
		desc.put("schedenabled", 0);
		desc.put("mondbconprofile", dbName);
		desc.put("jobcreator", "admin");
		desc.put("jobid", "0");
		desc.put("jobdesc", "");
		desc.put("dbreqforjob", "1");
		return desc;
	}
	
	public Properties genOQWTSection(String random, String sqlFileName, 
			String schema,String dbName,String dbType,String sqlid,String wtaaAccelName) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("tuningType", "WORKLOAD");
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("workloadName", "Workload" + random);
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("reExplainWorkloadValCheck", true);
		oqwt_SectionData.put("wsaValCheck", true);
		oqwt_SectionData.put("wiaValCheck", true);
		oqwt_SectionData.put("wtaaValCheck", true);
		oqwt_SectionData.put("wtaaModelSelect", "virtual");
		oqwt_SectionData.put("wtaaAccelName", wtaaAccelName);
		oqwt_SectionData.put("curPathValue", "");
		oqwt_SectionData.put("curDegreeValue", "ANY");
		oqwt_SectionData.put("curRefAgeValue", "ANY");
		oqwt_SectionData.put("curMQTValue", "ALL");
		oqwt_SectionData.put("curBusiTimeValue", "NULL");
		oqwt_SectionData.put("curSysTimeValue", "NULL");
		oqwt_SectionData.put("curGetArchiveValue", "Y");
		oqwt_SectionData.put("SQLID", sqlid);
		oqwt_SectionData.put("Select", "3");
		oqwt_SectionData.put("wccJobID", "workload#" + random);
		oqwt_SectionData.put("wccJobStatus", "Running");
		oqwt_SectionData.put("sqlFileName", sqlFileName);
		oqwt_SectionData.put("stmtDelimiter", ";");
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("captureMaxRowsCount", "100");
		oqwt_SectionData.put("stmtDelim", ";");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		oqwt_SectionData.put("monitoredDbType", dbType);
		return oqwt_SectionData;
	}
	
}































