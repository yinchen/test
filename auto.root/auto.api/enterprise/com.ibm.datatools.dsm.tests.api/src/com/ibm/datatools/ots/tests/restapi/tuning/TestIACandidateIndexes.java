package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;


/**
 * Case Type : LUW automation case
 * Case number on wiki :  4
 * Case description : Select one single tuning job with IA recommendations->View Results->
 * 					  in IA result page click Test Candidate Indexes->do Add/Edit/virtually drop Existing indexes->
 * 					  click Run Test->set custom statistics for the indexes-> click Run->verify IA whatif result
 * @author yinchen
 *
 */
public class TestIACandidateIndexes extends TuningTestBase{
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("TestIACandidateIndexes.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	@Test(dataProvider = "testdata")
	public void testCreateSingleJob(Object key,Object inputPara) throws FileNotFoundException, InterruptedException, IOException{
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		String random = String.valueOf(System.currentTimeMillis()).substring(9, 13);
		String jobName = "Query" + random +"-LUW4";
		
		String queryText = obj.getString("queryText");
		String sch = obj.getString("sch");
		
		String dbType = cs.getDBTypeByDBProfile(dbName);
		/**
		 *   Create single job with IA
		 */
		JSONObject result = cs.submitJob(genDescSection(jobName, dbName), 
				genOQWTSection(jobName, queryText, schema, dbType, dbName));
		
		int responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		
		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
		
		result = cs.runJob(jobId, dbName);
		responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		
		String resultCode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultCode));
		
		CheckJobStatus.checkJobStatus(jobId, 25000L);
		
		JSONObject singleJob = cs.getJobByJobID(jobId);
		
		//System.out.println(">>"+singleJob);
		String instID = singleJob.getString("INSTID");
		String arId = singleJob.getString("RESULTID");
		JSONObject singleJobDetails = cs.getSingleJobDetails(dbName, jobId, jobName, instID, arId);
		/**
		 *Ma Wei
		 *Verify IA
		 */
		
		try {
			Object indexes = singleJobDetails.get("IA_RECOMMENDATIONS");
			System.out.println("Here is IA RECOMMENDATIONS : " + indexes.toString());
			Assert.assertTrue(indexes.toString().contains("STORE_SALES"), "Verify the index can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify IA error*******");
		}
		
		
		
		
		
		String queryNodeID = singleJobDetails.getString("queryNodeID");
		String queryResultID = singleJobDetails.getString("queryResultID");
		String WTI_JOB_ID = singleJobDetails.getString("jobid");
		
	
		
		/**
		 *  Run candidate indexes
		 */
		JSONObject candidateIndexes = cs.getCandidateIndex(dbName, queryNodeID, queryResultID, WTI_JOB_ID);
		//System.out.println(">>"+candidateIndexes);
		JSONArray items = candidateIndexes.getJSONArray("items");
		String strIndex = null;
		
		
		
		for(int i=0; i<items.size(); i++){
			JSONObject objCandidateIndexes = items.getJSONObject(i);
			String INDEXNAME = objCandidateIndexes.getString("INDEXNAME");
			String INDEXCREATOR = objCandidateIndexes.getString("INDEXCREATOR");
			String TABLENAME = objCandidateIndexes.getString("TABLENAME");
			String TABLESCHEMA = objCandidateIndexes.getString("TABLESCHEMA");
			String KEYCOLUMNS = objCandidateIndexes.getString("KEYCOLUMNS");
			String INCLUDECOLUMNS = objCandidateIndexes.getString("INCLUDECOLUMNS");
			String UNIQUE = objCandidateIndexes.getString("UNIQUE");
			String ESTIMATEDDISKSPACE = objCandidateIndexes.getString("ESTIMATEDDISKSPACE");
			String CREATEDBY = objCandidateIndexes.getString("CREATEDBY");
			String FIRSTKEYCARD = objCandidateIndexes.getString("FIRSTKEYCARD");
			String FIRST2KEYCARD = objCandidateIndexes.getString("FIRST2KEYCARD");
			String FIRST3KEYCARD = objCandidateIndexes.getString("FIRST3KEYCARD");
			String FIRST4KEYCARD = objCandidateIndexes.getString("FIRST4KEYCARD");
			String FULLKEYCARD = objCandidateIndexes.getString("FULLKEYCARD");
			String PCTFREE = objCandidateIndexes.getString("PCTFREE");
			String FREEPAGE = objCandidateIndexes.getString("FREEPAGE");
			String MINPCTUSED = objCandidateIndexes.getString("MINPCTUSED");
			String CLUSTERRATIO = objCandidateIndexes.getString("CLUSTERRATIO");
			String SEQPAGES = objCandidateIndexes.getString("SEQPAGES");
			String DENSITY = objCandidateIndexes.getString("DENSITY");
			String PAGESIZE = objCandidateIndexes.getString("PAGESIZE");
			String LEVELS = objCandidateIndexes.getString("LEVELS");
			String LEAFPAGES = objCandidateIndexes.getString("LEAFPAGES");
			String REVERSESCANS = objCandidateIndexes.getString("REVERSESCANS");
			String ISRECOMMEND = objCandidateIndexes.getString("ISRECOMMEND");
			strIndex = "{\"INDEXNAME\" : \""+INDEXNAME+"\",\"INDEXCREATOR\" : \""+INDEXCREATOR+"\",\"TABLENAME\" : \""+TABLENAME+"\"," +
					"\"TABLESCHEMA\" : \""+TABLESCHEMA+"\",\"KEYCOLUMNS\" : \""+KEYCOLUMNS+"\",\"INCLUDECOLUMNS\" : \""+INCLUDECOLUMNS+"\"," +
					"\"UNIQUE\" : \""+UNIQUE+"\",\"ESTIMATEDDISKSPACE\" : \""+ESTIMATEDDISKSPACE+"\",\"CREATEDBY\" : \""+CREATEDBY+"\"," +
					"\"FIRSTKEYCARD\" : \""+FIRSTKEYCARD+"\",\"FIRST2KEYCARD\" : \""+FIRST2KEYCARD+"\",\"FIRST3KEYCARD\" : \""+FIRST3KEYCARD+"\"," +
					"\"FIRST4KEYCARD\" : \""+FIRST4KEYCARD+"\",\"FULLKEYCARD\" : \""+FULLKEYCARD+"\",\"PCTFREE\" : \""+PCTFREE+"\"," +
					"\"FREEPAGE\" : \""+FREEPAGE+"\",\"MINPCTUSED\" : \""+MINPCTUSED+"\",\"CLUSTERRATIO\" : \""+CLUSTERRATIO+"\"," +
					"\"SEQPAGES\" : \""+SEQPAGES+"\",\"DENSITY\" : \""+DENSITY+"\",\"PAGESIZE\" : \""+PAGESIZE+"\",\"LEVELS\" : \""+LEVELS+"\"," +
					"\"LEAFPAGES\" : \""+LEAFPAGES+"\",\"REVERSESCANS\" : \""+REVERSESCANS+"\",\"ISRECOMMEND\" : \""+ISRECOMMEND+"\"},";
		}
		strIndex= " ["+strIndex.substring(0, strIndex.length()-1)+"] ";
		result = cs.submitJob(gDescSection(jobName), g(queryNodeID, queryResultID, WTI_JOB_ID, dbName, strIndex, sch));
		String candidateIndexJobId = result.getString("jobid");
		result = cs.runJob(candidateIndexJobId, dbName);
		
		responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String resultcode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultcode));
		
		CheckJobStatus.checkJobStatus(candidateIndexJobId, 5000L);
		
		/**
		 *Ma Wei
		 *Verify What-if job
		 */
		try {
			responseCode = (Integer) result.get("ResponseCode");
			CheckJobStatus.checkResponseCode(result, responseCode);
			resultcode = result.getString("resultcode");
			Assert.assertTrue("success".equals(resultcode));
		} catch (Exception e) {
			Assert.fail("******* Verify what if job error *******");
		}
		
		
		
		/*
		 * Delete job when run what-if job successfully
		 */
		JSONObject whatIfJobStatus = cs.getJobByJobID(candidateIndexJobId);
		String whatIfJobInstID = whatIfJobStatus.getString("INSTID");
		List<String> jobInstID = new ArrayList<String>();
		jobInstID.add(instID);
		jobInstID.add(whatIfJobInstID);
		JSONObject delJobStatus = cs.deleteJobs(jobInstID);
		String delResultCode = delJobStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
		
	}
	
	public Properties genDescSection(String jobName,String dbName) {
		Properties desc = new Properties();
		desc.put("jobname", jobName);
		desc.put("jobtype", "querytunerjobs");
		desc.put("schedenabled", "0");
		desc.put("monDbConProfile", dbName);
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}

	public Properties genOQWTSection(String jobName,String queryText,String schema, String dbType, String dbName) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("queryid", "");
		oqwt_SectionData.put("tuningType", "SQL_BASED");
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("jobName", jobName);
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("returnAllStats", "OFF");
		oqwt_SectionData.put("queryName", jobName);
		oqwt_SectionData.put("resultName", " ");
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("reExplainValCheck", true);
		oqwt_SectionData.put("apgValCheck", true);
		oqwt_SectionData.put("formatQueryValCheck", true);
		oqwt_SectionData.put("iaValCheck", true);
		oqwt_SectionData.put("saValCheck", true);
		oqwt_SectionData.put("queryText", queryText);
		oqwt_SectionData.put("hashID", "");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		oqwt_SectionData.put("monitoredDbType", dbType);
		return oqwt_SectionData;
	}
	
	
	public Properties gDescSection(String jobName) {
		Properties desc = new Properties();
		desc.put("jobname", "WhatIfAnalysis_"+jobName);
		desc.put("jobtype", "testcandidatejob");
		desc.put("schedenabled", "0");
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}
								
	public Properties g(String queryNodeID,String queryResultID,String WTI_JOB_ID,
			String dbName,String strIndex,String sch) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("tuningType", "WHAT-IF-ANALYSIS");
		oqwt_SectionData.put("queryNodeID", queryNodeID);
		oqwt_SectionData.put("queryResultID", queryResultID);
		oqwt_SectionData.put("WTI_JOB_ID", WTI_JOB_ID);
		oqwt_SectionData.put("profile", dbName);
		oqwt_SectionData.put("indexesJSON", strIndex);
		oqwt_SectionData.put("disabledIndexesJSON", "");
		oqwt_SectionData.put("schema", sch);
		return oqwt_SectionData;
	}
	
}


































