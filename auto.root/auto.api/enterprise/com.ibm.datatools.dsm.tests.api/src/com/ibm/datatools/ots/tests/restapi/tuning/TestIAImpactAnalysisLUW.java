package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;

import net.sf.json.JSONObject;

/**
 * Case Type : LUW automation case
 * Case number on wiki : 5
 * Case description : Select one single tuning job with IA recommendations->View Results->
 * 					  in IA result page click Impact analysis->verify results
 * @author yinchen
 *
 */
public class TestIAImpactAnalysisLUW extends TuningTestBase{
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("TestIAWhatIf.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	@Test(dataProvider = "testdata")
	public void testCreateSingleIAJob(Object key,Object inputPara) throws FileNotFoundException, InterruptedException, IOException{
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		
		String random = String.valueOf(System.currentTimeMillis());
		String queryText = obj.getString("queryText");
		String jobName = "Query" + random.substring(8, 12) + "-LUW5";

		String dbType = cs.getDBTypeByDBProfile(dbName);
		String sqlid = cs.getSQLIDByDBProfile(dbName);
		
		/**
		 *  Create single job with IA
		 */
		JSONObject result = cs.submitJob(genDescSection(jobName), 
				genOQWTSection(random, queryText, schema, dbType, dbName, sqlid));
		int responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		
		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
		result = cs.runJob(jobId, dbName);
		responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String resultcode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultcode));
		
		
		CheckJobStatus.checkJobStatus(jobId, 25000L);	
		
		JSONObject status = cs.getJobStatus(jobId);
		System.out.println(">>"+status);
		
		String instID = status.getString("INSTID");
		String arId = status.getString("RESULTID");
		
		JSONObject singleJobDetails = cs.getSingleJobDetails(dbName, jobId, jobName, instID, arId);
		String queryNodeID = singleJobDetails.getString("queryNodeID");
		String queryResultID = singleJobDetails.getString("queryResultID");
		JSONObject singleIAImpactJobResult = cs.submitJob(gDescSection(jobName), gOQWTSection(random, queryNodeID, queryResultID, dbName));
		int impactJobResponseCode = (Integer) singleIAImpactJobResult.get("ResponseCode");
		CheckJobStatus.checkResponseCode(singleIAImpactJobResult, impactJobResponseCode);
		
		String impactJobID = singleIAImpactJobResult.getString("jobid");
		Assert.assertTrue(impactJobID != null);
		
		singleIAImpactJobResult = cs.runJob(impactJobID, dbName);
		impactJobResponseCode = (Integer)singleIAImpactJobResult.get("ResponseCode");
		
		CheckJobStatus.checkResponseCode(singleIAImpactJobResult, impactJobResponseCode);
		String impactJobResultcode = result.getString("resultcode");
		Assert.assertTrue("success".equals(impactJobResultcode));
		
		CheckJobStatus.checkJobStatus(jobId, 5000L);	
		
		List<String> jobInstId = new ArrayList<String>();
		JSONObject impactJobStatus = cs.getJobByJobID(impactJobID);
		String impactJobInstID = impactJobStatus.getString("INSTID");
		
		jobInstId.add(impactJobInstID);
		jobInstId.add(instID);
		
		JSONObject delStatus = cs.deleteJobs(jobInstId);
		String delResultCode = delStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
		
		
	}
	
	
	public Properties genDescSection(String jobName) {
		Properties desc = new Properties();
		desc.put("jobname", jobName);
		desc.put("jobtype", "querytunerjobs");
		desc.put("schedenabled", 1);
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		return desc;
	}

	public Properties genOQWTSection(String random, String queryText,
			String schema, String dbType, String dbName,String sqlid) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("queryid", "");
		oqwt_SectionData.put("tuningType", "SQL_BASED");
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("jobName", "Query_" + random + "-Result_" + random);
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("sqlid", sqlid);
		oqwt_SectionData.put("degree", "ANY");
		oqwt_SectionData.put("hint", "");
		oqwt_SectionData.put("refreshAge", "ANY");
		oqwt_SectionData.put("MQT", "ALL");
		oqwt_SectionData.put("systemTime", "NULL");
		oqwt_SectionData.put("businessTime", "NULL");
		oqwt_SectionData.put("getArchive", "Y");
		oqwt_SectionData.put("returnAllStats", "OFF");
		oqwt_SectionData.put("queryName", "Query_" + random);
		oqwt_SectionData.put("resultName", "Result_" + random);
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("reExplainValCheck", true);
		oqwt_SectionData.put("apgValCheck", true);
		oqwt_SectionData.put("formatQueryValCheck", true);
		oqwt_SectionData.put("iaValCheck", true);
		oqwt_SectionData.put("saValCheck", true);
		oqwt_SectionData.put("queryText", queryText);
		oqwt_SectionData.put("hashID", "");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		oqwt_SectionData.put("monitoredDbType", dbType);
		return oqwt_SectionData;
	}
	
	public Properties gDescSection(String jobName) {
		Properties desc = new Properties();
		desc.put("jobname", "WIA-ImpactAnalysis_" + jobName);
		desc.put("jobtype", "indeximpactjob");
		desc.put("schedenabled", 0);
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}
	
	public Properties gOQWTSection(String random,String queryNodeID,String queryResultID,String dbName) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("tuningType", "IMPACT-ANALYSIS");
		oqwt_SectionData.put("workloadName", "Impact-" + random);
		oqwt_SectionData.put("wccJobID", "Impact-" + random);
		oqwt_SectionData.put("queryNodeID", queryNodeID);
		oqwt_SectionData.put("queryResultID", queryResultID);
		oqwt_SectionData.put("profile", dbName);
		oqwt_SectionData.put("schema","admin");
		return oqwt_SectionData;
	}
	
	
}

















