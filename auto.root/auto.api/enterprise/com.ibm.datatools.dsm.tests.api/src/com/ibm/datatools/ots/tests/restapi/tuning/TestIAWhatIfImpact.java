package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;


/**
 * Case Type : ZOS automation case
 * Case number on wiki : 7
 * Case description : Select one single tuning job with IA recommendations->View Results->
 * 					  in IA result page click Test Candidate Indexes->
 * 					  do Add/Remove/virtually drop Existing indexes->
 * 					  click Run->verify IA whatif result->run impact analysis->verify result
 * @author zhumo
 *
 */
public class TestIAWhatIfImpact extends TuningTestBase{
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("TestIAWhatIfImpact.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	@Test(dataProvider = "testdata")
	public void testIAWhatIfImpact(Object key,Object inputPara) throws FileNotFoundException, InterruptedException, IOException{
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		
		String random = String.valueOf(System.currentTimeMillis());
		String queryText = obj.getString("queryText");

		String dbType = cs.getDBTypeByDBProfile(dbName);
		String sqlid = cs.getSQLIDByDBProfile(dbName);
		
		/**
		 *  Create single job with IA
		 */
		JSONObject result = cs.submitJob(genDescSection(random), 
				genOQWTSection(random, queryText, schema, dbType, dbName, sqlid));
		int responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		
		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
		result = cs.runJob(jobId, dbName);
		responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String resultcode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultcode));
		
		JSONObject status;
		status = cs.getJobStatus(jobId);
		CheckJobStatus.checkJobStatus(jobId, 10000L);	
		status = cs.getJobStatus(jobId);
		
		String jobID = status.getString("JOBID");
		String jobName = status.getString("JOBNAME");
		String instID = status.getString("INSTID");
		String arId = status.getString("RESULTID");
		
		result = cs.getSingleJobDetails(dbName, jobID, jobName, instID, arId);
		
		
		String queryResultID = result.getString("queryResultID");
		String WTI_JOB_ID = result.getString("jobid");
		String queryNodeID = result.getString("queryNodeID");
		
		/**
		 *  Test candidate index
		 */
		
		result = cs.getSingleJobCandidateIndex(dbName, arId, jobID, schema);
		
		System.out.println("@@"+result);
		
		JSONArray items = result.getJSONArray("CANDIDATEINDEXES");
		System.out.println(items.size());
		
		if(items.size() == 0){
			String errorMessage = "No candidate indexes of current SQL statement," +
					"please check SQL statement or DB connection";
			Assert.fail(errorMessage);
			return;
		}
		
		String strIndex = null;
		for(int i=0;i<items.size();i++){
			JSONObject objCandidateIndexe = items.getJSONObject(i);
			if(objCandidateIndexe.getString("ACTION").equals("CREATE")){
				String INDEXNAME = objCandidateIndexe.getString("IXNAME");
				String INDEXCREATOR = objCandidateIndexe.getString("IXCREATOR");
				String TABLENAME = objCandidateIndexe.getString("TBNAME");
				String TABLESCHEMA = objCandidateIndexe.getString("TBSCHEMA");
				String KEYCOLUMNS = objCandidateIndexe.getString("KEYCOLUMNS");
				String INCLUDECOLUMNS = objCandidateIndexe.getString("INCLUDE");
				String UNIQUE = objCandidateIndexe.getString("UNIQUE");
				String CLUSTERING = objCandidateIndexe.get("CLUSTERING").toString();
				String DPSI = objCandidateIndexe.get("DPSI").toString();
				String KEYTARGETCOUNT = objCandidateIndexe.get("KEYTARGETCOUNT").toString();
				String ACTION = objCandidateIndexe.get("ACTION").toString();
				String ISRECOMMEND = objCandidateIndexe.get("ISRECOMMENDED").toString();
					
					if(ISRECOMMEND.equals("true"))
						strIndex="{\"INDEXNAME\" : \""+INDEXNAME+"\",\"INDEXCREATOR\" : \""+INDEXCREATOR+"\",\"TABLENAME\" : \""+TABLENAME+"\",\"TABLESCHEMA\" : \""+TABLESCHEMA+"\",\"KEYCOLUMNS\" : \""+KEYCOLUMNS+"\",\"INCLUDECOLUMNS\" : \""+INCLUDECOLUMNS+"\",\"UNIQUE\" : \""+UNIQUE+"\",\"CLUSTERING\" : \""+CLUSTERING+"\",\"DPSI\" : \""+DPSI+"\",\"CLUSTERING\" : \""+CLUSTERING+"\",\"KEYTARGETCOUNT\" : \""+KEYTARGETCOUNT+"\",\"ACTION\" : \""+ACTION+"\",\"EXPCOLNOS\" : \"\",\"ISRECOMMEND\" : \"Y\"},";
					else
						strIndex="{\"INDEXNAME\" : \""+INDEXNAME+"\",\"INDEXCREATOR\" : \""+INDEXCREATOR+"\",\"TABLENAME\" : \""+TABLENAME+"\",\"TABLESCHEMA\" : \""+TABLESCHEMA+"\",\"KEYCOLUMNS\" : \""+KEYCOLUMNS+"\",\"INCLUDECOLUMNS\" : \""+INCLUDECOLUMNS+"\",\"UNIQUE\" : \""+UNIQUE+"\",\"CLUSTERING\" : \""+CLUSTERING+"\",\"DPSI\" : \""+DPSI+"\",\"CLUSTERING\" : \""+CLUSTERING+"\",\"KEYTARGETCOUNT\" : \""+KEYTARGETCOUNT+"\",\"ACTION\" : \""+ACTION+"\",\"EXPCOLNOS\" : \"\",\"ISRECOMMEND\" : \"N\"},";
			}
		}	

			strIndex= " ["+strIndex.substring(0, strIndex.length()-1)+"] ";
		
			
			String disabledIndexesJSON = "";
			String sch = obj.getString("sch"); 
			result = cs.submitJob(gDescSection(random), 
					g(queryNodeID, queryResultID, WTI_JOB_ID, dbName, strIndex, disabledIndexesJSON, sch,dbType,sqlid));
			
			int resCode = (Integer)result.get("ResponseCode");
			CheckJobStatus.checkResponseCode(result, resCode);
			String indexJobId = result.getString("jobid");
			Assert.assertTrue(indexJobId != null);
				
			JSONObject whatIfResult = cs.runJob(indexJobId, dbName);
			int whatIfResCode = (Integer)whatIfResult.get("ResponseCode");
			CheckJobStatus.checkResponseCode(whatIfResult, whatIfResCode);
				
			String whatIfResultcode = whatIfResult.getString("resultcode");
			Assert.assertTrue("success".equals(whatIfResultcode));
			
			CheckJobStatus.checkJobStatus(indexJobId, 5000L);
			
			/**
			 * Zhu Mo
			 * Run impact analysis in what-if result
			 */
			JSONObject whatIfJobDetails = cs.getJobByJobID(indexJobId);
			//Assert.assertTrue(whatIfJobDetails.getString("whatIfRunResultSummary") != null);

			String whatIfQueryResultID = whatIfJobDetails.getString("RESULTID");
			String whatIfJobId =  whatIfJobDetails.getString("JOBID");
			String wccJobID = "iia-z" + "#" + new Date().getTime();
			String whatIfJobName = whatIfJobDetails.getString("JOBNAME");
			result = cs.submitJob(genImpactDescSection(jobName), 
					genImpactOQWTSection(whatIfQueryResultID, whatIfJobId, dbName, wccJobID,sqlid));
			
			responseCode = (Integer)result.get("ResponseCode");
			CheckJobStatus.checkResponseCode(result, responseCode);
			String whatIfImpactJobId = result.getString("jobid");
			Assert.assertTrue(whatIfImpactJobId != null);
			result = cs.runJob(whatIfImpactJobId, dbName);
			
			responseCode = (Integer) result.get("ResponseCode");
			CheckJobStatus.checkResponseCode(result, responseCode);
			resultcode = result.getString("resultcode");
			Assert.assertTrue("success".equals(resultcode));
			
			CheckJobStatus.checkJobStatus(whatIfImpactJobId, 5000L);
			JSONObject whatIfImpactJobDetails = cs.getJobByJobID(whatIfImpactJobId);
			String whatIfImpactJobInstID =  whatIfImpactJobDetails.getString("INSTID");
			JSONObject whatifImpactJobResult = cs.getImpactAnalysisresult(
					whatIfImpactJobDetails.getString("DATABASENAME"),
					whatIfImpactJobDetails.getString("JOBID"),
					whatIfImpactJobDetails.getString("JOBNAME"),
					whatIfImpactJobDetails.getString("INSTID"));
			JSONArray pkgInfoDetails = ((JSONObject)whatifImpactJobResult.getJSONArray("recommendations").get(1)).getJSONArray("children");
			Assert.assertTrue(!pkgInfoDetails.isEmpty());
			
			/*
			 * Delete job when run IA what-if job successfully
			 */
			List<String> jobInstIDList = new ArrayList<String>();
			JSONObject whatIfJobStatus = cs.getJobByJobID(indexJobId);
			String whatIfJobInstID = whatIfJobStatus.getString("INSTID");
			jobInstIDList.add(instID);
			jobInstIDList.add(whatIfJobInstID);
			jobInstIDList.add(whatIfImpactJobInstID);
			JSONObject delJobStatus = cs.deleteJobs(jobInstIDList);
			String delResultCode = delJobStatus.getString("resultcode");
			if("success".equals(delResultCode)){
				System.out.println("Delete job successfully");
			}else{
				Assert.fail("Delete job failure");
			}
			
			
		
	}
	
	public Properties genDescSection(String random) {
		Properties desc = new Properties();
		desc.put("jobname", "Query" + random.substring(8, 12));
		desc.put("jobtype", "querytunerjobs");
		desc.put("schedenabled", 1);
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		return desc;
	}

	public Properties genOQWTSection(String random, String queryText,
			String schema, String dbType, String dbName,String sqlid) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("queryid", "");
		oqwt_SectionData.put("tuningType", "SQL_BASED");
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("jobName", "Query_" + random + "-Result_" + random);
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("sqlid", sqlid);
		oqwt_SectionData.put("degree", "ANY");
		oqwt_SectionData.put("hint", "");
		oqwt_SectionData.put("refreshAge", "ANY");
		oqwt_SectionData.put("MQT", "ALL");
		oqwt_SectionData.put("systemTime", "NULL");
		oqwt_SectionData.put("businessTime", "NULL");
		oqwt_SectionData.put("getArchive", "Y");
		oqwt_SectionData.put("returnAllStats", "OFF");
		oqwt_SectionData.put("queryName", "Query_" + random);
		oqwt_SectionData.put("resultName", "Result_" + random);
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("reExplainValCheck", true);
		oqwt_SectionData.put("apgValCheck", true);
		oqwt_SectionData.put("formatQueryValCheck", true);
		oqwt_SectionData.put("iaValCheck", true);
		oqwt_SectionData.put("saValCheck", true);
		oqwt_SectionData.put("queryText", queryText);
		oqwt_SectionData.put("hashID", "");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		oqwt_SectionData.put("monitoredDbType", dbType);
		return oqwt_SectionData;
	}
	
	public Properties gDescSection(String random) {
		Properties desc = new Properties();
		desc.put("jobname", "Query" + random.substring(8, 12) + "_WhatIfAnalysis");
		desc.put("jobtype", "testcandidatejob");
		desc.put("schedenabled", 0);
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}
								
	public Properties g(String queryNodeID,String queryResultID,
			String WTI_JOB_ID,String dbName,String strIndex,String disabledIndexesJSON,String sch,String dbType,String sqlid) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("tuningType", "WHAT-IF-ANALYSIS");
		oqwt_SectionData.put("dbType", dbType);
		oqwt_SectionData.put("queryNodeID", queryNodeID);
		oqwt_SectionData.put("queryResultID", queryResultID);
		oqwt_SectionData.put("WTI_JOB_ID", WTI_JOB_ID);
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		oqwt_SectionData.put("sqlid", sqlid);
		oqwt_SectionData.put("indexesJSON", strIndex);
		oqwt_SectionData.put("disabledIndexesJSON", disabledIndexesJSON);
		oqwt_SectionData.put("schema", sch);
		return oqwt_SectionData;
	}
	
	public Properties genImpactDescSection(String jobName) {
		String random = String.valueOf(System.currentTimeMillis());
		Properties desc = new Properties();
		desc.put("jobname", jobName + "_ImpactAnalysis");
		desc.put("jobtype", "indeximpactjob");
		desc.put("schedenabled", "0");
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}
								
	public Properties genImpactOQWTSection(String queryResultID,String WTI_JOB_ID,
			String dbName,String wccJobID,String sqlid) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("PKGANALYZING", true);
		oqwt_SectionData.put("cacheALYZING", true);
		oqwt_SectionData.put("ddltype", "AL");
		oqwt_SectionData.put("isWhatIfResult", true);
		oqwt_SectionData.put("monitoreddbprofile", dbName);
		oqwt_SectionData.put("profile", dbName);
		//oqwt_SectionData.put("queryNodeID", null);
		oqwt_SectionData.put("queryResultID", queryResultID);
		oqwt_SectionData.put("sqlid", sqlid);
		oqwt_SectionData.put("schema", sqlid);
		oqwt_SectionData.put("tuningType", "IMPACT-ANALYSIS");
		oqwt_SectionData.put("wccJobID", wccJobID);
		oqwt_SectionData.put("workloadName", wccJobID);
		oqwt_SectionData.put("wtiJobId", WTI_JOB_ID);

		return oqwt_SectionData;
	}
	
}










































