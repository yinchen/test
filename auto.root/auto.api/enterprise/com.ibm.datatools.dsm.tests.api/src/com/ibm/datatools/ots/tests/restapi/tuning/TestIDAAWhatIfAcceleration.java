package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;

/**
 * 
 * Case Type : ZOS automation case
 * Case number on wiki : 21
 * Case description : Select one workload tuning job with IDAA recommendations-> View Results->
 * 					  in IDAA result click Test acceleration what-if-> click Run->
 * 					  verify IDAA whatif result
 * @author yinchen
 *
 */
public class TestIDAAWhatIfAcceleration extends TuningTestBase{

	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("TestIDAAWhatIfAcceleration.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	@Test(dataProvider = "testdata")
	public void testCreateWorkloadJob(Object key,Object inputPara) throws InterruptedException, FileNotFoundException, IOException {
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		String wtaaAccelName = obj.getString("wtaaAccelName");
		String random = String.valueOf(System.currentTimeMillis());
		String jobName = "Workload" + random.substring(9, random.length()) +"-ZOS21";
		
		String dbType = cs.getDBTypeByDBProfile(dbName);
		String sqlid = cs.getSQLIDByDBProfile(dbName);
		String wccJobID = "workload#" + random;
		//get the sql path from DSM server
		String sqlFileName = cs.uploadFile(obj.getString("sqlFileName"));
		
		/**
		 *   Create workload job with IDAA recommendations
		 */
		
		JSONObject result = cs.submitJob(genDescSection(jobName,dbName), 
				genOQWTSection(random,wtaaAccelName,sqlid,wccJobID,sqlFileName,schema,dbType,dbName));
		
		int responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
			
		result = cs.runJob(jobId, dbName);
		responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String resultcode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultcode));
				 
		CheckJobStatus.checkJobStatus(jobId, 15000L);
		
		/**
		 * 		Test acceleration what-if
		 */
		result = cs.submitJob(gDescSection(jobName,random), 
				genOQWTSection(wccJobID, dbName, schema, sqlid, dbType, wtaaAccelName));
		responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		
		String accJobId = result.getString("jobid");
		Assert.assertTrue(accJobId != null);
		
		result = cs.runJob(accJobId, dbName);
		responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String accResultCode = result.getString("resultcode");
		Assert.assertTrue("success".equals(accResultCode));
		
		CheckJobStatus.checkJobStatus(accJobId, 20000L);
		
		/*
		 * Delete job when run IDAA what-if acceleration job successfully
		 */
		List<String> jobInstID = new ArrayList<String>();
		JSONObject workloadJobStatus = cs.getJobByJobID(jobId);
		String workloadJobInstID = workloadJobStatus.getString("INSTID");
		JSONObject accJobStatus = cs.getJobByJobID(accJobId);
		String accJobInstID = accJobStatus.getString("INSTID");
		
		jobInstID.add(workloadJobInstID);
		jobInstID.add(accJobInstID);
		JSONObject delJobStatus = cs.deleteJobs(jobInstID);
		String delResultCode = delJobStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
		
	}
	
	public Properties genDescSection(String jobName,String dbName) {
		Properties desc = new Properties();
		desc.put("jobname", jobName);
		desc.put("jobType", "querytunerjobs");
		desc.put("schedenabled", 0);
		desc.put("mondbconprofile", dbName);
		desc.put("jobcreator", "admin");
		desc.put("jobid", "0");
		desc.put("jobdesc", "");
		desc.put("dbreqforjob", "1");
		return desc;
	}
	
	public Properties genOQWTSection(String random,String wtaaAccelName,
			String sqlid,String wccJobID,String sqlFileName,String schema,String dbType,String dbName) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("tuningType", "WORKLOAD");
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("workloadName", "Workload_" + random);
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("reExplainWorkloadValCheck", true);
		oqwt_SectionData.put("wsaValCheck", true);
		oqwt_SectionData.put("wiaValCheck", true);
		oqwt_SectionData.put("wtaaValCheck", true);
		oqwt_SectionData.put("wtaaModelSelect", "virtual");
		oqwt_SectionData.put("wtaaAccelName", wtaaAccelName);
		oqwt_SectionData.put("curPathValue", "");
		oqwt_SectionData.put("curDegreeValue", "ANY");
		oqwt_SectionData.put("curRefAgeValue", "ANY");
		oqwt_SectionData.put("curMQTValue", "ALL");
		oqwt_SectionData.put("curBusiTimeValue", "NULL");
		oqwt_SectionData.put("curSysTimeValue", "NULL");
		oqwt_SectionData.put("curGetArchiveValue", "Y");
		oqwt_SectionData.put("SQLID", sqlid);
		oqwt_SectionData.put("wccJobID", wccJobID);
		oqwt_SectionData.put("wccJobStatus", "Running");
		oqwt_SectionData.put("sqlFileName", sqlFileName);
		oqwt_SectionData.put("stmtDelimiter", ";");
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("stmtDelim", ";");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbType", dbType);
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		return oqwt_SectionData;
	}

	public Properties gDescSection(String jobName,String random) {
		Properties desc = new Properties();
		desc.put("jobname", jobName + "_TestCandidate_Acceleration");
		desc.put("jobType", "wtctaajob");
		desc.put("schedenabled", 0);
		desc.put("jobcreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}
	
	public Properties genOQWTSection(String workloadName,String dbName,String schema,String sqlid,String dbType,String wtaaAccelName) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("tuningType", "ACCELERATION WHAT-IF");
		oqwt_SectionData.put("workloadName", workloadName);
		oqwt_SectionData.put("tablesJSON", "{\"list\":[]}");
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("sqlid", sqlid);
		oqwt_SectionData.put("monitoredDbType", dbType);
		oqwt_SectionData.put("monitoredDbName", dbName);
		oqwt_SectionData.put("wtaaAccelName", wtaaAccelName);
		oqwt_SectionData.put("jobCreator", "admin");
		return oqwt_SectionData;
	}
	
	
}































