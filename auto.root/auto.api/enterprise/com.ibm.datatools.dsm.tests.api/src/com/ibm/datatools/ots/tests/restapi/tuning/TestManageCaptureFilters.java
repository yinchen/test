package com.ibm.datatools.ots.tests.restapi.tuning;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.RestAPIBaseTest;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class TestManageCaptureFilters {

	TuningCommandService cs;
	
	@BeforeTest
	public void beforeTest() throws Exception {
		cs = new TuningCommandService();
		RestAPIBaseTest.loginDSM();
		
		//TODO ZGB
		//check repository database is setup
	}
	
	@Test
	public void testGetAllFilters(){
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("OWNER", "admin");
		
		JSONObject result = cs.getAllFilters(parameters);
		Assert.assertEquals(result.getInt("ResponseCode"), 200);
		JSONArray filters = result.getJSONArray("ALL_FILTERS");
		
		int size = filters.size();
		boolean founded = false;
		for(int i=0;i<size;i++){
			JSONObject filter = (JSONObject) filters.get(i);
			if(filter.getString("NAME").equals("GETPAGES_DESC") && filter.getString("SOURCE").equals("5")){
				founded = true;
				
				Assert.assertEquals(filter.getString("SHARED"), "Y");
				Assert.assertEquals(filter.getString("OWNER"), "SYSTEM");
				JSONArray conds = filter.getJSONArray("FILTER_CONDS");
				
				Assert.assertEquals(conds.size(), 1);
				JSONObject cond = conds.getJSONObject(0);
				Assert.assertEquals(cond.getString("OP"), "DESC");
				Assert.assertEquals(cond.getString("LHS"), "STAT_GPAGB");
				Assert.assertEquals(cond.getString("RHS"), "");
				Assert.assertEquals(cond.getString("SEQ"), "1");
			}
		}
		Assert.assertTrue(founded);
		
		System.out.println(result);
	}
	
	@Test
	public void testAddDeleteFilter(){
		Map<String,String> parameters4AddFilter = new HashMap<String,String>();

		String filterName2Add = "cacheAuto4Test1";
		String filterSource2Add = "1";
		String filterShared2Add = "N";
		String filterOwner2Add = "only_for_test";
		String filterMaxStmts2Add = "99";
		parameters4AddFilter.put("NAME", filterName2Add);
		parameters4AddFilter.put("SOURCE", filterSource2Add);
		parameters4AddFilter.put("SHARED", filterShared2Add);
		parameters4AddFilter.put("OWNER", filterOwner2Add);
		parameters4AddFilter.put("MAX_STMTS", filterMaxStmts2Add);
		parameters4AddFilter.put("FILTER_CONDS",
				"[{\"OP\":\"=\",\"LHS\":\"COLUMN5\",\"RHS\":\"VAL1\", \"SEQ\":\"1\"},{\"OP\":\"=\",\"LHS\":\"COLUMN6\",\"RHS\":\"VAL1\", \"SEQ\":\"2\"}]");
		
		JSONObject result4AddFilter = cs.addFilter(parameters4AddFilter);
		String id4NewAddedFilter = "";
		System.out.println(result4AddFilter.toString());
		if ((Boolean) result4AddFilter.get("isSucc")) {
			System.out.println("new added filter:" + result4AddFilter);
			Assert.assertTrue(result4AddFilter.containsKey("ID"));
			id4NewAddedFilter = result4AddFilter.getString("ID");
			Assert.assertTrue(result4AddFilter.containsKey("MODIFIED_TS"));
		}else{
			Assert.fail();
		}
		
		// verify can get the filter
		Boolean foundNewAddedFilter = false;
		HashMap<String,String> params4GetFilters = new HashMap<String,String>();
		params4GetFilters.put("SOURCE", filterSource2Add);
		params4GetFilters.put("OWNER", filterOwner2Add);
		JSONObject result4getAllFilters = cs.getAllFilters(params4GetFilters);
		
		JSONArray filters = result4getAllFilters.getJSONArray("ALL_FILTERS");
		int size = filters.size();
		boolean founded = false;
		for(int i=0;i<size;i++){
			JSONObject filter = (JSONObject) filters.get(i);

			if (filter.getString("NAME").equals(filterName2Add) && filter.getString("SOURCE").equals(filterSource2Add)
					&& filter.getString("SHARED").equals(filterShared2Add)
					&& filter.getString("OWNER").equals(filterOwner2Add)
					&& filter.getString("MAX_STMTS").equals(filterMaxStmts2Add)) {
				foundNewAddedFilter = true;
				break;
			}
		}
		Assert.assertTrue(foundNewAddedFilter);
		
		//delete the new added test filter
		HashMap<String,String> params4DeleteFilter = new HashMap<String,String>();
		params4DeleteFilter.put("ID", id4NewAddedFilter);
		JSONObject result4deleteFilter = cs.deleteFilter(params4DeleteFilter);
		System.out.println(result4deleteFilter.toString());
		Assert.assertEquals(result4deleteFilter.getInt("ResponseCode"), 200);
	}
	
	@Test
	public void testAddDeleteFilterRHS_null(){
		Map<String,String> parameters4AddFilter = new HashMap<String,String>();

		String filterName2Add = "cacheAuto4Test1";
		String filterSource2Add = "1";
		String filterShared2Add = "N";
		String filterOwner2Add = "only_for_test";
		String filterMaxStmts2Add = "99";
		parameters4AddFilter.put("NAME", filterName2Add);
		parameters4AddFilter.put("SOURCE", filterSource2Add);
		parameters4AddFilter.put("SHARED", filterShared2Add);
		parameters4AddFilter.put("OWNER", filterOwner2Add);
		parameters4AddFilter.put("MAX_STMTS", filterMaxStmts2Add);
		parameters4AddFilter.put("FILTER_CONDS",
				"[{\"OP\":\"=\",\"LHS\":\"COLUMN5\",\"RHS\":\"VAL1\", \"SEQ\":\"1\"},{\"OP\":\"=\",\"LHS\":\"COLUMN6\",\"RHS\":\"VAL1\", \"SEQ\":\"2\",\"RHS\":null}]");
		
		JSONObject result4AddFilter = cs.addFilter(parameters4AddFilter);
		String id4NewAddedFilter = "";
		System.out.println(result4AddFilter.toString());
		if ((Boolean) result4AddFilter.get("isSucc")) {
			System.out.println("new added filter:" + result4AddFilter);
			Assert.assertTrue(result4AddFilter.containsKey("ID"));
			id4NewAddedFilter = result4AddFilter.getString("ID");
			Assert.assertTrue(result4AddFilter.containsKey("MODIFIED_TS"));
		}else{
			Assert.fail();
		}
		
		// verify can get the filter
		Boolean foundNewAddedFilter = false;
		HashMap<String,String> params4GetFilters = new HashMap<String,String>();
		params4GetFilters.put("SOURCE", filterSource2Add);
		params4GetFilters.put("OWNER", filterOwner2Add);
		JSONObject result4getAllFilters = cs.getAllFilters(params4GetFilters);
		
		JSONArray filters = result4getAllFilters.getJSONArray("ALL_FILTERS");
		int size = filters.size();
		boolean founded = false;
		for(int i=0;i<size;i++){
			JSONObject filter = (JSONObject) filters.get(i);

			if (filter.getString("NAME").equals(filterName2Add) && filter.getString("SOURCE").equals(filterSource2Add)
					&& filter.getString("SHARED").equals(filterShared2Add)
					&& filter.getString("OWNER").equals(filterOwner2Add)
					&& filter.getString("MAX_STMTS").equals(filterMaxStmts2Add)) {
				foundNewAddedFilter = true;
				break;
			}
		}
		Assert.assertTrue(foundNewAddedFilter);
		
		//delete the new added test filter
		HashMap<String,String> params4DeleteFilter = new HashMap<String,String>();
		params4DeleteFilter.put("ID", id4NewAddedFilter);
		JSONObject result4deleteFilter = cs.deleteFilter(params4DeleteFilter);
		System.out.println(result4deleteFilter.toString());
		Assert.assertEquals(result4deleteFilter.getInt("ResponseCode"), 200);
	}
	
	@Test
	public void testAddEditDeleteFilterRHS_null(){
		Map<String,String> parameters4AddFilter = new HashMap<String,String>();

		String filterName2Add = "sortFilter1";
		String filterSource2Add = "1";
		String filterShared2Add = "N";
		String filterOwner2Add = "only_for_test";
		String filterMaxStmts2Add = "99";
		parameters4AddFilter.put("NAME", filterName2Add);
		parameters4AddFilter.put("SOURCE", filterSource2Add);
		parameters4AddFilter.put("SHARED", filterShared2Add);
		parameters4AddFilter.put("OWNER", filterOwner2Add);
		parameters4AddFilter.put("MAX_STMTS", filterMaxStmts2Add);
		parameters4AddFilter.put("FILTER_CONDS",
				"[{\"OP\":\"DESC\",\"LHS\":\"COLUMN5\", \"SEQ\":\"1\"},{\"OP\":\"ASC\",\"LHS\":\"COLUMN6\", \"SEQ\":\"2\"}]");
		
		JSONObject result4AddFilter = cs.addFilter(parameters4AddFilter);
		String id4NewAddedFilter = "";
		System.out.println(result4AddFilter.toString());
		if ((Boolean) result4AddFilter.get("isSucc")) {
			System.out.println("new added filter:" + result4AddFilter);
			Assert.assertTrue(result4AddFilter.containsKey("ID"));
			id4NewAddedFilter = result4AddFilter.getString("ID");
			Assert.assertTrue(result4AddFilter.containsKey("MODIFIED_TS"));
		}else{
			Assert.fail();
		}
		
		// verify can get the filter
		Boolean foundNewAddedFilter = false;
		HashMap<String,String> params4GetFilters = new HashMap<String,String>();
		params4GetFilters.put("SOURCE", filterSource2Add);
		params4GetFilters.put("OWNER", filterOwner2Add);
		JSONObject result4getAllFilters = cs.getAllFilters(params4GetFilters);
		
		JSONArray filters = result4getAllFilters.getJSONArray("ALL_FILTERS");
		int size = filters.size();
		boolean founded = false;
		for(int i=0;i<size;i++){
			JSONObject filter = (JSONObject) filters.get(i);

			if (filter.getString("NAME").equals(filterName2Add) && filter.getString("SOURCE").equals(filterSource2Add)
					&& filter.getString("SHARED").equals(filterShared2Add)
					&& filter.getString("OWNER").equals(filterOwner2Add)
					&& filter.getString("MAX_STMTS").equals(filterMaxStmts2Add)) {
				foundNewAddedFilter = true;
				
				HashMap<String,String> params4Edit = new HashMap<String,String>();
				params4Edit.put("ID", filter.getString("ID"));
				params4Edit.put("NAME", filter.getString("NAME"));
				params4Edit.put("SHARED", "Y");
				params4Edit.put("OWNER", "only_for_test");
				params4Edit.put("MAX_STMTS", "100");
				//before [{\"OP\":\"DESC\",\"LHS\":\"COLUMN5\", \"SEQ\":\"1\"},{\"OP\":\"ASC\",\"LHS\":\"COLUMN6\", \"SEQ\":\"2\"}]
				params4Edit.put("FILTER_CONDS",
						"[{\"OP\":\"DESC\",\"LHS\":\"COLUMN5\", \"SEQ\":\"1\"},{\"OP\":\"ASC\",\"LHS\":\"COLUMN7\", \"SEQ\":\"2\",\"RHS\":null}]");

				JSONObject newAddedFilter = cs.editFilter(params4Edit);
				if ((Boolean) newAddedFilter.get("isSucc")) {
					System.out.println("filter after edit:" + newAddedFilter);
					Assert.assertTrue(newAddedFilter.containsKey("NAME"));
					Assert.assertTrue(newAddedFilter.containsKey("OWNER"));
					Assert.assertTrue(newAddedFilter.containsKey("SHARED"));
					Assert.assertTrue(newAddedFilter.containsKey("MAX_STMTS"));
					Assert.assertTrue(newAddedFilter.containsKey("ADDITION1"));
					Assert.assertTrue(newAddedFilter.containsKey("MODIFIED_TS"));
				}else{
					Assert.fail();
				}
				break;
			}
		}
		Assert.assertTrue(foundNewAddedFilter);
		
		//delete the new added test filter
		HashMap<String,String> params4DeleteFilter = new HashMap<String,String>();
		params4DeleteFilter.put("ID", id4NewAddedFilter);
		JSONObject result4deleteFilter = cs.deleteFilter(params4DeleteFilter);
		System.out.println(result4deleteFilter.toString());
		Assert.assertEquals(result4deleteFilter.getInt("ResponseCode"), 200);
	}
}
