package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;


/**
 * Case Type : ZOS automation case
 * Case number on wiki : 27
 * Case description : Settings->Manage Connections->in db connection list, select one z/OS db connection->
 * 				 	   click Query Tuning->Migrate from Optim Query Workload Tuner 4.1.x->
 * 					   click Choose a Workload File button->select zip file->verify migrate result.
 * @author yinchen
 *
 */
public class TestMigrateFromOQWT extends TuningTestBase{
	
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("TestMigrateFromOQWT.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	@Test(dataProvider = "testdata")
	public void testCreateWorkloadJob(Object key,Object inputPara) throws InterruptedException, FileNotFoundException, IOException{
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTime = format.format(date);
		String dbType = cs.getDBTypeByDBProfile(dbName);
		
		String filePath = cs.uploadFile(obj.getString("filePath"));
		System.out.println("Migrate file is : "+filePath);
		
		String fileName = filePath.substring(filePath.lastIndexOf("/"));
		String copyFileName = fileName.replace("/", "copy-");
		String copyFilePath = filePath.substring(0, filePath.lastIndexOf("/")+1);
		String copyPath = copyFilePath + copyFileName;
		copyFile(filePath, copyPath);
		
		JSONObject result = cs.migrateFromQT(filePath, dbName, currentTime, dbType);
		System.out.println("Migrate result : "+result);
		
		String migrateResult = result.getString("canMigrate");
		if(!"true".equals(migrateResult)){
			String errorMessage = "Run migrate from OQWT job failed,it could be zip file is not complete."
					+ "Please check zip file under this path : " + copyPath;
			Assert.fail(errorMessage);
		}else{
			System.out.println("Run migrate from OQWT job successful");
			
			String migrateJobName = result.getString("migratedWL");
			JSONObject migrateJob = cs.getJobByJobName(migrateJobName);
			String jobID = migrateJob.getString("JOBID");
			CheckJobStatus.checkJobStatus(jobID, 5000L);
			
			JSONObject delStatus = cs.deleteJob(migrateJob.getString("INSTID"));
			String delResultCode = delStatus.getString("resultcode");
			if("success".equals(delResultCode)){
				System.out.println("Delete job successfully");
			}else{
				Assert.fail("Delete job failure");
			}
			
		}
		
	}
	
	
	public static void copyFile(String oldPath, String newPath) { 
	       try { 
	           int byteread = 0; 
	           File oldfile = new File(oldPath); 
	           if (oldfile.exists()) {                 
	               InputStream inStream = new FileInputStream(oldPath);      
	               FileOutputStream fs = new FileOutputStream(newPath); 
	               byte[] buffer = new byte[1444]; 
	               while ( (byteread = inStream.read(buffer)) != -1) { 
	                   fs.write(buffer, 0, byteread); 
	               } 
	               inStream.close(); 
	           } 
	       }  catch (Exception e) { 
	           System.out.println("copy file error"); 
	           e.printStackTrace(); 
	       } 
	   } 
}
















