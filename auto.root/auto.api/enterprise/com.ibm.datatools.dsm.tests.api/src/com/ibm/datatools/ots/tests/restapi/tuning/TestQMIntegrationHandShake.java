package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;


import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckMonitorDB;
import com.ibm.datatools.ots.tests.restapi.common.RestAPIBaseTest;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;

/**
 * Case Type : ZOS automation case
 * Case number on wiki : 26
 * @author yinchen
 *
 */
public class TestQMIntegrationHandShake {

	TuningCommandService cs;
	Properties p;
	
	String dbName = null;
	
	@Parameters("dbProfile")
	@BeforeTest
	public void beforeTest(String monitorDBName) throws FileNotFoundException, IOException {
		cs = new TuningCommandService();
		RestAPIBaseTest.loginDSM();
		
		dbName = monitorDBName;
		Assert.assertEquals(CheckMonitorDB.checkMonitorDBStatus(dbName), "success");
	}
	
	
	@Test
	public void testHankShake() throws InterruptedException {
		String result = cs.handshake();
		String checkString = result.substring(result.lastIndexOf("RC0")+5, result.lastIndexOf("RC0")+15);
		Assert.assertTrue("Successful".equals(checkString));
	}
	
}































