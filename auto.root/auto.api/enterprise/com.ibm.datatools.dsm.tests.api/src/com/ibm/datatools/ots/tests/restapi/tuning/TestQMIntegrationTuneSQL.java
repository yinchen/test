package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;

public class TestQMIntegrationTuneSQL extends TuningTestBase{

	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("TestQMIntegrationTuneSQL.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	@Test(dataProvider = "testdata")
	public void testCreateWorkloadJob(Object key,Object inputPara) throws InterruptedException, FileNotFoundException, IOException {
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		
		String dbType = cs.getDBTypeByDBProfile(dbName);
		JSONObject monitorDBObj = cs.getMonitorDBDetails(dbName);
		String response = monitorDBObj.getString("response");
		JSONObject responseObj = JSONObject.fromObject(response);
		String url = responseObj.getString("URL");
		String user = responseObj.getString("user");
		String password = responseObj.getString("password");
		String sqlStatement = obj.getString("sqlText");
		
		JSONObject result = cs.tuneSQL(dbType,dbName,url,user,password,sqlStatement,schema);
		System.out.println("Run QM Integration result : " + result);
		String message = result.getString("message");
		if(!message.equals("success")){
			Assert.fail("Run QM Integration TuneSQL job error , please refer this message >> " + result.getString("message"));
		}
		
		Thread.sleep(5000L);
		
		List<JSONObject> jobList = cs.getQMTuneSQLJobs("QueryMonitor");
		
		if(jobList==null || jobList.isEmpty()){
			System.out.println("No Single QM job exist in job list");
			return;
		}
		
		List<String> jobInstIDList = new ArrayList<>();
		
		for(JSONObject jobs : jobList){
			String jobInstID = jobs.getString("INSTID");
			System.out.println(jobInstID);
			jobInstIDList.add(jobInstID);
		}
		
		JSONObject delStatus = cs.deleteJobs(jobInstIDList);
		String delResultCode = delStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
		
	}
	
}































