package com.ibm.datatools.ots.tests.restapi.tuning;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;

public class TestQMIntegrationTuneWorkload extends TuningTestBase{

	@Test
	public void testCreateWorkloadJob() throws InterruptedException {
		
		String dbType = cs.getDBTypeByDBProfile(dbName);
		JSONObject monitorDBObj = cs.getMonitorDBDetails(dbName);
		
		JSONObject responseObj = JSONObject.fromObject(monitorDBObj.getString("response"));
		String url = responseObj.getString("URL");
		String user = responseObj.getString("user");
		String password = responseObj.getString("password");
		JSONObject result = cs.tuneworkload(dbType,url,dbName,user,password,schema);
		
		System.out.println(result);
		Thread.sleep(3000L);
		
		List<JSONObject> jobList = cs.getQMWorkloadJobs("WL_");
		if(jobList==null || jobList.isEmpty()){
			System.out.println("No Workload QM job exist in job list");
			return;
		}
		
		List<String> jobInstIDList = new ArrayList<>();
		
		for(JSONObject jobs : jobList){
			String jobInstID = jobs.getString("INSTID");
			System.out.println(jobInstID);
			jobInstIDList.add(jobInstID);
		}
		
		JSONObject delStatus = cs.deleteJobs(jobInstIDList);
		String delResultCode = delStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
		
	}
	
}





























