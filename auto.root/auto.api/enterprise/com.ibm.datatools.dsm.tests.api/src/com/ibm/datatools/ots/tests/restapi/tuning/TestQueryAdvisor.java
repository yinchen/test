package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;

public class TestQueryAdvisor  extends TuningTestBase{
		@DataProvider(name = "testdata")
		public Object[][] loadJSON() throws FileNotFoundException, IOException {
			// return multiple JSON post properties
			cs = new TuningCommandService("QueryAdvisor.properties");
			p = cs.getJSONProperties();
			return FileTools.readProperties(p);

		}

		@Test(dataProvider = "testdata")
		public void TestQueryAdvisorLUW(Object key, Object inputPara) throws InterruptedException, FileNotFoundException, IOException {
			logger.info("Test data key:" + key);
			JSONObject obj = JSONObject.fromObject(inputPara);
			String sqltext = obj.getString("sqltext");
			String jobId = null;
			
			String dbType = cs.getDBTypeByDBProfile(dbName);
			String sqlid = cs.getSQLIDByDBProfile(dbName);
			
			String random = String.valueOf(System.currentTimeMillis());
			JSONObject result = cs.submitJob(genDescSection(random), genOQWTSection(random, sqltext, schema, dbType, dbName,sqlid));
			
			int responseCode = (Integer) result.get("ResponseCode");
			CheckJobStatus.checkResponseCode(result, responseCode);
			jobId = result.getString("jobid");
			Assert.assertTrue(jobId != null);
				
			result = cs.runJob(jobId, dbName);
			responseCode = (Integer) result.get("ResponseCode");
			CheckJobStatus.checkResponseCode(result, responseCode);
			String resultcode = result.getString("resultcode");
			Assert.assertTrue("success".equals(resultcode));
					 
			CheckJobStatus.checkJobStatus(jobId, 3000L);
			/**
			 *Ma Wei
			 *Verify IA
			 */
			
			
			JSONObject resultDetails = cs.getJobByJobID(jobId);
			JSONObject jso = cs.getSingleJobDetails(dbName,  result.getString("jobid"), "Query_InputSQL" + random.substring(8, 12) + "-ByRestAPI",resultDetails.getString("INSTID"), resultDetails.getString("RESULTID"));
			
			try {
				String recommendations =  jso.get("tuningResult").toString();
				System.out.println("Here is the RECOMMENDATIONS url: " + recommendations);
				Assert.assertTrue( ! "".equals(recommendations), "Verify the recommendations can be showed correctly");
			} catch (Exception e) {
				Assert.fail("*******Verify recommendations error*******");
			}
			
			/*
			 * Delete job when check verification point successfully
			 */
			JSONObject delStatus = cs.deleteJob(resultDetails.getString("INSTID"));
			String delResultCode = delStatus.getString("resultcode");
			if("success".equals(delResultCode)){
				System.out.println("Delete job successfully");
			}else{
				Assert.fail("Delete job failure");
			}
			
		}

		public Properties genDescSection(String random) {
			Properties desc = new Properties();
			desc.put("jobname", "QueryAdvisorLUW_" + random.substring(8, 12) + "-ByRestAPI");
			desc.put("jobtype", "querytunerjobs");
			desc.put("schedenabled", 0);
			desc.put("monDbConProfile", dbName);
			desc.put("jobCreator", "admin");
			desc.put("jobid", "0");
			desc.put("dbreqforjob", "1");
			desc.put("jobdesc", "");
			return desc;
		}

		public Properties genOQWTSection(String random, String sqltext,
				String schema, String dbType, String dbName,String sqlid) {
			Properties oqwt_SectionData = new Properties();
			oqwt_SectionData.put("ISNEWTUNE", true);
			oqwt_SectionData.put("apaValCheck", false);
			oqwt_SectionData.put("apgValCheck", false);
			oqwt_SectionData.put("businessTime", "NULL");
			oqwt_SectionData.put("dbconfigured", "1");
			oqwt_SectionData.put("degree", "1");
			oqwt_SectionData.put("desc", "");
			oqwt_SectionData.put("formatQueryValCheck", false);
			oqwt_SectionData.put("getArchive", "N");
			oqwt_SectionData.put("hashID", "");
			oqwt_SectionData.put("iaValCheck", false);
			oqwt_SectionData.put("isNewF", true);
			oqwt_SectionData.put("jobName", "Query_" + random + "-Result_" + random);
			oqwt_SectionData.put("licenseLabel","The full set of tuning features is available.");			
			oqwt_SectionData.put("monitoredDbProfile", dbName);
			oqwt_SectionData.put("monitoredDbType", dbType);
			oqwt_SectionData.put("oqwtLicenseType", "OQWT");
			oqwt_SectionData.put("qaValCheck", true);
			oqwt_SectionData.put("queryName", "Query_" + random);
			oqwt_SectionData.put("queryText", sqltext);
			oqwt_SectionData.put("queryid", "");
			oqwt_SectionData.put("reExplainValCheck", true);
			oqwt_SectionData.put("resultName", "Result_" + random);
			oqwt_SectionData.put("retune", "false");
			oqwt_SectionData.put("returnAllStats", "OFF");
			oqwt_SectionData.put("rid", "");
			oqwt_SectionData.put("saValCheck", false);
			oqwt_SectionData.put("schema", schema);
			oqwt_SectionData.put("selectedJobResultID", "");
			oqwt_SectionData.put("sqlid", sqlid);
			oqwt_SectionData.put("tuningCtx", "");
			oqwt_SectionData.put("tuningType", "SQL_BASED");
//			oqwt_SectionData.put("desc", "");
//			oqwt_SectionData.put("apgValCheck", false);
//			oqwt_SectionData.put("formatQueryValCheck", false);
//			oqwt_SectionData.put("executionId", "");
//			oqwt_SectionData.put("stmtCacheId", "");
//			oqwt_SectionData.put("monitoredDbName", dbName);
			return oqwt_SectionData;
		}
}
