package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.RestAPIBaseTest;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;

public class TestRetrieveWAPCInfo {
	private final Logger logger = LogManager.getLogger(getClass());
	TuningCommandService cs;
	Properties p;

	@BeforeTest
	public void beforeTest() throws Exception {
		cs = new TuningCommandService("retrieveWAPCInfo.properties");
		RestAPIBaseTest.loginDSM();
	}

	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		// return multiple JSON post properties
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);

	}

	@Test(dataProvider = "testdata")
	public void testRetrieveWAPCInfo(Object key, Object inputPara) {
		logger.info("Test data key:" + key);
		JSONObject result = cs.callTuningCommandServiceByJson(String
				.valueOf(inputPara));
		int responseCode = (Integer) result.get("ResponseCode");
		if (responseCode != 200)

			Assert.fail("FAILURE=> Response code =" + responseCode
					+ " for URL: " + result.get("URL") + ", POSTDATA:"
					+ result.get("PostData"));
		else {
			JSONObject responseData = (JSONObject) result.get("ResponseData");
			String report = (String) responseData.get("report");
			// Validation
			Assert.assertTrue(report.endsWith("html"));
		}
	}
}
