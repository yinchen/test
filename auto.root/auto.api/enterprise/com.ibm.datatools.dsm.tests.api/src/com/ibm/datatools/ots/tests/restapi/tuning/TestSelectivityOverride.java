package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.ots.tests.restapi.base.CheckMonitorDB;
import com.ibm.datatools.ots.tests.restapi.base.DBUtils;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.test.utils.JSONUtils;
import com.ibm.datatools.ots.tests.restapi.common.RestAPIBaseTest;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class TestSelectivityOverride {
	TuningCommandService cs;
	Properties p;
	
	String dbName = null;
	String schema = null;
	TuningService tuningService = null;
	JSONObject resultRetrieveEO = null;
	String instID_EOJob = null;
	@Parameters({"dbProfile","schema"})
	@BeforeTest
	public void beforeTest(String monitorDBName,String monitorDBSchema) throws FileNotFoundException, IOException, Exception {
		tuningService = new TuningService();
		
		cs = new TuningCommandService("TestHostVariablesGridProvider.properties");
		RestAPIBaseTest.loginDSM();
		schema = monitorDBSchema;
		dbName = monitorDBName;
		Assert.assertEquals(CheckMonitorDB.checkMonitorDBStatus(dbName), "success");
		
		runSQL();
	}
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() {
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	@AfterClass
	public void afterTest() {
		deleteWorkload("LOCDB21:1");
		deleteWorkload(instID_EOJob);
	}
	
	@Test(dataProvider = "testdata")
	public void testHostVariablesGridProvider(Object key,Object inputPara) throws IOException{
		JSONObject obj = JSONObject.fromObject(inputPara);
		String instID = obj.getString("instID");
		
		JSONObject result = cs.getHostVariablesGridProvider(dbName, instID);
		
		// Verify the result code
		int responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);

		// Verify the result response
		String expectedResponse = tuningService.getJSONData("getHostVariablesGridProviderExpected");
		String resultResponse =  tuningService.getResponseResult(result);

		verifyResult(resultResponse,expectedResponse);
	}
	
	@Test(dataProvider = "testdata")
	public void testHostVariableSetAndCount(Object key,Object inputPara) throws IOException{
		JSONObject obj = JSONObject.fromObject(inputPara);
		String instID = obj.getString("instID");
		
		JSONObject result = cs.getHostVariableSetAndCount(dbName, instID);
		
		// Verify the result code
		int responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);

		// Verify the result response
		String expectedResponse = tuningService.getJSONData("getHostVariableSetAndCountExpected");
		String resultResponse =  tuningService.getResponseResult(result);

		verifyResult(resultResponse,expectedResponse);
	}
	
	@Test(dataProvider = "testdata")
	public void testRetrieveEO(Object key,Object inputPara) throws InterruptedException, IOException{

		String saveObject = tuningService.getJSONData("saveObject");
		JSONObject resultSubmit = cs.submitJob(saveObject);
		
		int responseCodeSubmit = (Integer)resultSubmit.get("ResponseCode");
		CheckJobStatus.checkResponseCode(resultSubmit, responseCodeSubmit);
		
		String jobId = resultSubmit.getString("jobid");
		Assert.assertTrue(jobId != null);
		
		JSONObject resultRun = cs.runJob(jobId, dbName);
		int responseCodeRun = (Integer)resultRun.get("ResponseCode");
		CheckJobStatus.checkResponseCode(resultRun, responseCodeRun);
		
		CheckJobStatus.checkJobStatus(jobId, 10000L);

	    JSONObject resultDetails = cs.getJobByJobID(jobId);
	    String jobID = resultDetails.getString("JOBID");
	    String jobName = resultDetails.getString("JOBNAME");
	    instID_EOJob = resultDetails.getString("INSTID");
	    String arID = resultDetails.getString("RESULTID");

		resultRetrieveEO = cs.getRetrieveEO(dbName, jobID, jobName,instID_EOJob,arID);
		
		// Verify the result response
		String expectedResponse = tuningService.getJSONData("retrieveEOExpected");
		String resultResponse =  tuningService.getResponseResult(resultRetrieveEO);

		verifyRetrieveEOResult(resultResponse,expectedResponse);
	}

	@Test(dataProvider = "testdata")
	public void testRetrieveEOAPG(Object key,Object inputPara) throws IOException{

		String query_no = resultRetrieveEO.getString("queryNO");
		String sql_statement = resultRetrieveEO.getString("sqlText");
		String param4SET_PLAN_HINT = resultRetrieveEO.getString("param4SET_PLAN_HINT");
		String isCompare = "false";
		String arId = resultRetrieveEO.getString("arId");

		JSONObject result = cs.getRetrieveEOAPGAndCompare(dbName, schema, query_no,  sql_statement,  param4SET_PLAN_HINT,  isCompare,  arId);
		
		// Verify the result code
		int responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
	}
	
	@Test(dataProvider = "testdata")
	public void testRetrieveEOCompare(Object key,Object inputPara) throws IOException{
		
		String query_no = resultRetrieveEO.getString("queryNO");
		String sql_statement = resultRetrieveEO.getString("sqlText");
		String param4SET_PLAN_HINT = resultRetrieveEO.getString("param4SET_PLAN_HINT");
		String isCompare = "true";
		String arId = resultRetrieveEO.getString("arId");
		
		JSONObject result = cs.getRetrieveEOAPGAndCompare(dbName, schema, query_no,  sql_statement,  param4SET_PLAN_HINT,  isCompare,  arId);
		
		// Verify the result code
		int responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		


	}
	
	private void verifyResult(String resultResponse, String expectedResponse){
		JSONObject propExpected = JSONObject.fromObject( expectedResponse );
		JSONObject propResult = JSONObject.fromObject( resultResponse );
		JSONArray baseArray = JSONArray.fromObject(propExpected);
		JSONArray resultArray = JSONArray.fromObject(propResult);

		Assert.assertTrue(resultArray.containsAll(baseArray));
	}
	
	private void verifyRetrieveEOResult(String resultResponse, String expectedResponse){
		JSONObject propExpected = JSONObject.fromObject( expectedResponse );
		JSONObject propResult = JSONObject.fromObject( resultResponse );

		ArrayList<String> key = new ArrayList();
		key.add("arId");
		key.add("queryNO");
		key.add("profileExplainTime");
		key.add("EOScript");
		key.add("param4SET_PLAN_HINT");
		Assert.assertTrue(JSONUtils.compareJsons(propExpected, propResult, key));
	}
	
	private void deleteWorkload(String instID){
		/*
		 * Delete job when check verification point successfully
		 */
		JSONObject delStatus = cs.deleteJob(instID);
		String delResultCode = delStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
	}
	
	private void runSQL()throws Exception{
		StringBuffer runsql = new StringBuffer();
		runsql.append("delete from DSJOBMGR.JOBS where jobid = 1;");
		runsql.append("delete from DSJOBMGR.JOBPROPS where jobid = 1;");
		runsql.append("delete from PROCMGMT.PROCHIST where INSTID in ('1','LOCDB21:1');");
		runsql.append("delete from PROCMGMT.PROCHIST_PROPERTY where INSTID in ('1','LOCDB21:1');");
		
		//create job
		runsql.append("insert into DSJOBMGR.JOBS(JOBID,JOBNAME,JOBTYPE,SCHEDENABLED,JOBCREATOR,LASTMODIFIED,JOBCOMPONENT, DBREQFORJOB) VALUES (1,'QMEOTestJob','querytunerjobs', 0,'admin',current timestamp,'OQWTComp', 1);");
		runsql.append("insert into DSJOBMGR.JOBPROPS(JOBID,JOBPROPNAME,JOBPROPVALUE) VALUES(1,'DESC_SECTION','{\"schedenabled\":\"0\",\"dbreqforjob\":\"1\",\"jobType\":\"querytunerjobs\",\"stagingtabledbconnectionprofilename\":\"LOCDB21\",\"jobname\":\"QMEOTestJob\",\"jobcreator\":\"admin\",\"jobid\":\"1\",\"mondbconprofile\":\"LOCDB21\",\"jobdesc\":\"\"}');");
		runsql.append("insert into DSJOBMGR.JOBPROPS(JOBID,JOBPROPNAME,JOBPROPVALUE) VALUES(1,'OQWT_SECTION','{\"wtaaValCheck\":false,\"wsaValCheck\":true,\"curDegreeValue\":\"ANY\",\"tuningType\":\"WORKLOAD\",\"deleteStagingDataOnSuccessfulCapture\":\"NO\",\"rid\":\"\",\"captureMaxRowsCount\":\"100\",\"wccJobStatus\":\"Running\",\"reExplainWorkloadValCheck\":true,\"ISNEWTUNE\":true,\"isNewF\":true,\"curRefAgeValue\":\"ANY\",\"wtaaModelSelect\":\"modeling\",\"stagingTableSchema\":\"CQMTOOLS\",\"retune\":\"false\",\"workloadName\":\"QMEOTestJob\",\"stagingTableDBConnectionProfileName\":\"LOCDB21\",\"wccJobID\":\"QMEOTestJob#1\",\"dbconfigured\":\"1\",\"curMQTValue\":\"ALL\",\"curSysTimeValue\":\"NULL\",\"stagingTableDBType\":\"DB2Z\",\"tuningCtx\":\"STANDALONE\",\"providerID\":\"CQM\",\"workloadID\":\"75\",\"Select\":\"3\",\"oqwtLicenseType\":\"The full set of tuning features is available.\",\"curBusiTimeValue\":\"NULL\",\"curPathValue\":\"\",\"curGetArchiveValue\":\"Y\",\"licenseLabel\":\"The full set of tuning features is available.\",\"wiaValCheck\":false,\"SQLID\":\"c01d02\",\"monitoredDbType\":\"DB2Z\",\"desc\":\"\",\"monitoredDbProfile\":\"LOCDB21\"}');");
		runsql.append("insert into PROCMGMT.PROCHIST(INSTID,PROCID,COMPID,START_TIME,END_TIME,STATUS) VALUES ('1', 1.1,'jobmanager',current timestamp,current timestamp, 3);");
		runsql.append("insert into PROCMGMT.PROCHIST(INSTID,PROCID,COMPID,START_TIME,END_TIME,STATUS) VALUES ('LOCDB21:1', 1.1,'jobmanager',current timestamp,current timestamp, 2);");
		runsql.append("insert into PROCMGMT.PROCHIST_PROPERTY(INSTID,PROPERTY_ID,SEQUENCE,PROPERTY_VAL) VALUES (1,'PROCESS_RETURN_CODE', 0, 0);");
		runsql.append("insert into PROCMGMT.PROCHIST_PROPERTY(INSTID,PROPERTY_ID,SEQUENCE,PROPERTY_VAL) VALUES ('LOCDB21:1','ADDITIONALDETAILS', 0,'WORKLOAD TUNING, EXPLAIN_START=2016-11-11 11:24:03.393, EXPLAIN_END=2016-11-11 11:24:03.716');");
		runsql.append("insert into PROCMGMT.PROCHIST_PROPERTY(INSTID,PROPERTY_ID,SEQUENCE,PROPERTY_VAL) VALUES ('LOCDB21:1','PROCESS_RETURN_CODE', 0, 0);");
		runsql.append("insert into PROCMGMT.PROCHIST_PROPERTY(INSTID,PROPERTY_ID,SEQUENCE,PROPERTY_VAL) VALUES ('LOCDB21:1','PROGRESS', 0,'Completed');");
		runsql.append("insert into PROCMGMT.PROCHIST_PROPERTY(INSTID,PROPERTY_ID,SEQUENCE,PROPERTY_VAL) VALUES ('LOCDB21:1','tuningResult', 0,'TUNING_SUCCESS');");
		
		//create workload
		runsql.append("delete from db2osc.dsn_wcc_workloads where wlid=1;");
		runsql.append("delete from db2osc.dsn_wcc_wl_sources where wlid=1;");
		runsql.append("delete from db2osc.dsn_wcc_stmt_insts where wlid=1;");
		runsql.append("delete from db2osc.dsn_wcc_stmt_texts where stmt_text_id=1;");
		runsql.append("delete from db2osc.dsn_wcc_tasks where taskid in (1, 2, 3);");
		runsql.append("delete from  db2osc.dsn_wcc_hostv_set where instid = 1;");
		runsql.append("delete from  db2osc.dsn_wcc_hostv_val where instid = 1;");
		runsql.append("delete from  db2osc.dsn_wcc_hostv_pred where instid = 1;");
		
		runsql.append("insert into db2osc.dsn_wcc_workloads (WLID,NAME,STATUS,ANALYZE_COUNT,OWNER,EXPLAIN_STATUS,CPUTIME_STATUS) values(1,'QMEOTestJob#1', 9, 0,'DB2INST1', 5,'N');");
		runsql.append("insert into db2osc.dsn_wcc_wl_sources (SRCID,NAME,WLID,TYPE,STATUS,CONSOLIDATE_STATUS,LASTUPDATETS,QUERY_COUNT,MONITOR_STATUS,NEW_QUERY_COUNT,MONITOR_ENABLED) values(1,'Source_0', 1, 207, 9, 21,current timestamp, 1, 1, 0,'Y');");
		runsql.append("insert into db2osc.dsn_wcc_stmt_texts ( STMT_TEXT_ID,QUALIFIER,HASHKEY,TRANSFORMED_HASKEY,STMT_LENGTH,STMT_TEXT) values (1,'C01D02', 0, 1186955152, 87,'SELECT *  FROM T2, T1  WHERE T1.C1 < ?  AND T2.C2 = ?  AND T1.C3 > ?  AND T2.C2 < T1.C2');");
		runsql.append("insert into db2osc.dsn_wcc_stmt_insts ( INSTID,STMT_TEXT_ID,TRANSFORMED_TEXTID,WLID,SRCID,PRIMAUTH,CURSQLID,PLANNAME,COLLID,PKGNAME,VERSION,SECTNOI,REOPT,PATHSCHEMAS,BIND_ISO,BIND_CDATA,BIND_DYNRL,BIND_DEGREE,BIND_SQLRL,BIND_CHOLD,CACHED_TS,LAST_UPDATE_TS,LAST_EXPLAIN_TS,LITERALS,PERMANENT,EXPLAIN_STATUS,STMT_TOKEN,GROUP_MEMBER,LITERAL_REPL,STMTNO,BIND_TIME,QUERYNO,BIND_OWNER,BIND_EXPLAIN_OPTION,QUALIFIER,STMT_ID,BIND_SYSTIME_SENSITIVE,BIND_BUSTIME_SENSITIVE,BIND_GETARCHIVE_SENSITIVE,EXPANSION_REASON,STMTNOI,ACCELERATED,STAT_ACC_ELAP,STAT_ACC_CPU,STAT_ACC_ROW,STAT_ACC_BYTE,STAT_ACC_1ROW,STAT_ACC_DB2,STAT_ACC_EXEC,STAT_ACC_WAIT,ACCEL_OFFLOAD_ELIGIBLE,ACCELERATOR_NAME,COST_CATEGORY,PROCMS,PROCSU,NUM_RELATIONAL_SCANS,NUM_NONMATCHING_INDEXSCANS,NUM_MERGESCAN_JOINS,NUM_HYBRID_JOINS,NUM_SORT_NODES,TOTAL_COST,STMT_TYPE,PER_STMT_ID,STBLGRP,STABILIZED,EO_CANDIDATE,EXEC_COUNT,HOSTV_COUNT,HOSTV_SET_COUNT) values (1, 1, NULL, 1, 1,'C01D02','C01D02','' ,'' , '','' , 0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,current timestamp,current timestamp,current timestamp,NULL,'Y', 5,'6381F27B5D4EE1152032','','', 0,NULL, 0,'','N','C01D02', 0,NULL,NULL,NULL,'', 0,NULL, 0, 0, 0, 0, 0, 0, 0, 0,NULL,NULL,'B', 1, 1, 1, 0, 0, 0, 0, 1.1931812782713629,'SELECT', 0,'','', 1, 16, 3, 3);");
		runsql.append("insert into db2osc.dsn_wcc_tasks (TASKID,NEXTTASK,SCHEDULED_TASKID,WLID,SRCID,TYPE,SUBTYPE,STATUS,CREATOR ,START_TIME,END_TIME,INTERVAL,CONSOLIDATE_RTINFO,CONSOLIDATE_EPINFO,CONSOLIDATE_LITERA,KEEP_STATEMENTS,CONSOLIDATION_TIME,START_TRACE,STOP_TRACE,WARMUP_TIME,LAST_FIRE_TIME,LAST_UPDATE_TS,CONFIG,ACT_START_TIME,ACT_END_TIME) values ( 1,NULL,NULL, 1,NULL, 5, 14,'F','DB2INST1',NULL,NULL,NULL,'N',NULL,'N','N',NULL,'N','N',NULL,NULL,current timestamp,blob('23467269204e6f762031312031313a32313a31322043535420323031360a50524f434553534f525f4e414d453d636f6d2e69626d2e64617461746f6f6c732e64736f652e736572762e456d62656454756e696e6741647669736f720a454e41424c455f5357544943485f53514c49443d66616c73650a'),current timestamp,current timestamp);");
		runsql.append("insert into db2osc.dsn_wcc_tasks (TASKID,NEXTTASK,SCHEDULED_TASKID,WLID,SRCID,TYPE,SUBTYPE,STATUS,CREATOR ,START_TIME,END_TIME,INTERVAL,CONSOLIDATE_RTINFO,CONSOLIDATE_EPINFO,CONSOLIDATE_LITERA,KEEP_STATEMENTS,CONSOLIDATION_TIME,START_TRACE,STOP_TRACE,WARMUP_TIME,LAST_FIRE_TIME,LAST_UPDATE_TS,CONFIG,ACT_START_TIME,ACT_END_TIME) values ( 2,NULL,NULL, 1,NULL, 4, 1,'F','DB2INST1',NULL,NULL,NULL,'Y', 1,'N','N',NULL,'N','N',NULL,NULL,current timestamp,blob('23467269204e6f762031312031313a32313a31322043535420323031360a50524f434553534f525f4e414d453d636f6d2e69626d2e64617461746f6f6c732e64736f652e736572762e456d62656454756e696e6741647669736f720a454e41424c455f5357544943485f53514c49443d66616c73650a'),current timestamp,current timestamp);");
		runsql.append("insert into db2osc.dsn_wcc_tasks (TASKID,NEXTTASK,SCHEDULED_TASKID,WLID,SRCID,TYPE,SUBTYPE,STATUS,CREATOR ,START_TIME,END_TIME,INTERVAL,CONSOLIDATE_RTINFO,CONSOLIDATE_EPINFO,CONSOLIDATE_LITERA,KEEP_STATEMENTS,CONSOLIDATION_TIME,START_TRACE,STOP_TRACE,WARMUP_TIME,LAST_FIRE_TIME,LAST_UPDATE_TS,CONFIG,ACT_START_TIME,ACT_END_TIME) values ( 3,NULL,NULL, 1,NULL, 5, 1,'F','DB2INST1',NULL,NULL,NULL,'N',NULL,'N','N',NULL,'N','N',NULL,NULL,current timestamp,blob('23467269204e6f762031312031313a32313a31322043535420323031360a50524f434553534f525f4e414d453d636f6d2e69626d2e64617461746f6f6c732e64736f652e736572762e456d62656454756e696e6741647669736f720a454e41424c455f5357544943485f53514c49443d66616c73650a'),current timestamp,current timestamp);");
		runsql.append("insert into db2osc.dsn_wcc_hostv_set (INSTID,HOSTV_SET_ID,HOSTV_SET_EXEC_COUNT,HOSTV_SET_WEIGHT) values (1, 1, 6, 0.375);");
		runsql.append("insert into db2osc.dsn_wcc_hostv_val (INSTID,HOSTV_SET_ID,HOSTV_POSITION,HOSTV_TYPE,HOSTV_VALUE) values (1, 1, 1,'INTEGER', 1);");
		runsql.append("insert into db2osc.dsn_wcc_hostv_val (INSTID,HOSTV_SET_ID,HOSTV_POSITION,HOSTV_TYPE,HOSTV_VALUE) values (1, 1, 2,'INTEGER', 2);");
		runsql.append("insert into db2osc.dsn_wcc_hostv_val (INSTID,HOSTV_SET_ID,HOSTV_POSITION,HOSTV_TYPE,HOSTV_VALUE) values (1, 1, 3,'INTEGER', 3);");
		runsql.append("insert into db2osc.dsn_wcc_hostv_pred (INSTID,HOSTV_POSITION,HOSTV_PRED_NO,HOSTV_PRED_TEXT,HOSTV_EO_CANDIDATE) values (1, 1, 2,'\"C01D02\".\"T1\".\"C1\"  <  ?', 1);");
		runsql.append("insert into db2osc.dsn_wcc_hostv_pred (INSTID,HOSTV_POSITION,HOSTV_PRED_NO,HOSTV_PRED_TEXT,HOSTV_EO_CANDIDATE) values (1, 2, 3,'\"C01D02\".\"T2\".\"C2\"  =  ?', 0);");
		runsql.append("insert into db2osc.dsn_wcc_hostv_pred (INSTID,HOSTV_POSITION,HOSTV_PRED_NO,HOSTV_PRED_TEXT,HOSTV_EO_CANDIDATE) values (1, 3, 4,'\"C01D02\".\"T1\".\"C3\"  >  ?', 1); ");
		runsql.append("");
		runsql.append("");
		
		DBUtils dbUtils =new DBUtils();
		
		dbUtils.executeBatch(runsql.toString());

	}
	
     
}
