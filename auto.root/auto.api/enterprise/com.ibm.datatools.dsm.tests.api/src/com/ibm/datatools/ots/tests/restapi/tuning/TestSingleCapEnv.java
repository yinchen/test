package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;


/**
 * Case Type : ZOS automation case
 * Case number on wiki : 31
 * Case description : Select one single tuning job->ViewResults->
 * 					  Tuning Job Log->Capture Query Environment->Click OK->Verify capture result
 * @author yinchen
 *
 */
public class TestSingleCapEnv extends TuningTestBase{
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		// return multiple JSON post properties
		cs = new TuningCommandService("TestCreateSingleJobInputSQLAll.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);

	}

	@Test(dataProvider = "testdata")
	public void testRunSingleQueryJob(Object key, Object inputPara) throws InterruptedException, FileNotFoundException, IOException {
		
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		String queryText = obj.getString("queryText");
		
		String sqlid = cs.getSQLIDByDBProfile(dbName);
		String dbType = cs.getDBTypeByDBProfile(dbName);
		
		String random = String.valueOf(System.currentTimeMillis()).substring(8, 12);
		
		String jobName = "Query" + random + "-ZOS31";
		JSONObject result = cs.submitJob(genDescSection(random,jobName), genOQWTSection(random, queryText,sqlid,dbName,dbType));
		
		int responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
			
		result = cs.runJob(jobId, dbName);
		responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String resultcode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultcode));
			
		CheckJobStatus.checkJobStatus(jobId, 5000L);
		
		JSONObject singleJobStatus = cs.getJobByJobID(jobId);
		String jobID = singleJobStatus.getString("JOBID");
		String instID = singleJobStatus.getString("INSTID");
		String arId = singleJobStatus.getString("RESULTID");
		
		JSONObject singleJobDetails = cs.getSingleJobDetails(dbName, jobID, jobName, instID, arId);
		
		String resultID = singleJobDetails.getString("queryResultID");
		result = cs.submitJob(gDescSection(random,jobName), genOQWTSection(dbName,jobID,resultID));
		
		responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String capJobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
			
		result = cs.runJob(capJobId, dbName);
		responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		resultcode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultcode));
		
		CheckJobStatus.checkJobStatus(capJobId, 5000L);
		
		/*
		 * Delete job when run capture environment job successfully
		 */
		List<String> jobInstID = new ArrayList<String>();
		JSONObject capJobStatus = cs.getJobByJobID(capJobId);
		String capJobInstID = capJobStatus.getString("INSTID");
		jobInstID.add(instID);
		jobInstID.add(capJobInstID);
		JSONObject delJobStatus = cs.deleteJobs(jobInstID);
		String delResultCode = delJobStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
		
		
	}
	
	public Properties genDescSection(String random,String jobName) {
		Properties desc = new Properties();
		desc.put("jobname", jobName);
		desc.put("jobtype", "querytunerjobs");
		desc.put("schedenabled", 1);
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		return desc;
	}

	public Properties genOQWTSection(String random, String queryText,String sqlid,String dbName,String dbType) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("queryid", "");
		oqwt_SectionData.put("tuningType", "SQL_BASED");
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("jobName", "Query_" + random + "-Result_" + random);
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("sqlid", sqlid);
		oqwt_SectionData.put("degree", "ANY");
		oqwt_SectionData.put("hint", "");
		oqwt_SectionData.put("refreshAge", "ANY");
		oqwt_SectionData.put("MQT", "ALL");
		oqwt_SectionData.put("systemTime", "NULL");
		oqwt_SectionData.put("businessTime", "NULL");
		oqwt_SectionData.put("getArchive", "Y");
		oqwt_SectionData.put("returnAllStats", "OFF");
		oqwt_SectionData.put("queryName", "Query_" + random);
		oqwt_SectionData.put("resultName", "Result_" + random);
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("reExplainValCheck", true);
		oqwt_SectionData.put("apgValCheck", true);
		oqwt_SectionData.put("formatQueryValCheck", true);
		oqwt_SectionData.put("iaValCheck", true);
		oqwt_SectionData.put("saValCheck", true);
		oqwt_SectionData.put("queryText", queryText);
		oqwt_SectionData.put("hashID", "");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		oqwt_SectionData.put("monitoredDbType", dbType);
		return oqwt_SectionData;
	}

	public Properties gDescSection(String random,String jobName) {
		Properties desc = new Properties();
		desc.put("jobname", "Capture_" + jobName);
		desc.put("jobtype", "captureEnvjob");
		desc.put("schedenabled", 0);
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}

	public Properties genOQWTSection(String dbName,String wccJobID,String queryResultID) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("tuningType", "Capture Environment");
		oqwt_SectionData.put("wccJobID", wccJobID);
		oqwt_SectionData.put("queryResultID", queryResultID);
		oqwt_SectionData.put("profile", dbName);
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		return oqwt_SectionData;
	}
}










