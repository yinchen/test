package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;


/**
 * Case Type : ZOS automation case
 * Case number on wiki : 15
 * Case description : Select one single tuning job with IA recommendations->View Results-> 
 * 					  in IA result page click Impact analysis->set Job name->set SQLID->
 * 					  select Analyze statements in existing jobs->select one job->click OK->verify results
 * @author yinchen
 *
 */
public class TestSingleIAImpactAnalysis extends TuningTestBase{

	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("TestSingleImpactAnalysis.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	@Test(dataProvider = "testdata")
	public void testCreateSingleJob(Object key,Object inputPara) throws InterruptedException, FileNotFoundException, IOException {
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		
		String dbType = cs.getDBTypeByDBProfile(dbName);
		String sqlid = cs.getSQLIDByDBProfile(dbName);
		
		String queryText = obj.getString("queryText");
		String impactAnaschema = sqlid;
		String random = String.valueOf(System.currentTimeMillis());
		String jobName = "Query_" + random.substring(8, 12) + "-ZOS15";
		
		String sqlFileName = cs.uploadFile(obj.getString("sqlFileName"));
		
		String wccJobID = "workload#" + random;
		String workloadJobName = "Workload" + random.substring(8, 12) +"-ZOS15";
		JSONObject result = cs.submitJob(genDescSection(workloadJobName,dbName), 
				genOQWTSection(random,sqlFileName ,schema,dbType,dbName,wccJobID,sqlid));
		
		int responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String workloadJobId = result.getString("jobid");
		Assert.assertTrue(workloadJobId != null);
			
		result = cs.runJob(workloadJobId, dbName);
		responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String resultcode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultcode));
		
		CheckJobStatus.checkJobStatus(workloadJobId, 15000L);
		
		
		
		/**
		 * Create and run a new single job with WIA option
		 */
		result = cs.submitJob(genDescSection(jobName,random,dbName), 
				genOQWTSection(random,queryText,schema,dbName,dbType,sqlid));
		responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
			
		result = cs.runJob(jobId, dbName);
		responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		resultcode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultcode));
				 
		CheckJobStatus.checkJobStatus(jobId, 5000L);
		
		JSONObject createdSingleJob = cs.getJobByJobID(jobId);
		//System.out.println(">>>"+createdSingleJob);
		String queryResultID = createdSingleJob.getString("RESULTID");
		String arId = queryResultID;
		String instID = createdSingleJob.getString("INSTID");
		JSONObject singleJobDetails = cs.getSingleJobDetails(dbName, jobId, workloadJobName, instID, arId);
		String queryNodeID = singleJobDetails.getString("queryNodeID");
		/**
		 * Get "queryNodeID" and "queryResultID" from single job
		 */
		
				 
		/**
		 * Create impact analysis job	 
		 */
		String srcJobNames = " [{\'JOBID\':"+workloadJobId+", \'JOBNAME\':"+workloadJobName+" }] ";
		JSONObject impactAnaResult = cs.submitJob(genDescSection(random), 
				gOQWTSection(random,queryNodeID,queryResultID,impactAnaschema,dbName,srcJobNames,sqlid));
		
		/**
		 * Run impact analysis job
		 */
		int resCode = (Integer)impactAnaResult.get("ResponseCode");
		CheckJobStatus.checkResponseCode(impactAnaResult, resCode);
		String impactAnajobID = impactAnaResult.getString("jobid");
		Assert.assertTrue(impactAnajobID != null);
		impactAnaResult = cs.runJob(impactAnajobID, dbName);
		
		resCode = (Integer)impactAnaResult.get("ResponseCode");
		CheckJobStatus.checkResponseCode(impactAnaResult, resCode);
		String impactAnaresultcode = impactAnaResult.getString("resultcode");
		Assert.assertTrue("success".equals(impactAnaresultcode));
		CheckJobStatus.checkJobStatus(impactAnajobID, 5000L);
		
		/*
		 * Delete job when run IA impact analysis successfully
		 */
		List<String> jobInstID = new ArrayList<String>();
		JSONObject workloadJobStatus = cs.getJobByJobID(workloadJobId);
		JSONObject singleJobStatus = cs.getJobByJobID(jobId);
		JSONObject impactAnaJobStatus = cs.getJobByJobID(impactAnajobID);
		
		String workloadJobInstID = workloadJobStatus.getString("INSTID");
		String singleJobInstID = singleJobStatus.getString("INSTID");
		String impactAnaJobInstID = impactAnaJobStatus.getString("INSTID");
		
		jobInstID.add(workloadJobInstID);
		jobInstID.add(singleJobInstID);
		jobInstID.add(impactAnaJobInstID);
		
		JSONObject delJobStatus = cs.deleteJobs(jobInstID);
		String delResultCode = delJobStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
	}
	
	public Properties genDescSection(String workloadJobName,String dbName) {
		Properties desc = new Properties();
		desc.put("jobname", workloadJobName);
		desc.put("jobType", "querytunerjobs");
		desc.put("schedenabled", 0);
		desc.put("mondbconprofile", dbName);
		desc.put("jobcreator", "admin");
		desc.put("jobid", "0");
		desc.put("jobdesc", "");
		desc.put("dbreqforjob", "1");
		return desc;
	}
	
	public Properties genOQWTSection(String random, String sqlFileName,
			String schema, String dbType,String dbName,String wccJobID,String sqlid) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("tuningType", "WORKLOAD");
		oqwt_SectionData.put("wccJobStatus", "Running");
		oqwt_SectionData.put("sqlFileName", sqlFileName);
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("workloadName", "Workload_" + random);
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("wtaaValCheck", false);
		oqwt_SectionData.put("wsaValCheck", true);
		oqwt_SectionData.put("curDegreeValue", "ANY");
		oqwt_SectionData.put("reExplainWorkloadValCheck", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("curRefAgeValue", "ANY");
		oqwt_SectionData.put("stmtDelimiter", ";");
		oqwt_SectionData.put("wtaaModelSelect", "modeling");
		oqwt_SectionData.put("wccJobID", wccJobID);
		oqwt_SectionData.put("dbconfigured", true);
		oqwt_SectionData.put("curMQTValue", "ALL");
		oqwt_SectionData.put("curSysTimeValue", "NULL");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("curBusiTimeValue", "NULL");
		oqwt_SectionData.put("curPathValue", "");
		oqwt_SectionData.put("curGetArchiveValue", "Y");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("wiaValCheck", true);
		oqwt_SectionData.put("SQLID", sqlid);
		//oqwt_SectionData.put("monitoredDbName", dbName);
		oqwt_SectionData.put("monitoredDbType", dbType);
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		return oqwt_SectionData;
	}
	
	
	public Properties genDescSection(String jobName,String random,String dbName) {
		Properties desc = new Properties();
		desc.put("jobname", jobName);
		desc.put("jobType", "querytunerjobs");
		desc.put("schedenabled", 0);
		desc.put("mondbconprofile", dbName);
		desc.put("jobcreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}
	
	public Properties genOQWTSection(String random, String queryText,
			String schema, String dbType, String dbName,String sqlid) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("queryid", "");
		oqwt_SectionData.put("tuningType", "SQL_BASED");
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("jobName", "Query_" + random + "-Result_" + random);
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("sqlid", sqlid);
		oqwt_SectionData.put("degree", "ANY");
		oqwt_SectionData.put("hint", "");
		oqwt_SectionData.put("refreshAge", "ANY");
		oqwt_SectionData.put("MQT", "ALL");
		oqwt_SectionData.put("systemTime", "NULL");
		oqwt_SectionData.put("businessTime", "NULL");
		oqwt_SectionData.put("getArchive", "Y");
		oqwt_SectionData.put("returnAllStats", "OFF");
		oqwt_SectionData.put("queryName", "Query_" + random);
		oqwt_SectionData.put("resultName", "Result_" + random);
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("reExplainValCheck", true);
		oqwt_SectionData.put("apgValCheck", true);
		oqwt_SectionData.put("formatQueryValCheck", true);
		oqwt_SectionData.put("iaValCheck", true);
		oqwt_SectionData.put("saValCheck", true);
		oqwt_SectionData.put("queryText", queryText);
		oqwt_SectionData.put("hashID", "");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		oqwt_SectionData.put("monitoredDbType", dbType);
		return oqwt_SectionData;
	}

	public Properties genDescSection(String jobName) {
		Properties desc = new Properties();
		desc.put("jobname", "WIA-ImpactAnalysis" + jobName);
		desc.put("jobtype", "indeximpactjob");
		desc.put("schedenabled", 0);
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}
	
	public Properties gOQWTSection(String random,String queryNodeID,String queryResultID,String impactAnaschema,
			String dbName,String srcJobNames,String sqlid) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("tuningType", "IMPACT-ANALYSIS");
		oqwt_SectionData.put("workloadName", "iia-z#" + random);
		oqwt_SectionData.put("wccJobID", "iia-z#" + random);
		oqwt_SectionData.put("queryNodeID", queryNodeID);
		oqwt_SectionData.put("queryResultID", queryResultID);
		oqwt_SectionData.put("profile", dbName);
		oqwt_SectionData.put("monitoreddbprofile", dbName);
		oqwt_SectionData.put("schema", impactAnaschema);
		oqwt_SectionData.put("sqlid", sqlid);
		oqwt_SectionData.put("ddltype", "AL");
		oqwt_SectionData.put("srcJobNames",srcJobNames);
		return oqwt_SectionData;
	}
	
}































