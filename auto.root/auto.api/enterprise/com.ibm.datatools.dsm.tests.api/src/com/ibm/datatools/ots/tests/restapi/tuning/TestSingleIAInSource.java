package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;

import net.sf.json.JSONObject;

/**
 * Case Type : ZOS automation case
 * Case number on wiki : 8
 * Case description : Select one single tuning job with IA recommendations->View Results->
 * 					  in IA result page click Impact analysis->set Job name->set SQLID->
 * 					  select Analyze statements in the following sources->
 * 					  select package and Dynamic Statement Cache ->click OK->verify results
 * @author yinchen
 *
 */
public class TestSingleIAInSource extends TuningTestBase{
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("TestSingleImpactAnalysis.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	@Test(dataProvider = "testdata")
	public void testCreateSingleJob(Object key,Object inputPara) throws FileNotFoundException, InterruptedException, IOException{
		
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		
		String dbType = cs.getDBTypeByDBProfile(dbName);
		String sqlid = cs.getSQLIDByDBProfile(dbName);
		
		String queryText = obj.getString("queryText");
		String random = String.valueOf(System.currentTimeMillis());
		
		/**
		 * Create a single job with IA option
		 */
		String jobName = "Query_" + random.substring(8, 12) + "-ZOS8";
		JSONObject singleJobResult = cs.submitJob(genDescSection(jobName, random, dbName), 
				genOQWTSection(random, queryText, schema, dbType, dbName, sqlid));
		int responseCode = (Integer) singleJobResult.get("ResponseCode");
		CheckJobStatus.checkResponseCode(singleJobResult, responseCode);
		String jobId = singleJobResult.getString("jobid");
		Assert.assertTrue(jobId != null);
			
		singleJobResult = cs.runJob(jobId, dbName);
		responseCode = (Integer) singleJobResult.get("ResponseCode");
		CheckJobStatus.checkResponseCode(singleJobResult, responseCode);
		String resultcode = singleJobResult.getString("resultcode");
		Assert.assertTrue("success".equals(resultcode));
				 
		CheckJobStatus.checkJobStatus(jobId, 5000L);
		
		JSONObject singleJobObj = cs.getJobByJobID(jobId);
		String instID = singleJobObj.getString("INSTID");
		String queryResultID = singleJobObj.getString("RESULTID");
		String arId = queryResultID;
		
		JSONObject singleJobDetails = cs.getSingleJobDetails(dbName, jobId, jobName, instID, arId);
		String queryNodeID = singleJobDetails.getString("queryNodeID");
		
		String impactAnaschema = sqlid;
		JSONObject impactJobResult = cs.submitJob(genDescSection(jobName,random.substring(9, 13)),
				gOQWTSection(random, queryNodeID, queryResultID, impactAnaschema, dbName, sqlid));
		
		responseCode = (Integer)impactJobResult.get("ResponseCode");
		CheckJobStatus.checkResponseCode(impactJobResult, responseCode);
		
		String impactJobID = impactJobResult.getString("jobid");
		Assert.assertTrue(impactJobID != null);
		
		impactJobResult = cs.runJob(impactJobID, dbName);
		responseCode = (Integer) impactJobResult.get("ResponseCode");
		CheckJobStatus.checkResponseCode(impactJobResult, responseCode);
		resultcode = impactJobResult.getString("resultcode");
		Assert.assertTrue("success".equals(resultcode));
		CheckJobStatus.checkJobStatus(impactJobID, 35000L);
		
		/*
		 * Delete job when run IA impact analysis successfully
		 */
		List<String> jobInstID = new ArrayList<String>();
		JSONObject impactAnaJobStatus = cs.getJobByJobID(impactJobID);
		
		String impactAnaJobInstID = impactAnaJobStatus.getString("INSTID");
		
		jobInstID.add(instID);
		jobInstID.add(impactAnaJobInstID);
		
		JSONObject delJobStatus = cs.deleteJobs(jobInstID);
		String delResultCode = delJobStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
		
	}
	
	
	
	public Properties genDescSection(String jobName,String random,String dbName) {
		Properties desc = new Properties();
		desc.put("jobname", jobName);
		desc.put("jobType", "querytunerjobs");
		desc.put("schedenabled", 0);
		desc.put("mondbconprofile", dbName);
		desc.put("jobcreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}
	
	public Properties genOQWTSection(String random, String queryText,
			String schema, String dbType, String dbName,String sqlid) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("queryid", "");
		oqwt_SectionData.put("tuningType", "SQL_BASED");
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("jobName", "Query_" + random + "-Result_" + random);
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("sqlid", sqlid);
		oqwt_SectionData.put("degree", "ANY");
		oqwt_SectionData.put("hint", "");
		oqwt_SectionData.put("refreshAge", "ANY");
		oqwt_SectionData.put("MQT", "ALL");
		oqwt_SectionData.put("systemTime", "NULL");
		oqwt_SectionData.put("businessTime", "NULL");
		oqwt_SectionData.put("getArchive", "Y");
		oqwt_SectionData.put("returnAllStats", "OFF");
		oqwt_SectionData.put("queryName", "Query_" + random);
		oqwt_SectionData.put("resultName", "Result_" + random);
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("reExplainValCheck", true);
		oqwt_SectionData.put("apgValCheck", true);
		oqwt_SectionData.put("formatQueryValCheck", true);
		oqwt_SectionData.put("iaValCheck", true);
		oqwt_SectionData.put("saValCheck", true);
		oqwt_SectionData.put("queryText", queryText);
		oqwt_SectionData.put("hashID", "");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		oqwt_SectionData.put("monitoredDbType", dbType);
		return oqwt_SectionData;
	}
	
	public Properties genDescSection(String jobName,String random) {
		Properties desc = new Properties();
		desc.put("jobname",jobName + "-Impact" + random);
		desc.put("jobtype", "indeximpactjob");
		desc.put("schedenabled", 0);
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}
	
	public Properties gOQWTSection(String random,String queryNodeID,String queryResultID,String impactAnaschema,
			String dbName,String sqlid) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("tuningType", "IMPACT-ANALYSIS");
		oqwt_SectionData.put("workloadName", "iia-z#" + random);
		oqwt_SectionData.put("wccJobID", "iia-z#" + random);
		oqwt_SectionData.put("queryNodeID", queryNodeID);
		oqwt_SectionData.put("queryResultID", queryResultID);
		oqwt_SectionData.put("profile", dbName);
		oqwt_SectionData.put("monitoreddbprofile", dbName);
		oqwt_SectionData.put("schema", impactAnaschema);
		oqwt_SectionData.put("sqlid", sqlid);
		oqwt_SectionData.put("ddltype", "AL");
		oqwt_SectionData.put("PKGANALYZING",true);
		oqwt_SectionData.put("cacheALYZING",true);
		return oqwt_SectionData;
	}
	
}














