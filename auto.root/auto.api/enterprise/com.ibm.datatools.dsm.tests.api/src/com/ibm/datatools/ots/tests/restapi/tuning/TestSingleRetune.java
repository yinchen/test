package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;

/**
 * Case Type : ZOS automation case
 * Case number on wiki : 16
 * Case description : Select one single tuning job->
 * 					  Click Retune ->set options->click Done-> verify all advisors result
 * @author yinchen
 *
 */
public class TestSingleRetune extends TuningTestBase{

	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		// return multiple JSON post properties
		cs = new TuningCommandService("TestSingleRetune.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);

	}

	@Test(dataProvider = "testdata")
	public void testCreateSingleJob(Object key, Object inputPara) throws InterruptedException, FileNotFoundException, IOException {
		
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		String queryText = obj.getString("sqltext");
		String jobId = null;
		
		String dbType = cs.getDBTypeByDBProfile(dbName);
		String sqlid = cs.getSQLIDByDBProfile(dbName);
		
		String random = String.valueOf(System.currentTimeMillis()).substring(8, 12);
		String jobName = "Query" + random +"-ZOS16";
		JSONObject result = cs.submitJob(gDescSection(random,jobName), genOQWTSection(random, queryText, schema, dbType, dbName,sqlid));
		
		int responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
			
		result = cs.runJob(jobId, dbName);
		responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String resultcode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultcode));
				 
		CheckJobStatus.checkJobStatus(jobId, 3000L);
		
		JSONObject singleJob = cs.getJobByJobID(jobId);
		
		String jobID = singleJob.getString("JOBID");
		String instID = singleJob.getString("INSTID");
		String stmtId = singleJob.getString("SQLSTMTID");
		
		/**
		 * Get retune job
		 */
		JSONObject retuneSingleJob = cs.retuneSingleJob(dbName, jobID, instID, stmtId);
		
		String queryid = retuneSingleJob.getString("queryNodeID");
		
		JSONObject retuneJobresult = cs.submitJob(genDescSection(jobName,dbName), 
				genOQWTSection( queryid, random, schema,sqlid, queryText, dbName, dbType));
		
		responseCode = (Integer)retuneJobresult.get("ResponseCode");
		
		CheckJobStatus.checkResponseCode(retuneJobresult, responseCode);
		
		String retuneJobId = retuneJobresult.getString("jobid");
		Assert.assertTrue(retuneJobId != null);
			
		retuneJobresult = cs.runJob(retuneJobId, dbName);
		responseCode = (Integer) retuneJobresult.get("ResponseCode");
		
		CheckJobStatus.checkResponseCode(retuneJobresult, responseCode);
		resultcode = retuneJobresult.getString("resultcode");
		Assert.assertTrue("success".equals(resultcode));
				 
		CheckJobStatus.checkJobStatus(retuneJobId, 5000L);
		
		/**
		 *Ma Wei
		 *Verify IA
		 *Verify SA
		 */
		
		
		JSONObject resultDetails = cs.getJobByJobID(jobId);
		JSONObject jso = cs.getSingleJobDetails(dbName,  result.getString("jobid"), jobName,resultDetails.getString("INSTID"), resultDetails.getString("RESULTID"));
		
		try {
			Object indexes =  jso.get("iaDDLsCreate");
			System.out.println("Here is IA RECOMMENDATIONS : " + indexes.toString());
			Assert.assertTrue(indexes.toString().contains("CREATE INDEX"), "Verify the index can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify IA error*******");
		}
		
		try {
			Object sa_recommendation =  jso.get("SA_RECOMMENDATIONS");
			System.out.println("Here is SA RECOMMENDATIONS : " + sa_recommendation.toString());
			Assert.assertTrue(sa_recommendation.toString().contains("RUNSTATS TABLESPACE"), "Verify the sa can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify SA error*******");
		}
		
		/*
		 * Delete job when run retune job successfully
		 */
		List<String> jobInstID = new ArrayList<String>();
		JSONObject retuneJobStatus = cs.getJobByJobID(retuneJobId);
		String retuneJobInstID = retuneJobStatus.getString("INSTID");
		jobInstID.add(instID);
		jobInstID.add(retuneJobInstID);
		JSONObject delJobStatus = cs.deleteJobs(jobInstID);
		String delResultCode = delJobStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
		
	}
	
	public Properties gDescSection(String random,String jobName) {
		Properties desc = new Properties();
		desc.put("jobname", jobName);
		desc.put("jobtype", "querytunerjobs");
		desc.put("schedenabled", 1);
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		return desc;
	}

	public Properties genOQWTSection(String random, String sqltext,
			String schema, String dbType, String dbName,String sqlid) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("tuningType", "SQL_BASED");
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("queryid", "");
		oqwt_SectionData.put("jobName", "Query_" + random + "-Result_" + random);
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("queryName", "Query_" + random);
		oqwt_SectionData.put("resultName", "Result_" + random);
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("reExplainValCheck", "1");
		oqwt_SectionData.put("apgValCheck", "1");
		oqwt_SectionData.put("formatQueryValCheck", "1");
		oqwt_SectionData.put("iaValCheck", "1");
		oqwt_SectionData.put("saValCheck", "1");
		oqwt_SectionData.put("queryText", sqltext);
		oqwt_SectionData.put("executionId", "");
		oqwt_SectionData.put("stmtCacheId", "");
		oqwt_SectionData.put("tuningCtx", "");
		oqwt_SectionData.put("sqlid", sqlid);
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "OQWT");
		oqwt_SectionData.put("licenseLabel","The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		oqwt_SectionData.put("monitoredDbName", dbName);
		oqwt_SectionData.put("monitoredDbType", dbType);
		return oqwt_SectionData;
	}
	
	public Properties genDescSection(String jobName,String dbName) {
		Properties desc = new Properties();
		desc.put("jobname", "Retune_" + jobName);
		desc.put("jobType", "querytunerjobs");
		desc.put("schedenabled", 0);
		desc.put("mondbconprofile", dbName);
		desc.put("jobcreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}
	
	public Properties genOQWTSection(String queryid,String random,String schema,
			String sqlid,String queryText,String dbName,String dbType) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", true);
		oqwt_SectionData.put("queryid", queryid);
		oqwt_SectionData.put("tuningType", "SQL_BASED");
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("jobName", "Query_" + random + "Retune_" + random);
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("sqlid", sqlid);
		oqwt_SectionData.put("hint", "");
		oqwt_SectionData.put("refreshAge", "0");
		oqwt_SectionData.put("MQT", "SYSTEM");
		oqwt_SectionData.put("systemTime", "NULL");
		oqwt_SectionData.put("businessTime", "NULL");
		oqwt_SectionData.put("getArchive", "N");
		oqwt_SectionData.put("returnAllStats", "OFF");
		oqwt_SectionData.put("queryName", "Query_" + random);
		oqwt_SectionData.put("resultName", " ");
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("reExplainValCheck", true);
		oqwt_SectionData.put("apgValCheck", true);
		oqwt_SectionData.put("formatQueryValCheck", true);
		oqwt_SectionData.put("iaValCheck", true);
		oqwt_SectionData.put("saValCheck", true);
		oqwt_SectionData.put("queryText", queryText);
		oqwt_SectionData.put("hashID", "");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		oqwt_SectionData.put("monitoredDbType", dbType);
		return oqwt_SectionData;
	}	
	
}


	































