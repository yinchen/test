package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;

import net.sf.json.JSONObject;

/**
 * Case Type : Auto case for zOS
 * Case number on wiki : 25
 * Case description :   Run SQL->input Database connection info->input one SQL->
 * 						click Explain->
 * 						click Tune->verify explain and tune results
 * @author yinchen
 *
 */
public class TestSingleRunSQL extends TuningTestBase{

	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("TestCreateSingleJobInputSQLAll.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	/*
	 *  Verification point 1 :
	 *  Run SQL->input Database connection info->input one SQL->
		 *  click Explain->
	 */
	@Test(dataProvider = "testdata")
	public void testRunSQL(Object key,Object inputPara){
		
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		
		String sqlid = cs.getSQLIDByDBProfile(dbName);
		String sql = obj.getString("queryText");
		JSONObject parseResult = cs.parseRunSQL(sql, dbName);
		
		String stmt = parseResult.getString("stmt");
		stmt = "\"" + stmt + "\"";
		dbName = "\"" + dbName + "\"";
		schema = "\"" + schema + "\"";
		sqlid = "\"" + sqlid + "\"";
		
		JSONObject result = cs.explainRunSQL(dbName,stmt,schema,sqlid);
		System.out.println("Run SQL expalin result >> "+result);
		int resultCode = Integer.parseInt(result.getString("code"));
		
		
		if(resultCode == 0){
			System.out.println("Explain sql statement in Run SQL page successful.");
		}else{
			Assert.fail("Explain sql statement in Run SQL page failed. Please refer the error message here : " + result.getString("message") );
		}
		
	}
	
	
	
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	