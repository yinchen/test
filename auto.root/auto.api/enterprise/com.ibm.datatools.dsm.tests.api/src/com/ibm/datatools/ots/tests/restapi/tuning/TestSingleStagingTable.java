package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;

/**
 * Case Type: LUW automation case
 * Case number on wiki : 3
 * Case description :   Optimize->Start Tuning->select SQL statement source: Query Tuner Staging Table->
 * 						set Maximum number of statements to capture->set Staging table schema->
 * 						set workload ID->click Next->select one statement->click Tune selected statement->click Done->
 * 						verify all advisors result(Report, APG, SA, IA).
 * @author yinchen
 *
 */
public class TestSingleStagingTable extends TuningTestBase{
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("TestSingleStagingTable.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	@Test(dataProvider = "testdata")
	public void testCreateSingleJob(Object key,Object inputPara) throws FileNotFoundException, InterruptedException, IOException{
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		String random = String.valueOf(System.currentTimeMillis()).substring(9, 13);
		String stagingTableSchema = obj.getString("stagingTableSchema");
		String stagingTableWorkloadId = obj.getString("stagingTableWorkloadId");

		String dbType = cs.getDBTypeByDBProfile(dbName);
		String jobName = "Query" + random +"-LUW3";
		/**
		 *  Select one sql statement 
		 */
		String sqlStatement = cs.listSQLStatementOfStagingTable(dbName, stagingTableSchema, stagingTableWorkloadId);
		
		JSONObject result = cs.submitJob(genDescSection(jobName, dbName), 
				genOQWTSection(jobName, schema, sqlStatement, dbName, dbType));
		
		int responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		
		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
		
		result = cs.runJob(jobId, dbName);
		responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		
		String resultCode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultCode));
		CheckJobStatus.checkJobStatus(jobId, 5000L);
		
		/*
		 * Delete job when successfully
		 */
		JSONObject delJobStatus = cs.getJobByJobID(jobId);
		String jobInstID = delJobStatus.getString("INSTID");
		JSONObject delStatus = cs.deleteJob(jobInstID);
		String delResultCode = delStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
	}
	
	
	public Properties genDescSection(String jobName,String dbName) {
		Properties desc = new Properties();
		desc.put("jobname", jobName);
		desc.put("jobType", "querytunerjobs");
		desc.put("schedenabled", 0);
		desc.put("mondbconprofile", dbName);
		desc.put("jobcreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}
	
	public Properties genOQWTSection(String jobName,String schema,String queryText,String dbName,String dbType) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("queryid", "");
		oqwt_SectionData.put("stmtId", "");
		oqwt_SectionData.put("tuningType", "SQL_BASED");
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("jobName", jobName);
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("returnAllStats", "OFF");
		oqwt_SectionData.put("queryName", jobName);
		oqwt_SectionData.put("resultName", " ");
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("reExplainValCheck", true);
		oqwt_SectionData.put("apgValCheck", true);
		oqwt_SectionData.put("formatQueryValCheck", true);
		oqwt_SectionData.put("iaValCheck", true);
		oqwt_SectionData.put("saValCheck", true);
		oqwt_SectionData.put("queryText", queryText);
		oqwt_SectionData.put("hashID", "");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		oqwt_SectionData.put("monitoredDbType", dbType);
		return oqwt_SectionData;
	}	
	
}






























