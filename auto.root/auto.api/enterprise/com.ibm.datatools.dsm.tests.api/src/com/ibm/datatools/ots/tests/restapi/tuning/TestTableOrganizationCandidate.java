package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;


/**
 * Case Type : LUW automation case
 * Case number on wiki : 12
 * Case description : Select one workload tuning job with Table Organization recommendations->View Results->
 * 					  in Table Organization result page click Test Candidate Indexes->
 * 					  select tables->click Run-> verify result
 * @author yinchen
 *
 */
public class TestTableOrganizationCandidate extends TuningTestBase{
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("TestTableOrganizationCandidate.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	@Test(dataProvider = "testdata")
	public void testCreateWorkloadJob(Object key,Object inputPara) throws FileNotFoundException, InterruptedException, IOException{
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		
		/**
		 * Create workload job with Table Organization recommendations
		 */
		String random = String.valueOf(System.currentTimeMillis()).substring(8, 12);
		String jobName = "Workload" + random + "-LUW12";
		String wccJobID = "Workload#" + random;
		//get the sql path from DSM server
		String sqlFileName = cs.uploadFile(obj.getString("sqlFileName"));
		
		String dbType = cs.getDBTypeByDBProfile(dbName);
		String sqlid = cs.getSQLIDByDBProfile(dbName);
		
		JSONObject result = cs.submitJob(genDescSection(jobName, dbName), 
				genOQWTSection(jobName, sqlFileName, schema, dbType, dbName, wccJobID, sqlid));
		
		
		int responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		
		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
		
		result = cs.runJob(jobId, dbName);
		responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		
		String resultCode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultCode));
		
		CheckJobStatus.checkJobStatus(jobId, 25000L);
		
		
		
		/**
		 *  Test candidate table organization
		 */
		String workloadid = wccJobID;
		result = cs.getCandidateTableOrganizationList(dbName, workloadid, schema);
		
		System.out.println(">>"+result);
		resultCode = result.getString("resultCode");
		
		if(result == null || !"SUCCESS".equals(resultCode)){
			String errorMessage = "CandidateTableOrganizationList result is null";
			Assert.fail(errorMessage);
			return;
		}
		
		String items = result.getString("items");
		if(items == null){
			String errorMessage = "No candidate table organization";
			Assert.fail(errorMessage);
			return;
		}
		
		
		JSONArray candidateTable = result.getJSONArray("items");
		if(candidateTable.size() == 0){
			String errorMessage = "No candidate table organization";
			Assert.fail(errorMessage);
			return;
		}
		
		String tablesJSON = null;
		for(int i=0; i<candidateTable.size(); i++){
			JSONObject objCanTable = candidateTable.getJSONObject(i);
			String TABLE = objCanTable.getString("TABLE");
			String CREATOR = objCanTable.getString("CREATOR");
			String CURRENTORGANIZATION = objCanTable.getString("CURRENTORGANIZATION");
			String CARDINALITY = objCanTable.get("CARDINALITY").toString();
			String TABLEREFERENCES = objCanTable.get("TABLEREFERENCES").toString();
			String TOTALCOST= objCanTable.get("TOTALCOST").toString();
			String IUDM = objCanTable.get("IUDM").toString();
			String TABLETYPE = objCanTable.getString("TABLETYPE");
			
			
			tablesJSON = "{\"list\":[{\"TABLENAME\" : \""+TABLE+"\",\"TABLESCHEMA\" : \""+CREATOR+"\",\"CURRENTORGANIZATION\" : \""+CURRENTORGANIZATION+"\",\"CARDINALITY\" : \""+CARDINALITY+"\",\"TABLEREFERENCES\" : \""+TABLEREFERENCES+"\",\"TOTALCOST\" : \""+TOTALCOST+"\",\"IUDM\" : \""+IUDM+"\",\"TABLETYPE\" : \""+TABLETYPE+"\"}]}";
			//tablesJSON = "{\"list\":[{\"TABLENAME\" : \"STORE\",\"TABLESCHEMA\" : \"TPCDS\",\"CURRENTORGANIZATION\" : \"ROW\",\"CARDINALITY\" : \"12\",\"TABLEREFERENCES\" : \"1\",\"TOTALCOST\" : \"966907\",\"IUDM\" : \"0\",\"TABLETYPE\" : \"Columnar\"},{\"TABLENAME\" : \"STORE_RETURNS\",\"TABLESCHEMA\" : \"TPCDS\",\"CURRENTORGANIZATION\" : \"ROW\",\"CARDINALITY\" : \"287514\",\"TABLEREFERENCES\" : \"1\",\"TOTALCOST\" : \"966907\",\"IUDM\" : \"0\",\"TABLETYPE\" : \"Columnar\"},{\"TABLENAME\" : \"DATE_DIM\",\"TABLESCHEMA\" : \"TPCDS\",\"CURRENTORGANIZATION\" : \"ROW\",\"CARDINALITY\" : \"73049\",\"TABLEREFERENCES\" : \"49\",\"TOTALCOST\" : \"2219995.94921875\",\"IUDM\" : \"0\",\"TABLETYPE\" : \"Columnar\"},{\"TABLENAME\" : \"CUSTOMER_DEMOGRAPHICS\",\"TABLESCHEMA\" : \"TPCDS\",\"CURRENTORGANIZATION\" : \"ROW\",\"CARDINALITY\" : \"1920800\",\"TABLEREFERENCES\" : \"2\",\"TOTALCOST\" : \"87859.14453125\",\"IUDM\" : \"0\",\"TABLETYPE\" : \"Columnar\"},{\"TABLENAME\" : \"CUSTOMER\",\"TABLESCHEMA\" : \"TPCDS\",\"CURRENTORGANIZATION\" : \"ROW\",\"CARDINALITY\" : \"100000\",\"TABLEREFERENCES\" : \"4\",\"TOTALCOST\" : \"1166576.390625\",\"IUDM\" : \"0\",\"TABLETYPE\" : \"Columnar\"}]}";
		}
		
		//tablesJSON= " ["+tablesJSON.substring(0, tablesJSON.length()-1)+"] ";
		
		
		//System.out.println(">>"+tablesJSON);
		
		String workloadName = wccJobID;
		result = cs.submitJob(gDescSection(jobName), 
				gOQWTSection(workloadName, dbName, tablesJSON, wccJobID, dbType));
		
		int candidateResCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, candidateResCode);
		
		String candidateJobId = result.getString("jobid");
		Assert.assertTrue(candidateJobId != null);
		
		result = cs.runJob(candidateJobId, dbName);
		candidateResCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, candidateResCode);
		
		String candidateResultCode = result.getString("resultcode");
		Assert.assertTrue("success".equals(candidateResultCode));
		
		CheckJobStatus.checkJobStatus(candidateJobId, 20000L);
		
		/**
		 * Delete job when run candidate job successfully
		 */
		List<String> jobInstID = new ArrayList<String>();
		JSONObject workloadJobStatus = cs.getJobByJobID(jobId);
		JSONObject candidateJobStatus = cs.getJobByJobID(candidateJobId);
		String workloadJobInstID = workloadJobStatus.getString("INSTID");
		String candidateJobInstID = candidateJobStatus.getString("INSTID");
		
		jobInstID.add(workloadJobInstID);
		jobInstID.add(candidateJobInstID);
		
		JSONObject delJobStatus = cs.deleteJobs(jobInstID);
		String delResultCode = delJobStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
		
	}
	
	public Properties genDescSection(String jobName,String dbName) {
		Properties desc = new Properties();
		desc.put("jobname", jobName);
		desc.put("jobType", "querytunerjobs");
		desc.put("schedenabled", 0);
		desc.put("mondbconprofile", dbName);
		desc.put("jobcreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}
	
	public Properties genOQWTSection(String jobName, String sqlFileName,
			String schema, String dbType,String dbName,String wccJobID,String sqlid) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("tuningType", "WORKLOAD");
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("workloadName", jobName);
		oqwt_SectionData.put("reExplainWorkloadValCheck", true);
		oqwt_SectionData.put("wsaValCheck", true);
		oqwt_SectionData.put("wiaValCheck", false);
		oqwt_SectionData.put("wtoValCheck", true);
		oqwt_SectionData.put("wsvaValCheck", true);
		oqwt_SectionData.put("wdaValCheck", false);
		oqwt_SectionData.put("SQLID", sqlid);
		oqwt_SectionData.put("wccJobID", wccJobID);
		oqwt_SectionData.put("wccJobStatus", "Running");
		oqwt_SectionData.put("sqlFileName", sqlFileName);
		oqwt_SectionData.put("stmtDelimiter", ";");
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("stmtDelim", ";");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbType", dbType);
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		return oqwt_SectionData;
	}
	
	
	public Properties gDescSection(String jobName) {
		Properties desc = new Properties();
		desc.put("jobname", jobName + "TestCandidate_TableOrganization" );
		desc.put("jobtype", "tctojob");
		desc.put("schedenabled", 0);
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}
								
	public Properties gOQWTSection(String workloadName,String dbName,String tablesJSON,
			String wccJobID,String dbType) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("tuningType", "TABLE-ORGANIZATION-ANALYSIS");
		oqwt_SectionData.put("workloadName", workloadName);
		oqwt_SectionData.put("profile", dbName);
		oqwt_SectionData.put("tablesJSON", tablesJSON);
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("schema", "");
		oqwt_SectionData.put("wccJobID", wccJobID);
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		oqwt_SectionData.put("monitoredDbName", dbName);
		oqwt_SectionData.put("monitoredDbType", dbType);
		oqwt_SectionData.put("jobCreator", "admin");
		return oqwt_SectionData;
	}
}

































