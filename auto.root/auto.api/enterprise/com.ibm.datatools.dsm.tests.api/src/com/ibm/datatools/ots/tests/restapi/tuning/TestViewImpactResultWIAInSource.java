package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;

/**
 * Case Type : ZOS automation case
 * Case number on wiki : 18
 * Case description  : Select one workload tuning job with WIA recommendations->View Results->
 * 					   in WIA result page click Impact analysis->set Job name->set SQLID->
 * 					   select Analyze statements in the following sources->
 * 					   select package and Dynamic Statement Cache ->		
 * 					   click OK->verify results
 * 					   
 * @author yinchen
 *
 */
public class TestViewImpactResultWIAInSource extends TuningTestBase{

	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("TestViewImpactResultWIA.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	@Test(dataProvider = "testdata")
	public void testCreateWorkloadJob(Object key,Object inputPara) throws InterruptedException, FileNotFoundException, IOException {
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		String sqlFileName = cs.uploadFile(obj.getString("sqlFileName"));
		String random = String.valueOf(System.currentTimeMillis());
		String jobName = "Workload" + random.substring(8, 12) + "-ZOS18";
		String wccJobID = "Workload_" + random;
		
		String dbType = cs.getDBTypeByDBProfile(dbName);
		String sqlid = cs.getSQLIDByDBProfile(dbName);
		String impactAnaschema = sqlid;
		
		JSONObject result = cs.submitJob(genDescSection(jobName,random,dbName), 
				genOQWTSection(random,dbType,schema,sqlFileName,dbName,wccJobID,sqlid));
		
		int responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
			
		result = cs.runJob(jobId, dbName);
		responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String resultcode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultcode));
				 
		CheckJobStatus.checkJobStatus(jobId, 10000L);
		/**
		 *Ma Wei
		 *Verify WIA
		*/
	
	    JSONObject resultDetails = cs.getJobByJobID(jobId);
		JSONObject jso = cs.getWorkloadJobDetails(dbName,  result.getString("jobid"), "Workload" + random.substring(8, 12) + "-ByRestAPI",resultDetails.getString("INSTID"));
		
		try {
			Object WIA_recommendation =  jso.get("wiaDDLsCreate");
			System.out.println("Here is WIA RECOMMENDATIONS : " + WIA_recommendation.toString());
			Assert.assertTrue( WIA_recommendation.toString().contains("INDEX"), "Verify the WIA can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify WIA error*******");
		}
		
		
				 
				 /**
				  * Run Impact Analysis
				  */
				 
		JSONObject impactAnaResult = cs.submitJob(genDescSection(jobName), 
						gOQWTSection(random,impactAnaschema,dbName,wccJobID,sqlid));
		int resCode = (Integer)impactAnaResult.get("ResponseCode");
		CheckJobStatus.checkResponseCode(impactAnaResult, resCode);
		String impactAnajobID = impactAnaResult.getString("jobid");
		Assert.assertTrue(impactAnajobID != null);
					 
		impactAnaResult = cs.runJob(impactAnajobID, dbName);
		resCode = (Integer)impactAnaResult.get("ResponseCode");
		CheckJobStatus.checkResponseCode(impactAnaResult, resCode);
		String impactAnaresultcode = impactAnaResult.getString("resultcode");
		Assert.assertTrue("success".equals(impactAnaresultcode));
						 
		CheckJobStatus.checkJobStatus(impactAnajobID, 30000L);
		/**
		 *Ma Wei
		 *Verify impact result
		*/
	
	    JSONObject resultDetails1 = cs.getJobByJobID(impactAnajobID);
		JSONObject jso1 = cs.getwhatifresult(dbName,  impactAnaResult.getString("jobid"), "WIA-ImpactAnalysis" + jobName,resultDetails1.getString("INSTID"));
		
		try {
			Object impact =  jso1.get("recommendations");
			System.out.println("Here is impact result : " + impact.toString());
			Assert.assertTrue(impact.toString().contains("improvemence"), "Verify the impact result can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify impact result error*******");
		}
		
		List<String> jobInstID = new ArrayList<String>();
		JSONObject workloadJobStatus = cs.getJobByJobID(jobId);
		String workloadJobInstID = workloadJobStatus.getString("INSTID");
		String impactAnaJobInstID = resultDetails1.getString("INSTID");
		
		jobInstID.add(workloadJobInstID);
		jobInstID.add(impactAnaJobInstID);
		
		JSONObject delStatus = cs.deleteJobs(jobInstID);
		String delResultCode = delStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
	}
	
	public Properties genDescSection(String jobName,String random,String dbName) {
		Properties desc = new Properties();
		desc.put("jobname", jobName);
		desc.put("jobType", "querytunerjobs");
		desc.put("schedenabled", 0);
		desc.put("mondbconprofile", dbName);
		desc.put("jobcreator", "admin");
		desc.put("jobid", "0");
		desc.put("jobdesc", "");
		desc.put("dbreqforjob", "1");
		return desc;
	}
	
	public Properties genOQWTSection(String random,String dbType,String schema,
			String sqlFileName,String dbName,String wccJobID,String sqlid) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("tuningType", "WORKLOAD");
		oqwt_SectionData.put("wccJobStatus", "Running");
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("workloadName", "Workload_" + random);
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("userDefinedRepoQualifier", "");
		oqwt_SectionData.put("userDefinedRepoName", "");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("wtaaValCheck", false);
		oqwt_SectionData.put("wsaValCheck", false);
		oqwt_SectionData.put("curDegreeValue", "ANY");
		oqwt_SectionData.put("reExplainWorkloadValCheck", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("sqlFileName", sqlFileName);
		oqwt_SectionData.put("curRefAgeValue", "ANY");
		oqwt_SectionData.put("wtaaModelSelect", "modeling");
		oqwt_SectionData.put("wccJobID", wccJobID);
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("stmtDelimiter", ";");
		oqwt_SectionData.put("curMQTValue", "ALL");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("curPathValue", "");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("wiaValCheck", true);
		oqwt_SectionData.put("SQLID", sqlid);
		oqwt_SectionData.put("monitoredDbType", dbType);
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		return oqwt_SectionData;
	}

	public Properties genDescSection(String jobName) {
		Properties desc = new Properties();
		desc.put("jobname", "WIA-ImpactAnalysis_" + jobName);
		desc.put("jobtype", "indeximpactjob");
		desc.put("schedenabled", 0);
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}
	
	public Properties gOQWTSection(String random,String impactAnaschema,String dbName,String recommJobname,String sqlid) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("tuningType", "IMPACT-ANALYSIS");
		oqwt_SectionData.put("workloadName", "iia-z#" + random);
		oqwt_SectionData.put("wccJobID", "iia-z#" + random);
		oqwt_SectionData.put("queryNodeID", "null");
		oqwt_SectionData.put("queryResultID", "null");
		oqwt_SectionData.put("profile", dbName);
		oqwt_SectionData.put("monitoreddbprofile", dbName);
		oqwt_SectionData.put("schema", impactAnaschema);
		oqwt_SectionData.put("recommJobname", recommJobname);
		oqwt_SectionData.put("sqlid", sqlid);
		oqwt_SectionData.put("ddltype", "AL");
		oqwt_SectionData.put("PKGANALYZING",true);
		oqwt_SectionData.put("cacheALYZING",true);
		return oqwt_SectionData;
	}
	
}































