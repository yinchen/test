package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;

/**
 * Case Type : ZOS automation case
 * Case number on wiki : 20
 * Case description : Select one WIA whatif tuning job ->View Results-> 
 * 					  click Compare Access Plan-> verify results
 * @author yinchen
 *
 */
public class TestWIAWhatIfCompareAccessPlan extends TuningTestBase{
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("TestWIAWhatIf.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	@Test(dataProvider = "testdata")
	public void testCreateWorkloadJob(Object key, Object inputPara) throws FileNotFoundException, InterruptedException, IOException{
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		String sch = obj.getString("sch");
		String disabledIndexesJSON = obj.getString("disabledIndexesJSON");
		
		String dbType = cs.getDBTypeByDBProfile(dbName);
		String sqlid = cs.getSQLIDByDBProfile(dbName);
		
		String random = String.valueOf(System.currentTimeMillis());
		String workloadName = "Workload_" + random;
		String jobname = "Workload" + random.substring(8, 12) + "-ZOS20";
		//get the sql path from DSM server
		String sqlFileName = cs.uploadFile(obj.getString("sqlFileName"));

		/**
		 * Create a workload job with WIA
		 */
		JSONObject result = cs.submitJob(
				genDescSection(random, dbName, jobname),
				genOQWTSection(random, sqlFileName, schema, dbName, dbType,
						workloadName, sqlid));

		int responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);

		/**
		 * Run workload job
		 */
		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
		result = cs.runJob(jobId, dbName);
		responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String resultcode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultcode));

		JSONObject status;
		status = cs.getJobStatus(jobId);
		CheckJobStatus.checkJobStatus(jobId, 10000L);
		
		
		status = cs.getJobStatus(jobId);
		System.out.println("<<" + status);
		/**
		 * Run candidate index
		 */
		System.out.println("status>>:" + status);
		String jobID = status.getString("JOBID");
		System.out.println("jobID:" + jobID);
		JSONObject r = cs.queryJob(dbName, jobID, workloadName);
		System.out.println("r>>" + r);
		JSONArray arryCandidateIndexes = r.getJSONArray("CANDIDATEINDEXES");

		if (arryCandidateIndexes.size() == 0) {
			String errorMessage = "No candidate indexes of current SQL statement,"
					+ "please check SQL statement or DB connection";
			Assert.fail(errorMessage);
			return;
		}

		String strIndex = null;
		for (int i = 0; i < arryCandidateIndexes.size(); i++) {
			JSONObject objCandidateIndexe = arryCandidateIndexes
					.getJSONObject(i);
			String keyColumns = objCandidateIndexe.getString("KEYCOLUMNS");
			String unique = objCandidateIndexe.getString("UNIQUE");
			String tabSchema = objCandidateIndexe.getString("TBSCHEMA");
			String dpsi = objCandidateIndexe.getString("DPSI");
			String ixname = objCandidateIndexe.getString("IXNAME");
			String isrecommended = objCandidateIndexe.get("ISRECOMMENDED")
					.toString();
			String tbname = objCandidateIndexe.getString("TBNAME");
			String action = objCandidateIndexe.getString("ACTION");
			String clustering = objCandidateIndexe.getString("CLUSTERING");
			String keytargetcount = objCandidateIndexe.get("KEYTARGETCOUNT")
					.toString();
			if (isrecommended.equals("true"))
				isrecommended = "Y";
			else
				isrecommended = "N";
			if (strIndex == null)
				strIndex = "{\"INDEXNAME\" : \"" + ixname
						+ "\",\"INDEXCREATOR\" : \"" + tabSchema
						+ "\",\"TABLENAME\" : \"" + tbname
						+ "\",\"TABLESCHEMA\" : \"" + tabSchema
						+ "\",\"KEYCOLUMNS\" : \"" + keyColumns
						+ "\",\"INCLUDECOLUMNS\" : \"\",\"UNIQUE\" : \""
						+ unique + "\",\"CLUSTERING\" : \"" + clustering
						+ "\",\"DPSI\" : \"" + dpsi + "\",\"CLUSTERING\" : \""
						+ clustering + "\",\"KEYTARGETCOUNT\" : \""
						+ keytargetcount + "\",\"ACTION\" : \"" + action
						+ "\",\"EXPCOLNOS\" : \"\",\"ISRECOMMEND\" : \""
						+ isrecommended + "\"},";
			else
				strIndex = strIndex + "{\"INDEXNAME\" : \"" + ixname
						+ "\",\"INDEXCREATOR\" : \"" + tabSchema
						+ "\",\"TABLENAME\" : \"" + tbname
						+ "\",\"TABLESCHEMA\" : \"" + tabSchema
						+ "\",\"KEYCOLUMNS\" : \"" + keyColumns
						+ "\",\"INCLUDECOLUMNS\" : \"\",\"UNIQUE\" : \""
						+ unique + "\",\"CLUSTERING\" : \"" + clustering
						+ "\",\"DPSI\" : \"" + dpsi + "\",\"CLUSTERING\" : \""
						+ clustering + "\",\"KEYTARGETCOUNT\" : \""
						+ keytargetcount + "\",\"ACTION\" : \"" + action
						+ "\",\"EXPCOLNOS\" : \"\",\"ISRECOMMEND\" : \""
						+ isrecommended + "\"},";
		}

		strIndex = " [" + strIndex.substring(0, strIndex.length() - 1) + "] ";

		/*
		 * Generate What-If Analysis job
		 */
		JSONObject candidateIndexResult = cs.submitJob(
				gDescSection(jobname),
				g(strIndex, workloadName, disabledIndexesJSON, jobID, sch,
						dbName, dbType));
		int resCode = (Integer) candidateIndexResult.get("ResponseCode");
		CheckJobStatus.checkResponseCode(candidateIndexResult, resCode);
		String indexJobId = candidateIndexResult.getString("jobid");
		Assert.assertTrue(indexJobId != null);

		JSONObject whatIfResult = cs.runJob(indexJobId, dbName);
		int whatIfResCode = (Integer) whatIfResult.get("ResponseCode");
		CheckJobStatus.checkResponseCode(whatIfResult, whatIfResCode);

		String whatIfResultcode = whatIfResult.getString("resultcode");
		Assert.assertTrue("success".equals(whatIfResultcode));

		CheckJobStatus.checkJobStatus(indexJobId, 5000L);

		/**
		 *Ma Wei
		 *Verify what-if result
		*/
	 
	    JSONObject resultDetails = cs.getJobByJobID(indexJobId);
	    JSONObject jso = cs.getZOSwhatifresult(dbName, whatIfResult.getString("jobid"),workloadName,resultDetails.getString("INSTID"));
	    
	    try {
	    	Object whatif_recommendation =  jso.get("whatIfRunResultSummary");
	    	System.out.println("Here is what-if result : " + whatif_recommendation.toString());
			Assert.assertTrue( whatif_recommendation.toString().contains("Estimated change in performance"), "Verify the what-if result can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify what-if result error*******");
		}
		
		
		JSONObject whatIfCompareResult = cs.submitJob(genDescSection(jobname), genOQWTSection(dbName, indexJobId));
		int whatIfCompareResponseCode = (Integer)whatIfCompareResult.get("ResponseCode");
		CheckJobStatus.checkResponseCode(whatIfCompareResult, whatIfCompareResponseCode);
		
		String whatIfCompareJobID = whatIfCompareResult.getString("jobid");
		Assert.assertTrue(whatIfCompareJobID != null);
		whatIfCompareResult = cs.runJob(whatIfCompareJobID, dbName);
		responseCode = (Integer)whatIfCompareResult.get("ResponseCode");
		CheckJobStatus.checkResponseCode(whatIfCompareResult, responseCode);
		String resultCode = whatIfCompareResult.getString("resultcode");
		Assert.assertTrue("success".equals(resultCode));
		
		CheckJobStatus.checkJobStatus(whatIfCompareJobID, 3000L);
		
		/**
		 *Ma Wei
		 *Verify what-if compare results
		*/
	     
	    JSONObject whatIfResultDetails = cs.getJobByJobID(indexJobId);
	    JSONObject whatIfJso = cs.getWorkloadcompareDetails(dbName, whatIfCompareResult.getString("jobid"),"WhatIfAnalysisAccessPlan_"+ jobname,whatIfResultDetails.getString("INSTID"));
	    
	    try {
	    	Object WIA_whatif_result =  whatIfJso.get("content");
	    	System.out.println("Here is what-if compare result : " + WIA_whatif_result.toString());
			Assert.assertTrue( WIA_whatif_result.toString().contains("Comparison of Access Plans"), "Verify the compare results can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify what-if compare results error*******");
		}
		
		/*
		 * Delete job when run case successfully
		 */
		List<String> jobInstID = new ArrayList<String>();
		String workloadJobInstID = status.getString("INSTID");
	    String whatIfJobInstID = resultDetails.getString("INSTID");
	    
	    JSONObject whatIfJobStatus = cs.getJobByJobID(whatIfCompareJobID);
	    String whatIfCompareJobInstID = whatIfJobStatus.getString("INSTID");
	    
	    jobInstID.add(workloadJobInstID);
	    jobInstID.add(whatIfJobInstID);
	    jobInstID.add(whatIfCompareJobInstID);
	    
	    JSONObject delStatus = cs.deleteJobs(jobInstID);
	    String delResultCode = delStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
	}
	
	/*
	 * Properties for workload job
	 */
	public Properties genDescSection(String random, String dbName,
			String jobname) {
		Properties desc = new Properties();
		desc.put("jobname", jobname);
		desc.put("jobType", "querytunerjobs");
		desc.put("schedenabled", 0);
		desc.put("mondbconprofile", dbName);
		desc.put("jobcreator", "admin");
		desc.put("jobid", "0");
		desc.put("jobdesc", "");
		desc.put("dbreqforjob", "1");
		return desc;
	}

	public Properties genOQWTSection(String random, String sqlFileName,
			String schema, String dbName, String dbType, String workloadName,
			String sqlid) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("monitoredDbType", dbType);
		oqwt_SectionData.put("licenseLabel",
				"The full set of tuning features is available.");
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("SQLID", sqlid);
		oqwt_SectionData.put("curRefAgeValue", "ANY");
		oqwt_SectionData.put("wccJobStatus", "Running");
		oqwt_SectionData.put("curMQTValue", "ALL");
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("curPathValue", "");
		oqwt_SectionData.put("oqwtLicenseType","The full set of tuning features is available.");
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("wtaaValCheck", false);
		oqwt_SectionData.put("captureMaxRowsCount", "100");
		oqwt_SectionData.put("userDefinedRepoQualifier", "");
		oqwt_SectionData.put("wiaValCheck", true);
		oqwt_SectionData.put("tuningType", "WORKLOAD");
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("wtaaModelSelect", "modeling");
		oqwt_SectionData.put("userDefinedRepoName", "");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("Select", "3");
		oqwt_SectionData.put("stmtDelimiter", ";");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("wsaValCheck", false);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("reExplainWorkloadValCheck", true);
		oqwt_SectionData.put("workloadName", workloadName);
		oqwt_SectionData.put("sqlFileName", sqlFileName);
		oqwt_SectionData.put("curDegreeValue", "ANY");
		oqwt_SectionData.put("wccJobID", workloadName);
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		return oqwt_SectionData;
	}
	
	/*
	 * Properties for what-if job
	 */
	public Properties gDescSection(String jobName) {
		Properties desc = new Properties();
		desc.put("jobname", "WhatIfAnalysis_" +jobName);
		desc.put("jobtype", "testcandidatejob");
		desc.put("schedenabled", 0);
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}

	public Properties g(String strIndex, String workloadName,
			String disabledIndexesJSON, String jobID, String sch,
			String dbName, String dbType) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("tuningType", "WHAT-IF-ANALYSIS");
		oqwt_SectionData.put("dbType", dbType);
		oqwt_SectionData.put("workloadName", workloadName);
		oqwt_SectionData.put("WTI_JOB_ID", jobID);
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		oqwt_SectionData.put("sqlid", "SYSADM");
		oqwt_SectionData.put("indexesJSON", strIndex);
		oqwt_SectionData.put("disabledIndexesJSON", disabledIndexesJSON);
		oqwt_SectionData.put("schema", sch);
		return oqwt_SectionData;
	}
	
	/*
	 * Properties for workload access plan job
	 */
	
	public Properties genDescSection(String jobName) {
		Properties desc = new Properties();
		desc.put("jobname", "WhatIfAnalysisAccessPlan_"+ jobName);
		desc.put("jobtype", "wapcjob");
		desc.put("schedenabled", 0);
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}
								
	public Properties genOQWTSection(String dbName,String jobId) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("tuningType", "WORKLOAD-ACCESS-PLAN-COMPARISON");
		oqwt_SectionData.put("dbProfile", dbName);
		oqwt_SectionData.put("jobId", jobId);
		oqwt_SectionData.put("model", "whatif");
		return oqwt_SectionData;
	}
	
}























