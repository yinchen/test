package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;

/**
 * Case Type : ZOS automation case
 * Case number on wiki : 32
 * Case description : Select one workload tuning job-> ViewResults-> Tuning Job Log->
 * 					  Capture Workload Environment-> Click OK->Verify capture result
 * @author yinchen
 *
 */
public class TestWorkloadCapEnv extends TuningTestBase{

	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("TuneWorkloadJob.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	@Test(dataProvider = "testdata")
	public void testCreateWorkloadJob(Object key,Object inputPara) throws InterruptedException, FileNotFoundException, IOException {
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		String sqlFileName = cs.uploadFile(obj.getString("sqlFileName"));
		String random = String.valueOf(System.currentTimeMillis());
		String wccJobID = "workload#" + random;
		
		String dbType = cs.getDBTypeByDBProfile(dbName);
		String sqlid = cs.getSQLIDByDBProfile(dbName);
		
		String jobName = "Workload" + random.substring(8, 12) +"-ZOS32";
		JSONObject result = cs.submitJob(genDescSection(jobName,dbName), genOQWTSection(random,sqlFileName,schema,dbType,dbName,wccJobID,sqlid));
		
		int responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
			
		result = cs.runJob(jobId, dbName);
		responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String resultcode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultcode));
		
		CheckJobStatus.checkJobStatus(jobId, 8000L);
				 
		//result = cs.deleteJob(status.getString("INSTID"));
				 
				 
		JSONObject workloadCapResult = cs.submitJob(gDescSection(jobName), gOQWTSection(random, dbName, wccJobID, jobId));
		int capResCode = (Integer)workloadCapResult.get("ResponseCode");
		CheckJobStatus.checkResponseCode(workloadCapResult, capResCode);
		String capJobID = workloadCapResult.getString("jobid");
		workloadCapResult = cs.runJob(capJobID, dbName);
		capResCode = (Integer)workloadCapResult.get("ResponseCode");
		CheckJobStatus.checkResponseCode(workloadCapResult, capResCode);
		CheckJobStatus.checkJobStatus(capJobID, 3000L);
		
		/*
		 * Delete job when run job successfully
		 */
		List<String> jobInstID = new ArrayList<String>();
		JSONObject workloadJobStatus = cs.getJobByJobID(jobId);
		JSONObject capJobStatus = cs.getJobByJobID(capJobID);
		
		String workloadJobInstID = workloadJobStatus.getString("INSTID");
		String capJobInstID = capJobStatus.getString("INSTID");
		
		jobInstID.add(workloadJobInstID);
		jobInstID.add(capJobInstID);
		
		JSONObject delJobStatus = cs.deleteJobs(jobInstID);
		String delResultCode = delJobStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
		
		
	}
	
	public Properties genDescSection(String jobName,String dbName) {
		Properties desc = new Properties();
		desc.put("jobname", jobName);
		desc.put("jobType", "querytunerjobs");
		desc.put("schedenabled", 0);
		desc.put("mondbconprofile", dbName);
		desc.put("jobcreator", "admin");
		desc.put("jobid", "0");
		desc.put("jobdesc", "");
		desc.put("dbreqforjob", "1");
		return desc;
	}
	
	public Properties genOQWTSection(String random, String sqlFileName,
			String schema, String dbType,String dbName,String wccJobID,String sqlid) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("tuningType", "WORKLOAD");
		oqwt_SectionData.put("wccJobStatus", "Running");
		oqwt_SectionData.put("sqlFileName", sqlFileName);
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("workloadName", "Workload_" + random);
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("wtaaValCheck", false);
		oqwt_SectionData.put("wsaValCheck", true);
		oqwt_SectionData.put("curDegreeValue", "ANY");
		oqwt_SectionData.put("reExplainWorkloadValCheck", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("curRefAgeValue", "ANY");
		oqwt_SectionData.put("stmtDelimiter", ";");
		oqwt_SectionData.put("wtaaModelSelect", "modeling");
		oqwt_SectionData.put("wccJobID", wccJobID);
		oqwt_SectionData.put("dbconfigured", true);
		oqwt_SectionData.put("curMQTValue", "ALL");
		oqwt_SectionData.put("curSysTimeValue", "NULL");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("curBusiTimeValue", "NULL");
		oqwt_SectionData.put("curPathValue", "");
		oqwt_SectionData.put("curGetArchiveValue", "Y");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("wiaValCheck", true);
		oqwt_SectionData.put("SQLID", sqlid);
		//oqwt_SectionData.put("monitoredDbName", dbName);
		oqwt_SectionData.put("monitoredDbType", dbType);
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		return oqwt_SectionData;
	}

	
	public Properties gDescSection(String jobName) {
		Properties desc = new Properties();
		desc.put("jobname", "Capture_" + jobName);
		desc.put("jobtype", "captureEnvjob");
		desc.put("schedenabled", 0);
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}

	public Properties gOQWTSection(String random,String dbName,String workloadName,String wccJobID) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("tuningType", "Capture Environment");
		oqwt_SectionData.put("workloadName", workloadName);
		oqwt_SectionData.put("wccJobID", wccJobID);
		oqwt_SectionData.put("queryResultID", -1);
		oqwt_SectionData.put("profile", dbName);
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		return oqwt_SectionData;
	}
	
}































