package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;

public class TestWorkloadQueryAdvisor extends TuningTestBase {
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("QueryAdvisor.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}

	@Test(dataProvider = "testdata")
	public void TestWorkloadQueryAdvisor(Object key, Object inputPara)
			throws FileNotFoundException, InterruptedException, IOException {
		
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		String sqltext = obj.getString("sqltext");
		String random = String.valueOf(System.currentTimeMillis());
		String jobName =  "WorkloadQueryAdvisor_" + random.substring(8, 12) + "-ByRestAPI";
		String wccJobID = jobName + "#" + random;

		String dbType = cs.getDBTypeByDBProfile(dbName);
		String sqlid = cs.getSQLIDByDBProfile(dbName);

		// get the sql path from DSM server
		//String sqlFileName = cs.uploadFile(obj.getString("sqlFileName"));

		JSONObject result = cs.submitJob(genDescSection(jobName), genOQWTSection(jobName, sqltext, schema,
				dbType, dbName, wccJobID, sqlid));
		int responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);

		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);

		result = cs.runJob(jobId, dbName);
		responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String resultCode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultCode));

		CheckJobStatus.checkJobStatus(jobId, 10000L);

		/**
		 * Ma Wei Verify WSA Verify WSVA
		 */

		JSONObject resultDetails = cs.getJobByJobID(jobId);

		JSONObject jso = cs.getWorkloadJobDetails(dbName, result.getString("jobid"), jobName,
				resultDetails.getString("INSTID"));

		try {
			Object wsa_recommendation = jso.get("wsaDDLs");
			Object stmts = jso.get("wqaStatements");
			Object summary = jso.get("wqaSummary");
			System.out.println("Here is wqa summary : " + summary.toString());
			Assert.assertTrue(summary.toString().contains("STMTS_WARNINGS"),
					"Verify the wqa summary can be showed correctly");
			System.out.println("Here is wqa statements : " + stmts.toString());
			Assert.assertTrue(stmts.toString().contains("STMTNOI"),
					"Verify the wqa statements can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify WQA error*******");
		}

		/*
		 * Delete job when check verification point successfully
		 */
		JSONObject delStatus = cs.deleteJob(resultDetails.getString("INSTID"));
		String delResultCode = delStatus.getString("resultcode");
		if ("success".equals(delResultCode)) {
			System.out.println("Delete job successfully");
		} else {
			Assert.fail("Delete job failure");
		}
	}

	public Properties genDescSection(String jobName) {
		Properties desc = new Properties();
		desc.put("jobname", jobName);
		desc.put("jobType", "querytunerjobs");
		desc.put("schedenabled", 0);
		desc.put("mondbconprofile", dbName);
		desc.put("jobcreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}

	public Properties genOQWTSection(String jobName, String sqltext, String schema, String dbType, String dbName,
			String wccJobID, String sqlid) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("tuningType", "WORKLOAD");
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("workloadName", jobName);
		oqwt_SectionData.put("reExplainWorkloadValCheck", true);
		oqwt_SectionData.put("wsaValCheck", false);
		oqwt_SectionData.put("wiaValCheck", false);
		oqwt_SectionData.put("wtoValCheck", false);
		oqwt_SectionData.put("wsvaValCheck", false);
		oqwt_SectionData.put("wdaValCheck", false);
		oqwt_SectionData.put("wapaValCheck", false);
		oqwt_SectionData.put("wqaValCheck", true);
		oqwt_SectionData.put("SQLID", sqlid);
		oqwt_SectionData.put("wccJobID", wccJobID);
		oqwt_SectionData.put("wccJobStatus", "Running");
		oqwt_SectionData.put("sqlList", sqltext);
		oqwt_SectionData.put("stmtDelimiter", ";");
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("stmtDelim", ";");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbType", dbType);
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		return oqwt_SectionData;
	}
}
