package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;

/**
 * Case Type : ZOS automation case
 * Case number on wiki : 23
 * Case description : Select one workload tuning job->View Results->click View Workload Statement->
 * 					  click Refine->set options->click Next->click Next->
 * 					  set explain options->click Done-> verify all advisors result
 * @author yinchen
 *
 */
public class TestWorkloadRefine extends TuningTestBase{
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("TestWorkloadCompare.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	@Test(dataProvider = "testdata")
	public void testCreateWorkloadJob(Object key,Object inputPara) throws FileNotFoundException, InterruptedException, IOException{
		
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		String random = String.valueOf(System.currentTimeMillis());
		String wccJobID = "Workload#" + random;
		
		String sqlFileName = cs.uploadFile(obj.getString("sqlFileName"));
		
		String jobName = "Workload" + random.substring(8, 12) + "-ZOS23";
		
		/**
		 * Generate a workload job
		 */
		String dbType = cs.getDBTypeByDBProfile(dbName);
		String sqlid = cs.getSQLIDByDBProfile(dbName);
		JSONObject result = cs.submitJob(gDescSection(jobName, dbName), genOQWTSection(jobName, sqlFileName, schema, dbType, dbName, wccJobID, sqlid));
		
		int responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		
		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
		
		result = cs.runJob(jobId, dbName);
		responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String resultCode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultCode));
		
		CheckJobStatus.checkJobStatus(jobId, 10000L);
		
		JSONObject workloadJob = cs.getJobByJobID(jobId);
		
		String workloadJobID = workloadJob.getString("JOBID");
		
		String workloadName = "Refine_"+jobName;
		System.out.println(workloadName);
		
		/**
		 *  Refine API : retrieveRefinedWorkload
		 */
		JSONObject refineJobResult = cs.refineWorkloadJob(dbName, workloadJobID);
		System.out.println(">>"+refineJobResult);
		String refineWccJobID = refineJobResult.getString("NEW_WORKLOAD_NAME");
		
		result = cs.submitJob(genDescSection(workloadName,random,dbName), 
				genOQWTSection(random, workloadName, refineWccJobID,dbName, dbType, sqlid));
		responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String refineJobId = result.getString("jobid");
		Assert.assertTrue(refineJobId != null);
		
		result = cs.runJob(refineJobId, dbName);
		responseCode = (Integer)result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		resultCode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultCode));
		
		CheckJobStatus.checkJobStatus(refineJobId, 15000L);
		
		/**
		 *Ma Wei
		 *Verify WSA
		 *Verify WIA
		*/
	
	    JSONObject resultDetails = cs.getJobByJobID(workloadJobID);
		JSONObject jso = cs.getWorkloadJobDetails(dbName,  result.getString("jobid"), workloadName +"-Refine"+random,resultDetails.getString("INSTID"));
		
		try {
			Object wsa_recommendation =  jso.get("wsaDDLs");
			System.out.println("Here is WSA RECOMMENDATIONS : " + wsa_recommendation.toString());
			Assert.assertTrue(wsa_recommendation.toString().contains("RUNSTATS"), "Verify the wsa can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify WSA error*******");
		}
		
		try {
			Object WIA_recommendation =  jso.get("wiaDDLsCreate");
			System.out.println("Here is WIA RECOMMENDATIONS : " + WIA_recommendation.toString());
			Assert.assertTrue( WIA_recommendation.toString().contains("INDEX"), "Verify the WIA can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify WIA error*******");
		}
		
		/*
		 * Delete job when check verification point successfully
		 */
		List<String> jobInstID = new ArrayList<String>();
		String workloadJobInstID = workloadJob.getString("INSTID");
		
		JSONObject refineJobStatus = cs.getJobByJobID(refineJobId);
		String refineJobInstID = refineJobStatus.getString("INSTID");
		
		jobInstID.add(workloadJobInstID);
		jobInstID.add(refineJobInstID);
		
		JSONObject delJobStatus = cs.deleteJobs(jobInstID);
		String delResultCode = delJobStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
	}
	
	public Properties gDescSection(String jobName,String dbName) {
		Properties desc = new Properties();
		desc.put("jobname", jobName);
		desc.put("jobType", "querytunerjobs");
		desc.put("schedenabled", 0);
		desc.put("mondbconprofile", dbName);
		desc.put("jobcreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}
	
	public Properties genOQWTSection(String jobName, String sqlFileName,
			String schema, String dbType,String dbName,String wccJobID,String sqlid) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("tuningType", "WORKLOAD");
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("workloadName", jobName);
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("reExplainWorkloadValCheck", true);
		oqwt_SectionData.put("wsaValCheck", true);
		oqwt_SectionData.put("wiaValCheck", true);
		oqwt_SectionData.put("wtaaValCheck", false);
		oqwt_SectionData.put("wtaaModelSelect", "modeling");
		oqwt_SectionData.put("curPathValue", "");
		oqwt_SectionData.put("curDegreeValue", "ANY");
		oqwt_SectionData.put("curRefAgeValue", "ANY");
		oqwt_SectionData.put("curMQTValue", "ALL");
		oqwt_SectionData.put("curBusiTimeValue", "NULL");
		oqwt_SectionData.put("curSysTimeValue", "NULL");
		oqwt_SectionData.put("curGetArchiveValue", "Y");
		oqwt_SectionData.put("SQLID", sqlid);
		oqwt_SectionData.put("Select", "3");
		oqwt_SectionData.put("wccJobID", wccJobID);
		oqwt_SectionData.put("wccJobStatus", "Running");
		oqwt_SectionData.put("sqlFileName", sqlFileName);
		oqwt_SectionData.put("stmtDelimiter", ";");
		oqwt_SectionData.put("schema", schema);
		oqwt_SectionData.put("stmtDelim", ";");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbType", dbType);
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		return oqwt_SectionData;
	}
	
	
	public Properties genDescSection(String workloadName,String random,String dbName ) {
		Properties desc = new Properties();
		desc.put("jobname", workloadName);
		desc.put("jobtype", "querytunerjobs");
		desc.put("schedenabled", 0);
		desc.put("monDbConProfile", dbName);
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}
	
	public Properties genOQWTSection(String random,String workloadName,String wccJobID,String dbName,String dbType,String sqlid) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", "true");
		oqwt_SectionData.put("tuningType", "WORKLOAD");
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("workloadName", workloadName);
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("reExplainWorkloadValCheck", true);
		oqwt_SectionData.put("wsaValCheck", true);
		oqwt_SectionData.put("wiaValCheck", true);
		oqwt_SectionData.put("wtaaValCheck", false);
		oqwt_SectionData.put("wtaaModelSelect", "modeling");
		oqwt_SectionData.put("curPathValue", "");
		oqwt_SectionData.put("curDegreeValue", "ANY");
		oqwt_SectionData.put("curRefAgeValue", "ANY");
		oqwt_SectionData.put("curMQTValue", "ALL");
		oqwt_SectionData.put("curBusiTimeValue", "NULL");
		oqwt_SectionData.put("curSysTimeValue", "NULL");
		oqwt_SectionData.put("curGetArchiveValue", "Y");  
		oqwt_SectionData.put("SQLID", sqlid);
		oqwt_SectionData.put("Select", "3");
		oqwt_SectionData.put("wccJobID", wccJobID);
		oqwt_SectionData.put("wccJobStatus", "Running");
		oqwt_SectionData.put("schema", "");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		oqwt_SectionData.put("monitoredDbType", dbType);
		return oqwt_SectionData;
	}
	
	
}










