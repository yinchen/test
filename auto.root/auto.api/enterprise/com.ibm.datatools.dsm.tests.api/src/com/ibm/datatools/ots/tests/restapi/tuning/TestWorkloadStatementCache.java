package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;

/**
 * Case Type : ZOS automation case
 * Case number on wiki : 11
 * Case description : Optimize->Start Tuning->select SQL statement source: Statement Cache->
 * 					  set Maximum number of statements to capture->set SQLID->set filters->click Next->
 * 					  click Tune All Statements->set Job name->set SQLID->set Explain options->
 * 					  select WSA, WIA with fine tuning ->click Done->verify advisors result(Report, WSA, WIA)
 * @author yinchen
 *
 */
public class TestWorkloadStatementCache extends TuningTestBase{

	
	@Test
	public void testCreateWorkloadJob() throws InterruptedException, FileNotFoundException, IOException {
		
		String dbType = cs.getDBTypeByDBProfile(dbName);
		String sqlid = cs.getSQLIDByDBProfile(dbName);
		
		//Set sql statement account
		String sqlCount = "20";
		
		String random = String.valueOf(System.currentTimeMillis());
		JSONObject result = cs.submitJob(genDescSection(random,dbName), genOQWTSection(random,dbName,dbType,sqlid,sqlCount));
		
		int responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
			
		result = cs.runJob(jobId, dbName);
		responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String resultcode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultcode));
				 
		CheckJobStatus.checkJobStatus(jobId, 10000L);
				 
		
		/**
		 *Ma Wei
		 *Verify WSA
		 *Verify WIA
		*/
	
	    JSONObject resultDetails = cs.getJobByJobID(jobId);
		JSONObject jso = cs.getWorkloadJobDetails(dbName,  result.getString("jobid"), "Workload" + random.substring(8, 12) + "-ZOS11",resultDetails.getString("INSTID"));
		
		try {
			Object wsa_recommendation =  jso.get("wsaDDLs");
			System.out.println("Here is WSA RECOMMENDATIONS : " + wsa_recommendation.toString());
			Assert.assertTrue(wsa_recommendation.toString().contains("RUNSTATS"), "Verify the wsa can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify WSA error*******");
		}
		
		try {
			Object WIA_recommendation =  jso.get("wiaDDLsCreate");
			System.out.println("Here is WIA RECOMMENDATIONS : " + WIA_recommendation.toString());
			Assert.assertTrue( WIA_recommendation.toString().contains("INDEX"), "Verify the WIA can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify WIA error*******");
		}
		
		/*
		 * Delete job when check verification point successfully
		 */
		JSONObject delStatus = cs.deleteJob(resultDetails.getString("INSTID"));
		String delResultCode = delStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
		
	}
	
	public Properties genDescSection(String random,String dbName ) {
		Properties desc = new Properties();
		desc.put("jobname", "Workload" + random.substring(8, 12) + "-ZOS11");
		desc.put("jobtype", "querytunerjobs");
		desc.put("schedenabled", 0);
		desc.put("monDbConProfile", dbName);
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}
	
	public Properties genOQWTSection(String random,String dbName,String dbType,String sqlid,String sqlCount) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("queryText", "");
		oqwt_SectionData.put("tuningType", "WORKLOAD");
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("workloadName", "Workload_" + random);
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("wsaValCheck", true);
		oqwt_SectionData.put("wiaValCheck", true);
		oqwt_SectionData.put("wtaaValCheck", false);
		oqwt_SectionData.put("wtaaModelSelect", "modeling");
		oqwt_SectionData.put("reExplainWorkloadValCheck", true);
		oqwt_SectionData.put("SQLID", sqlid);
		oqwt_SectionData.put("Select", "3");
		oqwt_SectionData.put("wccJobID", "workload#" + random);
		oqwt_SectionData.put("wccJobStatus", "Running");
		oqwt_SectionData.put("captureMaxRowsCount", sqlCount);
		oqwt_SectionData.put("stmtCacheTableQualifier", sqlid);
		oqwt_SectionData.put("CACHE_EXCLUDE_SYSTEM_STMT", true);
		oqwt_SectionData.put("type", "CACHE");
		oqwt_SectionData.put("dataSharingMembers", "[]");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		oqwt_SectionData.put("monitoredDbType", dbType);
		return oqwt_SectionData;
	}
	
}































