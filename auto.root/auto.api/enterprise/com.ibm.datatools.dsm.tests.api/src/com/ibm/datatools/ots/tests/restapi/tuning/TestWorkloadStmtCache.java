package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.ots.tests.restapi.base.CheckMonitorDB;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.RestAPIBaseTest;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;

/**
 * Case Type : ZOS automation case
 * Case number on wiki : 29
 * Case description : Optimize->Start Tuning->select db V12NFM->select statement cache->select STABILIZED=Y->
 * 					  click NEXT->select tune all statements->select WSA, WIA ,IDAA, input accename OQWTWAAA->
 * 					  verify there are recommendations in WSA, WIA ,IDAA. no exception.
 * @author yinchen
 *
 */
public class TestWorkloadStmtCache {

	private final Logger logger = LogManager.getLogger(getClass());
	TuningCommandService cs;
	Properties p;
	
	String dbName = null;
	@Parameters("dbProfile")
	@BeforeTest
	public void beforeTest(String monitorDBName) throws FileNotFoundException, IOException {
		cs = new TuningCommandService("TestWorkloadStmtCache.properties");
		RestAPIBaseTest.loginDSM();
		
		dbName = monitorDBName;
		Assert.assertEquals(CheckMonitorDB.checkMonitorDBStatus(dbName), "success");
	}
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() {
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	@Test(dataProvider = "testdata")
	public void testCreateWorkloadJob(Object key,Object inputPara) throws InterruptedException, FileNotFoundException, IOException {
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		String wtaaAccelName = obj.getString("wtaaAccelName");
		String dbType = cs.getDBTypeByDBProfile(dbName);
		String random = String.valueOf(System.currentTimeMillis());
		JSONObject result = cs.submitJob(genDescSection(random,dbName), genOQWTSection(random,dbName,dbType,wtaaAccelName));
		
		int responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
			
		result = cs.runJob(jobId, dbName);
		responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String resultcode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultcode));
		
		CheckJobStatus.checkJobStatus(jobId, 5000L);
				 
		//result = cs.deleteJob(status.getString("INSTID"));
	}
	
	public Properties genDescSection(String random,String dbName ) {
		Properties desc = new Properties();
		desc.put("jobname", "Workload_" + random.substring(8, 12) + "-ByRestAPI");
		desc.put("jobtype", "querytunerjobs");
		desc.put("schedenabled", 0);
		desc.put("monDbConProfile", dbName);
		desc.put("jobCreator", "admin");
		desc.put("jobid", "0");
		desc.put("dbreqforjob", "1");
		desc.put("jobdesc", "");
		return desc;
	}
	
	public Properties genOQWTSection(String random,String dbName,String dbType,String wtaaAccelName) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("tuningType", "WORKLOAD");
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("workloadName", "Workload_" + random);
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("reExplainWorkloadValCheck", true);
		oqwt_SectionData.put("wsaValCheck", true);
		oqwt_SectionData.put("wiaValCheck", true);
		oqwt_SectionData.put("wtaaValCheck", true);
		oqwt_SectionData.put("wtaaModelSelect", "virtual");
		oqwt_SectionData.put("wtaaAccelName", wtaaAccelName);
		oqwt_SectionData.put("wtaaModelSelect", "virtual");
		oqwt_SectionData.put("curPathValue", "");
		oqwt_SectionData.put("curDegreeValue", "ANY");
		oqwt_SectionData.put("curRefAgeValue", "ANY");
		oqwt_SectionData.put("curMQTValue", "ALL");
		oqwt_SectionData.put("curBusiTimeValue", "NULL");
		oqwt_SectionData.put("curSysTimeValue", "NULL");
		oqwt_SectionData.put("curGetArchiveValue", "Y");  
		oqwt_SectionData.put("SQLID", "SYSADM");
		oqwt_SectionData.put("wccJobID", "workload#" + random);
		oqwt_SectionData.put("wccJobStatus", "Running");
		oqwt_SectionData.put("stmtCacheTableQualifier", "SYSADM");
		oqwt_SectionData.put("type", "CACHE");
		oqwt_SectionData.put("STABILIZED", "=@@Y");
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		oqwt_SectionData.put("monitoredDbType", dbType);
		return oqwt_SectionData;
	}
	
}































