package com.ibm.datatools.ots.tests.restapi.tuning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.CheckJobStatus;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningTestBase;
import com.ibm.datatools.test.utils.FileTools;


/**
 * Case Type : ZOS automation case
 * Case number in wiki : 12
 * Case description : Optimize->Start Tuning->select SQL statement source: Catalog Plan or Package->
 * 					  set Maximum number of statements to capture->select capture statements that match criteria for packages->
 * 					  set filters->click Next->click Tune All Statements->set Job name->set SQLID->set Explain options->
 * 					  select select WSA, WIA with Remove obsolete indexes->click Done->verify advisors result(Report, WSA, WIA)
 * @author yinchen
 *
 */
public class Test_UT_CreateWK_PKG_WIA_HC extends TuningTestBase{

	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("Test_UT_CreateWK_PKG_WIA_HC.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}
	
	@Test(dataProvider = "testdata")
	public void testCreateWorkloadJob(Object key,Object inputPara) throws InterruptedException, FileNotFoundException, IOException {
		logger.info("Test data key:" + key);
		JSONObject obj = JSONObject.fromObject(inputPara);
		String NAME = obj.getString("NAME");
		
		String dbType = cs.getDBTypeByDBProfile(dbName);
		String sqlid = cs.getSQLIDByDBProfile(dbName);
		
		String random = String.valueOf(System.currentTimeMillis());
		JSONObject result = cs.submitJob(genDescSection(random), genOQWTSection(random,NAME,dbType,sqlid));
		
		int responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String jobId = result.getString("jobid");
		Assert.assertTrue(jobId != null);
			
		result = cs.runJob(jobId, dbName);
		responseCode = (Integer) result.get("ResponseCode");
		CheckJobStatus.checkResponseCode(result, responseCode);
		String resultcode = result.getString("resultcode");
		Assert.assertTrue("success".equals(resultcode));
		
		CheckJobStatus.checkJobStatus(jobId, 5000L);
				 
		
		/**
		 *Ma Wei
		 *Verify WSA
		 *Verify WIA
		*/
	
	    JSONObject resultDetails = cs.getJobByJobID(jobId);
		JSONObject jso = cs.getWorkloadJobDetails(dbName,  result.getString("jobid"), "Workload" + random.substring(8, 12) + "-ByRestAPI",resultDetails.getString("INSTID"));
		
		try {
			Object wsa_recommendation =  jso.get("wsaDDLs");
			System.out.println("Here is WSA RECOMMENDATIONS : " + wsa_recommendation.toString());
			Assert.assertTrue(wsa_recommendation.toString().contains("RUNSTATS"), "Verify the wsa can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify WSA error*******");
		}

		try {
			Object WIA_recommendation =  jso.get("wiaDDLsDrop");
			System.out.println("Here is WIA RECOMMENDATIONS : " + WIA_recommendation.toString());
			Assert.assertTrue( WIA_recommendation.toString().contains("DROP INDEX"), "Verify the WIA can be showed correctly");
		} catch (Exception e) {
			Assert.fail("*******Verify WIA error*******");
		}
		
		
		/*
		 * Delete job when check verification point successfully
		 */
		JSONObject delStatus = cs.deleteJob(resultDetails.getString("INSTID"));
		String delResultCode = delStatus.getString("resultcode");
		if("success".equals(delResultCode)){
			System.out.println("Delete job successfully");
		}else{
			Assert.fail("Delete job failure");
		}
	}
	
	public Properties genDescSection(String random) {
		Properties desc = new Properties();
		desc.put("jobname", "Workload" + random.substring(8, 12) + "-ZOS12");
		desc.put("jobType", "querytunerjobs");
		desc.put("schedenabled", 0);
		desc.put("mondbconprofile", dbName);
		desc.put("jobcreator", "admin");
		desc.put("jobid", "0");
		desc.put("jobdesc", "");
		desc.put("dbreqforjob", "1");
		desc.put("DBLOGINUSER", "AUTOD01");
		return desc;
	}
	
	public Properties genOQWTSection(String random, String NAME,String dbType,String sqlid) {
		Properties oqwt_SectionData = new Properties();
		oqwt_SectionData.put("retune", "false");
		oqwt_SectionData.put("queryText", "");
		oqwt_SectionData.put("tuningType", "WORKLOAD");
		oqwt_SectionData.put("isNewF", true);
		oqwt_SectionData.put("ISNEWTUNE", true);
		oqwt_SectionData.put("workloadName", "Workload_" + random);
		oqwt_SectionData.put("desc", "");
		oqwt_SectionData.put("wsaValCheck", true);
		oqwt_SectionData.put("wiaValCheck", true);
		oqwt_SectionData.put("wtaaValCheck", false);
		oqwt_SectionData.put("wtaaModelSelect", "modeling");
		oqwt_SectionData.put("reExplainWorkloadValCheck", true);
		oqwt_SectionData.put("SQLID", sqlid);
		oqwt_SectionData.put("Select", "3");
		oqwt_SectionData.put("wccJobID", "workload#" + random);
		oqwt_SectionData.put("wccJobStatus", "Running");
		oqwt_SectionData.put("captureMaxRowsCount", "10");
		oqwt_SectionData.put("type", "CATALOG");
		oqwt_SectionData.put("catalogType", "CATALOG.PACKAGE");
		oqwt_SectionData.put("FILTER_KEYS", NAME);
		oqwt_SectionData.put("rid", "");
		oqwt_SectionData.put("tuningCtx", "STANDALONE");
		oqwt_SectionData.put("dbconfigured", "1");
		oqwt_SectionData.put("oqwtLicenseType", "The full set of tuning features is available.");
		oqwt_SectionData.put("licenseLabel", "The full set of tuning features is available.");
		oqwt_SectionData.put("monitoredDbType", dbType);
		oqwt_SectionData.put("monitoredDbProfile", dbName);
		
		return oqwt_SectionData;
	}
	
}































