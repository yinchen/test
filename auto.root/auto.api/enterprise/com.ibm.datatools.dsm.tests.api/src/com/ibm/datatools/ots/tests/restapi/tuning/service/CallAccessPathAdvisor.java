package com.ibm.datatools.ots.tests.restapi.tuning.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.test.utils.JSONUtils;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningServiceAPITestBase;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class CallAccessPathAdvisor extends TuningServiceAPITestBase{
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("CallTuningServiceAPI.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}

	@Test(dataProvider = "testdata")
	public void callAPAAPI(Object key, Object inputPara) {
		JSONObject obj = JSONObject.fromObject(inputPara);
		String queryText = obj.getString("queryText");
		String url = DSMURLUtils.URL_PREFIX + Configuration.getProperty("tuningservice_AccessPathAdvisor");
		JSONObject result = cs.callTuningServiceAPI(url, prepareInputParameters(queryText));
		
		//verify result
		String code = result.get("code").toString();
		Assert.assertTrue("0".equals(code));
		String output = result.get("output").toString();
		JSONArray json = JSONArray.fromObject(output);
		String Number = json.getJSONObject(0).get("Number").toString();
		Assert.assertTrue("1".equals(Number));
	}
	
	private String prepareInputParameters(String sql) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("dbType", dbType);
		map.put("dbHost", dbHost);
		map.put("dbPort", dbPort);
		map.put("dbName", dbName);
		map.put("schema", schema);
		map.put("sqlid", sqlid);
		map.put("dbUser", username);
		map.put("dbPassword", password);
		map.put("sql", sql);
		String param = JSONUtils.map2JSONString(map);
		return param;
	}
}
