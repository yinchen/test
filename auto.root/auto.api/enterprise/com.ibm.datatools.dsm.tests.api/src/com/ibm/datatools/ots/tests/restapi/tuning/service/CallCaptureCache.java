package com.ibm.datatools.ots.tests.restapi.tuning.service;


import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.ots.tests.restapi.common.TuningServiceAPITestBase;
import com.ibm.datatools.test.utils.JSONUtils;

import net.sf.json.JSONObject;

public class CallCaptureCache extends TuningServiceAPITestBase{


	@Test
	public void callCaptureCacheAPI_ONCE() {

		String url = DSMURLUtils.URL_PREFIX + Configuration.getProperty("tuningservice_capturecache");
		JSONObject result = cs.callTuningServiceAPI(url, prepareInputParameters(1));

		//verify result
		String code = result.get("code").toString();
		Assert.assertTrue("0".equals(code));

	}
	
	@Test
	public void callCaptureCacheAPI_MULTI() {

		String url = Configuration.getProperty("tuningservice_capturecache");
		JSONObject result = cs.callTuningServiceAPI(url, prepareInputParameters(2));

		//verify result
		String code = result.get("code").toString();
		Assert.assertTrue("0".equals(code));

	}
	
	private String prepareInputParameters(int flag) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("dbType", dbType);
		map.put("dbHost", dbHost);
		map.put("dbPort", dbPort);
		map.put("dbName", dbName);
		map.put("dbUser", username);
		map.put("dbPassword", password);
		map.put("workloadName", workloadname);
		
		if(flag == 1){
			map.put("captureType", "ONCE");
		}else{
			map.put("duration", "1");
			map.put("interval", "1");
			map.put("captureType", "MULT");
		}
	
		map.put("queryLimitCount", "3");
		String param = JSONUtils.map2JSONString(map);
		return param;
	}
}
