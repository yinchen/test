package com.ibm.datatools.ots.tests.restapi.tuning.service;


import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.ots.tests.restapi.common.TuningServiceAPITestBase;
import com.ibm.datatools.test.utils.JSONUtils;

import net.sf.json.JSONObject;

public class CallCapturePackageCache extends TuningServiceAPITestBase{


	@Test
	public void callCapturePackageCacheAPI() {
		String url = DSMURLUtils.URL_PREFIX + Configuration.getProperty("tuningservice_capturepkgcache");
		JSONObject result = cs.callTuningServiceAPI(url, prepareInputParameters());

		//verify result
		String code = result.get("code").toString();
		Assert.assertTrue("0".equals(code));
		
		String wlName = result.get("output").toString();
		Assert.assertTrue(wlName.startsWith(workloadname));
	}
	
	private String prepareInputParameters() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("dbType", dbType);
		map.put("dbHost", dbHost);
		map.put("dbPort", dbPort);
		map.put("dbName", dbName);
		map.put("dbUser", username);
		map.put("dbPassword", password);
		map.put("workloadName", workloadname);
		map.put("queryLimitCount", "10");
		String param = JSONUtils.map2JSONString(map);
		return param;
	}
}
