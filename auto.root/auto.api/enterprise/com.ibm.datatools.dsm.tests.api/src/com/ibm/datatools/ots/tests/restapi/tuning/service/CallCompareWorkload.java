package com.ibm.datatools.ots.tests.restapi.tuning.service;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.test.utils.JSONUtils;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningServiceAPITestBase;

import net.sf.json.JSONObject;

public class CallCompareWorkload extends TuningServiceAPITestBase{
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("CallTuningServiceAPI_Time.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}

	@Test
	public void callCompareWorkloadAPI_1workload() {
		String url = DSMURLUtils.URL_PREFIX + Configuration.getProperty("tuningservice_compareworkload");
		JSONObject result = cs.callTuningServiceAPI(url, prepareInputParameters(1));

		//verify result
		String code = result.get("code").toString();
		Assert.assertTrue("0".equals(code));
		
		String state = result.get("state").toString();
		Assert.assertTrue("0".equals(state));
	}
	
	@Test(dataProvider = "testdata")
	public void callCompareWorkloadAPI_2workloads(Object key, Object inputPara) {

		String url = DSMURLUtils.URL_PREFIX + Configuration.getProperty("tuningservice_compareworkload");
		JSONObject result = cs.callTuningServiceAPI(url, prepareInputParameters(2));
		
		//verify result
		String code = result.get("code").toString();
		Assert.assertTrue("0".equals(code));
		
		String state = result.get("state").toString();
		Assert.assertTrue("0".equals(state));
		
		//for demo
		String link = result.get("link").toString();
		getCompareReportLink(inputPara, link);
	}
	
	@Test
	public void callCompareWorkloadAPI_2workloads2db() {
		String url = DSMURLUtils.URL_PREFIX + Configuration.getProperty("tuningservice_compareworkload");
		JSONObject result = cs.callTuningServiceAPI(url, prepareInputParameters(3));

		//verify result
		String code = result.get("code").toString();
		Assert.assertTrue("0".equals(code));
		
		if(!"ZOS".equals(dbType)){
			String output = result.get("output").toString();
			Assert.assertTrue("2".equals(output));
		}else{
			String output = result.get("output").toString();
			Assert.assertTrue("0".equals(output));
		}
		
		String state = result.get("state").toString();
		Assert.assertTrue("0".equals(state));
	}

	private String prepareInputParameters(int flag) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("dbType", dbType);
		map.put("dbHost", dbHost);
		map.put("dbPort", dbPort);
		map.put("dbName", dbName);
		map.put("dbUser", username);
		map.put("dbPassword", password);
		if(flag == 1){
			map.put("workload", workloadname);
		}else{
			map.put("workload", workloadname2);
		}

		if(flag != 1){
			map.put("workload2", workloadname);
		}
		
		if(flag == 3){
			map.put("dbHost2", dbHost);
			map.put("dbPort2", dbPort);
			map.put("dbName2", dbName);
			map.put("dbUser2", username);
			map.put("dbPassword2", password);
		}
		map.put("costRegressed", "0");
		String param = JSONUtils.map2JSONString(map);
		return param;
	}
	
	private void getCompareReportLink(Object inputPara, String link){
		if(workloadname2.contains("baseline")){
			JSONObject obj = JSONObject.fromObject(inputPara);
			String baselineExplainTime = obj.getString("baselineExplainTime");
			
			TuningServiceAPI.setCompareReportLink( "This is workload " + workloadname2 +"("+ baselineExplainTime +") and " +workloadname +"("+ TuningServiceAPI.getTargetExplainTime() +") access plans comparison report link: "+link);
		}

	}
}
