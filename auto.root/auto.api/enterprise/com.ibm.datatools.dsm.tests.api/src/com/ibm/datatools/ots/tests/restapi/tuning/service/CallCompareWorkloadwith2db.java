package com.ibm.datatools.ots.tests.restapi.tuning.service;

import java.util.HashMap;
import java.util.Map;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.JSONUtils;
import com.ibm.datatools.ots.tests.restapi.common.TuningServiceAPITestBaseforCompare;

import net.sf.json.JSONObject;

public class CallCompareWorkloadwith2db extends TuningServiceAPITestBaseforCompare{

	@Test
	public void callCompareWorkloadAPIwith2d() {
		String url = DSMURLUtils.URL_PREFIX + Configuration.getProperty("tuningservice_compareworkload");
		JSONObject result = cs.callTuningServiceAPI(url, prepareInputParameters());

		//verify result
		String code = result.get("code").toString();
		Assert.assertTrue("0".equals(code));
		
	
		String output = result.get("output").toString();
		Assert.assertTrue("2".equals(output));

		
		String state = result.get("state").toString();
		Assert.assertTrue("0".equals(state));
	}

	private String prepareInputParameters() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("dbType", dbType);
		map.put("dbHost", dbHost);
		map.put("dbPort", dbPort);
		map.put("dbName", dbName);
		map.put("dbUser", username);
		map.put("dbPassword", password);
		map.put("workload", workloadname);

		map.put("dbHost2", dbHost2);
		map.put("dbPort2", dbPort2);
		map.put("dbName2", dbName2);
		map.put("dbUser2", username2);
		map.put("dbPassword2", password2);
		map.put("workload2", workloadname2);
		
		map.put("costRegressed", "0");
		String param = JSONUtils.map2JSONString(map);
		return param;
	}
	

}
