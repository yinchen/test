package com.ibm.datatools.ots.tests.restapi.tuning.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.test.utils.JSONUtils;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningServiceAPITestBase;

import net.sf.json.JSONObject;

public class CallDefineWorkload extends TuningServiceAPITestBase{
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("CallTuningServiceAPI.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);
	}

	@Test(dataProvider = "testdata")
	public void callDefineWorkloadAPI(Object key, Object inputPara) {
		JSONObject obj = JSONObject.fromObject(inputPara);
		String queryText = "";
		if("ZOS".equals(dbType)){
			 queryText = obj.getString("workloadQueries_zos");
		}else{
			 queryText = obj.getString("workloadQueries_luw");
		}
	
		String url = DSMURLUtils.URL_PREFIX + Configuration.getProperty("tuningservice_defineworkload");
		JSONObject result = cs.callTuningServiceAPI(url, prepareInputParameters(queryText));

		//verify result
		String code = result.get("code").toString();
		Assert.assertTrue("0".equals(code));
		
		String output = result.get("output").toString();
		Assert.assertTrue(workloadname.equals(output));
	}
	
	private String prepareInputParameters(String sql) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("dbType", dbType);
		map.put("dbHost", dbHost);
		map.put("dbPort", dbPort);
		map.put("dbName", dbName);
		map.put("dbUser", username);
		map.put("dbPassword", password);
		map.put("fileContent", sql);
		map.put("workloadName", workloadname);
		map.put("delimiter", ";");
		String param = JSONUtils.map2JSONString(map);
		return param;
	}
}
