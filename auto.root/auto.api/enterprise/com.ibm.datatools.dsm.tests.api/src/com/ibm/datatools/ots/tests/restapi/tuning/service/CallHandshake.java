package com.ibm.datatools.ots.tests.restapi.tuning.service;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.ots.tests.restapi.common.TuningServiceAPITestBasewithVerifyCredential;

public class CallHandshake extends TuningServiceAPITestBasewithVerifyCredential{

	@Test
	public void callHandshakeAPI() {
		String url = DSMURLUtils.URL_PREFIX + Configuration.getProperty("tuningservice_handshake");
		String result = cs.callTuningServiceAPIbyGet(url, prepareInputParameters());
		
		//verify result
		String expectedresult = "<result><OQWTVersion>5.1</OQWTVersion><lowestSupportedDDLversion>1</lowestSupportedDDLversion><highestSupportedDDLversion>2</highestSupportedDDLversion><lowestSupportedAPIversion>2</lowestSupportedAPIversion><highestSupportedAPIversion>4</highestSupportedAPIversion><returnValue><key>call_status</key><value>API_INITIATED</value></returnValue><returnValue><key>call_status_msg</key><value>API is initiated</value></returnValue><returnValue><key>reason_code</key><value>RC0</value></returnValue><returnValue><key>reason_code_msg</key><value>RC0: Successful.</value></returnValue></result>";
		Assert.assertTrue(result.contains(expectedresult));
	}
	
	private Map<String, String> prepareInputParameters() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("APIidentifier", "Querytuner");
		map.put("DDLversion", "2");
		map.put("APIversion", "2");
		map.put("providerID", "CQM");
		map.put("providerVersion", "4.1");
		return map;
	}
}
