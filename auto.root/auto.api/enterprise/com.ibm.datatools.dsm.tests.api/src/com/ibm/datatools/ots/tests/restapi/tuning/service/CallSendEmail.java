package com.ibm.datatools.ots.tests.restapi.tuning.service;

import java.util.HashMap;
import java.util.Map;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.ots.tests.restapi.common.SendEmailTestBase;

import net.sf.json.JSONObject;

public class CallSendEmail extends SendEmailTestBase{

	@Test
	public void callSendEmailAPI() {
		String url = DSMURLUtils.URL_PREFIX + Configuration.getProperty("sendemail");
		String mailMessage = TuningServiceAPI.getCompareReportLink();

		JSONObject result = cs.callTuningServiceAPI(url, prepareInputParameters(mailMessage));
		
		//verify result
		String code = result.get("resultCode").toString();
		Assert.assertTrue("success".equals(code));
	}
	
	private Map<String, String> prepareInputParameters(String mailMessage) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("mailSender", mailSender);
		map.put("mailRecipients", mailRecipients);
		map.put("mailSubject", mailSubject);
		map.put("mailMessage", mailMessage);
		map.put("alertIDs", alertIDs);
		map.put("cmd", cmd);
		return map;
	}
}
