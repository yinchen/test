package com.ibm.datatools.ots.tests.restapi.tuning.service;

import java.util.HashMap;
import java.util.Map;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.JSONUtils;
import com.ibm.datatools.ots.tests.restapi.common.TuningServiceAPITestBase;

import net.sf.json.JSONObject;

public class CallSwtich extends TuningServiceAPITestBase{
	

	@Test
	public void CallSwtichAPI_command1() {
		String url = DSMURLUtils.URL_PREFIX + Configuration.getProperty("tuningservice_switch");
		JSONObject result = cs.callTuningServiceAPI(url, prepareInputParameters("trace=enable"));

		//verify result
		String code = result.get("code").toString();
		Assert.assertTrue("0".equals(code));
	}
	
	@Test
	public void CallSwtichAPI_command2() {
		String url = DSMURLUtils.URL_PREFIX + Configuration.getProperty("tuningservice_switch");
		JSONObject result = cs.callTuningServiceAPI(url, prepareInputParameters("switch_sql_id=disable"));

		//verify result
		String code = result.get("code").toString();
		Assert.assertTrue("0".equals(code));
	}
	private String prepareInputParameters(String command) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("command", command);
		String param = JSONUtils.map2JSONString(map);
		return param;
	}
}
