package com.ibm.datatools.ots.tests.restapi.tuning.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;
import com.ibm.datatools.ots.tests.restapi.common.TuningServiceAPITestBasewithVerifyCredential;

import net.sf.json.JSONObject;

public class CallTuneSQL extends TuningServiceAPITestBasewithVerifyCredential{
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		cs = new TuningCommandService("CallTuningServiceAPI.properties");
		p = cs.getJSONProperties();
		return FileTools.readProperties(p);

	}

	@Test(dataProvider = "testdata")
	public void callTuneSQLAPI(Object key, Object inputPara) {
		JSONObject obj = JSONObject.fromObject(inputPara);
		String queryText = obj.getString("QMQueryText");
		String url = DSMURLUtils.URL_PREFIX + Configuration.getProperty("tuningservice_tunesql");
		JSONObject result = cs.callTuningServiceAPI(url, prepareInputParameters(queryText, "unstatic"));

		//verify result
		String status = result.get("status").toString();
		Assert.assertTrue("0".equals(status));
		
		String message = result.get("message").toString();
		Assert.assertTrue("success".equals(message));
	}
	
	@Test
	public void callTuneSQLAPI4StaticSQLWithoutSQLText() {
		
		String url = Configuration.getProperty("tuningservice_tunesql");

		JSONObject result = cs.callTuningServiceAPI(url, prepareInputParameters("", "static"));
		
		// verify result
		String status = result.get("status").toString();
		Assert.assertTrue("0".equals(status));
	}	
	
	private Map<String, String> prepareInputParameters(String sql, String flag) {
		Map<String, String> map = new HashMap<String, String>();
	
		map.put("APIidentifier", "Querytuner");
		map.put("APIversion", "2");
		map.put("providerID", "CQM");
		map.put("monitorDBType", dbType);
		map.put("monitorDBURL", "jdbc:db2://"+dbHost+":"+dbPort+"/"+dbName);
		map.put("monitorDBConnectionProfileName", dbName);
		map.put("monitorDBUserName", username);
		map.put("monitorDBPassword", password);
		map.put("monitorDBSecurityMechanism", "3");
		map.put("explainSQLID", sqlid);
		map.put("providerVersion", "4.1");
		
		if(flag.equals("static")){
			//query from catalog package
			map.put("stmtType", "S");
			map.put("collID", "DB2OSC");
			map.put("program", "AOC5OADM");
			map.put("version", "20130829");
			map.put("sectNoI", "1");
			map.put("stmtNoI", "1");
		}else{
			map.put("sqlStatement", sql);
			map.put("sqlSchema", schema);
		}
		return map;
	}
	
	
	@Test
	public void callTuneSQLAPI4StaticSQLCanNotFindStaticText() {
		
		String url = Configuration.getProperty("tuningservice_tunesql");

		
		Map<String, String> map = new HashMap<String, String>();
		
		map.put("APIidentifier", "Querytuner");
		map.put("APIversion", "2");
		map.put("providerID", "CQM");
		map.put("monitorDBType", dbType);
		map.put("monitorDBURL", "jdbc:db2://"+dbHost+":"+dbPort+"/"+dbName);
		map.put("monitorDBConnectionProfileName", dbName);
		map.put("monitorDBUserName", username);
		map.put("monitorDBPassword", password);
		map.put("monitorDBSecurityMechanism", "3");
		map.put("explainSQLID", sqlid);
		map.put("providerVersion", "4.1");
			
		// query from catalog package
		map.put("stmtType", "S");
		map.put("collID", "DB2OSC");
		map.put("program", "AOC5OADM");
		map.put("version", "20130829");
		map.put("sectNoI", "1");
		map.put("stmtNoI", "0");
		
		JSONObject result = cs.callTuningServiceAPI(url, map);
		
		// verify result
		String status = result.get("status").toString();
		Assert.assertTrue("8".equals(status));
		Assert.assertEquals(result.get("message").toString(), "Fail to get the static statement text from the Db2 catalog, please check your parameters.");
	}
	
	@Test
	public void callTuneSQLAPI4StaticSQLSectNoIMinus1() {
		
		String url = Configuration.getProperty("tuningservice_tunesql");

		
		Map<String, String> map = new HashMap<String, String>();
		
		map.put("APIidentifier", "Querytuner");
		map.put("APIversion", "2");
		map.put("providerID", "CQM");
		map.put("monitorDBType", dbType);
		map.put("monitorDBURL", "jdbc:db2://"+dbHost+":"+dbPort+"/"+dbName);
		map.put("monitorDBConnectionProfileName", dbName);
		map.put("monitorDBUserName", username);
		map.put("monitorDBPassword", password);
		map.put("monitorDBSecurityMechanism", "3");
		map.put("explainSQLID", sqlid);
		map.put("providerVersion", "4.1");
			
		// query from catalog package
		map.put("stmtType", "S");
		map.put("collID", "DB2OSC");
		map.put("program", "AOC5OADM");
		map.put("version", "20130829");
		map.put("sectNoI", "1");
		map.put("stmtNoI", "-1");
		
		JSONObject result = cs.callTuningServiceAPI(url, map);
		
		// verify result
		String status = result.get("status").toString();
		Assert.assertTrue("0".equals(status));
	}
	
	@Test
	public void callTuneSQLAPI4StaticSQLWithSTMT_ID() {
		
		String url = Configuration.getProperty("tuningservice_tunesql");

		
		Map<String, String> map = new HashMap<String, String>();
		
		map.put("APIidentifier", "Querytuner");
		map.put("APIversion", "2");
		map.put("providerID", "CQM");
		map.put("monitorDBType", dbType);
		map.put("monitorDBURL", "jdbc:db2://"+dbHost+":"+dbPort+"/"+dbName);
		map.put("monitorDBConnectionProfileName", dbName);
		map.put("monitorDBUserName", username);
		map.put("monitorDBPassword", password);
		map.put("monitorDBSecurityMechanism", "3");
		map.put("explainSQLID", sqlid);
		map.put("providerVersion", "4.1");
			
		// query from catalog package
		map.put("stmtType", "S");
		map.put("stmtID", "60679");
		
		JSONObject result = cs.callTuningServiceAPI(url, map);
		
		// verify result
		String status = result.get("status").toString();
		Assert.assertTrue("0".equals(status));	
	}
	
	@Test
	public void callTuneSQLAPI4StaticSQLWithConsisToken() {
		
		String url = Configuration.getProperty("tuningservice_tunesql");

		
		Map<String, String> map = new HashMap<String, String>();
		
		map.put("APIidentifier", "Querytuner");
		map.put("APIversion", "2");
		map.put("providerID", "CQM");
		map.put("monitorDBType", dbType);
		map.put("monitorDBURL", "jdbc:db2://"+dbHost+":"+dbPort+"/"+dbName);
		map.put("monitorDBConnectionProfileName", dbName);
		map.put("monitorDBUserName", username);
		map.put("monitorDBPassword", password);
		map.put("monitorDBSecurityMechanism", "3");
		map.put("explainSQLID", sqlid);
		map.put("providerVersion", "4.1");
			
		// query from catalog package
		map.put("stmtType", "S");
		map.put("conToken", "7a4845546557415a");
		map.put("sectNoI", "1");
		map.put("stmtNoI", "1");
		
		JSONObject result = cs.callTuningServiceAPI(url, map);
		
		// verify result
		String status = result.get("status").toString();
		Assert.assertTrue("0".equals(status));	
	}
	
	@Test
	public void callTuneSQLAPI4StaticSQLParaIncomplete() {
		
		String url = Configuration.getProperty("tuningservice_tunesql");

		
		Map<String, String> map = new HashMap<String, String>();
		
		map.put("APIidentifier", "Querytuner");
		map.put("APIversion", "2");
		map.put("providerID", "CQM");
		map.put("monitorDBType", dbType);
		map.put("monitorDBURL", "jdbc:db2://"+dbHost+":"+dbPort+"/"+dbName);
		map.put("monitorDBConnectionProfileName", dbName);
		map.put("monitorDBUserName", username);
		map.put("monitorDBPassword", password);
		map.put("monitorDBSecurityMechanism", "3");
		map.put("explainSQLID", sqlid);
		map.put("providerVersion", "4.1");
			
		// query from catalog package
		map.put("stmtType", "S");
		map.put("conToken", "7a4845546557415a");
		map.put("sectNoI", "1");
		
		JSONObject result = cs.callTuningServiceAPI(url, map);
		
		// verify result
		String status = result.get("status").toString();
		Assert.assertTrue("8".equals(status));	
	}
}
