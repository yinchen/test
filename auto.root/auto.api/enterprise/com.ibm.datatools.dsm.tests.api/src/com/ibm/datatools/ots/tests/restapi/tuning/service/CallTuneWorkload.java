package com.ibm.datatools.ots.tests.restapi.tuning.service;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.ots.tests.restapi.common.TuningServiceAPITestBasewithVerifyCredential;

import net.sf.json.JSONObject;

public class CallTuneWorkload extends TuningServiceAPITestBasewithVerifyCredential{

	@Test
	public void callTuneWorkloadAPI() {
		String url = DSMURLUtils.URL_PREFIX + Configuration.getProperty("tuningservice_tuneworkload");
		JSONObject result = cs.callTuningServiceAPI(url, prepareInputParameters());

		//verify result
		String status = result.get("status").toString();
		Assert.assertTrue("0".equals(status));
		
		String message = result.get("message").toString();
		Assert.assertTrue("success".equals(message));
	}
	
	private Map<String, String> prepareInputParameters() {
		Map<String, String> map = new HashMap<String, String>();
	
		map.put("APIidentifier", "Querytuner");
		map.put("DDLversion", "2");
		map.put("APIversion", "2");
		map.put("providerID", "CQM");
		map.put("monitorDBType", dbType);
		map.put("monitorDBURL", "jdbc:db2://"+dbHost+":"+dbPort+"/"+dbName);
		map.put("monitorDBConnectionProfileName", dbName);
		map.put("monitorDBUserName", username);
		map.put("monitorDBPassword", password);
		map.put("monitorDBSecurityMechanism", "3");
		map.put("stagingTableDBType", dbType);
		map.put("stagingTableDBURL", "jdbc:db2://"+dbHost+":"+dbPort+"/"+dbName);
		map.put("stagingTableDBConnectionProfileName", dbName);
		map.put("stagingTableDBUserName", username);
		map.put("stagingTableDBPassword", password);
		map.put("stagingTableDBSecurityMechanism", "3");
		map.put("stagingTableSchema", "CQMTOOLS");
		map.put("workloadId", "2");
		map.put("explainSQLID", sqlid);
		map.put("workloadNamePrefix", "workload_");
		map.put("collectExistingExplain", "NO");
		map.put("deleteStagingDataOnSuccessfulCapture", "NO");
		map.put("providerVersion", "4.1");
		return map;
	}
}
