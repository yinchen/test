package com.ibm.datatools.ots.tests.restapi.tuning.service;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.Configuration;
import com.ibm.datatools.ots.tests.restapi.base.DSMURLUtils;
import com.ibm.datatools.test.utils.JSONUtils;
import com.ibm.datatools.ots.tests.restapi.common.TuningServiceAPITestBase;

import net.sf.json.JSONObject;

public class CallWIA extends TuningServiceAPITestBase{
	

	@Test
	public void callWIAAPI() {
		String url = DSMURLUtils.URL_PREFIX + Configuration.getProperty("tuningservice_wia");
		JSONObject result = cs.callTuningServiceAPI(url, prepareInputParameters());

		//verify result
		String code = result.get("code").toString();
		Assert.assertTrue("0".equals(code));
		
	}
	
	private String prepareInputParameters() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("dbType", dbType);
		map.put("dbHost", dbHost);
		map.put("dbPort", dbPort);
		map.put("dbName", dbName);
		map.put("dbUser", username);
		map.put("dbPassword", password);
		map.put("workloadName", workloadname);
		String param = JSONUtils.map2JSONString(map);
		return param;
	}
}
