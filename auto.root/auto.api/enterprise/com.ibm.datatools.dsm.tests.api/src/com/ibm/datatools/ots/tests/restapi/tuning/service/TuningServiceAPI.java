package com.ibm.datatools.ots.tests.restapi.tuning.service;

public class TuningServiceAPI{

	//for demo
	private static String compareReportLink;
	private static String targetExplainTime;
	
	public static void setCompareReportLink(String link){
		 compareReportLink = link;
	}
	
	public static String getCompareReportLink(){
		return compareReportLink;
	}

	public static void setTargetExplainTime(String targetTime){
		targetExplainTime = targetTime;
	}
	
	public static String getTargetExplainTime(){
		return targetExplainTime;
	}
}
