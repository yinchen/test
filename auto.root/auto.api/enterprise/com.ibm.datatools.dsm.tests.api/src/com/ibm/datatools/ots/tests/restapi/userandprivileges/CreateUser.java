package com.ibm.datatools.ots.tests.restapi.userandprivileges;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.common.RestAPIBaseTest;
import com.ibm.datatools.ots.tests.restapi.common.TuningCommandService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class CreateUser {
	
	TuningCommandService cs;
	Properties p;
	
	
	@BeforeTest
	public void beforeTest() throws FileNotFoundException, IOException {
		
		cs = new TuningCommandService();
		RestAPIBaseTest.loginDSM();
		
	}
	
	@Test
	public void testCreateUser() throws FileNotFoundException, IOException{
		
		Properties prop = new Properties();
		String rootPath = System.getProperty("user.dir");
		String path = "/testData/postData/UserAndPrivileges/createUser.properties";
		String filePath = rootPath + path;
		prop.load(new FileInputStream(new File(filePath)));
		
		String userID = prop.getProperty("userid");
		String password = prop.getProperty("password");
		
		JSONObject result = cs.getUsers();
		JSONArray items = result.getJSONArray("items");
		
		boolean isUserIDExist = false;
		
		if(items.size()>0){
			for(int i=0; i<items.size(); i++){
				Map<String, String> userMap = (Map<String, String>) items.get(i);
				if(userMap.get("user").equals(userID)){
					isUserIDExist = true;
				}
			}
		}
		
		if(isUserIDExist){
			JSONObject delUserIDResult = cs.deleteUser(userID);
			String delResultCode = delUserIDResult.getString("resultcode");
			if(delResultCode.equals("success")){
				String message = "UserID [ "+userID+" ] has been deleted successfully";
				System.out.println("[LOG]->"+message);
				Assert.assertTrue(delResultCode.equals("success"), message);
			}
		}
		
		JSONObject cereteResult = cs.createUser(userID, password);
		System.out.println("[LOG]->"+cereteResult);
		
		String resultCode = cereteResult.getString("resultcode");
		if(!resultCode.equals("success")){
			Assert.fail("Create User failed");
		}
		
	}
}

























