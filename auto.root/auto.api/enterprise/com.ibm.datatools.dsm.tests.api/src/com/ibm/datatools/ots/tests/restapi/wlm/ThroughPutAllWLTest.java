package com.ibm.datatools.ots.tests.restapi.wlm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.ots.tests.restapi.base.RepoDBUtils;

public class ThroughPutAllWLTest{

	static int connId = 1;
	static final String connName = "bjwlmtest";
	static final Timestamp ts1 = Timestamp.valueOf(LocalDateTime.now().minus(10, ChronoUnit.MINUTES));
	static final Timestamp ts2 = Timestamp.valueOf(LocalDateTime.now());
	static final int wlid = 999;
	static final String wlName = "testWL";
	static final int i = 1;
	
	static final double TOTAL_CPU_TIME_WL = 1;
	static final double CPU_USER = 2;
	static final double CPU_SYSTEM = 3;
	static final double CPU_IOWAIT = 4;
	static final double CPU_IDLE = 5;
	static final double logical_reads = 3600; //6
	static final double total_act_time = 7;
	static final double act_aborted_total = 8;
	static final double act_completed_total = 9;
	static final double total_app_commits = 3600; //10
	static final double total_sorts = 11;
	static final double total_compilations = 12;
	static final double deadlocks = 13;
	static final double sort_overflows = 14;
	static final double total_section_proc_time = 20;  //total_section_proc_time >total_section_sort_proc_time
	static final double total_routine_user_code_proc_time = 16;
	static final double total_section_sort_proc_time = 17;
	static final double lock_wait_time = 59;
	static final double cf_wait_time = 19;
	static final double pool_read_time = 20;
	static final double pool_write_time = 21;
	static final double direct_read_time = 22;
	static final double direct_write_time = 23;
	static final double total_compile_proc_time = 24;
	static final double total_implicit_compile_proc_time = 25;
	static final double total_load_proc_time = 26;
	static final double total_reorg_proc_time = 27;
	static final double total_runstats_proc_time = 28;
	static final double total_commit_proc_time = 29;
	static final double total_rollback_proc_time = 30;
	static final double total_connect_request_proc_time = 31;
	static final double agent_wait_time = 32;
	static final double wlm_queue_time_total = 33;
	static final double log_buffer_wait_time = 34;
	static final double log_disk_wait_time = 35;
	static final double tcpip_wait_time = 36;
	static final double ipc_wait_time = 37;
	static final double ida_wait_time = 38;
	static final double audit_subsystem_wait_time = 39;
	static final double audit_file_write_wait_time = 40;
	static final double diaglog_write_wait_time = 41;
	static final double evmon_wait_time = 42;
	static final double total_extended_latch_wait_time = 43;
	static final double prefetch_wait_time = 44;
	static final double comm_exit_wait_time = 45;
	static final double reclaim_wait_time = 46;
	static final double spacemappage_reclaim_wait_time = 47;
	static final double fed_wait_time = 48;
	static final double lock_wait_time_global = 49;
	static final double total_backup_proc_time = 50;
	static final double total_index_build_proc_time = 51;
	static final double direct_reads = 52;
	static final double lock_escals = 53;
	static final double lock_waits = 54;
	static final double fed_waits_total = 55;
	static final double physical_reads = 56;
	static final double rows_modified = 57;
	static final double rows_read = 58;
	static final double fed_rows_deleted = 59;
	static final double fed_rows_inserted = 60;
	static final double fed_rows_updated = 61;
	static final double fed_rows_read = 62;
	
	static final double sqlexecutiontime = // 36
			total_section_proc_time // 20
			+ total_routine_user_code_proc_time;// 16
	
	static final double iotime  = //86
			pool_read_time  //20 
			+pool_write_time  //21 
			+ direct_read_time //22 
			+  direct_write_time ; //23 
	
	static final double otherprocessingtime =//220 
			total_compile_proc_time //24
			+total_implicit_compile_proc_time  //25
			+ total_load_proc_time //26 
			+total_reorg_proc_time  //27 
			+total_runstats_proc_time //28 
			+total_commit_proc_time //29 
			+total_rollback_proc_time //30 
			+total_connect_request_proc_time; //31 
	
	static final double otherwaittime =//680
			agent_wait_time //32 
			+wlm_queue_time_total //33
			+log_buffer_wait_time //34
			+log_disk_wait_time //35
			+tcpip_wait_time //36 
			+ipc_wait_time //37 
			+ida_wait_time  //38
			+audit_subsystem_wait_time  //39
			+audit_file_write_wait_time  //40
			+diaglog_write_wait_time  //41
			+evmon_wait_time  //42
			+total_extended_latch_wait_time  //43
			+prefetch_wait_time  //44
			+comm_exit_wait_time //45
			+reclaim_wait_time  //46
			+spacemappage_reclaim_wait_time //47 
			+fed_wait_time ; //48 

	static final double total_time =  //1100  
			sqlexecutiontime //36 
			+iotime  //86 
			+  otherprocessingtime //220 
			+  otherwaittime  //680
			+  lock_wait_time  //59 
			+ cf_wait_time; //19

	@BeforeClass
	private static void insertExample() throws Throwable {
		
		Connection con =null;
		Statement stmt = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			con = RepoDBUtils.getConnection();
			
			stmt = con.createStatement();
			stmt.execute("insert into ibm_rtmon_metadata.rtmon_map_dbconn(dbconn_id) values('"+connName+"')");
			con.commit();
			rs = stmt.executeQuery("select dbconn_int from ibm_rtmon_metadata.rtmon_map_dbconn where dbconn_id='"+connName+"'");
			while(rs.next()){
				connId = rs.getInt("dbconn_int");
			}
			rs.close();
			stmt.close();

			ps = con.prepareStatement("insert into ibm_rtmon_data.\"throughputAllWL\" "
					+ "( dbconn_int   , collected   , WORKLOAD_ID  ,  WORKLOAD_NAME ,"
					+ " TOTAL_CPU_TIME_WL  ,  CPU_USER    , CPU_SYSTEM    , CPU_IOWAIT    , CPU_IDLE    , "
					+ " logical_reads   ,  total_act_time   , act_aborted_total   , act_completed_total  ,  total_app_commits   , "
					+ " total_sorts    , total_compilations  ,  deadlocks    , sort_overflows   , total_section_proc_time , "
					+ " TOTAL_RTN_USER_CODE_PROC_TM , total_section_sort_proc_time , lock_wait_time , cf_wait_time , pool_read_time ,"
					+ " pool_write_time , direct_read_time , direct_write_time , total_compile_proc_time , total_implicit_compile_proc_tm ,"
					+ " total_load_proc_time , total_reorg_proc_time , total_runstats_proc_time , total_commit_proc_time , total_rollback_proc_time ,"
					+ " total_connect_request_proc_tm  , agent_wait_time , wlm_queue_time_total , log_buffer_wait_time , log_disk_wait_time ,"
					+ " tcpip_wait_time , ipc_wait_time , ida_wait_time , audit_subsystem_wait_time , audit_file_write_wait_time ,"
					+ " diaglog_write_wait_time , evmon_wait_time , total_extended_latch_wait_time , prefetch_wait_time , comm_exit_wait_time ,"
					+ " reclaim_wait_time , spacemappage_reclaim_wait_time , fed_wait_time  , lock_wait_time_global , total_backup_proc_time ,"
					+ " total_index_build_proc_time, direct_reads, lock_escals, lock_waits, fed_waits_total, physical_reads, rows_modified, "
					+ " rows_read, fed_rows_deleted, fed_rows_inserted, fed_rows_updated, fed_rows_read) values(?, ?, "
					+ "?, ?, ?, ?, ?,   ?, ?, ?, ?, ?, "
					+ "?, ?, ?, ?, ?,   ?, ?, ?, ?, ?, "
					+ "?, ?, ?, ?, ?,   ?, ?, ?, ?, ?, "
					+ "?, ?, ?, ?, ?,   ?, ?, ?, ?, ?, "
					+ "?, ?, ?, ?, ?,   ?, ?, ?, ?, ?, "
					+ "?, ?, ?, ?, ?,   ?, ?, ?, ?, ?, "
					+ "?, ?, ?, ? )");
			
			
			ps.setInt(1, connId);
			ps.setTimestamp(2, ts1);
			ps.setInt(3, wlid);
			ps.setString(4, wlName);
			ps.setInt(1 + 4, (int) (TOTAL_CPU_TIME_WL * i));
			ps.setInt(2 + 4, (int) (CPU_USER * i));
			ps.setInt(3 + 4, (int) (CPU_SYSTEM * i));
			ps.setInt(4 + 4, (int) (CPU_IOWAIT * i));
			ps.setInt(5 + 4, (int) (CPU_IDLE * i));
			ps.setInt(6 + 4, (int) (logical_reads * i));
			ps.setInt(7 + 4, (int) (total_act_time * i));
			ps.setInt(8 + 4, (int) (act_aborted_total * i));
			ps.setInt(9 + 4, (int) (act_completed_total * i));
			ps.setInt(10 + 4, (int) (total_app_commits * i));
			ps.setInt(11 + 4, (int) (total_sorts * i));
			ps.setInt(12 + 4, (int) (total_compilations * i));
			ps.setInt(13 + 4, (int) (deadlocks * i));
			ps.setInt(14 + 4, (int) (sort_overflows * i));
			ps.setInt(15 + 4, (int) (total_section_proc_time * i)); 
			ps.setInt(16 + 4, (int) (total_routine_user_code_proc_time * i));
			ps.setInt(17 + 4, (int) (total_section_sort_proc_time * i));
			ps.setInt(18 + 4, (int) (lock_wait_time * i));  //lock_wait_time > lock_wait_time_global 49
			ps.setInt(19 + 4, (int) (cf_wait_time * i));
			ps.setInt(20 + 4, (int) (pool_read_time * i));
			ps.setInt(21 + 4, (int) (pool_write_time * i));
			ps.setInt(22 + 4, (int) (direct_read_time * i));
			ps.setInt(23 + 4, (int) (direct_write_time* i));
			ps.setInt(24 + 4, (int) (total_compile_proc_time * i));
			ps.setInt(25 + 4, (int) (total_implicit_compile_proc_time * i));
			ps.setInt(26 + 4, (int) (total_load_proc_time * i));
			ps.setInt(27 + 4, (int) (total_reorg_proc_time * i));
			ps.setInt(28 + 4, (int) (total_runstats_proc_time * i));
			ps.setInt(29 + 4, (int) (total_commit_proc_time * i));
			ps.setInt(30 + 4, (int) (total_rollback_proc_time * i));
			ps.setInt(31 + 4, (int) (total_connect_request_proc_time * i));
			ps.setInt(32 + 4, (int) (agent_wait_time * i));
			ps.setInt(33 + 4, (int) (wlm_queue_time_total * i));
			ps.setInt(34 + 4, (int) (log_buffer_wait_time * i));
			ps.setInt(35 + 4, (int) (log_disk_wait_time * i));
			ps.setInt(36 + 4, (int) (tcpip_wait_time * i));
			ps.setInt(37 + 4, (int) (ipc_wait_time * i));
			ps.setInt(38 + 4, (int) (ida_wait_time * i));
			ps.setInt(39 + 4, (int) (audit_subsystem_wait_time * i));
			ps.setInt(40 + 4, (int) (audit_file_write_wait_time * i));
			ps.setInt(41 + 4, (int) (diaglog_write_wait_time * i));
			ps.setInt(42 + 4, (int) (evmon_wait_time * i));
			ps.setInt(43 + 4, (int) (total_extended_latch_wait_time * i));
			ps.setInt(44 + 4, (int) (prefetch_wait_time * i));
			ps.setInt(45 + 4, (int) (comm_exit_wait_time * i));
			ps.setInt(46 + 4, (int) (reclaim_wait_time* i));
			ps.setInt(47 + 4, (int) (spacemappage_reclaim_wait_time * i));
			ps.setInt(48 + 4, (int) (fed_wait_time * i));
			ps.setInt(49 + 4, (int) (lock_wait_time_global * i));
			ps.setInt(50 + 4, (int) (total_backup_proc_time * i));
			ps.setInt(51 + 4, (int) (total_index_build_proc_time * i));
			ps.setInt(52 + 4, (int) (direct_reads * i));
			ps.setInt(53 + 4, (int) (lock_escals * i));
			ps.setInt(54 + 4, (int) (lock_waits * i));
			ps.setInt(55 + 4, (int) (fed_waits_total * i));
			ps.setInt(56 + 4, (int) (physical_reads * i));
			ps.setInt(57 + 4, (int) (rows_modified * i));
			ps.setInt(58 + 4, (int) (rows_read * i));
			ps.setInt(59 + 4, (int) (fed_rows_deleted * i));
			ps.setInt(60 + 4, (int) (fed_rows_inserted * i));
			ps.setInt(61 + 4, (int) (fed_rows_updated * i));
			ps.setInt(62 + 4, (int) (fed_rows_read * i));

			ps.executeUpdate();
			System.out.println("Insert first row!");

			ps.setInt(1, connId);
			ps.setTimestamp(2, ts2);
			ps.setInt(3, wlid);
			ps.setString(4, wlName);
			ps.setInt(1 + 4, (int) (TOTAL_CPU_TIME_WL * i + 128));
			ps.setInt(2 + 4, (int) (CPU_USER * i + 128));
			ps.setInt(3 + 4, (int) (CPU_SYSTEM * i + 128));
			ps.setInt(4 + 4, (int) (CPU_IOWAIT * i + 128));
			ps.setInt(5 + 4, (int) (CPU_IDLE * i + 128));
			ps.setInt(6 + 4, (int) (logical_reads * i + 128));
			ps.setInt(7 + 4, (int) (total_act_time * i + 128));
			ps.setInt(8 + 4, (int) (act_aborted_total * i + 128));
			ps.setInt(9 + 4, (int) (act_completed_total * i + 128));
			ps.setInt(10 + 4, (int) (total_app_commits * i + 128));
			ps.setInt(11 + 4, (int) (total_sorts * i + 128));
			ps.setInt(12 + 4, (int) (total_compilations * i + 128));
			ps.setInt(13 + 4, (int) (deadlocks * i + 128));
			ps.setInt(14 + 4, (int) (sort_overflows * i + 128));
			ps.setInt(15 + 4, (int) (total_section_proc_time * i + 128)); 
			ps.setInt(16 + 4, (int) (total_routine_user_code_proc_time * i + 128));
			ps.setInt(17 + 4, (int) (total_section_sort_proc_time * i + 128));
			ps.setInt(18 + 4, (int) (lock_wait_time * i + 128));  //lock_wait_time > lock_wait_time_global 49
			ps.setInt(19 + 4, (int) (cf_wait_time * i + 128));
			ps.setInt(20 + 4, (int) (pool_read_time * i + 128));
			ps.setInt(21 + 4, (int) (pool_write_time * i + 128));
			ps.setInt(22 + 4, (int) (direct_read_time * i + 128));
			ps.setInt(23 + 4, (int) (direct_write_time* i + 128));
			ps.setInt(24 + 4, (int) (total_compile_proc_time * i + 128));
			ps.setInt(25 + 4, (int) (total_implicit_compile_proc_time * i + 128));
			ps.setInt(26 + 4, (int) (total_load_proc_time * i + 128 + 128));
			ps.setInt(27 + 4, (int) (total_reorg_proc_time * i + 128 + 128));
			ps.setInt(28 + 4, (int) (total_runstats_proc_time * i + 128 + 128));
			ps.setInt(29 + 4, (int) (total_commit_proc_time * i + 128));
			ps.setInt(30 + 4, (int) (total_rollback_proc_time * i + 128));
			ps.setInt(31 + 4, (int) (total_connect_request_proc_time * i + 128));
			ps.setInt(32 + 4, (int) (agent_wait_time * i + 128));
			ps.setInt(33 + 4, (int) (wlm_queue_time_total * i + 128));
			ps.setInt(34 + 4, (int) (log_buffer_wait_time * i + 128));
			ps.setInt(35 + 4, (int) (log_disk_wait_time * i + 128));
			ps.setInt(36 + 4, (int) (tcpip_wait_time * i + 128));
			ps.setInt(37 + 4, (int) (ipc_wait_time * i + 128));
			ps.setInt(38 + 4, (int) (ida_wait_time * i + 128));
			ps.setInt(39 + 4, (int) (audit_subsystem_wait_time * i + 128));
			ps.setInt(40 + 4, (int) (audit_file_write_wait_time * i + 128));
			ps.setInt(41 + 4, (int) (diaglog_write_wait_time * i + 128));
			ps.setInt(42 + 4, (int) (evmon_wait_time * i + 128));
			ps.setInt(43 + 4, (int) (total_extended_latch_wait_time * i + 128));
			ps.setInt(44 + 4, (int) (prefetch_wait_time * i + 128));
			ps.setInt(45 + 4, (int) (comm_exit_wait_time * i + 128));
			ps.setInt(46 + 4, (int) (reclaim_wait_time* i + 128));
			ps.setInt(47 + 4, (int) (spacemappage_reclaim_wait_time * i + 128));
			ps.setInt(48 + 4, (int) (fed_wait_time * i + 128));
			ps.setInt(49 + 4, (int) (lock_wait_time_global * i + 128));
			ps.setInt(50 + 4, (int) (total_backup_proc_time * i + 128));
			ps.setInt(51 + 4, (int) (total_index_build_proc_time * i + 128));
			ps.setInt(52 + 4, (int) (direct_reads * i + 128));
			ps.setInt(53 + 4, (int) (lock_escals * i + 128));
			ps.setInt(54 + 4, (int) (lock_waits * i + 128));
			ps.setInt(55 + 4, (int) (fed_waits_total * i + 128));
			ps.setInt(56 + 4, (int) (physical_reads * i + 128));
			ps.setInt(57 + 4, (int) (rows_modified * i + 128));
			ps.setInt(58 + 4, (int) (rows_read * i + 128));
			ps.setInt(59 + 4, (int) (fed_rows_deleted * i + 128));
			ps.setInt(60 + 4, (int) (fed_rows_inserted * i + 128));
			ps.setInt(61 + 4, (int) (fed_rows_updated * i + 128));
			ps.setInt(62 + 4, (int) (fed_rows_read * i + 128));

			ps.executeUpdate();
			System.out.println("Insert second row!");

		} catch (Exception err) {
			err.printStackTrace(System.out);
		}finally{
			con.close();
		}
	}
	
	@Test
	public void VerifyViewExist() throws Exception{
		Connection con =null;
		Statement stmt = null;
		ResultSet rs = null;
		String DBName = null;
		String sql = "select dbconn_id from ibm_dsm_views.throughput_all_wl where dbconn_int="+connId; 
		try {
			con  = RepoDBUtils.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				DBName = rs.getString("dbconn_id");
			}
			rs.close();
			stmt.close();
			con.close();
			System.out.println(DBName);
			Assert.assertEquals(DBName, "bjwlmtest");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw e;
		}
	}
	
	@AfterClass
	private static void clearEnvironment() throws Throwable {
		Connection con =null;
		Statement stmt = null;
		String DBName = null;
		String sql1 = "delete from ibm_rtmon_data.\"throughputAllWL\" where dbconn_int="+connId; 
		String sql2 = "delete from ibm_rtmon_metadata.rtmon_map_dbconn where dbconn_int="+connId; 
		try {
			con  = RepoDBUtils.getConnection();
			stmt = con.createStatement();
			stmt.execute(sql1);
			con.commit();
			stmt.execute(sql2);
			con.commit();
			stmt.close();
			con.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw e;
		}
	}


}
