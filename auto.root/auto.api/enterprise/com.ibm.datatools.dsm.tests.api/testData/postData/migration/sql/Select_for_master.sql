-------------------------------------------------------------------------------
-- IBM Data Server Manager Master
--
-- Purpose: You can use this script to verify new added column / new added table  
--          exists or Not after migration
--         
--
-- Usage: Run the script and ensure no SQL errors are returned.
-------------------------------------------------------------------------------

--#SET TERMINATOR #
--Example 1: To verify new added column exists or Not
--SELECT DBCONN_INT, COLLECTED FROM IBM_RTMON_DATA."throughputAllWL" FETCH FIRST 1 ROW ONLY#

--Example 2: To verify new added table exists or Not
--SELECT 1 FROM IBMPDQ.MANAGED_DATABASE WHERE(1=-1)#

--SAMPLING_SIZE check in DB2OSC.DSN_WCC_STMT_INSTS table for EO enhancement
SELECT SAMPLING_SIZE FROM DB2OSC.DSN_WCC_STMT_INSTS FETCH FIRST 1 ROWS ONLY;
SELECT COLLECTED from ibm_rtmon_data."wlmQueueStats" FETCH FIRST 1 ROWS ONLY;

--DISCONNECT_TIME column check for EI enhancement.
SELECT DISCONNECT_TIME FROM IBMIOCM.MGCLIENT_DATASOURCE_CONN_TIME FETCH FIRST 1 ROWS ONLY;
SELECT 1 FROM QUERYTUNER.OQWT_CAP_FILTER FETCH FIRST 1 ROWS ONLY;
SELECT 1 FROM QUERYTUNER.OQWT_CAP_FILTER_COND FETCH FIRST 1 ROWS ONLY;
--Add APPLID_HOLDING_OLDEST_XACT, APPL_NAME for Log Full Alert Enhancement
select APPLID_HOLDING_OLDEST_XACT, APPL_NAME from IBMOTS.LOG_SPACE_USED_STATS;


