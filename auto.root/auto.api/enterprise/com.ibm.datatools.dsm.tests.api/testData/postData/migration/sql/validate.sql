-------------------------------------------------------------------------------
-- IBM Data Server Manager V2.1.5.0.
--
-- Purpose: You can use this script to verify that the product repository
--          tables and functions were created successfully.
--
-- Usage: Run the script and ensure no SQL errors are returned.
-------------------------------------------------------------------------------

--#SET TERMINATOR #

SELECT 1 FROM IBMPDQ.MANAGED_DATABASE WHERE(1=-1)#
SELECT 1 FROM IBMPDQ.MANAGED_DATABASE_PROPS WHERE(1=-1)#
SELECT 1 FROM IBMPDQ.PROFILE WHERE(1=-1)#
SELECT 1 FROM IBMPDQ.PROFILE_PROPS WHERE(1=-1)#
SELECT 1 FROM IBMPDQ.TAGS WHERE(1=-1)#
SELECT 1 FROM IBMPDQ.USERCRED WHERE(1=-1)#
SELECT 1 FROM IBMPDQ.COREPREF WHERE(1=-1)#
SELECT 1 FROM PROCMGMT.PROCDEF WHERE(1=-1)#
SELECT 1 FROM PROCMGMT.PROCHIST WHERE(1=-1)#
SELECT 1 FROM PROCMGMT.PROCDEF_PROPERTY WHERE(1=-1)#
SELECT 1 FROM PROCMGMT.PROCHIST_PROPERTY WHERE(1=-1)#
SELECT 1 FROM PROCMGMT.VERSION WHERE(1=-1)#
SELECT 1 FROM DSSCHED.QJOB_DETAILS WHERE(1=-1)#
SELECT 1 FROM DSSCHED.QJOB_LISTENERS WHERE(1=-1)#
SELECT 1 FROM DSSCHED.QTRIGGERS WHERE(1=-1)#
SELECT 1 FROM DSSCHED.QSIMPLE_TRIGGERS WHERE(1=-1)#
SELECT 1 FROM DSSCHED.QCRON_TRIGGERS WHERE(1=-1)#
SELECT 1 FROM DSSCHED.QBLOB_TRIGGERS WHERE(1=-1)#
SELECT 1 FROM DSSCHED.QTRIGGER_LISTENERS WHERE(1=-1)#
SELECT 1 FROM DSSCHED.QCALENDARS WHERE(1=-1)#
SELECT 1 FROM DSSCHED.QFIRED_TRIGGERS WHERE(1=-1)#
SELECT 1 FROM DSSCHED.QPAUSED_TRIGGER_GRPS WHERE(1=-1)#
SELECT 1 FROM DSSCHED.QSCHEDULER_STATE WHERE(1=-1)#
SELECT 1 FROM DSSCHED.QLOCKS WHERE(1=-1)#
SELECT 1 FROM DSSCHED.SCHED_VERSION WHERE(1=-1)#
SELECT 1 FROM DSJOBMGR.JOBS WHERE(1=-1)#
SELECT 1 FROM DSJOBMGR.JOBPROPS WHERE(1=-1)#
SELECT 1 FROM DSJOBMGR.SCHEDULE WHERE(1=-1)#
SELECT 1 FROM DSJOBMGR.SCHEDULEPROPS WHERE(1=-1)#
SELECT 1 FROM DSJOBMGR.JOBINSTANCE WHERE(1=-1)#
SELECT 1 FROM DSJOBMGR.NOTIFICATION WHERE(1=-1)#
SELECT 1 FROM DSJOBMGR.NOTIFICATIONPROPS WHERE(1=-1)#
SELECT 1 FROM DSJOBMGR.VERSION WHERE(1=-1)#
SELECT 1 FROM IBMPDQ.GROUPS WHERE(1=-1)#
SELECT 1 FROM IBMPDQ.DATABASE_TO_GROUPS_MAPPING WHERE(1=-1)#
SELECT 1 FROM IBMPDQ.GRP_VERSION WHERE(1=-1)#
SELECT 1 FROM IBMPDQ.MAN_DBHOSTPORT_VU WHERE(1=-1)#
SELECT 1 FROM IBMPDQ.GRP_ALLDBS_VU WHERE(1=-1)#
SELECT 1 FROM DSSHSV1.CATEGORY WHERE(1=-1)#
SELECT 1 FROM DSSHSV1.ALERT WHERE(1=-1)#
SELECT 1 FROM DSSHSV1.ALERT_PROPERTIES WHERE(1=-1)#
SELECT 1 FROM DSSHSV1.ALERT_COMMENTS WHERE(1=-1)#
SELECT 1 FROM DSSHSV1.SHS_VERSION WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.DBSTATUS WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.DPFPARTITIONINFO WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.PURESCALE_MEMBER WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.PURESCALE_CF WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.CLUSTER_HOST WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.PURESCALE_INSTANCE WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.APPCONNECTIONS WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.TBSPACE_UTILIZATION WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.LOG_UTILIZATION WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.TABLE_AVAILABLE WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.TBSPACE_CONT_UTILIZATION WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.HADR_OPERATIONAL WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.STORAGE_PATH WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.BLOCKING_LOG_ALERTS WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.MESSAGESNAPSHOT WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.SNAPOPENALERTREF WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.ALERT_CONFIG_PARAM WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.PURESCALE_HADR_STATS WHERE(1=-1)#
SELECT 1 FROM "DSSHSV1"."ALERT_NOTIFICATION" WHERE(1=-1)#
SELECT 1 FROM DSSHSV1.NOTIFICATION_VERSION WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.HADR_STATS WHERE(1=-1)#
SELECT 1 FROM "IBMOTS"."SQL_DIM" WHERE(1=-1)#
SELECT 1 FROM "IBMOTS"."OTSANLSOBJECT" WHERE(1=-1)#
SELECT 1 FROM "IBMOTS"."OTSANLSOBJECTSEGMENT" WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.DB2Z_APP_CONNECTIONS WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.DB2ZSYSPARMS WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.DB2ZMESSAGES WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.DB2Z_TBSPACE_INDEX_STATES WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.DB2ZDBSTATUS WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.DB2Z_TBSPACE_UTILIZATION WHERE(1=-1)#
SELECT 1 FROM HEALTHMETRICS.DB2Z_INDEX_UTILIZATION WHERE(1=-1)#
SELECT 1 FROM IBMOTS.QUERY_STATS WHERE(1=-1)#
SELECT 1 FROM IBMOTS.LOCK_WAITTIME_STATS WHERE(1=-1)#
SELECT 1 FROM IBMOTS.LOCK_IDLETIME_STATS WHERE(1=-1)#
SELECT 1 FROM IBMOTS.REAL_MEMORY_STATS WHERE(1=-1)#
SELECT 1 FROM IBMOTS.VIRTUAL_MEMORY_STATS WHERE(1=-1)#
SELECT 1 FROM IBMOTS.WORKLOAD_STATS WHERE(1=-1)#
SELECT 1 FROM IBMOTS.SORTING_STATS WHERE(1=-1)#
SELECT 1 FROM IBMOTS.IO_STATS WHERE(1=-1)#
SELECT 1 FROM IBMOTS.BUFFERPOOL_STATS WHERE(1=-1)#
SELECT 1 FROM IBMOTS.LOG_SPACE_USED_STATS WHERE(1=-1)#
SELECT 1 FROM IBMOTS.LOG_SNDLOG_SPACE_ALLOCATED_STATS WHERE(1=-1)#
SELECT 1 FROM IBMOTS.LOG_DRTY_PAGES_LOG_STATS WHERE(1=-1)#
SELECT 1 FROM IBMOTS.LOG_HADR WHERE(1=-1)#
SELECT 1 FROM IBMOTS.PAGE_RECLAIM_CF_STATS WHERE(1=-1)#
SELECT 1 FROM IBMOTS.VIRTUAL_MEMORY_CF_STATS WHERE(1=-1)#
SELECT 1 FROM IBMOTS.VERSION WHERE(1=-1)#
SELECT 1 FROM IBMPDQ.DASHBOARD_PROPERTIES WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_baseline."bottleSortOverflows" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_baseline."dbSummarySessions" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_baseline."memDbTotalUsed" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_baseline."monGetBufferpool" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_baseline."throughputAll" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_baseline."throughputAllWL" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_baseline."throughputSystem" WHERE(1=-1)#
SELECT 1 FROM IBM_RTMON_BASELINE.RTMON_USER_BASELINE_DATA WHERE(1=-1)#
SELECT 1 FROM IBM_RTMON_BASELINE.RTMON_USER_BASELINE_DIRECTORY WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."memDbTotalUsed" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."memDbTotalUsedDpf" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."memDBGlobal" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."monGetMemoryPool" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."monGetMemoryPoolDpf" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."monGetMemorySet" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."instMemSet" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."instMemSetDpf" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."bottleWaitTimes" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."bottleCpu" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."bottleDirectReads" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."bottleDirectWrites" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."bottleNumLocks" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."bottleLockWait" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."bottleSorts" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."bottleSortOverflows" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."bottleSortTime" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."bottleLogSpace" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."bottleRowsRead" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."bottleRowsMod" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."bottleRowsReturned" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."bottleElapsedTime" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."bottleFcmRw" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."bottleQueryCost" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."bottleMemory" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."bottleWlmQTime" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."bottleFedRowsRead" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."bottleFedWaitTotal" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."bottleFedWaitTime" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."monGetBufferpool" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."monGetBufferpoolDpf" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."monGetTablespace" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."monGetTablespaceDpf" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."monGetRoutine" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."sessions" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."throughputAll" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."throughputAllDpf" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."throughputConnection" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."throughputMember" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."throughputServiceclass" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."throughputWorkload" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."throughputSystem" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."dbSummarySessions" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."dbSummarySessionsDpf" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."monGetTransactionLog" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."monGetTransactionLogDpf" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."hadrRoleSummary" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."cfInfo" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."globalBufferPool" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."clusterCachePerf" WHERE(1=-1)#
SELECT 1 FROM IBM_RTMON_EVMON.LOCK_EVENT WHERE(1=-1)#
SELECT 1 FROM IBM_RTMON_EVMON.LOCK_PARTICIPANTS WHERE(1=-1)#
SELECT 1 FROM IBM_RTMON_EVMON.LOCK_PARTICIPANT_ACTIVITIES WHERE(1=-1)#
SELECT 1 FROM IBM_RTMON_EVMON.THRESH_VIOLATIONS WHERE(1=-1)#
SELECT 1 FROM IBM_RTMON_EVMON.EVENT_ACTIVITY WHERE(1=-1)#
SELECT 1 FROM IBM_RTMON_METADATA.RTMON_MAP_COL WHERE(1=-1)#
SELECT 1 FROM IBM_RTMON_METADATA.RTMON_MAP_DBCONN WHERE(1=-1)#
SELECT 1 FROM IBM_RTMON_METADATA.RTMON_MAP_QUERY WHERE(1=-1)#
SELECT 1 FROM IBM_RTMON_METADATA.RTMON_MAP_STMT_TYPE WHERE(1=-1)#
SELECT 1 FROM IBM_RTMON_METADATA.RTMON_MAP_SUBSET WHERE(1=-1)#
SELECT 1 FROM IBM_RTMON_METADATA.RTMON_MAP_UBASE WHERE(1=-1)#
SELECT 1 FROM IBM_RTMON_METADATA.RTMON_VERSION WHERE(1=-1)#
SELECT 1 FROM IBMOTS.SQL_FACT WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.BOTTLE_CPU WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.BOTTLE_DIRECT_READS WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.BOTTLE_DIRECT_WRITES WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.BOTTLE_ELAPSED_TIME WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.BOTTLE_FCMRW WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.BOTTLE_LOCK_WAIT WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.BOTTLE_LOG_SPACE WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.BOTTLE_MEMORY WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.BOTTLE_NUM_LOCKS WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.BOTTLE_QUERY_COST WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.BOTTLE_ROWS_MOD WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.BOTTLE_ROWS_READ WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.BOTTLE_ROWS_RETURNED WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.BOTTLE_SORT_OVERFLOWS WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.BOTTLE_SORTS WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.BOTTLE_SORT_TIME WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.BOTTLE_WAIT_TIMES WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.BOTTLE_WLM_QTIME WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.BOTTLE_FED_ROWS_READ WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.BOTTLE_FED_WAITS_TOTAL WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.BOTTLE_FED_WAIT_TIME WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.DB_SUMMARY_SESSIONS WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.DB_SUMMARY_SESSIONS_DPF WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.INST_MEM_SET WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.INST_MEM_SET_DPF WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.MEM_DB_GLOBAL WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.MEM_DB_TOTAL_USED WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.MEM_DB_TOTAL_USED_DPF WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.MON_GET_BUFFERPOOL WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.MON_GET_BUFFERPOOL_DPF WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.MON_GET_MEMORY_POOL WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.MON_GET_MEMORY_POOL_DPF WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.MON_GET_MEMORY_SET WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.MON_GET_ROUTINE WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.MON_GET_TABLESPACE WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.MON_GET_TABLESPACE_DPF WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.MON_GET_TRANSACTION_LOG WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.MON_GET_TRANSACTION_LOG_DPF WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.SESSIONS WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.THROUGHPUT_ALL WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.THROUGHPUT_ALL_DPF WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.THROUGHPUT_CONNECTION WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.THROUGHPUT_MEMBER WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.THROUGHPUT_SERVICECLASS WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.THROUGHPUT_SYSTEM WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.THROUGHPUT_WORKLOAD WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.HADR_ROLE_SUMMARY WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.CF_INFO WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.GLOBAL_BUFFER_POOL WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.CLUSTER_CACHE_PERF WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."throughputAllWL" WHERE(1=-1)#
SELECT 1 FROM IBM_RTMON_EVMON.UTIL_EVMON WHERE(1=-1)#
SELECT 1 FROM IBM_RTMON_EVMON.EVMON_CODE_NAME WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."wlmQueueStats" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."wlmWorkloadStats" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."wlmServiceSubclass" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."wlmMonWorkloads" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."wlmServiceSubclassStats" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."wlmSystemResource" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."pgStatDatabase" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."pgStatConn" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."pgStatActivity" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."pgStatAllTables" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."mongoOverview" WHERE(1=-1)#
SELECT 1 FROM ibm_rtmon_data."pgStatements" WHERE(1=-1)#
SELECT 1 FROM IBM_DSM_VIEWS.THROUGHPUT_ALL_WL WHERE(1=-1)#
SELECT 1 FROM IBMPDQ.ADMIN_INSTANCE_PROPERTY WHERE(1=-1)#
SELECT 1 FROM IBMPDQ.ADMIN_INSTANCE_DATABASE WHERE(1=-1)#
SELECT 1 FROM IBMPDQ.ADMIN_INSTANCE_NODE WHERE(1=-1)#
SELECT 1 FROM IBMPDQ.ADMIN_INSTANCE_REGISTER_VARIABLE WHERE(1=-1)#
SELECT 1 FROM IBMPDQ.ADMIN_INSTANCE_SDNODE WHERE(1=-1)#
SELECT 1 FROM IBMOTS.TRACK_ACCESS_PLAN_DATA WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.CONTACT WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.CONTACTLOCATORTYPE WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.CONTACTLOCATOR WHERE(1=-1)#
SELECT 1 FROM IBMIOCM."TYPE" WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.CI WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.TYPEATTRIB WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.CIATTRIB WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.CIATTRIBHIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.CISTATUSHIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.CIHIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.RELATION WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.CIRELATION WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.RELATIONRULES WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.CIRELATIONHIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.REPORT_TYPE WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.TEMPLATES WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.CATEGORY WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.TEMPLATE_TRACKING_OBJECTS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.CATEGORY_TRACKING_OBJECTS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.TRACKING_OBJECTS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.TRACKING_DETAILS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.TRACK_JOB_SUMMARY WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.TRACK_JOB_DETAILS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DISCOVER_DBREF WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.PREF_PROP_GROUPS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.PREF_PROPERTIES WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.PREF_PROP_HISTORY WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.RULESET WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.CLIENT_RULE WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.CLIENT_RULE_TAG WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.CLIENT_RULE_TAG_JOIN WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.CLIENT_RULE_COND WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.CLIENT_RULE_SETTING WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.CLIENT_RULE_SP_SETTING WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.MODELS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.STORAGE_ITEMS_INFO WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.SELDOM_USED_ITEM_INFO WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.SELDOM_USED_ITEM_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.COMPRESSION_SAVINGS_INFO WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.RECLAIM_STORAGE_INFO WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.RECLAIM_STORAGE_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.STORAGE_JOB_SUMMARY WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.STORAGE_JOB_DETAILS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.MIGRATION_RULES_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.MIGRATION_JOB_SUMMARY WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.MIGRATION_ALTER_JOBS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.MIGRATION_RULES_USED WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.COMPARE_JOB_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.COMPARE_JOB_HIS_DETAILS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.HISTORY WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DOCSTORE_FOLDER WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DOCSTORE_OBJECT WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.TRACKING_ITEM_HISTORY WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.MGCLIENT_DRIVER_DESC WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.MGCLIENT_DRIVER_DESC_PROP WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.MGCLIENT_DRIVER_DESC_IPADDR WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.MGCLIENT_DATASOURCE_DESC WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.MGCLIENT_DATASOURCE_DESC_PROP WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.MGCLIENT_DATASOURCE_CLIENT_INFO WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.MGCLIENT_DATASOURCE_WAS_POOL_STATS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.MGCLIENT_DATASOURCE_CONN_TIME WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.MGCLIENT_DRIVER_DESC_PROP_HIST WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.MGCLIENT_DATASOURCE_DESC_PROP_HIST WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.CLIENT_RULE_CHANGE_TIME WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.ADMIN_TASKS_STATUS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.ADBCHGOCM WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.ADBCSTMT WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_COLUMN WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_COLUMN_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_COLUMNAUTH WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_COLUMNAUTH_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_DBAUTH WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_DBAUTH_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_INDEX WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_INDEX_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_INDEXAUTH WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_INDEXAUTH_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_PACKAGE WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_PACKAGE_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_PACKAGEAUTH WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_PACKAGEAUTH_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_ROLEAUTH WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_ROLEAUTH_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_ROUTINE WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_ROUTINE_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_ROUTINEAUTH WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_ROUTINEAUTH_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_SCHEMAAUTH WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_SCHEMAAUTH_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_SEQUENCEAUTH WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_SEQUENCEAUTH_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_TABLE WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_TABLE_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_TABLEAUTH WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_TABLEAUTH_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_TABLESPACE WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_TABLESPACE_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_TABLESPACEAUTH WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_TABLESPACEAUTH_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_TRIGGER WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_TRIGGER_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_USERAUTH WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_USERAUTH_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_VIEW WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_VIEW_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_BUFFERPOOL WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_BUFFERPOOL_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_BUFFERPOOLDBPARTITION WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_BUFFERPOOLDBPARTITION_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_BUFFERPOOLEXCEPTION WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_BUFFERPOOLEXCEPTION_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_BUFFERPOOLNODE WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_BUFFERPOOLNODE_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.COMPARE_JOB_MAPPINGS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.COMPARE_JOB_MAPPINGS_PROPS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_DBCFG WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_DBCFG_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_DBMCFG WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_DBMCFG_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_DB_HISTORY WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_DB_HISTORY_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_KEYVALUE WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_KEYVALUE_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_DATAPARTITION WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_DATAPARTITION_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_REG_VARIABLES WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_REG_VARIABLES_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_FEATURE_INFO WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_FEATURE_INFO_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_PROD_INFO WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_PROD_INFO_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_MONTABLE WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_MONTABLE_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_MONINDEX WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_MONINDEX_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_MONTABLESPACE WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DB2LUW_MONTABLESPACE_HIS WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.DASHBOARD_FAVORITE WHERE(1=-1)#
SELECT 1 FROM IBMIOCM.PRUNING_LIST WHERE(1=-1)#
SELECT 1 FROM IBMOTS.MGCLIENT_MON_STMT_DATA WHERE(1=-1)#
SELECT 1 FROM IBMOTS.MGCLIENT_MON_TXN_DATA WHERE(1=-1)#
SELECT 1 FROM IBMOTS.MGCLIENT_MON_SETTING_DATASOURCE WHERE(1=-1)#
SELECT 1 FROM IBMOTS.MGCLIENT_MON_SETTING WHERE(1=-1)#
SELECT 1 FROM IBMOTS.MGCLIENT_MON_SETTING_ALT_SERVER_IPADDR WHERE(1=-1)#
SELECT 1 FROM IBMOTS.MGCLIENT_MON_SETTING_CLIENT_INFO_MASK WHERE(1=-1)#
SELECT 1 FROM QUERYTUNER.OQWT_GROUP WHERE(1=-1)#
SELECT 1 FROM QUERYTUNER.OQWT_NODE WHERE(1=-1)#
SELECT 1 FROM QUERYTUNER.OQWT_QUERY_RESULT WHERE(1=-1)#
SELECT 1 FROM QUERYTUNER.OQWT_QUERY_RESULT_DETAILS WHERE(1=-1)#
SELECT 1 FROM QUERYTUNER.OQWT_STMT WHERE(1=-1)#
SELECT 1 FROM QUERYTUNER.OQWT_TUNING_PREF WHERE(1=-1)#
SELECT 1 FROM QUERYTUNER.OQWT_TUNING_PREF_PROPERTIES WHERE(1=-1)#
SELECT 1 FROM QUERYTUNER.OQWT_VERSION WHERE(1=-1)#
SELECT 1 FROM SYSTOOLS.QT_WCC_WORKLOAD_NAME WHERE(1=-1)#
SELECT 1 FROM SYSTOOLS.QT_WCC_WORKLOAD WHERE(1=-1)#
SELECT 1 FROM SYSTOOLS.QT_WCC_WORKLOAD_SOURCE WHERE(1=-1)#
SELECT 1 FROM SYSTOOLS.QT_WCC_WORKLOAD_SOURCE_FILTER WHERE(1=-1)#
SELECT 1 FROM SYSTOOLS.QT_WCC_WORKLOAD_TASK WHERE(1=-1)#
SELECT 1 FROM SYSTOOLS.QT_WCC_EVENT_HISTORY WHERE(1=-1)#
SELECT 1 FROM SYSTOOLS.QT_WCC_ERROR_MESSAGE WHERE(1=-1)#
SELECT 1 FROM SYSTOOLS.QT_WCC_STMT_TEXT WHERE(1=-1)#
SELECT 1 FROM SYSTOOLS.QT_WCC_STMT_INSTANCE WHERE(1=-1)#
SELECT 1 FROM SYSTOOLS.QT_WCC_STMT_EXPLAIN_INFO WHERE(1=-1)#
SELECT 1 FROM SYSTOOLS.QT_WCC_WORKLOAD_ADVISOR_INFO WHERE(1=-1)#
SELECT 1 FROM SYSTOOLS.QT_WCC_STMT_RUNTIME_METRICS WHERE(1=-1)#
SELECT 1 FROM SYSTOOLS.QT_WCC_TEMP_CAPTURE WHERE(1=-1)#
SELECT 1 FROM SYSTOOLS.QT_WCC_TEMP_EXPLAIN_HANDLE WHERE(1=-1)#
SELECT 1 FROM SYSTOOLS.QT_WCC_STMT_DEPENDENT_OBJECT WHERE(1=-1)#
SELECT 1 FROM SYSTOOLS.QT_WAPC_SESSIONS WHERE(1=-1)#
SELECT 1 FROM SYSTOOLS.QT_WAPC_SESSIONS_RESULT WHERE(1=-1)#
SELECT 1 FROM SYSTOOLS.QT_WAPC_SESSION_EXCEPTIONS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_SA_PROFILES WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_STMT_TEXTS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_CAP_TMP_RS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_WORKLOADS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_WL_SOURCES WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_SOURCE_DTL WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_WL_AUTHIDS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_EV_HISTORY WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_MESSAGES WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_EP_HISTORY WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_STMT_INSTS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_INST_SUMMY WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_STMT_RUNTM WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_OBJ_RUNTM WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_ERROR_MESSAGE WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_TASKS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_TB_STATUS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_WL_INFO WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_STMT_INFO WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_RP_TBS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_RP_TBS_HIS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_RP_IDXES WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_RP_IDX_HIS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_RP_STMTS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_STMT_RTDS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_DGTT_INFO WHERE(1=-1)#
SELECT 1 FROM DB2OSC.DSN_WCC_STMT_V2 WHERE(1=-1)#
SELECT 1 FROM "DB2OSC"."DSN_WCC_PLAN_TABLE" WHERE(1=-1)#
SELECT 1 FROM "DB2OSC"."DSN_WCC_STMT_V3" WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WSA_SESSIONS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WSA_DATABASES WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WSA_TSS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WSA_TABLES WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WSA_COLUMNS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WSA_COLGROUPS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WSA_CGFREQS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WSA_CGHISTS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WSA_INDEXES WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WSA_KEYS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WSA_KEYTARGETS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WSA_KTGS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WSA_KTGFREQS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WSA_KTGHISTS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WSA_LITERALS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WSA_ADVICE WHERE(1=-1)#
SELECT 1 FROM "DB2OE".DSN_WIA_SESSION WHERE(1=-1)#
SELECT 1 FROM "DB2OE".DSN_WIA_TABLES WHERE(1=-1)#
SELECT 1 FROM "DB2OE".DSN_WIA_COLUMNS WHERE(1=-1)#
SELECT 1 FROM "DB2OE".DSN_WIA_INDEXES WHERE(1=-1)#
SELECT 1 FROM "DB2OE".DSN_WIA_KEYS WHERE(1=-1)#
SELECT 1 FROM "DB2OE".DSN_WIA_COL_SEQ WHERE(1=-1)#
SELECT 1 FROM "DB2OE".DSN_WIA_STMT WHERE(1=-1)#
SELECT 1 FROM "DB2OE".DSN_WIA_QBLOCK WHERE(1=-1)#
SELECT 1 FROM "DB2OE".DSN_WIA_TAB_REF WHERE(1=-1)#
SELECT 1 FROM "DB2OE".DSN_WIA_PREDICATE WHERE(1=-1)#
SELECT 1 FROM "DB2OE".DSN_WIA_GBOBDIST WHERE(1=-1)#
SELECT 1 FROM "DB2OE".DSN_WIA_SESION_HIS WHERE(1=-1)#
SELECT 1 FROM "DB2OE".DSN_WIA_COL_REF WHERE(1=-1)#
SELECT 1 FROM "DB2OE".DSN_WIA_COLSEQ_KEY WHERE(1=-1)#
SELECT 1 FROM "DB2OE".DSN_WIA_TABREF_IDX WHERE(1=-1)#
SELECT 1 FROM "DB2OE".DSN_WIA_ERROR_HIST WHERE(1=-1)#
SELECT 1 FROM "DB2OE".DSN_WIA_HC_RESULT WHERE(1=-1)#
SELECT 1 FROM "DB2OE".DSN_WIA_COLUMN_INFO WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".AOC_WAPC_SESSIONS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".AOC_WAPC_RS WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".AOC_WAPC_EX WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".AOC_STATS_HIST WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_HOSTV_SET WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_HOSTV_PRED WHERE(1=-1)#
SELECT 1 FROM "DB2OSC".DSN_WCC_HOSTV_VAL WHERE(1=-1)#
SELECT 1 FROM QUERYTUNER.OQWT_CAP_FILTER WHERE(1=-1)#
SELECT 1 FROM QUERYTUNER.OQWT_CAP_FILTER_COND WHERE(1=-1)#
SELECT 1 FROM QUERYTUNER.OQWT_PREFERENCE WHERE(1=-1)#
SELECT 1 FROM IBMPDQ.ADMIN_EDITOR_SCRIPTS WHERE(1=-1)#
SELECT 1 FROM IBMPDQ.ADMIN_EDITOR_SCRIPTS_SUBSCRIBE WHERE(1=-1)#
SELECT 1 FROM IBMPDQ.ADMIN_EDITOR_PREF WHERE(1=-1)#
SELECT 1 FROM IBMPDQ.ADMIN_EDITOR_RESULT WHERE(1=-1)#
