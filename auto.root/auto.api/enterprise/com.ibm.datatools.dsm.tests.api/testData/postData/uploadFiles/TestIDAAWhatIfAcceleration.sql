with customer_total_return as
 (select sr_customer_sk as ctr_customer_sk
        ,sr_store_sk as ctr_store_sk, 
 	sum(sr_return_amt) as ctr_total_return
 from tpcds.store_returns
     ,tpcds.date_dim
 where sr_returned_date_sk = d_date_sk 
   and d_year =2000 
 group by sr_customer_sk
         ,sr_store_sk)
 select c_customer_id
 from customer_total_return ctr1
     ,tpcds.store
     ,tpcds.customer
 where ctr1.ctr_total_return > (select avg(ctr_total_return)*1.2
 			  from customer_total_return ctr2 
                  	  where ctr1.ctr_store_sk = ctr2.ctr_store_sk)
       and s_store_sk = ctr1.ctr_store_sk
       and s_state = 'TN'
       and ctr1.ctr_customer_sk = c_customer_sk
 order by c_customer_id;

select 
  cd_gender,
  cd_marital_status,
  cd_education_status,
  count(*),
  cd_purchase_estimate,
  count(*),
  cd_credit_rating,
  count(*),
  cd_dep_count,
  count(*),
  cd_dep_employed_count,
  count(*),
  cd_dep_college_count,
  count(*)
 from
  tpcds.customer c,tpcds.customer_address ca,tpcds.customer_demographics
 where
  c.c_current_addr_sk = ca.ca_address_sk and
  ca_county in ('Rush County','Toole County','Jefferson County','Dona Ana County','La Porte County') and
  cd_demo_sk = c.c_current_cdemo_sk and 
  exists (select *
          from tpcds.store_sales,tpcds.date_dim
          where c.c_customer_sk = ss_customer_sk and
                ss_sold_date_sk = d_date_sk and
                d_year = 2002 and
                d_moy between 1 and 1+3) and
   (exists (select *
            from tpcds.web_sales,tpcds.date_dim
            where c.c_customer_sk = ws_bill_customer_sk and
                  ws_sold_date_sk = d_date_sk and
                  d_year = 2002 and
                  d_moy between 1 ANd 1+3) or 
    exists (select * 
            from tpcds.catalog_sales,tpcds.date_dim
            where c.c_customer_sk = cs_ship_customer_sk and
                  cs_sold_date_sk = d_date_sk and
                  d_year = 2002 and
                  d_moy between 1 and 1+3))
 group by cd_gender,
          cd_marital_status,
          cd_education_status,
          cd_purchase_estimate,
          cd_credit_rating,
          cd_dep_count,
          cd_dep_employed_count,
          cd_dep_college_count
 order by cd_gender,
          cd_marital_status,
          cd_education_status,
          cd_purchase_estimate,
          cd_credit_rating,
          cd_dep_count,
          cd_dep_employed_count,
          cd_dep_college_count;

with year_total as (
 select c_customer_id customer_id
       ,c_first_name customer_first_name
       ,c_last_name customer_last_name
       ,c_preferred_cust_flag
       ,c_birth_country
       ,c_login
       ,c_email_address
       ,d_year dyear
       ,sum(ss_ext_list_price-ss_ext_discount_amt) year_total
       ,'s' sale_type
 from tpcds.customer
     ,tpcds.store_sales
     ,tpcds.date_dim
 where c_customer_sk = ss_customer_sk
   and ss_sold_date_sk = d_date_sk
 group by c_customer_id
         ,c_first_name
         ,c_last_name
         ,d_year
         ,c_preferred_cust_flag
         ,c_birth_country
         ,c_login
         ,c_email_address
         ,d_year 
 union all
 select c_customer_id customer_id
       ,c_first_name customer_first_name
       ,c_last_name customer_last_name
       ,c_preferred_cust_flag
       ,c_birth_country
       ,c_login
       ,c_email_address
       ,d_year dyear
       ,sum(ws_ext_list_price-ws_ext_discount_amt) year_total
       ,'w' sale_type
 from tpcds.customer
     ,tpcds.web_sales
     ,tpcds.date_dim
 where c_customer_sk = ws_bill_customer_sk
   and ws_sold_date_sk = d_date_sk
 group by c_customer_id
         ,c_first_name
         ,c_last_name
         ,c_preferred_cust_flag
         ,c_birth_country
         ,c_login
         ,c_email_address
         ,d_year
         )
  select t_s_secyear.customer_id, t_s_secyear.customer_first_name, t_s_secyear.customer_last_name
        ,t_s_secyear.c_preferred_cust_flag,t_s_secyear.c_birth_country,t_s_secyear.c_login
 from year_total t_s_firstyear
     ,year_total t_s_secyear
     ,year_total t_w_firstyear
     ,year_total t_w_secyear
 where t_s_secyear.customer_id = t_s_firstyear.customer_id
         and t_s_firstyear.customer_id = t_w_secyear.customer_id
         and t_s_firstyear.customer_id = t_w_firstyear.customer_id
         and t_s_firstyear.sale_type = 's'
         and t_w_firstyear.sale_type = 'w'
         and t_s_secyear.sale_type = 's'
         and t_w_secyear.sale_type = 'w'
         and t_s_firstyear.dyear = 2001
         and t_s_secyear.dyear = 2001+1
         and t_w_firstyear.dyear = 2001
         and t_w_secyear.dyear = 2001+1
         and t_s_firstyear.year_total > 0
         and t_w_firstyear.year_total > 0
         and case when t_w_firstyear.year_total > 0 then t_w_secyear.year_total / t_w_firstyear.year_total else null end
             > case when t_s_firstyear.year_total > 0 then t_s_secyear.year_total / t_s_firstyear.year_total else null end
 order by t_s_secyear.customer_id, t_s_secyear.customer_first_name, t_s_secyear.customer_last_name
        ,t_s_secyear.c_preferred_cust_flag,t_s_secyear.c_birth_country,t_s_secyear.c_login
;

select i_item_desc 
      ,i_category 
      ,i_class 
      ,i_current_price
      ,sum(ws_ext_sales_price) as itemrevenue 
      ,sum(ws_ext_sales_price)*100/sum(sum(ws_ext_sales_price)) over
          (partition by i_class) as revenueratio
from	
	tpcds.web_sales
    	,tpcds.item 
    	,tpcds.date_dim
where 
	ws_item_sk = i_item_sk 
  	and i_category in ('Sports', 'Books', 'Home')
  	and ws_sold_date_sk = d_date_sk
	and d_date between cast('1999-02-22' as date) 
				and (cast('1999-02-22' as date) + 30 days)
group by 
	i_item_id
        ,i_item_desc 
        ,i_category
        ,i_class
        ,i_current_price
order by 
	i_category
        ,i_class
        ,i_item_id
        ,i_item_desc
        ,revenueratio;

select i_item_desc 
      ,i_category 
      ,i_class 
      ,i_current_price
      ,sum(ws_ext_sales_price) as itemrevenue 
      ,sum(ws_ext_sales_price)*100/sum(ws_ext_sales_price) as revenueratio
from	
	tpcds.web_sales
    	,tpcds.item 
    	,tpcds.date_dim
where 
	ws_item_sk = i_item_sk 
  	and i_category in ('Sports', 'Books', 'Home')
  	and ws_sold_date_sk = d_date_sk
	and d_date between cast('1999-02-22' as date) 
				and (cast('1999-02-22' as date) + 30 days)
group by 
	i_item_id
        ,i_item_desc 
        ,i_category
        ,i_class
        ,i_current_price
order by 
	i_category
        ,i_class
        ,i_item_id
        ,i_item_desc
        ,revenueratio;

select avg(ss_quantity), avg(ss_ext_sales_price), avg(ss_ext_wholesale_cost), sum(ss_ext_wholesale_cost)
 from tpcds.store_sales, tpcds.store, tpcds.customer_demographics, tpcds.household_demographics, tpcds.customer_address, tpcds.date_dim
 where s_store_sk = ss_store_sk
 and  ss_sold_date_sk = d_date_sk and d_year = 2001
 and  
 (
  (
   ss_hdemo_sk=hd_demo_sk
   and
   cd_demo_sk = ss_cdemo_sk
   and 
   cd_marital_status = 'M'
   and 
   cd_education_status = 'Advanced Degree'
   and 
   ss_sales_price between 100.00 and 150.00
   and
   hd_dep_count = 3   
   )
 or
  (
   ss_hdemo_sk=hd_demo_sk
   and
   cd_demo_sk = ss_cdemo_sk
   and 
   cd_marital_status = 'S'
   and 
   cd_education_status = 'College'
   and 
   ss_sales_price between 50.00 and 100.00   
   and
   hd_dep_count = 1
  )
 or 
 (
  ss_hdemo_sk=hd_demo_sk
  and
  cd_demo_sk = ss_cdemo_sk
  and 
   cd_marital_status = 'W'
   and 
   cd_education_status = '2 yr Degree'
   and 
   ss_sales_price between 150.00 and 200.00 
   and
   hd_dep_count = 1  
 )
 )
 and
 (
  (
  ss_addr_sk = ca_address_sk
  and
  ca_country = 'United States'
  and
  ca_state in ('TX', 'OH', 'TX')
  and ss_net_profit between 100 and 200  
  )
 or
  (ss_addr_sk = ca_address_sk
  and
  ca_country = 'United States'
  and
  ca_state in ('OR', 'NM', 'KY')
  and ss_net_profit between 150 and 300  
  )
 or
  (ss_addr_sk = ca_address_sk
  and
  ca_country = 'United States'
  and
  ca_state in ('VA', 'TX', 'MS')
  and ss_net_profit between 50 and 250  
  )
 );

select iss.i_brand_id as brand_id
     ,iss.i_class_id class_id
     ,iss.i_category_id category_id
 from tpcds.store_sales
     ,tpcds.item iss
     ,tpcds.date_dim d1
 where ss_item_sk = iss.i_item_sk
   and ss_sold_date_sk = d1.d_date_sk
   and d1.d_year between 1999 AND 1999 + 2
 intersect 
 select ics.i_brand_id
     ,ics.i_class_id
     ,ics.i_category_id
 from tpcds.catalog_sales
     ,tpcds.item ics
     ,tpcds.date_dim d2
 where cs_item_sk = ics.i_item_sk
   and cs_sold_date_sk = d2.d_date_sk
   and d2.d_year between 1999 AND 1999 + 2
 intersect
 select iws.i_brand_id
     ,iws.i_class_id
     ,iws.i_category_id
 from tpcds.web_sales
     ,tpcds.item iws
     ,tpcds.date_dim d3
 where ws_item_sk = iws.i_item_sk
   and ws_sold_date_sk = d3.d_date_sk
   and d3.d_year between 1999 AND 1999 + 2;

with  cross_items as
 (select i_item_sk ss_item_sk
 from tpcds.item,
 (select iss.i_brand_id brand_id
     ,iss.i_class_id class_id
     ,iss.i_category_id category_id
 from tpcds.store_sales
     ,tpcds.item iss
     ,tpcds.date_dim d1
 where ss_item_sk = iss.i_item_sk
   and ss_sold_date_sk = d1.d_date_sk
   and d1.d_year between 1999 AND 1999 + 2
 intersect 
 select ics.i_brand_id
     ,ics.i_class_id
     ,ics.i_category_id
 from tpcds.catalog_sales
     ,tpcds.item ics
     ,tpcds.date_dim d2
 where cs_item_sk = ics.i_item_sk
   and cs_sold_date_sk = d2.d_date_sk
   and d2.d_year between 1999 AND 1999 + 2
 intersect
 select iws.i_brand_id
     ,iws.i_class_id
     ,iws.i_category_id
 from tpcds.web_sales
     ,tpcds.item iws
     ,tpcds.date_dim d3
 where ws_item_sk = iws.i_item_sk
   and ws_sold_date_sk = d3.d_date_sk
   and d3.d_year between 1999 AND 1999 + 2) as x (brand_id, class_id, category_id)
 where i_brand_id = brand_id
      and i_class_id = class_id
      and i_category_id = category_id
),
 avg_sales as
 (select avg(quantity*list_price) average_sales
  from table (select ss_quantity quantity
             ,ss_list_price list_price
       from tpcds.store_sales
           ,tpcds.date_dim
       where ss_sold_date_sk = d_date_sk
         and d_year between 1999 and 2001 
       union all 
       select cs_quantity quantity 
             ,cs_list_price list_price
       from tpcds.catalog_sales
           ,tpcds.date_dim
       where cs_sold_date_sk = d_date_sk
         and d_year between 1999 and 1999 + 2 
       union all
       select ws_quantity quantity
             ,ws_list_price list_price
       from tpcds.web_sales
           ,tpcds.date_dim
       where ws_sold_date_sk = d_date_sk
         and d_year between 1999 and 1999 + 2) x)
 select channel, i_brand_id,i_class_id,i_category_id,sum(sales), sum(number_sales)
 from(
       select 'store' channel, i_brand_id,i_class_id
             ,i_category_id,sum(ss_quantity*ss_list_price) sales
             , count(*) number_sales
       from tpcds.store_sales
           ,tpcds.item
           ,tpcds.date_dim
       where ss_item_sk in (select ss_item_sk from cross_items)
         and ss_item_sk = i_item_sk
         and ss_sold_date_sk = d_date_sk
         and d_year = 1999+2 
         and d_moy = 11
       group by i_brand_id,i_class_id,i_category_id
       having sum(ss_quantity*ss_list_price) > (select average_sales from avg_sales)
       union all
       select 'catalog' channel, i_brand_id,i_class_id,i_category_id, sum(cs_quantity*cs_list_price) sales, count(*) number_sales
       from tpcds.catalog_sales
           ,tpcds.item
           ,tpcds.date_dim
       where cs_item_sk in (select ss_item_sk from cross_items)
         and cs_item_sk = i_item_sk
         and cs_sold_date_sk = d_date_sk
         and d_year = 1999+2 
         and d_moy = 11
       group by i_brand_id,i_class_id,i_category_id
       having sum(cs_quantity*cs_list_price) > (select average_sales from avg_sales)
       union all
       select 'web' channel, i_brand_id,i_class_id,i_category_id, sum(ws_quantity*ws_list_price) sales , count(*) number_sales
       from tpcds.web_sales
           ,tpcds.item
           ,tpcds.date_dim
       where ws_item_sk in (select ss_item_sk from cross_items)
         and ws_item_sk = i_item_sk
         and ws_sold_date_sk = d_date_sk
         and d_year = 1999+2
         and d_moy = 11
       group by i_brand_id,i_class_id,i_category_id
       having sum(ws_quantity*ws_list_price) > (select average_sales from avg_sales)
 ) y
 group by channel, i_brand_id,i_class_id,i_category_id
 order by channel,i_brand_id,i_class_id,i_category_id
 ;

with cross_items as
 (select
  ss_item_sk
  from tpcds.store_sales
     ,tpcds.catalog_sales
     ,tpcds.web_sales
     ,tpcds.item iss
     ,tpcds.item ics
     ,tpcds.item iws
     ,tpcds.date_dim d1
     ,tpcds.date_dim d2
     ,tpcds.date_dim d3
  where ss_item_sk = iss.i_item_sk
   and cs_item_sk = ics.i_item_sk
   and ws_item_sk = iws.i_item_sk
   and ss_customer_sk = cs_bill_customer_sk
   and cs_bill_customer_sk = ws_bill_customer_sk
   and iss.i_brand_id = ics.i_brand_id
   and ics.i_brand_id = iws.i_brand_id
   and iss.i_class_id = ics.i_class_id
   and ics.i_class_id = iws.i_class_id
   and iss.i_category_id = ics.i_category_id
   and ics.i_category_id = iws.i_category_id
   and ss_sold_date_sk = d1.d_date_sk
   and cs_sold_date_sk = d2.d_date_sk
   and ws_sold_date_sk = d3.d_date_sk
   and d1.d_year between 1999 and 1999 + 2 
   and d2.d_year between 1999 and 1999 + 2 
   and d3.d_year between 1999 and 1999 + 2
 ),
 avg_sales as
 (select avg(quantity*list_price) average_sales
  from (select ss_quantity quantity
             ,ss_list_price list_price
       from tpcds.store_sales
           ,tpcds.date_dim
       where ss_sold_date_sk = d_date_sk
         and d_year between 1999 and 1999 + 2
       union all
       select cs_quantity quantity
             ,cs_list_price list_price
       from tpcds.catalog_sales
           ,tpcds.date_dim
       where cs_sold_date_sk = d_date_sk
         and d_year between 1999 and 1999 + 2
       union all
       select ws_quantity quantity
             ,ws_list_price list_price
       from tpcds.web_sales
           ,tpcds.date_dim
       where ws_sold_date_sk = d_date_sk
         and d_year between 1999 and 1999 + 2) x)
 select 'store' channel, i_brand_id,i_class_id,i_category_id
       ,sum(ss_quantity*ss_list_price) sales
       ,count(*) number_sales
 from tpcds.store_sales 
     ,tpcds.item
     ,tpcds.date_dim
 where ss_item_sk in 
(select ss_item_sk from cross_items)
   and ss_item_sk = i_item_sk
   and ss_sold_date_sk = d_date_sk
   and d_week_seq = (select d_week_seq
                     from tpcds.date_dim
                     where d_year = 1999+2
                       and d_moy = 11
                       and d_dom = 8)
 group by i_brand_id,i_class_id,i_category_id
 having sum(ss_quantity*ss_list_price) > (select average_sales from avg_sales);


with  cross_items as
 (select i_item_sk ss_item_sk
 from tpcds.item,
 (select iss.i_brand_id brand_id
     ,iss.i_class_id class_id
     ,iss.i_category_id category_id
 from tpcds.store_sales
     ,tpcds.item iss
     ,tpcds.date_dim d1
 where ss_item_sk = iss.i_item_sk
   and ss_sold_date_sk = d1.d_date_sk
   and d1.d_year between 1999 AND 1999 + 2
 intersect
 select ics.i_brand_id
     ,ics.i_class_id
     ,ics.i_category_id
 from tpcds.catalog_sales
     ,tpcds.item ics
     ,tpcds.date_dim d2
 where cs_item_sk = ics.i_item_sk
   and cs_sold_date_sk = d2.d_date_sk
   and d2.d_year between 1999 AND 1999 + 2
 intersect
 select iws.i_brand_id
     ,iws.i_class_id
     ,iws.i_category_id
 from tpcds.web_sales
     ,tpcds.item iws
     ,tpcds.date_dim d3
 where ws_item_sk = iws.i_item_sk
   and ws_sold_date_sk = d3.d_date_sk
   and d3.d_year between 1999 AND 1999 + 2) as x(brand_id, class_id, category_id)
 where i_brand_id = brand_id
      and i_class_id = class_id
      and i_category_id = category_id
),
avg_sales as
 (select avg(quantity*list_price) average_sales
  from (select ss_quantity quantity
             ,ss_list_price list_price
       from tpcds.store_sales
           ,tpcds.date_dim
       where ss_sold_date_sk = d_date_sk
         and d_year between 1999 and 1999 + 2
       union all
       select cs_quantity quantity
             ,cs_list_price list_price
       from tpcds.catalog_sales
           ,tpcds.date_dim
       where cs_sold_date_sk = d_date_sk
         and d_year between 1999 and 1999 + 2
       union all
       select ws_quantity quantity
             ,ws_list_price list_price
       from tpcds.web_sales
           ,tpcds.date_dim
       where ws_sold_date_sk = d_date_sk
         and d_year between 1999 and 1999 + 2) x)
 select * from
 (select 'store' channel, i_brand_id,i_class_id,i_category_id 
        ,sum(ss_quantity*ss_list_price) sales, count(*) number_sales
 from tpcds.store_sales 
     ,tpcds.item
     ,tpcds.date_dim
 where ss_item_sk in (select ss_item_sk from cross_items)
   and ss_item_sk = i_item_sk
   and ss_sold_date_sk = d_date_sk
   and d_week_seq = (select d_week_seq
                     from tpcds.date_dim
                     where d_year = 1999 + 2
                       and d_moy = 11
                       and d_dom = 8)
 group by i_brand_id,i_class_id,i_category_id
 having sum(ss_quantity*ss_list_price) > (select average_sales from avg_sales)) this_year,
 (select 'store' channel, i_brand_id,i_class_id,i_category_id, sum(ss_quantity*ss_list_price) sales, count(*) number_sales
 from tpcds.store_sales
     ,tpcds.item
     ,tpcds.date_dim
 where ss_item_sk in (select ss_item_sk from cross_items)
   and ss_item_sk = i_item_sk
   and ss_sold_date_sk = d_date_sk
   and d_week_seq = (select d_week_seq
                     from tpcds.date_dim
                     where d_year = 1999 + 1
                       and d_moy = 11
                       and d_dom = 8)
 group by i_brand_id,i_class_id,i_category_id
 having sum(ss_quantity*ss_list_price) > (select average_sales from avg_sales)) last_year
 where this_year.i_brand_id= last_year.i_brand_id
   and this_year.i_class_id = last_year.i_class_id
   and this_year.i_category_id = last_year.i_category_id
 ;


with  cross_items as
 (select i_item_sk ss_item_sk
 from tpcds.item,
 (select iss.i_brand_id brand_id
     ,iss.i_class_id class_id
     ,iss.i_category_id category_id
 from tpcds.store_sales
     ,tpcds.item iss
     ,tpcds.date_dim d1
 where ss_item_sk = iss.i_item_sk
   and ss_sold_date_sk = d1.d_date_sk
   and d1.d_year between 1999 AND 1999 + 2
 intersect
 select ics.i_brand_id
     ,ics.i_class_id
     ,ics.i_category_id
 from tpcds.catalog_sales
     ,tpcds.item ics
     ,tpcds.date_dim d2
 where cs_item_sk = ics.i_item_sk
   and cs_sold_date_sk = d2.d_date_sk
   and d2.d_year between 1999 AND 1999 + 2
 intersect
 select iws.i_brand_id
     ,iws.i_class_id
     ,iws.i_category_id
 from tpcds.web_sales
     ,tpcds.item iws
     ,tpcds.date_dim d3
 where ws_item_sk = iws.i_item_sk
   and ws_sold_date_sk = d3.d_date_sk
   and d3.d_year between 1999 AND 1999 + 2) as x(brand_id, class_id, category_id)
 where i_brand_id = brand_id
      and i_class_id = class_id
      and i_category_id = category_id
),
avg_sales as
 (select avg(quantity*list_price) average_sales
  from (select ss_quantity quantity
             ,ss_list_price list_price
       from tpcds.store_sales
           ,tpcds.date_dim
       where ss_sold_date_sk = d_date_sk
         and d_year between 1999 and 1999 + 2
       union all
       select cs_quantity quantity
             ,cs_list_price list_price
       from tpcds.catalog_sales
           ,tpcds.date_dim
       where cs_sold_date_sk = d_date_sk
         and d_year between 1999 and 1999 + 2
       union all
       select ws_quantity quantity
             ,ws_list_price list_price
       from tpcds.web_sales
           ,tpcds.date_dim
       where ws_sold_date_sk = d_date_sk
         and d_year between 1999 and 1999 + 2) x)
 select * from
 (select 'store' channel, i_brand_id,i_class_id,i_category_id
         ,sum(ss_quantity*ss_list_price) sales, count(*) number_sales
 from tpcds.store_sales 
     ,tpcds.item
     ,tpcds.date_dim
 where ss_item_sk in (select ss_item_sk from cross_items)
   and ss_item_sk = i_item_sk
   and ss_sold_date_sk = d_date_sk
   and d_week_seq = (select d_week_seq
                     from tpcds.date_dim
                     where d_year = 1999 + 1
                       and d_moy = 11
                       and d_dom = 8)
 group by i_brand_id,i_class_id,i_category_id
 having sum(ss_quantity*ss_list_price) > (select average_sales from avg_sales)) this_year,
 (select 'store' channel, i_brand_id,i_class_id
        ,i_category_id, sum(ss_quantity*ss_list_price) sales, count(*) number_sales
 from tpcds.store_sales
     ,tpcds.item
     ,tpcds.date_dim
 where ss_item_sk in (select ss_item_sk from cross_items)
   and ss_item_sk = i_item_sk
   and ss_sold_date_sk = d_date_sk
   and d_week_seq = (select d_week_seq
                     from tpcds.date_dim
                     where d_year = 1999
                       and d_moy = 11
                       and d_dom = 8)
 group by i_brand_id,i_class_id,i_category_id
 having sum(ss_quantity*ss_list_price) > (select average_sales from avg_sales)) last_year
 where this_year.i_brand_id= last_year.i_brand_id
   and this_year.i_class_id = last_year.i_class_id
   and this_year.i_category_id = last_year.i_category_id
 ;


