package com.ibm.datatools.test.utils;

public  class AdminConst {
	public static final String DB_VERSION_LUW_97 = "v9.7" ;
	public static final String DB_VERSION_LUW_101 = "v10.1" ;
	public static final String DB_VERSION_LUW_105 = "v10.5" ;
	public static final String DB_VERSION_Z_10 = "V10" ;
	public static final String DB_VERSION_Z_11 = "V11" ;
}
