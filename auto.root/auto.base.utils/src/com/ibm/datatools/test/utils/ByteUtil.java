package com.ibm.datatools.test.utils;

public class ByteUtil {
	
	public static byte [] Merge(byte [] a, byte [] b, byte [] c){
	
	  byte[] all = new byte[a.length+b.length+c.length];
	  	  
	  System.arraycopy(a,0,all,0,a.length);
	  System.arraycopy(b,0,all,a.length,b.length);
	  System.arraycopy(c,0,all,a.length+b.length,c.length);
	  
	  return all;
	
	}
}
