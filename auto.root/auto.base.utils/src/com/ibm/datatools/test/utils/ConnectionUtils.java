package com.ibm.datatools.test.utils;

import java.util.Properties;

/**
 * A const to reference the key under "database" folder.
 * 
 * */
public  class ConnectionUtils {
	
	public static String NAME = "name" ;
	public static String DATABASE_NAME = "databaseName" ;
	public static String HOST = "host" ;
	public static String PORT = "port" ;
	public static String USER = "user" ;
	public static String PASSWORD = "PASSWORD" ;
	public static String TIMEZONE = "timeZone" ;
	public static String SECURITY = "securityMechanism" ;
	public static String DATA_SERVER_TYPE = "dataServerType" ;
	
	public static String getDatabaseName(Properties p){
		return p.getProperty(DATABASE_NAME) ;
	}
	
	public static String getHost(Properties p){
		return p.getProperty(HOST) ;
	}
	
	public static int getPort(Properties p){
		return Integer.parseInt(p.getProperty(PORT)) ;
	}
	public static String getUser(Properties p){
		return p.getProperty(USER) ;
	}
	public static String getPassword(Properties p){
		return p.getProperty(PASSWORD) ;
	}
}
