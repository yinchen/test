package com.ibm.datatools.test.utils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CryptUtils {
    public static final String DSSERVER_HOME = "dsserver_home";
    public static final String DSM_CONFIG_DIR = System.getProperty(DSSERVER_HOME, System.getenv(DSSERVER_HOME)) + File.separator + "Config";
    
    public static final String 	DSM_ENCRYPTION_KEY = DSM_CONFIG_DIR + File.separator +"encryption.key";
	
    /**
     * Encrypt a string
     * @param str - the string to be encrypted
     * @return The encrypted string
     */
    public static String encryptString(String str)
    {
        WTIV2CryptHelper helper = new WTIV2CryptHelper();
        String customizeKey = getCustomizeKey() ;
        helper.setCustomAESKeySeeds(customizeKey);
        if (str != null)
        {
            String encryptedPswd;
            try
            {
                encryptedPswd = helper.encrypt(str);
            }
            catch (Exception e)
            {
                encryptedPswd = str;
                e.printStackTrace();
            }
            return encryptedPswd;
        }
        return null;
    }
    
    /**
     * Decrypt a string
     * 
     * @param str
     *            - the string to be decrypted
     * @return The decrypted string
     * @throws IOException
     */
    public static String decryptString(String str) throws IOException
    {
        if( (null !=str) && str.startsWith(WTIV2CryptHelper.PREFIX))
        {
            WTIV2CryptHelper helper = new WTIV2CryptHelper();
            String customizeKey = getCustomizeKey() ;
            helper.setCustomAESKeySeeds(customizeKey);
            try
            {
                return helper.decrypt(str);
            }
            catch (Exception e)
            {
                throw new IOException(e.getMessage());
            }
        }
        ResourceHelper resHelper = new ResourceHelper();
        if (str != null)
        {
            String decryptedPswd = resHelper.getPasswordFromHexEncoding(str);
            return decryptedPswd;
        }
        return null;
    }
    

 
   public static String  customizeKey = null ;
    
    /**
     * Check if there is encryption.key exists under Config folder, if have extract the content of this file
     *
     */
    private static String getCustomizeKey(){
    	if(customizeKey == null){
	       	 File wsp = new File(DSM_ENCRYPTION_KEY);
	         if (wsp.exists() && wsp.canRead())
	         {
	             try
	             {
	            	 customizeKey = new String(Files.readAllBytes(Paths.get(DSM_ENCRYPTION_KEY)),StandardCharsets.UTF_8) ;
	            	 if(customizeKey == null){
	            		 customizeKey = "" ;
	            	 }else{
	                   	  customizeKey = customizeKey.trim() ;
	            	 }
	             }
	             catch (IOException e)
	             {
	                 System.err.println("Extract encryption.key failed: " + e);
	                 customizeKey = "" ;
	             }
	         }
	         
	        
    	}
    	return customizeKey ;
    }
    public static void main(String[] args){
    	//Set the dsserver_home in java vm argument to test 
    	//-Ddsserver_home=c:   put encryption.key under Config folder under C:
    	String password = CryptUtils.encryptString("a") ;
    	System.out.println(password);
    	try{
    		String password_de = CryptUtils.decryptString(password) ;
    		System.out.println(password_de);
    		assert(password_de.equals("a"));
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
    }
    
}
