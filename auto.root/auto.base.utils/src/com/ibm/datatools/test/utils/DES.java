package com.ibm.datatools.test.utils;

public class DES {
	static final private String rcsid    =
		"$Id: CDMF.java,v 1.3 1996/08/05 13:36:39 daniel Exp $"; //$NON-NLS-1$
 
	public static final String rcsidString()   { return rcsid;   }
 
 static private long
	 stab[]={
	 0X00404100L, 0X00000000L, 0X00004000L, 0X00404101L,     /* tab1 */
  0X00404001L, 0X00004101L, 0X00000001L, 0X00004000L,
  0X00000100L, 0X00404100L, 0X00404101L, 0X00000100L,
  0X00400101L, 0X00404001L, 0X00400000L, 0X00000001L,
  0X00000101L, 0X00400100L, 0X00400100L, 0X00004100L,
  0X00004100L, 0X00404000L, 0X00404000L, 0X00400101L,
  0X00004001L, 0X00400001L, 0X00400001L, 0X00004001L,
  0X00000000L, 0X00000101L, 0X00004101L, 0X00400000L,
  0X00004000L, 0X00404101L, 0X00000001L, 0X00404000L,
  0X00404100L, 0X00400000L, 0X00400000L, 0X00000100L,
  0X00404001L, 0X00004000L, 0X00004100L, 0X00400001L,
  0X00000100L, 0X00000001L, 0X00400101L, 0X00004101L,
  0X00404101L, 0X00004001L, 0X00404000L, 0X00400101L,
  0X00400001L, 0X00000101L, 0X00004101L, 0X00404100L,
  0X00000101L, 0X00400100L, 0X00400100L, 0X00000000L,
  0X00004001L, 0X00004100L, 0X00000000L, 0X00404001L,

  0X00000082L, 0X02008080L, 0X00000000L, 0X02008002L,      /* tab3 */
  0X02000080L, 0X00000000L, 0X00008082L, 0X02000080L,
  0X00008002L, 0X02000002L, 0X02000002L, 0X00008000L,
  0X02008082L, 0X00008002L, 0X02008000L, 0X00000082L,
  0X02000000L, 0X00000002L, 0X02008080L, 0X00000080L,
  0X00008080L, 0X02008000L, 0X02008002L, 0X00008082L,
  0X02000082L, 0X00008080L, 0X00008000L, 0X02000082L,
  0X00000002L, 0X02008082L, 0X00000080L, 0X02000000L,
  0X02008080L, 0X02000000L, 0X00008002L, 0X00000082L,
  0X00008000L, 0X02008080L, 0X02000080L, 0X00000000L,
  0X00000080L, 0X00008002L, 0X02008082L, 0X02000080L,
  0X02000002L, 0X00000080L, 0X00000000L, 0X02008002L,
  0X02000082L, 0X00008000L, 0X02000000L, 0X02008082L,
  0X00000002L, 0X00008082L, 0X00008080L, 0X02000002L,
  0X02008000L, 0X02000082L, 0X00000082L, 0X02008000L,
  0X00008082L, 0X00000002L, 0X02008002L, 0X00008080L,

  0X00000040L, 0X00820040L, 0X00820000L, 0X10800040L,   /* tab5 */
  0X00020000L, 0X00000040L, 0X10000000L, 0X00820000L,
  0X10020040L, 0X00020000L, 0X00800040L, 0X10020040L,
  0X10800040L, 0X10820000L, 0X00020040L, 0X10000000L,
  0X00800000L, 0X10020000L, 0X10020000L, 0X00000000L,
  0X10000040L, 0X10820040L, 0X10820040L, 0X00800040L,
  0X10820000L, 0X10000040L, 0X00000000L, 0X10800000L,
  0X00820040L, 0X00800000L, 0X10800000L, 0X00020040L,
  0X00020000L, 0X10800040L, 0X00000040L, 0X00800000L,
  0X10000000L, 0X00820000L, 0X10800040L, 0X10020040L,
  0X00800040L, 0X10000000L, 0X10820000L, 0X00820040L,
  0X10020040L, 0X00000040L, 0X00800000L, 0X10820000L,
  0X10820040L, 0X00020040L, 0X10800000L, 0X10820040L,
  0X00820000L, 0X00000000L, 0X10020000L, 0X10800000L,
  0X00020040L, 0X00800040L, 0X10000040L, 0X00020000L,
  0X00000000L, 0X10020000L, 0X00820040L, 0X10000040L,

  0X00080000L, 0X81080000L, 0X81000200L, 0X00000000L,    /* tab7 */
  0X00000200L, 0X81000200L, 0X80080200L, 0X01080200L,
  0X81080200L, 0X00080000L, 0X00000000L, 0X81000000L,
  0X80000000L, 0X01000000L, 0X81080000L, 0X80000200L,
  0X01000200L, 0X80080200L, 0X80080000L, 0X01000200L,
  0X81000000L, 0X01080000L, 0X01080200L, 0X80080000L,
  0X01080000L, 0X00000200L, 0X80000200L, 0X81080200L,
  0X00080200L, 0X80000000L, 0X01000000L, 0X00080200L,
  0X01000000L, 0X00080200L, 0X00080000L, 0X81000200L,
  0X81000200L, 0X81080000L, 0X81080000L, 0X80000000L,
  0X80080000L, 0X01000000L, 0X01000200L, 0X00080000L,
  0X01080200L, 0X80000200L, 0X80080200L, 0X01080200L,
  0X80000200L, 0X81000000L, 0X81080200L, 0X01080000L,
  0X00080200L, 0X00000000L, 0X80000000L, 0X81080200L,
  0X00000000L, 0X80080200L, 0X01080000L, 0X00000200L,
  0X81000000L, 0X01000200L, 0X00000200L, 0X80080000L,

  0X20042008L, 0X20002000L, 0X00002000L, 0X00042008L,   /* tab2 */
  0X00040000L, 0X00000008L, 0X20040008L, 0X20002008L,
  0X20000008L, 0X20042008L, 0X20042000L, 0X20000000L,
  0X20002000L, 0X00040000L, 0X00000008L, 0X20040008L,
  0X00042000L, 0X00040008L, 0X20002008L, 0X00000000L,
  0X20000000L, 0X00002000L, 0X00042008L, 0X20040000L,
  0X00040008L, 0X20000008L, 0X00000000L, 0X00042000L,
  0X00002008L, 0X20042000L, 0X20040000L, 0X00002008L,
  0X00000000L, 0X00042008L, 0X20040008L, 0X00040000L,
  0X20002008L, 0X20040000L, 0X20042000L, 0X00002000L,
  0X20040000L, 0X20002000L, 0X00000008L, 0X20042008L,
  0X00042008L, 0X00000008L, 0X00002000L, 0X20000000L,
  0X00002008L, 0X20042000L, 0X00040000L, 0X20000008L,
  0X00040008L, 0X20002008L, 0X20000008L, 0X00040008L,
  0X00042000L, 0X00000000L, 0X20002000L, 0X00002008L,
  0X20000000L, 0X20040008L, 0X20042008L, 0X00042000L,

  0X40200800L, 0X40000820L, 0X40000820L, 0X00000020L,    /* tab4 */
  0X00200820L, 0X40200020L, 0X40200000L, 0X40000800L,
  0X00000000L, 0X00200800L, 0X00200800L, 0X40200820L,
  0X40000020L, 0X00000000L, 0X00200020L, 0X40200000L,
  0X40000000L, 0X00000800L, 0X00200000L, 0X40200800L,
  0X00000020L, 0X00200000L, 0X40000800L, 0X00000820L,
  0X40200020L, 0X40000000L, 0X00000820L, 0X00200020L,
  0X00000800L, 0X00200820L, 0X40200820L, 0X40000020L,
  0X00200020L, 0X40200000L, 0X00200800L, 0X40200820L,
  0X40000020L, 0X00000000L, 0X00000000L, 0X00200800L,
  0X00000820L, 0X00200020L, 0X40200020L, 0X40000000L,
  0X40200800L, 0X40000820L, 0X40000820L, 0X00000020L,
  0X40200820L, 0X40000020L, 0X40000000L, 0X00000800L,
  0X40200000L, 0X40000800L, 0X00200820L, 0X40200020L,
  0X40000800L, 0X00000820L, 0X00200000L, 0X40200800L,
  0X00000020L, 0X00200000L, 0X00000800L, 0X00200820L,

  0X08000004L, 0X08100000L, 0X00001000L, 0X08101004L,    /* tab6 */
  0X08100000L, 0X00000004L, 0X08101004L, 0X00100000L,
  0X08001000L, 0X00101004L, 0X00100000L, 0X08000004L,
  0X00100004L, 0X08001000L, 0X08000000L, 0X00001004L,
  0X00000000L, 0X00100004L, 0X08001004L, 0X00001000L,
  0X00101000L, 0X08001004L, 0X00000004L, 0X08100004L,
  0X08100004L, 0X00000000L, 0X00101004L, 0X08101000L,
  0X00001004L, 0X00101000L, 0X08101000L, 0X08000000L,
  0X08001000L, 0X00000004L, 0X08100004L, 0X00101000L,
  0X08101004L, 0X00100000L, 0X00001004L, 0X08000004L,
  0X00100000L, 0X08001000L, 0X08000000L, 0X00001004L,
  0X08000004L, 0X08101004L, 0X00101000L, 0X08100000L,
  0X00101004L, 0X08101000L, 0X00000000L, 0X08100004L,
  0X00000004L, 0X00001000L, 0X08100000L, 0X00101004L,
  0X00001000L, 0X00100004L, 0X08001004L, 0X00000000L,
  0X08101000L, 0X08000000L, 0X00100004L, 0X08001004L,

  0X04000410L, 0X00000400L, 0X00010000L, 0X04010410L,    /* tab8 */
  0X04000000L, 0X04000410L, 0X00000010L, 0X04000000L,
  0X00010010L, 0X04010000L, 0X04010410L, 0X00010400L,
  0X04010400L, 0X00010410L, 0X00000400L, 0X00000010L,
  0X04010000L, 0X04000010L, 0X04000400L, 0X00000410L,
  0X00010400L, 0X00010010L, 0X04010010L, 0X04010400L,
  0X00000410L, 0X00000000L, 0X00000000L, 0X04010010L,
  0X04000010L, 0X04000400L, 0X00010410L, 0X00010000L,
  0X00010410L, 0X00010000L, 0X04010400L, 0X00000400L,
  0X00000010L, 0X04010010L, 0X00000400L, 0X00010410L,
  0X04000400L, 0X00000010L, 0X04000010L, 0X04010000L,
  0X04010010L, 0X04000000L, 0X00010000L, 0X04000410L,
  0X00000000L, 0X04010410L, 0X00010010L, 0X04000010L,
  0X04010000L, 0X04000400L, 0X04000410L, 0X00000000L,
  0X04010410L, 0X00010400L, 0X00010400L, 0X00000410L,
  0X00000410L, 0X00010010L, 0X04000000L, 0X04010400L  };
 
 static private int kseltab[][]= { 
  {8, 44, 29, 52, 42, 14,    2, 30, 22, 21, 38, 50,
  19, 24, 34, 47, 32,  3,   53, 18, 33, 55, 13, 17},
  {28, 49,  1,  7, 16, 36,   51,  0, 31, 23, 15, 35,
  41, 26,  4, 46, 20, 25,   39, 12, 11, 54, 48, 27},
  {1, 37, 22, 45, 35,  7,   52, 23, 15, 14, 31, 43,
  12, 17, 27, 40, 25, 55,   46, 11, 26, 48,  6, 10},
  {21, 42, 51,  0,  9, 29,   44, 50, 49, 16,  8, 28,
  34, 19, 24, 39, 13, 18,   32,  5,  4, 47, 41, 20},

  {44, 23,  8, 31, 21, 50,   38,  9,  1,  0, 42, 29,
  53,  3, 13, 26, 11, 41,   32, 24, 12, 34, 47, 55},
  {7, 28, 37, 43, 52, 15,   30, 36, 35,  2, 51, 14,
  20,  5, 10, 25, 54,  4,   18, 46, 17, 33, 27,  6},
  {30,  9, 51, 42,  7, 36,   49, 52, 44, 43, 28, 15,
  39, 48, 54, 12, 24, 27,   18, 10, 53, 20, 33, 41},
  {50, 14, 23, 29, 38,  1,   16, 22, 21, 45, 37,  0,
   6, 46, 55, 11, 40, 17,    4, 32,  3, 19, 13, 47},

  {16, 52, 37, 28, 50, 22,   35, 38, 30, 29, 14,  1,
  25, 34, 40, 53, 10, 13,    4, 55, 39,  6, 19, 27},
  {36,  0,  9, 15, 49, 44,    2,  8,  7, 31, 23, 43,
  47, 32, 41, 24, 26,  3,   17, 18, 48,  5, 54, 33},
  {2, 38, 23, 14, 36,  8,   21, 49, 16, 15,  0, 44,
  11, 20, 26, 39, 55, 54,   17, 41, 25, 47,  5, 13},
  {22, 43, 52,  1, 35, 30,   45, 51, 50, 42,  9, 29,
  33, 18, 27, 10, 12, 48,    3,  4, 34, 46, 40, 19},

  {45, 49,  9,  0, 22, 51,    7, 35,  2,  1, 43, 30,
  24,  6, 12, 25, 41, 40,    3, 27, 11, 33, 46, 54},
  {8, 29, 38, 44, 21, 16,   31, 37, 36, 28, 52, 15,
  19,  4, 13, 55, 53, 34,   48, 17, 20, 32, 26,  5},
  {31, 35, 52, 43,  8, 37,   50, 21, 45, 44, 29, 16,
  10, 47, 53, 11, 27, 26,   48, 13, 24, 19, 32, 40},
  {51, 15, 49, 30,  7,  2,   42, 23, 22, 14, 38,  1,
   5, 17, 54, 41, 39, 20,   34,  3,  6, 18, 12, 46},

  {49, 28, 45, 36,  1, 30,   43, 14, 38, 37, 22,  9,
   3, 40, 46,  4, 20, 19,   41,  6, 17, 12, 25, 33},
  {44,  8, 42, 23,  0, 52,   35, 16, 15,  7, 31, 51,
  53, 10, 47, 34, 32, 13,   27, 55, 54, 11,  5, 39},
  {35, 14, 31, 22, 44, 16,   29,  0, 49, 23,  8, 52,
  48, 26, 32, 17,  6,  5,   27, 47,  3, 53, 11, 19},
  {30, 51, 28,  9, 43, 38,   21,  2,  1, 50, 42, 37,
  39, 55, 33, 20, 18, 54,   13, 41, 40, 24, 46, 25},

  {21,  0, 42,  8, 30,  2,   15, 43, 35,  9, 51, 38,
  34, 12, 18,  3, 47, 46,   13, 33, 48, 39, 24,  5},
  {16, 37, 14, 52, 29, 49,    7, 45, 44, 36, 28, 23,
  25, 41, 19,  6,  4, 40,   54, 27, 26, 10, 32, 11},
  {7, 43, 28, 51, 16, 45,    1, 29, 21, 52, 37, 49,
  20, 53,  4, 48, 33, 32,   54, 19, 34, 25, 10, 46},
  {2, 23,  0, 38, 15, 35,   50, 31, 30, 22, 14,  9,
  11, 27,  5, 47, 17, 26,   40, 13, 12, 55, 18, 24},

  {50, 29, 14, 37,  2, 31,   44, 15,  7, 38, 23, 35,
   6, 39, 17, 34, 19, 18,   40,  5, 20, 11, 55, 32},
  {45,  9, 43, 49,  1, 21,   36, 42, 16,  8,  0, 52,
  24, 13, 46, 33,  3, 12,   26, 54, 53, 41,  4, 10},
  {36, 15,  0, 23, 45, 42,   30,  1, 50, 49,  9, 21,
  47, 25,  3, 20,  5,  4,   26, 46,  6, 24, 41, 18},
  {31, 52, 29, 35, 44,  7,   22, 28,  2, 51, 43, 38,
  10, 54, 32, 19, 48, 53,   12, 40, 39, 27, 17, 55},

  {22,  1, 43,  9, 31, 28,   16, 44, 36, 35, 52,  7,
  33, 11, 48,  6, 46, 17,   12, 32, 47, 10, 27,  4},
  {42, 38, 15, 21, 30, 50,    8, 14, 45, 37, 29, 49,
  55, 40, 18,  5, 34, 39,   53, 26, 25, 13,  3, 41},
  {15, 51, 36,  2, 49, 21,    9, 37, 29, 28, 45,  0,
  26,  4, 41, 54, 39, 10,    5, 25, 40,  3, 20, 24},
  {35, 31,  8, 14, 23, 43,    1,  7, 38, 30, 22, 42,
  48, 33, 11, 53, 27, 32,   46, 19, 18,  6, 55, 34}  };
  
	/** the key expanded to a long array so we can work faster */
	private long kp[] = new long[64];
 
	/** the starting chaining order for the CBC */
	private int iv[] = {0,0,0,0,0,0,0,0};
 
	/** this is so that we don't encrypt if there is no key */
	private boolean keyOk = false;
 
	/** In the public interface we allow 3 types of parameters. Arrays of
		ints, arrays of bytes and Strings. This is in order to provide the
		user with confortable interfaces, internally all are converted to
		int[] */
	public DES()           { keyOk = false;  }
	public DES(int key[])  { setKey(key);    }
	public DES(byte key[]) { setKey(key);    }
	public DES(String key) { setKey(key);    }
 
	/** Change the key to the byte[] given (and regenerate kp[]).
		Only 8 elements are used, if less are given the key is made
		by filling in the rest with default values. */
	public void setKey(byte key[]) {
		int arr[] = makeIntArray(key);
		arr = makeKeyArray(arr);
		buildKtab(arr);
	}
	/** Change the key to the int[] given (and regenerate kp[]).
		Only 8 elements are used, if less are given the key is made
		by filling in the rest with default values. */
	public void setKey(int key[]) {
		int myKey[] = makeKeyArray(key);
		buildKtab(myKey);
	}
	/** Change the key to the String given (and regenerate kp[]).
		Only 8 elements are used, if less are given the key is made
		by filling in the rest with default values. */
	public void setKey(String key) {
		int arr[] = makeIntArray(key);
		arr = makeKeyArray(arr);
		buildKtab(arr);
	}
	/** Encode an array of ints into a new one (same size). First a copy of
		the array is made and then this is 'encoded over' and returned. */
	public int [] encode(int bb[]) {
		int arr[] = makeIntArray(bb);
		encstring(1, arr);
		return arr;
	}
	/** Encode an array of bytes into a new one (same size). First an array
		of ints is made copying the elements of the byte array. Then this
		int array is 'encoded over', converted back to an array of bytes and
		finally returned. */
	public byte []encode(byte bb[]) {
		int arr[] = makeIntArray(bb);
		arr = encode(arr);
		return makeByteArray(arr);
	}
	/** Encode a String into a new one (same size). First an array of ints
		is made copying the elements of the String. Then this int array is
		'encoded over', conerted back to a String and finally returned. */
	public String encode(String bb) {
		int arr[] = makeIntArray(bb);
		arr = encode(arr);
		return makeString(arr);
	}
 
	/** Decode an array of ints into a new one (same size). */
	public int []decode(int bb[]) {
		int arr[] = makeIntArray(bb);
		encstring(0, arr);
		return arr;
	}
	/** Decode an array of bytes into a new one (same size). */
	public byte []decode(byte bb[]) {
		int arr[] = makeIntArray(bb);
		arr = decode(arr);
		return makeByteArray(arr);
	}
	/** Decode a String into a new one (same size). */
	public String decode(String bb) {
		int arr[] = makeIntArray(bb);
		arr = decode(arr);
		return makeString(arr);
	}
 
	/** private stuff starts here */
 
	/** build an int[] from an int[] (basically copy it) */
	private int []makeIntArray(int ia[]) {
		int i=0, len=ia.length, arr[]=new int[len];
		for (i=0; i<len; i++)
			arr[i] = ia[i];
		return arr;
	}
 
	/** make int[] from parameters received */
	/** build an int[] from a String */
	private int []makeIntArray(String str) {
		int i=0, len=str.length(), arr[] = new int[len];
		for (i=0; i<len; i++)
			arr[i] = (int) str.charAt(i);
		return arr;
	}
	/** build an int[] from a byte[] */
	private int []makeIntArray(byte ba[]) {
		int i=0, len=ba.length, arr[] = new int[len];
		for (i=0; i<len; i++) {
			arr[i] = (int) ba[i];
			if (arr[i] < 0) {              
			  arr[i] += 0x0100;
			}
		  //System.out.println ("arr[i]="+arr[i]); //$NON-NLS-1$
		}
		return arr;
	}
	/** now the opposite, make the return type */
	/** build an byte[] from a int[] */
	private byte []makeByteArray(int ia[]) {
		int i=0, len=ia.length;
		byte arr[] = new byte[len];
		for (i=0; i<len; i++)
			arr[i] = (byte) ia[i];
		return arr;
	}
	/** build an String from a int[] */
	private String makeString(int ia[]) {
		StringBuffer sb = new StringBuffer(ia.length);
		int i=0, len=ia.length;
		for (i=0; i<len; i++)
			sb.append((char)ia[i]);
		return sb.toString();
	}
	/** make an array of ints suitable to be a key (at least 8 bytes, if it is
		more only 8 will be used anyway). If there are less than 8 bytes fill
		the rest with the array below */
	private int []makeKeyArray(int arr[]) {
		int len=arr.length;
		if (len <= 8) {
			int i=0, newArr[] = {0x01,0x23,0x45,0x67,0x89,0xAB,0xCD,0xEF};
			for (i=0; i<len; i++)
				newArr[i] = arr[i];
			return newArr;
		}
		return arr;
	}
	/** make the expanded key (kp[] above) from the key given, but first
		weaken it to 40 bits */
	private void buildKtab(int key[]) {
		keyOk = true;    // set the marker to indicate we have a valid key
		initktab(key, kp); // finally build the expanded array
	}


	static private int K1[]={0xC4, 0x08, 0xB0, 0x54, 0x0B, 0xA1, 0xE0, 0xAE};
	static private int K3[]={0xEF, 0x2C, 0x04, 0x1C, 0xE6, 0x38, 0x2F, 0xE6};
 
 
	/*-----------------------------------------------------------------------\
	|                             INITKTAB                                   |
	\-----------------------------------------------------------------------*/
	static private long masks[]= {2147483648L, 1073741824L, 536870912L,
								   268435456L,  134217728L,  67108864L,
									 8388608L,    4194304L,   2097152L,
									 1048576L,     524288L,    262144L,
									   32768L,      16384L,      8192L,
										4096L,       2048L,      1024L,
										 128L,         64L,        32L,
										  16L,          8L,         4L};
	private void initktab(int orig_key[], long pkt[]) {
		long keya[] = new long[56];
		int i, j, k;
		long a, b, c;
 
		a = (long)orig_key[3]+(long)256*orig_key[2]+(long)256*256*orig_key[1]+
			(long)65536*256*orig_key[0];
		b = a; b &= 0XFE000000L;
		a = (a << 1) & 0xFFFFFFFFL; c = a; c &= 0X01FC0000L; b |= c;
		a = (a << 1) & 0xFFFFFFFFL; c = a; c &= 0X0003F800L; b |= c;
		a = (a << 1) & 0xFFFFFFFFL; a &= 0X000007F0L; a |= b;
		b = 0X80000000L;
		for (i=0; i<28; i++) {
		  keya[i] = a & b;
		  b >>=1;
		}
		a = (long)orig_key[7]+(long)256*orig_key[6]+(long)256*256*orig_key[5]+
			(long)65536*256*orig_key[4];
		b = a; b &= 0XFE000000L;
		a = (a << 1) & 0xFFFFFFFFL; c = a; c &= 0X01FC0000L; b |= c;
		a = (a << 1) & 0xFFFFFFFFL; c = a; c &= 0X0003F800L; b |= c;
		a = (a << 1) & 0xFFFFFFFFL; a &= 0X000007F0L; a |= b;
		b = 0X80000000L;
		for (i=0; i<28; i++) {
		  keya[i+28] = a & b;
		  b >>=1;
		}
		for (i=0; i<32; i++) {
		  a = 0;
		  for (j=0; j<24; j++) {
			if (keya[kseltab[i][j]] != 0)
			  a = a | masks[j];
		  }
		  k = i % 2;
		  pkt[i] = a;
		  pkt[62 + 2*k - i] = a;
		}
	}
	/*-----------------------------------------------------------------------\
	|               Auxiliary functions for ENCSTRING                        |
	\-----------------------------------------------------------------------*/
	private long ror(long x, int n) {
	  long a = x;
	  x >>= n;
	  a = (a << (32 - n)) & 0xFFFFFFFFL;
	  return x | a;
	}
 
	private long rol(long x, int n) {
	  long a = x;
	  x = (x << n) & 0xFFFFFFFFL;
	  a >>= (32 - n);
	  return x | a;
	}
 
	private void swap(long m) {
	  long a = u1;
	  a ^= u2;
	  a &= m;
	  u1 ^= a;
	  u2 ^= a;
	}
 
	private long swaphilo(long x) {
	  long a = x;
	  long b = x;
	  a >>= 24;
	  b = (b << 24) & 0xFF000000L;
	  x &= 0X00FFFF00L;
	  x |= a;
	  return x | b;
	}
 
	private void swaphalves() {
	  long a = (u2 << 16) & 0xFFFF0000L;
	  long b = u1 >> 16;
	  u2 &= 0XFFFF0000L;
	  u2 |= b;
	  u1 &= 0XFFFFL;
	  u1 |= a;
	}
 
	private void iperm() {
	  swaphalves();
	  u2 = ror(u2, 2);
	  swap(0X33333333L);
	  u2 = rol(u2, 2);
	  swaphalves();
	  u2 = ror(u2, 4);
	  swap(0X0F0F0F0FL);
	  u2 = rol(u2, 4);
	  u2 = rol(u2, 8);
	  swap(0XFF00FF00L);
	  u2 = ror(u2, 8);
	  u1 = rol(u1, 1);
	  swap(0X55555555L);
	  u2 = ror(u2, 1);
	  u1 = swaphilo(u1);
	  u2 = swaphilo(u2);
	  u1 = ror(u1, 1);
	  u2 = ror(u2, 1);
	}
 
	private void fperm() {
	  u1 = rol(u1, 1);
	  u2 = rol(u2, 1);
	  u1 = swaphilo(u1);
	  u2 = swaphilo(u2);
	  u2 = rol(u2, 1);
	  swap(0X55555555L);
	  u1 = ror(u1, 1);
	  u2 = rol(u2, 8);
	  swap(0XFF00FF00L);
	  u2 = ror(u2, 8);
	  u2 = ror(u2, 4);
	  swap(0X0F0F0F0FL);
	  u2 = rol(u2, 4);
	  swaphalves();
	  u2 = ror(u2, 2);
	  swap(0X33333333L);
	  u2 = rol(u2, 2);
	  swaphalves();
	}
 
	private void round2(long b, long tkey[], int t) {
	  int i=0;
	  for (i=0; i<8; i++) {
		long a = (u2 & b) ^ tkey[t];
		int p=0;
		p = (int) (      (a >> 26)     ); u1 ^= stab[p];
		p = (int) ( 64 + (a >> 18) % 64); u1 ^= stab[p];
		p = (int) (128 + (a >> 10) % 64); u1 ^= stab[p];
		p = (int) (192 + (a >>  2) % 64); u1 ^= stab[p];
		t++;
		a = ((((u2 << 4) & 0xFFFFFFFFL) | (u2 >> 28)) & b) ^ tkey[t];
		p = (int) (256 + (a >> 26)     ); u1 ^= stab[p];
		p = (int) (320 + (a >> 18) % 64); u1 ^= stab[p];
		p = (int) (384 + (a >> 10) % 64); u1 ^= stab[p];
		p = (int) (448 + (a >>  2) % 64); u1 ^= stab[p];
		t++;
		a = (u1 & b) ^ tkey[t];
		p = (int) (      (a >> 26)     ); u2 ^= stab[p];
		p = (int) ( 64 + (a >> 18) % 64); u2 ^= stab[p];
		p = (int) (128 + (a >> 10) % 64); u2 ^= stab[p];
		p = (int) (192 + (a >>  2) % 64); u2 ^= stab[p];
		t++;
		a = ((((u1 << 4) & 0xFFFFFFFFL) | (u1 >> 28)) & b) ^ tkey[t];
		p = (int) (256 + (a >> 26)     ); u2 ^= stab[p];
		p = (int) (320 + (a >> 18) % 64); u2 ^= stab[p];
		p = (int) (384 + (a >> 10) % 64); u2 ^= stab[p];
		p = (int) (448 + (a >>  2) % 64); u2 ^= stab[p];
		t++;
	  }
	}
	/** we make this global because we don't know how to pass values by ref. */
	private long a, b, chn[];
	private long u1, u2;
	private int n, n2, nn, fract;
	/*-----------------------------------------------------------------------\
	|                          ENCSTRING                                     |
	\-----------------------------------------------------------------------*/
	private void encstring(int eflag, int bb[]) {
	  if (keyOk) {
		int i=0, len=bb.length;
		fract = len % 8;
		nn = (len - fract) / 4;
		long bu[] = new long[nn+2];
		/* Convert iv from bytes into words for proc. in this function. */
		/** these were casts in C *(unsigned int *). No can do in java, for the
			moment manually convert the values */
		chn = new long[2];
		chn[0] = (long)iv[3] + (long)256*iv[2] + (long)256*256*iv[1] +
				 (long)65536*256*iv[0];
		chn[1] = (long)iv[7] + (long)256*iv[6] + (long)256*256*iv[5] +
				 (long)65536*256*iv[4];
		for (i=0; i<nn; i++)
		  bu[i] = (long)bb[i*4+3]+(long)256*bb[i*4+2]+(long)256*256*bb[i*4+1]+
				  (long)65536*256*bb[i*4+0];
		bu[nn] = bu[nn+1] = 0;
		for (i=0; i<fract; i++)
		  if (i<4)
			bu[nn] += bb[4*nn+i] * ((long)65536*256 >> 8*i);
		  else
			bu[nn+1] += bb[4*nn+i] * ((long)65536*256 >> 8*(i-4));
 
		/* finished conersion routines, here we do the ral work */
		if (eflag != 0)
			encipher(eflag, len, bu);
		else
			decshort(eflag, len, bu);
 
		/* back to converting the values */
		for (i=0; i < nn; i++ ) {
		  bb[i*4+3] = (int) ( bu[i]        & 0xFFL);
		  bb[i*4+2] = (int) ((bu[i] >>  8) & 0xFFL);
		  bb[i*4+1] = (int) ((bu[i] >> 16) & 0xFFL);
		  bb[i*4+0] = (int) ((bu[i] >> 24) & 0xFFL);
		}
		for (i=0; i<fract; i++)
		  if (i < 4)
			bb[4*nn+i] = (int) ((bu[nn] >> (3-i)*8) & 0xFFL);
		  else
			bb[4*nn+i] = (int) ((bu[nn+1] >> (7-i)*8) & 0xFFL);
	  } else
		  System.out.println("Key not set!!!"); //$NON-NLS-1$
	}
 
// ENCIPHER:
	private void encipher(int eflag, int len, long bu[]) {
		u2 = chn[0];
		u1 = chn[1];
		n = 0;
		while (n < nn)  {
		  a  = bu[n];
		  u2 ^= a;
		  a  = bu[n+1];
		  u1 ^= a;
		  iperm();
		  round2(0XFCFCFCFCL, kp, 0);
		  a = u1;
		  u1 = u2;
		  u2 = a;
		  fperm();
		  bu[n] = u2;
		  bu[n+1] = u1;
		  n += 2;
		}
		if (fract != 0)
			encshort(eflag, len, bu);
	}
 
// DECSHORT:
	private void decshort(int eflag, int len, long bu[]) {
		if (fract == 0)
			decipher(eflag, len, bu);
		else {
			if (nn==0) {
			  u2 = chn[0];
			  u1 = chn[1];
			} else {
			  u2 = bu[nn-2];
			  u1 = bu[nn-1];
			}
			n = nn ;
			encshort(eflag, len, bu);
		}
	}
 
// ENCSHORT:
	static private long AND_ARRAY1[] = {0XFF000000L, 0XFFFF0000L, 0XFFFFFF00L,
										0XFFFFFFFFL, 0XFFFFFFFFL, 0XFFFFFFFFL,
										0XFFFFFFFFL};
	static private long AND_ARRAY2[] = {0XFF000000L, 0XFFFF0000L, 0XFFFFFF00L};
	private void encshort(int eflag, int len, long bu[]) {
		iperm();
		round2(0XFCFCFCFCL, kp, 0);
		a = u1; u1 = u2; u2 = a;
		fperm();
		bu[n] ^= u2 & AND_ARRAY1[fract-1]; // should not be called if fract == 0
		if (fract > 4)
			bu[n+1] ^= u1 & AND_ARRAY2[fract-5];
		if (eflag == 0)
			decipher(eflag, len, bu);
	}
 
// DECIPHER:
	private void decipher(int eflag, int len, long bu[]) {
		if (nn != 0) {
		  u2 = bu[nn-2];
		  u1 = bu[nn-1];
		  n = nn;
		  while (n > 0) {
			n -= 2;
			iperm();
			round2(0XFCFCFCFCL, kp, 32);
			a = u1; u1 = u2; u2 = a;
			fperm();
			a = u2; b = u1;
			if (n > 0) {
			  u2 = bu[n-2];
			  u1 = bu[n-1];
			} else {
			  u2 = chn[0];
			  u1 = chn[1];
			}
			bu[n] = a ^ u2;
			bu[n+1] = b ^ u1;
		  }
		}
	}
	
	public static void main(String[] args) {
	}
}