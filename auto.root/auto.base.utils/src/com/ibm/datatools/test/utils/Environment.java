package com.ibm.datatools.test.utils;

public class Environment {
	
	public static String getOSVersion(){
		//Windows, Linux ...
		return System.getProperty("os.name");
	}
	
	public static boolean isWindows(){
		if (getOSVersion().indexOf("Windows") != -1)
			return true;
		else
			return false;
	}
	
	public static boolean isLinux(){
		if (getOSVersion().indexOf("Linux") != -1)
			return true;
		else
			return false;
	}
	
	public static boolean isMacOS(){
		if (getOSVersion().indexOf("Mac OS X") != -1)
			return true;
		else
			return false;
	}
	
	public static String getProjectRootPath(){
		return System.getProperty("user.dir");
	}

}
