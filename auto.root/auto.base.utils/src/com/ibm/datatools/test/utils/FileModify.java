package com.ibm.datatools.test.utils;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;



public class FileModify {

	private String path;
	private final String target;
	private final String newContent;

	public FileModify(String path, String target, String newContent) {
		this.path = path;
		this.target = target;
		this.newContent = newContent;

		operation();
	}

	private void operation() {
		File file = new File(path);
		opeationDirectory(file);
	}

	public void opeationDirectory(File dir) {

		File[] files = dir.listFiles();
		for (int i = 0; i < files.length; i++) {
			File f = files[i];
			if (f.isDirectory())
				opeationDirectory(f);
			if (f.isFile())
				operationFile(f);
		}
	}

	public void operationFile(File file) {

		try {
			InputStream is = new FileInputStream(file);
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(is));

			String filename = file.getName();
			File tmpfile = new File(file.getParentFile().getAbsolutePath()
					+ "\\" + filename + ".tmp");

			BufferedWriter writer = new BufferedWriter(new FileWriter(tmpfile));

			boolean flag = false;
			String str = null;
			while (true) {
				str = reader.readLine();

				if (str == null)
					break;

				if (str.contains(target)) {
					writer.write(newContent + "\n");

					flag = true;
				} else
					writer.write(str + "\n");
			}

			is.close();

			writer.flush();
			writer.close();

			if (flag) {
				file.delete();
				tmpfile.renameTo(new File(file.getAbsolutePath()));
			} else
				tmpfile.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}