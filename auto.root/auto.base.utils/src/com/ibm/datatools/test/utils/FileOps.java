package com.ibm.datatools.test.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class FileOps {

	// ******************************************************************************************************
	/**
	 * Writes specified string content to file
	 * <p>
	 * 
	 * @param filename
	 *            path and filename of file to write string to
	 * @param sContents
	 *            String to write to file
	 * @author Tony Venditti
	 */
	// /*******************************************************************************************************/
	public static void writeFileContents(String filename, String sContents) {
		// write specified string content to file
		// String file = getFolderStructure(filename);
		try {
			FileWriter out = new FileWriter(filename);
			out.write(sContents);
			out.close();
		} catch (IOException e) {

		}
	}

	// ********************************************************************************************************
	/**
	 * Appends specified string content to a file
	 * <p>
	 * 
	 * @param filename
	 *            path and filename of file to append string to
	 * @param sContents
	 *            String to append to file
	 * @author Tony Venditti
	 */
	// /*******************************************************************************************************/
	public static void appendStringToFile(String filename, String sContents) {

		try {
			FileWriter out = new FileWriter(filename, true); // tells
			// FileWriter to
			// append
			out.write(System.getProperty("line.separator") + sContents);
			out.close();
		} catch (IOException e) {

		}
	}

	// ******************************************************************************************************
	/**
	 * Reads specified file contents and returns file contents as a string
	 * <p>
	 * 
	 * @param filename
	 *            Path and filename of file to read
	 * @return String of specified file contents
	 * @author Tony Venditti
	 */
	// *******************************************************************************************************
	public static String getFileContents(String filename) {

		try {
			File file = new File(filename);
			FileReader in = new FileReader(file);
			char c[] = new char[(int) file.length()];
			in.read(c);
			String fileContentsString = new String(c);
			in.close();
			return fileContentsString;
		} catch (IOException e) {

			return null;
		}
	}

	public static boolean compareFilesOrderLineByStep(String source,
			String golden) {
		try {
			FileInputStream fstream = new FileInputStream(source);
			BufferedReader sourceBR = new BufferedReader(new InputStreamReader(
					fstream));

			FileInputStream goldenStream = new FileInputStream(golden);
			BufferedReader goldenBR = new BufferedReader(new InputStreamReader(
					goldenStream));

			String line = "";
			Map<String, Integer> sourceMap = new HashMap<String, Integer>();
			while ((line = sourceBR.readLine()) != null) {
				// keep track of the line number
				line = line.trim();
				if(line.startsWith("--"))
					continue;
				// if the line from the golden file does not exist in the
				// actualFileResult
				// then append the following information to the comparison
				// string
				if (!sourceMap.containsKey(line)) {
					sourceMap.put(line, new Integer(1));
				} else {
					Integer count = (Integer) sourceMap.get(line);
					count++;
					sourceMap.put(line, new Integer(count.intValue()));
				}
			}
			sourceBR.close();

			line = "";
			Map<String, Integer> goldenMap = new HashMap<String, Integer>();
			while ((line = goldenBR.readLine()) != null) {
				// keep track of the line number
				line = line.trim();
				if(line.startsWith("--"))
					continue;
				// if the line from the golden file does not exist in the
				// actualFileResult
				// then append the following information to the comparison
				// string
				if (!goldenMap.containsKey(line)) {
					goldenMap.put(line, new Integer(1));
				} else {
					Integer count = (Integer) goldenMap.get(line);
					count++;
					goldenMap.put(line, new Integer(count.intValue()));
				}
			}
			goldenBR.close();
			if (goldenMap.size() != sourceMap.size()) {
				return false;
			}

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	/*
	 * return true if the file is successfully deleted; false otherwise
	 */
	public static boolean deleteFile (String fileName) {
		File f = new File (fileName);
		boolean result = false;
		if (f.exists()) {
			result = f.delete();
		}
		
		return result;
	}
	
	/**
	 * returns a true or false depending on whether or not the specified
	 * file or folder exists. Use the full path
	 * @param fileName
	 * @return
	 */
	public static boolean fileExists(String fileName){
		File f = new File(fileName);
		if(f.exists()){
			return true;
		}
		return false;
	}
}
