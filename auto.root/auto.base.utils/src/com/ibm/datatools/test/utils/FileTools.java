package com.ibm.datatools.test.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import org.apache.commons.json.JSONArray;
import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;




public class FileTools {
		
	/**
	 * filename is the path of a file or a directory, this method get the folder
	 * path of this file/directory name, then check the folder exists or not, if
	 * the folder does not exist, create it. When filename ended with / or \,
	 * think it as a directory,just create this directory; otherwise, think it
	 * as a file, remove the file name portion , and create the directory if it
	 * does not exist
	 * 
	 * @param filename
	 */
	public static void forceCreateDirectory(String filename) {
		
		if (filename == null)
			return;
		
		String tmp = filename.replace("\\", "/");
		int end = tmp.lastIndexOf("/");
		if (end < 0)
			return;

		tmp = filename.substring(0, end);
		File path = new File(tmp);
		if (!path.exists())
			path.mkdirs();
	}

	/**
	 * return the content of specified file and return the contents as a string
	 * 
	 * @param file
	 * @return on error, null is returned
	 */
	public static String readFileAsString(String file) {
		File theFile = new File(file);
		String content = "";
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(theFile));
			while (true) {
				String line = reader.readLine();
				if (line == null)
					return content;
				else
					content += line + "\r\n";
				}
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally
		{
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public static boolean writeStringAsFile(String content, String file) {
		if (file == null)
			return false;

		try {
			FileWriter fw = new FileWriter(file);
			fw.write(content);
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Delete all file immediately under a directory
	 * 
	 * @param path
	 *            an absolute path
	 */
	public static void deleteAllFiles(String path) {

		if (path == null || "".equals(path)) {
			return;
		}
		File directory = new File(path);
		if (!directory.isDirectory()) {
			return;
		}
		String[] fileList = directory.list();
		if (fileList == null) {
			return;
		}

		TestLogger.logInfo("Delete all files under directory: " + path);
		File tmpFile = null;
		for (int i = 0; i < fileList.length; i++) {

			if (path.endsWith(File.separator)) {
				tmpFile = new File(path + fileList[i]);
			} else {
				tmpFile = new File(path + File.separator + fileList[i]);
			}

			if (tmpFile.isFile()) {
				tmpFile.delete();
			}
		}

	}
	
	public static Object[][] readPropertiesFile(String filePath) throws FileNotFoundException{
		 
		 File file = new File(filePath);
		 Vector<String> lines = new Vector<String>();
		 try {
			 Scanner scanner = new Scanner(file);
			 
			 while (scanner.hasNextLine()) {
				 String nextLine = scanner.nextLine();
				 if(nextLine.trim().length()==0 || nextLine.startsWith("#"))
				 {
					 TestLogger.logDebug("Ignoring line : "+nextLine); 
				 }
				 else{
					 TestLogger.logDebug("Adding Line :"+nextLine);
					 lines.add(nextLine);
				 }
				 	
			 }
	            scanner.close();
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	            throw e;
	        }
		
		
		 Object[][] testdata= new Object[lines.size()][2] ;
		 for(int i=0;i<lines.size();i++){
			 String[] t= lines.elementAt(i).split("=");
			 testdata[i][0]=t[0];
			 testdata[i][1]=t[1];
		 }
		 
		 return testdata;
	}

	public static Object[][] readProperties(Properties p){
		Enumeration<Object> allKeys = p.keys();
		Object[][] testdata = new Object[p.size()][2];
		int i=0;
		while(allKeys.hasMoreElements()){
			String key = allKeys.nextElement().toString();
			String value = p.getProperty(key);
			testdata[i][0]=key;
			testdata[i][1]=value;
			i++;
		}
				 
		
		return testdata;
	}
	
	public static JSONArray fiterByTag(JSONArray p){
		JSONArray filterJSONArray = new JSONArray();
		try {
		for(int i=0; i<p.size(); i++) {
			if(p.getJSONObject(i).optString("tags") != null) {
				
				String strTag = p.getJSONObject(i).optString("tags");				
				//remove the character [,] at the beginning and at the end
				String [] tags = strTag.substring(1, strTag.length()-1).split(",");				
				//remove character "," at the beginning and at the end
				for(int j=0; j<tags.length; j++) {
					tags[j] = tags[j].substring(1, tags[j].length()-1);
				}
				
				//intersection
				if(Setting.getSetting().getTags()!= null && isIntersection(Setting.getSetting().getTags().split(","), tags)) {
					filterJSONArray.add(p.getJSONObject(i));
				}				
			}
			else {
				filterJSONArray.add(p.getJSONObject(i));
			}				
		}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return filterJSONArray;
	}
	
	private static Object[][] readJSONArray(JSONArray p){

		JSONArray filterJSONArray = fiterByTag(p);
		Object[][] testdata = new Object[0][0];
		
		if (filterJSONArray.size() > 0) {
			testdata = new Object[filterJSONArray.size()][2];	
			try {
				for(int i=0; i<filterJSONArray.size(); i++) {					
					testdata[i][0] = filterJSONArray.getJSONObject(i).optString("description");
					testdata[i][1] = filterJSONArray.getJSONObject(i);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return testdata;	
	}
	
//	public static JSONObject readJSONArray(JSONArray p, int index) throws JSONException{
//		JSONArray filterJSONArray = fiterByTag(p);
//		
//		if (index <= filterJSONArray.length() - 1)	
//			return filterJSONArray.getJSONObject(index);
//		
//		return new JSONObject();
//	}
	
	public static Object[][] readJSONFile(String fpath) throws JSONException {
		return readJSONArray(JsonUtil.getJsonUnderDef(fpath));
	}
	
	public static Object[][] readJSONFile(String fpath, JSONObject replacements) throws JSONException {
		return readJSONArray(JsonUtil.getJsonUnderDef(fpath, replacements));
	}
	
	public static Object[][] readJSONFile(String fpath, boolean exactPath) throws JSONException {
		return readJSONArray(JsonUtil.getJsonUnderDef(fpath, exactPath));
	}

	/*
	 * @comment the character indicates this line is comment eg.// or -- or #
	 * return compressed string
	 */
	public static String readFile(String fpath, String comment) {
		Path path = Paths.get(fpath);
		BufferedReader br;
		StringBuilder sb = new StringBuilder();
		try {
			br = Files.newBufferedReader(path);
			String line = "";
			
			while (null != (line = br.readLine())) {
				if ((line = removecommnent(line, comment)).length() > 0) {
					sb.append(line);
//					sb.append("\n");
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	/*
	 * @comment the character indicates this line is comment eg.// or -- or #
	 * return compressed string
	 */
	private static String removecommnent(String line, String comment) {
//		line = line.trim();
		int index = line.indexOf(comment);
		if (index >= 0)
			line = line.substring(0, index);
		return line;
	}
	
	private static boolean isIntersection(String [] a, String [] b) {
		
		Set<String> aSet = new TreeSet<String>();
		for(int i=0; i<a.length ; i++) {
			aSet.add(a[i].trim().toUpperCase());
		}
		Set<String> bSet = new TreeSet<String>();
		for(int i=0; i<b.length ; i++) {
			bSet.add(b[i].trim().toUpperCase());
		}
		int totalLengh = aSet.size() + bSet.size();
		aSet.addAll(bSet);
		
		if(totalLengh == aSet.size()) 
			return false;		
		else
			return true;		
	}

	public static Object[][] convertJ2A(JSONObject js) {
		Object[][] testdata = new Object[1][2];		
			
		testdata[0][0] = 1;
		testdata[0][1] = js;
				
		return testdata;	
	}

}
