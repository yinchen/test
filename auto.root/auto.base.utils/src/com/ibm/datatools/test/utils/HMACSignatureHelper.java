/***************************************************************************/
/*  IBM Confidential                                                       */
/*  OCO Source Materials                                                   */
/*  5724-E34,5724-W93                                                               */
/*  � Copyright IBM Corporation 2011.              */
/*  All rights reserved.                                                   */
/*                                                                         */
/*  The source code for this program is not published or otherwise         */
/*  divested of its trade secrets, irrespective of what has been           */
/*  deposited with the U.S. Copyright Office.                              */
/***************************************************************************/

package com.ibm.datatools.test.utils;

import java.security.SignatureException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class HMACSignatureHelper
{
    private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";
    
    /**
     * Computes RFC 2104-compliant HMAC signature. * @param data The data to be
     * signed.
     * 
     * @param key
     *            The signing key.
     * @return The Base64-encoded RFC 2104-compliant HMAC signature.
     * @throws java.security.SignatureException
     *             when signature generation fails
     */
    public static String calculateRFC2104HMAC(String data, String key) throws java.security.SignatureException
    {
        String result;
        try
        {
            
            // get an hmac_sha1 key from the raw key bytes
            SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), HMAC_SHA1_ALGORITHM);
            
            // get an hmac_sha1 Mac instance and initialize with the signing key
            Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
            mac.init(signingKey);
            
            // compute the hmac on input data bytes
            byte[] rawHmac = mac.doFinal(data.getBytes());
            
            result = WTIV2CryptHelper.createHexEncodingFromByteArray(rawHmac);
            
        }
        catch (Exception e)
        {
            throw new SignatureException("Failed to generate HMAC : " + e.getMessage());
        }
        return result;
    }
    
    public static void main(String[] args){
    	try {
			String a = HMACSignatureHelper.calculateRFC2104HMAC( "X-SOrigin" + "/+*+" , "0000dNCDPWN1Ke_GcCEJvydhSTc:b2c7d75d-8ffd-475b-82e4-2a6164b0343d");
		System.out.println(a);
    	} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    
    
 
}
