package com.ibm.datatools.test.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.json.JSON;
import org.apache.commons.json.JSONArray;
import org.apache.commons.json.JSONArtifact;
import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;

import sun.misc.BASE64Encoder;

public class HTTPClient {
	public String user = null;
	public String password = null;
	private boolean useCertification = false;
	private boolean isAdmin = false;
	private String keyStorePath;
	private Setting st = Setting.getSetting();
	
	private static String token;
	private static String nonadminToken;
	
	public static final int TIMEOUT = 1*60*1000;
	public static final String METHOD_POST = "POST";
	public static final String METHOD_GET = "GET";
	public static final String METHOD_PUT = "PUT";
	public static final String METHOD_DELETE = "DELETE";
	private static final String AUTH_PATH_V2 = "/auth";
	private static final String AUTH_PATH_V3 = "/auth/tokens";

	public HTTPClient(boolean isAdmin) throws Exception {
		if (!isAdmin) {
			user = st.getNonAdminID();
			password = st.getNonAdminPwd();
		} else {
			user = st.getAdminID();
			password = st.getAdminPwd();
		}
		this.isAdmin = isAdmin;
		getToken();
	}
	
	
	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	private HttpsURLConnection getHttpsConnection(String urlStr, boolean isAuthCall) {
		Exception exp = null;
		HttpsURLConnection conn = null;
		String encodedAuthorization = null;
		SSLSocketFactory socketFactory = null;
		SSLContext sc = null;
		if (!st.isToken()) {
			String authorization = user + ":" + password;
			encodedAuthorization = new String(new BASE64Encoder().encode(authorization.getBytes()));
		}
		URL url;
		try {
			url = new URL(urlStr);
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public X509Certificate[] getAcceptedIssuers() {
					return new X509Certificate[0];
				}

				public void checkClientTrusted(X509Certificate[] certs,
						String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs,
						String authType) {
				}
			} };

			sc = SSLContext.getInstance("TLS");
			if (!useCertification) {			
				sc.init(null, trustAllCerts, new SecureRandom());
			} else {
				sc.init(getKeyManagerFactory().getKeyManagers(), trustAllCerts, new SecureRandom());
			}
			if (null != st.getProxyHost()) {
				conn = (HttpsURLConnection) addProxy(url);
			} else {
				conn = (HttpsURLConnection) url.openConnection();
			}
			conn.setDoInput(true);
			conn.setDoOutput(true);
			if (!isAuthCall) {
				if (!st.isToken())
					conn.setRequestProperty("Authorization", "Basic " + encodedAuthorization);
				else {
					conn.setRequestProperty("Authorization", "Bearer " + (isAdmin?token:nonadminToken));
				}
			}
			socketFactory = sc.getSocketFactory();
			conn.setSSLSocketFactory(socketFactory);
		} catch (MalformedURLException e) {
			exp = e;
		} catch (IOException e) {
			exp = e;
		} catch (NoSuchAlgorithmException e) {
			exp = e;
		} catch (KeyManagementException e) {
			exp = e;
		} catch (UnrecoverableKeyException e) {
			exp = e;
		} catch (KeyStoreException e) {
			exp = e;
		}
		if (exp != null)
			exp.printStackTrace();
		return conn;
	}
	
	public HttpURLConnection getHttpConnection(String urlStr, boolean isAuthCall) {
		HttpURLConnection conn = null;
		URL url;
		try {
			url = new URL(urlStr);
			if (null != st.getProxyHost()) {
				conn = (HttpURLConnection) addProxy(url);
			} else {
				conn = (HttpURLConnection) url.openConnection();
			}
			conn.setDoInput(true);
			conn.setDoOutput(true);
			if (!isAuthCall) {
				if (!st.isToken()) {
//					String authorization = user + ":" + password;
//					String encodedAuthorization = new String(new BASE64Encoder().encode(authorization.getBytes()));
//					conn.setRequestProperty("Authorization", "Basic " + encodedAuthorization);
				} else {
					String t = isAdmin ? token : nonadminToken;
					conn.setRequestProperty("Authorization", "Bearer " + t);
				}
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	private URLConnection addProxy(URL url) throws IOException {
		Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(st.getProxyHost(), Integer.parseInt(st.getProxyPort())));
		return url.openConnection(proxy);
	}

//	public SSLContext getSSLContext(String password) throws Exception {
//		KeyManagerFactory keyManagerFactory = KeyManagerFactory
//				.getInstance(KeyManagerFactory.getDefaultAlgorithm());
//		KeyStore keyStore = getKeyStore(password, keyStorePath);
//		keyManagerFactory.init(keyStore, password.toCharArray());
//
//		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
//			public X509Certificate[] getAcceptedIssuers() {
//				return new X509Certificate[0];
//			}
//
//			public void checkClientTrusted(X509Certificate[] certs,
//					String authType) {
//			}
//
//			public void checkServerTrusted(X509Certificate[] certs,
//					String authType) {
//			}
//		} };
//
//		SSLContext context = SSLContext.getInstance("TLS");
//		context.init(keyManagerFactory.getKeyManagers(), trustAllCerts,
//				new SecureRandom());
//		return context;
//	}
	
	private void getToken() throws Exception {
		if ((null == token && isAdmin) || (null == nonadminToken && !isAdmin)) {
			auth();
		}
	}

	private void auth() throws Exception {
		if (!st.isToken())
			return;
		String protocol = "http";
		if (st.isSSL())
			protocol = "https";
		String url = protocol + "://" + st.getHost() + ":" + st.getPort() + st.getBaseURL() + AUTH_PATH_V2;
		if (st.getVersion().equals("v3"))
			url = protocol + "://" + st.getHost() + ":" + st.getPort() + st.getBaseURL() + AUTH_PATH_V3;
		Response res;
		res = call(false, null, true, METHOD_POST, url, st.getAuthBody(isAdmin));
		if (res.status == HttpURLConnection.HTTP_OK) {
			String t = res.jsonResult.getString("token");
			if (isAdmin)
				token = t;
			else
				nonadminToken = t;
		}
	}

	private KeyManagerFactory getKeyManagerFactory() throws NoSuchAlgorithmException, UnrecoverableKeyException, KeyStoreException {
		KeyManagerFactory keyManagerFactory = KeyManagerFactory
				.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		KeyStore keyStore = getKeyStore(password, keyStorePath);
		keyManagerFactory.init(keyStore, password.toCharArray());
		return keyManagerFactory;
	}

	private KeyStore getKeyStore(String password2, String keyStorePath) {
		KeyStore ks = null;
		Exception exp = null;
		FileInputStream is = null;
		try {
			ks = KeyStore.getInstance("PKCS12");
			is = new FileInputStream(keyStorePath);
			ks.load(is, password.toCharArray());
			is.close();
		} catch (KeyStoreException e) {
			exp = e;
		} catch (FileNotFoundException e) {
			exp = e;
		} catch (NoSuchAlgorithmException e) {
			exp = e;
		} catch (CertificateException e) {
			exp = e;
		} catch (IOException e) {
			exp = e;
		} finally {
			if (is != null)
				try {
					is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		if (exp != null)
			exp.printStackTrace();
		return ks;
	}
	/*
	 * The first param is request type
	 * The second param is url
	 * The third param is post body
	 * 
	 */
	public Response call(String... args) throws Exception{
		return call(false, null, false, args);
	}
	/*
	 * usebyte: is body in byte[]
	 * The first param is request type
	 * The second param is url
	 * The third param is post body
	 * 
	 */
	public Response call(boolean upload, String filename, String... args) throws Exception{
		return call(upload, filename, false, args);
	}
	/*
	 * The first param is request type
	 * The second param is url
	 * The third param is post body
	 * 
	 */
	private Response call(boolean upload, String filename, boolean isAuthCall, String... args) throws Exception{
		Response res = new Response();
		String type = args[0];
		HttpURLConnection urlConn = null;
		try {
			urlConn = getConn(type, args[1], isAuthCall);
			if (urlConn instanceof HttpsURLConnection)
				((HttpsURLConnection)urlConn).setHostnameVerifier(new MyHostnameVerifier());
			if (!upload && (type.equals(METHOD_POST) || type.equals(METHOD_PUT)) && args.length > 2) {
				String body = args[2];
				if (null != body) {
					urlConn.setRequestProperty("Content-Type", "application/json");
					urlConn.getOutputStream().write(body.getBytes("UTF-8"));
					urlConn.getOutputStream().flush();
					urlConn.getOutputStream().close();
				}
			} else if (upload && filename.length() > 0) {
				
				final String newLine = "\r\n";
				final String boundaryPrefix = "--";
				
				String HEADERBOUNDARY = "" + System.currentTimeMillis();
				String BOUNDARY = boundaryPrefix + HEADERBOUNDARY;  //"------WebKitFormBoundaryGsokj37Tl567UVfe"; 		
				String DISPOSITION = "Content-Disposition: form-data; name=\"format\"";
				String FORMAT = "csv";
				urlConn.addRequestProperty("Content-Type", "multipart/form-data; boundary=" + HEADERBOUNDARY); 
				StringBuilder sb = new StringBuilder(); 
				 
				sb.append(boundaryPrefix);
				sb.append(BOUNDARY);
				sb.append(newLine);		
				 
				sb.append("Content-Disposition: form-data; name=\"uploadedfile\"; filename=\"" + filename + "\"" + newLine);
				sb.append("Content-Type: text/csv; charset=UTF-8");
				 
				sb.append(newLine);
				sb.append(newLine);
				 
				byte[] start_data = sb.toString().getBytes("UTF-8");				
				urlConn.getOutputStream().write(start_data);
				urlConn.getOutputStream().flush();
				
				FileInputStream sqlFileStream = null;
				byte[] file_data = null;
				
				try {
					
					sqlFileStream = new FileInputStream(new File(filename));
					file_data = new byte[sqlFileStream.available()];
					sqlFileStream.read(file_data);
					sqlFileStream.close();
					urlConn.getOutputStream().write(file_data);
					urlConn.getOutputStream().flush();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
				
				byte[] end_data = (newLine + BOUNDARY + newLine + 
			    		  DISPOSITION + newLine + newLine
						  +FORMAT +newLine +
						  BOUNDARY + boundaryPrefix + newLine).getBytes("UTF-8");
				urlConn.getOutputStream().write(end_data);
				urlConn.getOutputStream().flush();
				urlConn.getOutputStream().close();
			}
			
			urlConn.connect();
			int responseCode = urlConn.getResponseCode();
			res.status = responseCode;
			res.headers = urlConn.getHeaderFields();
			HashMap<String, List<String>> map = new HashMap<>();
			Set<String> keys = res.headers.keySet();
			for (String key:keys) {
				if (null != key)
					map.put(key.toLowerCase(), res.headers.get(key));//change key to lowercase
			}
			res.headers = map;
			InputStream inputStream = null;
			if (responseCode == HttpURLConnection.HTTP_CREATED || responseCode == HttpURLConnection.HTTP_OK) {				
				inputStream = urlConn.getInputStream();	
				BufferedReader d = new BufferedReader(new InputStreamReader(
						inputStream));
				String readLine = null;
				StringBuffer sf = new StringBuffer();
				while ((readLine = d.readLine()) != null) {
					sf.append(readLine);
					sf.append("\n");
				}
				inputStream.close();
				d.close();
				res.result = sf.toString();
				try {
					JSONArtifact raw = JSON.parse(res.result);
					if (raw instanceof JSONObject)
						res.jsonResult = (JSONObject) raw;
					else if (raw instanceof JSONArray) 
						res.jsonarrayResult = (JSONArray) raw;
				} catch (JSONException e) {
					
				}				
			} else {//request is not successful
				inputStream = urlConn.getErrorStream();	
				StringBuffer sb = new StringBuffer();
				String readLine = null;		
				byte[] buffer = new byte[1024];
				int length = -1;
				while ((length = inputStream.read(buffer)) != -1) {				
					readLine = new String(buffer, 0, length, "UTF-8");
					buffer = new byte[1024];
					
					sb.append(readLine);
				}
				res.error = sb.toString();
				res.result = res.error;
				try {
					JSONObject json = (JSONObject) JSON.parse(res.error);					
					res.jsonError = json;
					res.jsonResult = json;
				} catch (JSONException e) {
					
				}
				inputStream.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != urlConn) {
				urlConn.disconnect();
				urlConn = null;
			}
		}
		return res;
	}
	
	
	private HttpURLConnection getConn(String type, String urlStr, boolean isAuthCall) throws ProtocolException
	{
		HttpURLConnection urlConn;
		if (urlStr.startsWith("https"))
			urlConn = getHttpsConnection(urlStr, isAuthCall);
		else
			urlConn = getHttpConnection(urlStr, isAuthCall);
		urlConn.setRequestMethod(type);
		urlConn.setUseCaches(false);
		urlConn.setRequestProperty("Accept",
				"application/json");
		urlConn.setRequestProperty(
				"User-Agent",
				"Mozilla/5.0");
		urlConn.setRequestProperty("Accept-Charset", "UTF-8");
		urlConn.setConnectTimeout(TIMEOUT);//TODO
		urlConn.setReadTimeout(TIMEOUT);//TODO
		return urlConn;
	}
}

class MyHostnameVerifier implements HostnameVerifier {

	@Override
	public boolean verify(String arg0, SSLSession arg1) {
		return true;
	}

}