package com.ibm.datatools.test.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class JSONUtils {

	private final static Logger logger = LogManager.getLogger(JSONUtils.class);

    public static Map<String, String> parseJSON2MapString(String jsonStr){  
        Map<String, String> map = new HashMap<String, String>();  
        JSONObject json = JSONObject.fromObject(jsonStr);  
        for(Object k : json.keySet()){ 
            Object v = json.get(k);   
            if(null!=v){
                map.put(k.toString(), v.toString());  
            }
        }  
        return map;  
    }
    
	/**
	 * Given the queryID, return the JSONObject used in http request to execute this queryID
	 * 
	 * */
	public static JSONObject[] getJSONTemplate4RunQuery(String queryID,int size){
		JSONObject[] result = new JSONObject[size];
		for(int i=0;i<size;i++){
			JSONObject obj = new JSONObject();
			obj.accumulate( "action", "RUNCOMMAND" );
			obj.accumulate( "type", "JDBC" );
			obj.accumulate( "queryId", queryID );
			obj.accumulate( "index", i );
			obj.accumulate( "total", size );
			result[i] = obj ;
		}
		
		return result;
		
	}
	/**
	 * Get the JSONOBject used to execute a sql in JDBC, execute the sql will not really run the sql
	 * just return the sqlID
	 * */
	public static JSONObject getJSONTemplate4ExecuteInJDBC(String script,String terminator){
		
		JSONObject result = new JSONObject();
		result.accumulate("script", script);
		result.accumulate("offset", 0);
		result.accumulate("type", "JDBC");
		result.accumulate("terminator", terminator);
		result.accumulate("rowLimit", "100");
		result.accumulate("failureAction", "CONTINUE");
		result.accumulate("successAction", "AUTOCOMMIT");
		result.accumulate("showRowNumber", "true");
		result.accumulate("refreshExplorer", "false");
		result.accumulate("action", "RUNSCRIPT");
		return result ;
	}
	/**
	 * Get the JSONOBject used to execute a sql in JDBC by reusing JDBC connectoin, execute the sql will not really run the sql
	 * just return the sqlID
	 * */
	public static JSONObject getJSONTemplate4ExecuteInReusedJDBC(String script,String terminator){
		
		JSONObject result = new JSONObject();
		result.accumulate("script", script);
		result.accumulate("offset", 0);
		result.accumulate("type", "JDBC");
		result.accumulate("terminator", terminator);
		result.accumulate("rowLimit", "100");
		result.accumulate("failureAction", "CONTINUE");
		result.accumulate("successAction", "AUTOCOMMIT");
		result.accumulate("showRowNumber", "true");
		result.accumulate("refreshExplorer", "false");
		result.accumulate("action", "RUNSCRIPT");
		result.accumulate("reuseConnection", "true");
		result.accumulate("clientId", "1");
		return result ;
	}
	/**
	 * Get the JSONOBject used to execute a sql in CLP
	 * */
	public static JSONObject getJSONTemplate4ExecuteInCLP(String script,String terminitor,boolean addConnectTo){
		
		JSONObject result = new JSONObject();
		result.accumulate("script", script);
		result.accumulate("offset", 0);
		result.accumulate("type", "CLPSSH");
		result.accumulate("terminator", ";");
		result.accumulate("rowLimit", "100");
		result.accumulate("failureAction", "CONTINUE");
		result.accumulate("successAction", "AUTOCOMMIT");
		result.accumulate("showRowNumber", "true");
		result.accumulate("addConnectTo", addConnectTo);
		result.accumulate("refreshExplorer", "false");
		result.accumulate("action", "RUNSCRIPT");
		return result ;

	}

	public static boolean compareJsons(JSONObject base, JSONObject result) {
		return compareJsons(base, result, null, false);
	}

	public static boolean compareJsons(JSONObject base, JSONObject result, ArrayList<String> ignoreKeyValues) {
		return compareJsons(base, result, ignoreKeyValues, false);
	}

	public static boolean compareJsons(JSONObject base, JSONObject result, Boolean ignoreNumValues) {
		return compareJsons(base, result, null, ignoreNumValues);
	}

	/**
	 * Compare two jsonObjects. 
	 * base: The expected jsonObject.
	 * result: The actual jsonObject. 
	 * ignoreKeys: Only compare these keys, but don't compare these key's values. 
	 * ignoreNumValues: true -- all Integer, Long, Double values don't be compared. 
	 *                  false -- all Integer, Long, Double values will be compared. 
	 * return value: true -- the base josonObject and the result jsonObject are the same with the condition ignoreKeys and ignoreNumValues. 
	 *               false -- the base josonObject and the result jsonObject are not the same with the condition ignoreKeys and ignoreNumValues.
	 */
	public static boolean compareJsons(JSONObject base, JSONObject result, ArrayList<String> ignoreKeys,
			Boolean ignoreNumValues) {
		return compareJsons(base, result, ignoreKeys, ignoreNumValues, null);
	}
	public static boolean compareJsons(JSONObject base, JSONObject result, ArrayList<String> ignoreKeys,
			Boolean ignoreNumValues, List<String> ArrayKeyList) {
		if (base.compareTo(result) == 0) {
			return true;
		}
		Iterator its = base.entrySet().iterator();
		String baseValue = null;
		String resultValue = null;
		while (its.hasNext()) {
			Entry entry = (Entry) its.next();
			String key = (String) entry.getKey();

			if (result.containsKey(key)) {
				if ((base.get(key) instanceof JSONArray) && (result.get(key) instanceof JSONArray)) {
					JSONArray baseJsonArray = (JSONArray) base.get(key);
					JSONArray resultJsonArray = (JSONArray) result.get(key);
					if (!compareJsonArrays(baseJsonArray, resultJsonArray, ignoreKeys, ignoreNumValues, ArrayKeyList)) {
						return false;
					}
				} else if ((base.get(key) instanceof JSONObject) && (result.get(key) instanceof JSONObject)) {
					if (!compareJsons(base.getJSONObject(key), result.getJSONObject(key), ignoreKeys,
							ignoreNumValues, ArrayKeyList)) {
						return false;
					}
				} else {
					if (ignoreNumValues == true) {

						if ((base.get(key).equals(null) && result.get(key) instanceof Long)
								|| (base.get(key).equals(null) && result.get(key) instanceof Double)
								|| (base.get(key).equals(null) && result.get(key) instanceof Integer)
								|| (base.get(key) instanceof Long) || (base.get(key) instanceof Double)
								|| (base.get(key) instanceof Integer)) {
							continue;
						}
					}
					baseValue = base.get(key).toString();
					resultValue = result.get(key).toString();

					if (baseValue.equals(resultValue)) {
						continue;
					} else if (ignoreKeys != null && ignoreKeys.size() > 0) {
						Iterator it = ignoreKeys.iterator();
						int i = 0;
						while (it.hasNext()) {
							if (key.equalsIgnoreCase(it.next().toString())) {
								i = i + 1;
							}
						}
						if (i > 0) {
							continue;
						} else {
							logger.error("Suppose value of key " + key + " is " + baseValue + ",but current result is "
									+ resultValue);
							return false;
						}
					} else {
						logger.error("Suppose value of key " + key + " is " + baseValue + ",but current result is "
								+ resultValue);
						return false;
					}
				}
			} else {
				logger.error("No Key:" + key);
				return false;
			}
		}
		return true;
	}
	
	public static boolean compareJsonArrays(JSONArray baseJsonArray, JSONArray resultJsonArray) {
		return compareJsonArrays(baseJsonArray, resultJsonArray, null, false, null);
	}
	
	private static boolean compareJsonArrays(JSONArray baseJsonArray, JSONArray resultJsonArray,
			ArrayList<String> ignoreKeyValues, Boolean ignoreNumValues, List<String> ArrayKeyList) {

		for (int i = 0; i < baseJsonArray.size(); i++) {
			int resultIndex = -1;
			if (baseJsonArray.get(i) instanceof JSONArray) {
				resultIndex = i;
			} else {
				resultIndex = getIndexFromJsonArray(resultJsonArray, baseJsonArray.get(i), ArrayKeyList);
			}
			
			if (resultIndex >= 0) {
				if ((baseJsonArray.get(i) instanceof JSONArray) && (resultJsonArray.get(resultIndex) instanceof JSONArray)) {
					if (!compareJsonArrays((JSONArray) baseJsonArray.get(i), (JSONArray) resultJsonArray.get(resultIndex),
							ignoreKeyValues, ignoreNumValues, ArrayKeyList)) {
						return false;
					}
				} else if ((baseJsonArray.get(i) instanceof JSONObject)
						&& (resultJsonArray.get(resultIndex) instanceof JSONObject)) {
					if (!compareJsons(baseJsonArray.getJSONObject(i), resultJsonArray.getJSONObject(resultIndex), ignoreKeyValues,
							ignoreNumValues)) {

						return false;
					}

				} else {
					if (ignoreNumValues == true) {
						if ((baseJsonArray.get(i) == null && resultJsonArray.get(resultIndex) instanceof Long)
								|| (baseJsonArray.get(i) == null && resultJsonArray.get(resultIndex) instanceof Double)
								|| (baseJsonArray.get(i) == null && resultJsonArray.get(resultIndex) instanceof Integer)
								|| (baseJsonArray.get(i) instanceof Long) || (baseJsonArray.get(resultIndex) instanceof Double)
								|| (baseJsonArray.get(i) instanceof Integer)) {
							continue;
						}
						String baseJsonArrayChild = baseJsonArray.get(i).toString();
						String resultArrayChild = resultJsonArray.get(resultIndex).toString();

						if (!baseJsonArrayChild.equals(resultArrayChild)) {
							logger.error("Suppose value is " + baseJsonArrayChild + ",but current result is "
									+ resultArrayChild);
							return false;
						}
					}
				}
			} else {
				logger.error("The resultJsonArray can't contain BaseJsonObject");
				return false;
			}
		}
		return true;
	}
	
	/*
	 * When ArrayKeyList is null:
	 *  If the resultJsonArray contains one JSONObject which all keys' values are the same as oneBaseJSONObject, then return the index of 
	 *  the resultJsonArray which is the same as oneBaseJSONObject;
 	 *  Otherwise, return -1;
 	 * When ArrayKeyList is not null:
 	 *  If the resultJsonArray contains one JSONObject which only the values of the keys belonging to ArrayKeyList are the same as oneBaseJSONObject,
 	 *  then return the index of the resultJsonArray which is the same as oneBaseJSONObject;;
 	 *  Otherwise, return -1;
	 */
	private static int getIndexFromJsonArray(JSONArray resultJsonArray, Object oneBaseObject, List<String> ArrayKeyList) {
		int index = -1;
		Boolean isArrayKeyListValid = false;
		List<String> validKeys = new ArrayList<String>();
		
		if(resultJsonArray == null || resultJsonArray.size() == 0 || oneBaseObject == null) {
			return index;
		} 
		
		if(ArrayKeyList == null || (!(oneBaseObject instanceof JSONObject))) {
			return resultJsonArray.indexOf(oneBaseObject);
		}
		
		JSONObject oneBaseJSONObject = null;
		if(oneBaseObject instanceof JSONObject) {
			oneBaseJSONObject = JSONObject.fromObject(oneBaseObject);
		}

		for(int i=0; i<ArrayKeyList.size(); i++){
			if(oneBaseJSONObject.containsKey(ArrayKeyList.get(i))) {
				isArrayKeyListValid = true;
				validKeys.add(ArrayKeyList.get(i));
			}
		}
		
		if(isArrayKeyListValid == false) {
			return resultJsonArray.indexOf(oneBaseJSONObject);
		}
		
		for(int i=0; i<resultJsonArray.size(); i++) {
			JSONObject oneResultJSONArray = resultJsonArray.getJSONObject(i);
			Boolean containsEveryKey = true;
			for(int j=0; j<validKeys.size(); j++) {
				String key = validKeys.get(j);
				if(!oneResultJSONArray.containsKey(key) 
				  || (!oneResultJSONArray.get(key).equals(oneBaseJSONObject.get(key)))) {
					containsEveryKey = false;
					break;
				}
			}
			if(containsEveryKey == true) {
				index = i;
				break;
			}
		}
		
		return index;
	}
	
	/**
	 * 
	 * <B>Purpose:</B> Transform a map to JSON string
	 * 
	 * @param map
	 *            a map instance which contains key/value pairs
	 * @return a JSON string
	 */
	public static String map2JSONString(Map<String, String> map) {
		JSONObject json = new JSONObject();
		try {
			for (Map.Entry<String, String> pair : map.entrySet()) {
				String name = pair.getKey();
				String value = pair.getValue();
				json.put(name, value);
			}
		} catch (Exception ex) {
			return "";
		}
		return json.toString();
	}	
}
