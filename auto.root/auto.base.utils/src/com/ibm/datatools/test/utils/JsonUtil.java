package com.ibm.datatools.test.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.json.JSON;
import org.apache.commons.json.JSONArray;
import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;

public class JsonUtil {

	public static void main(String[] args) throws JSONException {
//		Path path = Paths.get("ori.txt");
//		BufferedReader br;
//		try {
//			br = Files.newBufferedReader(path);
//			String line = "";
//			StringBuilder sb = new StringBuilder();
//			while (null != (line = br.readLine())) {
//				line = line.trim();
//				if (notEmpty(line) && !line.startsWith("#")) {
//					sb.append(line);			
//				}
//			}
//			br.close();
////			System.out.println(sb.toString());
//			String ori = sb.toString();
//			Pattern p = Pattern.compile("('|\")((\\w|\\.)+)('|\")\\s*:\\s*('|\")((\\w|\\.)+)('|\")");
//			Matcher m = p.matcher(ori);
//			while (m.find()) {
//				System.out.println(m.group(1));
//			}
//			
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		getJsonUnderDef("achievements.json");
		
//		JSONObject o = new JSONObject();
//		try {
//			o.put("user", "admin");
//			o.put("pw", "pw");
//			Pattern p = generatePattern(o);
//			Matcher m = p.matcher("tsted${user}");
//			System.out.println(m.find());
//			System.out.println(m.group(0));
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
//		JSONObject o = new JSONObject();
//		try {
//			o.put("id", "db2inst1");
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		System.out.println(getJsonUnderDef("sqleditor_spec.json", o));
		System.out.println("/v2.0/hus".matches("^/v[23]\\.0/.+"));
	}
	
	public static JSONArray getJsonUnderDef(String fpath) {
		return FileTools.fiterByTag(getJsonUnderDef(fpath, null));
	}
	
	public static JSONArray getJsonUnderDef(String fpath, boolean exactPath) {
		return getJsonUnderDef(fpath, null, exactPath);
	}

	/**
	 * 
	 * @param fpath
	 * @param replacements
	 * @param exactPath The given path is a complete path thus no need to add a version prefix
	 * @return
	 */
	public static JSONArray getJsonUnderDef(String fpath, JSONObject replacements, boolean exactPath) {
		JSONArray result = null;
		String prefix = "def/";
		String version = Setting.getSetting().getVersion();
		String prefix2 = "v2.0/";
		if (version.equalsIgnoreCase("v3"))
			prefix2 = "v3.0/";
		if (!exactPath && !fpath.matches("^/v[23]\\.0/.+"))//the path already has a version
			prefix = "def/" + prefix2;
		Path path = Paths.get(prefix + fpath);
		BufferedReader br;
		try {
			br = Files.newBufferedReader(path);
			String js = parse(br, replacements);
			result = (JSONArray) JSON.parse(js);
			br.close();
		} catch (IOException | JSONException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * @param fpath
	 * @param replacements
	 * @param exactPath The given path is a complete path thus no need to add a version prefix
	 * @return
	 */
	public static String getContentUnderDef(String fpath, JSONObject replacements, boolean exactPath) {
		String prefix = "def/";
		String version = Setting.getSetting().getVersion();
		String prefix2 = "v2.0/";
		String js = null;
		if (version.equalsIgnoreCase("v3"))
			prefix2 = "v3.0/";
		if (!exactPath && !fpath.matches("^/v[23]\\.0/.+"))//the path already has a version
			prefix = "def/" + prefix2;
		Path path = Paths.get(prefix + fpath);
		BufferedReader br;
		try {
//			br = Files.newBufferedReader(path);
			js = readAndReplace(path, replacements);
//			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return js;
	}
	
	public static JSONArray getJsonUnderDef(String fpath, JSONObject replacements) {
		return getJsonUnderDef(fpath, replacements, false);
	}
	
	/**
	 * Read from a file and replace ${} with replacements. replacements has key/value pairs in json format
	 * @param br
	 * @param replacements in JSON format
	 * @return
	 */
	private static String parse(BufferedReader br, JSONObject replacements) {
		boolean rep = (null != replacements && replacements.length() > 0);
		String line = "";
		StringBuilder sb = new StringBuilder();
		try {
			while (null != (line = br.readLine())) {
				if ((line = compress(line)).length() > 0) {
					sb.append(line);			
				}
			}
			
			if (rep) {				
				return replaceVariables(sb.toString(), replacements);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	private static String readAndReplace(Path path, JSONObject replacements) throws IOException {
		boolean rep = (null != replacements && replacements.length() > 0);
		StringBuffer sb = new StringBuffer();

		List<String> lines = Files.readAllLines(path);
		for (String line : lines) {
			sb.append(line);
			sb.append("\n");
		}
		if (rep) {
			return replaceVariables(sb.toString(), replacements);
		}

		return sb.toString();
	}
	
	/**
	 * trim each line and remove comments after //, comments should start with //
	 * @param line
	 * @return
	 */
	private static String compress(String line) {
		line = line.trim();
		if (line.startsWith("//"))
			line = "";
		return line;
	}
	
	private static Pattern generatePattern(JSONObject replacements) {
		Iterator it = null;
		if(replacements != null)
			it = replacements.keys();
		StringBuilder sb = new StringBuilder("\\$\\{(");
		int i = 0;
		while (it!=null && it.hasNext()) {
			String key = (String) it.next();
			sb.append("(");
			sb.append(key);
			sb.append(")");
			i++;
			if (i < replacements.length())
				sb.append("|");
		}
		sb.append(")\\}");
		return Pattern.compile(sb.toString());
	}
	
	public static boolean isGoodJson(String json) {
		if (json == null || json.isEmpty()) {
			return false;
		}
		try {
			JSONObject temp =  (JSONObject) JSON.parse(json);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static boolean isGoodJsonArray(String json) {
		if (json == null || json.isEmpty()) {
			return false;
		}
		try {
			JSONArray temp =  (JSONArray) JSON.parse(json);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static String replaceVariable(String str, String variable, String value) {	
		variable = "\\$\\{"+variable+"\\}";
		return str.replaceAll(variable, value);		
	}
	
	/**
	 * 
	 * @param text param format is ${}
	 * @param replacements key/value pairs in JSON format
	 * eg. ${id} will be replaced with the value of 'id' key in replacement
	 * @return
	 */
	public static String replaceVariables(String text, JSONObject replacements) {
		Pattern p = generatePattern(replacements);
		Matcher m = p.matcher(text);
		StringBuffer sbf = new StringBuffer();
		while (m.find()) {
			String param = m.group(1);
			String rp = replacements.optString(param);
			if (null != rp) {
				rp = rp.replaceAll("\"", "\\\\\\\\\"");//replace " to avoid JSON parse problem. \\\\ -> \
//				System.out.println(rp);
				m.appendReplacement(sbf, rp);
			}
		}
		m.appendTail(sbf);
//		System.out.println(sbf);
		return sbf.toString();
	}
	
	public static JSONObject toJSONObject(String str) {
		JSONObject json = null;
		try {
			json = (JSONObject) JSON.parse(str);
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	}
	
	
}