package com.ibm.datatools.test.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;

public class Properties {
	
	private static java.util.Properties defaultProp;
	private java.util.Properties prop;

	static {
		String configFile =getCurrentWorkPath()+"config/config.properties";
		defaultProp=new java.util.Properties();
		try {  
			defaultProp.load(new FileInputStream(configFile));  
        } catch (IOException e) {  
            e.printStackTrace();  
        } 
	}
	public static String getConfigValue(String key){		
		return defaultProp.getProperty(key);
	}
	
	public Properties(String filePath){
		prop= new java.util.Properties();  
		try {  
			prop.load(new FileInputStream(filePath));  
			prop.list(System.out);  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
	}
	
	public String getProValue(String key) {
		if(!containsProKey(key)){
			return null;
		}else
		return prop.getProperty(key);
	}
	
	public boolean containsProKey(String key) {
		return prop.containsKey(key);
	}

	public static String getCurrentWorkPath() {
		String result = "";
		try {
			URL url = Thread.currentThread().getContextClassLoader()
					.getResource("");
			result = url.getFile();
		} catch (Exception e) {
		}
		if (result == null || result.equals("")) {
			return result;
		}
		String userDir = System.getProperty("user.dir");
		java.io.File file = new java.io.File(result);
		result = file.getPath();
		if (result.startsWith(userDir)) {
			result = userDir;
		}
		return result + java.io.File.separator;
	}
	

}
