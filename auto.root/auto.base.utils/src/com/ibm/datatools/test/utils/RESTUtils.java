package com.ibm.datatools.test.utils;

import org.apache.commons.json.JSONArray;
import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;

public class RESTUtils {
	
//	public static String replaceAllParams(String str, String userid, String password) {
//		return str.replaceAll("\\$\\{USERID\\}", userid)                          //replace ${USERID}
//				  .replaceAll("\\$\\{USERID_UPPERCASE\\}",userid.toUpperCase())   //replace ${USERID_UPPERCASE}
//				  .replaceAll("\\$\\{PASSWORD\\}", password);                     //replace ${PASSWORD}
//		
//	}
	public static String replaceKnownParams(String str, String userid, String password) throws JSONException {
		JSONObject replacements = new JSONObject();
		replacements.put("USERID", userid);//replace ${USERID}
		replacements.put("USERID_UPPERCASE", userid.toUpperCase()); //replace ${USERID_UPPERCASE}
		replacements.put("PASSWORD", password);//replace ${PASSWORD}
		Setting st = Setting.getSetting();
		replacements.put("ADMINID", st.getAdminID());//replace ${ADMINID}
		replacements.put("ADMINID_UPPERCASE", st.getAdminID().toUpperCase());//replace ${ADMINID_UPPERCASE}
		replacements.put("ADMINPW", st.getAdminPwd());//replace ${ADMINPW}
		replacements.put("NONADMINID", st.getNonAdminID());//replace ${NONADMINID}
		replacements.put("NONADMINID_UPPERCASE", st.getNonAdminID().toUpperCase());
		replacements.put("NONADMINPW", st.getNonAdminPwd());//replace ${NONADMINPW}	
		replacements.put("SCHEMA", st.getAdminID().toUpperCase());//replace ${SCHEMA}
		return JsonUtil.replaceVariables(str, replacements);	
	}

	public static Response runDDL(String file) throws Exception {
		JSONObject replacements = new JSONObject();
		Setting st = Setting.getSetting();
		String ddl = FileTools.readFile(file, "--");
		replacements.put("SCHEMA", st.getAdminID().toUpperCase());//replace ${SCHEMA}
		ddl = JsonUtil.replaceVariables(ddl, replacements);
		replacements.remove("SCHEMA");
		replacements.put("commands", ddl);
		JSONArray js = JsonUtil.getJsonUnderDef("setup_runddl.json", replacements, true);
		System.out.println(js);
		JSONObject request = js.getJSONObject(0);
		HTTPClient client = new HTTPClient(true);
		return client.call(request.getString("method"), st.getURL() + "/" + request.getString("url"), request.getString("body"));
	}
}
