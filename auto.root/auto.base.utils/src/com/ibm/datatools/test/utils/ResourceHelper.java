package com.ibm.datatools.test.utils;

public final class ResourceHelper  {

	private String key = "xV37oP9876rtyukl9wjh909h";	//$NON-NLS-1$
	
	public ResourceHelper() {
	    // add some check to see whether the caller is 
	    // allowed to instantiate this class???
	    
	    // enhance the key with some userdefined suffix
	    String morekey = getUserKey();
	    key = (key + morekey).trim();
	}
	
	private String getUserKey() {
	    String userkey =  ""; //$NON-NLS-1$
	    
	    // read a key from a properties file?
	    // we could even provide an option to encrypt this userkey
	    // with our key, that is, the userkey would be encrypted
	    // just with the key, and everything else with the 
	    // private key and the decrypted userkey
	    
	    return userkey;
	}
	
	public String getPassword(String pswd) { 
		return decryptPassword(pswd);
		
	}
	
	public String getPasswordEncrypted(String pswd) {
		return encryptPassword(pswd);
		
	}
	
	public byte[] encryptPassword(byte[] pswd) {
	    DES des = new DES(key);
	    return des.encode(pswd);
	}
		
	public String getPasswordFromBytes(byte[] pswd) {
	    DES des = new DES(key);
		try {
			String decpswd = new String(des.decode(pswd), "UTF-8");
			return decpswd;
		} catch (Exception e) {
			// The String( ) constructor should never fail since UTF-8 is always 
			// a valid encoding in java - this return will satsify the compiler
			return null;
		}
	}
	
	private String decryptPassword(String pswd) {
		String decpswd = null;
		DES des =  new DES(key);
		decpswd = des.decode(pswd);
		//System.out.println("encrypted=" + pswd + "-> " + decpswd);
		return decpswd;
	}
	
	private String encryptPassword(String pswd) {
		String decpswd = null;
		DES des =  new DES(key);
		decpswd = des.encode(pswd);
		//System.out.println("encrypted=" + decpswd);
		return decpswd;
	}
	
	/**
	 * This helper routine generates a string containing the hex encoding for
	 * a byte array that was previously encrypted by the above functions.  This   
	 * routine can be used to encode the data in a hex format that can be written
	 * to disk ( and read from the disk ).  This function is directly used 
	 * by the Design Studio.
	 * @param input - byte array containing an encoded string.
	 * @return - A string containing the base16 encoding of the byte array 
	 */
	public String createHexEncodingFromByteArray( byte[] input )
	{
		StringBuffer formattedStringRep = new StringBuffer(input.length*2);
		for( int i=0; i < input.length; i++)
		{
			formattedStringRep.append( String.format("%02x", input[i] ) );
		}
		return formattedStringRep.toString();
	}

	/**
	 * This helper function returns a hex-decimal string that can be 
	 * stored on the disk or in the database from a string input.  This
	 * function first converts the input string to a byte array and then 
	 * encrypts the byte array.  Finally a hex representation of the 
	 * encrypted byte array is returned to the caller.  The 
	 * getPasswordFromHexEncoding() function can be used to decrypt the 
	 * hex string to get back the original value.  This function is 
	 * primarily used by the DWEAdmin
	 *   
	 * @param input User-provided string to be encrypted
	 * @return String containing the hex representation of the encoded
	 *         string.
	 */

	public String encryptPasswordReturnHexEncoding( String input )
	{
		try
		{
			byte[] byteValue = input.getBytes("UTF-8");
			byte[] encryptedBytes = encryptPassword(byteValue);
			return( createHexEncodingFromByteArray(encryptedBytes) );
		} catch ( Exception e ) { };
		// The getBytes( ) call should never fail since UTF-8 is always 
		// a valid encoding in java - this return will satsify the compiler
		return null;
	}



	/**
	 * This helper function returns an decoded password from a hexidecimal (base16) 
	 * encoding.  
	 * @param hexInput - the hex representation that contains the string to be converted
	 * @return
	 */
	public String getPasswordFromHexEncoding( String hexInput )
	{
		byte[] output = new byte[hexInput.length()/2];

		for( int i=0; i < hexInput.length()/2 ;i++ )
		{
			// We'll parse 2 bytes at a time from the input string to be converted
			// into a single byte value.  For example, given the input hex value
			// "fe8034", we will process "fe", then "80" and finally "34"
			// The integer value will then be converted to it's signed byte
			// values ( in the example above : -2, 128 and 52 ).
			// The Byte.parseByte() routine does not handle values greater than 7F
			// which is why we're using an Integer to parse the byte value.

			String byteChar = hexInput.substring(i*2, i*2+2 );
			Integer val = Integer.parseInt(byteChar, 16);

			output[i] = val.byteValue();
		}

		return getPasswordFromBytes(output);
	}


}
