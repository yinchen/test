package com.ibm.datatools.test.utils;

import java.util.List;
import java.util.Map;

import org.apache.commons.json.JSONArray;
import org.apache.commons.json.JSONObject;


public class Response {
	int status;
	String result;
	String error;
	JSONObject jsonResult;
	JSONObject jsonError;
	JSONArray jsonarrayResult;
	Map<String, List<String>> headers;
	
	public int getStatus() {
		return status;
	}
	public String getResult() {
		return result;
	}
	public String getError() {
		return error;
	}
	public JSONObject getJsonResult() {
		return jsonResult;
	}
	public JSONArray getJsonArrayResult() {
		return jsonarrayResult;
	}
	public JSONObject getJsonError() {
		return jsonError;
	}
	public Map<String, List<String>> getHeaders() {
		return headers;
	}
}
