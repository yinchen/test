package com.ibm.datatools.test.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Properties;

import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;


public class Setting {
	
	Properties properties = new Properties();
	private static Setting INSTANCE = new Setting();
	private String URL;
	private String prefixURL;
	private String sfname = "names.conf";
	private String curlFileName = null;
	private boolean initianized = false;
	
	private Setting() {
		INSTANCE = this;
	}
	public String getNonAdminID() {
		return properties.getProperty("nonAdminID");
	}
	public String getNonAdminPwd() {
		return properties.getProperty("nonAdminPwd");
	}
	public String getAdminID() {
		return properties.getProperty("adminID");
	}
	public String getAdminPwd() {
		return properties.getProperty("adminPwd");
	}	
	public String getContext() {
		return d(properties.getProperty("context"));
	}
	public String getURL() {
		return URL;
	}
	public String getPrefixURL() {
		return prefixURL;
	}
	public String getHost() {
		return properties.getProperty("host");
	}
	public int getPort() {
		return Integer.parseInt(properties.getProperty("port"));
	}
	public boolean isToken() {
		return properties.getProperty("auth").equals("token");
	}
	protected String getBaseURL() {
		return properties.getProperty("baseurl");
	}
	public String getDBName() {
		return properties.getProperty("db");
	}
	public boolean isSSL() {
		return Boolean.parseBoolean(properties.getProperty("ssl"));
	}
	public String getVersion() {
		return properties.getProperty("version");
	}
	public String getProxyHost() {
		return properties.getProperty("proxy.host");
	}
	public String getProxyPort() {
		return properties.getProperty("proxy.port");
	}
	public String getTags() {
		return properties.getProperty("tags");
	}
	public boolean generateCurl() {
		return d(properties.getProperty("curl")).equalsIgnoreCase("yes");
	}
	@Parameters({ "version" })
	@BeforeSuite(alwaysRun=true,enabled=true)
	public void loadSetting(String version) {
		if (version.equals("v3")) {
			sfname = "namesv3.conf";
		} else if (version.equals("v2")) {
			sfname = "names.conf";
		}
		loadSetting(true);
	}
	private void loadSetting(boolean force){
		if (!force && initianized)
			return;
		Path path = Paths.get(sfname);
		BufferedReader br;
		try {
			br = Files.newBufferedReader(path);
			String line = "", confname = "";
			while (null != (line = br.readLine())) {
				line = line.trim();
				if (notEmpty(line) && !line.startsWith("#")) {
						confname = line;				
				}
			}
			br.close();
			
			path = Paths.get(confname);
			br = Files.newBufferedReader(path);
			while (null != (line = br.readLine())) {
				line = line.trim();
				if (notEmpty(line) && !line.startsWith("#")) {
					int index = line.indexOf("=");
					String name = line.substring(0, index).trim();
					String value = line.substring(index + 1).trim();
					properties.put(name, d(value));
				}
			}
			br.close();
			
			String protocol = "http";
			if (isSSL())
				protocol = "https";
			
			prefixURL = protocol + "://" + getHost() + ":" + getPort();
			URL = prefixURL + getContext() + getBaseURL();
			initianized = true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public static Setting getSetting() {
		INSTANCE.loadSetting(false);
		return INSTANCE;
	}
	public String getAuthBody(boolean isAdmin){
		JSONObject rj = new JSONObject();
		String id = getNonAdminID(), pw = getNonAdminPwd();
		if (isAdmin) {
			id = getAdminID();
			pw = getAdminPwd();
		}
		try {
			rj.put("userid", id);
			rj.put("password", pw);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rj.toString();		
	}
	private boolean notEmpty(String line) {
		return line.trim().length() > 0;
	}
	private String d(String ori) {
		return null == ori ? "" : ori;
	}
	public String getCurlFileName() throws JSONException, IOException {
		if (null == curlFileName) {
			curlFileName = "curl-api-v2";
			String header = "";
			if (getVersion().equals("v3")) {
				curlFileName = "curl-api-v3";
			}
			Instant now = Instant.now();
			LocalDateTime ldt = LocalDateTime.ofInstant(now, ZoneId.systemDefault());
			curlFileName = curlFileName + "-" + ldt.getYear() + "-" + ldt.getMonthValue() + "-" + ldt.getDayOfMonth() + "-"
					+ ldt.getHour() + "-" + ldt.getMinute() + ".sh";
			Path path = Paths.get(curlFileName);
			JSONObject jo = new JSONObject();
			jo.put("h", getHost());
			jo.put("a", getAdminID());
			jo.put("ap", getAdminPwd());
			jo.put("u", getNonAdminID());
			jo.put("p", getNonAdminPwd());
			if (!Files.exists(path)) {
				header = JsonUtil.getContentUnderDef("header-curl-api.sh.template", jo, false);
				Files.write(path, header.getBytes());
			}
		}
		return curlFileName;
	}
	public void setCurlFileName(String curlFileName) {
		this.curlFileName = curlFileName;
	}
}
