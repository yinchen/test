package com.ibm.datatools.test.utils;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Reporter;


public class TestLogger {
	
	private static Logger logger = LogManager.getLogger("TestLogger");
	
	public static void logInfo(String message){
		logger.info(message);
	  	//System.out.println(message+"\n");
		String dateInfo = DateFormatUtils.format(System.currentTimeMillis(),"yyyy-MM-dd_hh:mm:ss");
		if( message!= null  && ( message.contains("<") || message.contains(">") ))
			message = message.replaceAll("<", "&lt").replaceAll(">", "&gt");
		Reporter.log("<font color=Blue>[" + dateInfo + "] " + message + "</font>",3);
	}
	
	public static void logSuccess(String message){
		logger.info(message);
		//System.out.println(message+"\n");
		String dateInfo = DateFormatUtils.format(System.currentTimeMillis(),"yyyy-MM-dd_hh:mm:ss");
		if( message!= null  && ( message.contains("<") || message.contains(">") ))
			message = message.replaceAll("<", "&lt").replaceAll(">", "&gt");
		Reporter.log("<font color=Green>[" + dateInfo + "] " + message + "</font>",1);	
	}
	
	public static void logError(String message){
		logger.error(message);
		//System.out.println(message+"\n");
		String dateInfo = DateFormatUtils.format(System.currentTimeMillis(),"yyyy-MM-dd_hh:mm:ss");
		if( message!= null  && ( message.contains("<") || message.contains(">") ))
			message = message.replaceAll("<", "&lt").replaceAll(">", "&gt");
		Reporter.log("<font color=Red>[" + dateInfo + "] " + message + "</font>",1);	
	}
	
	public static void logError(String message, Throwable e){
		logger.error(message, e);	
	}
	
	
	public static void logWarn(String message, Throwable e){
		logger.warn(message, e);	
	}
	
	public static void logDebug(String message, Throwable e){
		logger.debug(message, e);	
	}
	
	public static void logWarn(String message){
		//System.out.println(message+"\n");
		logger.warn(message);
		String dateInfo = DateFormatUtils.format(System.currentTimeMillis(),"yyyy-MM-dd_hh:mm:ss");
		if( message!= null  && ( message.contains("<") || message.contains(">") ))
			message = message.replaceAll("<", "&lt").replaceAll(">", "&gt");
		Reporter.log("<font color=Brown>[" + dateInfo + "] " + message + "</font>",2);	
	}	
	
	public static void logDebug(String message){
		//System.out.println(message+"\n");
		logger.debug(message);
		String dateInfo = DateFormatUtils.format(System.currentTimeMillis(),"yyyy-MM-dd_hh:mm:ss");
		if( message!= null  && ( message.contains("<") || message.contains(">") ))
			message = message.replaceAll("<", "&lt").replaceAll(">", "&gt");
		Reporter.log("<font color=Black>[" + dateInfo + "] " + message + "</font>",4);	
	}
	
	public static void logBlankLine(){
		Reporter.log("\n");	
	}
	
	public static void logStepLable(String stepLable){
		//System.out.println(stepLable+"\n");
		logger.info(stepLable);
		String dateInfo = DateFormatUtils.format(System.currentTimeMillis(),"yyyy-MM-dd_hh:mm:ss");
		if( stepLable!= null  && ( stepLable.contains("<") || stepLable.contains(">") ))
			stepLable = stepLable.replaceAll("<", "&lt").replaceAll(">", "&gt");
		String bar = "";
		for (int i = 0; i < stepLable.length()+4; i++)
			bar = bar + '*';
		Reporter.log("<font color=Blue>[" + dateInfo + "] " + bar + "</font>",3);
		Reporter.log("<font color=Blue>[" + dateInfo + "] * " + stepLable + " *</font>",3);
		Reporter.log("<font color=Blue>[" + dateInfo + "] " + bar + "</font>",3);
	}
	
	public static void logColorHeader(String colorName, String header){
		//System.out.println(stepLable+"\n");
		logger.info(header);
		String dateInfo = DateFormatUtils.format(System.currentTimeMillis(),"yyyy-MM-dd_hh:mm:ss");
		if( header!= null  && ( header.contains("<") || header.contains(">") ))
			header = header.replaceAll("<", "&lt").replaceAll(">", "&gt");
		String bar = "";
		for (int i = 0; i < header.length()+(header.length()/2); i++)
			bar = bar + '-';		
		Reporter.log("<font color="+colorName+">[" + dateInfo + "] " + bar + "</font>",3);
		Reporter.log("<font color="+colorName+">[" + dateInfo + "] * " + header + " *</font>",3);
		Reporter.log("<font color="+colorName+">[" + dateInfo + "] " + bar + "</font>",3);
	}	
	
	public static void logDebugHeader(String header){
		//System.out.println(stepLable+"\n");
		logger.debug(header);
		String dateInfo = DateFormatUtils.format(System.currentTimeMillis(),"yyyy-MM-dd_hh:mm:ss");
		if( header!= null  && ( header.contains("<") || header.contains(">") ))
			header = header.replaceAll("<", "&lt").replaceAll(">", "&gt");
		String bar = "";
		for (int i = 0; i < header.length()+(header.length()/2); i++)
			bar = bar + '-';		
		Reporter.log("<font color=Black>[" + dateInfo + "] " + bar + "</font>",4);
		Reporter.log("<font color=Black>[" + dateInfo + "] * " + header + " *</font>",4);
		Reporter.log("<font color=Black>[" + dateInfo + "] " + bar + "</font>",4);
	}		
	
	

	

	


}
