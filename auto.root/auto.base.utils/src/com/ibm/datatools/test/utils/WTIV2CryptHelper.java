
/**
 * IBM CONFIDENTIAL OCO SOURCE MATERIALS COPYRIGHT: 5725-R75 (C) COPYRIGHT IBM CORP. 2015, 2017.
 * The source code for this program is not published or otherwise divested of its trade secrets, 
 * irrespective of what has been deposited with the U.S. Copyright Office. 
 * */
package com.ibm.datatools.test.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
/**
 * @litaocdl
 * 
 * This is the core class for String encryption and decryption.
 * Caution that !! this class exists in three places, modification for this class need be sync to all those three locations
 * 1.com.ibm.datatools.dsweb.client.crypt.WTIV2CryptHelper
 * 2.com.ibm.datatools.common.services.util.crypt
 * 3.com.ibm.datatools.dsweb.otstext
 * 
 * By default, this class will use a default AES seeds to encrypt the key, but if invoking the 
 * setCustomAESKeySeeds(String seeds),  we will based on the input string to generate a new AES key seeds
 * 
 * */
public class WTIV2CryptHelper
{
    
    public static final String PREFIX = "wtiv2_";
    
    public static final int LEN_PREFIX = PREFIX.length();
    
    public static final String ALGO = "AES";
      
    //By default, we will use this raw as the AES key seeds, this is a length=16 seeds
    private byte[] raw = { (byte) 0xB6, (byte) 0x12, (byte) 0x8C, (byte) 0xF6, (byte) 0x34, (byte) 0xDB,
                    (byte) 0xE5, (byte) 0xC1, (byte) 0xD5, (byte) 0x4E, (byte) 0x6C, (byte) 0x7A, (byte) 0xFA, (byte) 0xB9,
                    (byte) 0x2B, (byte) 0xE0 };
           
    private int keySize = 128;  
           
    protected static WTIV2CryptHelper INSTANCE = new WTIV2CryptHelper();
    private static boolean ENABLE_TRACE = false ;
    private static String ENCRYPT_TRACE = "ENCRYPT_TRACE" ;
    static{
    	ENABLE_TRACE = "true".equalsIgnoreCase( System.getProperty(ENCRYPT_TRACE, System.getenv(ENCRYPT_TRACE))) ;	
    }
    /**
     * The default constructor will use default AES Key seeds
     * 
     * */
    public WTIV2CryptHelper()
    {
       
    }
    public static WTIV2CryptHelper getInstance()
    {
        return INSTANCE;
    }
    
    public void setCustomAESKeySeeds(String keySeeds){
    	//If Key seeds exists
    	if(keySeeds != null && keySeeds.trim().length() > 0){
    		
    		byte[] customizedKeyBytes;
			try {
				trace("Customized AES Key seeds provided is: "+ keySeeds);
				customizedKeyBytes = keySeeds.getBytes("UTF-8");
				//Use SHA-256 to get the signature for the bytes, the output is 256 bytes
				MessageDigest sha = MessageDigest.getInstance("SHA-256");
				customizedKeyBytes = sha.digest(customizedKeyBytes) ;
				//Get a raw with length is 16
		    	raw = Arrays.copyOf(customizedKeyBytes, keySize/8);
		    	
		    	trace("Use customize AES Key seeds: " + raw + " length is: "+ raw.length);
			} catch (UnsupportedEncodingException e) {
				System.out.println("setCustomAESKey Seeds Failed, using the default one"+e.getMessage());
			} catch (NoSuchAlgorithmException e) {
				System.out.println("setCustomAESKey Seeds Failed, using the default one"+e.getMessage());
			}
    		
    	}else{
    		//If key seeds not exists
    		trace("Use default key seeds: "+ raw+ " length is: "+ raw.length);
    	}
    }
    

    
    /**
     * Create a Hex from byte array
     * 
     * */
    public static String createHexEncodingFromByteArray(byte[] input)
    {
        StringBuffer formattedStringRep = new StringBuffer(input.length * 2);
        for (int i = 0; i < input.length; i++)
        {
            formattedStringRep.append(String.format("%02x", input[i]));
        }
        return formattedStringRep.toString();
    }
    /**
     * Create bytes from Hex
     * 
     * */
    public static byte[] getFromHexEncoding(String hexInput)
    {
        byte[] output = new byte[hexInput.length() / 2];
        
        for (int i = 0; i < hexInput.length() / 2; i++)
        {
            // We'll parse 2 bytes at a time from the input string to be
            // converted
            // into a single byte value. For example, given the input hex value
            // "fe8034", we will process "fe", then "80" and finally "34"
            // The integer value will then be converted to it's signed byte
            // values ( in the example above : -2, 128 and 52 ).
            // The Byte.parseByte() routine does not handle values greater than
            // 7F
            // which is why we're using an Integer to parse the byte value.
            
            String byteChar = hexInput.substring(i * 2, i * 2 + 2);
            Integer val = Integer.parseInt(byteChar, 16);
            
            output[i] = val.byteValue();
        }
        return output;
    }
    /**
     * Encrypt the given String
     * */
    public String encrypt(String data) throws Exception
    {
        byte[] encVal = encrypt(data.getBytes("UTF-8"));
        String realEnc = createHexEncodingFromByteArray(encVal);
        return PREFIX + realEnc;
    }
    /**
     * Decrypt the given String
     * */
    public String decrypt(String encryptedData) throws Exception
    {
        
        encryptedData = encryptedData.substring(LEN_PREFIX);
        byte[] encBytes = getFromHexEncoding(encryptedData);
        
        byte[] decValue = decrypt(encBytes);
        String decryptedValue = new String(decValue, "UTF-8");
        return decryptedValue;
    }
    
    
    /************************* Private *****************************/
  
    private void trace(String msg){
    	if(ENABLE_TRACE){
    		System.out.println("Encryption Trace : " + msg);
    	}
    }
    private SecretKey generateKey( byte[] salt) throws Exception
    {
    
        return new SecretKeySpec(raw, ALGO);
      
    }
       
    private byte[] encrypt( final byte[] message) throws Exception
    {
        Cipher cipher = Cipher.getInstance(ALGO);
    	SecretKey key = generateKey(null);
        // Perform encryption using the Cipher
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] encryptedMessage = cipher.doFinal(message);
        return encryptedMessage;
               
    }
    
    private byte[] decrypt(final byte[] encryptedMessage) throws Exception
    {
        Cipher cipher = Cipher.getInstance(ALGO);     
    	SecretKey key =generateKey(null);
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] decryptedMessage = cipher.doFinal(encryptedMessage);
        return decryptedMessage;
   
    }
    
      
    public static void main(String[] args) throws Exception
    {
        WTIV2CryptHelper aHelper = WTIV2CryptHelper.getInstance();
        aHelper.setCustomAESKeySeeds("");
        String data = "test data";
        String encData = aHelper.encrypt(data);
           
        System.out.println("encData: " + encData);
        
        String plainText = aHelper.decrypt(encData);
        
        System.out.println("plainText: " + plainText);
     
        aHelper.setCustomAESKeySeeds("a");
        encData = aHelper.encrypt(data);
        
        System.out.println("encData: " + encData);
        
        plainText = aHelper.decrypt(encData);
        
        System.out.println("plainText: " + plainText);
        
        
        aHelper.setCustomAESKeySeeds("aAAAAAAAAAAAAAAAAAAAAAa");
        encData = aHelper.encrypt(data);
        
        System.out.println("encData: " + encData);
        
        plainText = aHelper.decrypt(encData);
        
        System.out.println("plainText: " + plainText);
        
    }
    
}
