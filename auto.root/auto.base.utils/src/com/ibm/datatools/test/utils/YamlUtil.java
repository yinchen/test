package com.ibm.datatools.test.utils;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.List;

import org.ho.yaml.Yaml;


public class YamlUtil {

    private String yamlFilePath;
    private List<String> yamlFilesPath;
    private HashMap<String, HashMap<String, String>> elementsMap = new HashMap<String, HashMap<String, String>>();

    public YamlUtil(String yamlFilePath) {
        this.yamlFilePath = yamlFilePath;
        loadYaml();
    }
    
    public YamlUtil(List<String> yamlFilesPath) {
        this.yamlFilesPath = yamlFilesPath;
        loadYamls();
    }

    @SuppressWarnings("unchecked")
	private void loadYaml() {
        try {
            elementsMap = Yaml.loadType(new FileInputStream(yamlFilePath),HashMap.class);
        } catch (Exception e) {
            //e.printStackTrace();
        	TestLogger.logError("The exception occurs for the Locator Yaml file: "+yamlFilePath, e);
        }
    }
    
    @SuppressWarnings("unchecked")
	private void loadYamls() {
        try {
        	for(String s:yamlFilesPath)
        		elementsMap.putAll(Yaml.loadType(new FileInputStream(s),HashMap.class));
        } catch (Exception e) {
            //e.printStackTrace();
        	TestLogger.logError("The exception occurs for one of the Locator Yaml files!", e);
        }
    }

    public HashMap<String, HashMap<String, String>> getElementsMap() {
        return elementsMap;
    }

}
