package com.ibm.datatools.dsm.tests.ui.framework.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import com.ibm.datatools.test.utils.Environment;
import com.ibm.datatools.test.utils.Properties;
import com.ibm.datatools.test.utils.TestLogger;



public class Configuration {	
	
	
	private static final String adminDataPath = Properties.getConfigValue("admin.testdata.path");	
	private static final String log4jConfigFilePath =  Properties.getConfigValue("log4j.config");
	private static final String screenshotPath =  Properties.getConfigValue("screenshot.path");
	private static final String dsmVersionFilePath = Properties.getConfigValue("dsm.version.path");
	
	private static final String locatorYamlPropFilePath =  Properties.getConfigValue("yaml.locators.prop.path");	
	private static final String locatorYamlFilePath =  Properties.getConfigValue("yaml.locators.path");
	private static final String connectionsFilePath = Properties.getConfigValue("monitor.db.connections.path");
	
	public static String getAdminDataPath() {
		return Environment.getProjectRootPath() + adminDataPath;
	}	
	
	public static String getLog4jConfigFilePath() {
		return Environment.getProjectRootPath() + log4jConfigFilePath;
	}
	
	public static String getLocatorYamlPropertyFile() {
		return Environment.getProjectRootPath() + locatorYamlPropFilePath;
	}
	
	public static String getScreenshotPath() {
		return Environment.getProjectRootPath() + screenshotPath;
	}
	
	public static String getDSMVersionFilePath() {
		return dsmVersionFilePath;
	}
	
	public static String getLocatorYamlPath() {
		return Environment.getProjectRootPath() + locatorYamlFilePath;
	}
	
	public static String getConnectionsFilePath() {
		return Environment.getProjectRootPath() + connectionsFilePath;
	}
	
	public static List<String> getLocatorYamlFiles(){
		
		java.util.Properties p = new java.util.Properties();
		try {
			p.load(new FileInputStream(getLocatorYamlPropertyFile()));
		} catch (FileNotFoundException e) {
			//e.printStackTrace();
			TestLogger.logError("The Locator Yaml Property file is not found!!!", e);			
		} catch (IOException e) {
			//e.printStackTrace();
			TestLogger.logError("The IO exception occurs for the Locator Yaml Property file!!!", e);
		}
		
		List<String> yamlFiles = new ArrayList<String>();         
        Enumeration<Object> enu=p.elements();
        while(enu.hasMoreElements()){
            String key = (String)enu.nextElement();
            yamlFiles.add(getLocatorYamlPath()+key);
        } 
        
        return yamlFiles;
		
	}
	
	
	
	

	

	
	
}
