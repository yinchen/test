package com.ibm.datatools.dsm.tests.ui.framework.base;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.csvreader.CsvReader;
import com.ibm.datatools.test.utils.CryptUtils;


public class Connections {
	
	public static Map<String, String> getConnectionInfo(String dbProfile) { 
		
		Map<String, String> connection = new HashMap<String, String>();
		try {
			CsvReader reader = new CsvReader(new FileReader(Configuration.getConnectionsFilePath()), ',');
			reader.readHeaders();
			
			while(reader.readRecord())
			{
			  if(reader.get("name").equalsIgnoreCase(dbProfile)){
				  connection.put("databaseName", reader.get("databaseName"));
				  connection.put("host", reader.get("host"));
				  connection.put("port", reader.get("port"));
				  connection.put("user", reader.get("user"));
				  connection.put("password", CryptUtils.decryptString(reader.get("password")));
				  connection.put("dataServerType", reader.get("dataServerType"));
				  break;				  
			  }			 
			}			
			return connection;			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connection;
	}	

}
