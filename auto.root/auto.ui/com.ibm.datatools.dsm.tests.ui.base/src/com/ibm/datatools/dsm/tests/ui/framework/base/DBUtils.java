package com.ibm.datatools.dsm.tests.ui.framework.base;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ibm.datatools.test.utils.Properties;

public class DBUtils {
	
	private static final String DRIVER = "com.ibm.db2.jcc.DB2Driver";  
    private String URLSTR = null; 
    private String HOST = null;
    private String USERNAME = null;    
    private String USERPASSWORD = null;    
    private Connection connnection = null;  
    private PreparedStatement preparedStatement = null;  
    private CallableStatement callableStatement = null;     
    private ResultSet resultSet = null;  
    
    static {  
        try {  
            Class.forName(DRIVER);  
        } catch (ClassNotFoundException e) {  
            System.out.println("Load driver error!!!");  
            System.out.println(e.getMessage());  
        }  
    }
    
	public DBUtils(String hOST, int pORT, String dATABASEANME, String uSERNAME, String uSERPASSWORD) {
		super();
		HOST = hOST;
		USERNAME = uSERNAME;
		USERPASSWORD = uSERPASSWORD;
		URLSTR = "jdbc:db2://"+HOST+":"+String.valueOf(pORT)+"/"+dATABASEANME;
	}

	public DBUtils(){		
		this(Properties.getConfigValue("repoIP"),
			Integer.parseInt(Properties.getConfigValue("repoPort")),
			Properties.getConfigValue("repoDatabase"),
			Properties.getConfigValue("repoUsername"),
			Properties.getConfigValue("repoPassword"));
	}	
	
	
	public boolean testConnection(){
		if(this.getConnection() != null){
			try {
				this.closeAll();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}
		else
			return false;
		
	}
	
	public Connection getConnection() {  
        try { 
            connnection = DriverManager.getConnection(URLSTR, USERNAME,  
                    USERPASSWORD);  
        } catch (SQLException e) {  
            System.out.println(e.getMessage());  
        }  
        return connnection;  
    } 
	
    private void closeAll() throws SQLException {  
        if (resultSet != null) {  
            try {  
                resultSet.close();  
            } catch (SQLException e) {  
                //System.out.println(e.getMessage()); 
                throw new SQLException(e.getMessage());
            }  
        }   
       
        if (preparedStatement != null) {  
            try {  
                preparedStatement.close();  
            } catch (SQLException e) {  
                //System.out.println(e.getMessage()); 
                throw new SQLException(e.getMessage());
            }  
        }           
        
        if (callableStatement != null) {  
            try {  
                callableStatement.close();  
            } catch (SQLException e) {  
                //System.out.println(e.getMessage());  
                throw new SQLException(e.getMessage());
            }  
        }
       
        if (connnection != null) {  
            try {  
                connnection.close();  
            } catch (SQLException e) {  
                //System.out.println(e.getMessage()); 
                throw new SQLException(e.getMessage());
            }  
        }     
    }  
	
	
    public int executeUpdate(String sql, Object[] params) throws SQLException {  
        int affectedLine = 0;            
        try {           
            connnection = this.getConnection();         
            preparedStatement = connnection.prepareStatement(sql);  
              
            if (params != null) {  
                for (int i = 0; i < params.length; i++) {  
                    preparedStatement.setObject(i + 1, params[i]);  
                }  
            }
            affectedLine = preparedStatement.executeUpdate();  
  
        } catch (SQLException e) {  
            //System.out.println(e.getMessage()); 
            throw new SQLException(e.getMessage());
        } finally {  
            closeAll();  
        }  
        return affectedLine;  
    } 	
  
    private ResultSet executeQueryRS(String sql, Object[] params) throws SQLException {  
        try {  
            connnection = this.getConnection(); 
            preparedStatement = connnection.prepareStatement(sql);      
            
            if (params != null) {  
                for (int i = 0; i < params.length; i++) {  
                    preparedStatement.setObject(i + 1, params[i]);  
                } 
            }             
            resultSet = preparedStatement.executeQuery();  
        } catch (SQLException e) {  
            //System.out.println(e.getMessage()); 
            throw new SQLException(e.getMessage());
        }   
  
        return resultSet;  
    }  
       
    public Object executeQuerySingle(String sql, Object[] params) throws SQLException {  
        Object object = null;  
        try {           
            connnection = this.getConnection();           
            preparedStatement = connnection.prepareStatement(sql);             
            
            if (params != null) {  
                for (int i = 0; i < params.length; i++) {  
                    preparedStatement.setObject(i + 1, params[i]);  
                }  
            }            
            resultSet = preparedStatement.executeQuery();  
            if(resultSet.next()) {  
                object = resultSet.getObject(1);  
            }  
              
        } catch (SQLException e) {  
            //System.out.println(e.getMessage());  
            throw new SQLException(e.getMessage());
        } finally {  
            closeAll();  
        }  
  
        return object;  
    }  
  
   
    public List<Object> excuteQuery(String sql, Object[] params) throws SQLException {  
        ResultSet rs = executeQueryRS(sql, params);     
        ResultSetMetaData rsmd = null;  
          
        int columnCount = 0;  
        try {  
            rsmd = rs.getMetaData();  
            columnCount = rsmd.getColumnCount();  
        } catch (SQLException e1) {  
            System.out.println(e1.getMessage());  
        }  
 
        List<Object> list = new ArrayList<Object>();  
  
        try {  
            
            while (rs.next()) {  
                Map<String, Object> map = new HashMap<String, Object>();  
                for (int i = 1; i <= columnCount; i++) {  
                    map.put(rsmd.getColumnLabel(i), rs.getObject(i));  
                }  
                list.add(map);  
            }  
        } catch (SQLException e) {  
            //System.out.println(e.getMessage());  
            throw new SQLException(e.getMessage());
        } finally {  
            closeAll();  
        }  
        return list;  
    }  
      
   
    public Object excuteQuery(String sql, Object[] params,int outParamPos, int SqlType) throws SQLException {  
        Object object = null;  
        connnection = this.getConnection();  
        try {           
            callableStatement = connnection.prepareCall(sql);           
            
            if(params != null) {  
                for(int i = 0; i < params.length; i++) {  
                    callableStatement.setObject(i + 1, params[i]);  
                }  
            }              
           
            callableStatement.registerOutParameter(outParamPos, SqlType);       
            callableStatement.execute();   
            object = callableStatement.getObject(outParamPos);  
              
        } catch (SQLException e) {  
            //System.out.println(e.getMessage());
            throw new SQLException(e.getMessage());
        } finally {  
            closeAll();  
        }           
        return object;  
    }  
    
    private List<String> loadSQL(String sqlFile, String terminator) throws Exception {    
    	List<String> sqlList = new ArrayList<String>();
	    try {
	         @SuppressWarnings("resource")	
	         			 
	         InputStream sqlFileIn = new FileInputStream(sqlFile);
	 
	         StringBuffer sqlSb = new StringBuffer();
	         byte[] buff = new byte[1024];
	         int byteRead = 0;
	         
	         while ((byteRead = sqlFileIn.read(buff)) != -1) {
	             sqlSb.append(new String(buff, 0, byteRead));
	         }
	         sqlSb.append("\n");
	    	 
	    	 String[] sqlArr = sqlSb.toString().split("("+terminator+"\r\n)|("+terminator+"\n)");
	    	 	    	 
	    	 for (int i = 0; i < sqlArr.length; i++) {
		    	 String sql = sqlArr[i].replaceAll("--.*", "").trim();
		    	 if (!sql.equals("")) {
		    		 sqlList.add(sql);
		    	 }
	    	  }	 
	    	 
	    	 return sqlList;
	    	} catch (Exception ex) {
	    	   throw new Exception(ex.getMessage());
	    	}
    }
    
    //Only for update, insert sql 
    public int[] executeBatchFile(String sqlFile) throws Exception {
		int[] affectedLine = null;
		Statement statement = null;
		List<String> sqlList = loadSQL(sqlFile, ";");
		try {
			connnection = this.getConnection();
			if (statement == null) {
				statement = connnection.createStatement();
			}
			connnection.setAutoCommit(false);
			for (String sql : sqlList) {
 	    	   statement.addBatch(sql);
 	       	} 
			affectedLine = statement.executeBatch();
			System.out.println("Row count:" + Arrays.toString(affectedLine));
			connnection.setAutoCommit(true);

		} catch (Exception ex) {
			connnection.rollback();
            throw ex;
		} finally {
			closeAll();
		}
		return affectedLine;
	}
    
    public int[] executeBatch(String sql) throws Exception{
    	
	  int[] affectedLine = null;
	  Statement statement = null;
	  try {
		    connnection = this.getConnection();	
			if (statement == null) {
				statement = connnection.createStatement();
			}
			String[] batchSQLs = sql.trim().split(";");
			connnection.setAutoCommit(false);
			for (int i = 0; i < batchSQLs.length; i++) {
				statement.addBatch(batchSQLs[i]);
			}
			affectedLine = statement.executeBatch();
			connnection.setAutoCommit(true);

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			connnection.rollback();
            throw e;
            
		} finally {
			closeAll();
		}
		return affectedLine;

	}
    
    
    public void execute(String sqlFile, String terminator) throws Exception {    	 
    	
    	 Statement statement = null;
    	 List<String> sqlList = loadSQL(sqlFile, terminator);
    	 String sql = null;
    	 try {
    		   connnection = this.getConnection();
    		   if (statement == null) {
   				statement = connnection.createStatement();
   				}
    		   connnection.setAutoCommit(false);
    	       for (int i=0; i<sqlList.size();i++) {
    	    	   sql =  sqlList.get(i);
    	    	   statement.execute(sql);
    	       }               
    	       connnection.commit();    	            
    	      } catch (Exception ex) {
		    	     System.out.println("Failed to exectue the SQL script: \""+sql +"\"");
		    	  	 connnection.rollback();
		             throw ex;
	          } finally {
	              closeAll();
	          }
    }
	
	public static void main(String[] args) {
		
		DBUtils a = new DBUtils();
		try {
			//a.excuteQuery("SELECT 1 FROM IBMPDQ.MANAGED_DATABASE WHERE(1=-1)", null);
			a.execute("C:\\validate.sql", "#");
			//a.executeBatch("C:\\validate.sql");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
