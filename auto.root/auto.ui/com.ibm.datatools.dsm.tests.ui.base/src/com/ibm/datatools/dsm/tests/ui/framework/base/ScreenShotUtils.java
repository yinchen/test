package com.ibm.datatools.dsm.tests.ui.framework.base;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.testng.Assert;

import com.ibm.datatools.test.utils.TestLogger;


public class ScreenShotUtils {
	
	public static String getDSMVersion(){
		
		String versionFilePath = Configuration.getDSMVersionFilePath();
		String dsmVersion = null;
		
		File dsmVersionFile = new File(versionFilePath);
		if(!dsmVersionFile.exists()){
			String message = "DSM version file not exists in current path : " + versionFilePath + ".";
			TestLogger.logError(message);
			Assert.fail(message);
		}
		try {
			FileReader reader = new FileReader(dsmVersionFile);
			BufferedReader bf = new BufferedReader(reader);
			dsmVersion = bf.readLine();
			bf.close();
		} catch (Exception e) {
			TestLogger.logError("Failed to get dsm version in current path : "+ versionFilePath + ".");
			e.printStackTrace();
		}
			return dsmVersion;
	}	
	
	
	public static String createScreenshotName(String pageName){
		
		String dsmVersion = getDSMVersion();
		String screenshotName = "DSM_" + dsmVersion +"_"+ pageName.replaceAll("->", "_");
		return screenshotName;
	}
	
	
}







