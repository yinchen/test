package com.ibm.datatools.dsm.tests.ui.framework.core;


import com.ibm.datatools.dsm.tests.ui.framework.widget.WButton;
import com.ibm.datatools.dsm.tests.ui.framework.widget.WLink;
import com.ibm.datatools.dsm.tests.ui.framework.widget.WMenuItem;
import com.ibm.datatools.dsm.tests.ui.framework.widget.WTimeSlider;
import com.ibm.datatools.dsm.tests.ui.framework.widget.WebWidget;
import com.ibm.datatools.test.utils.Properties;
import com.ibm.datatools.test.utils.TestLogger;

public class Action {
	
	private static int elmentlayer = Integer.parseInt(Properties.getConfigValue("element.layer"));
	//private static int pauseTime = Integer.parseInt(Properties.getConfigValue("mte.pauseTime.second"));
	
	private ObjectBase objBase;
	
	public Action(ObjectBase objBase) {
		this.objBase = objBase;
	}
	
	//For Link 
	public void clickForLink(int i, String parent, String sLinkName) {
		try {
			//Click button
			WLink link = this.objBase.getLink(parent, sLinkName);
			link.waitForClickable(i);
			link.click(i);
			
			TestLogger.logInfo("Click \"" + sLinkName + "["+ i +"]" + "\" link ");
		} catch (Exception error) {
			TestLogger.logError("FAIL - Can not find link \"" + sLinkName
					+ "["+ i +"]" + "\" in current page ");
			System.out.println(error);
			TestLogger.logError("Please refer to the screenshot for more informaton: "+objBase.getDriverCore().saveScreenshot());
		}
	}	
	
	public void clickForLink(String parent, String sLinkName) {
		this.clickForLink(this.objBase.getLink(parent, sLinkName).getCurrentIndex(), parent, sLinkName);
	}
	
	
	//For TextBox
	public void setForTextBox(int i, String parent, String sTextBoxName, String sInputText) {
		try {			
			//setText
			objBase.getTextBox(parent, sTextBoxName).setText(i, sInputText);
			
			TestLogger.logInfo("Enter text \"" + sInputText
					+ "\" in TextBox \"" + sTextBoxName + "["+ i +"]" + "\" ");
		} catch (Exception error) {
			TestLogger.logError("FAIL - Can not find input TextBox \""
					+ sTextBoxName + "["+ i +"]" + "\" in current page ");
			System.out.println(error);
			TestLogger.logError("Please refer to the screenshot for more informaton: "+objBase.getDriverCore().saveScreenshot());
		}
	}
	
	public void setForTextBox(String parent, String sTextBoxName, String sInputText) {
		this.setForTextBox(objBase.getTextBox(parent, sTextBoxName).getCurrentIndex(), parent, sTextBoxName, sInputText);
	}
	
	//For TextBox
	public void clickForTextBox(int i, String parent, String sTextBoxName) {
		try {			
			//setText
			objBase.getTextBox(parent, sTextBoxName).click(i);
			
			TestLogger.logInfo("Click \"" + sTextBoxName + "["+ i +"]" + "\" TextBox ");
		} catch (Exception error) {
			TestLogger.logError("FAIL - Can not find TextBox \""
					+ sTextBoxName + "["+ i +"]" + "\" in current page ");
			System.out.println(error);
			TestLogger.logError("Please refer to the screenshot for more informaton: "+objBase.getDriverCore().saveScreenshot());
		}
	}
	
	public void clickForTextBox(String parent, String sTextBoxName) {
		this.clickForTextBox(objBase.getTextBox(parent, sTextBoxName).getCurrentIndex(), parent,  sTextBoxName);
	}
	
	//For Button
	public void clickForButton(int i, String parent, String sButtonName) {
		try {
			//Click button
			WButton btn = this.objBase.getButton(parent, sButtonName);
			//btn.waitForVisible(i);
			btn.waitForClickable(i);			
			btn.click(i);
			
			TestLogger.logInfo("Click \"" + sButtonName + "["+ i +"]" + "\" button ");
		} catch (Exception error) {
			TestLogger.logError("FAIL - Can not find Button \"" + sButtonName
					+ "["+ i +"]" + "\" in current page ");
			System.out.println(error);
			TestLogger.logError("Please refer to the screenshot for more informaton: "+objBase.getDriverCore().saveScreenshot());
		}
	}
	
	public void clickForButton(String parent, String sButtonName) {
		this.clickForButton(objBase.getButton(parent, sButtonName).getCurrentIndex(), parent, sButtonName);
	}	
	
	
	//For Button
	public boolean isBtnEnabled(int i, String parent, String sButtonName){
		boolean flag = this.objBase.getButton(parent, sButtonName).isEnabled(i);
		if(flag)
			TestLogger.logInfo("\"" + sButtonName + "\" is enabled. ");
		else
			TestLogger.logInfo("\"" + sButtonName + "\" is not enabled. ");
		return flag;
	}
	
	public boolean isBtnEnabled(String parent, String sButtonName){
		return this.isBtnEnabled(objBase.getButton(parent, sButtonName).getCurrentIndex(), parent, sButtonName);
	}
	
	
	
	
	
	//For MenuItem
	public void clickForMenuItem(int i, String parent, String sMenuItemName) {
		try {
			//Select menu
			WMenuItem menuItem = this.objBase.getMenuItem(parent, sMenuItemName);			
			menuItem.waitForVisible(i);
			menuItem.select(i);

			TestLogger.logInfo("Click \"" + sMenuItemName + "["+ i +"]" + "\" menu item ");
		} catch (Exception error) {
			
			TestLogger.logError("FAIL - Can not find menu item \"" + sMenuItemName
					+ "["+ i +"]" + "\" in current page ");
			System.out.println(error);
			TestLogger.logError("Please refer to the screenshot for more informaton: "+objBase.getDriverCore().saveScreenshot());
		}
	}
	
	public void clickForMenuItem(String parent, String sMenuItemName) {
		this.clickForMenuItem(objBase.getMenuItem(parent, sMenuItemName).getCurrentIndex(), parent, sMenuItemName);
	}
	
	
	//For List	(No parameter int i)
	public void selectForList(String parent, String sListItemName, String selectedName) {
		try {
			//Select the specific item from List
			this.objBase.getList(parent, sListItemName).select(selectedName);
			
			TestLogger.logInfo("Select \"" + selectedName + "\" from List " + "\"" + sListItemName +"\"");
		} catch (Exception error) {
			TestLogger.logError("FAIL - Can not find list item \"" + sListItemName
					+ "\" in current page ");
			System.out.println(error);
			TestLogger.logError("Please refer to the screenshot for more informaton: "+objBase.getDriverCore().saveScreenshot());
		}
	}
	

	
    //For WebWidget
	public boolean isDisplayed(int i, String parent, String sElementName){
		try {
						
			WebWidget widget = this.objBase.getWidget(parent, sElementName);			
			//widget.waitForVisible(i);
			//WebDriverCore.pause(pauseTime);	
			
			boolean flag = widget.isDisplayed(i);
			if(flag)
				TestLogger.logInfo("\"" + sElementName + "\" is displayed. ");
			else
				TestLogger.logInfo("\"" + sElementName + "\" is not displayed. ");
				
			return flag;							
		}catch (Exception error) {
			TestLogger.logInfo("\"" + sElementName + "\" is not displayed. ");
			System.out.println(error);
			TestLogger.logError("Please refer to the screenshot for more informaton: "+objBase.getDriverCore().saveScreenshot());
			return false;
		}
		
	}
	
	public boolean isDisplayed(String parent, String sElementName){
		return this.isDisplayed(objBase.getWidget(parent, sElementName).getCurrentIndex(), parent, sElementName);
	}
	

	
	//For WebWidget	(No parameter int i)
	public int getSize(String parent, String sElementName){
		int size = 0;
		try {						
			WebWidget widget = this.objBase.getWidget(parent, sElementName);
			size = widget.getSize();
		
			TestLogger.logInfo("Get size for the web element " + "\"" + sElementName +"\"");
			return size;
		} catch (Exception error) {
			TestLogger.logError("FAIL - Can not find web element \"" + sElementName
					+ "\" in current page ");
			System.out.println(error);
			TestLogger.logError("Please refer to the screenshot for more informaton: "+objBase.getDriverCore().saveScreenshot());
			return size;
		}		
	}

	
	
	//For WebWidget (No parameter int i)
	public void scrolledIntoView(String parent, String sElementName){		
		try {						
			WebWidget widget = this.objBase.getWidget(parent, sElementName);
			widget.scrolledIntoView(elmentlayer);
		
			TestLogger.logInfo("Scroll to the web element " + "\"" + sElementName +"\"");
		} catch (Exception error) {
			TestLogger.logError("FAIL - Can not find web element \"" + sElementName
					+ "\" in current page ");
			System.out.println(error);
			TestLogger.logError("Please refer to the screenshot for more informaton: "+objBase.getDriverCore().saveScreenshot());
		}		
	}
	

	//For WebWidget
	public void contextClick(int i, String parent, String sElementName){
		try {						
			WebWidget widget = this.objBase.getWidget(parent, sElementName);
			widget.contextClick(i);
		
			TestLogger.logInfo("Context click the web element " + "\"" + sElementName +"\"");
		} catch (Exception error) {
			TestLogger.logError("FAIL - Can not find web element \"" + sElementName
					+ "\" in current page ");
			System.out.println(error);
			TestLogger.logError("Please refer to the screenshot for more informaton: "+objBase.getDriverCore().saveScreenshot());
		}		
	}	
	
	public void contextClick(String parent, String sElementName){
		this.contextClick(objBase.getWidget(parent, sElementName).getCurrentIndex(), parent, sElementName);
	}
	
	
	//For WebWidget
	public void moveToElement(int i, String parent, String sElementName){		
		try {						
			WebWidget widget = this.objBase.getWidget(parent, sElementName);
			widget.moveToElement(i);
		
			TestLogger.logInfo("Move the mouse to the web element " + "\"" + sElementName +"\"");
		} catch (Exception error) {
			TestLogger.logError("FAIL - Can not find web element \"" + sElementName
					+ "\" in current page ");
			System.out.println(error);
			TestLogger.logError("Please refer to the screenshot for more informaton: "+objBase.getDriverCore().saveScreenshot());
		}		
	}
	
	public void moveToElement(String parent, String sElementName){
		this.moveToElement(objBase.getWidget(parent, sElementName).getCurrentIndex(), parent, sElementName);
	}
			
	
	//For Dialog
	public String getDialogTitle(int i, String parent, String sDialogName){
		String title = this.objBase.getDialog(parent, sDialogName).getTitle(i);
		TestLogger.logInfo("The dialog title is \"" + title + "\".");
		return title;
	}
	
	public String getDialogTitle(String parent, String sDialogName){
		return this.getDialogTitle(this.objBase.getDialog(parent, sDialogName).getCurrentIndex(), sDialogName, sDialogName);
	}
	
	
	//For Static Text
	public String getStaticText(int i, String parent, String sStaticTextName){
		String text ="";
		try{
		 text = this.objBase.getStaticText(parent, sStaticTextName).getText(i);
		TestLogger.logInfo("The static text is \"" + text + "\".");
		}catch(Exception e){
			e.printStackTrace();
			TestLogger.logError("Action getStaticText Exception :"+e.getMessage());
		}
		return text;
	}
	
	public String getStaticText(String parent, String sStaticTextName){
		return this.getStaticText(objBase.getStaticText(parent, sStaticTextName).getCurrentIndex(), parent, sStaticTextName);
	}	
	
	
	//For Radio Button
	public void selectRadioButton(int i, String parent, String sRadioButtonName){
		try {
			//Select the specific item from List
			this.objBase.getRadio(parent, sRadioButtonName).select(i);
			
			TestLogger.logInfo("Select \"" + sRadioButtonName + "\"");
		} catch (Exception error) {
			TestLogger.logError("FAIL - Can not find radio button \"" + sRadioButtonName
					+ "\" in current page ");
			System.out.println(error);
			TestLogger.logError("Please refer to the screenshot for more informaton: "+objBase.getDriverCore().saveScreenshot());
		}
		
	}
	
	public void selectRadioButton(String parent, String sRadioButtonName){
		this.selectRadioButton(objBase.getRadio(parent, sRadioButtonName).getCurrentIndex(), parent, sRadioButtonName);
	}
	
	
	
	//For CheckBox 
	public void checkCheckBox(int i, String parent, String sCheckBoxName){
		try {
			//Select the specific item from List
			this.objBase.getCheckBox(parent, sCheckBoxName).check(i);
			
			TestLogger.logInfo("check \"" + sCheckBoxName + "\"");
		} catch (Exception error) {
			TestLogger.logError("FAIL - Can not find checkbox \"" + sCheckBoxName
					+ "\" in current page ");
			System.out.println(error);
			TestLogger.logError("Please refer to the screenshot for more informaton: "+objBase.getDriverCore().saveScreenshot());
		}
		
	}
	
	public void checkCheckBox(String parent, String sCheckBoxName){
		this.checkCheckBox(objBase.getCheckBox(parent, sCheckBoxName).getCurrentIndex(), parent, sCheckBoxName);
	}
	
	//Drag time slider
	
	public void dragTimeSlider(int i, String parent, String sSliderName,int movement) {
		try {
			//Drag time slider
			WTimeSlider slider = this.objBase.getTimeSlider(parent, sSliderName);
			slider.drag(i, movement);
			
			TestLogger.logInfo("Click \"" + sSliderName + "["+ i +"]" + "\" button ");
		} catch (Exception error) {
			TestLogger.logError("FAIL - Can not find Slider \"" + sSliderName
					+ "["+ i +"]" + "\" in current page ");
			System.out.println(error);
			TestLogger.logError("Please refer to the screenshot for more informaton: "+objBase.getDriverCore().saveScreenshot());
		}
	}
	
	
	public void dragTimeSlider(String parent, String sButtonName,int movement) {
		this.dragTimeSlider(objBase.getButton(parent, sButtonName).getCurrentIndex(), parent, sButtonName,movement);
	}
	
		public void selectOneCheckBox(int i, String parent, String sCheckBoxName){
			try {
				//Select the specific item from List
				this.objBase.getCheckBox(parent, sCheckBoxName).check(i);
				
				TestLogger.logInfo("check \"" + sCheckBoxName + "\"");
			} catch (Exception error) {
				TestLogger.logError("FAIL - Can not find checkbox \"" + sCheckBoxName
						+ "\" in current page ");
				System.out.println(error);
				TestLogger.logError("Please refer to the screenshot for more informaton: "+objBase.getDriverCore().saveScreenshot());
			}
		}
		
		public void selectOneCheckBox(String parent, String sCheckBoxName,int i){
			this.selectOneCheckBox(objBase.getCheckBox(parent, sCheckBoxName).getCurrentIndex()+i, parent, sCheckBoxName);
		}

}
