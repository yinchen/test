package com.ibm.datatools.dsm.tests.ui.framework.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;

import com.ibm.datatools.test.utils.Properties;
import com.ibm.datatools.test.utils.YamlUtil;



public class Locator {
	
    private String split = Properties.getConfigValue("split.character");	
	private HashMap<String, HashMap<String, String>> elementsMap;

    private final String type = "type";
    private final String value = "value";

    public Locator(String locatorFile) {
        setLocatorsMapFromFile(locatorFile);
    }
    
    public Locator(List<String> locatorFiles) {
        setLocatorsMapFromFile(locatorFiles);
    }

    private void setLocatorsMapFromFile(String locatorFile) {
        YamlUtil util = new YamlUtil(locatorFile);
        elementsMap = util.getElementsMap();
    }
    
    private void setLocatorsMapFromFile(List<String> locatorFiles) {
        YamlUtil util = new YamlUtil(locatorFiles);
        elementsMap = util.getElementsMap();
    }

    public By getLocator(String key, ArrayList<String> locatorStringReplacement) {

        By locator = null;
        if (elementsMap == null) {
            return null;
        }

        if (elementsMap.containsKey(key)) {
            String locatorString = elementsMap.get(key).get(value);
            if (locatorStringReplacement != null
                    && locatorStringReplacement.size() != 0) {
                for (String replaceItem : locatorStringReplacement) {
                    locatorString = locatorString.replaceFirst("%s", replaceItem);
                }
            }
            locator = getLocatorByType(elementsMap.get(key).get(type), locatorString);

        } else {
            return null;
        }
        return locator;
    }

    private By getLocatorByType(String type, String locatorString) {

        By locator = null;

        switch (type) {
            case "id":
                locator = By.id(locatorString);
                break;
            case "name":
                locator = By.name(locatorString);
                break;
            case "xpath":
                locator = By.xpath(locatorString);
                break;
            case "linkText":
                locator = By.linkText(locatorString);
                break;
            case "partialLinkText":
                locator = By.partialLinkText(locatorString);
                break;
            case "className":
                locator = By.className(locatorString);
                break;
            case "tagName":
                locator = By.tagName(locatorString);
                break;
            case "cssSelector":
                locator = By.cssSelector(locatorString);
                break;
            default:
                locator = null;
                break;
        }
        return locator;
    }

    public By getLocator(String key) {
        
    	String [] result = key.split(split);
    	int size = result.length;
    	
    	if(size == 1)
    		return getLocator(key, null);
    	else{
    		ArrayList<String> list = new ArrayList<String>();
    		key = result[0];
    		for(int i=1; i<size; i++){
    			list.add(result[i]);
    		}
    		return getLocator(key, list);	    	
    	}
    	
    }

    public String getLocatorType(String key) {
        if (elementsMap == null)
            return null;
        if (elementsMap.containsKey(key)) {
            return elementsMap.get(key).get(type);
        } else {
            return null;
        }
    }

    public String getLocatorValue(String key) {
        System.out.println(elementsMap.size());
        if (elementsMap == null)
            return null;
        if (elementsMap.containsKey(key)) {
            return elementsMap.get(key).get(value);
        } else {
            return null;
        }
    }

}
