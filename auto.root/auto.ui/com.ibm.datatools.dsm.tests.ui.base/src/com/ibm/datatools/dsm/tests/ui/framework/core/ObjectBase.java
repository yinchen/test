package com.ibm.datatools.dsm.tests.ui.framework.core;


import com.ibm.datatools.dsm.tests.ui.framework.widget.WButton;
import com.ibm.datatools.dsm.tests.ui.framework.widget.WCheckBox;
import com.ibm.datatools.dsm.tests.ui.framework.widget.WDialog;
import com.ibm.datatools.dsm.tests.ui.framework.widget.WLink;
import com.ibm.datatools.dsm.tests.ui.framework.widget.WList;
import com.ibm.datatools.dsm.tests.ui.framework.widget.WMenuItem;
import com.ibm.datatools.dsm.tests.ui.framework.widget.WRadioButton;
import com.ibm.datatools.dsm.tests.ui.framework.widget.WStaticText;
import com.ibm.datatools.dsm.tests.ui.framework.widget.WTextBox;
import com.ibm.datatools.dsm.tests.ui.framework.widget.WTimeSlider;
import com.ibm.datatools.dsm.tests.ui.framework.widget.WebWidget;

public class ObjectBase {

	protected static WebDriverCore driverCore;	

	public ObjectBase(WebDriverCore driverCore) {		
		if(ObjectBase.driverCore == null)
			ObjectBase.driverCore = driverCore;		
	}

	public WebDriverCore getDriverCore() {
		return driverCore;
	}

	public void setDriverCore(WebDriverCore driverCore) {
		ObjectBase.driverCore = driverCore;
	}
	
	//WebWidget
	public WebWidget getWidget(String parent, String children){
		return new WebWidget(ObjectBase.driverCore, parent, children);
	}
	
	//Button
	public WButton getButton(String parent, String children){
		return new WButton(ObjectBase.driverCore, parent, children);
	}

	//TextBox
	public WTextBox getTextBox(String parent, String children){
		return new WTextBox(ObjectBase.driverCore, parent, children);
	}
	
	//WMenu	
	public WMenuItem getMenuItem(String parent, String children){
		return new WMenuItem(ObjectBase.driverCore, parent, children);
	}
	
	//WStaticText
	public WStaticText getStaticText(String parent, String children){
		return new WStaticText(ObjectBase.driverCore, parent, children);
	}
	
	//WList
	public WList getList(String parent, String children){
		return new WList(ObjectBase.driverCore, parent, children);
	}
	
	//WDialog
	public WDialog getDialog(String parent, String children){
		return new WDialog(ObjectBase.driverCore, parent, children);
	}
	
	//WRadio
	public WRadioButton getRadio(String parent, String children){
		return new WRadioButton(ObjectBase.driverCore, parent, children);
	}
	
	//WLink
	public WLink getLink(String parent, String children){
		return new WLink(ObjectBase.driverCore, parent, children);
	}
	
	//CheckBox
	public WCheckBox getCheckBox(String parent, String children){
		return new WCheckBox(ObjectBase.driverCore, parent, children);
	}
	
	//TimeSlider
	public WTimeSlider getTimeSlider(String parent,String children){
		return new WTimeSlider(ObjectBase.driverCore, parent, children);
	}
	
}
