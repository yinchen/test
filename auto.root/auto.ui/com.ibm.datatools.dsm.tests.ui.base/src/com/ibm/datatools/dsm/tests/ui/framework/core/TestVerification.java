package com.ibm.datatools.dsm.tests.ui.framework.core;

import org.testng.Assert;

import com.ibm.datatools.test.utils.TestLogger;

public class TestVerification {
	
    private static WebDriverCore driverCore;     
	
	public TestVerification(WebDriverCore driverCore) {
		TestVerification.driverCore = driverCore;
	}

	public void assertTrue(boolean condition, String msg) {
		try {
			Assert.assertTrue(condition, msg);			
			TestLogger.logSuccess("[VP Pass] : " + msg);
		} catch (Throwable e) {			
			TestLogger.logError("[VP Fail] : " + msg);
			//It is added for review failed test case information in Jenkins
			Assert.fail("[VP Fail] : " + msg +" Please refer to the screenshot for more informaton: " +driverCore.saveScreenshot());
			throw e;			
		}
	}
	
	public void assertFalse(boolean condition, String msg) {
		try {
			Assert.assertFalse(condition, msg);
			TestLogger.logSuccess("[VP Pass] : " + msg);
		} catch (Throwable e) {
			TestLogger.logError("[VP Fail] : " + msg);
			Assert.fail("[VP Fail] : " + msg +" Please refer to the screenshot for more informaton: " +driverCore.saveScreenshot());
			throw e;
		}
	}
	
	// Verify if two String is equal
	public void assertStringEquals(String actual, String expected, String msg) {
		try {
			Assert.assertEquals(actual, expected);
			TestLogger.logSuccess("[VP Pass] : " + msg + " [ Expected: "
					+ expected + ", Actual: " + actual + "]");
		} catch (Throwable e) {
			if (actual == null)
				actual = "NULL";
			if (expected == null)
				expected = "NULL";
			TestLogger.logError("[VP Fail] : " + msg + " [Expected: "
					+ expected + ", Actual: " + actual + "]");
			Assert.fail("[VP Fail] : " + msg +" Please refer to the screenshot for more informaton: " +driverCore.saveScreenshot());	
			throw e;
		}
	}
	
	public void assertEquals(Object actual, Object expected, String msg) {
		try {
			
			Assert.assertEquals(actual, expected, msg);
			TestLogger.logSuccess("[VP Pass] : " + msg);
		} catch (Throwable e) {
			TestLogger.logError("[VP Fail] : " + msg);
			Assert.fail("[VP Fail] : " + msg +" Please refer to the screenshot for more informaton: " +driverCore.saveScreenshot());	
			throw e;
		}
	}	
	
	public void assertContained(String actual, String expected, String msg) {
		try {
			boolean bAsExpected;			
			if(expected.indexOf(actual)!= -1)
				bAsExpected = true;
			else			
				bAsExpected = false;
						
			Assert.assertTrue(bAsExpected, msg);
			TestLogger.logSuccess("[VP Pass] : " + msg);
		} catch (Throwable e) {
			TestLogger.logError("[VP Fail] : " + msg);
			Assert.fail("[VP Fail] : " + msg +" Please refer to the screenshot for more informaton: " +driverCore.saveScreenshot());	
			throw e;
		}
	}	

}
