package com.ibm.datatools.dsm.tests.ui.framework.core;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ibm.datatools.dsm.tests.ui.framework.base.Configuration;
import com.ibm.datatools.dsm.tests.ui.framework.base.ScreenShotUtils;
import com.ibm.datatools.test.utils.Properties;
import com.ibm.datatools.test.utils.TestLogger;



public class WebDriverCore {

	//related to selenium 
	private static WebDriver driver = null;	
	private Actions seleniumactions = null;
	private WebDriverWait wait = null; 	
	
	private static int pauseTime = Integer.parseInt(Properties.getConfigValue("mte.pauseTime.ms"));
	private static int timeOut = Integer.parseInt(Properties.getConfigValue("mte.timeOut.second"));
	private static int loadTime = Integer.parseInt(Properties.getConfigValue("mte.loadTime.second"));
	private boolean screenshot = Properties.getConfigValue("screenshot").equalsIgnoreCase("yes") ? true : false;
	
	private String httpserver = Properties.getConfigValue("httpserver");
	private String httpport = Properties.getConfigValue("httpport");
	private String screenshotpath = Properties.getConfigValue("screenshot.path");
	
	

	public WebDriverCore(WebDriver driver) {
		WebDriverCore.driver = driver;		
		this.seleniumactions = new Actions(driver);
		this.wait = new WebDriverWait(driver, timeOut);
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		WebDriverCore.driver = driver;		
	}	

	public WebDriverWait getWait() {
		return wait;
	}

	public void setWait(WebDriverWait wait) {
		this.wait = wait;
	}
	

	public Actions getSeleniumactions() {
		return seleniumactions;
	}

	

	public static void pause(long ms) {
		try {
			//Thread.sleep(second * 1000);
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			TestLogger.logDebug("pause error:", e);
		}
	}
	
	public static void pause() {
		pause(pauseTime);

	}
	
	
	public long getTimeOut(String sTimeout){
		long timeout = 0;
		if(sTimeout == null || sTimeout.trim().isEmpty())
			timeout = timeOut;
		else{
			try{
				timeout = Integer.parseInt(sTimeout);
			}catch(NumberFormatException e){
				TestLogger.logDebug("error for parseInt: "+sTimeout, e);
			}			
		}
		return timeout;
	}
	
	//Find one or more elements	
	public List<WebElement> findElements(Object obj, By by, String sTimeout){
		//transfer the sTimeout to long type
		long timeout = this.getTimeOut(sTimeout);
		//Start to findElements		
		boolean isSucceed = false;
		List<WebElement> elements = null;
		long timeBegins = System.currentTimeMillis();
		int findType = 0;
		do {
			if(obj instanceof WebDriver){
				findType = 1;
				elements = ((WebDriver)obj).findElements(by);
			}				
			else if(obj instanceof WebElement){
				findType = 2;
				elements = ((WebElement)obj).findElements(by);
			}
			else
				TestLogger.logDebug(obj+" is not WebDriver or WebElement type!!!");
			
			if(elements.size() >= 1){
				if(findType == 1)
					TestLogger.logDebug("Find "+elements.size()+" WebElement for the "+by+" from root node");
				if(findType == 2)
					TestLogger.logDebug("Find "+elements.size()+" WebElement for the "+by+" from parent node: " + obj);
				isSucceed = true;
				break;
			}		
			pause(pauseTime);
		} while (System.currentTimeMillis() - timeBegins <= timeout * 1000);

		if (!isSucceed) {
			if(findType == 1)
				TestLogger.logDebug("Failed to findElement: " + by +" from root node");
			if(findType == 2)
				TestLogger.logDebug("Failed to findElement: " + by +" from parent node: " + obj);
		}
		return elements;	
	}
	
	//For one or more elements from root
	public List<WebElement> findElements(By by, String sTimeout) {		
		return this.findElements(WebDriverCore.driver, by, sTimeout);
	}		
	
	
	
	//find the specific element
	public WebElement findElement(int i, Object obj, By by, String sTimeout){
		return this.findElements(obj, by, sTimeout).get(i);
	}	

	//For specific element from root. If sTimeout is null or empty, we will read timeout from config file
	public WebElement findElement(int i, By by, String sTimeout) {
		return this.findElement(i, WebDriverCore.driver, by, sTimeout);
	}	

	
	public boolean isTextContentsDisplayed(String contents, long timeout) {

		boolean isDisplayed = false;
		long timeBegins = System.currentTimeMillis();
		do {
			try {
				if (driver.getPageSource().contains(contents)) {
					isDisplayed = true;
					break;
				}
			} catch (Exception e) {
				TestLogger.logDebug("isTextContentsDisplayed error :", e);
			}
			pause(pauseTime);
		} while (System.currentTimeMillis() - timeBegins <= timeout * 1000);
		return isDisplayed;
	}	
	
	public boolean isTextContentsDisplayed(String contents) {
		return isTextContentsDisplayed(contents, timeOut);
	}
	
	public void moveToElement(WebElement element)
	{
		seleniumactions.moveToElement(element);
	}
	
	public void contextClick(WebElement element)
	{
		seleniumactions.contextClick(element);
	}

	public String saveScreenshot() {
		
		String snapUrl = null;
		if (screenshot) {
			File screenShotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			String snapName = screenShotFile.getName();
			String fileName = Configuration.getScreenshotPath() + snapName;
			snapUrl = httpserver + ":" + httpport + screenshotpath + snapName;

			try {
				FileUtils.copyFile(screenShotFile, new File(fileName));
			} catch (IOException e) {
				TestLogger.logDebug("Failed to save screenshot into file " + fileName + ".");
				e.printStackTrace();
			}
		}
		return snapUrl;
	}
	
	
	public static String screenShot(String screenshotName){
		
		String snapUrl = null;
		String versionFolderName = ScreenShotUtils.getDSMVersion();
		
		try {  
            int width = (int)Toolkit.getDefaultToolkit().getScreenSize().getWidth(); 
            int height = (int)Toolkit.getDefaultToolkit().getScreenSize().getHeight(); 
            Robot robot = new Robot();  
            BufferedImage image = robot.createScreenCapture(new Rectangle(width,height));  
            image = image.getSubimage(0, 0, width, height);
            
            String versionFolderPath = Configuration.getScreenshotPath() +""+versionFolderName+"/";
            File f = new File(versionFolderPath);
            f.mkdirs();
            
            String screenshotFiles = versionFolderPath + screenshotName;
            
            ImageIO.write (image, "png" , new File(""+screenshotFiles+".png"));   
               
        } catch (AWTException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        } 
		
		return snapUrl;
	}
	

	public void open(String url) {
		driver.manage().window().maximize();
		driver.get(url);
	}

	public void waitForPageLoad(long loadtime) {		
		
		String webReadyState = "";
		long timeBegins = System.currentTimeMillis();
		do {			
			webReadyState = (String) ((JavascriptExecutor) driver).executeScript("return document.readyState");
			if(webReadyState.equals("complete"))
				break;
			pause(pauseTime);
		} while (System.currentTimeMillis() - timeBegins <= loadtime * 1000);

		if (!webReadyState.equals("complete")) {
			//Assert.fail("Application error: page is not loaded completed after timeout " + timeout + " seconds");
			TestLogger.logDebug("Application error: page is not loaded completed after loadtime " + loadtime + " seconds");
		}	
	}
	
	public void waitForPageLoad() {			
		// this.waitForPageLoad(loadTime);
	}
	
	public Object executeScript(String arg0, Object... arg1){
		return ((JavascriptExecutor) driver).executeScript(arg0, arg1);
	}

	public void closeAlert() {
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}
	

	public boolean enabledElement(WebElement element) {
		if (element.isEnabled())
			return true;
		return false;
	}

	public boolean disabledElement(WebElement element) {
		String aria = element.getAttribute("aria-disabled");
		if (aria.equals("true"))
			return true;
		return false;
	}
	
	public String getWindowHandle()
	{
		return driver.getWindowHandle();
	}
	
	//get number of windows handles size
	public int getWindowSize(){
		 return driver.getWindowHandles().size();
	}
	
	public void switchToLastWindow(){			

		TestLogger.logDebug("The current active windows handle before switch is: "+ driver.getWindowHandle());
		Set<String> handles = driver.getWindowHandles();
		Object[] handle = handles.toArray();

		int size = handle.length;
		if (size > 0)
			driver.switchTo().window((String) handle[size - 1]);
		TestLogger.logDebug("The current active windows handle after switch is: "+ driver.getWindowHandle());
	}
	
	//close the current tab
	public void close(){
		WebDriverCore.driver.close();
	}


}
