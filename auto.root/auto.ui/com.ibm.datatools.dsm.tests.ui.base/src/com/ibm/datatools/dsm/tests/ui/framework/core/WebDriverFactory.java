package com.ibm.datatools.dsm.tests.ui.framework.core;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.ibm.datatools.test.utils.Environment;
import com.ibm.datatools.test.utils.Properties;
import com.ibm.datatools.test.utils.TestLogger;



public class WebDriverFactory {

	private WebDriver driver;
	private String browserPath = "../com.ibm.datatools.dsm.tests.ui.base/testFiles/others/";

	public WebDriver getFirefoxDriver(String headless) {

		String FirefoxBinary = null; 
		String geckodriver = null;
		
		if (Environment.isWindows()) {
			FirefoxBinary = Properties.getConfigValue("WIN_FIREFOX_BINARY");
			geckodriver = "geckodriver.exe";
		} else if (Environment.isLinux()) {
			FirefoxBinary = Properties.getConfigValue("LINUX_FIREFOX_BINARY");
			geckodriver = "geckodriver";
		} else if (Environment.isMacOS()) {
			FirefoxBinary = Properties.getConfigValue("MACOS_FIREFOX_BINARY");
			geckodriver = "geckodriver_macos";
		} else {
			TestLogger.logWarn("We do not support this OS for selenium automation, please contact owner for details.");
			return null;
		}
		final File firefoxPath = new File(FirefoxBinary);
		FirefoxBinary firefoxBinary = new FirefoxBinary(firefoxPath);

		if (headless.equals("true")) {
			System.out.println("Headless option specified");
			String Xport = System.getProperty("lmportal.xvfb.id", ":1");
			System.out.println("port is: " + Xport);
			firefoxBinary.setEnvironmentProperty("DISPLAY", Xport);
		}

		System.out.println("Creating Firefox driver...");
		FirefoxProfile profile = new FirefoxProfile();
		profile.setEnableNativeEvents(true);
		
		//profile.setPreference("network.proxy.type", 1); 
		
		//profile.setAcceptUntrustedCertificates(true);
		//profile.setAssumeUntrustedCertificateIssuer(true);	
		
		System.setProperty("webdriver.gecko.driver",  browserPath + geckodriver);
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		capabilities.setCapability("marionette", true);
		driver = new FirefoxDriver(firefoxBinary, profile, capabilities);
		return driver;
	}

	public WebDriver getChromeDriver() {
		System.setProperty("webdriver.chrome.driver",
				browserPath
						+ "chromedriver_for_win.exe");
		driver = new ChromeDriver();
		return driver;
	}

	public WebDriver getIEDriver() {
		System.setProperty("webdriver.ie.driver",
				browserPath
						+ "IEDriverServer.exe");
		DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
		ieCapabilities.setCapability(
				InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		driver = new InternetExplorerDriver(ieCapabilities);
		return driver;
	}
	
}
