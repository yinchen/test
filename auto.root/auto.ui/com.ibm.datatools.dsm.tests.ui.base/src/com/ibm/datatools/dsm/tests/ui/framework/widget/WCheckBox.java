package com.ibm.datatools.dsm.tests.ui.framework.widget;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class WCheckBox extends WebWidget{

	public WCheckBox(WebDriverCore dc, String parent, String children) {
		super(dc, parent, children);
	}
	
	public WCheckBox(WebDriverCore dc, String children) {
		super(dc, children);
	}
	
	public void check(int i){
		if(!this.isSelected(i))
			this.getElements().get(i).click();
	}
	
	public void uncheck(int i){
		if(this.isSelected(i))
			this.getElements().get(i).click();
	}
	
	public boolean isSelected(int i){
		return this.getElements().get(i).isSelected();
	}
	
	

}
