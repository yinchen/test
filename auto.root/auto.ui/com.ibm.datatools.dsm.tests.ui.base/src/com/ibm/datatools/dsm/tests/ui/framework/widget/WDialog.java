package com.ibm.datatools.dsm.tests.ui.framework.widget;


import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class WDialog extends WebWidget {

	private WDialog(WebDriverCore dc, String children) {
		super(dc, children);
	}
	
	public WDialog(WebDriverCore dc, String parent, String children){		
		super(dc, parent, children);
	}
	
	
	public String getTitle(int i){
		//dialog title will be in the first line
		try{
			return this.getElements().get(i).getText().split("\n")[0];
		}
		catch(Exception e){
			return null;
		}	
	}


}
