package com.ibm.datatools.dsm.tests.ui.framework.widget;

import org.openqa.selenium.support.ui.ExpectedConditions;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class WLink extends WebWidget{

	private WLink(WebDriverCore dc, String children) {
		super(dc, children);
	}
	
	public WLink(WebDriverCore dc, String parent, String children){		
		super(dc, parent, children);
	}
	
	public String getLinkText(int i){	
		return this.getElements().get(i).getText();
	}
	
	public void click(int i){		
		this.getElements().get(i).click();		
	}
	
	public boolean isEnabled(int i){
		return this.getElements().get(i).isEnabled();
	}
	
	public void waitForClickable(int i){
		this.driverCore.getWait().until(ExpectedConditions.elementToBeClickable(this.getElements().get(i)));
	}

}
