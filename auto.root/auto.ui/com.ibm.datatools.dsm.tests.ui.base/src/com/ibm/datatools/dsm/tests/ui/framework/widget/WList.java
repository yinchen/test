package com.ibm.datatools.dsm.tests.ui.framework.widget;

import org.openqa.selenium.WebElement;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

 
public class WList extends WebWidget{

	private WList(WebDriverCore dc, String children){
		super(dc, children);
	}
	
	public WList(WebDriverCore dc, String parent, String children){		
		super(dc, parent, children);
	}
	
	public void select(String s){	
		for(WebElement wb:this.getElements()){
			if(wb.getText().equals(s))
				wb.click();
			    break;
		}
	}

}
