package com.ibm.datatools.dsm.tests.ui.framework.widget;



import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class WMenuItem extends WebWidget{
	
	
	private WMenuItem(WebDriverCore dc, String children){
		super(dc, children);
	}
	
	public WMenuItem(WebDriverCore dc, String parent, String children){		
		super(dc, parent, children);
	}
	
	//Only support menu item selection 
	public void select(int i) {
		this.getElements().get(i).click();
	}
	

}
