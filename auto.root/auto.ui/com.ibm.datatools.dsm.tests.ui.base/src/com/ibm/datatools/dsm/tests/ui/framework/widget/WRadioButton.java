package com.ibm.datatools.dsm.tests.ui.framework.widget;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class WRadioButton extends WebWidget{

	private WRadioButton(WebDriverCore dc, String children) {
		super(dc, children);
	}
	
	public WRadioButton(WebDriverCore dc, String parent, String children){		
		super(dc, parent, children);
	}
	
	public void select(int i){	
		this.getElements().get(i).click();
	}

}
