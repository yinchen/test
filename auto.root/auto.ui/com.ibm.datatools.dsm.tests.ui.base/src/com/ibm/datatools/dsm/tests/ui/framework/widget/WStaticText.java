package com.ibm.datatools.dsm.tests.ui.framework.widget;



import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class WStaticText extends WebWidget {

	private WStaticText(WebDriverCore dc, String children){
		super(dc, children);
	}
	
	public WStaticText(WebDriverCore dc, String parent, String children){		
		super(dc, parent, children);
	}
	
	public String getText(int i){	
		return this.getElements().get(i).getText();
	}
	
	
	

}
