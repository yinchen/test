package com.ibm.datatools.dsm.tests.ui.framework.widget;




import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class WTextBox extends WebWidget{
	
	
	private WTextBox(WebDriverCore dc, String children){
		super(dc, children);
	}
	
	public WTextBox(WebDriverCore dc, String parent, String children){
		super(dc, parent, children);
	}
	
	//Used for log
	public String getWidgetType() {
		return "textbox"; 
	}
	
	public void setText(int i, String s){	
		this.getElements().get(i).click();              //activate text field
		this.getElements().get(i).clear();			
		//After clear text, the text can not input correctly after pause, so sleep 1s
		WebDriverCore.pause(1);			
		this.getElements().get(i).sendKeys(s);	
		//this.driverCore.getSeleniumactions().sendKeys(Keys.TAB).perform();
	}
	
	
	public void click(int i){		
		this.getElements().get(i).click();		
	}
	
	
	

}
