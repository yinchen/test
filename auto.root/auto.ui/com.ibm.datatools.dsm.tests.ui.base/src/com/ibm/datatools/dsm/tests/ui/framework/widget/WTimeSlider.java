package com.ibm.datatools.dsm.tests.ui.framework.widget;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class WTimeSlider extends WebWidget{
	
	private WTimeSlider(WebDriverCore dc, String children){
		super(dc, children);
	}
	
	public WTimeSlider(WebDriverCore dc, String parent, String children){
		super(dc, parent, children);
	}
	
	public void drag(int i,int movement){
		for(int j=0; j<movement; j++){
			this.getElements().get(i).click();
		}
	}
	
}
