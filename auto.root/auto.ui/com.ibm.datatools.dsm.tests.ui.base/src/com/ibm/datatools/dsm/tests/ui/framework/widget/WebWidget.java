package com.ibm.datatools.dsm.tests.ui.framework.widget;

import java.util.List;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.ibm.datatools.dsm.tests.ui.framework.base.Configuration;
import com.ibm.datatools.dsm.tests.ui.framework.core.Locator;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.test.utils.TestLogger;


public class WebWidget {	
	
	protected WebDriverCore driverCore = null;
	protected String parent = null;
	protected String children = null;
		
	protected static Locator locator = new Locator(Configuration.getLocatorYamlFiles());
	
	protected WebWidget(WebDriverCore dc, String children){
		this.driverCore = dc;			
		this.children = children;
	}
	
	public WebWidget(WebDriverCore dc, String parent, String children){		
		this(dc, children);		
		this.parent = parent;		
	}
	
	/*Get children elements from root OR from parent. 
	 *If sTimeout is null or empty, it use the config timeOut value from property file "config.properties"
	 *If sTimeout is 0, it will find elements with no wait time even though no element is found 
	 *If sTimeout is large than 0, if specified element is not found, it will wait for sTimeout time and find again.
	 */
	public List<WebElement> getElements(String sTimeout){		
		
		//if the parent is null, will return children elements directly
		if(parent == null){
			return driverCore.findElements(locator.getLocator(children), sTimeout);
		}
		/*if the parent is not null, firstly will find the first displayed parent, 
		 * and then find its children
		 */
		else{			
			List<WebElement> parents = driverCore.findElements(locator.getLocator(parent), sTimeout);			
			if (parents.size() == 0 )
			{
				TestLogger.logError("FAIL - Can not find Parent Web Element \"" + parent + "\" in current page ");	
				TestLogger.logError("Please refer to the screenshot for more informaton: "+ driverCore.saveScreenshot());
				//Assert.fail("FAIL - Can not find Parent Web Element \"" + parent + "\" in current page ");
				return driverCore.findElements(locator.getLocator(children), sTimeout);	
			}
			
			int index = 0;
			for(int i=0;  i<parents.size(); i++)
			{
				if(parents.get(i).isDisplayed())
					index = i;
			}			
			return driverCore.findElements(parents.get(index), 
					locator.getLocator(children), sTimeout);	
		}
	}
	
	//The sTimeout is empty, it use the config timeOut value from property file "config.properties"
	public List<WebElement> getElements(){
		return this.getElements(null);
	}	
	
	/*The element exists, it does not indicate the element will display
	 * It will return result immediately without wait time
	 */
	private boolean isExists() {
		//The timeout is 0
		String sTimeout = "0";
		if (this.getElements(sTimeout).size() >= 1)
			return true;
		else
			return false;
	}	
	
	//It will return result immediately without wait time
	public boolean isDisplayed(int i) {			
		if(this.isExists()){
			String sTimeout = "0";
			if (this.getElements(sTimeout).get(i).isDisplayed())
	        	return true;
	        else
				return false;		
		}
		else
			return false;		
	}
	
	//It will return result immediately without wait time
	public int getSize(){
		String sTimeout = "0";
		return this.getElements(sTimeout).size();
	}
	
	public void waitForVisible(int i){
		if(this.isExists())
			this.driverCore.getWait().until(ExpectedConditions.visibilityOf(this.getElements().get(i)));
	}
	
	
	public void scrolledIntoView(int i){
		WebElement wb = this.getElements().get(i);		
		driverCore.executeScript("arguments[0].scrollIntoView(true);", wb);
		WebDriverCore.pause();
	}
	
	/*It will return result immediately without wait time
	 * When multiple elements are found, it will find the first displayed element, and return its index.
	 */
	public int getCurrentIndex(){
		
		String sTimeout = "0";
		int size = this.getSize();
		if (size == 1)
			return 0;
		else if(size > 1)
		{
			for(int i =0 ; i < this.getSize() ; i++){			
				if(this.getElements(sTimeout).get(i).isDisplayed())
				{
					return i;					
				}
			}
			//this is the error return 
			TestLogger.logWarn("No displayed element was found!!!");
			return 0;			
		}
		else
			//this is the error return 
			TestLogger.logWarn("No element was found!!!");
			return 0;

	}	
	
	//Mouse Right-click 
	public void contextClick(int i){
		this.driverCore.contextClick(this.getElements().get(i));
	}
	
	//Move the mouse to Element
	public void moveToElement(int i){
		this.driverCore.moveToElement(this.getElements().get(i));
	}

}
