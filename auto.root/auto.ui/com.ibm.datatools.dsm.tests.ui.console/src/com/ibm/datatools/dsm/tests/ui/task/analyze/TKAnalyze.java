package com.ibm.datatools.dsm.tests.ui.task.analyze;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;

public class TKAnalyze extends TaskBase{
	
	private final String analyze_page = "analyze_page";
	
	private final String develop_label = "develop_label";
	private final String datascienceexperience_label = "datascienceexperience_label";
	private final String randrstudio_label = "randrstudio_label";
	private final String analyticapis_label = "analyticapis_label";
	private final String launchdsx_button = "launchdsx_button";
	private final String launchrstudio_button = "launchrstudio_button";
	private final String exploretheapis_button = "exploretheapis_button";

	public TKAnalyze(WebDriverCore driverCore) {
		super(driverCore);
	}
	
	public boolean isDispalyDevelopLabel(){
		return this.isDisplayed(analyze_page, develop_label);
	}
	
	public boolean isDispalyDataScienceExperienceLabel(){
		return this.isDisplayed(analyze_page, datascienceexperience_label);
	}
	
	public boolean isDispalyRandRStudioLabel(){
		return this.isDisplayed(analyze_page, randrstudio_label);
	}
	
	public boolean isDispalyAnalyticAPIsLabel(){
		return this.isDisplayed(analyze_page, analyticapis_label);
	}
	
	public boolean isDispalyLaunchDSXButton(){
		return this.isDisplayed(analyze_page, launchdsx_button);
	}
	
	public boolean isDispalyLaunchRStudioButton(){
		return this.isDisplayed(analyze_page, launchrstudio_button);
	}
	
	public boolean isDispalyExploretheAPIsButton(){
		return this.isDisplayed(analyze_page, exploretheapis_button);
	}
	
	
	public void verifyAnalyzePageInfo() {
/*		this.testVerify.assertTrue(this.isDispalyDevelopLabel(), 
				"Analyze page Show Develop analytic and machine learning application label displayed!!!");		
		this.testVerify.assertTrue(this.isDispalyRandRStudioLabel(), 
				"Analyze page R and RStudio Label displayed!!!");		
		
		this.testVerify.assertTrue(this.isDispalyLaunchDSXButton(), 
				"Analyze page Launch DSX Button displayed!!!");
		
		this.testVerify.assertTrue(this.isDispalyLaunchRStudioButton(), 
				"Analyze page Launch RStudio Button displayed!!!");*/
		this.testVerify.assertTrue(this.isDispalyAnalyticAPIsLabel(), 
				"Analyze page Analytic APIs Label displayed!!!");
		this.testVerify.assertTrue(this.isDispalyDataScienceExperienceLabel(), 
				"Analyze page Data Science Experience Label displayed!!!");
		this.testVerify.assertTrue(this.isDispalyLaunchDSXButton(), 
				"Analyze page Launch DSX Button displayed!!!");
		this.testVerify.assertTrue(this.isDispalyExploretheAPIsButton(), 
				"Analyze page Explore the APIs Button displayed!!!");
		
	}
	
	public void waitForPageLoadCompleted(){		
		this.waitForPageLoadCompleted(analyze_page, datascienceexperience_label);
		
	}

}
