package com.ibm.datatools.dsm.tests.ui.task.backupandrestore;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;

public class TKBackupAndRestore extends TaskBase {

	public TKBackupAndRestore(WebDriverCore driverCore) {
		super(driverCore);
	}
	
	private final String backupandrestore_page = "backupandrestore_page";
	
	private final String schedulebackup_label = "schedulebackup_label";
	private final String restore_label = "restore_label";

	
	public boolean isDispalyScheduleBackupLabel(){
		return this.isDisplayed(backupandrestore_page, schedulebackup_label);
	}
	
	public boolean isDispalyRestoreLabel(){
		return this.isDisplayed(backupandrestore_page, restore_label);
	}
	
	public void verifyBackupAndRestorePageInfo(){		
		this.testVerify.assertTrue(this.isDispalyScheduleBackupLabel(), 
				"Backup and Restore page schedule backup label displayed!!!");
		
		
		this.testVerify.assertTrue(this.isDispalyRestoreLabel(), 
				"Backup and Restore page restore label displayed!!!");
	
	}
	
	public void waitForPageLoadCompleted(){		
		long timeBegins = System.currentTimeMillis();		
		do {
			WebDriverCore.pause(pauseTime);			
			if(this.isDispalyScheduleBackupLabel()){	
				break;
			}		
		} while (System.currentTimeMillis() - timeBegins <= timeOut * 1000);
	System.currentTimeMillis();	
	}
}
