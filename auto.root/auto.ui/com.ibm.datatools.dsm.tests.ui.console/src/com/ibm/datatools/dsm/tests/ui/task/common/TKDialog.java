package com.ibm.datatools.dsm.tests.ui.task.common;


import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;
import com.ibm.datatools.test.utils.Properties;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;


public class TKDialog extends TaskBase{
	

	//private final String cred_dialog = "cred_dialog";
	
	private final String quick_tour_dialog = "quick_tour_dialog";	
	private final String quick_tour_dialog_close_btn = "quick_tour_dialog_close_btn";	
	
	private final String connect_dialog_username_textbox = "connect_dialog_username_textbox";
	private final String connect_dialog_password_textbox = "connect_dialog_password_textbox";
	private final String connect_save_checkbox = "connect_save_checkbox";

	public TKDialog(WebDriverCore driverCore) {
		super(driverCore);
	}	
	
	public String getFocusedDialogTitle(){
		return this.getDialogTitle(focused_dialog);
	}
	
	public void handleDialog(String dbProfile, String dbUserName, String dbPassword){
		//for performance skip handleDialog add by ZengYan
		String performanceProperties = Properties.getConfigValue("performance");
		   if( !"performance".equals(performanceProperties)){
			  //tour dialog	
			   if(this.isDisplayed(quick_tour_dialog_close_btn)){
					this.handleQuickTourDialog();
				}
				//other dialogs
				if(this.isDisplayed(focused_dialog))
				{
					//Add pause because the title is null if dialog pops up but title is still loading
					//WebDriverCore.pause(5);	
					if(this.getFocusedDialogTitle().equals("Connect: "+dbProfile))
					{
						this.setTextBox(focused_dialog, connect_dialog_username_textbox, dbUserName);
						this.setTextBox(focused_dialog, connect_dialog_password_textbox, dbPassword);
						this.checkCheckBox(focused_dialog, connect_save_checkbox);
						this.clickBtn(focused_dialog, ok_button);						
					}
					else if(this.getFocusedDialogTitle().startsWith("Connect:"))
						this.clickBtn(focused_dialog, cancel_button);
					else
						this.clickBtn(focused_dialog, close_button);
				}
		   }else{
			   System.out.println("ExcuteQuickTourAndCredential");
		   }
				
	}
	
	public void handleDialogUser(String dbProfile, String dbUserName, String dbPassword){
				
			  //tour dialog	
			   if(this.isDisplayed(quick_tour_dialog_close_btn)){
					this.handleQuickTourDialog();
				}
				//other dialogs
				if(this.isDisplayed(focused_dialog))
				{
					//Add pause because the title is null if dialog pops up but title is still loading
					//WebDriverCore.pause(5);	
					if(this.getFocusedDialogTitle().equals("Connect: "+dbProfile))
					{
						this.setTextBox(focused_dialog, connect_dialog_username_textbox, dbUserName);
						this.setTextBox(focused_dialog, connect_dialog_password_textbox, dbPassword);
						this.checkCheckBox(focused_dialog, connect_save_checkbox);
						this.clickBtn(focused_dialog, ok_button);						
					}
					else if(this.getFocusedDialogTitle().startsWith("Connect:"))
						this.clickBtn(focused_dialog, cancel_button);
					else
						this.clickBtn(focused_dialog, close_button);
				}
	}


	
	public void handleDialog(String dbProfile, String dbPassword){			

		
		//tour dialog	
		this.handleQuickTourDialog();
		//other dialogs
		if(this.isDisplayed(focused_dialog))
		{		
			//Add pause because the title is null if dialog pops up but title is still loading
			//WebDriverCore.pause(5);	
			//this.waitForDialogLoading();				
			if(this.getFocusedDialogTitle().equals("Connect: "+dbProfile))
			{
				this.setTextBox(focused_dialog, connect_dialog_password_textbox, dbPassword);
				this.checkCheckBox(focused_dialog, connect_save_checkbox);
				this.clickBtn(focused_dialog, ok_button);						
			}
			else if(this.getFocusedDialogTitle().startsWith("Connect:"))
				this.clickBtn(focused_dialog, cancel_button);
			else
				this.clickBtn(focused_dialog, close_button);
		}
	}
	/**
	 * @author yhjyang
	 * handle quick tour when admin menu task changed
	 */
	public void handleQuickTourDialog() {
		WebDriverCore.pause(3);
		
		if(this.isDisplayed(quick_tour_dialog_close_btn))
		{
		  //quick_tour_dialog_close_btn
			this.clickBtn(quick_tour_dialog_close_btn);	
		}		
		WebDriverCore.pause(1);
	}

	public void clickOKbtnonFocusedDialog(){
		if(this.isDisplayed(focused_dialog)){
			//The DSM fix the related issue
			//this.clickBtn(getSize(focused_dialog, ok_button)-1, focused_dialog, ok_button);
			this.clickBtn(focused_dialog, ok_button);
		}
	}
	
	public void closeFocusedDialog(){
		if(this.isDisplayed(focused_dialog)){
			//The DSM fix the related issue
			//this.clickBtn(getSize(focused_dialog, close_button)-1, focused_dialog, close_button);
			this.clickBtn(focused_dialog, close_button);
		}
	}

	public void closeFocusedDialog(String dialogFocused,String closeButton){
		if(this.isDisplayed(dialogFocused)){
			this.clickBtn(dialogFocused, closeButton);
		}
	}
	
	//It is not ready yet.
	//Add pause because the title is null if dialog pops up but title is still loading
	public void waitForDialogLoading(){		
		int retry = 0;
		int maxRety = 10;
		while(this.getFocusedDialogTitle() == null || this.getFocusedDialogTitle().isEmpty()){
			WebDriverCore.pause();
			
			retry++;
			if(retry >= maxRety)
				break;
		}	
	}
	
	public void waitForDialogPopup(){		
		long timeBegins = System.currentTimeMillis();		
		do {			
			WebDriverCore.pause(pauseTime);
			if(this.isDisplayed(focused_dialog)){				
				break;
			}		
		} while (System.currentTimeMillis() - timeBegins <= timeOut * 1000);
	}
	
	public void waitForDialogPopup(String dialogWanted){		
		long timeBegins = System.currentTimeMillis();		
		do {			
			WebDriverCore.pause(pauseTime);
			if(this.isDisplayed(dialogWanted)){				
				break;
			}		
		} while (System.currentTimeMillis() - timeBegins <= timeOut * 1000);
	}
	
	public void clickYesbtnonFocusedDialog(){
		this.clickBtn(yes_button);
	}
	
}