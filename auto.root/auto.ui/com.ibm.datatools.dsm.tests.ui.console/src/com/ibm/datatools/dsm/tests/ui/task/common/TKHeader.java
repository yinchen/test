package com.ibm.datatools.dsm.tests.ui.task.common;




import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKHeader extends TaskBase {
	
	private final String header_section="header_section";
			
	private final String header_logout_button = "header_logout_button";
	private final String header_dbPicker_textbox = "header_dbPicker_textbox";
	private final String header_dbPicker_list = "header_dbPicker_list";
	
	private final String menu_user = "menu_user";


	
	public TKHeader(WebDriverCore driverCore) {
		super(driverCore);
	}
	
	

	

	
	
	public void logout(TKMenu tkMenu, TKDialog dialog){
		tkMenu.selectMenu("user->logout");
		//dialog.clickOKbtnonFocusedDialog();
		dialog.clickYesbtnonFocusedDialog();
	}
	

	
	public void verifyUserMenu() {
		testVerify.assertTrue(this.isDisplayed(menu_user), "User menu button exists!!!");
	}
	
	
	
	
	

	

	
	

}
