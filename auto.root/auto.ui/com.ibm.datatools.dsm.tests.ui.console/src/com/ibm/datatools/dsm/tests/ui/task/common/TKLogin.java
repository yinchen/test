package com.ibm.datatools.dsm.tests.ui.task.common;



import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;



public class TKLogin extends TaskBase {
	
	private final String login_page_username_textbox = "login_page_username_textbox";
	private final String login_page_password_textbox = "login_page_password_textbox";
	private final String login_page_login_button = "login_page_login_button";

	public TKLogin(WebDriverCore driverCore) {
		super(driverCore);
	}
	
	public void login(String username, String password){		
		this.setTextBox(login_page_username_textbox, username);
		this.setTextBox(login_page_password_textbox, password);
		this.pause(500);
		this.clickBtn(login_page_login_button);
		//after click login button, it will load home page
		//WebDriverCore.pause(10);
		//driverCore.waitForPageLoad();
		//WebDriverCore.pause(5);	
		
		this.waitForConsoleLoading();
	}
	

	
	public void verifyLoginPage(){
		testVerify.assertTrue(this.isDisplayed(login_page_username_textbox), "User name text box exists!!!");
		testVerify.assertTrue(this.isDisplayed(login_page_password_textbox), "Password text box exists!!!");
		testVerify.assertTrue(this.isDisplayed(login_page_login_button), "Login btn exists!!!");
	}
	

}
