package com.ibm.datatools.dsm.tests.ui.task.common;


import java.util.StringTokenizer;


import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKMenu extends TaskBase{
	
	
	

	public TKMenu(WebDriverCore driverCore) {
		super(driverCore);
		
	}

	// Select Menu Item
	private void selectMenuItem(String sMenuItemName) {		
		//this.moveToElement(children);	     it does not take effect	
		this.clickMenuItem(sMenuItemName);		
	}
	
	
	public void selectMenuwithoutLoadPage(String path){
		
		int maxRetryTimes = 3; 
		int retryTime = 0;
		
		String menuitem = "";
		StringTokenizer st = new StringTokenizer(path.trim(), "->");		
	
		while (st.hasMoreTokens()) {			
			menuitem = st.nextToken();
			
			//it will not transfer the menuitem to lower case any more
			//menuitem = "menu_" + menuitem.toLowerCase();
			menuitem = "menu_" + menuitem;
			//Enhance the code, if one of them is failed, we will begin again.
			if(this.isDisplayed(menuitem))
			{
				this.selectMenuItem(menuitem);
			}
			else
			{
				st = new StringTokenizer(path.trim(), "->");
				retryTime ++;
				TaskBase.driverCore.pause(pauseTime);
			}
			if (retryTime >= maxRetryTimes)
				break;			
								
		}
		TaskBase.driverCore.waitForPageLoad();			
	}
	
	
	/**
	 * Parses the path parameter and calls click the appropriate number of times to support menu selection.
	 * For example, calling with a parameter path of "Help->Welcome" will result in the following 2 click invocations: <br>
	 * selectMenuItem("Help"); <br>
	 * selectMenuItem("Welcome"); <br>
	 * @param path		the path to the menu you want to select
	 */	
	public void selectMenu(String path, TKDialog dialog, String dbPrfile, String dbUserName, String dbPassword){
		
		this.selectMenuwithoutLoadPage(path);
		dialog.handleDialog(dbPrfile, dbUserName, dbPassword);		
		TaskBase.driverCore.waitForPageLoad();
		this.waitForStandByIconDisappear();
	}
	
	
	public void selectMenu(String path){		
		this.selectMenuwithoutLoadPage(path);		
		TaskBase.driverCore.waitForPageLoad();
		this.waitForStandByIconDisappear();
	}
	
	public void selectMenuNoWaitingIcon(String path){		
		this.selectMenuwithoutLoadPage(path);		
		TaskBase.driverCore.waitForPageLoad();
		//this.waitForStandByIconDisappear();
	}
	
	public boolean isAnalyticsDisp() {
		return this.isDisplayed("menu_develop_analytics");
	}
	
	public void VerifyAnalyticsDisp() {
		this.testVerify.assertTrue(this.isAnalyticsDisp(), "Develop Analyze menu displayed");
	}
	
	public void VerifyAnalyticsNotDisp() {
		this.testVerify.assertTrue(!this.isAnalyticsDisp(), "Develop Analyze menu displayed");
	}

}
