package com.ibm.datatools.dsm.tests.ui.task.connection;

import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKConnectionInformation extends TaskBase{

	
	private final String connection_info_page = "connection_info_page";
	
	private final String linux_menu = "linux_menu";
	private final String mac_menu = "mac_menu";
	private final String power_linux_menu = "power_linux_menu";
	private final String windows_menu = "windows_menu";
	
	
	public TKConnectionInformation(WebDriverCore driverCore) {
		super(driverCore);
	}
	
	public boolean isExistsLinuxMenu(){
		return this.isDisplayed(connection_info_page, linux_menu);
	}
	
	public boolean isExistsMacMenu(){
		return this.isDisplayed(connection_info_page, mac_menu);
	} 
	
	public boolean isExistsPLinuxMenu(){
		return this.isDisplayed(connection_info_page, power_linux_menu);
	} 
	
	public boolean isExistsWinMenu(){
		return this.isDisplayed(connection_info_page, windows_menu);
	} 

	public void waitForPageLoadCompleted(){
		long timeBegins = System.currentTimeMillis();		
		do {
			WebDriverCore.pause(pauseTime);			
			if(this.isExistsLinuxMenu()){	
				break;
			}		
		} while (System.currentTimeMillis() - timeBegins <= timeOut * 1000);
	}
	
	public void verifyConnectionInfomationpage(){
		
		this.testVerify.assertTrue(this.isExistsLinuxMenu(), "Connection Information page Linux menu displayed");
		this.testVerify.assertTrue(this.isExistsMacMenu(), "Connection Information page Mac menu displayed");
		this.testVerify.assertTrue(this.isExistsPLinuxMenu(), "Connection Information page PowerLinux menu displayed");
		this.testVerify.assertTrue(this.isExistsWinMenu(), "Connection Information page Windows menu displayed");
		
	}
	
	
}
