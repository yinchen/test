package com.ibm.datatools.dsm.tests.ui.task.connection;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;

public class TKDownloadTools extends TaskBase{

	public TKDownloadTools(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}
	
	private final String download_tools_page = "download_tools_page";
	
	private final String client_container = "client_container";
	private final String design_software = "design_software";
	private final String administration_software = "administration_software";
	private final String conversion_workbench = "conversion_workbench";
	private final String database_support_tools = "database_support_tools";
	
	public boolean isExistsClient_Container(){
		return this.isDisplayed(download_tools_page, client_container);
	}
	
	public boolean isExistsDesign_Software(){
		return this.isDisplayed(download_tools_page, design_software);
	} 
	
	public boolean isExistsAdministration_Software(){
		return this.isDisplayed(download_tools_page, administration_software);
	} 
	
	public boolean isExistsDatabase_Support_Tools(){
		return this.isDisplayed(download_tools_page, database_support_tools);
	} 
	
	public boolean isExistsConversion_Workbench(){
		return this.isDisplayed(download_tools_page, conversion_workbench);
	} 
	
	public void waitForPageLoadCompleted(){
		long timeBegins = System.currentTimeMillis();		
		do {
			WebDriverCore.pause(pauseTime);			
			if(this.isExistsClient_Container()){	
				break;
			}		
		} while (System.currentTimeMillis() - timeBegins <= timeOut * 1000);
	}
	
	public void verifyDownloadToolsPage(){
		
		this.testVerify.assertTrue(this.isExistsClient_Container(), "Download tools page Client_Containter item displayed");
		this.testVerify.assertTrue(this.isExistsDesign_Software(), "Download tools page Design_Software item displayed");
		this.testVerify.assertTrue(this.isExistsAdministration_Software(), "Download tools page Administration_Software item displayed");
		this.testVerify.assertTrue(this.isExistsDatabase_Support_Tools(), "Download tools page Database_Support_Tools item displayed");
		this.testVerify.assertTrue(this.isExistsConversion_Workbench(), "Download tools page Conversion_Workbench item displayed");
		
	}

}
