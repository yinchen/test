package com.ibm.datatools.dsm.tests.ui.task.explore;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;

public class TKExplore extends TaskBase{

	private final String expolre_page = "expolre_page";
	
	private final String showsystemschemas_button = "showsystemschemas_button";
	private final String explore_refresh_button = "explore_refresh_button";
	private final String newschema_button = "newschema_button";
	private final String findaschema_input = "findaschema_input";
	private final String schema_label = "schema_label";
	
	public TKExplore(WebDriverCore driverCore) {
		super(driverCore);
	}

	public boolean isDispalyShowSystemSchemasButton(){
		return this.isDisplayed(expolre_page, showsystemschemas_button);
	}
	
	public boolean isDispalyRefreshButton(){
		return this.isDisplayed(expolre_page, explore_refresh_button);
	}
	
	public boolean isDispalyNewSchemaButton(){
		return this.isDisplayed(expolre_page, newschema_button);
	}
	
	public boolean isDispalyFindAschemaInput(){
		return this.isDisplayed(expolre_page, findaschema_input);
	}
	
	public boolean isDispalySchemaLabel(){
		return this.isDisplayed(expolre_page, schema_label);
	}
	
	
	public void verifyExplorePageInfo(){		
		this.testVerify.assertTrue(this.isDispalyShowSystemSchemasButton(), 
				"Explore page Show System Schemas Button displayed!!!");
		
		
		this.testVerify.assertTrue(this.isDispalyNewSchemaButton(), 
				"Explore page New Schema Button displayed!!!");
		
		this.testVerify.assertTrue(this.isDispalyFindAschemaInput(), 
				"Explore page Find A schema Input displayed!!!");
		
		this.testVerify.assertTrue(this.isDispalySchemaLabel(), 
				"Explore page Schema label displayed!!!");
		
		this.testVerify.assertTrue(this.isDispalyRefreshButton(), 
				"Explore page Refresh Button displayed!!!");
	}
	
	public void waitForPageLoadCompleted(){		
		long timeBegins = System.currentTimeMillis();		
		do {
			WebDriverCore.pause(pauseTime);			
			if(this.isDispalyNewSchemaButton()){	
				break;
			}		
		} while (System.currentTimeMillis() - timeBegins <= timeOut * 1000);
	System.currentTimeMillis();	
	}
	
	
}
