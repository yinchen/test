package com.ibm.datatools.dsm.tests.ui.task.help;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;


public class TKAskQuestion extends TaskBase{

//	private final String askquestion_stackoverflow_Lable = "askquestion_stackoverflow_Lable";
//	private final String askquestion_search_Lable = "askquestion_search_Lable";
//	private final String askquestion_search_button = "askquestion_search_button";
//	private final String askquestion_askquestion_button = "askquestion_askquestion_button";
	private final String askquestion_perpage_label = "askquestion_perpage_label";
	public TKAskQuestion(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyURL(String actual){		
		this.testVerify.assertStringEquals(actual, "https://stackoverflow.com/search?q=dashdb",  
				"Ask A Question page URL is correct!!!");
		
	}
	
//	public boolean isStackOverflowLableDisplay() {
//		return this.isDisplayed(askquestion_stackoverflow_Lable);
//	}
//	
//	public boolean isSeachLableDisplay() {
//		return this.isDisplayed(askquestion_search_Lable);
//	}
//	
//	public boolean isSeachButtonDisplay() {
//		return this.isDisplayed(askquestion_search_button);
//	}
//	
//	public boolean isAskQuestionButtonDisplay() {
//		return this.isDisplayed(askquestion_askquestion_button);
//	}
//	public void verifyHelpAskQuestionPageInfo(){		
//		this.testVerify.assertTrue(this.isStackOverflowLableDisplay(), 
//				"Ask a question page Stack Overflow Lable displayed!!!");
//		this.testVerify.assertTrue(this.isSeachLableDisplay(), 
//				"Ask a question page Search label displayed!!!");
//		this.testVerify.assertTrue(this.isSeachButtonDisplay(), 
//				"Ask a question page Seach Button displayed!!!");
//		this.testVerify.assertTrue(this.isAskQuestionButtonDisplay(), 
//				"Ask a question page Ask Question Button displayed!!!");
//	}
	
	public boolean isPerPageLaeblDisplay() {
	return this.isDisplayed(askquestion_perpage_label);
    }

	public void waitForPageLoadCompleted(){		
		long timeBegins = System.currentTimeMillis();		
		do {
			WebDriverCore.pause(pauseTime);			
			if(this.isPerPageLaeblDisplay()){	
				break;
			}		
		} while (System.currentTimeMillis() - timeBegins <= timeOut * 1000);
	System.currentTimeMillis();	
	}

}
