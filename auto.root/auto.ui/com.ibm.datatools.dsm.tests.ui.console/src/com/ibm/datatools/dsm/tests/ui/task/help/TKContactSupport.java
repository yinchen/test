package com.ibm.datatools.dsm.tests.ui.task.help;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;


public class TKContactSupport extends TaskBase{
//	private final String contractSupport_blumix_Lable = "contractSupport_blumix_Lable";
//	private final String contractSupport_Sales_Lable = "contractSupport_Sales_Lable";
//	private final String contractSupport_billing_Lable = "contractSupport_billing_Lable";
//	private final String contractSupport_account_Lable = "contractSupport_account_Lable";
//	private final String contractSupport_technical_Lable = "contractSupport_technical_Lable";
	
	private final String contractSupport_gethelp_button = "contractSupport_gethelp_button";
	
	public TKContactSupport(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}
	
	
//	public void verifyHelpContactSupportPageInfo(){		
//		this.testVerify.assertTrue(this.isTextDisp(), 
//				"Contact Support page Show System Schemas Button displayed!!!");
//		this.testVerify.assertTrue(this.isSalesLableDisplay(), 
//				"Contact Support page Sales label displayed!!!");
//		this.testVerify.assertTrue(this.isBillingLableDisplay(), 
//				"Contact Support page Billing label displayed!!!");
//		this.testVerify.assertTrue(this.isAccountLableDisplay(), 
//				"Contact Support page Account and login label displayed!!!");
//		this.testVerify.assertTrue(this.isTechnicalLableDisplay(), 
//				"Contact Support page Technical Support label displayed!!!");
//	}
	
	public void verifyURL(String actual){	
		
		this.testVerify.assertStringEquals(actual, "https://watson.service-now.com/wcp",  
				"Contact Support page URL is correct!!!");
		
	}
	
	public void waitForPageLoadCompleted(){		
		long timeBegins = System.currentTimeMillis();		
		do {
			WebDriverCore.pause(pauseTime);			
			if(this.isGetHelpDisp()){	
				break;
			}		
		} while (System.currentTimeMillis() - timeBegins <= timeOut * 1000);
	System.currentTimeMillis();	
	}

	public boolean isGetHelpDisp(){
	return this.isDisplayed(contractSupport_gethelp_button);
}
	
//	public boolean isTextDisp(){
//		return this.isDisplayed(contractSupport_blumix_Lable);
//	}
//	
//	public boolean isSalesLableDisplay(){
//		return this.isDisplayed(contractSupport_Sales_Lable);
//	}
//	
//	public boolean isAccountLableDisplay(){
//		return this.isDisplayed(contractSupport_billing_Lable);
//	}
//	
//	public boolean isTechnicalLableDisplay(){
//		return this.isDisplayed(contractSupport_account_Lable);
//	}
//	
//	public boolean isBillingLableDisplay(){
//		return this.isDisplayed(contractSupport_technical_Lable);
//	}
	
	
}
