package com.ibm.datatools.dsm.tests.ui.task.help;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;

public class TKDOCS extends TaskBase{
	
//	private final String Docs_ibm_Lable = "Docs_ibm_Lable";
//	private final String Docs_marketplace_button = "Docs_marketplace_button";
//	private final String Docs_search_input = "Docs_search_input";
//	private final String Docs_tableofcontents_button = "Docs_tableofcontents_button";
	private final String Docs_signin_button = "Docs_signin_button";
	
	public TKDOCS(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}
	
	public void verifyURL(String actual){		
		this.testVerify.assertStringEquals(actual, "https://www.ibm.com/support/knowledgecenter/SS6NHC/com.ibm.swg.im.dashdb.kc.doc/welcome.html",  
				"DOCS page URL is correct!!!");
		
	}
	
//	public boolean isIBMLabelDisplay() {
//		return this.isDisplayed(Docs_ibm_Lable);
//	}
//	
//	public boolean isMarketplaceButtonDisplay() {
//		return this.isDisplayed(Docs_marketplace_button);
//	}
//	
//	public boolean isSearchInputDisplay() {
//		return this.isDisplayed(Docs_search_input);
//	}
//	
//	public boolean isTableOfContentsButtonDisplay() {
//		return this.isDisplayed(Docs_tableofcontents_button);
//	}
//	
//	public void verifyHelpDocsPageInfo(){		
//		this.testVerify.assertTrue(this.isIBMLabelDisplay(), 
//				"Docs page IBM Label displayed!!!");
//		this.testVerify.assertTrue(this.isMarketplaceButtonDisplay(), 
//				"Docs page Marketplace Button displayed!!!");
//		this.testVerify.assertTrue(this.isSearchInputDisplay(), 
//				"Docs page Search Input displayed!!!");
//		this.testVerify.assertTrue(this.isTableOfContentsButtonDisplay(), 
//				"Docs page Table Of Contents Button displayed!!!");
//	}
//	
	public void waitForPageLoadCompleted(){		
		long timeBegins = System.currentTimeMillis();		
		do {
			WebDriverCore.pause(pauseTime);			
			if(this.isSignInButtonDisplay()){	
				break;
			}		
		} while (System.currentTimeMillis() - timeBegins <= timeOut * 1000);
	System.currentTimeMillis();	
	}
	
	public boolean isSignInButtonDisplay() {
	return this.isDisplayed(Docs_signin_button);
	}
	

}
