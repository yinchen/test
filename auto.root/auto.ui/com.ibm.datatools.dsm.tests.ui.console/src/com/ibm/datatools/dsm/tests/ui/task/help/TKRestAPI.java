package com.ibm.datatools.dsm.tests.ui.task.help;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;

public class TKRestAPI extends TaskBase{

	public TKRestAPI(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}

	
	private final String restapi_schemas_dropdownbutton = "restapi_schemas_dropdownbutton";
//	private final String restapi_authorize_button = "restapi_authorize_button";
	
	public boolean isSchemasDropDownBtnDisp(){
		return this.isDisplayed(restapi_schemas_dropdownbutton);
	}
//	
//	public boolean isAuthorizeBtnDisp(){
//		return this.isDisplayed(restapi_authorize_button);
//	}
	

	public void waitForPageLoadCompleted(){		
		long timeBegins = System.currentTimeMillis();		
		do {
			WebDriverCore.pause(pauseTime);			
			if(this.isSchemasDropDownBtnDisp()){	
				break;
			}		
		} while (System.currentTimeMillis() - timeBegins <= timeOut * 1000);
	}
//	
//	public void verifyRestAPIInfo(){
//		
//		this.testVerify.assertTrue(this.isSchemasDropDownBtnDisp(), 
//				"Help->Rest API page schema drop down button displayed!!!");
//		
//		this.testVerify.assertTrue(this.isAuthorizeBtnDisp(), 
//				"Help->Rest API page Authorize button displayed!!!");
//					
//	}
	public void verifyURL(String actual,String dsmserver){	
		String ip=dsmserver.substring(8);
		this.testVerify.assertStringEquals(actual, "https://"+ip+"/console/api/index.html", 
				"Ask A Question page URL is correct!!!");
		
	}

}
