package com.ibm.datatools.dsm.tests.ui.task.home;

import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKHome extends TaskBase{
	private final String home_page = "home_page";
	private final String quickstats_label = "quickstats_label";
	private final String connecttoibmdb2warehouseoncloud_label = "connecttoibmdb2warehouseoncloud_label";
	private final String loaddata_button = "loaddata_button";
	private final String no_items_label = "no_items_label";
	private final String home_refresh_icon = "home_refresh_icon";

	public TKHome(WebDriverCore driverCore) {
		super(driverCore);
	}
	
	public boolean isExistsQuickStatsMenu(){
		return this.isDisplayed(home_page, quickstats_label);
	}
	
	public boolean isExistsConnectToIBMDb2WarehouseOnCloudMenu(){
		return this.isDisplayed(home_page, connecttoibmdb2warehouseoncloud_label);
	}
	
	public boolean isExistsLoadDataButton(){
		return this.isDisplayed(home_page, loaddata_button);
	}	
	
	public boolean isExistsRefreshIcon() {
		return this.isDisplayed(home_page, home_refresh_icon);
	}
	//No items to display.
	public boolean isExistsNoItemsLabel() {
		return this.isDisplayed(home_page, no_items_label);
	}
	
	public boolean isExistLoadButton() {
		return this.isDisplayed(home_page, load_button);
	}
	
	public void waitForPageLoadCompleted(){		
		long timeBegins = System.currentTimeMillis();		
		do {
			WebDriverCore.pause(pauseTime);			
			if(this.isExistsNoItemsLabel() || this.isExistsLoadDataButton()){	
				break;
			}		
		} while (System.currentTimeMillis() - timeBegins <= timeOut * 1000);
	}
	
	
	public void verifyHomePageInfo(){			
		
		this.testVerify.assertTrue(this.isExistsQuickStatsMenu(), 
				"Home page QuickStats displayed!!!");
		
		this.testVerify.assertTrue(this.isExistsConnectToIBMDb2WarehouseOnCloudMenu(), 
				"Home page ConnectToIBMDb2WarehouseOnCloud menu displayed!!!");
		
		if(isExistsNoItemsLabel())
		{
			this.testVerify.assertTrue(this.isExistLoadButton(), 
					"Home page Load button displayed!!!");
		}
		else 
		{
			this.testVerify.assertTrue(this.isExistsLoadDataButton(), 
					"Home page Loaddata button displayed!!!");
		}
	}
	
	
}
