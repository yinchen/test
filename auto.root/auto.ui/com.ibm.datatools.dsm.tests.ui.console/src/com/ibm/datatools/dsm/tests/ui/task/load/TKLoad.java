package com.ibm.datatools.dsm.tests.ui.task.load;

import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKLoad extends TaskBase{

	public TKLoad(WebDriverCore driverCore) {
		super(driverCore);
	}
	
	private final String load_page = "load_page";
	
	private final String next_button = "next_button";
	private final String lift_label = "lift_label";
	private final String softlayerswift_label = "softlayerswift_label";
	private final String mycomputer_label = "mycomputer_label";
	
	public boolean isExistsNextButton(){
		return this.isDisplayed(load_page, next_button);
	}
	
	public boolean isExistsLiftLabel(){
		return this.isDisplayed(load_page, lift_label);
	}
	
	public boolean isExistsSoftLayerSwiftLabel(){
		return this.isDisplayed(load_page, softlayerswift_label);
	}
	
	public boolean isExistsMyComputerLabel(){
		return this.isDisplayed(load_page, mycomputer_label);
	}
	
	public void verifyLoadPageInfo(){
		
				this.testVerify.assertTrue(this.isExistsNextButton(), 
						"Load page Next button displayed!!!");
				
				this.testVerify.assertTrue(this.isExistsLiftLabel(), 
						"Load page Lift label displayed!!!");
				
				this.testVerify.assertTrue(this.isExistsSoftLayerSwiftLabel(), 
						"Load page SoftLayer Swift label displayed!!!");
				
				this.testVerify.assertTrue(this.isExistsMyComputerLabel(), 
						"Load page My Computer Label displayed!!!");
	}
	
	public void waitForPageLoadCompleted(){		
		long timeBegins = System.currentTimeMillis();		
		do {
			WebDriverCore.pause(pauseTime);			
			if(this.isExistsNextButton()){	
				break;
			}		
		} while (System.currentTimeMillis() - timeBegins <= timeOut * 1000);
	System.currentTimeMillis();	
	}

}
