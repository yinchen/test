package com.ibm.datatools.dsm.tests.ui.task.monitor;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKConnections extends TKMonitorBase{

	public TKConnections(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}	
		
	public void verifyConnectionsInfo(){
		
		this.testVerify.assertTrue(this.isSearchBtnDisp(), 
				"Monitor->Connections page Search button displayed!!!");
		
		this.testVerify.assertTrue(this.isSysObjectsBtnDisp(), 
				"Monitor->Connections page System objects button displayed!!!");
		
		this.testVerify.assertTrue(this.isRefreshBtnDisp(), 
				"Monitor->Connections page Refresh button displayed!!!");
		
		this.testVerify.assertTrue(this.isFilterOffBtnDisp(), 
				"Monitor->Connections page Filter off button displayed!!!");
		
		this.testVerify.assertTrue(this.isColumnBtnDisp(), 
				"Monitor->Connections page Column button displayed!!!");
		
		this.testVerify.assertTrue(this.isExportCVSBtnDisp(), 
				"Monitor->Connections page Export CVS button displayed!!!");
	}

	
}
