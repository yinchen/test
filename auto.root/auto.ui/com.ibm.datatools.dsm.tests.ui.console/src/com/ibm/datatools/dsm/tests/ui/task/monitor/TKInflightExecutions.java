package com.ibm.datatools.dsm.tests.ui.task.monitor;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKInflightExecutions extends TKMonitorBase{

	public TKInflightExecutions(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}	
	
	
	public void verifyInflightExecutionsInfo(){
		
		this.testVerify.assertTrue(this.isSearchBtnDisp(), 
				"Monitor->Inflight Executions page Search button displayed!!!");
		
		this.testVerify.assertTrue(this.isSysStatementBtnDisp(), 
				"Monitor->Inflight Executions page System statement button displayed!!!");
		
		this.testVerify.assertTrue(this.isRefreshBtnDisp(), 
				"Monitor->Inflight Executions page Refresh button displayed!!!");
		
		this.testVerify.assertTrue(this.isFilterOffBtnDisp(), 
				"Monitor->Inflight Executions page Filter off button displayed!!!");
		
		this.testVerify.assertTrue(this.isColumnBtnDisp(), 
				"Monitor->Inflight Executions page Column button displayed!!!");
		
		this.testVerify.assertTrue(this.isExportCVSBtnDisp(), 
				"Monitor->Inflight Executions page Export CVS button displayed!!!");
	}
	
	
	

}
