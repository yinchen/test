package com.ibm.datatools.dsm.tests.ui.task.monitor;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;

public class TKMonitorBase extends TaskBase{

	public TKMonitorBase(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}
	
	public void waitForPageLoadCompleted(){		
		long timeBegins = System.currentTimeMillis();		
		do {
			WebDriverCore.pause(pauseTime);			
			if(this.isSearchBtnDisp()){	
				break;
			}		
		} while (System.currentTimeMillis() - timeBegins <= timeOut * 1000);
	}
	
	public boolean isSearchBtnDisp(){
		return this.isDisplayed(search_button);
	}
	
	public boolean isSysStatementBtnDisp(){
		return this.isDisplayed(system_statement_button);
	}
	
	public boolean isSysObjectsBtnDisp() {
		return this.isDisplayed(system_objects_button);
	}
	
	public boolean isRefreshBtnDisp(){
		return this.isDisplayed(refresh_button);
	}
	
	public boolean isFilterOffBtnDisp(){
		return this.isDisplayed(filter_off_button);
	}
	
	public boolean isColumnBtnDisp() {
		return this.isDisplayed(columns_button);
	}
	
	public boolean isExportCVSBtnDisp() {
		return this.isDisplayed(export_cvs_button);
	}



}
