package com.ibm.datatools.dsm.tests.ui.task.monitor;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKStorage extends TKMonitorBase{

	public TKStorage(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}
	
	protected final String schemas_button = "schemas_button";	
	protected final String show_heap_map_button = "show_heap_map_button";
	protected final String show_largest_size_button = "show_largest_size_button";
	
	public boolean isSchemasBtnDisp(){
		return this.isDisplayed(schemas_button);
	}
	
	public boolean isShowHeapMapBtnDisp(){
		return this.isDisplayed(show_heap_map_button);
	}
	
	public boolean isShowLargestSizeBtnDisp(){
		return this.isDisplayed(show_largest_size_button);
	}
	
	public void waitForPageLoadCompleted() {
		this.waitForPageLoadCompleted(search_button);
	}
	
	public void verifyStorageInfo(){
		
		this.testVerify.assertTrue(this.isSearchBtnDisp(), 
				"Monitor->Storage page Search button displayed!!!");
		
		this.testVerify.assertTrue(this.isSysObjectsBtnDisp(), 
				"Monitor->Storage page System objects button displayed!!!");
		
		this.testVerify.assertTrue(this.isRefreshBtnDisp(), 
				"Monitor->Storage page Refresh button displayed!!!");
		
		this.testVerify.assertTrue(this.isFilterOffBtnDisp(), 
				"Monitor->Storage page Filter off button displayed!!!");
		
		this.testVerify.assertTrue(this.isColumnBtnDisp(), 
				"Monitor->Storage page Column button displayed!!!");
		
		this.testVerify.assertTrue(this.isExportCVSBtnDisp(), 
				"Monitor->Storage page Export CVS button displayed!!!");
		
		this.testVerify.assertTrue(this.isSchemasBtnDisp(), 
				"Monitor->Storage page Schemas button displayed!!!");		
		this.testVerify.assertTrue(this.isShowHeapMapBtnDisp(), 
				"Monitor->Storage page Shwo Heap Map button displayed!!!");
		this.testVerify.assertTrue(this.isShowLargestSizeBtnDisp(), 
				"Monitor->Storage page Show Largest Size button displayed!!!");
		
	}

}
