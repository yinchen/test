package com.ibm.datatools.dsm.tests.ui.task.settings;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;

public class TKAbout extends TaskBase{

	public TKAbout(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}
	
	private final String about_page = "about_page";
	private final String details_section = "details_section";
	private final String plan_details_section = "plan_details_section";
	private final String database_connects_section = "database_connects_section";
	private final String plan_details_type_single_node ="plan_details_type_single_node";
	private final String plan_details_type_multi_node ="plan_details_type_multi_node";
	private final String plan_details_type_entry_node ="plan_details_type_entry_node";
	private final String about_ssl_checkbox="about_ssl_checkbox";
	private final String about_name ="about_name";
	
	
    public boolean isDetailsDisp() {
    	return this.isDisplayed(about_page, details_section);
    }
    
    public boolean isPlanDetailsDisp() {
    	return this.isDisplayed(about_page, plan_details_section);
    }
    
    public boolean isSingle_Node() {
    	return this.isDisplayed(about_page, plan_details_type_single_node);
    }
    
    public boolean isMulti_Node() {
    	return this.isDisplayed(about_page, plan_details_type_multi_node);
    }
    
    public boolean isEntry_Node() {
    	return this.isDisplayed(about_page, plan_details_type_entry_node);
    }
    
    
    
    public boolean isDBConnectionsDisp() {
    	return this.isDisplayed(about_page, database_connects_section);
    }
    
    public void waitForPageLoadCompleted(){
    	this.waitForPageLoadCompleted(about_page, about_name);
	}
	
	public void verifyAboutpage(){
		
		this.testVerify.assertTrue(this.isDetailsDisp(), "About page Details section displayed");
		this.testVerify.assertTrue(this.isPlanDetailsDisp(), "About page Plan Details section displayed");
		this.testVerify.assertTrue(this.isDBConnectionsDisp(), "About page Database Connections section displayed");
		
	}
	
	

}
