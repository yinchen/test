package com.ibm.datatools.dsm.tests.ui.task.settings;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;

public class TKManageUsers extends TaskBase{

	public TKManageUsers(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}
	
	private final String manage_users_page = "manage_users_page";	

	
	public boolean isAddButDisp(){
		return this.isDisplayed(manage_users_page, add_button);
	}
	
	public boolean isSearchButDisp(){
		return this.isDisplayed(manage_users_page, search_button);
	}
	
	public boolean isRefreshButDisp(){
		return this.isDisplayed(manage_users_page, refresh_button);
	}
	
	public void waitForPageLoadCompleted(){
		long timeBegins = System.currentTimeMillis();		
		do {
			WebDriverCore.pause(pauseTime);			
			if(this.isAddButDisp()){	
				break;
			}		
		} while (System.currentTimeMillis() - timeBegins <= timeOut * 1000);
	}
	
	public void verifyManageUserspage(){
		
		this.testVerify.assertTrue(this.isAddButDisp(), "Manage Users page Add button displayed");
		this.testVerify.assertTrue(this.isSearchButDisp(), "Manage Users page Search button displayed");
		this.testVerify.assertTrue(this.isRefreshButDisp(), "Manage Users page Refresh button displayed");
		
	}
	
	
	
	
	

}
