package com.ibm.datatools.dsm.tests.ui.task.settings;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;

public class TKMyProfile extends TaskBase{

	public TKMyProfile(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}
	
	private final String my_profile_page = "my_profile_page";	

	
	public boolean isUpdateButDisp(){
		return this.isDisplayed(my_profile_page, update_button);
	}
	

	
	public void waitForPageLoadCompleted(){
		long timeBegins = System.currentTimeMillis();		
		do {
			WebDriverCore.pause(pauseTime);			
			if(this.isUpdateButDisp()){	
				break;
			}		
		} while (System.currentTimeMillis() - timeBegins <= timeOut * 1000);
	}
	
	public void verifyMyProfilepage(){		
		this.testVerify.assertTrue(this.isUpdateButDisp(), "My Profile page Update button displayed");		
	}

}
