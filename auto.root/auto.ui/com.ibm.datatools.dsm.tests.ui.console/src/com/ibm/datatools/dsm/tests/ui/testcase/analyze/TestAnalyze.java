package com.ibm.datatools.dsm.tests.ui.testcase.analyze;

import org.testng.annotations.Test;


import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;

public class TestAnalyze extends TestCaseBase{

//	 /**
//	* Test Analyze page and verify
//	* 1. It has "Develop analytic and machine learning application" label with enabled 
//	* 2. It has "Data Science Experience" label with enabled 
//	* 3. It has "R and RStudio" label with enabled 
//	* 4. It has "Analytic APIs" label with enabled 
//	* 5. It has "Launch DSX" button with enabled 
//	* 6. It has "Launch RStudio" button with enabled 
//	* 7. It has "Explore the APIs" button with enabled 
//	*/
	
	@Test
	public void DevelopAnalyticsItemCheck() {
		TestLogger.logInfo("= = = = = = = = Start the test: Go to the about page... = = = = = = = =");
		tkMenu.selectMenuNoWaitingIcon("user->user_about");		
		tkAbout.waitForPageLoadCompleted();
		tkAbout.pause(1000);
		if(tkAbout.isSingle_Node() ||tkAbout.isMulti_Node() || tkAbout.isEntry_Node()) {
			tkMenu.selectMenuNoWaitingIcon("root");
			tkMenu.VerifyAnalyticsDisp();
			tkMenu.selectMenuNoWaitingIcon("root");
			
			testAnalyzePageInfo();
		}
		else {
			tkMenu.selectMenuNoWaitingIcon("root");
			tkMenu.VerifyAnalyticsNotDisp();
			tkMenu.selectMenuNoWaitingIcon("root");
		}		
	}
	
	
	
	public void testAnalyzePageInfo(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Analyze Page... = = = = = = = =");
		tkMenu.selectMenuNoWaitingIcon("root->develop_analytics");
		tkAnalyzePage.waitForPageLoadCompleted();
		tkAnalyzePage.verifyAnalyzePageInfo();
		TestLogger.logInfo("= = = = = = = = End the test: Test Analyze Page... = = = = = = = =");
		
	}

}
