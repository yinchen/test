package com.ibm.datatools.dsm.tests.ui.testcase.backupandrestore;

import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;

public class TestBackupAndRestore extends TestCaseBase{
	
//	 /**
//	* Test Backup and restore page and verify
//	* 1. It has "schedule backup" label with enabled 
//	* 2. It has "restore" label with enabled 
//	*/
	
	@Test
	public void testBackupAndRestorePageInfo(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Backup and restore Page... = = = = = = = =");
		tkMenu.selectMenuNoWaitingIcon("root->backupandrestore");
		tkBackupAndRestorePage.waitForPageLoadCompleted();
		tkBackupAndRestorePage.verifyBackupAndRestorePageInfo();
		TestLogger.logInfo("= = = = = = = = End the test: Test Backup and restore Page... = = = = = = = =");
		
	}
}
