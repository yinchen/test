package com.ibm.datatools.dsm.tests.ui.testcase.base;

import java.awt.AWTException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Configurator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.ibm.datatools.dsm.tests.ui.framework.base.Configuration;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverFactory;
import com.ibm.datatools.dsm.tests.ui.task.analyze.TKAnalyze;
import com.ibm.datatools.dsm.tests.ui.task.backupandrestore.TKBackupAndRestore;
import com.ibm.datatools.dsm.tests.ui.task.common.TKDialog;
import com.ibm.datatools.dsm.tests.ui.task.common.TKHeader;
import com.ibm.datatools.dsm.tests.ui.task.common.TKLogin;
import com.ibm.datatools.dsm.tests.ui.task.common.TKMenu;
import com.ibm.datatools.dsm.tests.ui.task.common.TKSSL;
import com.ibm.datatools.dsm.tests.ui.task.connection.TKConnectionInformation;
import com.ibm.datatools.dsm.tests.ui.task.connection.TKDownloadTools;
import com.ibm.datatools.dsm.tests.ui.task.explore.TKExplore;
import com.ibm.datatools.dsm.tests.ui.task.help.TKAskQuestion;
import com.ibm.datatools.dsm.tests.ui.task.help.TKContactSupport;
import com.ibm.datatools.dsm.tests.ui.task.help.TKDOCS;
import com.ibm.datatools.dsm.tests.ui.task.help.TKRestAPI;
import com.ibm.datatools.dsm.tests.ui.task.home.TKHome;
import com.ibm.datatools.dsm.tests.ui.task.load.TKLoad;
import com.ibm.datatools.dsm.tests.ui.task.monitor.TKConnections;
import com.ibm.datatools.dsm.tests.ui.task.monitor.TKInflightExecutions;
import com.ibm.datatools.dsm.tests.ui.task.monitor.TKStorage;
import com.ibm.datatools.dsm.tests.ui.task.runsql.TKRunSQL;
import com.ibm.datatools.dsm.tests.ui.task.settings.TKAbout;
import com.ibm.datatools.dsm.tests.ui.task.settings.TKManageUsers;
import com.ibm.datatools.dsm.tests.ui.task.settings.TKMyProfile;
import com.ibm.datatools.test.utils.Properties;
import com.ibm.datatools.test.utils.TestLogger;

public class TestCaseBase {
	
	//Initialize log4j configuration file
	static{		
		try {
			String resFile = Configuration.getLog4jConfigFilePath();
			File logConfig = new File(resFile);
			ConfigurationSource source = new ConfigurationSource(new FileInputStream(logConfig), logConfig);
			Configurator.initialize(null, source);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	private String headless = Properties.getConfigValue("headless");	
	private int waitTimeout = Integer.parseInt(Properties.getConfigValue("mte.pauseTime.ms"));
	private int scriptTimeout = Integer.parseInt(Properties.getConfigValue("mte.timeOut.second"));
	private int pageLoadTimeout = Integer.parseInt(Properties.getConfigValue("mte.loadTime.second"));
	private String dsmserver = Properties.getConfigValue("dsmserver");
	private int dsmport = Integer.parseInt(Properties.getConfigValue("dsmport"));
	private String prefix = Properties.getConfigValue("prefix");
	
	//For DSM users: admin user and normal user
	protected static String adminUser = Properties.getConfigValue("adminUser");
	protected static String adminPassword = Properties.getConfigValue("adminPassword");	
	protected static String loginUser = Properties.getConfigValue("LoginUser");
	protected static String loginPassword = Properties.getConfigValue("LoginPassword");
	protected static String currentUser = null;
	protected static String currentPassword = null;
	
	//For DSM REPO DB information
	
	
	//This is for web driver
	protected static WebDriverCore driverCore;
	
	//This is for Task layer
	//For Common
	protected TKLogin tkLoginPage;
	
	protected TKMenu tkMenu;	
	protected TKHeader tkHeader;
	protected TKDialog tkDialog;


	//For help
	protected TKContactSupport tkContactSupport;
	protected TKAskQuestion tkAskQuestion;
	protected TKDOCS tkDocs;
	protected TKRestAPI tkRestAPI;
	
	
	//For Home
	protected TKHome tkHomePage;
	
	//For Load
	protected TKLoad tkLoadPage;
		
	//For Monitor
	protected TKInflightExecutions tkInflightExecutions;
	protected TKConnections tkConnections;
	protected TKStorage tkStorage;



	//For Explore
	protected TKExplore tkExplorePage;
	
	//For Analyze
	protected TKAnalyze tkAnalyzePage;
	
	//For Tunning
	protected TKRunSQL tkRunSQLPage;

	//For Backup and Restore
	protected TKBackupAndRestore tkBackupAndRestorePage;
	
	//For SSL 
	protected TKSSL tkSSL;
	
	
	//For Connection
	protected TKConnectionInformation tkConnectionInformation;
	protected TKDownloadTools tkDownloadTools;
	
	//For Settings
	protected TKAbout tkAbout;
	protected TKManageUsers tkManageUsers;
	protected TKMyProfile tkMyProfiles;
	
	
	/*
	 * First level mune
	 */
	//For Home

	
	//For Admin

	
	//For RunSQL
	protected String runsql_page = "runsql";
	
	//For Monitor

	
	//For Jobs
	protected String jobs_page = "jobs";
	
	//For Optimize
	
	
	//For Settings

	
	//For help

		
	
	public TestCaseBase(){		
		//This is init for Web Driver 
		if(TestCaseBase.driverCore == null){
			TestCaseBase.driverCore = new WebDriverCore(initWebDriver());	
		}			
		//This is init for Task layer pages
		//For Common
		tkMenu = new TKMenu(driverCore);
		tkLoginPage = new TKLogin(driverCore);
		tkHeader = new TKHeader(driverCore);
		tkDialog = new TKDialog(driverCore);
		
		//For Home
		tkHomePage = new TKHome(driverCore);
		
		//For Load
		tkLoadPage = new TKLoad(driverCore);
		
		//For Monitor
		tkInflightExecutions = new TKInflightExecutions(driverCore);
		tkConnections = new TKConnections(driverCore);
		tkStorage = new TKStorage(driverCore);
		
		//For Explore
		tkExplorePage = new TKExplore(driverCore);
		
		//For Tunning
		tkRunSQLPage = new TKRunSQL(driverCore);
		
		//For Backup and Restore
		tkBackupAndRestorePage = new TKBackupAndRestore(driverCore);
		
		//For Analyze
		tkAnalyzePage = new TKAnalyze(driverCore);

		//For SSL
		tkSSL = new TKSSL(driverCore);
		
		//For Help 
		tkContactSupport = new TKContactSupport(driverCore);
		tkAskQuestion = new TKAskQuestion(driverCore);
		tkDocs = new TKDOCS(driverCore);
		tkRestAPI = new TKRestAPI(driverCore);
		
		//For Connection
		tkConnectionInformation = new TKConnectionInformation(driverCore);
		tkDownloadTools = new TKDownloadTools(driverCore);

		//for Settings
		tkAbout = new TKAbout(driverCore);
		tkManageUsers = new TKManageUsers(driverCore);
		tkMyProfiles = new TKMyProfile(driverCore);
		
		
		
	}
	

	@BeforeSuite
	public void start() throws Throwable {			
		
		//Pre-condition 2: 
		openURL();
		currentUser = adminUser;
		currentPassword = adminPassword;
		tkLoginPage.login(currentUser, currentPassword);
		
		//this.tkHeader.verifyUserMenu();
				
	}

	@AfterSuite
	public void quit() {
		this.tkHeader.logout(tkMenu, tkDialog);
		//closeBrowser();
	}
	
	

	public void openURL(String URL) {		
		driverCore.open(URL);	
	}
	
	public void openURL() throws AWTException {

		String URL = dsmserver + ":" + dsmport + "/" + prefix;

		if (URL.startsWith("https")) {

			try {
				driverCore.open(URL);
			} catch (Exception e) {
				// statements to handle any exceptions
				// logMyErrors(e); // pass exception object to error handler
			} finally {				
				//driverCore.pause(1000);
				this.tkSSL.addSecurityException();
				driverCore.waitForPageLoad();
			}
		} else {
			driverCore.open(URL);
			driverCore.waitForPageLoad();
		}

	}
	
	
	public void closeBrowser() {
		closeWebDriver();
	}

	

	public WebDriver initWebDriver(String driverType) {

		WebDriver driver = null;
		WebDriverFactory driverFactory = new WebDriverFactory();

		switch (driverType.trim()) {
		case "firefox":
			driver = driverFactory.getFirefoxDriver(headless);
			break;
		case "chrome":
			driver = driverFactory.getChromeDriver();
			break;
		case "ie":
			driver = driverFactory.getIEDriver();
			break;
		default:			
			driver = null;
			TestLogger.logWarn("We do not support this browser for selenium automation, please contact owner for details.");
		}
		
		
		if(driver != null){
			driver.manage().timeouts().pageLoadTimeout(pageLoadTimeout, TimeUnit.SECONDS);
			TestLogger.logDebug("set pageLoadTimeout : " + pageLoadTimeout);
			driver.manage().timeouts().implicitlyWait(waitTimeout, TimeUnit.SECONDS);
			TestLogger.logDebug("set waitTimeout : " + waitTimeout);
			driver.manage().timeouts().setScriptTimeout(scriptTimeout, TimeUnit.SECONDS);
			TestLogger.logDebug("set scriptTimeout : " + scriptTimeout);
		}
		
		return driver;
	}

	public WebDriver initWebDriver() {
		String driverType = Properties.getConfigValue("browser");
		return initWebDriver(driverType);
	}

	public void closeWebDriver() {	
		try{
			driverCore.getDriver().quit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	public java.util.Properties loadJSONPostData(String filename) throws FileNotFoundException, IOException {
		
		String filePath = Configuration.getAdminDataPath() + filename;
		java.util.Properties p = new java.util.Properties();	
		p.load(new FileInputStream(filePath));		
		return p;
	}
	
	public String getDSMServer() {
		return dsmserver;
	}
}
