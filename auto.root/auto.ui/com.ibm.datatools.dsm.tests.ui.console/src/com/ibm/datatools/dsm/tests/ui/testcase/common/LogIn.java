package com.ibm.datatools.dsm.tests.ui.testcase.common;



import org.junit.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;

public class LogIn extends TestCaseBase {
	
	@BeforeClass
	public void quit() {
		this.tkHeader.logout(tkMenu, tkDialog);
		closeBrowser();
	}
	

//	 /**
//	* Test Run SQL page and verify
//	* 1. It has "Run" menu with enabled 
//	* 2. It has "Script" menu with enabled 
//	* 3. It has "Edit" menu with enabled 
//	* 4. It has "Favorites" menu with enabled 
//	* 5. It has "Learn more" with enabled 
//	*/

	
	@Test
    public void testLogin() throws Throwable {			
		
		
		openURL();
		currentUser = adminUser;
		currentPassword = adminPassword;
		tkLoginPage.login(currentUser, currentPassword);
		
		//this.tkHeader.verifyUserMenu();
				
	}
	
	

}



