package com.ibm.datatools.dsm.tests.ui.testcase.connection;


import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;

public class TestConnection extends TestCaseBase{

	
	/**
	 * Test Connection Info page and verify
	 */
	
	
	
	@Test
	public void testConnectionInformationPage(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test Connection Info Page... = = = = = = = =");		
		tkMenu.selectMenuNoWaitingIcon("root->connection_info->connection_information");
		tkConnectionInformation.waitForPageLoadCompleted();
		tkConnectionInformation.verifyConnectionInfomationpage();
		TestLogger.logInfo("= = = = = = = = End the test: Test Connection Info Page... = = = = = = = =");	
		
	}
	
	@Test
	public void testDownloadToolsPage(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test Download tools Page... = = = = = = = =");		
		tkMenu.selectMenuNoWaitingIcon("root->connection_info->download_tools");
		tkDownloadTools.waitForPageLoadCompleted();
		tkDownloadTools.verifyDownloadToolsPage();
		TestLogger.logInfo("= = = = = = = = End the test: Test Download tools Page... = = = = = = = =");	
		
	}
	@AfterClass
	public void closeConnections() 
	{
		tkMenu.selectMenuNoWaitingIcon("root->connection_info");
	}
	
	
}
