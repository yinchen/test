package com.ibm.datatools.dsm.tests.ui.testcase.explore;

import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;

public class TestExplore extends TestCaseBase{
	
//	 /**
//	* Test Explore page and verify
//	* 1. It has "show system schemas" button with enabled 
//	* 2. It has "refresh" button with enabled 
//	* 3. It has "New Schema" button with enabled 
//	* 4. It has "Find a schema" input with enabled 
//	* 5. It has "Schema" label with enabled 
//	*/
	
	@Test
	public void testExplorePageInfo(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Explore Page... = = = = = = = =");
		tkMenu.selectMenuNoWaitingIcon("root->explore");
		tkExplorePage.waitForPageLoadCompleted();
		tkExplorePage.verifyExplorePageInfo();
		TestLogger.logInfo("= = = = = = = = End the test: Test Explore Page... = = = = = = = =");
		
	}

}
