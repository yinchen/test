package com.ibm.datatools.dsm.tests.ui.testcase.help;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;

public class TestHelp extends TestCaseBase{
	
	@Test
	public void TestContactSupportURL() {
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Contact Support Page URL... = = = = = = = =");
		tkMenu.selectMenuNoWaitingIcon("root->help->contactsupport");
		tkContactSupport.switchToLastWindow();
		tkContactSupport.waitForPageLoadCompleted();
		
		String url = tkContactSupport.getURL();
		tkContactSupport.verifyURL(url);
		
//		tkContactSupport.verifyHelpContactSupportPageInfo();
		tkContactSupport.closeCurrentWindow();
		//the following test case could continue to run
		tkContactSupport.switchToLastWindow();	
		TestLogger.logInfo("= = = = = = = = End the test: Test Contact Support Page URL.... = = = = = = = =");
	}
	
	@Test
	public void TestAskQuestionURL() {
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Ask A Question Page URL... = = = = = = = =");
		tkMenu.selectMenuNoWaitingIcon("root->help->askaquestion");
		tkAskQuestion.switchToLastWindow();
		tkAskQuestion.waitForPageLoadCompleted();
		
		String url = tkAskQuestion.getURL();
		tkAskQuestion.verifyURL(url);

//		tkAskQuestion.verifyHelpAskQuestionPageInfo();
		tkAskQuestion.closeCurrentWindow();	
		//the following test case could continue to run
		tkAskQuestion.switchToLastWindow();		
		TestLogger.logInfo("= = = = = = = = End the test: Test Ask A Question Page URL.... = = = = = = = =");
	}
	
	@Test
	public void TestDOCSURL() {
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test DOCS Page URL... = = = = = = = =");
		tkMenu.selectMenuNoWaitingIcon("root->help->docs");
		driverCore.switchToLastWindow();
		tkDocs.waitForPageLoadCompleted();
		
		String url = tkDocs.getURL();
		tkDocs.verifyURL(url);

//		tkDocs.verifyHelpDocsPageInfo();
		tkDocs.closeCurrentWindow();		
		//the following test case could continue to run
		tkDocs.switchToLastWindow();		
		TestLogger.logInfo("= = = = = = = = End the test: Test DOCS Page URL.... = = = = = = = =");
	}
	
	@Test
	public void TestRestAPIPage() {
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Rest API Page Info... = = = = = = = =");
		tkMenu.selectMenuNoWaitingIcon("root->help->rest_api");
		tkRestAPI.switchToLastWindow();
		tkRestAPI.waitForPageLoadCompleted();
		
		String url = tkRestAPI.getURL();
		tkRestAPI.verifyURL(url,this.getDSMServer());
		
//		tkRestAPI.verifyRestAPIInfo();
		tkRestAPI.closeCurrentWindow();	
		//the following test case could continue to run
		tkRestAPI.switchToLastWindow();		
		TestLogger.logInfo("= = = = = = = = End the test: Test Rest API Page Info.... = = = = = = = =");
	}
	
	@AfterMethod
	public void clear() {
		
		for(int i=this.driverCore.getDriver().getWindowHandles().size(); i>1; i--) {
			tkMenu.switchToLastWindow();
			tkMenu.closeCurrentWindow();
			tkMenu.switchToLastWindow();
		}	
		
		tkMenu.selectMenuNoWaitingIcon("root->help");
		
	}

}
