package com.ibm.datatools.dsm.tests.ui.testcase.home;

import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;

public class TestHome extends TestCaseBase {
	
	
//	 /**
//	* Test Home page and verify
//	* 1. It has "Quick stats" label with enabled 
//	* 2. It has "Connect to IBM Db2 Warehouse on Cloud" label with enabled 
//	* 3. It has "Load Data" button with enabled 
//	*/
	
	@Test
	public void testHomePageInfo(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Home Page... = = = = = = = =");
		tkMenu.selectMenuNoWaitingIcon("root->home");
		tkHomePage.waitForPageLoadCompleted();
		tkHomePage.verifyHomePageInfo();
		TestLogger.logInfo("= = = = = = = = End the test: Test Home Page... = = = = = = = =");
		
	}
}
