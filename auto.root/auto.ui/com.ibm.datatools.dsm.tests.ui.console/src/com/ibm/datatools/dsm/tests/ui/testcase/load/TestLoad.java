package com.ibm.datatools.dsm.tests.ui.testcase.load;

import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;

public class TestLoad extends TestCaseBase {
	
	
//	 /**
//	* Test Load page and verify
//	* 1. It has "Next" button with disabled 
//	* 2. It has "Lift" label with enabled 
//	* 3. It has "SoftLayer Swift" label with enabled 
//	* 4. It has "My computer" label with enabled 
//	*/
	
	@Test
	public void testLoadPageInfo(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Load Page... = = = = = = = =");
		tkMenu.selectMenuNoWaitingIcon("root->load");
		tkLoadPage.waitForPageLoadCompleted();
		tkLoadPage.verifyLoadPageInfo();
		TestLogger.logInfo("= = = = = = = = End the test: Test Load Page... = = = = = = = =");
		
	}
}
