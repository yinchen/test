package com.ibm.datatools.dsm.tests.ui.testcase.monitor;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;

public class TestMonitor extends TestCaseBase{
	
	
	@Test
	public void testInflightExecutionsPageInfo(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Monitor->In-flight exections Page... = = = = = = = =");
		tkMenu.selectMenuNoWaitingIcon("root->monitor->inflight_executions");
		tkInflightExecutions.waitForPageLoadCompleted();
		tkInflightExecutions.verifyInflightExecutionsInfo();
		TestLogger.logInfo("= = = = = = = = End the test: Test Monitor->In-flight exections Page... = = = = = = = =");
		
	}
	
	@Test
	public void testConnectionsPageInfo(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Monitor->Connections Page... = = = = = = = =");
		tkMenu.selectMenuNoWaitingIcon("root->monitor->connections");	
		tkConnections.waitForPageLoadCompleted();
		tkConnections.verifyConnectionsInfo();
		TestLogger.logInfo("= = = = = = = = End the test: Test Monitor->Connections Page... = = = = = = = =");
		
	}
	
	@Test
	public void testStoragePageInfo(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Monitor->Storage Page... = = = = = = = =");
		tkMenu.selectMenuNoWaitingIcon("root->monitor->storage");	
		tkStorage.waitForPageLoadCompleted();
		tkStorage.verifyStorageInfo();
		TestLogger.logInfo("= = = = = = = = End the test: Test Monitor->Storage Page... = = = = = = = =");
		
	}
	
	@AfterClass
	public void closeMonitor() {
		tkMenu.selectMenu("root->monitor");
	}

}
