package com.ibm.datatools.dsm.tests.ui.testcase.runsql;


import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;

public class TestRunSQL extends TestCaseBase {

	
	@BeforeClass
	public void beforetest(){
		
	}	
	

//	 /**
//	* Test Run SQL page and verify
//	* 1. It has "Run" menu with enabled 
//	* 2. It has "Script" menu with enabled 
//	* 3. It has "Edit" menu with enabled 
//	* 4. It has "Favorites" menu with enabled 
//	* 5. It has "Learn more" with enabled 
//	*/
	
	@Test
	public void testRunSQLPageInfo(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Run SQL Page... = = = = = = = =");
		tkMenu.selectMenuNoWaitingIcon("root->runsql");	
		tkRunSQLPage.waitForPageLoadCompleted();
		tkRunSQLPage.verifyRunSQLPageInfo();
		TestLogger.logInfo("= = = = = = = = End the test: Test Run SQL Page... = = = = = = = =");
		
	}
	
	

}



