package com.ibm.datatools.dsm.tests.ui.testcase.settings;


import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;

public class TestSettings extends TestCaseBase {	

	
	//@Test
	public void testAboutPage(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test About Page... = = = = = = = =");
		tkMenu.selectMenuNoWaitingIcon("root->settings->about");	
		tkAbout.waitForPageLoadCompleted();
		tkAbout.verifyAboutpage();
		TestLogger.logInfo("= = = = = = = = End the test: Test About Page... = = = = = = = =");
		
	}


	//@Test
	public void testManageUsersPage(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Manage Users Page... = = = = = = = =");
		tkMenu.selectMenuNoWaitingIcon("root->settings->manage_users");	
		tkManageUsers.waitForPageLoadCompleted();
		tkManageUsers.verifyManageUserspage();
		TestLogger.logInfo("= = = = = = = = End the test: Test Manage Users Page... = = = = = = = =");
		
	}
	
	@Test
	public void testMyProfilePage(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test My profile Page... = = = = = = = =");
		tkMenu.selectMenuNoWaitingIcon("root->settings->my_profile");	
		tkMyProfiles.waitForPageLoadCompleted();
		tkMyProfiles.verifyMyProfilepage();
		TestLogger.logInfo("= = = = = = = = End the test: Test My profile Page... = = = = = = = =");
		
	}
	
	@AfterClass
	public void closeSettings() {
		tkMenu.selectMenuNoWaitingIcon("root->settings");
	}
	

}



