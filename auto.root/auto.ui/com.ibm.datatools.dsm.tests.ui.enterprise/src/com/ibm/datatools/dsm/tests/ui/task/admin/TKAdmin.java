package com.ibm.datatools.dsm.tests.ui.task.admin;


import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;

public class TKAdmin extends TaskBase{

	public TKAdmin(WebDriverCore driverCore) {
		super(driverCore);
	}
	
	protected final String create_button = "create_button";
	protected final String revalidate_button = "revalidate_button";
	protected final String name_textbox = "name_textbox";	
	protected final String name_label = "name_label";
	protected final String columns_pane = "columns_pane";	
	protected final String addcolumn_button = "addcolumn_button";
	protected final String run_button = "run_button";
	protected final String trackChanges_button = "trackChanges_button";
	protected final String showSystemObjects_button = "showSystemObjects_button";
	protected final String start_button = "start_button";
	protected final String stop_button = "stop_button";
	protected final String quiesce_button = "quiesce_button";
	protected final String unquiesce_button = "unquiesce_button";
	protected final String createNewDatabase_button = "createNewDatabase_button";	
	protected final String configurationParameters_link = "configurationParameters_link";
	protected final String configureBLUAcceleration_link = "configureBLUAcceleration_link";	
	protected final String activate_button = "activate_button";
	protected final String deactivate_button = "deactivate_button";
	protected final String backup_button = "backup_button";
	protected final String restore_button = "restore_button";	
	protected final String recover_button = "recover_button";
	protected final String rollForward_button = "rollForward_button";
	protected final String configureLogging_button = "configureLogging_button";	
	protected final String privileges_button = "privileges_button";
	protected final String privilege_add_button = "privilege_add_button";
	protected final String privilege_remove_button = "privilege_remove_button";
	protected final String privilege_add_auth_dialog = "privilege_add_auth_dialog";
	protected final String addAuth_user_name_input = "addAuth_user_name_input";
	protected final String privilege_add_auth_dialog_ok_btn = "privilege_add_auth_dialog_ok_btn";
	protected final String privilege_auth_tab = "privilege_auth_tab";
	protected final String privilege_new_auth_tab = "privilege_new_auth_tab";
	protected final String result_status_column = "result_status_column";
	protected final String columns_link = "columns_link";
	protected final String length_textbox = "length_textbox";
	protected final String authorities_button = "authorities_button";
	protected final String generateDDL_button = "generateDDL_button";
	
	

}
