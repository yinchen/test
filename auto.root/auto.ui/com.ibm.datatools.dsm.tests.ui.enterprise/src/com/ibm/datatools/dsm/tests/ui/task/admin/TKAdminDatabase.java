package com.ibm.datatools.dsm.tests.ui.task.admin;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKAdminDatabase extends TKAdmin{	
	
	private final String admin_database_page = "admin_database_page";
	
	

	public TKAdminDatabase(WebDriverCore driverCore) {
		super(driverCore);
	}

	public boolean isActivateBtnDisplayed(){
		return this.isDisplayed(admin_database_page, activate_button);
	}
	
	public boolean isDeactivateBtnDisplayed(){
		return this.isDisplayed(admin_database_page, deactivate_button);
	}
	
	public boolean isBackupBtnDisplayed(){
		return this.isDisplayed(admin_database_page, backup_button);
	}
	
	public boolean isRestoreBtnDisplayed(){
		return this.isDisplayed(admin_database_page, restore_button);
	}
	
	public boolean isQuiesceBtnDisplayed(){
		return this.isDisplayed(admin_database_page, quiesce_button);
	}
	
	public boolean isUnquiesceBtnDisplayed(){
		return this.isDisplayed(admin_database_page, unquiesce_button);
	}
	
	public boolean isRollForwardBtnDisplayed(){
		return this.isDisplayed(admin_database_page, rollForward_button);
	}
	
	public boolean isRecoverBtnDisplayed(){
		return this.isDisplayed(admin_database_page, recover_button);
	}
	
	public boolean isConfigureLoggingBtnDisplayed(){
		return this.isDisplayed(admin_database_page, configureLogging_button);
	}
	
	public boolean isAuthoritiesBtnDisplayed(){
		return this.isDisplayed(admin_database_page, authorities_button);
	}
	
	public boolean isConfigurationParametersLinkDisplayed(){
		return this.isDisplayed(admin_database_page, configurationParameters_link);
	}
	
	public boolean isGenerateDDLBtnDisplayed(){
		return this.isDisplayed(admin_database_page, generateDDL_button);
	}
	
	public void verifyAdminDatabasePageInfo(){		
		
		//For button
		this.testVerify.assertTrue(this.isActivateBtnDisplayed(), 
				"Administer database page Activate button displays!!!");		
		this.testVerify.assertTrue(this.isDeactivateBtnDisplayed(), 
				"Administer database page Deactivate button displays!!!");		
		this.testVerify.assertTrue(this.isBackupBtnDisplayed(), 
				"Administer database page Backup button displays!!!");		
		this.testVerify.assertTrue(this.isRestoreBtnDisplayed(), 
				"Administer database page Restore button displays!!!");		
		this.testVerify.assertTrue(this.isQuiesceBtnDisplayed(), 
				"Administer database page Quiesce button displays!!!");		
		this.testVerify.assertTrue(this.isUnquiesceBtnDisplayed(), 
				"Administer database page Unquiesce button displays!!!");			
		/*this.testVerify.assertTrue(this.isRollForwardBtnDisplayed(),
				"Administer database page Roll Forward button displays!!!");	*/	
		
		if(this.isDisplayed(more_actions_button))
		{
			this.testVerify.assertTrue(this.isDisplayed(more_actions_button), 
					"Administer database page More actions button displays!!!");
		}
		else
		{		
			this.testVerify.assertTrue(this.isRecoverBtnDisplayed(), 
					"Administer database page Recover button displays!!!");		
			this.testVerify.assertTrue(this.isConfigureLoggingBtnDisplayed(),
					"Administer database page Configure Logging button displays!!!");		
			this.testVerify.assertTrue(this.isAuthoritiesBtnDisplayed(),
					"Administer database page Authorities button displays!!!");
			this.testVerify.assertTrue(this.isGenerateDDLBtnDisplayed(),
					"Administer database page Gernerate DDL button displays!!!");	
		}
		
		//For Link
		this.testVerify.assertTrue(this.isConfigurationParametersLinkDisplayed(),
				"Administer database page Configuration parameters link displays!!!");
	}
	
}




















