package com.ibm.datatools.dsm.tests.ui.task.admin;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKAdminIndexes extends TKAdmin{
	
	private final String admin_indexes_page = "admin_indexes_page";
	
	public TKAdminIndexes(WebDriverCore driverCore) {
		super(driverCore);
	}
	
	public boolean isCreateBtnDisplayed(){
		return this.isDisplayed(admin_indexes_page, create_button);
	}
	
	public boolean isTrackChangesBtnDisplayed(){
		return this.isDisplayed(admin_indexes_page, trackChanges_button);
	}
	
	public boolean isShowSystemObjectsBtnDisplayed(){
		return this.isDisplayed(admin_indexes_page, showSystemObjects_button);
	}
	
	
	public void verifyIndexesPageInfo(){
		this.waitForPageLoadCompleted(admin_indexes_page, create_button);
		this.testVerify.assertTrue(this.isCreateBtnDisplayed(), "Create button displays!!!");		
		this.testVerify.assertTrue(this.isTrackChangesBtnDisplayed(), "Track Changes button displays!!!");		
		//this.testVerify.assertTrue(this.isShowSystemObjectsBtnDisplayed(), "Show System Objects button displays!!!");
		
	}
	
}
