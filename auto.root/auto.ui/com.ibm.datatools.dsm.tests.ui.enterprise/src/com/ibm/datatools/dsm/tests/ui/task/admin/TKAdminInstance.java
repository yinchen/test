package com.ibm.datatools.dsm.tests.ui.task.admin;


import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;


public class TKAdminInstance extends TKAdmin{
	
	private final String admin_instance_page = "admin_instance_page";	

	public TKAdminInstance(WebDriverCore driverCore) {
		super(driverCore);
	}	
	
	public boolean isStartBtnDisplayed(){
		return this.isDisplayed(admin_instance_page, start_button);
	}
	
	public boolean isStopBtnDisplayed(){
		return this.isDisplayed(admin_instance_page, stop_button);
	}
	
	public boolean isQuiesceBtnDisplayed(){
		return this.isDisplayed(admin_instance_page, quiesce_button);
	}
	
	public boolean isUnquiesceBtnDisplayed(){
		return this.isDisplayed(admin_instance_page, unquiesce_button);
	}
	
	public boolean isCreatNewDatabaseBtnDisplayed(){
		return this.isDisplayed(admin_instance_page, createNewDatabase_button);
	}
	
	public boolean isConfigurationParametersLinkDisplayed(){
		return this.isDisplayed(admin_instance_page, configurationParameters_link);
	}
	
	public boolean isConfigureBLUAccelerationLinkDisplayed(){
		return this.isDisplayed(admin_instance_page, configureBLUAcceleration_link);
	}
	
	
	public void verifyAdminInstancePageInfo(){
		this.waitForPageLoadCompleted(admin_instance_page, start_button);
		
		this.testVerify.assertTrue(this.isStartBtnDisplayed(), 
				"Administer instance page Start button displays!!!");			
		this.testVerify.assertTrue(this.isStopBtnDisplayed(), 
				"Administer instance page Activate button displays!!!");			
		this.testVerify.assertTrue(this.isQuiesceBtnDisplayed(), 
				"Administer instance page Stop button displays!!!");			
		this.testVerify.assertTrue(this.isUnquiesceBtnDisplayed(), 
				"Administer instance page Unquiesce button displays!!!");		
		this.testVerify.assertTrue(this.isCreatNewDatabaseBtnDisplayed(), 
				"Administer instance page Creat New Database button displays!!!");		
		this.testVerify.assertTrue(this.isConfigurationParametersLinkDisplayed(), 
				"Administer instance page Configuration Parameters Link displays!!!");		
		this.testVerify.assertTrue(this.isConfigureBLUAccelerationLinkDisplayed(), 
				"Administer instance page Configure BLU Acceleration Link displays!!!");	
		
	}
}






