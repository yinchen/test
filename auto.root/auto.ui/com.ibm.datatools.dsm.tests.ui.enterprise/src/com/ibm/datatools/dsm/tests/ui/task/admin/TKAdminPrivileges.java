package com.ibm.datatools.dsm.tests.ui.task.admin;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKAdminPrivileges extends TKAdmin{
	
	private final String admin_privilege_page = "admin_privilege_page";
	

	public TKAdminPrivileges(WebDriverCore driverCore) {
		super(driverCore);
	}
	
	public boolean isAddBtnDisplayed(){
		return this.isDisplayed(admin_privilege_page, privilege_add_button);
	}
	
	public boolean isRemoveBtnDisplayed(){
		return this.isDisplayed(admin_privilege_page, privilege_remove_button);
	}
	
	public boolean isAddAuthDialogDisplayed(){
		return this.isDisplayed(privilege_add_auth_dialog);
	}
	
	public void verifyOKBtnClickAction(){
		this.clickBtn(privilege_add_button);
		this.testVerify.assertTrue(this.isAddAuthDialogDisplayed(), "Add auth dialog displays!!!");
		this.testVerify.assertTrue(!this.isBtnEnabled(privilege_add_auth_dialog, privilege_add_auth_dialog_ok_btn),"OK button in add auth is disabled");
		this.setTextBox(privilege_add_auth_dialog, addAuth_user_name_input, "TEST_USER");
		this.testVerify.assertTrue(this.isBtnEnabled(privilege_add_auth_dialog, privilege_add_auth_dialog_ok_btn),"Now OK button in add auth is enabled");
		this.clickBtn(privilege_add_auth_dialog, privilege_add_auth_dialog_ok_btn);
		this.testVerify.assertTrue(this.isDisplayed(privilege_auth_tab),"Auth tab is displayed");
		this.testVerify.assertTrue(this.isDisplayed(privilege_new_auth_tab),"New auth tab is displayed");
	}
	
	public void verifyPrivilegesInfo(){
		this.testVerify.assertTrue(this.isAddBtnDisplayed(), "Add button displays!!!");		
//		this.testVerify.assertTrue(this.isRemoveBtnDisplayed(), "Remove button displays!!!");		
		this.verifyOKBtnClickAction();		
		
	}
	
}
