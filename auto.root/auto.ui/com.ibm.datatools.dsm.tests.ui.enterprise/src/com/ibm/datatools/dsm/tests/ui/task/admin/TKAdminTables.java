package com.ibm.datatools.dsm.tests.ui.task.admin;


import com.ibm.datatools.dsm.tests.ui.framework.base.Connections;
import com.ibm.datatools.dsm.tests.ui.framework.base.DBUtils;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.task.common.TKDialog;

import net.sf.json.JSONObject;

public class TKAdminTables extends TKAdmin{

	private final String admin_tables_page = "admin_tables_page";	
	
	public TKAdminTables(WebDriverCore driverCore) {
		super(driverCore);
	}	
	
	public boolean isCreateBtnDisplayed(){
		return this.isDisplayed(admin_tables_page, create_button);
	}
	
	public boolean isTrackChangesBtnDisplayed(){
		//return this.isDisplayed(admin_tables_page, trackChanges_button);
		return this.isDisplayed(trackChanges_button);
	}
	
	public boolean isShowSystemObjectsBtnDisplayed(){
		return this.isDisplayed(admin_tables_page, showSystemObjects_button);
	}
	
	
	public void verifyTablesPageInfo(){		
		this.testVerify.assertTrue(this.isCreateBtnDisplayed(), "Create button displays!!!");		
		this.testVerify.assertTrue(this.isTrackChangesBtnDisplayed(), "Track Changes button displays!!!");		
		//this.testVerify.assertTrue(this.isShowSystemObjectsBtnDisplayed(), "Show System Objects button displays!!!");
	}
	
	public void clickCreateBtn(){		
		this.clickBtn(admin_tables_page, create_button);
		driverCore.waitForPageLoad();
		WebDriverCore.pause();
	}
	
	public void setName(String s){	
		long timeBegins = System.currentTimeMillis();		
		do {			
			this.pause();
			if(this.isNameDisplay()){				
				break;
			}		
		} while (System.currentTimeMillis() - timeBegins <= timeOut * 1000);			
		
		this.setTextBox(admin_tables_page, name_textbox, s);
	}
		
	public void clickColumnsPane(){
		
		long timeBegins = System.currentTimeMillis();		
		do {			
			this.pause();
			if(this.isDisplayed(admin_tables_page, columns_pane)){				
				break;
			}		
		} while (System.currentTimeMillis() - timeBegins <= timeOut * 1000);
		this.clickBtn(admin_tables_page, columns_pane);	
	}
	
	public void clickAddColumnBtn(){
		this.clickBtn(admin_tables_page, addcolumn_button);
	}
	
	public void clickRunBtn(){		
		this.clickBtn(admin_tables_page, run_button);
	}
	
	public void clickColumnsLink(){
		this.clickLink(admin_tables_page, columns_link);
	}
	
	public String getLastStatus(){
		return this.getStaticText(admin_tables_page, result_status_column+";0;1");
	}
	
	public boolean isNameDisplay() {
		return this.isDisplayed(admin_tables_page, name_label);
	}
	
	/*The DSM code change, so comment the code
	public void waitForComplete(){
		
		boolean waitFlag = true;		
		while(waitFlag){
			if(this.getLastStatus().indexOf("Succeeded")!=-1 || this.getLastStatus().indexOf("Failed")!=-1)
				waitFlag = false;
		}
	}	

	public void verifyResultStatus(String expected){
		this.testVerify.assertContained(expected,this.getLastStatus(),"the result status is "+expected+"!!!");
	}
	*/
	
	
	
	
	public void createTable(Object inputPara, String dbProfile, TKDialog tkDialog){		
		
		String schemaName;
		String tableName;		
		
		JSONObject obj = JSONObject.fromObject(inputPara);	
		
		this.clickCreateBtn();
		
		//For Properties Pane
		if(obj.containsKey("Properties")){
			//System.out.println(obj.getJSONObject("Properties").get("Organization"));
			JSONObject objProperties = obj.getJSONObject("Properties");
			
			if(objProperties.containsKey("Name")){				
				tableName = objProperties.getString("Name");					
					
				//this.setName(tableName);
				
				//drop the table before, will update the code later
				/*new DBUtils("9.125.138.22", 50000, "selenium", "db2inst1", "N1cetest").
				executeUpdate("drop table "+tableName, null);	*/				
				
			try {					
				new DBUtils(Connections.getConnectionInfo(dbProfile).get("host"),
						Integer.valueOf(Connections.getConnectionInfo(dbProfile).get("port")),
						Connections.getConnectionInfo(dbProfile).get("databaseName"),
						Connections.getConnectionInfo(dbProfile).get("user"),
						Connections.getConnectionInfo(dbProfile).get("password")).
						executeUpdate("drop table TABLE", null);
				new DBUtils(Connections.getConnectionInfo(dbProfile).get("host"),
						Integer.valueOf(Connections.getConnectionInfo(dbProfile).get("port")),
						Connections.getConnectionInfo(dbProfile).get("databaseName"),
						Connections.getConnectionInfo(dbProfile).get("user"),
						Connections.getConnectionInfo(dbProfile).get("password")).
						executeUpdate("drop table "+tableName, null);
				
				System.out.println("host is:"+Connections.getConnectionInfo(dbProfile).get("host"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
				
		}
		
		//For Columns Pane
		if(obj.containsKey("Columns")){
			
			this.pause();
			this.clickColumnsPane();
			JSONObject columns = obj.getJSONObject("Columns");
						
			for(int i=1; i<=columns.size();i++){			
				//In progress
				this.clickAddColumnBtn();
				if(columns.getJSONObject("Column"+String.valueOf(i)).containsKey("Name")){
				       this.setName(columns.getJSONObject("Column"+String.valueOf(i)).getString("Name"));		      
				      
				 }
				//this.setName("");	
				
				//workaround 
				//this.clickTextBox(admin_tables_page, length_textbox);				
				this.clickColumnsLink();				
			}
		}			
		
		//WebDriverCore.pause(2);
		
		this.clickRunBtn();
		//this.waitForComplete();
		//this.verifyResultStatus("Succeeded");	
		tkDialog.waitForNewDialogPopup();
		
		String dialogTitle = tkDialog.getTitle();		
		tkDialog.clickClosebtnonInformationDialog();
		
		
		//Verify the pop up dialog title 
		testVerify.assertEquals(dialogTitle, "Success", "The execution result status is successful");
		
	}

  }
}
}
