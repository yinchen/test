package com.ibm.datatools.dsm.tests.ui.task.admin;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKAdminTriggers extends TKAdmin{
	
	private final String admin_triggers_page = "admin_triggers_page";
	
	public TKAdminTriggers(WebDriverCore driverCore) {
		super(driverCore);
		
	}
	
	public boolean isCreateBtnDisplayed(){
		return this.isDisplayed(admin_triggers_page, create_button);
	}
	
	public boolean isTrackChangesBtnDisplayed(){
		//return this.isDisplayed(admin_tables_page, trackChanges_button);
		return this.isDisplayed(admin_triggers_page,trackChanges_button);
	}
	
	public boolean isRevalidateBtnDisplayed(){
		//return this.isDisplayed(admin_tables_page, trackChanges_button);
		return this.isDisplayed(admin_triggers_page,revalidate_button);
	}

	public void verifyTriggersInfo(){
		this.waitForPageLoadCompleted(admin_triggers_page, create_button);
		this.testVerify.assertTrue(this.isCreateBtnDisplayed(), "Create button displays!!!");
		this.testVerify.assertTrue(this.isTrackChangesBtnDisplayed(), "Track Changes button displays!!!");
		this.testVerify.assertTrue(this.isRevalidateBtnDisplayed(), "Revalidate button displays!!!");
	}

}
