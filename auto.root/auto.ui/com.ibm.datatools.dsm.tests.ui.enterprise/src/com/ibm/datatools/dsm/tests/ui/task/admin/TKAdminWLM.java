package com.ibm.datatools.dsm.tests.ui.task.admin;


import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKAdminWLM extends TKAdmin{

	private final String admin_tables_page = "admin_tables_page";
	
	
	public TKAdminWLM(WebDriverCore driverCore) {
		super(driverCore);
	}	
	
	public boolean isAddBtnDisplayed(){
		return this.isDisplayed(admin_tables_page, add_button);
	}
	
	public boolean isTrackChangesBtnDisplayed(){
		//return this.isDisplayed(admin_tables_page, trackChanges_button);
		return this.isDisplayed(trackChanges_button);
	}
	
	public boolean isShowSystemObjectsBtnDisplayed(){
		return this.isDisplayed(admin_tables_page, showSystemObjects_button);
	}
	
	
	public void verifyBtnExists(String btnName){		
		this.testVerify.assertTrue(isDisplayed(admin_tables_page, btnName), "Add button displays!!!");		
	}
	
	
	
	public void clickCreateBtn(){		
		this.clickBtn(admin_tables_page, create_button);
		driverCore.waitForPageLoad();
		WebDriverCore.pause();
	}
	
	public void setName(String s){	
		this.setTextBox(admin_tables_page, name_textbox, s);
	}
		
	public void clickColumnsPane(){
		this.clickBtn(admin_tables_page, columns_pane);	
	}
	
	public void clickAddColumnBtn(){
		this.clickBtn(admin_tables_page, addcolumn_button);
	}
	
	public void clickRunBtn(){
		this.clickBtn(admin_tables_page, run_button);
	}
	
	public void clickColumnsLink(){
		this.clickLink(admin_tables_page, columns_link);
	}
	
	public String getLastStatus(){
		return this.getStaticText(admin_tables_page, result_status_column+";0;1");
	}
	

	
	
	
}
