package com.ibm.datatools.dsm.tests.ui.task.base;


import com.ibm.datatools.dsm.tests.ui.framework.core.Action;
import com.ibm.datatools.dsm.tests.ui.framework.core.ObjectBase;
import com.ibm.datatools.dsm.tests.ui.framework.core.TestVerification;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.framework.widget.WebWidget;
import com.ibm.datatools.test.utils.Properties;
import com.ibm.datatools.test.utils.TestLogger;

public class TaskBase {
	
	/**
	 * The call order is TaskBase (4 functions) -> Action (2 functions) -> Widget (1 function)
	 */
	protected static int pauseTime = Integer.parseInt(Properties.getConfigValue("mte.pauseTime.ms"));
	protected static int timeOut = Integer.parseInt(Properties.getConfigValue("mte.timeOut.second"));	
	protected static int loadTime = Integer.parseInt(Properties.getConfigValue("mte.loadTime.second"));
	
	protected static WebDriverCore driverCore;		
	protected TestVerification testVerify;
	protected Action action;
	
	protected String split = Properties.getConfigValue("split.character");
	
    //For Common Elements
	protected final String run_button = "run_button";
	protected final String cancel_button = "cancel_button";
	protected final String add_button = "add_button";
	protected final String edit_button = "edit_button";
	protected final String delete_button = "delete_button";
	protected final String send_button = "send_button";
	protected final String ok_button = "ok_button";
	protected final String add_user_button = "add_user_button";
	protected final String update_settings_button = "update_settings_button";
	protected final String save_button = "save_button";
	protected final String close_button = "close_button";
	protected final String focused_dialog = "focused_dialog";
	protected final String standby_icon = "standby_icon";
	protected final String big_standby_icon = "big_standby_icon";
	protected final String more_actions_button = "more_actions_button";
	protected final String welcome_minimum_icon = "welcome_minimum_icon";
	protected final String information_close_button ="information_close_button";
	
	//This is for Object layer		
	protected ObjectBase objBase;
	
	public TaskBase(WebDriverCore driverCore){
		
		if(TaskBase.driverCore == null)
			TaskBase.driverCore = driverCore;		
		//This is init for Object layer pages	
		objBase = new ObjectBase(driverCore);
		
		this.testVerify = new TestVerification(driverCore);
		this.action = new Action(objBase);	
		
	}	
	//For Link	
	public void clickLink(String parent, String sLinkName){
		action.clickForLink(parent, sLinkName);
	}	
	
	public void clickLink(String sLinkName){
		action.clickForLink(null, sLinkName);
	}
	
	public void clickLink(int i, String parent, String sLinkName){
		action.clickForLink(i, parent, sLinkName);
	}
	
	public void clickLink(int i, String sLinkName){
		action.clickForLink(i, null, sLinkName);
	}	
	
	
	//For TextBox
	public void setTextBox(String parent, String sTextBoxName,  String sInputText){		
		action.setForTextBox(parent, sTextBoxName, sInputText);
	}
	
	public void setTextBox(String sTextBoxName,  String sInputText){
		action.setForTextBox(null, sTextBoxName, sInputText);
	}
	
	public void setTextBox(int i, String parent, String sTextBoxName,  String sInputText){		
		action.setForTextBox(i, parent, sTextBoxName, sInputText);
	}
	
	public void setTextBox(int i, String sTextBoxName,  String sInputText){		
		action.setForTextBox(i, null, sTextBoxName, sInputText);
	}
	
	
	
	
	//For TextBox	
	public void clickTextBox(String parent, String sTextBoxName){
		action.clickForTextBox(parent, sTextBoxName);
	}
	
	public void clickTextBox(String sTextBoxName){
		action.clickForTextBox(null, sTextBoxName);
	}
	
	public void clickTextBox(int i, String parent, String sTextBoxName){
		action.clickForTextBox(i, parent, sTextBoxName);
	}
	
	public void clickTextBox(int i, String sTextBoxName){
		action.clickForTextBox(i, null, sTextBoxName);
	}
	
	
	//For Button	
	public void clickBtn(String parent, String sButtonName){
		action.clickForButton(parent, sButtonName);
	}	
	
	public void clickBtn(String sButtonName){
		action.clickForButton(null, sButtonName);
	}
	
	public void clickBtn(int i, String parent, String sButtonName){
		action.clickForButton(i, parent, sButtonName);
	}	
	
	public void clickBtn(int i, String sButtonName){
		action.clickForButton(i, null, sButtonName);
	}		
	
	
	//For Button
	public boolean isBtnEnabled(String parent, String sButtonName){
		return action.isBtnEnabled(parent, sButtonName);
	}
	
	public boolean isBtnEnabled(String sButtonName){
		return action.isBtnEnabled(null, sButtonName);
	}
	
	public boolean isBtnEnabled(int i, String parent, String sButtonName){
		return action.isBtnEnabled(i, parent, sButtonName);
	}
	
	public boolean isBtnEnabled(int i, String sButtonName){
		return action.isBtnEnabled(i, null, sButtonName);
	}
	
	//For MenuItem		
	public void clickMenuItem(String parent, String sMenuItemName){
		action.clickForMenuItem(parent, sMenuItemName);		
	}
	
	public void clickMenuItem(String sMenuItemName){
		action.clickForMenuItem(null, sMenuItemName);
	}
	
	public void clickMenuItem(int i, String parent, String sMenuItemName){
		action.clickForMenuItem(i, parent, sMenuItemName);		
	}
	
	public void clickMenuItem(int i, String sMenuItemName){
		action.clickForMenuItem(i, null, sMenuItemName);
	}
	
	//For List	(No parameter int i)
	public void selectList(String parent, String sListName, String selectName){
		action.selectForList(parent, sListName, selectName);		
	}
	
	public void selectList(String sListName, String selectName){
		action.selectForList(null, sListName, selectName);		
	}	
	
	
	
	//For WebWidget
	public boolean isDisplayed(String parent, String sWebWidgetName){
		return action.isDisplayed(parent, sWebWidgetName);
	}
		
	public boolean isDisplayed(String sWebWidgetName){
		return action.isDisplayed(null, sWebWidgetName);
	}
	
	public boolean isDisplayed(int i, String parent, String sWebWidgetName){
		return action.isDisplayed(i, parent, sWebWidgetName);
	}
		
	public boolean isDisplayed(int i, String sWebWidgetName){
		return action.isDisplayed(i, null, sWebWidgetName);
	}
	
	
	//For WebWidget (No parameter int i)
	public int getSize(String parent, String sWebWidgetName){
		return action.getSize(parent, sWebWidgetName);
	}
	
	public int getSize(String sWebWidgetName){
		return action.getSize(null, sWebWidgetName);
	}
	
	//For WebWidget (No parameter int i)
	public void scrolledIntoView(String parent, String sWebWidgetName){
		action.scrolledIntoView(parent, sWebWidgetName);
	}
	
	
	public void scrolledIntoView(String sWebWidgetName){
		action.scrolledIntoView(null, sWebWidgetName);
	}
	
	//For WebWidget
	public void contextClick(String parent, String sWebWidgetName){
		action.contextClick(parent, sWebWidgetName);
	}
	
	public void contextClick(String sWebWidgetName){
		action.contextClick(null, sWebWidgetName);
	}
	
	public void contextClick(int i, String parent, String sWebWidgetName){
		action.contextClick(i, parent, sWebWidgetName);
	}
	
	public void contextClick(int i, String sWebWidgetName){
		action.contextClick(i, null, sWebWidgetName);
	}
	
	//For WebWidget
	public void moveToElement(String parent, String sWebWidgetName){
		action.moveToElement(parent, sWebWidgetName);
	}
	
	public void moveToElement(String sWebWidgetName){
		action.moveToElement(null, sWebWidgetName);
	}
	
	public void moveToElement(int i, String parent, String sWebWidgetName){
		action.moveToElement(i, parent, sWebWidgetName);
	}
	
	public void moveToElement(int i, String sWebWidgetName){
		action.moveToElement(i, null, sWebWidgetName);
	}
	
	
	//For Dialog	
	public String getDialogTitle(String parent, String sDialogName){
		return action.getDialogTitle(parent, sDialogName);
	}
	
	public String getDialogTitle(String sDialogName){
		return action.getDialogTitle(null, sDialogName);
	}
	
	public String getDialogTitle(int i, String parent, String sDialogName){
		return action.getDialogTitle(i, parent, sDialogName);
	}
	
	public String getDialogTitle(int i, String sDialogName){
		return action.getDialogTitle(i, null, sDialogName);
	}	
	

	
	//For StaticTextBox	
	public String getStaticText(String parent, String sStaticTextName){
		return action.getStaticText(parent, sStaticTextName);
	}
	
	public String getStaticText(String sStaticTextName){
		return action.getStaticText(null, sStaticTextName);
	}
	
	public String getStaticText(int i, String parent, String sStaticTextName){
		return action.getStaticText(i, parent, sStaticTextName);
	}
	
	public String getStaticText(int i, String sStaticTextName){
		return action.getStaticText(i, null, sStaticTextName);
	}
	

	
	//For Radio Button	
	public void selectRadioButton(String parent, String sRadioButtonName){
		action.selectRadioButton(parent, sRadioButtonName);
	}
	
	public void selectRadioButton(String sRadioButtonName){
		action.selectRadioButton(null, sRadioButtonName);
	}
	
	public void selectRadioButton(int i, String parent, String sRadioButtonName){
		action.selectRadioButton(i, parent, sRadioButtonName);
	}
	
	public void selectRadioButton(int i, String sRadioButtonName){
		action.selectRadioButton(i, null, sRadioButtonName);
	}
	
	//For CheckBox
	public void checkCheckBox(String parent, String sRadioButtonName){
		action.checkCheckBox(parent, sRadioButtonName);
	}
	
	public void checkCheckBox(String sRadioButtonName){
		action.checkCheckBox(null, sRadioButtonName);
	}
	
	public void checkCheckBox(int i, String parent, String sRadioButtonName){
		action.checkCheckBox(i, parent, sRadioButtonName);
	}
	
	public void checkCheckBox(int i, String sRadioButtonName){
		action.checkCheckBox(i, null, sRadioButtonName);
	}
	
	//For Slider
	public void dragSlider(String parent,String sSliderName,int movement){
		action.dragTimeSlider(parent, sSliderName, movement);
	}
	
	public void selectOneCheckBox(String sRadioButtonName,int i){
		action.selectOneCheckBox(null, sRadioButtonName,i);
	}
	
	//Wait for loading, small standby icon
	public void waitForStandByIconDisappear(){
		
		long timeBegins = System.currentTimeMillis();
		do {	
			this.driverCore.pause(pauseTime);
			if(!this.isDisplayed(standby_icon))
				break;		
		} while (System.currentTimeMillis() - timeBegins <= loadTime * 1000);		
	}
	
	//Wait for loading, big standby icon
	public void waitForConsoleLoading(){
		//wait for big standby icon displays 
		long timeBegins = System.currentTimeMillis();
		do {	
			this.driverCore.pause(pauseTime);
			if(this.isDisplayed(big_standby_icon))
				break;		
		} while (System.currentTimeMillis() - timeBegins <= loadTime * 1000);
		
		//wait for big standby icon disappear
		timeBegins = System.currentTimeMillis();
		do {	
			this.driverCore.pause(pauseTime);
			if(!this.isDisplayed(big_standby_icon))
				break;		
		} while (System.currentTimeMillis() - timeBegins <= loadTime * 1000);
	}
	
	//Switch to last window
	public void switchToLastWindow(){
		TestLogger.logInfo("Switch to the last window.");
		this.driverCore.switchToLastWindow();
	}
	
	//close the current window
	public void closeCurrentWindow(){
		TestLogger.logInfo("Close the current window.");
		this.driverCore.close();
	}
	
	public void pause(){
		TestLogger.logInfo("Pause...");
		this.driverCore.pause();
	}
	
	public void pause(long ms){
		TestLogger.logInfo("Pause...");
		this.driverCore.pause(ms);
	}
	
	public void switchFrametoParentFrame(){
		TestLogger.logInfo("Switch the frame to the parent one.");
		this.driverCore.getDriver().switchTo().parentFrame();
	}
	
	public void switchFrametoTargetbyID(String frameID){
		TestLogger.logInfo("Switch the frame to the specific one by frameID: "+frameID);
			this.driverCore.getDriver().switchTo().frame(frameID);
	}
	
	public void switchFrametoTargetbyName(String frameName){
		TestLogger.logInfo("Switch the frame to the specific one by frameName:"+frameName);
			this.driverCore.getDriver().switchTo().frame(frameName);
	}
	
	public void switchFrametoTargetbyWebElement(String parent, String children){
		TestLogger.logInfo("Switch the frame to the specific one by WebElement.");
		WebWidget temp = new WebWidget(this.driverCore, parent, children);
		this.driverCore.getDriver().switchTo().frame(temp.getElements().get(0));
		
	}
	
	public void switchFrametoTargetbyWebElement(String children){
		switchFrametoTargetbyWebElement(null, children);
		
	}
	
	
	public void waitForPageLoadCompleted(String parent, String children){		
		long timeBegins = System.currentTimeMillis();		
		do {
			WebDriverCore.pause(pauseTime);			
			if(this.isDisplayed(parent, children)){	
				break;
			}		
		} while (System.currentTimeMillis() - timeBegins <= timeOut * 1000);
	}
	
	public void waitForPageLoadCompleted(String children) {
		this.waitForPageLoadCompleted(null, children);
	}
	
	
	
	
	
	
	
	
	
	

}
