package com.ibm.datatools.dsm.tests.ui.task.common;



import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKHeader extends TaskBase {
	
	private final String header_section="header_section";
			
	private final String header_logout_button = "header_logout_button";
	private final String header_dbPicker_textbox = "header_dbPicker_textbox";
	private final String header_dbPicker_list = "header_dbPicker_list";

	
	public TKHeader(WebDriverCore driverCore) {
		super(driverCore);
	}
	
	
	private void setDBTextBox(String s){
		this.setTextBox(header_section, header_dbPicker_textbox, s);		
	}
	
	private void selectDB(String s){
		this.selectList(header_section, header_dbPicker_list, s);
	}
	
	public void pickDB(String s){
		//sleep 1s due to failure sometimes
		WebDriverCore.pause(1);
		this.setDBTextBox(s);
		WebDriverCore.pause(1);
		this.selectDB(s);
	}	
	
	public void logout(TKDialog dialog){
		this.clickBtn(header_section, header_logout_button);
		dialog.clickOKbtnonFocusedDialog();
		//TaskBase.driverCore.waitForPageLoad();
	}
	
	public void verifyLogOutBtn(){
		this.waitForPageLoadCompleted(header_section, header_logout_button);
		testVerify.assertTrue(this.isDisplayed(header_section, header_logout_button), "Log Out button exists!!!");
	}
	
	
	
	
	
	
	

	

	
	

}
