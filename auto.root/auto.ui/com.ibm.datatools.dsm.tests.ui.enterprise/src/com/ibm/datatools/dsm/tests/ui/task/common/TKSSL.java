package com.ibm.datatools.dsm.tests.ui.task.common;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;


import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKSSL extends TaskBase{
	
	public TKSSL(WebDriverCore driverCore) {
		super(driverCore);
	}
	
	//For SSL
	private final String ssl_advanced_button = "ssl_advanced_button";
	private final String ssl_exception_button = "ssl_exception_button";
	
	public void addSecurityException() throws AWTException{
		
		if(this.isDisplayed(ssl_advanced_button)){
			this.clickBtn(ssl_advanced_button);
		    this.clickBtn(ssl_exception_button);
		    this.pressAlt_C();
		}
		
	}
	
	public void pressAlt_C() throws AWTException{
		
		Robot robot = new Robot(); 		
        robot.keyPress(KeyEvent.VK_ALT); 
        robot.keyPress(KeyEvent.VK_C); 
        
        robot.keyRelease(KeyEvent.VK_C);
        robot.keyRelease(KeyEvent.VK_ALT);        
	}
	
	
	
	

}
