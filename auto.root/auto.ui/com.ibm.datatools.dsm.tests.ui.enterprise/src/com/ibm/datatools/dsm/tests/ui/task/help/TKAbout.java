package com.ibm.datatools.dsm.tests.ui.task.help;

import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKAbout extends TaskBase{

	public TKAbout(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}
	
	private final String plugins_button = "plugins_button";
	
	
	public void clickPluginsBtn(){		
		this.clickBtn(plugins_button);		
	};
	
	
	

}
