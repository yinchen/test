package com.ibm.datatools.dsm.tests.ui.task.help;


import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKWelcome extends TaskBase{
	
	//private final String welcome_page = "welcome_page";
	
	private final String welcome_page_title = "welcome_page_title";

	public TKWelcome(WebDriverCore driverCore) {
		super(driverCore);
	}	
	
	public boolean isMaximum(){
		return this.isDisplayed(welcome_minimum_icon);		
	}
	
	public void clickMiniumBtn() {
		this.clickBtn(welcome_minimum_icon);
	}
	
	
	
	public String getWelcomePageTitle() {
		return this.getStaticText(welcome_page_title);
	}	

	public void verifyWelcomePageTitle() {
		this.testVerify.assertStringEquals(this.getWelcomePageTitle(), 
				"Welcome to Data Server Manager",
				"Welcome page title is correct!!!");

	}
}
