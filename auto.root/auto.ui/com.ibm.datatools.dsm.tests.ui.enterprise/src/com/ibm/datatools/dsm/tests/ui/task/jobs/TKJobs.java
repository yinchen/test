package com.ibm.datatools.dsm.tests.ui.task.jobs;

import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKJobs extends TaskBase{
	
	private final String jobs_page = "jobs_page";	
	
	private final String alljobtypes_option = "alljobtypes_option";
	private final String viewoptionshistory_option = "viewoptionshistory_option";	
	private final String viewresults_button = "viewresults_button";
	private final String jobhistorysettings_button = "jobhistorysettings_button";

	public TKJobs(WebDriverCore driverCore) {
		super(driverCore);
	}

	public boolean isExistsAllJobTypesOption(){
		return this.isDisplayed(jobs_page, alljobtypes_option);
	}
	
	public boolean isExistsViewOptionsHistoryOption(){
		return this.isDisplayed(jobs_page, viewoptionshistory_option);
	}
	
	public boolean isExistsRunButton(){
		return this.isDisplayed(jobs_page, run_button);
	}
	
	public boolean isExistsCancelButton(){
		return this.isDisplayed(jobs_page, cancel_button);
	}
	
	public boolean isExistsViewResultsButton(){
		return this.isDisplayed(jobs_page, viewresults_button);
	}
	
	public boolean isExistsJobHistorySettingsButton(){
		return this.isDisplayed(jobs_page, jobhistorysettings_button);
	}
	
	public void verifyJobsPageInfo(){
		this.testVerify.assertTrue(this.isExistsAllJobTypesOption(), 
				"Jobs page All Job Types option displayed!!!");
		
		this.testVerify.assertTrue(this.isExistsViewOptionsHistoryOption(), 
				"Jobs page View Options History option displayed!!!");
		
		this.testVerify.assertTrue(this.isExistsRunButton(), 
				"Jobs page Run Button displayed!!!");
		
		this.testVerify.assertTrue(this.isExistsCancelButton(), 
				"Jobs page Cancel Button displayed!!!");
		
		this.testVerify.assertTrue(this.isExistsViewResultsButton(), 
				"Jobs page View Results Button displayed!!!");
		
		this.testVerify.assertTrue(this.isExistsJobHistorySettingsButton(), 
				"Jobs page Job History Settings Button displayed!!!");
	}
	
	
}














