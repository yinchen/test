package com.ibm.datatools.dsm.tests.ui.task.monitor;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKAlerts extends TKMonitor{

	
	private final String monitor_alerts_page = "monitor_alerts_page";
	
	private final String showAllOpen_menu = "showAllOpen_menu";
	private final String viewdetails_button = "viewdetails_button";
	
	
	public TKAlerts(WebDriverCore driverCore) {
		super(driverCore);
	}

	
	public boolean isShowAllOpenMenuDisplayed(){
		return this.isDisplayed(monitor_alerts_page, showAllOpen_menu);
	}
	
	public boolean isViewdetailsMenuDisplayed(){
		return this.isDisplayed(monitor_alerts_page, viewdetails_button);
	}
	
	public boolean isDeleteMenuDisplayed(){
		return this.isDisplayed(monitor_alerts_page, delete_button);
	}
	
	public boolean isSendMenuDisplayed(){
		return this.isDisplayed(monitor_alerts_page, send_button);
	}
	
	public void verifyAlertsPageInfo(){
		
		this.testVerify.assertTrue(this.isShowAllOpenMenuDisplayed(), 
				"Monitor Alerts page ShowAllOpen menu displays!!!");
		
		this.testVerify.assertTrue(this.isViewdetailsMenuDisplayed(), 
				"Monitor Alerts page View details menu displays!!!");
		
		this.testVerify.assertTrue(this.isDeleteMenuDisplayed(), 
				"Monitor Alerts page Delete menu displays!!!");
		
		this.testVerify.assertTrue(this.isSendMenuDisplayed(), 
				"Monitor Alerts page Send menu displays!!!");
		
	}
}
