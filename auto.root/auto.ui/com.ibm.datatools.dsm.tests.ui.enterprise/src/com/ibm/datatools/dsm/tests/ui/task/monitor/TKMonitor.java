package com.ibm.datatools.dsm.tests.ui.task.monitor;

import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKMonitor extends TaskBase{

	public TKMonitor(WebDriverCore driverCore) {
		super(driverCore);
	}
	
	protected final String Overview_tab = "Overview_tab";
	protected final String Statements_tab = "Statements_tab";
	protected final String Locking_tab = "Locking_tab";
	protected final String Applications_tab = "Applications_tab";
	protected final String Workload_tab = "Workload_tab";	
	protected final String Memory_tab = "Memory_tab";
	protected final String IO_tab = "IO_tab";
	protected final String Storage_tab = "Storage_tab";
	protected final String Notification_tab = "Notification_tab";
	protected final String Throughput_tab = "Throughput_tab";
	
    protected final String History_button = "History_button";
	
	protected final String PackageCache_ViewDetails_button = "PackageCache_ViewDetails_button";
	protected final String PackageCache_Explain_button = "PackageCache_Explain_button";
	protected final String PackageCache_ShowSystemStatements_button = "PackageCache_ShowSystemStatements_button";
	protected final String PackageCache_Average_button = "PackageCache_Average_button";
	protected final String PackageCache_GenerateReport_button = "PackageCache_GenerateReport_button";
	protected final String PackageCache_DataLink = "PackageCache_DataLink";
	protected final String PackageCache_TimeSlider = "PackageCache_TimeSlider";
	protected final String PackageCache_DataRow = "PackageCache_DataRow";

}
