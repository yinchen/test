package com.ibm.datatools.dsm.tests.ui.task.monitor;



import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;


public class TKMonitorDatabase extends TKMonitor {
	
	private final String monitor_database_page = "monitor_database_page";
	
	
	
	
	public TKMonitorDatabase(WebDriverCore driverCore) {
		super(driverCore);
	}
	
	public boolean isOverviewTabDisplayed(){
		return this.isDisplayed(monitor_database_page, Overview_tab);
	}
	
	public boolean isStatementsTabDisplayed(){
		return this.isDisplayed(monitor_database_page, Statements_tab);
	}
	
	public boolean isLockingTabDisplayed(){
		return this.isDisplayed(monitor_database_page, Locking_tab);
	}
	
	public boolean isApplicationsTabDisplayed(){
		return this.isDisplayed(monitor_database_page, Applications_tab);
	}
	
/*	
   public boolean isWorkloadTabDisplayed(){
		return this.isDisplayed(monitor_database_page, Workload_tab);
	}
	
	public boolean isMemoryTabDisplayed(){
		return this.isDisplayed(monitor_database_page, Memory_tab);
	}
*/	
	public boolean isThroughputTabDisplayed(){
		return this.isDisplayed(monitor_database_page, Throughput_tab);
	}
	
	public boolean isIOTabDisplayed(){
		return this.isDisplayed(monitor_database_page, IO_tab);
	}
	
	public boolean isStorageTabDisplayed(){
		return this.isDisplayed(monitor_database_page, Storage_tab);
	}
	
	public boolean isNotificationTabDisplayed(){
		return this.isDisplayed(monitor_database_page, Notification_tab);
	}
	
	public void verifyDatabastPageInfo(){
		
		this.testVerify.assertTrue(this.isOverviewTabDisplayed(), 
				"Monitor Database page Overview tab displays!!!");
		
		this.testVerify.assertTrue(this.isStatementsTabDisplayed(), 
				"Monitor Database page tab displays!!!");
		
		this.testVerify.assertTrue(this.isLockingTabDisplayed(), 
				"Monitor Database page Statements tab displays!!!");
		
		this.testVerify.assertTrue(this.isApplicationsTabDisplayed(), 
				"Monitor Database page Applications tab displays!!!");
		
		this.testVerify.assertTrue(this.isThroughputTabDisplayed(), 
				"Monitor Database page isThroughput tab displays!!!");
/*		
		this.testVerify.assertTrue(this.isWorkloadTabDisplayed(), 
				"Monitor Database page Workload tab displays!!!");
		
		this.testVerify.assertTrue(this.isMemoryTabDisplayed(), 
				"Monitor Database page Memory tab displays!!!");
*/		
		this.testVerify.assertTrue(this.isIOTabDisplayed(), 
				"Monitor Database page I/O tab displays!!!");
		
		this.testVerify.assertTrue(this.isStorageTabDisplayed(), 
				"Monitor Database page Storage tab displays!!!");
		
		this.testVerify.assertTrue(this.isNotificationTabDisplayed(), 
				"Monitor Database page Notifications tab displays!!!");
		
	}
	
	public void clickHistoryBtn(){
		this.clickBtn(monitor_database_page, History_button);
	}
	
	public boolean isViewDetailsBtnDisplayed(){
		return this.isDisplayed(monitor_database_page, PackageCache_ViewDetails_button);
	}
	
	public boolean isExplainBtnDisplayed(){
		return this.isDisplayed(monitor_database_page, PackageCache_Explain_button);
	}
	
	public boolean isShowSystemStatementsBtnDisplayed(){
		return this.isDisplayed(monitor_database_page, PackageCache_ShowSystemStatements_button);
	}
	
	public boolean isAverageBtnDisplayed(){
		return this.isDisplayed(monitor_database_page, PackageCache_Average_button);
	}
	
	public boolean isGenerateReportBtnDisplayed(){
		return this.isDisplayed(monitor_database_page, PackageCache_GenerateReport_button);
	}
	
	public void verifyHistoryStatementLabBtn(){
//		this.testVerify.assertTrue(this.isViewDetailsBtnDisplayed(), 
//				"Monitor Database page View Details button displays!!!");
//		
//		this.testVerify.assertTrue(this.isExplainBtnDisplayed(), 
//				"Monitor Database page Explain button displays!!!");
//		
//		this.testVerify.assertTrue(this.isShowSystemStatementsBtnDisplayed(), 
//				"Monitor Database page Show System Statements button displays!!!");
//		
		this.testVerify.assertTrue(this.isAverageBtnDisplayed(), 
				"Monitor Database page Average button displays!!!");
		/*
		this.testVerify.assertTrue(this.isGenerateReportBtnDisplayed(), 
				"Monitor Database page Generate Report button displays!!!");
		*/
	}
	
	public boolean noDataDisplayed(){
		return this.isDisplayed(monitor_database_page, PackageCache_DataLink);
	}
	
	
	
	public void dragTimeSlider(int movement){
		 this.dragSlider(monitor_database_page, PackageCache_TimeSlider,movement);
	}
	
	public void selectOneRow(int i){
		this.selectOneCheckBox(PackageCache_DataRow,i);
	}
	
	public void viewSelectRowDetails(){
		this.clickBtn(PackageCache_ViewDetails_button);
	}
	
	
	
	public void waitForPageLoadCompleted(){		
		long timeBegins = System.currentTimeMillis();		
		do {
			WebDriverCore.pause(pauseTime);			
			if(this.isOverviewTabDisplayed()){	
				break;
			}		
		} while (System.currentTimeMillis() - timeBegins <= timeOut * 1000);
	}
	
}






