package com.ibm.datatools.dsm.tests.ui.task.runsql;

import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKRunSQL extends TaskBase{

	
	private final String runsql_page = "runsql_page";	
	
	private final String run_menu = "run_menu";
	private final String script_menu = "script_menu";
	private final String edit_menu = "edit_menu";
	private final String favorites_menu = "favorites_menu";
	private final String runsql_learnmore_button = "runsql_learnmore_button";
	private final String sqleditor_input = "sqleditor_input";
	private final String tune_item = "tune_item";
	private final String runsql_run_button="runsql_run_button";
	
	public TKRunSQL(WebDriverCore driverCore) {
		super(driverCore);
	}
	
	public boolean isExistsRunMenu(){
		return this.isDisplayed(runsql_page, run_menu);
	}
	
	public boolean isExistsScriptMenu(){
		return this.isDisplayed(runsql_page, script_menu);
	}
	
	public boolean isExistsEditMenu(){
		return this.isDisplayed(runsql_page, edit_menu);
	}
	
	public boolean isExistsFavoritesMenu(){
		return this.isDisplayed(runsql_page, favorites_menu);
	}
	
	public boolean isExistsLearnmoreMenu(){
		return this.isDisplayed(runsql_page, runsql_learnmore_button);
	}
	
	
	public void verifyRunSQLPageInfo(){
		
		this.testVerify.assertTrue(this.isExistsRunMenu(), 
				"Run SQL page Run menu displayed!!!");
		
		this.testVerify.assertTrue(this.isExistsScriptMenu(), 
				"Run SQL page Script menu displayed!!!");
		
		this.testVerify.assertTrue(this.isExistsEditMenu(), 
				"Run SQL page Edit menu displayed!!!");
		
		this.testVerify.assertTrue(this.isExistsFavoritesMenu(), 
				"Run SQL page Favorites menu displayed!!!");
/*		
		this.testVerify.assertTrue(this.isExistsLearnmoreMenu(), 
				"Run SQL page Learnmore menu displayed!!!");
*/
	}
	
	public void waitForPageLoadCompleted(){		
		long timeBegins = System.currentTimeMillis();		
		do {
			WebDriverCore.pause(pauseTime);			
			if(this.isExistsRunMenu()){	
				break;
			}		
		} while (System.currentTimeMillis() - timeBegins <= timeOut * 1000);
	}
	
	public void inputSQL(String sql){
		this.setTextBox(sqleditor_input,sql);
	}
	
	public void tuneQuery(){
		this.clickBtn(0,runsql_run_button);
		WebDriverCore.pause(1);
		this.clickMenuItem(tune_item);
	}
	
}













