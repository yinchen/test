package com.ibm.datatools.dsm.tests.ui.task.tuning;

import com.ibm.datatools.dsm.tests.ui.framework.base.DBUtils;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;


public class TKEOJob extends TKTuning {

	public TKEOJob(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}

	private final String selectivity_page = "selectivity_page";
	private final String eojob_page = "eojob_page";
	
	private final String jobName_filter="jobName_filter";
	private final String select_job = "select_job";
	private final String statement = "select *";	
	private final String host_variables_button = "host_variables_button";
	private final String host_variables_menuitem = "host_variables_menuitem";
	private final String select_statement = "select_statement";
	private final String select_hv = "select_hv";
	private final String selectivity_override_button="selectivity_override_button";
	private final String job_list_tab_button="job_list_tab_button";
	private final String close_workload_tab_button="close_workload_tab_button";
	private final String filter_contains_dropdown="filter_contains_dropdown";
	private final String dropdown_button="dropdown_button";
	private final String result_message="result_message";	
	private final String job_status="job_status";
	private final String view_revised_apg_button="view_revised_apg_button";
	
	private String qmjobname = "QMEOTestJob";
	private String eojobname="SelectivityOverride";
	private String jobstatus="";
	
	public boolean isMoreActionsDisplayed(){
		return this.isDisplayed(result_page, more_actions_button);
	}
	
	public boolean isViewRevisedApgDisplayed() {
		return this.isDisplayed(eojob_page, view_revised_apg_button);
	}
	
	public boolean isViewRevisedApgEnabled() {
		return this.isBtnEnabled(view_revised_apg_button);
	}
	
	public void verifyJoblistPageInfo() {
		this.testVerify.assertTrue(this.isMoreFiltersDisplayed(), 
				"Optimize Tuning Jobs More Filters button displayed!!!");
	}
	
	public void createEOJob() throws Exception {
		//select qm workload
		
		StringBuffer runsql = new StringBuffer();
		runsql.append("delete from DSJOBMGR.JOBS where jobid = 1;");
		runsql.append("delete from DSJOBMGR.JOBPROPS where jobid = 1;");
		runsql.append("delete from PROCMGMT.PROCHIST where INSTID in ('1','LOCDB21:1');");
		runsql.append("delete from PROCMGMT.PROCHIST_PROPERTY where INSTID in ('1','LOCDB21:1');");
		
		//create job
		runsql.append("insert into DSJOBMGR.JOBS(JOBID,JOBNAME,JOBTYPE,SCHEDENABLED,JOBCREATOR,LASTMODIFIED,JOBCOMPONENT, DBREQFORJOB) VALUES (1,'QMEOTestJob','querytunerjobs',0,'admin',current timestamp,'OQWTComp',1);");
		runsql.append("insert into DSJOBMGR.JOBPROPS(JOBID,JOBPROPNAME,JOBPROPVALUE) VALUES(1,'DESC_SECTION','{\"schedenabled\":\"0\",\"dbreqforjob\":\"1\",\"jobType\":\"querytunerjobs\",\"stagingtabledbconnectionprofilename\":\"LOCDB21\",\"jobname\":\"QMEOTestJob\",\"jobcreator\":\"admin\",\"jobid\":\"1\",\"mondbconprofile\":\"LOCDB21\",\"jobdesc\":\"\"}');");
		runsql.append("insert into DSJOBMGR.JOBPROPS(JOBID,JOBPROPNAME,JOBPROPVALUE) VALUES(1,'OQWT_SECTION','{\"wtaaValCheck\":false,\"wsaValCheck\":true,\"curDegreeValue\":\"ANY\",\"tuningType\":\"WORKLOAD\",\"deleteStagingDataOnSuccessfulCapture\":\"NO\",\"rid\":\"\",\"captureMaxRowsCount\":\"100\",\"wccJobStatus\":\"Running\",\"reExplainWorkloadValCheck\":true,\"ISNEWTUNE\":true,\"isNewF\":true,\"curRefAgeValue\":\"ANY\",\"wtaaModelSelect\":\"modeling\",\"stagingTableSchema\":\"CQMTOOLS\",\"retune\":\"false\",\"workloadName\":\"QMEOTestJob\",\"stagingTableDBConnectionProfileName\":\"LOCDB21\",\"wccJobID\":\"QMEOTestJob#1\",\"dbconfigured\":\"1\",\"curMQTValue\":\"ALL\",\"curSysTimeValue\":\"NULL\",\"stagingTableDBType\":\"DB2Z\",\"tuningCtx\":\"STANDALONE\",\"providerID\":\"CQM\",\"workloadID\":\"75\",\"Select\":\"3\",\"oqwtLicenseType\":\"The full set of tuning features is available.\",\"curBusiTimeValue\":\"NULL\",\"curPathValue\":\"\",\"curGetArchiveValue\":\"Y\",\"licenseLabel\":\"The full set of tuning features is available.\",\"wiaValCheck\":false,\"SQLID\":\"c01d02\",\"monitoredDbType\":\"DB2Z\",\"desc\":\"\",\"monitoredDbProfile\":\"LOCDB21\"}');");
		runsql.append("insert into PROCMGMT.PROCHIST(INSTID,PROCID,COMPID,START_TIME,END_TIME,STATUS) VALUES ('1',1.1,'jobmanager',current timestamp,current timestamp,3);");
		runsql.append("insert into PROCMGMT.PROCHIST(INSTID,PROCID,COMPID,START_TIME,END_TIME,STATUS) VALUES ('LOCDB21:1',1.1,'jobmanager',current timestamp,current timestamp,2);");
		runsql.append("insert into PROCMGMT.PROCHIST_PROPERTY(INSTID,PROPERTY_ID,SEQUENCE,PROPERTY_VAL) VALUES (1,'PROCESS_RETURN_CODE',0,0);");
		runsql.append("insert into PROCMGMT.PROCHIST_PROPERTY(INSTID,PROPERTY_ID,SEQUENCE,PROPERTY_VAL) VALUES ('LOCDB21:1','ADDITIONALDETAILS',0,'WORKLOAD TUNING, EXPLAIN_START=2016-11-11 11:24:03.393, EXPLAIN_END=2016-11-11 11:24:03.716');");
		runsql.append("insert into PROCMGMT.PROCHIST_PROPERTY(INSTID,PROPERTY_ID,SEQUENCE,PROPERTY_VAL) VALUES ('LOCDB21:1','PROCESS_RETURN_CODE',0,0);");
		runsql.append("insert into PROCMGMT.PROCHIST_PROPERTY(INSTID,PROPERTY_ID,SEQUENCE,PROPERTY_VAL) VALUES ('LOCDB21:1','PROGRESS',0,'Completed');");
		runsql.append("insert into PROCMGMT.PROCHIST_PROPERTY(INSTID,PROPERTY_ID,SEQUENCE,PROPERTY_VAL) VALUES ('LOCDB21:1','tuningResult',0,'TUNING_SUCCESS');");
		
		//create workload
		runsql.append("delete from db2osc.dsn_wcc_workloads where wlid=1;");
		runsql.append("delete from db2osc.dsn_wcc_wl_sources where wlid=1;");
		runsql.append("delete from db2osc.dsn_wcc_stmt_insts where wlid=1;");
		runsql.append("delete from db2osc.dsn_wcc_stmt_texts where stmt_text_id=1;");
		runsql.append("delete from db2osc.dsn_wcc_tasks where taskid in (1,2,3);");
		runsql.append("delete from  db2osc.dsn_wcc_hostv_set where instid = 1;");
		runsql.append("delete from  db2osc.dsn_wcc_hostv_val where instid = 1;");
		runsql.append("delete from  db2osc.dsn_wcc_hostv_pred where instid = 1;");
		
		runsql.append("insert into db2osc.dsn_wcc_workloads (WLID,NAME,STATUS,ANALYZE_COUNT,OWNER,EXPLAIN_STATUS,CPUTIME_STATUS) values(1,'QMEOTestJob#1',9,0,'DB2INST1',5,'N');");
		runsql.append("insert into db2osc.dsn_wcc_wl_sources (SRCID,NAME,WLID,TYPE,STATUS,CONSOLIDATE_STATUS,LASTUPDATETS,QUERY_COUNT,MONITOR_STATUS,NEW_QUERY_COUNT,MONITOR_ENABLED) values(1,'Source_0',1,207,9,21,current timestamp,1,1,0,'Y');");
		runsql.append("insert into db2osc.dsn_wcc_stmt_texts ( STMT_TEXT_ID,QUALIFIER,HASHKEY,TRANSFORMED_HASKEY,STMT_LENGTH,STMT_TEXT) values (1,'C01D02',0,1186955152,87,'SELECT *  FROM T2, T1  WHERE T1.C1 < ?  AND T2.C2 = ?  AND T1.C3 > ?  AND T2.C2 < T1.C2');");
		runsql.append("insert into db2osc.dsn_wcc_stmt_insts ( INSTID,STMT_TEXT_ID,TRANSFORMED_TEXTID,WLID,SRCID,PRIMAUTH,CURSQLID,PLANNAME,COLLID,PKGNAME,VERSION,SECTNOI,REOPT,PATHSCHEMAS,BIND_ISO,BIND_CDATA,BIND_DYNRL,BIND_DEGREE,BIND_SQLRL,BIND_CHOLD,CACHED_TS,LAST_UPDATE_TS,LAST_EXPLAIN_TS,LITERALS,PERMANENT,EXPLAIN_STATUS,STMT_TOKEN,GROUP_MEMBER,LITERAL_REPL,STMTNO,BIND_TIME,QUERYNO,BIND_OWNER,BIND_EXPLAIN_OPTION,QUALIFIER,STMT_ID,BIND_SYSTIME_SENSITIVE,BIND_BUSTIME_SENSITIVE,BIND_GETARCHIVE_SENSITIVE,EXPANSION_REASON,STMTNOI,ACCELERATED,STAT_ACC_ELAP,STAT_ACC_CPU,STAT_ACC_ROW,STAT_ACC_BYTE,STAT_ACC_1ROW,STAT_ACC_DB2,STAT_ACC_EXEC,STAT_ACC_WAIT,ACCEL_OFFLOAD_ELIGIBLE,ACCELERATOR_NAME,COST_CATEGORY,PROCMS,PROCSU,NUM_RELATIONAL_SCANS,NUM_NONMATCHING_INDEXSCANS,NUM_MERGESCAN_JOINS,NUM_HYBRID_JOINS,NUM_SORT_NODES,TOTAL_COST,STMT_TYPE,PER_STMT_ID,STBLGRP,STABILIZED,EO_CANDIDATE,EXEC_COUNT,HOSTV_COUNT,HOSTV_SET_COUNT) values (1,1,NULL,1,1,'C01D02','C01D02','' ,'' , '','' ,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,current timestamp,current timestamp,current timestamp,NULL,'Y',5,'6381F27B5D4EE1152032','','',0,NULL,0,'','N','C01D02',0,NULL,NULL,NULL,'',0,NULL,0,0,0,0,0,0,0,0,NULL,NULL,'B',1,1,1,0,0,0,0,1.1931812782713629,'SELECT',0,'','',1,16,3,3);");
		runsql.append("insert into db2osc.dsn_wcc_tasks (TASKID,NEXTTASK,SCHEDULED_TASKID,WLID,SRCID,TYPE,SUBTYPE,STATUS,CREATOR ,START_TIME,END_TIME,INTERVAL,CONSOLIDATE_RTINFO,CONSOLIDATE_EPINFO,CONSOLIDATE_LITERA,KEEP_STATEMENTS,CONSOLIDATION_TIME,START_TRACE,STOP_TRACE,WARMUP_TIME,LAST_FIRE_TIME,LAST_UPDATE_TS,CONFIG,ACT_START_TIME,ACT_END_TIME) values ( 1,NULL,NULL,1,NULL,5,14,'F','DB2INST1',NULL,NULL,NULL,'N',NULL,'N','N',NULL,'N','N',NULL,NULL,current timestamp,blob('23467269204e6f762031312031313a32313a31322043535420323031360a50524f434553534f525f4e414d453d636f6d2e69626d2e64617461746f6f6c732e64736f652e736572762e456d62656454756e696e6741647669736f720a454e41424c455f5357544943485f53514c49443d66616c73650a'),current timestamp,current timestamp);");
		runsql.append("insert into db2osc.dsn_wcc_tasks (TASKID,NEXTTASK,SCHEDULED_TASKID,WLID,SRCID,TYPE,SUBTYPE,STATUS,CREATOR ,START_TIME,END_TIME,INTERVAL,CONSOLIDATE_RTINFO,CONSOLIDATE_EPINFO,CONSOLIDATE_LITERA,KEEP_STATEMENTS,CONSOLIDATION_TIME,START_TRACE,STOP_TRACE,WARMUP_TIME,LAST_FIRE_TIME,LAST_UPDATE_TS,CONFIG,ACT_START_TIME,ACT_END_TIME) values ( 2,NULL,NULL,1,NULL,4,1,'F','DB2INST1',NULL,NULL,NULL,'Y',1,'N','N',NULL,'N','N',NULL,NULL,current timestamp,blob('23467269204e6f762031312031313a32313a31322043535420323031360a50524f434553534f525f4e414d453d636f6d2e69626d2e64617461746f6f6c732e64736f652e736572762e456d62656454756e696e6741647669736f720a454e41424c455f5357544943485f53514c49443d66616c73650a'),current timestamp,current timestamp);");
		runsql.append("insert into db2osc.dsn_wcc_tasks (TASKID,NEXTTASK,SCHEDULED_TASKID,WLID,SRCID,TYPE,SUBTYPE,STATUS,CREATOR ,START_TIME,END_TIME,INTERVAL,CONSOLIDATE_RTINFO,CONSOLIDATE_EPINFO,CONSOLIDATE_LITERA,KEEP_STATEMENTS,CONSOLIDATION_TIME,START_TRACE,STOP_TRACE,WARMUP_TIME,LAST_FIRE_TIME,LAST_UPDATE_TS,CONFIG,ACT_START_TIME,ACT_END_TIME) values ( 3,NULL,NULL,1,NULL,5,1,'F','DB2INST1',NULL,NULL,NULL,'N',NULL,'N','N',NULL,'N','N',NULL,NULL,current timestamp,blob('23467269204e6f762031312031313a32313a31322043535420323031360a50524f434553534f525f4e414d453d636f6d2e69626d2e64617461746f6f6c732e64736f652e736572762e456d62656454756e696e6741647669736f720a454e41424c455f5357544943485f53514c49443d66616c73650a'),current timestamp,current timestamp);");
		runsql.append("insert into db2osc.dsn_wcc_hostv_set (INSTID,HOSTV_SET_ID,HOSTV_SET_EXEC_COUNT,HOSTV_SET_WEIGHT) values (1,1,6,0.375);");
		runsql.append("insert into db2osc.dsn_wcc_hostv_val (INSTID,HOSTV_SET_ID,HOSTV_POSITION,HOSTV_TYPE,HOSTV_VALUE) values (1,1,1,'INTEGER',1);");
		runsql.append("insert into db2osc.dsn_wcc_hostv_val (INSTID,HOSTV_SET_ID,HOSTV_POSITION,HOSTV_TYPE,HOSTV_VALUE) values (1,1,2,'INTEGER',2);");
		runsql.append("insert into db2osc.dsn_wcc_hostv_val (INSTID,HOSTV_SET_ID,HOSTV_POSITION,HOSTV_TYPE,HOSTV_VALUE) values (1,1,3,'INTEGER',3);");
		runsql.append("insert into db2osc.dsn_wcc_hostv_pred (INSTID,HOSTV_POSITION,HOSTV_PRED_NO,HOSTV_PRED_TEXT,HOSTV_EO_CANDIDATE) values (1,1,2,'\"C01D02\".\"T1\".\"C1\"  <  ?',1);");
		runsql.append("insert into db2osc.dsn_wcc_hostv_pred (INSTID,HOSTV_POSITION,HOSTV_PRED_NO,HOSTV_PRED_TEXT,HOSTV_EO_CANDIDATE) values (1,2,3,'\"C01D02\".\"T2\".\"C2\"  =  ?',0);");
		runsql.append("insert into db2osc.dsn_wcc_hostv_pred (INSTID,HOSTV_POSITION,HOSTV_PRED_NO,HOSTV_PRED_TEXT,HOSTV_EO_CANDIDATE) values (1,3,4,'\"C01D02\".\"T1\".\"C3\"  >  ?',1); ");
		runsql.append("");
		runsql.append("");
		
		
		
		//new DBUtils("9.125.138.22", 65000, "REPODB", "db2inst1", "N1cetest").		
		new DBUtils().
		executeBatch(runsql.toString());
		

		this.clickMoreFiltersBtn();
		this.clickResetBtn();
		this.setJobName(qmjobname);
		this.clickFilterBtn();
		this.selectJob(qmjobname);
		
		//view qm workload result
		this.clickViewResult();
		this.clickViewWorkloadStatements();
		this.selectStatement();
		
		if (this.isMoreActionsDisplayed()){
			this.clickMoreActionsBtn();
			clickHostVariablesItem();
		} else {
			//click hv button
			this.clickHostVariablesBtn();
		}
				
		//select all sets and create eo job
		this.clickAllCheckBox();
		this.clickSelectivityBtn();
		eojobname = this.getResultMessage().split(",")[1].substring(1);
		this.clickOKBtn();
		
		
	}
	
	public void viewEOJobInfo() {
		
		//set eo job name
		this.clickSwitchTableBtn();
		this.clickMoreFiltersBtn();
		this.clickResetBtn();
		this.clickdropdownBtn();
		this.clickFilterContainsBtn();
		this.setJobName(eojobname);
		this.clickFilterBtn();	
		
		//select eo job and view eo result
		this.selectJob(eojobname);
		jobstatus=this.getJobStatus();
		if (jobstatus.equals("Succeeded")){
			this.clickViewResult();
		}
		
		this.testVerify.assertTrue(this.isViewRevisedApgDisplayed(), 
				"Optimize Tuning EO Job View Revised Access Plan Graph button displayed!!!");
		this.testVerify.assertTrue(this.isViewRevisedApgEnabled(), 
				"Optimize Tuning EO Job View Revised Access Plan Graph button Enabled!!!");
	}
	
	public void setJobName(String s){	
		this.setTextBox(getSize(job_list_page,jobName_filter)-1, jobName_filter, s);
	}

	public void selectJob(String jobname) {
		this.clickMenuItem(job_list_page, select_job+split+jobname);
	}

	public void selectStatement() {
		this.clickMenuItem(result_page, select_statement+split+statement);
	}
	
	public void clickMoreActionsBtn() {
		this.clickBtn(result_page, more_actions_button);
		WebDriverCore.pause(5);
	}
	
	public void clickHostVariablesBtn() {
		this.clickBtn(result_page, host_variables_button);
		driverCore.waitForPageLoad();
	}
	
	public void clickHostVariablesItem() {
		this.clickMenuItem(result_page, host_variables_menuitem);
		driverCore.waitForPageLoad();
	}
	
	//Currently we just select all host variable sets to test
	public void clickAllCheckBox() {
		this.checkCheckBox(selectivity_page, select_hv);
	}
	
	public void clickSelectivityBtn() {
		this.clickBtn(selectivity_page,selectivity_override_button);
	}
	
	public void clickSwitchTableBtn() {
		this.clickBtn(result_page, close_workload_tab_button);
	}
	
	public void clickdropdownBtn() {
		this.clickBtn(getSize(job_list_page,dropdown_button)-1, job_list_page, dropdown_button);
	}
	
	public void clickFilterContainsBtn() {
		this.clickBtn(job_list_page, filter_contains_dropdown);
	}
	
	public String getResultMessage(){
		return this.getStaticText(message_page, result_message);
	}
	
	public String getJobStatus() {
		return this.getStaticText(job_list_page, job_status+split+eojobname);
	}
	

	
}
