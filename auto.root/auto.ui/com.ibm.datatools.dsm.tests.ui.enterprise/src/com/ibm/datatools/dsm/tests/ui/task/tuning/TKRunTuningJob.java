package com.ibm.datatools.dsm.tests.ui.task.tuning;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.task.common.TKDialog;
import com.ibm.datatools.dsm.tests.ui.task.jobs.TKJobs;
import com.ibm.datatools.dsm.tests.ui.task.runsql.TKRunSQL;

public class TKRunTuningJob extends TKStartTuning {

	public TKRunTuningJob(WebDriverCore driverCore) {
		super(driverCore);
	}

	protected TKStartTuning tkStartTuning = new TKStartTuning(driverCore);
	protected TKTuningSourcePage tkTuningSourcePage = new TKTuningSourcePage(driverCore);
	protected TKTuningScopePage tkTuningScopePage = new TKTuningScopePage(driverCore);
	protected TKTuningOpsPage tkTuningOpsPage = new TKTuningOpsPage(driverCore);
//	protected TKRunSQL tkRunSQLPage = new TKRunSQL(driverCore);
//	protected TKJobs tkJobsPage = new TKJobs(driverCore);
//	protected TKEOJob tkEOJobPage = new TKEOJob(driverCore);

	public void runSingleTuneJob() {
		tkTuningSourcePage.verifyInputSqlSourcePageInfo("select name from sysibm.systables t where t.name like 'SYS%' and t.type = 'T';");
		tkStartTuning.clickNexBtn();
		tkTuningScopePage.verifyScopePageInfo();
		tkStartTuning.clickNexBtn();
		tkTuningOpsPage.verifyOpsForSinglePageInfo("singleTune");
		tkStartTuning.clickLaunchBtn();
		//tkStartTuning.clickOkBtn();
		tkStartTuning.clickCloseBtn();
		driverCore.waitForPageLoad(20);
	}
	public void runSingleTuneJobForZ(String jobName) {
		tkTuningSourcePage.verifyInputSqlSourcePageInfo("select name from sysibm.systables t where t.name like 'SYS%' and t.type = 'T';");
		tkStartTuning.clickNexBtn();
		tkTuningScopePage.verifyScopePageInfo();
		tkStartTuning.clickNexBtn();
		tkTuningOpsPage.verifyOpsForSinglePageInfo(jobName);
		tkStartTuning.clickLaunchBtn();
		//tkStartTuning.clickOkBtn();
		tkStartTuning.clickCloseBtn();
		driverCore.waitForPageLoad(20);
	}
	public void runWorkloadTuneJob(String jobName) {
		tkTuningSourcePage.verifyInputSqlSourcePageInfo("select * from sysibm.systables where t.type = 'T';"+sql);
		tkStartTuning.clickNexBtn();
		tkTuningScopePage.verifyScopePageInfo();
		tkStartTuning.clickNexBtn();
		tkTuningOpsPage.verifyOpsForWorkloadPageInfo(jobName);
		tkStartTuning.clickLaunchBtn();
		//tkStartTuning.clickOkBtn();
		tkStartTuning.clickCloseBtn();
		driverCore.waitForPageLoad(20);
	}

	String sql = "select * from sysibm.systables t where t.name like 'SYS%' and t.type = 'T'";
	
	public void runPackCompareApg(TKDialog tkDialog) {
		tkTuningSourcePage.verifyPackgeSourcePageInfo();
		tkStartTuning.clickNexBtn();
		driverCore.waitForPageLoad();
		tkTuningScopePage.clickComapreBtn(tkDialog);
	}
}
