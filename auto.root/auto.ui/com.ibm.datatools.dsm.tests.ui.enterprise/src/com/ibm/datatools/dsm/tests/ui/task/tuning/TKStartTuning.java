package com.ibm.datatools.dsm.tests.ui.task.tuning;

import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKStartTuning extends TaskBase{
	public TKStartTuning(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}

	protected final String tuning_start_tuning_page = "tuning_start_tuning_page";	
	protected final String choose_StatementCache_source_option="choose_StatementCache_source_option";

	
	protected final String source_button = "source_button";
	protected final String next_button = "next_button";
	protected final String launch_tuning_job ="launch_tuning_job";
	protected final String tuning_ok_button ="tuning_ok_button";
	protected final String tuning_close_button="tuning_close_button";
	
	protected final String options_jobName_textarea = "options_jobName_textarea";
	protected final String options_explain = "options_explain";
	protected final String options_tuning = "options_tuning";
	

	
	public boolean isSourceBtnDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, source_button);
	}
	public boolean isNextBtnDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, next_button);
	}
	public boolean isLaunchBtnDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, launch_tuning_job);
	}
	public boolean isOKBtnDisplayed(){
		return this.isDisplayed(tuning_ok_button);
	}
	public void clickSourceBtn(){
		this.clickBtn(tuning_start_tuning_page, source_button);
		driverCore.waitForPageLoad();
	}
	public void clickNexBtn(){
		this.clickBtn(tuning_start_tuning_page, next_button);
		driverCore.waitForPageLoad();
	}
	public void clickLaunchBtn(){
		this.clickBtn(tuning_start_tuning_page, launch_tuning_job);
		driverCore.waitForPageLoad();
	}
	public void clickOkBtn(){
		this.clickBtn(tuning_ok_button);
		driverCore.waitForPageLoad();
	}
	
	public void clickCloseBtn(){
		this.clickBtn(tuning_close_button);
		driverCore.waitForPageLoad();
	}
	
	public boolean isJobNameTextareaDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, options_jobName_textarea);
	}
	public void SetJobName(String jobName){
		this.setTextBox(options_jobName_textarea, jobName);
	}
	public boolean isExplainOptionsDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, options_explain);
	}
	public boolean isTuningOptionsDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, options_tuning);
	}
	
}
