package com.ibm.datatools.dsm.tests.ui.task.tuning;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.task.common.TKDialog;

public class TKTuning extends TaskBase {

	protected final String tuning_start_tuning_page = "tuning_start_tuning_page";

	protected final String source_button = "source_button";
	protected final String launch_tuning_job = "launch_tuning_job";
	protected final String tuning_ok_button = "tuning_ok_button";

	// define tuning pages
	protected final String job_list_page = "job_list_page";
	protected final String result_page = "result_page";
	protected final String message_page = "message_page";

	// define tuning common buttons
	protected final String moreFilters_button = "moreFilters_button";
	protected final String filter_button = "filter_button";
	protected final String view_result_button = "view_result_button";
	protected final String reset_button = "reset_button";
	protected final String browse_And_Upload_Files = "browse_And_Upload_Files";

	// define other actions
	protected final String View_Workload_Statements = "View_Workload_Statements";
	// tuning dialog
	private final String tune_single_button = "tune_single_button";
	private final String tune_workload_button = "tune_workload_button";
	private final String tuning_dialog = "tuning_dialog";
	private final String schema_input = "schema_input";
	private final String tuning_dialog_OK = "tuning_dialog_OK";
	private final String explain_span = "explain_span";
	private final String tuning_dialog_close = "tuning_dialog_close";
	// tuning jobs
	private final String tuning_jobs_page = "tuning_jobs_page";
	private final String tuning_jobs_page_grid = "tuning_jobs_page_grid";
	private final String tuning_jobs_viewResult_btn = "tuning_jobs_viewResult_btn";
	private final String tuning_jobs_retune_btn = "tuning_jobs_retune_btn";
	private final String tuning_jobs_compare_btn = "tuning_jobs_compare_btn";
	private final String tuning_jobs_retention_btn = "tuning_jobs_retention_btn";
	private final String tuning_jobs_cancel_btn = "tuning_jobs_cancel_btn";
	private final String tuning_jobs_delete_btn = "tuning_jobs_delete_btn";
	private final String tuning_jobs_search_icon = "tuning_jobs_search_icon";
	private final String tuning_jobs_refresh_icon = "tuning_jobs_refresh_icon";
	private final String tuning_jobs_column_icon = "tuning_jobs_column_icon";
	private final String tuning_jobs_filter_icon = "tuning_jobs_filter_icon";
	private final String tuning_jobs_filter_dialog = "tuning_jobs_filter_dialog";
	private final String tuning_jobs_filter_dialog_clear_btn = "tuning_jobs_filter_dialog_clear_btn";
	private final String tuning_jobs_filter_dialog_jobname_input = "tuning_jobs_filter_dialog_jobname_input";
	private final String tuning_jobs_filter_dialog_filter_btn = "tuning_jobs_filter_dialog_filter_btn";
	private final String tuning_jobs_grid = "tuning_jobs_grid";
	private final String tuning_jobs_grid_first_row_jobName = "tuning_jobs_grid_first_row_jobName";
	private final String tuning_jobs_grid_selectAll = "tuning_jobs_grid_selectAll";
	private final String tuning_jobs_result_page = "tuning_jobs_result_page";
	private final String tuning_jobs_result_page_navItems = "tuning_jobs_result_page_navItems";
	private final String tuning_jobs_result_page_close_btn = "tuning_jobs_result_page_close_btn";
	private final String tuning_jobs_confirm_dialog = "tuning_jobs_confirm_dialog";
	private final String tuning_jobs_confirm_dialog_ok = "tuning_jobs_confirm_dialog_ok";
	private final String tuning_jobs_result_page_ia_tab = "tuning_jobs_result_page_ia_tab";
	private final String tuning_jobs_result_page_ia_tab_content = "tuning_jobs_result_page_ia_tab_content";
	private final String tuning_jobs_result_page_ia_tab_candidate_btn = "tuning_jobs_result_page_ia_tab_candidate_btn";
	private final String tuning_jobs_result_page_ia_tab_analysis_btn = "tuning_jobs_result_page_ia_tab_analysis_btn";
	private final String tuning_jobs_result_page_recommendations_tab = "tuning_jobs_result_page_recommendations_tab";
	private final String tuning_jobs_result_reommendation_iframe = "tuning_jobs_result_reommendation_iframe";
	private final String tuning_jobs_result_recommendation_apa_td = "tuning_jobs_result_recommendation_apa_td";
	private final String tuning_jobs_result_page_wapa_tab = "tuning_jobs_result_page_wapa_tab";
	private final String tuning_jobs_result_wapa_tab = "tuning_jobs_result_wapa_tab";
	private final String tuning_jobs_result_wapaOrQa_row = "tuning_jobs_result_wapaOrQa_row";
	private final String tuning_jobs_result_wapa_apg_span = "tuning_jobs_result_wapa_apg_span";
	private final String tuning_jobs_result_wapa_btn = "tuning_jobs_result_wapa_btn";
	private final String tuning_jobs_result_whatif_runTest_btn = "tuning_jobs_result_whatif_runTest_btn";
	private final String tuning_jobs_tab = "tuning_jobs_tab";
	private final String tuning_jobs_filter_dialog_tuningType = "tuning_jobs_filter_dialog_tuningType";
	private final String tuning_jobs_filter_dialog_whatifType = "tuning_jobs_filter_dialog_whatifType";
	private final String whatIf_result_impact_btn = "whatIf_result_impact_btn";
	private final String whatif_result_impact_radio_btn = "whatif_result_impact_radio_btn";
	private final String whatif_result_impact_pkgCheckBox = "whatif_result_impact_pkgCheckBox";
	private final String whatif_result_impact_cacheCheckBox = "whatif_result_impact_cacheCheckBox";
	private final String impact_dialog = "impact_dialog";
	private final String information_dialog = "information_dialog";
	private final String tuning_jobs_filter_dialog_impactType = "tuning_jobs_filter_dialog_impactType";
	private final String tuning_jobs_filter_dialog_jobStatus = "tuning_jobs_filter_dialog_jobStatus";
	private final String tuning_jobs_filter_dialog_sucJob = "tuning_jobs_filter_dialog_sucJob";
	private final String impact_jobname = "impact_jobname";
	private final String tuning_jobs_view_result_btn = "tuning_jobs_view_result_btn";
	private final String impact_ok_btn = "impact_ok_btn";
	private final String tuning_jobs_result_page_wqa_tab = "tuning_jobs_result_page_wqa_tab";
	private final String tuning_jobs_result_wqa_text="tuning_jobs_result_wqa_text";
	private final String tuning_jobs_confirm_dialog_yes="tuning_jobs_confirm_dialog_yes";

	public TKTuning(WebDriverCore driverCore) {
		super(driverCore);
	}

	public boolean isMoreFiltersDisplayed() {
		return this.isDisplayed(job_list_page, moreFilters_button);
	}

	public void clickMoreFiltersBtn() {
		this.clickBtn(job_list_page, moreFilters_button);
		driverCore.waitForPageLoad();
	}

	public void clickFilterBtn() {
		this.clickBtn(getSize(job_list_page, filter_button) - 1, job_list_page, filter_button);
		driverCore.waitForPageLoad();
	}

	public void clickResetBtn() {
		this.clickBtn(getSize(job_list_page, reset_button) - 1, job_list_page, reset_button);
	}

	public void clickViewResult() {
		this.clickBtn(job_list_page, view_result_button);
		driverCore.waitForPageLoad();
	}

	public void clickViewWorkloadStatements() {
		this.clickMenuItem(result_page, View_Workload_Statements);
		driverCore.waitForPageLoad();
	}

	public void clickOKBtn() {
		this.clickBtn(message_page, tuning_ok_button);
		driverCore.waitForPageLoad();
	}

	public void clickLaunchTuningJob() {
		this.clickBtn(launch_tuning_job);
		driverCore.waitForPageLoad();
	}

	public void clickOk_Btn() {
		this.clickBtn(tuning_ok_button);
		driverCore.waitForPageLoad();
	}

	public boolean isTuneDialogDisplayed() {
		return this.isDisplayed(tuning_dialog);
	}

	public boolean isSingleButtonDisplayed() {
		return this.isDisplayed(tuning_dialog, tune_single_button);
	}

	public boolean isWorkloadButtonDisplayed() {
		return this.isDisplayed(tuning_dialog, tune_workload_button);
	}

	public boolean isOKButtonDisabledWhenJobnameNull() {
		this.cleanSchema();
		return this.isBtnEnabled(tuning_dialog, tuning_dialog_OK);
	}

	public boolean isScrollbarDisplayed() {
		this.expandExpain();
		return this.isDisplayed(tuning_dialog, tuning_dialog_OK);
	}

	public void cleanSchema() {
		this.setTextBox(tuning_dialog, schema_input, "   ");
	}

	public void expandExpain() {
		this.clickBtn(explain_span);
	}

	public void verifyTuneDialog(TKDialog tkDialog) {
		tkDialog.waitForDialogPopup(tuning_dialog);
		this.testVerify.assertTrue(this.isTuneDialogDisplayed(), "Tune Dialog displayed!!!");
		this.testVerify.assertTrue(this.isWorkloadButtonDisplayed(), "Workload tune button displayed!!!");
		this.testVerify.assertTrue(this.isSingleButtonDisplayed(), "Single tune button displayed!!!");
		this.testVerify.assertFalse(this.isOKButtonDisabledWhenJobnameNull(), "OK button disabled!!!");
		this.testVerify.assertTrue(this.isScrollbarDisplayed(), "Scrollbar displayed!!!");
		tkDialog.closeFocusedDialog(tuning_dialog, tuning_dialog_close);
	}

	public void verifyTuningBtns() {
//		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_page_grid, tuning_jobs_viewResult_btn),
//				"View Results button shows");
//		this.testVerify.assertTrue(!this.isBtnEnabled(tuning_jobs_page_grid, tuning_jobs_viewResult_btn),
//				"View Results button disabled");
//
//		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_page_grid, tuning_jobs_retune_btn),
//				"Retune button shows");
//		this.testVerify.assertTrue(!this.isBtnEnabled(tuning_jobs_page_grid, tuning_jobs_retune_btn),
//				"Retune button disabled");
//
//		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_page_grid, tuning_jobs_compare_btn),
//				"Compare button shows");
//		this.testVerify.assertTrue(!this.isBtnEnabled(tuning_jobs_page_grid, tuning_jobs_compare_btn),
//				"Compare button disabled");

		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_page_grid, tuning_jobs_retention_btn),
				"Set retention button shows");
		this.testVerify.assertTrue(this.isBtnEnabled(tuning_jobs_page_grid, tuning_jobs_retention_btn),
				"Set retention button enable");

//		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_page_grid, tuning_jobs_cancel_btn),
//				"Cancel button shows");
//		this.testVerify.assertTrue(!this.isBtnEnabled(tuning_jobs_page_grid, tuning_jobs_cancel_btn),
//				"Cancel button disabled");
//
//		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_page_grid, tuning_jobs_delete_btn),
//				"Delete button shows");
//		this.testVerify.assertTrue(!this.isBtnEnabled(tuning_jobs_page_grid, tuning_jobs_delete_btn),
//				"Delete button disabled");
	}

	public void verifyTuningIconBtns() {
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_page_grid, tuning_jobs_search_icon),
				"Search icon button shows");
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_page_grid, tuning_jobs_refresh_icon),
				"Refresh icon button shows");
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_page_grid, tuning_jobs_column_icon),
				"Column config icon button shows");
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_page_grid, tuning_jobs_filter_icon),
				"Filter icon button shows");
	}

	/**
	 * Filter by name: singleTune/workloadTune
	 */
	public void inputFilterCondition(String jobName) {
		this.clickBtn(tuning_jobs_page_grid, tuning_jobs_filter_icon);
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_filter_dialog), "Filter dialog shows");
		this.clickBtn(tuning_jobs_filter_dialog, tuning_jobs_filter_dialog_clear_btn);
		this.clickBtn(tuning_jobs_page_grid, tuning_jobs_filter_icon);
		if (jobName != null) {
			this.clickBtn(tuning_jobs_filter_dialog, tuning_jobs_filter_dialog_jobStatus);
			this.clickBtn(tuning_jobs_filter_dialog, tuning_jobs_filter_dialog_sucJob);
			this.setTextBox(tuning_jobs_filter_dialog, tuning_jobs_filter_dialog_jobname_input, jobName);
		}
		this.clickBtn(tuning_jobs_filter_dialog, tuning_jobs_filter_dialog_filter_btn);
	}

	public void resetFilterCondition() {
		this.clickBtn(tuning_jobs_page, tuning_jobs_tab);
		this.clickBtn(tuning_jobs_page_grid, tuning_jobs_filter_icon);
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_filter_dialog), "Filter dialog shows");
		this.clickBtn(tuning_jobs_filter_dialog, tuning_jobs_filter_dialog_clear_btn);
	}

	public void inputFilterTypeCondition(String tuningType) {
		this.clickBtn(tuning_jobs_page_grid, tuning_jobs_filter_icon);
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_filter_dialog), "Filter dialog shows");
		this.clickBtn(tuning_jobs_filter_dialog, tuning_jobs_filter_dialog_clear_btn);
		this.clickBtn(tuning_jobs_page_grid, tuning_jobs_filter_icon);
		if (tuningType != null && tuningType.equals("What-if analysis")) {
			this.clickBtn(tuning_jobs_filter_dialog, tuning_jobs_filter_dialog_jobStatus);
			this.clickBtn(tuning_jobs_filter_dialog, tuning_jobs_filter_dialog_sucJob);
			this.clickBtn(tuning_jobs_filter_dialog, tuning_jobs_filter_dialog_tuningType);
			this.clickBtn(tuning_jobs_filter_dialog, tuning_jobs_filter_dialog_whatifType);
		}
		if (tuningType != null && tuningType.equals("Impact Analysis")) {
			this.clickBtn(tuning_jobs_filter_dialog, tuning_jobs_filter_dialog_jobStatus);
			this.clickBtn(tuning_jobs_filter_dialog, tuning_jobs_filter_dialog_sucJob);
			this.clickBtn(tuning_jobs_filter_dialog, tuning_jobs_filter_dialog_tuningType);
			this.clickBtn(tuning_jobs_filter_dialog, tuning_jobs_filter_dialog_impactType);
		}
		this.clickBtn(tuning_jobs_filter_dialog, tuning_jobs_filter_dialog_filter_btn);
	}

	public void verifyTuningJobResult(String jobName) {
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_page_grid, tuning_jobs_grid), "Tune jobs grid shows");
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_grid, tuning_jobs_grid_first_row_jobName),
				"The first row's job name is: " + jobName);
		this.clickLink(tuning_jobs_grid, tuning_jobs_grid_first_row_jobName);
		this.waitForPageLoadCompleted(tuning_jobs_page_grid, tuning_jobs_result_page);
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_page_grid, tuning_jobs_result_page),
				"Tune jobs result tab shows");
		int tabLen = 8;
		if (jobName.equals("singleTune"))
			tabLen = 7;
		driverCore.waitForPageLoad(3);
		this.testVerify.assertTrue(this.getSize(tuning_jobs_page_grid, tuning_jobs_result_page_navItems) == tabLen,
				"Tune jobs result with " + tabLen + " tabs");
		this.clickLink(tuning_jobs_page, tuning_jobs_result_page_close_btn);
	}
	
	public void verifyWorkloadJobWAPAResult(String jobName) {
		inputFilterCondition(jobName);
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_page_grid, tuning_jobs_grid), "Tune jobs grid shows");
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_grid, tuning_jobs_grid_first_row_jobName),
				"The first row's job name is: " + jobName);
		this.clickLink(tuning_jobs_grid, tuning_jobs_grid_first_row_jobName);
		driverCore.waitForPageLoad(2);
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_page_grid, tuning_jobs_result_page),
				"Tune jobs result tab shows");
		try {
			verifyWorkloadAPAResult();
			this.clickLink(tuning_jobs_page, tuning_jobs_result_page_close_btn);
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void verifyWorkloadJobWQAResult(String jobName) {
		inputFilterCondition(jobName);
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_page_grid, tuning_jobs_grid), "Tune jobs grid shows");
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_grid, tuning_jobs_grid_first_row_jobName),
				"The first row's job name is: " + jobName);
		this.clickLink(tuning_jobs_grid, tuning_jobs_grid_first_row_jobName);
		driverCore.waitForPageLoad(2);
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_page_grid, tuning_jobs_result_page),
				"Tune jobs result tab shows");
		try {
			verifyWorkloadQAResult();
			this.clickLink(tuning_jobs_page, tuning_jobs_result_page_close_btn);
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void verifyWorkloadQAResult() throws AWTException {
		this.clickResultTab("wqa");
		this.clickLink(tuning_jobs_result_wapa_tab, tuning_jobs_result_wapaOrQa_row);
		this.clickBtn(tuning_jobs_result_wapa_tab, tuning_jobs_result_wapa_btn);
		driverCore.waitForPageLoad(2);
		pressAlt_O();
		driverCore.waitForPageLoad(5);
		this.switchToLastWindow();
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_result_wqa_text), "wqa recommendations are displayed");
		this.closeCurrentWindow();
		this.switchToLastWindow();
	}
	
	public void verifySingleJobAPAResult(String jobName) {
		inputFilterCondition(jobName);
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_page_grid, tuning_jobs_grid), "Tune jobs grid shows");
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_grid, tuning_jobs_grid_first_row_jobName),
				"The first row's job name is: " + jobName);
		
		this.waitForPageLoadCompleted(tuning_jobs_grid, tuning_jobs_grid_first_row_jobName);
		//sometimes, clicking first row jobname fails, so add sleep time
		this.pause(1000);
		this.clickLink(tuning_jobs_grid, tuning_jobs_grid_first_row_jobName);
		this.waitForPageLoadCompleted(tuning_jobs_page_grid, tuning_jobs_result_page);
//		for(int i=0; i<10; i++)
//		{
//			if(this.isDisplayed(tuning_jobs_page_grid, tuning_jobs_result_page))
//				break;
//			else
//				driverCore.waitForPageLoad();
//				
//		}		
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_page_grid, tuning_jobs_result_page),
				"Tune jobs result tab shows");
		
//		verifySingleAPAResult();
		this.clickResultTab("recommendations");
		driverCore.waitForPageLoad(4);
		
		this.switchFrametoTargetbyWebElement(tuning_jobs_result_reommendation_iframe);		
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_result_recommendation_apa_td), "apa recommendations are displayed");		
		this.switchFrametoParentFrame();
		
		this.clickLink(tuning_jobs_page, tuning_jobs_result_page_close_btn);
		//this.clickLink(tuning_jobs_result_page_close_btn);
	}

	public void deleteAllJobs() {
		inputFilterCondition(null);
		//driverCore.waitForPageLoad(5);
		this.waitForPageLoadCompleted(tuning_jobs_grid, tuning_jobs_grid_selectAll);
		this.clickLink(tuning_jobs_grid, tuning_jobs_grid_selectAll);
		this.clickBtn(tuning_jobs_page_grid, tuning_jobs_delete_btn);
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_confirm_dialog), "Delete confirmation dialog shows");
		this.clickBtn(tuning_jobs_confirm_dialog, tuning_jobs_confirm_dialog_yes);
	}

	public void verifyTuningJobsPageInfo(Boolean isBVT) {
		String singleTune = "singleTune";
		String workloadTune = "workloadTune";
		verifyTuningBtns();
		verifyTuningIconBtns();
		inputFilterCondition(singleTune);
		verifyTuningJobResult(singleTune);
		if (!isBVT) {
			inputFilterCondition(workloadTune);
			verifyTuningJobResult(workloadTune);	
		}
		deleteAllJobs();
	}

	public void runIAWhatIfImpact(String jobName, String openTab, Boolean isWorkload) {
		// run what if job
		this.runIAWhatIf(jobName, openTab);
		driverCore.waitForPageLoad(5);
		this.clickBtn(tuning_jobs_page, tuning_jobs_tab);
		// view what if result
		String impactJobName = jobName + "_whatIf_impact";
		if( isWorkload ) {
			inputFilterTypeCondition("Workload what-if analysis");
			impactJobName = jobName + "_workload_whatIf_impact";
		} else {
			inputFilterTypeCondition("What-if analysis");
		}
		
		this.clickLink(tuning_jobs_grid, tuning_jobs_grid_first_row_jobName);
		this.clickBtn(tuning_jobs_grid, tuning_jobs_view_result_btn);
		driverCore.waitForPageLoad();
		// click impact button
		this.clickBtn(tuning_jobs_page, whatIf_result_impact_btn);
		// open impact page
		
		this.setTextBox(impact_dialog, impact_jobname, impactJobName);
		this.selectRadioButton(impact_dialog, whatif_result_impact_radio_btn);
		this.clickBtn(impact_dialog, whatif_result_impact_pkgCheckBox);
		this.clickBtn(impact_dialog, whatif_result_impact_cacheCheckBox);
		this.clickBtn(impact_dialog, impact_ok_btn);
		this.clickBtn(information_dialog, tuning_dialog_OK);

		// go to job list tab
		this.clickBtn(tuning_jobs_page, tuning_jobs_tab);
		driverCore.waitForPageLoad(5);
		inputFilterCondition(impactJobName);
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_grid, tuning_jobs_grid_first_row_jobName),
				"The first row's job name is: " + impactJobName);
		this.resetFilterCondition();

	}

	public void runIAWhatIf(String jobName, String openTab) {
		// verify tuning jobs page
		verifyTuningBtns();
		verifyTuningIconBtns();
		inputFilterCondition(jobName);

		// view tuning job result
		this.clickLink(tuning_jobs_grid, tuning_jobs_grid_first_row_jobName);
		driverCore.waitForPageLoad();
		if (openTab != null) {
			this.clickResultTab(openTab);
		}
		// click test candidate button
		this.clickBtn(tuning_jobs_page, tuning_jobs_result_page_ia_tab_analysis_btn);
		this.clickBtn(tuning_jobs_page, tuning_jobs_result_page_ia_tab_candidate_btn);
		// run what if
		this.clickBtn(tuning_jobs_page, tuning_jobs_result_whatif_runTest_btn);
		this.clickBtn(information_dialog, tuning_dialog_OK);

	}

	public void verifyWorkloadAPAResult() throws AWTException {
		this.clickResultTab("wapa");
		this.clickLink(tuning_jobs_result_wapa_tab, tuning_jobs_result_wapaOrQa_row);
		this.clickBtn(tuning_jobs_result_wapa_tab, tuning_jobs_result_wapa_btn);
		driverCore.waitForPageLoad(2);
		pressAlt_O();
		driverCore.waitForPageLoad(5);
		this.switchToLastWindow();
		this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_result_wapa_apg_span), "apa recommendations are displayed in apg");
		this.closeCurrentWindow();
		this.switchToLastWindow();
	}
	
	public void clickResultTab(String openTab) {
		// open the defined tab
		if (openTab.equals("ia")) {
			this.clickLink(tuning_jobs_page, tuning_jobs_result_page_ia_tab);
			// this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_result_page_ia_tab_content),
			// "IA has recommendations");
		}
		else if (openTab.equals("wapa")) {
			this.clickLink(tuning_jobs_page, tuning_jobs_result_page_wapa_tab);
			// this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_result_page_ia_tab_content),
			// "IA has recommendations");
		}
		else if (openTab.equals("recommendations")) {
			this.clickLink(tuning_jobs_page, tuning_jobs_result_page_recommendations_tab);
			// this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_result_page_ia_tab_content),
			// "IA has recommendations");
		}
		else if (openTab.equals("wqa")) {
			this.clickLink(tuning_jobs_page, tuning_jobs_result_page_wqa_tab);
			// this.testVerify.assertTrue(this.isDisplayed(tuning_jobs_result_page_ia_tab_content),
			// "IA has recommendations");
		}
	}
	
	public void pressAlt_O() throws AWTException{

		Robot robot = new Robot(); 		
        robot.keyPress(KeyEvent.VK_ALT); 
        robot.keyPress(KeyEvent.VK_O);

        robot.keyRelease(KeyEvent.VK_O);
        
        robot.keyPress(KeyEvent.VK_P);
        robot.keyRelease(KeyEvent.VK_P);
        robot.keyRelease(KeyEvent.VK_ALT);        
	} 
	
}
