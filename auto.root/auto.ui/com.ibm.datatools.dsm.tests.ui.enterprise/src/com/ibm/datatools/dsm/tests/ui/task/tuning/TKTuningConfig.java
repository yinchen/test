package com.ibm.datatools.dsm.tests.ui.task.tuning;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKTuningConfig  extends TKTuning {
	public TKTuningConfig(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}
	
	private final String tuning_configuration_page = "tuning_configuration_page";
	private final String tableSpaceInConfig_textbox = "tableSpaceInConfig_textbox";
	private final String statusInConfig_textbox = "statusInConfig_textbox";
	
	public boolean isTableSpaceOptionDisplayed(){
		return this.isDisplayed(tuning_configuration_page, tableSpaceInConfig_textbox);
	}
	
	public boolean isStatusOptionDisplayed(){
		return this.isDisplayed(tuning_configuration_page, statusInConfig_textbox);
	}
	
	public void verifyTuningConfigurationPageInfo() {
		this.testVerify.assertTrue(this.isTableSpaceOptionDisplayed(), 
				"Optimize tuning configuration page show table space for explain tables option displayed!!!");
		
		this.testVerify.assertTrue(this.isStatusOptionDisplayed(), 
				"Optimize tuning configuration page show status option displayed!!!");	
	}
}
