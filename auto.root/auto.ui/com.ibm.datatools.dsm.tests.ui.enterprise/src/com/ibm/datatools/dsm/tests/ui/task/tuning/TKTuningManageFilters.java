package com.ibm.datatools.dsm.tests.ui.task.tuning;


import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;


public class TKTuningManageFilters extends TKStartTuning {

	public TKTuningManageFilters(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}

	private final String choose_StatementCache_source_option="choose_StatementCache_source_option";
	private final String tuning_start_tuning_page = "tuning_start_tuning_page";
	private final String choose_manage_filters_button ="choose_manage_filters_button";
	private final String choose_manage_filters_title ="choose_manage_filters_title";
	private final String choose_filters_add_button ="choose_filters_add_button";
	private final String choose_filters_edit_button ="choose_filters_edit_button";
	private final String choose_filters_delete_button ="choose_filters_delete_button";
	private final String choose_filters_apply_button ="choose_filters_apply_button";
	private final String manage_filter_dialog_filter_name ="manage_filter_dialog_filter_name";
	private final String manage_filter_dilog_filter_name_textbox ="manage_filter_dilog_filter_name_textbox";
	private final String manage_filter_filter_dialog_ok_button ="manage_filter_filter_dialog_ok_button";
	private final String manage_filter_filter_dialog_yes_button ="manage_filter_filter_dialog_yes_button";
	private final String manage_filter_filter_dialog_close_button="manage_filter_filter_dialog_close_button";
	private final String manage_filter_myfilter ="manage_filter_myfilter";
	private final String manage_filter_checkbox ="manage_filter_checkbox";
	private final String manage_filter_delete_confirm ="manage_filter_delete_confirm";
	private final String manage_filter_applied_filter ="manage_filter_applied_filter";
	private final String FilterName = "MyFilter";
	
	public boolean isChooseCacheSourceDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, choose_StatementCache_source_option);
	}
	
	public boolean isFilterTitleDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, choose_manage_filters_title);
	}
	
	public boolean isFilterButtonDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, choose_filters_add_button);
	}
	
	public boolean isFilterDialogDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, manage_filter_dialog_filter_name);
	}
	
	public boolean isAddedFilterDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, manage_filter_myfilter);
	}
	
	public boolean isAppliedFilterDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, manage_filter_applied_filter);
	}
	
	public boolean isDeleteConfirmDialogDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, manage_filter_delete_confirm);
	}
	
	public void clickCacheBtn(){		
		this.clickBtn(tuning_start_tuning_page, choose_StatementCache_source_option);
		driverCore.waitForPageLoad();		
	}
	
	public void clickManageFilterBtn(){		
		this.clickBtn(tuning_start_tuning_page, choose_manage_filters_button);
		driverCore.waitForPageLoad();		
	}
	
	public void clickAddFilterBtn(){		
		this.clickBtn(tuning_start_tuning_page, choose_filters_add_button);
		driverCore.waitForPageLoad();		
	}
	
	public void clickEditFilterBtn(){		
		this.clickBtn(tuning_start_tuning_page, choose_filters_edit_button);
		driverCore.waitForPageLoad();		
	}
	
	public void clickApplyFilterBtn(){		
		this.clickBtn(tuning_start_tuning_page, choose_filters_apply_button);
		driverCore.waitForPageLoad();		
	}
	
	public void clickDeleteFilterBtn(){		
		this.clickBtn(tuning_start_tuning_page, choose_filters_delete_button);
		driverCore.waitForPageLoad();		
	}
	
	public void clickOKBtn(){		
		this.clickBtn(tuning_start_tuning_page, manage_filter_filter_dialog_ok_button);
		driverCore.waitForPageLoad();		
	}
	
	public void clickYesBtn(){		
		this.clickBtn(tuning_start_tuning_page, manage_filter_filter_dialog_yes_button);
		driverCore.waitForPageLoad();		
	}
	
	public void clickCloseBtn(){
		this.clickBtn(tuning_start_tuning_page, manage_filter_filter_dialog_close_button);
	}
	
	public void setFilterName(String s){	
		this.setTextBox(manage_filter_dilog_filter_name_textbox, s);
	}
	
	public void clickCheckBox() {
		//this.checkCheckBox(1, manage_filter_checkbox);
		this.checkCheckBox(manage_filter_checkbox);
	}
	
	public void verifyFilterPageInfo() {
		this.testVerify.assertTrue(this.isChooseCacheSourceDisplayed(), 
				"Optimize Start Tuning page choose cache source displayed!!!");	
		this.clickCacheBtn();
		this.testVerify.assertTrue(this.isFilterTitleDisplayed(), 
				"Optimize Start Tuning page cache filter title displayed!!!");
		this.clickManageFilterBtn();
		this.testVerify.assertTrue(this.isFilterButtonDisplayed(), 
				"Optimize Start Tuning page manage filter pane displayed!!!");
		this.clickAddFilterBtn();
		this.testVerify.assertTrue(this.isFilterDialogDisplayed(), 
				"Optimize Start Tuning page filter dialog displayed!!!");
		this.setFilterName(FilterName);		
		this.clickOKBtn();
		this.testVerify.assertTrue(this.isAddedFilterDisplayed(), 
				"Optimize Start Tuning page, add filter successfully!!!");
		this.clickCheckBox();
		this.clickApplyFilterBtn();
		this.testVerify.assertTrue(this.isAppliedFilterDisplayed(), 
				"Optimize Start Tuning page applied filter displayed!!!");
		WebDriverCore.pause(1);
		this.clickManageFilterBtn();
		WebDriverCore.pause(1);
		this.clickCheckBox();
		WebDriverCore.pause(1);
		this.clickDeleteFilterBtn();
		this.testVerify.assertTrue(this.isDeleteConfirmDialogDisplayed(), 
				"Optimize Start Tuning page, confirm dialog displayed!!!");
		this.clickYesBtn();
		this.clickCloseBtn();
	}
	
	
}








