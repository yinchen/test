package com.ibm.datatools.dsm.tests.ui.task.tuning;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKTuningMigrate extends TKTuning {
	public TKTuningMigrate(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}
	
	private final String tuning_migration_page = "tuning_migration_page";
	private final String choose_migrate_button = "choose_migrate_button";

	public boolean isChooseFileBtnDisplayed(){
		return this.isDisplayed(tuning_migration_page, tuning_migration_page);
	}
	
	public boolean isMigrateBtnDisplayed(){
		return this.isDisplayed(tuning_migration_page, choose_migrate_button);
	}
	
	public void verifyTuningMigrationPageInfo() {
		this.testVerify.assertTrue(this.isChooseFileBtnDisplayed(), 
				"Optimize tuning migration page show file upload button displayed!!!");
		this.testVerify.assertTrue(this.isMigrateBtnDisplayed(), 
				"Optimize tuning migration page show migrate button displayed!!!");
	}
}
