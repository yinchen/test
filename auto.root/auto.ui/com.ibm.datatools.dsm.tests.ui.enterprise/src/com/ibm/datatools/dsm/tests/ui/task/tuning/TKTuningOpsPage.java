package com.ibm.datatools.dsm.tests.ui.task.tuning;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKTuningOpsPage extends TKStartTuning {
	public TKTuningOpsPage(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}
	
	private final String options_singleTune = "options_singleTune";
	
	public boolean isSingleRadioBtnDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, options_singleTune);
	}
	
	public void verifyOpsForSinglePageInfo(String jobName) {
		this.testVerify.assertTrue(this.isSingleRadioBtnDisplayed(), 
				"Optimize Start Tuning options page single radio button displayed!!!");	
		this.verifyOpsCommonInfo(jobName);
	}
	
	public void verifyOpsForWorkloadPageInfo(String jobName) {
		this.verifyOpsCommonInfo(jobName);
	}
	
	public void verifyOpsCommonInfo(String jobName) {
		if (this.isJobNameTextareaDisplayed()){
			this.testVerify.assertTrue(this.isJobNameTextareaDisplayed(), 
					"Optimize Start Tuning options page job name textarea displayed!!!");
			this.SetJobName(jobName);
		}
		this.testVerify.assertTrue(this.isExplainOptionsDisplayed(), 
				"Optimize Start Tuning options page explain options displayed!!!");	
		this.testVerify.assertTrue(this.isTuningOptionsDisplayed(), 
				"Optimize Start Tuning options page tuning options displayed!!!");	
		this.testVerify.assertTrue(this.isLaunchBtnDisplayed(), 
				"Optimize Start Tuning Source page launch button displayed!!!");	
	}
}
