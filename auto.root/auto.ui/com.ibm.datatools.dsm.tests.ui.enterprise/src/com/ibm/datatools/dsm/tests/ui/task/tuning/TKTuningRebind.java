package com.ibm.datatools.dsm.tests.ui.task.tuning;


import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;


public class TKTuningRebind extends TKStartTuning {

	public TKTuningRebind(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}

	private final String tuning_start_tuning_page = "tuning_start_tuning_page";
	private final String choose_PackagePlan_source_option ="choose_PackagePlan_source_option";
	private final String manage_filter_package_additional_filter_label ="manage_filter_package_additional_filter_label";
	private final String manage_filter_package_additional_filter ="manage_filter_package_additional_filter";
	private final String manage_filter_package_additional_filter_input ="manage_filter_package_additional_filter_input";
	private final String choose_manage_filters_button ="choose_manage_filters_button";
	private final String choose_manage_filters_title ="choose_manage_filters_title";
	private final String choose_filters_add_button ="choose_filters_add_button";
	private final String choose_filters_edit_button ="choose_filters_edit_button";
	private final String choose_filters_delete_button ="choose_filters_delete_button";
	private final String choose_filters_apply_button ="choose_filters_apply_button";
	private final String manage_filter_dialog_filter_name ="manage_filter_dialog_filter_name";
	private final String manage_filter_dilog_filter_name_textbox ="manage_filter_dilog_filter_name_textbox";
	private final String manage_filter_filter_dialog_ok_button ="manage_filter_filter_dialog_ok_button";
	private final String manage_filter_filter_dialog_yes_button ="manage_filter_filter_dialog_yes_button";
	private final String manage_filter_filter_dialog_close_button="manage_filter_filter_dialog_close_button";
	private final String manage_filter_myfilter ="manage_filter_myfilter";
	private final String manage_package_filter_myfilter ="manage_package_filter_myfilter";
	private final String manage_filter_checkbox ="manage_filter_checkbox";
	private final String manage_package_filter_checkbox ="manage_package_filter_checkbox";
	private final String manage_filter_delete_confirm ="manage_filter_delete_confirm";
	private final String manage_filter_applied_filter ="manage_filter_applied_filter";
	private final String manage_package_filter_applied_filter ="manage_package_filter_applied_filter";
	private final String manage_package_filter_template_combobox ="manage_package_filter_template_combobox";
	private final String manage_package_filter_template_button ="manage_package_filter_template_button";
	private final String source_package_next_button ="source_package_next_button";
	private final String source_package_warning_dialog ="source_package_warning_dialog";
	private final String source_package_warning_dialog_yes_button ="source_package_warning_dialog_yes_button";
	private final String source_package_rebind_warning_dialog ="source_package_rebind_warning_dialog";
	private final String source_package_rebind_warning_dialog_ddl_button ="source_package_rebind_warning_dialog_ddl_button";
	private final String scope_package_layout ="scope_package_layout";
	private final String scope_package_layout_Back_button ="scope_package_layout_Back_button";
	private final String FilterName = "PackageFilter";
	
	public boolean isChoosePackageSourceDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, choose_PackagePlan_source_option);
	}
	
	public boolean isFilterTitleDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, choose_manage_filters_title);
	}
	
	public boolean isFilterButtonDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, choose_filters_add_button);
	}
	
	public boolean isFilterDialogDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, manage_filter_dialog_filter_name);
	}
	
	public boolean isAddedFilterDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, manage_package_filter_myfilter);
	}
	
	public boolean isAppliedFilterDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, manage_package_filter_applied_filter);
	}
	
	public boolean isDeleteConfirmDialogDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, manage_filter_delete_confirm);
	}
	
	public boolean isRebindWarningDialogDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, source_package_rebind_warning_dialog);
	}
	
	public boolean isWarningDialogDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, source_package_warning_dialog);
	}
	
	public boolean isScopePackageDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, scope_package_layout);
	}
	
	public void clickRebindWarningDDLBtn(){		
		this.clickBtn(tuning_start_tuning_page, source_package_rebind_warning_dialog_ddl_button);
		driverCore.waitForPageLoad();		
	}
	
	public void clickWarningYesBtn(){		
		this.clickBtn(tuning_start_tuning_page, source_package_warning_dialog_yes_button);
		driverCore.waitForPageLoad();		
	}
	
	public void clickBackBtn(){		
		this.clickBtn(tuning_start_tuning_page, scope_package_layout_Back_button);
		driverCore.waitForPageLoad();		
	}
	
	public void clickPackageBtn(){		
		this.clickBtn(tuning_start_tuning_page, choose_PackagePlan_source_option);
		driverCore.waitForPageLoad();		
	}
	
	public void clickManageFilterBtn(){		
		this.clickBtn(tuning_start_tuning_page, choose_manage_filters_button);
		driverCore.waitForPageLoad();		
	}
	
	public void clickAddFilterBtn(){		
		this.clickBtn(tuning_start_tuning_page, choose_filters_add_button);
		driverCore.waitForPageLoad();		
	}
	
	public void clickEditFilterBtn(){		
		this.clickBtn(tuning_start_tuning_page, choose_filters_edit_button);
		driverCore.waitForPageLoad();		
	}
	
	public void clickApplyFilterBtn(){		
		this.clickBtn(tuning_start_tuning_page, choose_filters_apply_button);
		driverCore.waitForPageLoad();		
	}
	
	public void clickDeleteFilterBtn(){		
		this.clickBtn(tuning_start_tuning_page, choose_filters_delete_button);
		driverCore.waitForPageLoad();		
	}
	
	public void clickOKBtn(){		
		this.clickBtn(tuning_start_tuning_page, manage_filter_filter_dialog_ok_button);
		driverCore.waitForPageLoad();		
	}
	
	public void clickNextBtn(){		
		this.clickBtn(tuning_start_tuning_page, source_package_next_button);
		driverCore.waitForPageLoad();		
	}
	
	public void clickTemplateCombobox(){		
		this.clickBtn(tuning_start_tuning_page, manage_package_filter_template_combobox);
		driverCore.waitForPageLoad();		
	}
	public void clickTemplateButton(){		
		this.clickBtn(tuning_start_tuning_page, manage_package_filter_template_button);
		driverCore.waitForPageLoad();		
	}
	
	public void clickAddFilterCombobox(){		
		this.clickBtn(6,tuning_start_tuning_page, manage_filter_package_additional_filter_label);
		driverCore.waitForPageLoad();		
	}
	
	public void clickCloseBtn(){
		this.clickBtn(tuning_start_tuning_page, manage_filter_filter_dialog_close_button);
	}
	
	public void setFilterName(String s){	
		this.setTextBox(manage_filter_dilog_filter_name_textbox, s);
	}
	
	public void setAddFilter(String s){	
		this.setTextBox(5,manage_filter_package_additional_filter, s);
	}
	
	public void setAddFilterInput(String s){	
		this.setTextBox(manage_filter_package_additional_filter_input, s);
	}
	
	public void clickCheckBox() {
		//this.checkCheckBox(1, manage_filter_checkbox);
		this.checkCheckBox(manage_package_filter_checkbox);
	}
	
	public void selectTemplate() {
		//this.checkCheckBox(1, manage_filter_checkbox);
		this.selectList(manage_package_filter_template_combobox,"Template");
	}
	
	public void verifyFilterPageInfo() throws AWTException {
		this.testVerify.assertTrue(this.isChoosePackageSourceDisplayed(), 
				"Optimize Start Tuning page choose package source displayed!!!");	
		this.clickPackageBtn();
		this.testVerify.assertTrue(this.isFilterTitleDisplayed(), 
				"Optimize Start Tuning page package filter title displayed!!!");
//		this.clickManageFilterBtn();
//		this.testVerify.assertTrue(this.isFilterButtonDisplayed(), 
//				"Optimize Start Tuning page manage filter pane displayed!!!");
//		this.clickAddFilterBtn();
//		this.testVerify.assertTrue(this.isFilterDialogDisplayed(), 
//				"Optimize Start Tuning page filter dialog displayed!!!");
//		this.setFilterName(FilterName);
		this.clickTemplateCombobox();
        this.clickTemplateButton();
		this.clickAddFilterCombobox();
        this.setAddFilter("TNAME");
        pressEnter();
        this.setAddFilterInput("SYSTABLES");
		this.clickNextBtn();
		this.testVerify.assertTrue(this.isWarningDialogDisplayed(), 
		"Optimize Start Tuning page package Warning dialog displayed!!!");
		this.clickWarningYesBtn();
		WebDriverCore.pause(7);
		this.testVerify.assertTrue(this.isRebindWarningDialogDisplayed(), 
				"Optimize Start Tuning page rebind package Warning dialog displayed!!!");
		this.clickRebindWarningDDLBtn();
		WebDriverCore.pause(2);
//		this.clickOKBtn();
//		this.testVerify.assertTrue(this.isAddedFilterDisplayed(), 
//				"Optimize Start Tuning page, add filter successfully!!!");
//		this.clickCheckBox();
//		this.clickApplyFilterBtn();
//		this.testVerify.assertTrue(this.isAppliedFilterDisplayed(), 
//				"Optimize Start Tuning page applied filter displayed!!!");
//		WebDriverCore.pause(1);
//		this.clickManageFilterBtn();
//		WebDriverCore.pause(1);
//		this.clickCheckBox();
//		WebDriverCore.pause(1);
//		this.clickDeleteFilterBtn();
//		this.testVerify.assertTrue(this.isDeleteConfirmDialogDisplayed(), 
//				"Optimize Start Tuning page, confirm dialog displayed!!!");
//		this.clickOKBtn();
//		this.clickCloseBtn();
	}
	
	public void backToSource(){
		this.testVerify.assertTrue(this.isScopePackageDisplayed(), 
				"Optimize Start Tuning page package scope displayed!!!");	
		this.clickBackBtn();
		this.testVerify.assertTrue(this.isChoosePackageSourceDisplayed(), 
				"Optimize Start Tuning page choose package source displayed!!!");
	}
	
	public void pressEnter() throws AWTException{

		Robot robot = new Robot(); 		
        robot.keyPress(KeyEvent.VK_ENTER);     
	} 
}








