package com.ibm.datatools.dsm.tests.ui.task.tuning;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.task.common.TKDialog;

public class TKTuningScopePage extends TKStartTuning{
	public TKTuningScopePage(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}
	
	private final String scope_viewStmtBtn = "scope_viewStmtBtn";
	private final String scope_tuneBtn = "scope_tuneBtn";
	
	//do the package comparison
	private final String scope_compareBtn = "scope_compareBtn";
	private final String package_compare_dialog = "package_compare_dialog";
//	private final String package_comapre_grid_first_row_checkbox = "package_comapre_grid_first_row_checkbox";
//	private final String package_comapre_grid_second_row_checkbox = "package_comapre_grid_second_row_checkbox";
//	private final String package_comapre_ok_button = "package_comapre_ok_button";
//	private final String package_comapre_close_button = "package_comapre_close_button";
//	private final String compare_package_popup_window = "compare_package_popup_window";
//	private final String compare_package_popup_window_close = "compare_package_popup_window_close";
//	private final String compare_package_popup_window_stmt = "compare_package_popup_window_stmt";
	private final String start_tuning_scope_page = "start_tuning_scope_page";
	private final String tuning_scope_grid_first_row = "tuning_scope_grid_first_row";
	
//	public boolean isViewFullStmtBtnDisplayed(){
//		return this.isDisplayed(tuning_start_tuning_page, scope_viewStmtBtn);
//	}
//	public boolean isTuneBtnDisplayed(){
//		return this.isDisplayed(tuning_start_tuning_page, scope_tuneBtn);
//	}
	
	public void verifyScopePageInfo() {
//		this.testVerify.assertTrue(this.isViewFullStmtBtnDisplayed(), 
//				"Optimize Start Tuning scope page view full statement button displayed!!!");	
//		this.testVerify.assertTrue(this.isTuneBtnDisplayed(), 
//				"Optimize Start Tuning scope page tune button displayed!!!");
		this.testVerify.assertTrue(this.isNextBtnDisplayed(), 
				"Optimize Start Tuning scope page next button displayed!!!");
	}
	
	private boolean isCompareBtnDisplayed() {
		return this.isDisplayed(tuning_start_tuning_page, scope_compareBtn);
	}
	
	private boolean isCompareDialogDisplayed() {
		return this.isDisplayed(package_compare_dialog);
	}

//	public boolean StmtTextDisplayed() {
//		return this.isDisplayed(compare_package_popup_window, compare_package_popup_window_stmt);
//	}
	public void clickComapreBtn(TKDialog tkDialog) {
		//select a record
		this.clickLink(start_tuning_scope_page, tuning_scope_grid_first_row);
		
		//click compare button
		this.testVerify.assertTrue(this.isCompareBtnDisplayed(), 
				"Optimize Start Tuning scope page compare button displayed!!!");
		this.clickBtn(tuning_start_tuning_page, scope_compareBtn);
		driverCore.waitForPageLoad();
		this.testVerify.assertTrue(this.isCompareDialogDisplayed(), 
				"Compare package access plan dialog displayed!!!");
//		//select two records to do the comparison
//		this.clickLink(package_compare_dialog, package_comapre_grid_first_row_checkbox);
//		this.clickLink(package_compare_dialog, package_comapre_grid_second_row_checkbox);
//		
//		//click ok button to submit
//		this.clickBtn(package_compare_dialog, package_comapre_ok_button);
//		
//		//popup new window
//		tkDialog.waitForDialogPopup(compare_package_popup_window);
//		this.testVerify.assertTrue(this.StmtTextDisplayed(), "Statement text displayed!!!");
//		tkDialog.closeFocusedDialog(compare_package_popup_window, compare_package_popup_window_close);
//		
//		//close package compare dialog
//		this.clickBtn(package_compare_dialog, package_comapre_close_button);

	}
}
