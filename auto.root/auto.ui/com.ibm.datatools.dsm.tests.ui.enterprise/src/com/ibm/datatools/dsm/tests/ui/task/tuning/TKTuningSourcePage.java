package com.ibm.datatools.dsm.tests.ui.task.tuning;

import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKTuningSourcePage  extends TKStartTuning{
	public TKTuningSourcePage(WebDriverCore driverCore) {
		super(driverCore);
		// TODO Auto-generated constructor stub
	}
	
	//Input sql
	private final String choose_inputsql_source_option = "choose_inputsql_source_option";
	private final String source_inputSql = "source_inputSql";
	private final String source_terminator = "source_terminator";
	private final String source_defaultSchema = "source_defaultSchema";
	private final String inputSql_textarea = "inputSql_textarea";
	
	//File source
	private final String choose_file_source_option = "choose_file_source_option";
	private final String choose_encoding_option = "choose_encoding_option";
	private final String choose_upload_button_option = "choose_upload_button_option";
	
	//Cache source
	private final String choose_StatementCache_source_option="choose_StatementCache_source_option";
	private final String manager_filters_button="manager_filters_button";
	private final String more_option="more_option";
	private final String dataShare_button="dataShare_button";
	private final String manage_connections_button="manage_connections_button";
	
	//package source
	private final String package_source_tab = "package_source_tab";
	
	public boolean isInputSqlSourceDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, choose_inputsql_source_option);
	}
	public void clickInputSqlBtn(){		
		this.clickBtn(tuning_start_tuning_page, choose_inputsql_source_option);
		driverCore.waitForPageLoad();		
	}
	public boolean isTerminatorOptionDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, source_terminator);
	}
	public boolean isDefaultSchemaOptionDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, source_defaultSchema);
	}
	public void inputSql(String sqls){
		this.setTextBox(tuning_start_tuning_page, inputSql_textarea, sqls);
	}

	public void verifyInputSqlSourcePageInfo(String sqls) {
		if(this.isInputSqlSourceDisplayed()){
			this.testVerify.assertTrue(this.isInputSqlSourceDisplayed(), 
					"Optimize Start Tuning Source page choose input SQL tab displayed!!!");
			this.clickInputSqlBtn();
		} else{
			this.testVerify.assertTrue(this.isSourceBtnDisplayed(), 
					"Optimize Start Tuning source button displayed!!!");
			this.clickSourceBtn();
		}
		this.testVerify.assertTrue(this.isTerminatorOptionDisplayed(), 
				"Optimize Start Tuning Source page terminator option displayed!!!");	
		
		this.testVerify.assertTrue(this.isDefaultSchemaOptionDisplayed(), 
				"Optimize Start Tuning Source page default schema option displayed!!!");	
		this.inputSql(sqls);
		this.testVerify.assertTrue(this.isNextBtnDisplayed(), 
				"Optimize Start Tuning Source page next button displayed!!!");	
	}
	
	public boolean isPackageSourceDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, package_source_tab);
	}
	
	public void clickPackageSourceBtn(){		
		this.clickBtn(tuning_start_tuning_page, package_source_tab);
		driverCore.waitForPageLoad();		
	}
	
	//package source
	public void verifyPackgeSourcePageInfo() {
		if(this.isPackageSourceDisplayed()){
			this.testVerify.assertTrue(this.isPackageSourceDisplayed(), 
					"Optimize Start Tuning Source page choose input SQL tab displayed!!!");
			this.clickPackageSourceBtn();
			this.testVerify.assertTrue(this.isNextBtnDisplayed(), 
					"Optimize Start Tuning Source page next button displayed!!!");	
		}
	}

	//File source
	public boolean isChooseFileSourceDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, choose_file_source_option);
	}
	public void clickFilesBtn(){		
		this.clickBtn(tuning_start_tuning_page, choose_file_source_option);
		driverCore.waitForPageLoad();		
	}
	public boolean isEncodingOptionDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, choose_encoding_option);
	}
	public boolean isUploadBtnDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, choose_upload_button_option);
	}
	
	public void verifyFileSourcePageInfo() {
		if(this.isChooseFileSourceDisplayed()){
			this.testVerify.assertTrue(this.isChooseFileSourceDisplayed(), 
					"Optimize Start Tuning Source page choose file source displayed!!!");
			this.clickFilesBtn();
		} else{
			this.testVerify.assertTrue(this.isSourceBtnDisplayed(), 
					"Optimize Start Tuning source button displayed!!!");
			this.clickSourceBtn();
		}
		this.testVerify.assertTrue(this.isEncodingOptionDisplayed(), 
				"Optimize Start Tuning page encoding option displayed!!!");	
		this.testVerify.assertTrue(this.isUploadBtnDisplayed(), 
				"Optimize Start Tuning page file upload button displayed!!!");	
	}

	//Cache source
	public boolean isChooseCacheSourceDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, choose_StatementCache_source_option);
	}
	public boolean isSourceBtnDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, source_button);
	}
	public void clickSourceBtn(){		
		this.clickBtn(tuning_start_tuning_page, source_button);
		driverCore.waitForPageLoad();		
	}
	public void clickCacheBtn(){		
		this.clickBtn(tuning_start_tuning_page, choose_StatementCache_source_option);
		driverCore.waitForPageLoad();		
	}
//	public boolean isManagerFiltersBtnDisplayed(){
//		return this.isDisplayed(tuning_start_tuning_page, manager_filters_button);
//	}
	public boolean isMoreOptionsBtnDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, more_option);
	}
	public void clickMoreOptionsBtn(){		
		this.clickBtn(tuning_start_tuning_page, more_option);
		driverCore.waitForPageLoad();		
	}
	public boolean isDataSharingBtnDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, dataShare_button);
	}
	public void clickDataSharingBtn(){		
		this.clickBtn(tuning_start_tuning_page, dataShare_button);
		driverCore.waitForPageLoad();		
	}
	public boolean isConnectionsLinkDisplayed(){
		return this.isDisplayed(tuning_start_tuning_page, manage_connections_button);
	}
	
	public void verifyCacheSourcePageInfo() {
		if(this.isChooseCacheSourceDisplayed()){
			this.testVerify.assertTrue(this.isChooseCacheSourceDisplayed(),
					"Optimize Start Tuning page choose cache source displayed!!!");
		} else {
			this.testVerify.assertTrue(this.isSourceBtnDisplayed(),
					"Optimize Start Tuning page source button displayed!!!");
			this.clickSourceBtn();
		}
		
		this.clickCacheBtn();
//		this.testVerify.assertTrue(this.isManagerFiltersBtnDisplayed(),
//				"Optimize Start Tuning page manage filters option displayed!!!");
		this.testVerify.assertTrue(this.isMoreOptionsBtnDisplayed(),
				"Optimize Start Tuning page more options button displayed!!!");
		this.clickMoreOptionsBtn();
		this.testVerify.assertTrue(this.isDataSharingBtnDisplayed(),
				"Optimize Start Tuning page data sharing button displayed!!!");
		this.clickDataSharingBtn();
		this.testVerify.assertTrue(this.isConnectionsLinkDisplayed(),
				"Optimize Start Tuning page manage database connections displayed!!!");
		this.testVerify.assertTrue(this.isNextBtnDisplayed(), 
				"Optimize Start Tuning Source page next button displayed!!!");	
	}
}
