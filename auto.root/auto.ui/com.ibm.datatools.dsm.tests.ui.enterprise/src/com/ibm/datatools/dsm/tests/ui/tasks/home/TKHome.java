package com.ibm.datatools.dsm.tests.ui.tasks.home;


import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;


public class TKHome extends TaskBase{
	
	private final String home_page = "home_page";
	private final String monitor_database_page = "monitor_database_page";

	private final String name_button = "name_button";
	private final String refreshEvery60s_button = "refreshEvery60s_button";
	private final String refreshData_button = "refreshData_button";
	private final String search_icon = "search_icon";
	private final String home_filter_button ="home_filter_button";
	private final String customize_button = "customize_button";
	private final String usefullinks_button = "usefullinks_button";
	private final String switchview_button = "switchview_button";
	private final String switchgridview_button = "switchgridview_button";
	private final String switchcardview_button = "switchcardview_button";
	private final String cardViewSort_button = "cardViewSort_button";
	private final String cardViewSortType_button = "cardViewSortType_button";
	private final String withAlertsSearch_icon = "withAlertsSearch_icon";
	private final String withAlertsGroup_button = "withAlertsGroup_button";
	private final String filterByTags_button = "filterByTags_button";
	private final String nonFavoriteSeleniumAuto_icon = "nonFavoriteSeleniumAuto_icon";
	private final String favoriteSeleniumAuto_icon = "favoriteSeleniumAuto_icon";
	private final String checkAllMetrics_span = "checkAllMetrics_span";
	private final String checkAllMetricsOK_button = "checkAllMetricsOK_button";
		
	private final String name_div = "name_div";
	private final String alerts_div = "alerts_div";
	private final String transactionRate_div = "transactionRate_div";
	private final String averageActivityTime_div = "averageActivityTime_div";
	private final String sysCPU_div = "sysCPU_div";
	private final String memory_div = "memory_div";
	private final String logic_div = "logic_div";
	private final String selenium_div = "selenium_div";
	private final String totalConnections_div = "totalConnections_div";
	private final String summDisconnected_div = "summDisconnected_div";
	private final String unavailableConnection_div = "unavailableConnection_div";
	private final String invalidCredentials_div = "invalidCredentials_div";
	private final String invalidConnection_div = "invalidConnection_div";
	private final String disconnectedIssue_div = "disconnectedIssue_div";
	private final String summWithalerts_div = "summWithalerts_div";
	private final String summNotmonitored_div = "summNotmonitored_div";
	private final String monitoringOff_div = "monitoringOff_div";
	private final String monitoringDisabled_div = "monitoringDisabled_div";
	private final String blackoutActive_div = "blackoutActive_div";
	private final String profileDeactivated_div = "profileDeactivated_div";
	private final String unsupportedVersion_div = "unsupportedVersion_div";
	private final String notMonitoreStatus_div = "notMonitoreStatus_div";
	private final String tagFilter_div = "tagFilter_div";
	private final String searchOpen_div = "searchOpen_div";
	private final String homeFilterOpen_div = "homeFilterOpen_div";
	
	protected final String seleniumdb_link = "seleniumdb_link";
	protected final String Overview_tab = "Overview_tab";
	
	private final String home_column_name = "home_column_name";

	public TKHome(WebDriverCore driverCore) {
		super(driverCore);
	}

	/*
	 *   New verification point for Home page
	 */
	public boolean isNameButtonDisplayed(){
		return this.isDisplayed(home_page, name_button);
	}
	
	public boolean isRefreshEvery60sButtonDisplayed(){
		return this.isDisplayed(home_page, refreshEvery60s_button);
	}
	
	public boolean isRefreshDataButtonDisplayed(){
		return this.isDisplayed(home_page, refreshData_button);
	}
	
	public boolean isSearchIconDisplayed(){
		return this.isDisplayed(home_page, search_icon);
	}
	
	public boolean isFilterButtonDisplayed(){
		return this.isDisplayed(home_page,home_filter_button);
	}
	
	public boolean isCustomizeButtonDisplayed(){
		return this.isDisplayed(home_page, customize_button);
	}
	
	public boolean isUsefullinksButtonDisplayed(){
		return this.isDisplayed(home_page, usefullinks_button);
	}
	
	public boolean isSwitchviewButtonDisplayed(){
		return this.isDisplayed(home_page, switchview_button);
	}
	
	public boolean isSwitchcardviewButtonDisplayed(){
		return this.isDisplayed(home_page, switchcardview_button);
	}
	
	public boolean isNameDivDisplayed(){
		return this.isDisplayed(home_page, name_div);
	}
	
	public boolean isAlertsDivDisplayed(){
		return this.isDisplayed(home_page, alerts_div);
	}
	
	public boolean isTransactionRateDivDisplayed(){
		return this.isDisplayed(home_page, transactionRate_div);
	}
	
	public boolean isAverageActivityTimeDivDisplayed(){
		return this.isDisplayed(home_page, averageActivityTime_div);
	}
	
	public boolean isSYSCPUDivDisplayed(){
		return this.isDisplayed(home_page, sysCPU_div);
	}
	
	public boolean isLogicalReadsDivDisplayed(){
		return this.isDisplayed(home_page, logic_div);
	}
	
	public boolean isMemoryDivDisplayed(){
		return this.isDisplayed(home_page, memory_div);
	}
	
	public boolean isSeleniumAutoDisplayed(){
		return this.isDisplayed(home_page, selenium_div);
	}
	
	public void clickDbLink(){
		this.clickLink(home_page, seleniumdb_link);
	}
	
	public boolean isOverviewTabDisplayed(){
		return this.isDisplayed(monitor_database_page, Overview_tab);
	}
	
	public void clickFilterByTagsButton(){
		this.clickBtn(home_page, filterByTags_button); 	
	}
	
	public boolean isTagFilterDivDisplayed(){
		return this.isDisplayed(home_page, tagFilter_div);
	}
	
	public void switchToCardView(){
		this.clickBtn(home_page, switchcardview_button);
	}
	
	public void switchToGridView(){
		this.clickBtn(home_page, switchgridview_button);
	}
	
	public boolean isCardViewSortButtonDisplayed(){
		return this.isDisplayed(home_page, cardViewSort_button);
	}
	
	public void changeCardViewSortType(){
		this.clickBtn(home_page, cardViewSortType_button);
	}
	
	public boolean isTotalConnectionsDivDisplayed(){
		return this.isDisplayed(home_page, totalConnections_div);
	}
	
	public void clickTotalConnectionsBtn(){
		this.clickTextBox(home_page, totalConnections_div);
	} 
	
	public void switchToDisconnectedView(){
		this.clickTextBox(home_page, summDisconnected_div);
	}
	
	public boolean isUnavailableConnectionDivDisplayed(){
		return this.isDisplayed(home_page, unavailableConnection_div);
	}
	
	public boolean isInvalidCredentialsDivDisplayed(){
		return this.isDisplayed(home_page, invalidCredentials_div);
	}
	
	public boolean isInvalidConnectionDivDisplayed(){
		return this.isDisplayed(home_page, invalidConnection_div);
	}
	
	public boolean isDisconnectedIssueDivDisplayed(){
		return this.isDisplayed(home_page, disconnectedIssue_div);
	}
	
	public void switchToWithAlertsView(){
		this.clickTextBox(home_page, summWithalerts_div);
	}
	
	public boolean isWithAlertsSearchIconDisplayed(){
		return this.isDisplayed(home_page, withAlertsSearch_icon);
	}
	
	public boolean isWithAlertsGroupButtonDisplayed(){
		return this.isDisplayed(home_page, withAlertsGroup_button);
	}		
	
	public void switchToNotmonitoredView(){
		this.clickTextBox(home_page, summNotmonitored_div);
	}
	
	public boolean isMonitoringOffDivDisplayed(){
		return this.isDisplayed(home_page, monitoringOff_div);
	}
	
	public boolean isMonitoringDisabledDivDisplayed(){
		return this.isDisplayed(home_page, monitoringDisabled_div);
	}
	
	public boolean isBlackoutActiveDivDisplayed(){
		return this.isDisplayed(home_page, blackoutActive_div);
	}
	
	public boolean isProfileDeactivatedDivDisplayed(){
		return this.isDisplayed(home_page, profileDeactivated_div);
	}
	
	public boolean isUnsupportedVersionDivDisplayed(){
		return this.isDisplayed(home_page, unsupportedVersion_div);
	}
	
	public boolean isNotMonitoredStatusDivDisplay(){
		return this.isDisplayed(home_page, notMonitoreStatus_div);
	}
	
	public void addSeleniumAutoToFavorite(){
		this.clickBtn(home_page, nonFavoriteSeleniumAuto_icon);
	}
	
	public boolean isSeleniumAutoFavoriteDisplayed(){
		//return this.isDisplayed(home_page, favoriteSeleniumAuto_icon);
		return this.isDisplayed(home_page, nonFavoriteSeleniumAuto_icon);
	}
	
	public void openSearchIcon(){
		this.clickBtn(home_page, search_icon);
	}
	
	public boolean isSearchOpenDivDisplayed(){
		return this.isDisplayed(home_page, searchOpen_div);
	}
	
	public void clickFilterBtn(){
		this.clickBtn(home_page, home_filter_button);
	}
	
	public boolean isFilterDropDownMenuDisplayed(){
		return this.isDisplayed(home_page, homeFilterOpen_div);
	}
	
	public void clickcustomizeMetricsBtn(){
		this.clickBtn(home_page, customize_button);
	}
	
	public boolean isCheckAllMetrcisSpanDisplayed(){
		return this.isDisplayed(home_page, checkAllMetrics_span);
	}
	
	public void clickCheckAllMetrcisSpan(){
		this.clickBtn(home_page, checkAllMetrics_span);
	}
	
	public void clickcheckAllMetricsOKBtn(){
		this.clickBtn(home_page, checkAllMetricsOK_button); 
	}
	
	public void verifyHomePageInfo(){
		this.waitForPageLoadCompleted(home_page, name_div);
		
		this.testVerify.assertTrue(this.isNameDivDisplayed(), 
				"Home page Name Div displays!!!");
		
		this.testVerify.assertTrue(this.isAlertsDivDisplayed(), 
				"Home page Alerts Div displays!!!");
		
		this.testVerify.assertTrue(this.isTransactionRateDivDisplayed(), 
				"Home page Transaction Rate Div displays!!!");

		
		this.testVerify.assertTrue(this.isAverageActivityTimeDivDisplayed(), 
				"Home page Average Activity Time Div displays!!!");
		
		this.testVerify.assertTrue(this.isSYSCPUDivDisplayed(), 
				"Home page SYS CPU Div displays!!!");
		/*  fix defect #2578
		this.testVerify.assertTrue(this.isLogicalReadsDivDisplayed(), 
				"Home page Logical reads Div displays!!!");
		*/
		/*
		this.testVerify.assertTrue(this.isMemoryDivDisplayed(), 
				"Home page Memory Div displays!!!");
		*/
	}
	
	/*
	 *  All buttons are verification point for Home page
	 */
	public void verifyHomeBtnDisp(){
			
		this.testVerify.assertTrue(this.isNameButtonDisplayed(), 
				"Home page Name button displays!!!");
		
		this.testVerify.assertTrue(this.isRefreshEvery60sButtonDisplayed(), 
				"Home page Refresh Every 60s button displays!!!");
		
		this.testVerify.assertTrue(this.isRefreshDataButtonDisplayed(), 
				"Home page Refresh Data button displays!!!");
		
		this.testVerify.assertTrue(this.isSearchIconDisplayed(), 
				"Home page Search Icon button displays!!!");
		
		this.testVerify.assertTrue(this.isFilterButtonDisplayed(), 
				"Home page Filter button displays!!!");
		
		this.testVerify.assertTrue(this.isCustomizeButtonDisplayed(), 
				"Home page Customize button displays!!!");
		/*The latest DSM removed the button.
		this.testVerify.assertTrue(this.isUsefullinksButtonDisplayed(), 
				"Home page Useful links button displays!!!");
		
		this.testVerify.assertTrue(this.isSwitchviewButtonDisplayed(), 
				"Home page SwitchTileview button displays!!!");
		*/
		this.testVerify.assertTrue(this.isSwitchcardviewButtonDisplayed(), 
				"Home page SwitchCardview button displays!!!");
	}
	
	public void verifySeleniumAutoDisp(){
		//sort the name 
		this.clickBtn(home_page, home_column_name);
		this.pause();
		this.clickBtn(home_page, home_column_name);
		this.pause();
		
		this.testVerify.assertTrue(this.isSeleniumAutoDisplayed(), 
				"SeleniumAuto DB displays!!!");
	}
		
	
	
	
	
	public void verifyTagFilterDivDisp(){
		this.testVerify.assertTrue(isTagFilterDivDisplayed(), 
				"Tag Filter div displays!!!");
	}
	
	public void verifyCardViewDivDisp(){
		this.testVerify.assertTrue(this.isCardViewSortButtonDisplayed(),
				"Card view Sort button displays!!!");
	}
	
	public void verifySummaryViewTabDisp(){
		this.testVerify.assertTrue(this.isTotalConnectionsDivDisplayed(),
				"All connections tab displays!!!");
	}
	
	public void verifySummDisconnectedDisp(){
		this.testVerify.assertTrue(this.isUnavailableConnectionDivDisplayed(),
				"Disconnected summary view Unavailable connection Div displays!!!");
		
		this.testVerify.assertTrue(this.isInvalidCredentialsDivDisplayed(),
				"Disconnected summary view Invaild credentials Div displays!!!");
		
		this.testVerify.assertTrue(this.isInvalidConnectionDivDisplayed(),
				"Disconnected summary view Invaild connection Div displays!!!");
		
		this.testVerify.assertTrue(this.isDisconnectedIssueDivDisplayed(),
				"Disconnected summary view Issue Div of header displays!!!");
	}
	
	public void verifySummWithAlertsDisp(){
		this.testVerify.assertTrue(this.isWithAlertsSearchIconDisplayed(),
				"With alerts summary view Search icon displays!!!");
		
		this.testVerify.assertTrue(this.isWithAlertsGroupButtonDisplayed(),
				"With alerts summary view Group button displays!!!");
	}

	public void verifySummNotMonitoredDisp(){
		this.testVerify.assertTrue(this.isMonitoringOffDivDisplayed(),
				"Not monitored view Monitoring off Div displays!!!");
		
		this.testVerify.assertTrue(this.isMonitoringDisabledDivDisplayed(),
				"Not monitored view Monitoring is disabled Div displays!!!");
		
		this.testVerify.assertTrue(this.isBlackoutActiveDivDisplayed(),
				"Not monitored view Blackout active Div displays!!!");
		
		this.testVerify.assertTrue(this.isProfileDeactivatedDivDisplayed(),
				"Not monitored view Monitoring profile deactivated Div displays!!!");
		
		this.testVerify.assertTrue(this.isUnsupportedVersionDivDisplayed(),
				"Not monitored view Unsupported server version Div displays!!!");
		
		this.testVerify.assertTrue(this.isNotMonitoredStatusDivDisplay(),
				"Not monitored summary view Status Div of header displays!!!");	
	}
	
	public void verifySeleniumAutoFavoriteDisp(){
		//this.testVerify.assertTrue(this.isSeleniumAutoFavoriteDisplayed(), 
				//"Add favorite to connection SeleniumAuto succeed!!!");
		this.testVerify.assertTrue(this.isSeleniumAutoFavoriteDisplayed(),
				"Favorite tag displayed!!!");
	}
	
	public void verifySearchOpenDivDisp(){
		this.testVerify.assertTrue(this.isSearchOpenDivDisplayed(),
				"Open search box succeed!!!");
	}
	
	public void verifyFilterDropDownMenuDisp(){
		this.testVerify.assertTrue(this.isFilterDropDownMenuDisplayed(),
				"Open filter dropdown menu succeed!!!");
	}
	
	public void verifyisCheckAllMetrcisSpanDisp(){
		this.testVerify.assertTrue(this.isCheckAllMetrcisSpanDisplayed(),
				"Open customize metrics button succeed!!!");
	}
	
	
}