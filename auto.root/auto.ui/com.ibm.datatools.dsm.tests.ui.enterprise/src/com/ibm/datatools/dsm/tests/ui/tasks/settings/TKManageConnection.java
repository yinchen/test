package com.ibm.datatools.dsm.tests.ui.tasks.settings;

import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKManageConnection extends TaskBase{
	
	private final String manage_connection_page = "manage_connection_page";	
	
	private final String DatabaseConnection_tab = "DatabaseConnection_tab";
	private final String InstanceConnection_tab = "InstanceConnection_tab";	
	private final String ValidateConfiguration_button = "ValidateConfiguration_button";
	private final String QueryTuning_button = "QueryTuning_button";
	private final String Blackout_button = "Blackout_button";
	private final String ActivateLicense_button = "ActivateLicense_button";
	private final String ValidateCredentials_button = "ValidateCredentials_button";
	private final String connection_add_button = "connection_add_button" ;
	private final String connection_edit_button = "connection_edit_button" ;
	private final String connection_delete_button = "connection_delete_button" ;
	
	public TKManageConnection(WebDriverCore driverCore) {
		super(driverCore);
	}
	
	public boolean isDatabaseConnectionTabDisplayed(){
		return this.isDisplayed(manage_connection_page, DatabaseConnection_tab);
	}
	
	public boolean isInstanceConnectionTabDisplayed(){
		return this.isDisplayed(manage_connection_page, InstanceConnection_tab);
	}
	
	public boolean isAddButtonDisplayed(){
		return this.isDisplayed(manage_connection_page, connection_add_button);
	}
	
	public boolean isEditButtonDisplayed(){
		return this.isDisplayed(manage_connection_page, connection_edit_button);
	}
	
	public boolean isDeleteButtonDisplayed(){
		return this.isDisplayed(manage_connection_page,  connection_delete_button);
	}
	
	public boolean isValidateConfigurationButtonDisplayed(){
		return this.isDisplayed(manage_connection_page, ValidateConfiguration_button);
	}
	
	public boolean isQueryTuningButtonDisplayed(){
		return this.isDisplayed(manage_connection_page, QueryTuning_button);
	}
	
	public boolean isBlackoutButtonDisplayed(){
		return this.isDisplayed(manage_connection_page, Blackout_button);
	}
	
	public boolean isActivateLicenseButtonDisplayed(){
		return this.isDisplayed(manage_connection_page, ActivateLicense_button);
	}
	
	public boolean isValidateCredentialsButtonDisplayed(){
		return this.isDisplayed(manage_connection_page, ValidateCredentials_button);
	}
	
	
	public void verifyManageConnectionsPageInfo(){
		
		this.testVerify.assertTrue(this.isDatabaseConnectionTabDisplayed(), 
				"Manage Connection page Database Connection Tab displays!!!");
		
		this.testVerify.assertTrue(this.isInstanceConnectionTabDisplayed(), 
				"Manage Connection page RepositoryDatabaseConfiguration tab displays!!!");
		
		this.testVerify.assertTrue(this.isAddButtonDisplayed(), 
				"Manage Connection page Instance Connection Tab displays!!!");
		
//		this.testVerify.assertTrue(this.isEditButtonDisplayed(), 
//				"Manage Connection page Edit Button displays!!!");
//		
//		this.testVerify.assertTrue(this.isDeleteButtonDisplayed(), 
//				"Manage Connection page Delete Button displays!!!");
		
		/*this.testVerify.assertTrue(this.isValidateConfigurationButtonDisplayed(), 
				"Manage Connection page Validate Configuration Button displays!!!");*/
		
		/*this.testVerify.assertTrue(this.isQueryTuningButtonDisplayed(), 
				"Manage Connection page Query Tuning Button displays!!!"); */
		
		/*this.testVerify.assertTrue(this.isBlackoutButtonDisplayed(), 
				"Manage Connection page Blackout Button displays!!!");*/
		
		/*this.testVerify.assertTrue(this.isActivateLicenseButtonDisplayed(), 
				"Manage Connection page Activate License Button displays!!!");*/
		
		//comment ValidateCredentialsButton test verify for zh#2060
		/*this.testVerify.assertTrue(this.isValidateCredentialsButtonDisplayed(), 
				"Manage Connection page Validate Credentials Button displays!!!");*/
		
	}

}










