package com.ibm.datatools.dsm.tests.ui.tasks.settings;

import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKMonitoringProfiles extends TaskBase{
	
	private final String monitoring_profiles_page = "monitoring_profiles_page";	
	
	private final String monitoring_profiles_new_button = "monitoring_profiles_new_button";
	private final String monitoring_profiles_edit_button = "monitoring_profiles_edit_button";	
	//add for TestCommon case verifying
	private final String monitoring_profiles_delete_button = "monitoring_profiles_delete_button";
	private final String monitoring_profiles_clone_button = "monitoring_profiles_clone_button";
	private final String monitoring_profiles_assign_button = "monitoring_profiles_assign_button";

	
	private final String monitoring_profiles_setdefault_button = "monitoring_profiles_setdefault_button";
	private final String monitoring_profiles_mongo_type = "monitoring_profiles_mongo_type";
	private final String monitoring_profiles_pg_type = "monitoring_profiles_pg_type";
	private final String monitoring_profiles_mongo_default_profile = "monitoring_profiles_mongo_default_profile";
	private final String monitoring_profiles_mongo_default_tab = "monitoring_profiles_mongo_default_tab";
	private final String monitoring_profiles_mongo_type_expand = "monitoring_profiles_mongo_type_expand";
	
	public TKMonitoringProfiles(WebDriverCore driverCore) {
		super(driverCore);
	}
	
	public boolean isNewButtonEnabled(){
		return this.isBtnEnabled(monitoring_profiles_page, monitoring_profiles_new_button);
	}
	
	public boolean isEditButtonDisabled(){
		return this.isBtnEnabled(monitoring_profiles_page, monitoring_profiles_edit_button);
	}
	// add for TestCommon case verifying
	public boolean isDeleteButtonDisplayed(){
		return this.isDisplayed(monitoring_profiles_page, monitoring_profiles_delete_button);
	}
	

	public boolean isCloneButtonDisplayed(){
		return this.isDisplayed(monitoring_profiles_page, monitoring_profiles_clone_button);
	}
	

	public boolean isAssignButtonDisplayed(){
		return this.isDisplayed(monitoring_profiles_page, monitoring_profiles_assign_button);
	}
	
	
	
	public boolean isSetDefaultProfileButtonDisabled(){
		return this.isBtnEnabled(monitoring_profiles_page, monitoring_profiles_setdefault_button);
	}
	
	public boolean isMongoDBTypeDisplayed(){
		return this.isDisplayed(monitoring_profiles_page,  monitoring_profiles_mongo_type);
	}
	
	public boolean isPostgreSQLTypeDisplayed(){
		return this.isDisplayed(monitoring_profiles_page, monitoring_profiles_pg_type);
	}
	
	public void selectMongoDBType(){
		clickBtn(monitoring_profiles_mongo_type_expand);
	}
	
	public void selectDefaultMongoDBProfile(){
		clickBtn(monitoring_profiles_mongo_default_profile);
	}
	
	public void clickEditBtn(){
		clickBtn(monitoring_profiles_edit_button);
	}
	
	public boolean isMongoDefaultTabDisplayed(){
		return this.isDisplayed(monitoring_profiles_page, monitoring_profiles_mongo_default_tab);
	}
	
	public void verifyMonitoringProfilesPageInfo(){
		
		this.testVerify.assertTrue(this.isNewButtonEnabled(), 
				"Monitoring Profiles page New button enabled!!!");
		
		this.testVerify.assertTrue(this.isEditButtonDisabled(), 
				"Monitoring Profiles page Edit button disabled!!!");
		
		this.testVerify.assertTrue(this.isSetDefaultProfileButtonDisabled(), 
				"Monitoring Profiles page SetDefaultProfile button disabled!!!");
		/*215 does not support opensource 
		this.testVerify.assertTrue(this.isMongoDBTypeDisplayed(), 
				"Monitoring Profiles page MongoDB Type displays!!!");
		
		this.testVerify.assertTrue(this.isPostgreSQLTypeDisplayed(), 
				"Monitoring Profiles page PostgreSQL Type displays!!!");
		*/
	}
	
	public void verifyMonitoringProfilesEditPageInfo(){
		this.testVerify.assertTrue(this.isMongoDefaultTabDisplayed(), 
				"Monitoring Profiles page MongoDB Edit Tab displays!!!");
	}
	
	public void verifyMonitoringProfilesPageInfoForCommon(){
		this.testVerify.assertTrue(this.isNewButtonEnabled(), 
				"Monitoring Profiles page New button displays!!!");
		this.testVerify.assertTrue(this.isEditButtonDisabled(), 
				"Monitoring Profiles page Edit button displays!!!");

		this.testVerify.assertTrue(this.isDeleteButtonDisplayed(), 
				"Monitoring Profiles page Delete button displays!!!");
		this.testVerify.assertTrue(this.isCloneButtonDisplayed(), 
				"Monitoring Profiles page Clone button displays!!!");
		this.testVerify.assertTrue(this.isAssignButtonDisplayed(), 
				"Monitoring Profiles page Assign button displays!!!");
	}
}










