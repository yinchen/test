package com.ibm.datatools.dsm.tests.ui.tasks.settings;

import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TKProductSetup extends TaskBase{

	private final String product_setup_page = "product_setup_page";	
	
	private final String RepositoryDatabaseConfiguration_menu = "RepositoryDatabaseConfiguration_menu";
	private final String SystemPortsConfiguration_menu = "SystemPortsConfiguration_menu";
	private final String EmailServer_menu = "EmailServer_menu";
	private final String SNMPServer_menu = "SNMPServer_menu";
	
	
	public TKProductSetup(WebDriverCore driverCore) {
		super(driverCore);
	}	
	
	public boolean isRepositoryDatabaseConfigurationTabDisplayed(){
		return this.isDisplayed(product_setup_page, RepositoryDatabaseConfiguration_menu);
	}
	
	public boolean isSystemPortsConfigurationTabDisplayed(){
		return this.isDisplayed(product_setup_page, SystemPortsConfiguration_menu);
	}
	
	public boolean isEmailServerOptionDisplayed(){
		return this.isDisplayed(product_setup_page, EmailServer_menu);
	}
	
	public boolean isSNMPServerOptionDisplayed(){
		return this.isDisplayed(product_setup_page, SNMPServer_menu);
	}
	
	
	public void verifyProductSetupPageInfo(){
		
		this.testVerify.assertTrue(this.isRepositoryDatabaseConfigurationTabDisplayed(), 
				"Product Setup page RepositoryDatabaseConfiguration tab displays!!!");	
		
		this.testVerify.assertTrue(this.isSystemPortsConfigurationTabDisplayed(), 
				"Product Setup page SystemPortsConfiguration tab displays!!!");	
		
		this.testVerify.assertTrue(this.isEmailServerOptionDisplayed(), 
				"Product Setup page EmailServer option displays!!!");	
		
		this.testVerify.assertTrue(this.isSNMPServerOptionDisplayed(), 
				"Product Setup page SNMPServer option displays!!!");	
		
	}

}
