package com.ibm.datatools.dsm.tests.ui.tasks.settings;

import com.ibm.datatools.dsm.tests.ui.task.base.TaskBase;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.task.common.TKDialog;

public class TKUsersandPrivileges extends TaskBase{

	private final String users_privileges_page = "users_privileges_page";
	
	
	private final String users_privileges_add_user_button = "users_privileges_add_user_button";
	private final String users_privileges_userid_textbox = "users_privileges_userid_textbox";
	private final String new_password_textbox = "new_password_textbox";
	private final String confirm_password_textbox = "confirm_password_textbox";
	private final String administrator_radiobutton = "administrator_radiobutton";
	private final String user_radiobutton = "user_radiobutton";
	private final String users_privileges_search_textbox = "users_privileges_search_textbox";
	private final String users_privileges_search_result = "users_privileges_search_result";
	
	public TKUsersandPrivileges(WebDriverCore driverCore) {
		super(driverCore);
	}
	
	
	public void addUser(String username, String password, String privileges, TKDialog dialog){
		
		//handle the tour screen
		dialog.handleQuickTourDialog();
		
		this.setTextBox(users_privileges_page, users_privileges_search_textbox, username);		
		//the added user exists, just for update
		if(this.isDisplayed(users_privileges_page, users_privileges_search_result+split+username)){
			this.clickMenuItem(users_privileges_page, users_privileges_search_result+split+username);
			this.addUserInfo(password, privileges);	
			this.scrolledIntoView(users_privileges_page, save_button);
			this.clickBtn(users_privileges_page, save_button);
			TaskBase.driverCore.waitForPageLoad();
		}
		else{
			//the added user does not exist, add a new one 
			this.clickBtn(users_privileges_page, users_privileges_add_user_button);
			this.setTextBox(users_privileges_userid_textbox, users_privileges_userid_textbox, username);
			this.addUserInfo(password, privileges);	
			this.clickBtn(users_privileges_page, add_user_button);
			TaskBase.driverCore.waitForPageLoad();
		}
		dialog.clickClosebtnonInformationDialog();
	}
	
	private void addUserInfo(String password, String privileges){
		this.setTextBox(users_privileges_page, new_password_textbox, password);
		this.setTextBox(users_privileges_page, confirm_password_textbox, password);
		if(privileges.equalsIgnoreCase("Administrator"))
			this.selectRadioButton(users_privileges_page, administrator_radiobutton);
		else
			this.selectRadioButton(users_privileges_page, user_radiobutton);		
		
	}
	

}
