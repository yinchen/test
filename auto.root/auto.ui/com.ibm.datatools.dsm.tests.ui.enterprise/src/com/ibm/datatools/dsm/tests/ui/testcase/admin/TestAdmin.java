package com.ibm.datatools.dsm.tests.ui.testcase.admin;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.FileTools;
import com.ibm.datatools.test.utils.TestLogger;


public class TestAdmin extends TestCaseBase {
	
	@BeforeClass
	public void beforetest(){
		tkHeader.pickDB(this.dbProfile);
		this.tkMenu.selectMenuwithoutLoadPage("root->administer->root");
		
		if(!currentUser.equals(adminUser)){
			this.tkMenu.selectMenuwithoutLoadPage("root->instance");
			this.tkDialog.handleDialogUser(this.dbProfile, this.dbUserName,this.dbPassword);
			WebDriverCore.pause(4);
		}		
	}
	
	@DataProvider(name = "testdata")
	public Object[][] loadJSON() throws FileNotFoundException, IOException {
		// return multiple JSON post propertieso 
		String propertyFileName = "createTable.properties";
		return FileTools.readProperties(this.loadJSONPostData(propertyFileName));
	}
	
	
	
	/**
	 * Test Verify Administer Instance page
	 * 1. It has "Start" button with enabled 
	 * 2. It has "Stop" button with enabled 
	 * 3. It has "Quiesce" button with enabled 
	 * 4. It has "Unqiuesce" button with enabled 
	 * 5. It has "Create a new database" button with enabled 
	 * 6. It has "Configuration parameters" link 
	 * 7. It has "Configure BLU Acceleration" link
	 * @throws Throwable 
	 */
	@Test
	public void testAdminInstancePageInfo(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Administer Instance page... = = = = = = = =");		
		tkMenu.selectMenu("root->instance", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);		
		tkAdminInstancePage.verifyAdminInstancePageInfo();
		TestLogger.logInfo("= = = = = = = = End the test: Test Administer Instance page... = = = = = = = =");
	}
	
	
	/**
	 * Test Verify Administer Database page
	 * 1. It has "Activate" button with enabled 
	 * 2. It has "Deactivate" button with enabled 
	 * 3. It has "Backup" button with enabled 
	 * 4. It has "Restore" button with enabled 
	 * 5. It has "Quiesce" button with enabled 
	 * 6. It has "Unquiesce" button with enabled 
	 * 7. It has "Recover" button with enabled 
	 * 8. It has "Roll Forward" button with enabled 
	 * 9. It has "Revalidate" button with enabled 
	 * 10. It has "Configure Logging" button with enabled 
	 * 11. It has "Configuration parameters" link
	 */
	@Test
	public void testAdminDatabaseInfo(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Administer Database page... = = = = = = = =");		
		tkMenu.selectMenu("root->admin_database", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);		
		tkAdminDatabasePage.verifyAdminDatabasePageInfo();	
		TestLogger.logInfo("= = = = = = = = End the test: Test Administer Database page... = = = = = = = =");	
	}
	
	
	/**
	 * Test Verify Administer Indexes page and verify
	 * 1. It has "Create" button with enabled 
	 * 2. It has "Track Changes" button with enabled 
	 * 3. It has "Show System Objects" button with enabled
	 */
	@Test
	public void testAdminIndexesInfo(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Administer Indexes page... = = = = = = = =");		
		tkMenu.selectMenu("root->indexes", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);		
		tkAdminIndexesPage.verifyIndexesPageInfo();
		TestLogger.logInfo("= = = = = = = = End the test: Test Administer Indexes page... = = = = = = = =");	
	}
	
	
	/**
	 * Test Verify Administer Tables page
	 * 1. It has "Create" button with enabled 
	 * 2. It has "Track Changes" button with enabled 
	 * 3. It has "Show System Objects" button with enabled
	 * @throws Throwable 
	 */
	@Test
	public void testAdminTablesInfo(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Administer Tables page... = = = = = = = =");
		tkMenu.selectMenu("root->tables", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		tkAdminTablesPage.verifyTablesPageInfo();	
		TestLogger.logInfo("= = = = = = = = End the test: Test Administer Tables page... = = = = = = = =");
	}
	
	
	/**
	 * Test Verify Administer Privileges for LUW
	 * 1. "Add" and "Remove" buttons are displayed 
	 * 2. After clicking "Add", a dialog is popup
	 * 3. A new tab will be added if "OK" btn clicked
	 * @throws Throwable 
	 */
	@Test
	public void testAdminPrivilegesInfo(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test New React Administer Privileges page for LUW... = = = = = = = =");		
		tkMenu.selectMenu("root->privileges", this.tkDialog, this.dbProfile, this.dbUserName, this.dbPassword);		
		tkAdminPrivilegesPage.verifyPrivilegesInfo();//this case need to be updated to fit new design
		TestLogger.logInfo("= = = = = = = = End the test: Test New React Administer Privileges page for LUW... = = = = = = = =");	
		
	}
	
	/**
	 * Test Verify Administer Trigger page
	 * 1. It has "Create" button with enabled 
	 * 2. It has "Revalidate" button with enabled 
	 * 3. It has "Track Changes" button with enabled
	 * @throws Throwable 
	 */
	@Test
	public void testAdminTriggersInfo(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test Administer Trigger page ... = = = = = = = =");		
		tkMenu.selectMenu("root->applicationobjects->triggers", this.tkDialog, this.dbProfile, this.dbUserName, this.dbPassword);		
		tkAdminTriggersPage.verifyTriggersInfo();//this case need to be updated to fit new design
		TestLogger.logInfo("= = = = = = = = End the test:  Test Administer Trigger page ... = = = = = = = =");	
	}
	
	/**
	    * 
	    * 1. Go to Administer/Tables page
		* 2. Click Create button
		* 3. Input table name
		* 4. Add two columns 
		* 5. Click Run button 
	 * @throws SQLException 
	 * @throws NumberFormatException 
	*/
	@Test(dataProvider = "testdata")
	public void testAdminCreateTableExecution(Object key,Object inputPara) {
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Administer Create page... = = = = = = = =");	
		tkMenu.selectMenu("root->tables", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		tkAdminTablesPage.createTable(inputPara, this.dbProfile, this.tkDialog);
		TestLogger.logInfo("= = = = = = = = End the test: Test Administer Create page... = = = = = = = =");	

	}	
	
	@AfterClass
	public void afetertest(){
		this.tkMenu.selectMenuwithoutLoadPage("root->administer->root");
	}
}












