package com.ibm.datatools.dsm.tests.ui.testcase.admin;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;

/**
 * Test Verify Administer WLM page
 */
public class TestWLM extends TestCaseBase {

	@BeforeClass
	public void beforetest() {
		tkHeader.pickDB(this.dbProfile);
	}

	/**
	 * Test has "Add" button with enabled
	 * 
	 * @throws Throwable
	 */
	@Test
	public void testWorkloadsPage() {
		TestLogger.logInfo("= = = = = = = = Start the test: Test Administer WLM page... = = = = = = = =");
		tkMenu.selectMenu("root->administer->workloads", this.tkDialog, this.dbProfile, this.dbUserName, this.dbPassword);
		tkAdminWLMPage.verifyBtnExists("wlm_add_button");
	}

	/**
	 * Test has "Drop" button with enabled
	 * 
	 * @throws Throwable
	 */
	@Test
	public void testWLMDrop() {
		tkAdminWLMPage.verifyBtnExists("wlm_drop_button");
	}

	/**
	 * Test has "enable " button with enabled
	 * 
	 * @throws Throwable
	 */
	@Test
	public void testWLMEnable() {
		tkAdminWLMPage.verifyBtnExists("wlm_enable_button");
	}

	/**
	 * Test has "disable " button with enabled
	 * 
	 * @throws Throwable
	 */
	@Test
	public void testWLMDisable() {
		tkAdminWLMPage.verifyBtnExists("wlm_disable_button");
	}

	/**
	 * Test has "setup " button with enabled
	 * 
	 * @throws Throwable
	 */
	@Test
	public void testWLMSetup() {
		tkAdminWLMPage.verifyBtnExists("wlm_setup_button");
	}
}
