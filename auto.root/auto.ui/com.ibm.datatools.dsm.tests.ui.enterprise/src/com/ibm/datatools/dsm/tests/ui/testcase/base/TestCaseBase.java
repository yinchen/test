package com.ibm.datatools.dsm.tests.ui.testcase.base;

import java.awt.AWTException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Configurator;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.ibm.datatools.dsm.tests.ui.framework.base.Configuration;
import com.ibm.datatools.dsm.tests.ui.framework.base.DBUtils;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverFactory;
import com.ibm.datatools.dsm.tests.ui.task.admin.TKAdmin;
import com.ibm.datatools.dsm.tests.ui.task.admin.TKAdminDatabase;
import com.ibm.datatools.dsm.tests.ui.task.admin.TKAdminIndexes;
import com.ibm.datatools.dsm.tests.ui.task.admin.TKAdminInstance;
import com.ibm.datatools.dsm.tests.ui.task.admin.TKAdminPrivileges;
import com.ibm.datatools.dsm.tests.ui.task.admin.TKAdminTables;
import com.ibm.datatools.dsm.tests.ui.task.admin.TKAdminTriggers;
import com.ibm.datatools.dsm.tests.ui.task.admin.TKAdminWLM;
import com.ibm.datatools.dsm.tests.ui.task.common.TKDialog;
import com.ibm.datatools.dsm.tests.ui.task.common.TKHeader;
import com.ibm.datatools.dsm.tests.ui.task.common.TKLogin;
import com.ibm.datatools.dsm.tests.ui.task.common.TKMenu;
import com.ibm.datatools.dsm.tests.ui.task.common.TKSSL;
import com.ibm.datatools.dsm.tests.ui.task.help.TKAbout;
import com.ibm.datatools.dsm.tests.ui.task.help.TKWelcome;
import com.ibm.datatools.dsm.tests.ui.task.jobs.TKJobs;
import com.ibm.datatools.dsm.tests.ui.task.monitor.TKAlerts;
import com.ibm.datatools.dsm.tests.ui.task.monitor.TKMonitorDatabase;
import com.ibm.datatools.dsm.tests.ui.task.runsql.TKRunSQL;
import com.ibm.datatools.dsm.tests.ui.task.tuning.TKEOJob;
import com.ibm.datatools.dsm.tests.ui.task.tuning.TKRunTuningJob;
import com.ibm.datatools.dsm.tests.ui.task.tuning.TKStartTuning;
import com.ibm.datatools.dsm.tests.ui.task.tuning.TKTuning;
import com.ibm.datatools.dsm.tests.ui.task.tuning.TKTuningSourcePage;
import com.ibm.datatools.dsm.tests.ui.task.tuning.TKTuningScopePage;
import com.ibm.datatools.dsm.tests.ui.task.tuning.TKTuningOpsPage;
import com.ibm.datatools.dsm.tests.ui.task.tuning.TKTuningRebind;
import com.ibm.datatools.dsm.tests.ui.task.tuning.TKTuningConfig;
import com.ibm.datatools.dsm.tests.ui.task.tuning.TKTuningMigrate;
import com.ibm.datatools.dsm.tests.ui.task.tuning.TKTuningManageFilters;
import com.ibm.datatools.dsm.tests.ui.tasks.home.TKHome;
import com.ibm.datatools.dsm.tests.ui.tasks.settings.TKManageConnection;
import com.ibm.datatools.dsm.tests.ui.tasks.settings.TKMonitoringProfiles;
import com.ibm.datatools.dsm.tests.ui.tasks.settings.TKProductSetup;
import com.ibm.datatools.dsm.tests.ui.tasks.settings.TKUsersandPrivileges;
import com.ibm.datatools.test.utils.Properties;
import com.ibm.datatools.test.utils.TestLogger;

public class TestCaseBase {
	
	//Initialize log4j configuration file
	static{		
		try {
			String resFile = Configuration.getLog4jConfigFilePath();
			File logConfig = new File(resFile);
			ConfigurationSource source = new ConfigurationSource(new FileInputStream(logConfig), logConfig);
			Configurator.initialize(null, source);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	private String headless = Properties.getConfigValue("headless");	
	private int waitTimeout = Integer.parseInt(Properties.getConfigValue("mte.pauseTime.ms"));
	private int scriptTimeout = Integer.parseInt(Properties.getConfigValue("mte.timeOut.second"));
	private int pageLoadTimeout = Integer.parseInt(Properties.getConfigValue("mte.loadTime.second"));
	private String dsmserver = Properties.getConfigValue("dsmserver");
	private int dsmport = Integer.parseInt(Properties.getConfigValue("dsmport"));
	private String login = Properties.getConfigValue("login");
	
	//For DSM users: admin user and normal user
	protected static String adminUser = Properties.getConfigValue("adminUser");
	protected static String adminPassword = Properties.getConfigValue("adminPassword");	
	protected static String loginUser = Properties.getConfigValue("LoginUser");
	protected static String loginPassword = Properties.getConfigValue("LoginPassword");
	protected static String currentUser = null;
	protected static String currentPassword = null;
	
	//For DSM REPO DB information
	protected String repoDatabase = Properties.getConfigValue("repoDatabase");
	protected String repoUsername = Properties.getConfigValue("repoUsername");	
	protected String repoPassword = Properties.getConfigValue("repoPassword");
	protected int repoPort = Integer.parseInt(Properties.getConfigValue("repoPort"));
	protected String repoIP = Properties.getConfigValue("repoIP");
	
	protected String dbProfile;
	protected String dbUserName;
	protected String dbPassword;
	
	
	
	//This is for web driver
	protected static WebDriverCore driverCore;
	
	//This is for Task layer
	//For Common
	protected TKLogin tkLoginPage;
	
	protected TKMenu tkMenu;	
	protected TKHeader tkHeader;
	protected TKDialog tkDialog;
	protected TKProductSetup tkProductSetupPage;
	protected TKManageConnection tkManageConnectionPage;
	protected TKUsersandPrivileges tkUsersandPrivilegesPage;
	protected TKMonitoringProfiles tkMonitoringProfiles;

	
	//For help
	protected TKWelcome tkWelcomePage;
	protected TKAbout tkAbout;
	
	//For Admin
	protected TKAdmin tkAdmin;
	protected TKAdminDatabase tkAdminDatabasePage;
	protected TKAdminIndexes tkAdminIndexesPage;
	protected TKAdminTables tkAdminTablesPage;
	protected TKAdminPrivileges tkAdminPrivilegesPage;
	protected TKAdminInstance tkAdminInstancePage;
	protected TKAdminWLM tkAdminWLMPage;
	protected TKAdminTriggers tkAdminTriggersPage;
		
	//For Monitor
	protected TKMonitorDatabase tkMonitorDatabasePage;
	protected TKHome tkHomePage;
	protected TKAlerts tkAlertsPage;
	
	//For Tunning
	protected TKRunSQL tkRunSQLPage;
	protected TKJobs tkJobsPage;
	protected TKEOJob tkEOJobPage;
	protected TKTuningManageFilters tkFilterPage;
	protected TKTuningRebind tkRebindPage;
	
    protected TKTuning tkTuningJobs;
	protected TKTuningConfig tkConfigPage;
	protected TKTuningMigrate tkMigratePage;
	protected TKRunTuningJob tkRunTuningJob;	
	
	protected TKStartTuning tkStartTuning;
	protected TKTuningSourcePage tkTuningSourcePage;
	protected TKTuningScopePage tkTuningScopePage;
	protected TKTuningOpsPage tkTuningOpsPage;

	

	
	//For SSL 
	protected TKSSL tkSSL;
	
	
	
	/*
	 * First level mune
	 */
	//For Home
	protected String home_page = "home";
	
	//For Admin
	protected String admin_instance_page = "administer->instance";
	protected String admin_database_page = "administer->admin_database";
	protected String admin_tables_page = "administer->tables";
	protected String admin_views_page = "administer->views";
	protected String admin_indexes_page = "administer->indexes";
	protected String admin_constraints_page = "administer->constraints";
	protected String admin_mqts_page = "administer->mqts";
	protected String admin_remotetables_page = "administer->remotetables";
	protected String admin_aliases_page = "administer->aliases";
	protected String admin_schemas_page = "administer->schemas";
	protected String admin_storageobjects_page = "administer->storageobjects";
	protected String admin_storageobjects_storagegroups_page = "administer->storageobjects->storagegroups";
	protected String admin_storageobjects_tablespaces_page = "administer->storageobjects->tablespaces";
	protected String admin_storageobjects_bufferpools_page = "administer->storageobjects->bufferpools";
	protected String admin_fgac_page = "administer->fgac";
	protected String admin_fgac_permissions_page = "administer->fgac->permissions";
	protected String admin_fgac_masks_page = "administer->fgac->masks";
	protected String admin_applicationobjects_page = "administer->applicationobjects";
	protected String admin_applicationobjects_triggers_page = "administer->applicationobjects->triggers";
	protected String admin_applicationobjects_sequences_page = "administer->applicationobjects->sequences";
	protected String admin_applicationobjects_packages_page = "administer->applicationobjects->packages";
	protected String admin_applicationobjects_storedprocedures_page = "administer->applicationobjects->storedprocedures";
	protected String admin_applicationobjects_userdefinedtypes_page = "administer->applicationobjects->user_definedtypes";
	protected String admin_applicationobjects_userdefinedfunctions_page = "administer->applicationobjects->user_definedfunctions";
	protected String admin_applicationobjects_plsqlpackages_page = "administer->applicationobjects->plsqlpackages";
	protected String admin_workloads_page = "administer->workloads";
	protected String admin_privileges_page = "administer->privileges";
	protected String admin_configurations_page = "administer->configurations";
	protected String admin_configurations_trackchanges_page = "administer->configurations->trackchanges";
	protected String admin_configurations_compareconfigurations_page = "administer->configurations->compareconfigurations";
	protected String admin_configurations_managealiases_page = "administer->configurations->managealiases";
	protected String admin_diagnosticlog_page = "administer->diagnosticlog";
	
	//For RunSQL
	protected String runsql_page = "runsql";
	
	//For Monitor
	protected String monitor_database_page = "monitor->monitor_database";
	protected String monitor_alerts_page = "monitor->alerts";
	protected String monitor_userdefinedalerts_page = "monitor->user_definedalerts";
	protected String monitor_stmehistorypre_page = "monitor->statementhistoryprerequisites";
	
	//For Jobs
	protected String jobs_page = "jobs";
	
	//For Optimize
	protected String optimize_starttuning_page= "optimize->starttuning";
	protected String optimize_tuningjobs_page = "optimize->tuningjobs";
	protected String optimize_controlclients_page = "optimize->controlclients";
	protected String optimize_utility_page= "optimize->tuningutility";
	protected String optimize_tuningConfig_page= "optimize->tuningutility->workloadconfiguration";
	protected String optimize_tuningMigrate_page= "optimize->tuningutility->migration";

	
	//For Settings
	protected String settings_productsetup_page = "settings->productsetup";
	protected String settings_monitoringprofiles_page = "settings->monitoringprofiles";
	protected String settings_manageconnections_page = "settings->manageconnection";
	protected String settings_usersandprivileages_page = "settings->users_and_privileges";
	protected String settings_blackoutevents_page = "settings->blackout_events";
	protected String settings_loggingandtracing_page = "settings->loggingandtracing";
	protected String settings_myprofile_page = "settings->myprofile";
	
	//For help
	protected String help_openhelp_page = "help->openhelp";
	protected String help_close_page = "help->closehelp";
	protected String help_welcome_page = "help->welcome";
	protected String help_about_page = "help->about";
	
	
		
	
	public TestCaseBase(){		
		//This is init for Web Driver 
		if(TestCaseBase.driverCore == null){
			TestCaseBase.driverCore = new WebDriverCore(initWebDriver());	
		}			
		//This is init for Task layer pages
		//For Common
		tkMenu = new TKMenu(driverCore);
		tkLoginPage = new TKLogin(driverCore);
		tkWelcomePage = new TKWelcome(driverCore);
		tkHeader = new TKHeader(driverCore);
		tkDialog = new TKDialog(driverCore);
		tkProductSetupPage = new TKProductSetup(driverCore);
		tkManageConnectionPage = new TKManageConnection(driverCore);
		tkUsersandPrivilegesPage = new TKUsersandPrivileges(driverCore);
		
		//For Admin
		tkAdmin = new TKAdmin(driverCore);
		tkAdminInstancePage = new TKAdminInstance(driverCore);		
		tkAdminDatabasePage = new TKAdminDatabase(driverCore);
		tkAdminIndexesPage = new TKAdminIndexes(driverCore);
		tkAdminTablesPage = new TKAdminTables(driverCore);
		tkAdminPrivilegesPage = new TKAdminPrivileges(driverCore);
		tkAdminWLMPage = new TKAdminWLM(driverCore);
		tkAdminTriggersPage = new TKAdminTriggers(driverCore);
		
		
		//For Monitor
		tkMonitorDatabasePage = new TKMonitorDatabase(driverCore);
		tkHomePage = new TKHome(driverCore);
		tkAlertsPage = new TKAlerts(driverCore);
		tkMonitoringProfiles = new TKMonitoringProfiles(driverCore);
		
		//For Tunning
		tkRunSQLPage = new TKRunSQL(driverCore);
		tkJobsPage = new TKJobs(driverCore);
		tkEOJobPage = new TKEOJob(driverCore);
		tkConfigPage = new TKTuningConfig(driverCore);
		tkTuningJobs = new TKTuning(driverCore);
		tkMigratePage = new TKTuningMigrate(driverCore);
		tkAbout = new TKAbout(driverCore);
		tkRunTuningJob = new TKRunTuningJob(driverCore);
		tkStartTuning = new TKStartTuning(driverCore);
		tkTuningSourcePage = new TKTuningSourcePage(driverCore);
		tkTuningScopePage = new TKTuningScopePage(driverCore);
		tkTuningOpsPage = new TKTuningOpsPage(driverCore);
		tkFilterPage = new TKTuningManageFilters(driverCore);
		tkRebindPage = new TKTuningRebind(driverCore);

		//For SSL
		tkSSL = new TKSSL(driverCore);
		
	}
	

	@BeforeSuite
	public void start() throws Throwable {	
		//Pre-condition 1: Make user repo db is available 	
		Assert.assertTrue(new DBUtils(repoIP, repoPort, repoDatabase, repoUsername, repoPassword).testConnection(), 
				"DSM REPODB is not available to use!!!");		
		
		//Pre-condition 2: 
		openURL();
		currentUser = adminUser;
		currentPassword = adminPassword;
		tkLoginPage.login(currentUser, currentPassword, this.tkDialog, this.dbProfile);
		
		this.tkHeader.verifyLogOutBtn();
		//this.tkMenu.selectMenuwithoutLoadPage("root");
				
	}

	@AfterSuite
	public void quit() {
		this.tkHeader.logout(tkDialog);
		closeBrowser();
	}
	
	@BeforeClass
	@Parameters({"dbProfile", "dbUserName", "dbPassword"})
	public void beforeTest(@Optional("")String dbProfile, @Optional("")String dbUserName, @Optional("")String dbPassword){		
		
		boolean flag = false;
		if(flag=dbProfile.isEmpty()){
			this.closeWebDriver();
			Assert.assertFalse(flag, "dbProfile name can not be empty!");			
		}
		if(flag=dbUserName.isEmpty()){
			this.closeWebDriver();
			Assert.assertFalse(flag, "dbUserName name can not be empty!");			
		}
		if(flag=dbPassword.isEmpty()){
			this.closeWebDriver();
			Assert.assertFalse(flag, "dbPassword name can not be empty!");			
		}	
		
		this.dbProfile = dbProfile;
		this.dbUserName = dbUserName;
		this.dbPassword = dbPassword;
	}

	public void openURL(String URL) {		
		driverCore.open(URL);	
	}
	
	public void openURL() throws AWTException {

		String URL = dsmserver + ":" + dsmport + login;

		if (URL.startsWith("https")) {

			try {
				driverCore.open(URL);
			} catch (Exception e) {
				// statements to handle any exceptions
				// logMyErrors(e); // pass exception object to error handler
			} finally {
				
				driverCore.pause();
				this.tkSSL.addSecurityException();
				driverCore.waitForPageLoad();
			}
		} else {
			driverCore.open(URL);
			driverCore.waitForPageLoad();
		}

	}
	
	
	public void closeBrowser() {
		closeWebDriver();
	}

	

	public WebDriver initWebDriver(String driverType) {

		WebDriver driver = null;
		WebDriverFactory driverFactory = new WebDriverFactory();

		switch (driverType.trim()) {
		case "firefox":
			driver = driverFactory.getFirefoxDriver(headless);
			break;
		case "chrome":
			driver = driverFactory.getChromeDriver();
			break;
		case "ie":
			driver = driverFactory.getIEDriver();
			break;
		default:			
			driver = null;
			TestLogger.logWarn("We do not support this browser for selenium automation, please contact owner for details.");
		}
		
		
		if(driver != null){
			//driver.manage().timeouts().implicitlyWait(waitTimeout, TimeUnit.MILLISECONDS);
			//TestLogger.logDebug("set waitTimeout : " + waitTimeout);
			
			driver.manage().timeouts().pageLoadTimeout(pageLoadTimeout, TimeUnit.SECONDS);
			TestLogger.logDebug("set pageLoadTimeout : " + pageLoadTimeout);
			
			driver.manage().timeouts().setScriptTimeout(scriptTimeout, TimeUnit.SECONDS);
			TestLogger.logDebug("set scriptTimeout : " + scriptTimeout);
		}
		
		return driver;
	}

	public WebDriver initWebDriver() {
		String driverType = Properties.getConfigValue("browser");
		return initWebDriver(driverType);
	}

	public void closeWebDriver() {	
		try{
			driverCore.getDriver().quit();			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	public java.util.Properties loadJSONPostData(String filename) throws FileNotFoundException, IOException {
		
		String filePath = Configuration.getAdminDataPath() + filename;
		java.util.Properties p = new java.util.Properties();	
		p.load(new FileInputStream(filePath));		
		return p;
	}
}
