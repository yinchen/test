package com.ibm.datatools.dsm.tests.ui.testcase.common;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;

public class TestHelp extends TestCaseBase {

	@BeforeClass
	public void beforetest(){		
		this.tkMenu.selectMenuwithoutLoadPage("root->help->root");		
		
	}	

	/**
	 * Test Help Welcome page
	 * @throws Throwable 
	 */
	@Test
	public void testHelpWelcome() {
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Help Welcome page... = = = = = = = =");			
		tkMenu.selectMenu("root->welcome", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);		
		tkWelcomePage.verifyWelcomePageTitle();			
		TestLogger.logInfo("= = = = = = = = End the test: Test Help Welcome page... = = = = = = = =");	
		
	}		
	
	
	public void testAbout() {
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test About page... = = = = = = = =");			
		tkMenu.selectMenu("root->about", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		
		//switch to the new window for about page 
		tkAbout.switchToLastWindow();
		tkAbout.clickPluginsBtn();
		
		//close new window 
		tkAbout.closeCurrentWindow();
		//switch back to previous window
		tkAbout.switchToLastWindow();
		
		TestLogger.logInfo("= = = = = = = = End the test: Test About page... = = = = = = = =");		
	}
	
	//@AfterClass
	public void afetertest(){
		this.tkMenu.selectMenuwithoutLoadPage("root->help->root");
	}
}













