package com.ibm.datatools.dsm.tests.ui.testcase.common;

import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;


public class TestHome extends TestCaseBase {

	@Test
	public void testHomePageInfo(){	
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Home page... = = = = = = = =");
		tkMenu.selectMenu("root->home", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);	
		
		//sometimes selenium will not enter home page after clicking it, so we click it again 
		if(!tkHomePage.isNameButtonDisplayed())
			tkMenu.selectMenu("root->home", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		//tkHomePage.switchToGridView();		
		tkHomePage.verifyHomePageInfo();
		TestLogger.logInfo("= = = = = = = = End the test: Test Home page... = = = = = = = =");
		
	}

	
}













