package com.ibm.datatools.dsm.tests.ui.testcase.common;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TestSettings extends TestCaseBase {
	
	@BeforeClass
	public void beforetest(){		
		this.tkMenu.selectMenuwithoutLoadPage("root->settings->root");		
		
	}
	/**
	    * 
	    * 1. Go to Settings/Users and Privileges page
		* 2. Add a new user 
		* 3. Log in the new user 
	*/
	
	@Test
	public void testNewUserExecution(){	
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Add New User... = = = = = = = =");	
		//add user or update the user
		tkMenu.selectMenu("root->users_and_privileges");	
		this.tkUsersandPrivilegesPage.addUser(loginUser, loginPassword, "Administrator", tkDialog);
		//log out
		WebDriverCore.pause();
		this.tkHeader.logout(tkDialog);	
		WebDriverCore.pause();
		
		currentUser = loginUser;
		currentPassword = loginPassword;
		tkLoginPage.login(currentUser, currentPassword, this.tkDialog, this.dbProfile);
		this.tkHeader.verifyLogOutBtn();
		TestLogger.logInfo("= = = = = = = = End the test: Test Add New User... = = = = = = = =");	
	}	
	
	/**
	    * Test verify Settings Product Setup page
	    * 1. It has "Repository Database Configuration" tab 
		* 2. It has "System Ports Configuration" tab 
		* 3. It has "Email Server" option 
		* 4. It has "SNMP Server" option
	    */
	@Test
	public void testSettingsProductSetupPageInfo(){
	
		TestLogger.logInfo("= = = = = = = = Start the test: Test Settings Product Setup page... = = = = = = = =");
		tkMenu.selectMenu("root->productsetup", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);		
		tkProductSetupPage.verifyProductSetupPageInfo();
		TestLogger.logInfo("= = = = = = = = End the test: Test Settings Product Setup page... = = = = = = = =");	
		
	}
	
	/**
	    * Test Verify Settings Manage Connections page
	    * 1. It has "Database Connection" tab 
		* 2. It has "Instance Connection" tab 
		* 3. It has "Add" button with enabled 
		* 4. It has "Edit" button with disabled 
		* 5. It has "Delete" button with disabled 
		* 6. It has "Validate Configuration" button with disabled 
		* 7. It has "Query Tuning" button with disabled 
		* 8. It has "Blackout" button with disabled 
		* 9. It has "Activate License" button with disabled 
		* 10. It has "Validate Credentials" button with disabled
	    */
	@Test
	public void testManageConnectionPageInfo(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Manage Connection page... = = = = = = = =");
		tkMenu.selectMenu("root->manageconnection", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);		
		tkManageConnectionPage.verifyManageConnectionsPageInfo();
		TestLogger.logInfo("= = = = = = = = End the test: Test Manage Connection page... = = = = = = = =");
		
	}
	
	/**
	    * Test Verify Settings Monitoring Profiles page
		* 1. It has "New" button with enabled 
		* 2. It has "Edit" button with disabled 
		* 3. It has "Delete" button with disabled 
		* 4. It has "Clone" button with disabled 
		* 5. It has "Assign" button with disabled 
		* 6. It has "Activate" button with disabled 
	    */
	@Test
	public void testSettingMonitoringProfilesInfoForCommon(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Monitoring Profiles page... = = = = = = = =");
		tkMenu.selectMenu("root->monitoringprofiles", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);		
		tkMonitoringProfiles.verifyMonitoringProfilesPageInfoForCommon();
		TestLogger.logInfo("= = = = = = = = End the test: Test Monitoring Profiles page... = = = = = = = =");
		
	}		
	
	@AfterClass
	public void afetertest(){
		this.tkMenu.selectMenuwithoutLoadPage("root->settings->root");
	}
}













