package com.ibm.datatools.dsm.tests.ui.testcase.home;


import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TestHome extends TestCaseBase {
	@BeforeClass
	public void beforetest(){
		tkHeader.pickDB(this.dbProfile);
		
		if(!currentUser.equals(adminUser)){
			this.tkMenu.selectMenuwithoutLoadPage("root->home");
			this.tkDialog.handleDialogUser(this.dbProfile, this.dbUserName,this.dbPassword);
			WebDriverCore.pause(4);
		}
			
	}
	
	

	/**
	    * Test Verify Home page
	    * 1. It has "Name" tab
		* 2. It has "Alerts" tab 
		* 3. It has "System CPU" tab 
		* 4. It has "Logical reads" tab 
		* 5. It has "Memory" tab 
		* 6. It has "Transaction rate" tab 
		* 7. It has "Average activity timeO" tab
	    */
	@Test
	public void testHomePageInfo(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test Home page... = = = = = = = .");
		
		//step 1
		tkMenu.selectMenu("root->home", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);	
		 
		//step 2
		//tkHomePage.switchToGridView();
		tkHomePage.verifyHomePageInfo();
		TestLogger.logInfo("= = = = = = = = End the test: Test Home page... = = = = = = = =");	
	}
		
	
	/**
	    * Test Verify Home page buttons
	    * 1. It has "Name" button
		* 2. It has "Refresh Every 60s" button
		* 3. It has "Refresh data" button
		* 4. It has "Search Icon" button
		* 5. It has "Filter Icon" button
		* 6. It has "Customize the visible metrics" button
		* 7. It has "Useful links" button
		* 8. It has "Switchview" button
		* 9. It has "Switchcardview" button
	    */
	
	@Test
	public void testHomeBtnDisp(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test Home page buttons...= = = = = = = = .");
		//step 1
		tkMenu.selectMenu("root->home", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		
		//sometimes selenium will not enter home page after clicking it, so we click it again 
		if(!tkHomePage.isNameButtonDisplayed())
			tkMenu.selectMenu("root->home", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		
		//step 2
		tkHomePage.verifyHomeBtnDisp();
		TestLogger.logInfo("= = = = = = = = End the test: Test Home page buttons...= = = = = = = = .");
	}


/**
 * Test verify SeleniumAuto DB displayed in grid view
    *  It has "SeleniumAuto DB"
 */

	@Test
	public void testSeleniumAutoDisp(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test SeleniumAuto DB display in grid view...= = = = = = = = .");
		//step 1
		tkMenu.selectMenu("root->home", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		
		//step 2
		tkHomePage.verifySeleniumAutoDisp();
		TestLogger.logInfo("= = = = = = = = End the test:  Test SeleniumAuto DB display in grid view...= = = = = = = = .");
	}

/**
 * 1. click SeleniumAuto Link
 * 2. entry into monitor page,verify the overview page,
 *  
 */

	@Test
	public void testSeleniumAutoLink(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test SeleniumAuto Link available...= = = = = = = = .");
		//step 1
		tkHomePage.clickDbLink();
		//step 2
//		tkHomePage.verifySeleniumAutoDisp();
		tkMonitorDatabasePage.waitForPageLoadCompleted();
		tkMonitorDatabasePage.verifyDatabastPageInfo();
		TestLogger.logInfo("= = = = = = = = End the test: Test SeleniumAuto Link available...= = = = = = = = .");
	}
	
	
	@Test
	public void testTagFilterDivDisp(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test Tag filter...= = = = = = = = .");
		//step 1
		tkMenu.selectMenu("home", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		//step 2
		tkHomePage.clickFilterByTagsButton();
		tkHomePage.verifyTagFilterDivDisp();
		TestLogger.logInfo("= = = = = = = = End the test: Test Tag filter...= = = = = = = = .");
	}
	
	@Test
	public void testCardViewBtnDisp(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test Card view buttons...= = = = = = = = .");
		//step 1
		tkMenu.selectMenu("home", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		//step 2
		tkHomePage.switchToCardView();
		tkHomePage.verifyCardViewDivDisp();
		tkHomePage.changeCardViewSortType();
		TestLogger.logInfo("= = = = = = = = End the test: Test Card view buttons...= = = = = = = = .");
	}
	
	@Test
	public void testSummaryViewTabDisp(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test Summary view tabs...= = = = = = = = .");
		//step 1
		tkMenu.selectMenu("home", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		//step 2
		tkHomePage.verifySummaryViewTabDisp();
		TestLogger.logInfo("= = = = = = = = End the test: Test Summary view tabs...= = = = = = = = .");
	}
	
	@Test
	public void testSummDisconnectedDisp(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test Disconnected summary view buttons...= = = = = = = = .");
		//step 1
		tkMenu.selectMenu("home", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		//step 2
		tkHomePage.switchToDisconnectedView();
		tkHomePage.verifySummDisconnectedDisp();
		TestLogger.logInfo("= = = = = = = = End the test: Test Disconnected summary view buttons...= = = = = = = = .");
	}
	
	@Test
	public void testSummWithAlertsDisp(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test With alerts summary view buttons...= = = = = = = = .");
		//step 1
		tkMenu.selectMenu("home", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		//step 2
		tkHomePage.switchToWithAlertsView();
		tkHomePage.verifySummWithAlertsDisp();
		TestLogger.logInfo("= = = = = = = = End the test: Test With alerts summary view buttons...= = = = = = = = .");
	}
	
	@Test
	public void testSummNotMonitoredDisp(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test Not monitored summary view buttons...= = = = = = = = .");
		//step 1
		tkMenu.selectMenu("home", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		//step 2
		tkHomePage.switchToNotmonitoredView();
		tkHomePage.verifySummNotMonitoredDisp();
		TestLogger.logInfo("= = = = = = = = End the test: Test Not monitored summary view buttons...= = = = = = = = .");
	}
	
	@Test
	public void testSearchOpenDivDisp(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test open search box...= = = = = = = = .");
		//step 1
		tkMenu.selectMenu("home", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		//step 2
		tkHomePage.clickTotalConnectionsBtn();
		tkHomePage.openSearchIcon();
		tkHomePage.verifySearchOpenDivDisp();
		TestLogger.logInfo("= = = = = = = = End the test: Test open search box...= = = = = = = = .");
	}	
	
	@Test
	public void testFilterDropDownMenuDisp(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test open filter tab...= = = = = = = = .");
		//step 1
		tkMenu.selectMenu("home", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		//step 2
		tkHomePage.clickTotalConnectionsBtn();
		tkHomePage.clickFilterBtn();
		tkHomePage.verifyFilterDropDownMenuDisp();
		TestLogger.logInfo("= = = = = = = = End the test: Test open filter tab...= = = = = = = = .");
	}
	
	@Test
	public void testZ1CustomizeMetricsMenuDisp(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test open customize metrics...= = = = = = = = .");
		//step 1
		tkMenu.selectMenu("home", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		//step 2
		tkHomePage.clickTotalConnectionsBtn();
		tkHomePage.clickcustomizeMetricsBtn();
		tkHomePage.verifyisCheckAllMetrcisSpanDisp();
		tkHomePage.clickCheckAllMetrcisSpan();
		tkHomePage.clickcheckAllMetricsOKBtn();
		TestLogger.logInfo("= = = = = = = = End the test: Test open customize metrics...= = = = = = = = .");
	}	
}