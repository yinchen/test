package com.ibm.datatools.dsm.tests.ui.testcase.monitor;


import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;



public class TestMonitor extends TestCaseBase {		

	@BeforeClass
	public void beforetest(){
		this.tkMenu.selectMenuwithoutLoadPage("root->monitor->root");
		tkHeader.pickDB(this.dbProfile);		
		
		if(!currentUser.equals(adminUser)){
			this.tkMenu.selectMenuwithoutLoadPage("root->monitor_database");
			this.tkDialog.handleDialogUser(this.dbProfile, this.dbUserName,this.dbPassword);
			WebDriverCore.pause(4);
		}
			
	}	

	/**
	    * Test Verify Monitor Database page
	    * 1. It has "Overview" tab and in this tab it has "Data Server" option 
		* 2. It has "Statements" tab 
		* 3. It has "Locking" tab 
		* 4. It has "Applications" tab 
		* 5. It has "Workload" tab 
		* 6. It has "Memory" tab 
		* 7. It has "I/O" tab
		* 8. It has "Storage" tab 
		* 9. It has "Notifications" tab 
	    */
	@Test
	public void testMonitorDatabasePageInfo(){
		
        TestLogger.logInfo("= = = = = = = = Start the test: Test Monitor Database page... = = = = = = = =");	
		tkMenu.selectMenu("root->monitor_database", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);		
		tkMonitorDatabasePage.verifyDatabastPageInfo();	
		TestLogger.logInfo("= = = = = = = = End the test: Test Monitor Database page... = = = = = = = =");	
	}
	
	
	/**
	    * Test Verify Monitor Alerts page
	    * 1. It has "Show All Open" menu with enabled 
		* 2. It has "View details.." menu with disabled 
		* 3. It has "delete" menu with disabled 
		* 4. It has "Send" menu with disabled
	    */
	@Test
	public void testMonitorAlertsPageInfo(){
		
		 TestLogger.logInfo("= = = = = = = = Start the test: Test Monitor Alerts page... = = = = = = = =");	
		 tkMenu.selectMenu("root->alerts", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);		
		 tkAlertsPage.verifyAlertsPageInfo();
		 TestLogger.logInfo("= = = = = = = = End the test: Test Monitor Alerts page... = = = = = = = =");	
		
	}
	
	/**
	 * 1. click Monitor->Database
	 * 2. select a connection "COLLEGE"(verify the overview page, 
	 * such as the we could find the overview statements locking applications throughput,IO,notification tab)
	 * 
	 */
	
	@Test
	public void testMontiorDatabase(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Monitor Database page... = = = = = = = =");	
		tkMenu.selectMenu("root->monitor_database", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		
		//work around, sometimes the monitor page element will load for a while, but the page is empty
		if(!tkMonitorDatabasePage.isOverviewTabDisplayed())
			tkMenu.selectMenu("root->monitor_database", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
			
		tkMonitorDatabasePage.verifyDatabastPageInfo();	
		TestLogger.logInfo("= = = = = = = = End the test: Test Monitor Database page... = = = = = = = =");	
	}
	
	@AfterClass
	public void afetertest(){
		this.tkMenu.selectMenuwithoutLoadPage("root->monitor->root");
	}
}














