package com.ibm.datatools.dsm.tests.ui.testcase.monitor;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;

public class TestMonitorViewHistoryData extends TestCaseBase{
	
	@BeforeClass
	public void beforetest(){
		tkHeader.pickDB(this.dbProfile);
			
	}
	
	
//	step 1. click Monitor->Database 
//	step 2. select a connection "COLLEGE"(verify the overview page,such as the we could find the overview statements locking applications throughput,IO,notification tab)
//	step 3. Click History mode button
//	step 4. Click Statements->Package Cache
//	step 5. Click Last 30 Days button ( verify the grid and buttons)
//	step 6. Drag the time slider (verify the data change)
//	step 7. select one row -> click view Details button (verify the panels)

	@Test
	public void testMonitorRealTimeDatabasePageInfo(){
		
		TestLogger.logInfo("View realtime databasein Monitor Database page...");		
		
		tkMenu.selectMenu("root->monitor->monitor_database", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);	
		tkMonitorDatabasePage.verifyDatabastPageInfo();	
		
	}
   
	@Test
    public void testMonitorHistoryDatabasePageInfo(){
		
		TestLogger.logInfo("View history database in Monitor Database page...");
		
		
		tkMonitorDatabasePage.clickHistoryBtn();
		tkMonitorDatabasePage.verifyDatabastPageInfo();		
		
		

	}
	
	@Test
    public void testMonitorHistoryStatements(){
		
		TestLogger.logInfo("View history statements in Monitor Database page...");
		
		tkMenu.selectMenu("statements_tab->packageCache_link");
		tkMonitorDatabasePage.verifyHistoryStatementLabBtn();
		
	}
    
	@Test
    public void testMonitorHistoryChangeCustomizeDuration(){
		
		TestLogger.logInfo("View history change customize duration in Monitor Database page...");
		
		tkMenu.selectMenu("last3hours_tab->last30days_link");	
	
	}
	
	@Test   
    public void testMonitorHistoryChangeTimeSlider(){
		
		TestLogger.logInfo("View history Change Time Slider in Monitor Database page...");
		
		int movement = 10;
		tkMonitorDatabasePage.dragTimeSlider(movement);
		
		//step 7
		/*int romNum = 1;
		tkMonitorDatabasePage.selectOneRow(romNum);
		tkMonitorDatabasePage.viewSelectRowDetails();
		*/
	}
	
	
}




