package com.ibm.datatools.dsm.tests.ui.testcase.monitor;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;

public class TestMonitoringProfiles extends TestCaseBase{
	
	@BeforeClass
	public void beforetest(){
		//this.tkMenu.selectMenuwithoutLoadPage("root->settings->root");
		tkHeader.pickDB(this.dbProfile);		
	}	
	
//	step 1. click Settings->Monitoring Profiles ( verify the grid and buttons)
//	step 2. select MongoDB type, then select Default MongoDB profile
//	step 3. Click Edit button (verify the panels)

	@Test
	public void testMonitorProfilesPageInfo(){
		
		TestLogger.logInfo("View Monitor Profiles page...");		
		tkMenu.selectMenu("root->monitoringprofiles", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		tkMonitoringProfiles.verifyMonitoringProfilesPageInfo();
	}
   
	@Test
    public void testEditMonitorProfiles(){
		
		TestLogger.logInfo("Edit Monitor Profiles page...");		
		tkMenu.selectMenu("root->monitoringprofiles", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		tkMonitoringProfiles.selectMongoDBType();
		tkMonitoringProfiles.selectDefaultMongoDBProfile();
		tkMonitoringProfiles.clickEditBtn();
		tkMonitoringProfiles.verifyMonitoringProfilesEditPageInfo();		
	}
	
	@AfterClass
	public void afetertest(){
		this.tkMenu.selectMenuwithoutLoadPage("root->settings->root");
	}
}




