package com.ibm.datatools.dsm.tests.ui.testcase.runsql;


import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TestRunSQL extends TestCaseBase {

	
	@BeforeClass
	public void beforetest(){
		tkHeader.pickDB(this.dbProfile);		
		if(!currentUser.equals(adminUser)){
			this.tkMenu.selectMenuwithoutLoadPage("root->runsql");
			this.tkDialog.handleDialogUser(this.dbProfile, this.dbUserName,this.dbPassword);
			WebDriverCore.pause(4);
		}	
	}	
	

//	 /**
//	* Test Run SQL page and verify
//	* 1. It has "Run" menu with enabled 
//	* 2. It has "Script" menu with enabled 
//	* 3. It has "Edit" menu with enabled 
//	* 4. It has "Favorites" menu with enabled 
//	* 5. It has "Learn more" with enabled 
//	*/
	
	@Test
	public void testRunSQLPageInfo(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Run SQL Page... = = = = = = = =");
		tkMenu.selectMenu("root->runsql", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);	
		tkRunSQLPage.waitForPageLoadCompleted();
		tkRunSQLPage.verifyRunSQLPageInfo();
		TestLogger.logInfo("= = = = = = = = End the test: Test Run SQL Page... = = = = = = = =");
		
	}
	
}



