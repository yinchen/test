package com.ibm.datatools.dsm.tests.ui.testcase.screenshot;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;
import com.ibm.datatools.dsm.tests.ui.framework.base.ScreenShotUtils;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TestCaptureScreenShots extends TestCaseBase{
	
	@BeforeClass
	public void beforetest(){
		
		tkHeader.pickDB(this.dbProfile);
	}
	
	
	List<String> firstLevelPageList = new ArrayList<String>();
	
	@Test
	public void testCaptureFirstLevelPage(){
		firstLevelPageList.add (home_page);                        
		firstLevelPageList.add (admin_instance_page);              
		firstLevelPageList.add (admin_database_page);              
		firstLevelPageList.add (admin_tables_page);                
		firstLevelPageList.add (admin_views_page);                 
		firstLevelPageList.add (admin_indexes_page);               
		firstLevelPageList.add (admin_constraints_page);           
		firstLevelPageList.add (admin_mqts_page);                  
		firstLevelPageList.add (admin_remotetables_page);          
		firstLevelPageList.add (admin_aliases_page);               
		firstLevelPageList.add (admin_schemas_page);               
		firstLevelPageList.add (admin_storageobjects_page); 
		firstLevelPageList.add(admin_storageobjects_storagegroups_page);
		firstLevelPageList.add(admin_storageobjects_tablespaces_page);
		firstLevelPageList.add(admin_storageobjects_bufferpools_page);
		firstLevelPageList.add (admin_fgac_page);
		firstLevelPageList.add(admin_fgac_permissions_page);
		firstLevelPageList.add(admin_fgac_masks_page);
		firstLevelPageList.add (admin_applicationobjects_page);   
		firstLevelPageList.add(admin_applicationobjects_triggers_page);
		firstLevelPageList.add(admin_applicationobjects_sequences_page);
		firstLevelPageList.add(admin_applicationobjects_packages_page);
		firstLevelPageList.add(admin_applicationobjects_storedprocedures_page);
		firstLevelPageList.add(admin_applicationobjects_userdefinedtypes_page);
		firstLevelPageList.add(admin_applicationobjects_userdefinedfunctions_page);
		firstLevelPageList.add(admin_applicationobjects_plsqlpackages_page);
		firstLevelPageList.add(admin_workloads_page);
		firstLevelPageList.add (admin_privileges_page);            
		firstLevelPageList.add (admin_configurations_page);  
		firstLevelPageList.add(admin_configurations_trackchanges_page);
		firstLevelPageList.add(admin_configurations_compareconfigurations_page);
		firstLevelPageList.add(admin_configurations_managealiases_page);
		firstLevelPageList.add (admin_diagnosticlog_page);         
		firstLevelPageList.add (runsql_page);                      
		firstLevelPageList.add (monitor_database_page);            
		firstLevelPageList.add (monitor_alerts_page);              
		firstLevelPageList.add (monitor_userdefinedalerts_page);   
		firstLevelPageList.add (monitor_stmehistorypre_page);      
		firstLevelPageList.add (jobs_page);                        
		firstLevelPageList.add (optimize_starttuning_page);        
		firstLevelPageList.add (optimize_tuningjobs_page);         
		firstLevelPageList.add (optimize_controlclients_page);     
		firstLevelPageList.add (settings_productsetup_page);       
		firstLevelPageList.add (settings_monitoringprofiles_page); 
		firstLevelPageList.add (settings_manageconnections_page);  
		firstLevelPageList.add (settings_usersandprivileages_page);
		firstLevelPageList.add (settings_blackoutevents_page);     
		firstLevelPageList.add (settings_loggingandtracing_page);  
		firstLevelPageList.add (settings_myprofile_page);          
		firstLevelPageList.add (help_openhelp_page); 
		firstLevelPageList.add(help_close_page);
		firstLevelPageList.add (help_welcome_page);                
		firstLevelPageList.add (help_about_page);                  
		
		for(String pageName : firstLevelPageList){
			
			TestLogger.logInfo("Capture "+pageName+" page...");
			tkMenu.selectMenu(pageName, this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
			String screenShotName = ScreenShotUtils.createScreenshotName(pageName);
			
			WebDriverCore.screenShot(screenShotName);
		}
	}
	
}	







































