package com.ibm.datatools.dsm.tests.ui.testcase.tuning;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TestTuning_LUW extends TestCaseBase {

	
	@BeforeClass
	public void beforetest(){			
		
		this.tkMenu.selectMenuwithoutLoadPage("root->optimize->root");
		tkHeader.pickDB(this.dbProfile);		
		
		if(!currentUser.equals(adminUser)){
			this.tkMenu.selectMenuwithoutLoadPage("root->starttuning");
			this.tkDialog.handleDialogUser(this.dbProfile, this.dbUserName,this.dbPassword);
			WebDriverCore.pause(4);
			this.tkMenu.selectMenuwithoutLoadPage("root->runsql");
			this.tkDialog.handleDialogUser(this.dbProfile, this.dbUserName,this.dbPassword);
			WebDriverCore.pause(4);
		}
		if(currentUser.equals(adminUser)){
			this.tkMenu.selectMenu("root->starttuning");
			this.tkDialog.handleDialogUser(this.dbProfile, this.dbUserName,this.dbPassword);
			WebDriverCore.pause(4);
		}
		//tkHeader.pickDB("LOCDB21");	
	}
	
	@Test
	public void runSingleTuneJob(){
		
		TestLogger.logInfo("= = = = = = = = Start the test: Create single tuning job... = = = = = = = =");
		tkMenu.selectMenu("root->starttuning",this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);		
		tkRunTuningJob.runSingleTuneJob();	
		TestLogger.logInfo("= = = = = = = = End the test:  Create single tuning job... = = = = = = = =");
	}
	
	@Test
	public void verifyInputSqlSourcePageInfo(){
		TestLogger.logInfo("= = = = = = = = Start the test: Verify input sql source page... = = = = = = = =");
		tkMenu.selectMenu("root->starttuning",this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);		
		tkTuningSourcePage.verifyInputSqlSourcePageInfo("select name from sysibm.systables t where t.name like 'SYS%' and t.type = 'T';");
		tkStartTuning.clickNexBtn();
		TestLogger.logInfo("= = = = = = = = End the test:  Verify input sql source page... = = = = = = = =");
	}

	@Test
	public void verifyScopePageInfo(){
		TestLogger.logInfo("= = = = = = = = Start the test: Verify scope page... = = = = = = = =");
		tkTuningScopePage.verifyScopePageInfo();
		tkStartTuning.clickNexBtn();
		TestLogger.logInfo("= = = = = = = = End the test:  Verify scope page... = = = = = = = =");
	}
	
	@Test
	public void verifyOpsForSinglePageInfo(){
		TestLogger.logInfo("= = = = = = = = Start the test: Verify options page... = = = = = = = =");
		tkTuningOpsPage.verifyOpsForSinglePageInfo("singleTune");
		TestLogger.logInfo("= = = = = = = = End the test:  Verify options page... = = = = = = = =");
	}

	@Test
	public void launchSingleTuneJob(){
		TestLogger.logInfo("= = = = = = = = Start the test: Verify options page... = = = = = = = =");
		tkStartTuning.clickLaunchBtn();
		tkStartTuning.clickCloseBtn();
		driverCore.waitForPageLoad(20);
		TestLogger.logInfo("= = = = = = = = End the test:  Verify options page... = = = = = = = =");
	}
	
	
	@Test
	public void testConfigurationPageInfo(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test Configuration page... = = = = = = = =");
		tkMenu.selectMenu("root->tuningutility->workloadconfiguration",this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);		
		tkConfigPage.verifyTuningConfigurationPageInfo();	
		TestLogger.logInfo("= = = = = = = = End the test: Test Configuration page... = = = = = = = =");
	}	
	
	@Test
	public void testSingleJobAPAInfo(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test Single APA Recommendations... = = = = = = = =");
		tkMenu.selectMenu("root->tuningjobs", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		String jobName = "singleTune";
		tkTuningJobs.verifySingleJobAPAResult(jobName);
		TestLogger.logInfo("= = = = = = = = End the test: Test Single APA Recommendations ... = = = = = = = =");
	}	
	
	

//   move case to jobs component
//	/**
//	    * Test Verify Jobs page
//	    * 1. It has "All job types" option 
//		* 2. It has "View options: History" option 
//		* 3. It has "Run" button with disabled 
//		* 4. It has "Cancel" button with disabled 
//		* 5. It has "View results" button with disabled 
//		* 6. It has "Job History Settings" with enabled
//	 * @throws Throwable 
//	    */
	
//	@Test
//	public void testJobsPageInfo(){
//		
//		TestLogger.logInfo("= = = = = = = = Start the test: Test Jobs Page... = = = = = = = =");
//		
//		tkMenu.selectMenu("jobs", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);	
//		tkJobsPage.verifyJobsPageInfo();
//		TestLogger.logInfo("= = = = = = = = End the test: Test Jobs Page... = = = = = = = =");
//	}
	
	/**
	    * Test and Verify Tuning Jobs page
	    * Attention: only call it in bvt bucket, do not register it in regression LUW test, the luw part will be included in z/OS case
	    * 1. Buttons: "View Results(disabled)", "Retune(disabled)", "Compare(disabled)", "Set Retention", "Cancel(disabled)", "Delete(disabled)"
		* 2. Icon buttons: "Search", "Refresh", "Columns", "Advanced filter"
		* 3. Verify a single-query job in BVT(open filter and filter out the job by name --> list the job --> open the job result --> verify --> close this tab)
		* 4. Clear filter -> select all -> delete all tuning jobs
		* @throws Throwable 
	    */
	@Test
	public void testTuningJobsPageInfo(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test Tuning Jobs Page... = = = = = = = =");
		tkMenu.selectMenu("root->tuningjobs", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);	
		tkTuningJobs.verifyTuningJobsPageInfo(true);
		TestLogger.logInfo("= = = = = = = = End the test: Test Tuning Jobs Page... = = = = = = = =");
	}
	
	
	//@AfterClass
	public void afetertest(){
		this.tkMenu.selectMenuwithoutLoadPage("root->optimize->root");
	}
}



