package com.ibm.datatools.dsm.tests.ui.testcase.tuning;


import java.awt.AWTException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ibm.datatools.dsm.tests.ui.testcase.base.TestCaseBase;
import com.ibm.datatools.test.utils.TestLogger;
import com.ibm.datatools.dsm.tests.ui.framework.core.WebDriverCore;

public class TestTuning_ZOS extends TestCaseBase {

	
	@BeforeClass
	public void beforetest(){
		tkHeader.pickDB(this.dbProfile);	
		if(!currentUser.equals(adminUser)){			
			this.tkMenu.selectMenuwithoutLoadPage("root->optimize->starttuning");
			this.tkDialog.handleDialogUser(this.dbProfile, this.dbUserName,this.dbPassword);
			WebDriverCore.pause(4);			
		}
	}	
	
	@Test
	public void runWorkloadTuneJobExecution(){		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Cache Source page... = = = = = = = =");
		tkMenu.selectMenu("root->optimize->starttuning",this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);		
		tkRunTuningJob.runWorkloadTuneJob("workloadTune");	
		TestLogger.logInfo("= = = = = = = = End the test: Test Cache Source page... = = = = = = = =");		
		
	}
	
	@Test
	public void runIAWhatIfImpactJobExecution(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test IA what-if impact job... = = = = = = = =");
		tkMenu.selectMenu("root->optimize->starttuning",this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);		
		String jobName = "singleTuneForZ";
		tkRunTuningJob.runSingleTuneJobForZ(jobName);	
		tkMenu.selectMenu("optimize->tuningjobs");		
		tkTuningJobs.runIAWhatIfImpact(jobName,"ia",false);	
		TestLogger.logInfo("= = = = = = = = End the test: Test IA what-if impact job... = = = = = = = =");		
	}
	
	@Test
	public void runWIAWhatIfImpactJobExecution(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test IA what-if impact job... = = = = = = = =");
		tkMenu.selectMenu("root->optimize->starttuning",this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);		
		String jobName = "workloadTuneForZ";
		tkRunTuningJob.runWorkloadTuneJob(jobName);	
		tkMenu.selectMenu("optimize->tuningjobs");		
		tkTuningJobs.runIAWhatIfImpact(jobName,"ia",true);	
		TestLogger.logInfo("= = = = = = = = End the test: Test IA what-if impact job... = = = = = = = =");		
		
	}
	
	@Test
	public void testEOPageInfo() throws Exception{
		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Optimize EO page... = = = = = = = =");
		tkMenu.selectMenu("root->optimize->tuningjobs");		
		tkEOJobPage.verifyJoblistPageInfo();	
		tkEOJobPage.createEOJob();
		tkEOJobPage.viewEOJobInfo();
		TestLogger.logInfo("= = = = = = = = End the test: Test Optimize EO page... = = = = = = = =");
	}
	
	
	@Test
	public void testMigrationPageInfo(){		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Migration page... = = = = = = = =");
		tkMenu.selectMenu("root->optimize->tuningutility->migration",this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);		
		tkMigratePage.verifyTuningMigrationPageInfo();	
		TestLogger.logInfo("= = = = = = = = End the test: Test Migration page... = = = = = = = =");		
		
	}
	
	@Test
	public void testFilterInfo(){		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Manage Filter page... = = = = = = = =");
		tkMenu.selectMenu("root->optimize->starttuning",this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);		
		tkFilterPage.verifyFilterPageInfo();	
		TestLogger.logInfo("= = = = = = = = End the test: Test Manage Filter page... = = = = = = = =");		
		
	}
	
	@Test
	public void testRebindInfo() throws AWTException{		
		TestLogger.logInfo("= = = = = = = = Start the test: Test Manage Filter page... = = = = = = = =");
		tkMenu.selectMenu("root->optimize->starttuning",this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);		
		tkRebindPage.verifyFilterPageInfo();
		tkMenu.selectMenu("root->optimize->starttuning",this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		tkRebindPage.backToSource();
		TestLogger.logInfo("= = = = = = = = End the test: Test Manage Filter page... = = = = = = = =");		
		
	}
	
	@Test
	public void testTuneDialogInfo() throws Exception{
		
		TestLogger.logInfo("= = = = = = = = Start the test: open tune dialog from sql editor... = = = = = = = =");
		tkMenu.selectMenu("root->runsql", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);	
		tkRunSQLPage.waitForPageLoadCompleted();
		tkRunSQLPage.inputSQL("SELECT * FROM SYSIBM.DBDR");
		tkRunSQLPage.tuneQuery();
		tkTuningJobs.verifyTuneDialog(this.tkDialog);
		TestLogger.logInfo("= = = = = = = = End the test: open tune dialog from sql editor... = = = = = = = =");
	}
	
	@Test
	public void testSingleJobAPAInfo(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test Single APA Recommendations... = = = = = = = =");
		tkMenu.selectMenu("root->optimize->tuningjobs", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);
		String jobName = "singleTune";
		tkTuningJobs.verifySingleJobAPAResult(jobName);
		TestLogger.logInfo("= = = = = = = = End the test: Test Single APA Recommendations ... = = = = = = = =");
	}	
	
	
	@Test
	public void testWAPAPageInfo(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test WAPA Page... = = = = = = = =");
		tkMenu.selectMenu("root->optimize->tuningjobs", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);	
		String jobName = "workloadTune";
		tkTuningJobs.verifyWorkloadJobWAPAResult(jobName);
		TestLogger.logInfo("= = = = = = = = End the test: Test WAPA Page... = = = = = = = =");
	}	
	
	@Test
	public void testWQAPageInfo(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test WQA Page... = = = = = = = =");
		tkMenu.selectMenu("root->optimize->tuningjobs", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);	
		String jobName = "workloadTune";
		tkTuningJobs.verifyWorkloadJobWQAResult(jobName);
		TestLogger.logInfo("= = = = = = = = End the test: Test WQA Page... = = = = = = = =");
	}	
	
	/**
	    * Test and Verify Tuning Jobs page
	    * 1. Buttons: "View Results(disabled)", "Retune(disabled)", "Compare(disabled)", "Set Retention", "Cancel(disabled)", "Delete(disabled)"
		* 2. Icon buttons: "Search", "Refresh", "Columns", "Advanced filter"
		* 3. Verify a single-query job (open filter and filter out the job by name --> list the job --> open the job result --> verify --> close this tab)
		* 4. Verify a single-query job and a z/OS workload in regression bucket 
		* 5. Clear filter -> select all -> delete all tuning jobs
		* @throws Throwable 
	    */
	@Test
	public void testTuningJobsPageInfo(){
		TestLogger.logInfo("= = = = = = = = Start the test: Test Tuning Jobs Page... = = = = = = = =");
		tkMenu.selectMenu("root->optimize->tuningjobs", this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);	
		tkTuningJobs.verifyTuningJobsPageInfo(false);
		TestLogger.logInfo("= = = = = = = = End the test: Test Tuning Jobs Page... = = = = = = = =");
	}	
	
	@Test
	public void testComparePkgAccessPlan() {
		TestLogger.logInfo("= = = = = = = = Start the test: Test compare package access plan... = = = = = = = =");
		tkMenu.selectMenu("root->optimize->starttuning",this.tkDialog, this.dbProfile, this.dbUserName,this.dbPassword);		
		tkRunTuningJob.runPackCompareApg(this.tkDialog);
		TestLogger.logInfo("= = = = = = = = End the test: Test compare package access plan... = = = = = = = =");
	}
}



