#!/usr/bin/env python
from xml.dom.minidom import parse
import xml.dom.minidom
import os
import sys

##function 1
def listxmlfiles(path): 
 xmlfilelists = [] 
 for root,dirs,files in os.walk(path):
  for f in files:
   sufix = os.path.splitext(f)[1][1:]
   if (sufix == "xml"):
    xmlfilelists.append(f)
  break
 return xmlfilelists 

##function 2
def updatexmlfile(prefix, targetfolder , originfolder, filename):
 prefix=prefix+"-"
 ##Read the XML file
 oldxmlfile=open(originfolder + "/" +filename)
 oldxml=oldxmlfile.read()
 oldxmlfile.close()

 doc = xml.dom.minidom.parseString(oldxml)
 collection = doc.documentElement

 ##Replace the value for the property name (just one element)
 name = collection.getAttribute("name")
 ##print "Root element : %s" % name
 collection.setAttribute("name", prefix+name)

 ##Replace the value for the property classname (one or more elements)
 testcases = collection.getElementsByTagName("testcase")
 for testcase in testcases: 
	classname =  testcase.getAttribute("classname")
	##print "Root element : %s" % classname
	testcase.setAttribute("classname", prefix+classname)

 newxml=doc.toprettyxml()
 filename=filename[:5] + prefix + filename[5:]
 xmlfile=open(targetfolder + "/" +filename ,"w")
 xmlfile.write(newxml);
 xmlfile.close()

##function 3
def parseArgument():
 if (len(sys.argv) != 4):
  raise Exception, u"Three arguments needed: migration scenario name, source folder name and target folder name"    
 #init and set
 argus = {}
 argus["name"] = sys.argv[1]
 argus["source"] = sys.argv[2]
 argus["target"] = sys.argv[3]
 return argus

if __name__ == '__main__':
 argus=parseArgument()
 for f in listxmlfiles(argus["source"]):
  updatexmlfile(argus["name"], argus["target"], argus["source"], f)
  






