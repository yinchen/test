#!/bin/bash

aps_tools_repo_path=$(cd `dirname $0`/..;cd ..;cd ..; pwd)
privateip=`cat $aps_tools_repo_path/build/cicd.properties |grep privateip|awk -F '=' '{print $2}'`

check_dsm_service() {
	URL=http://${privateip}:${dsm_port}/console/dswebcustomauth/pages/login.jsp
	HTTP_CODE=`curl -o /dev/null -s -w "%{http_code}" "${URL}"` 
	echo $HTTP_CODE
}

echo "privateip=$privateip"
echo "hostip=$hostip"
http_server=$hostip
echo "http_server=$http_server"
dsm_port="11080"
echo "dsm_port=$dsm_port"
http_port="80"
echo "http_port=$http_port"

check_dsm_service
if [ $HTTP_CODE -ne 200 ]; then
    echo "DSM is not ready!"
    echo "Please Manually check if your dsm is running on $privateip:$dsm_port"
    exit 1
fi

if [ "$addEmptyRepoDB" = "true" ]; then
	repodb="EMPTY"
else
	repodb="REPODB"
fi

##########JDK Environment##########
export JAVA_HOME="/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.91-0.b14.el6_7.x86_64"
export CLASS_PATH="$JAVA_HOME/lib:$JAVA_HOME/jre/lib"
export PATH="$PATH:$JAVA_HOME/bin"  

chmod -R 777 ${JAVA_HOME}

#automation="/var/jenkins/workspace/DSM_SIMPLE_Github/automation"
automation="/workspace/aps_tools/auto"

#RestAPI
cd ${automation}/auto.root/auto.api/enterprise/com.ibm.datatools.dsm.tests.api

sed -i 's#^data=.*#data={"host":"'${privateip}'","repodbName":"'${repodb}'"}#g'  testData/postData/connectionsData/TestAddRepoDB.properties
sed -i 's#^dsmserver=.*#dsmserver=http\\://'${privateip}'#g'  config/config_common.properties
#sed -i 's#^Referer=.*#Referer=http\\://'${privateip}'\\:'${dsm_port}'/dswebcustomauth/pages/login.jsp#g'  config/config_common.properties
sed -i 's#^dsmport=.*#dsmport='${dsm_port}'#g'  config/config_common.properties

sed -i 's#^host=.*#host='${privateip}'#g'  config/repositorydb.properties
sed -i 's#^dbProfile=.*#dbProfile='${repodb}'#g'  config/repositorydb.properties

cd ${automation}/auto.root
mvn clean test -P Ent_API_PBVT 

#Selenium UI

Xvfb :1 -ac&
export DISPLAY=:1
cd ${automation}/auto.root/auto.ui/com.ibm.datatools.dsm.tests.ui.enterprise

sed -i 's#^dsmserver=.*#dsmserver=http\\://'${privateip}'#g'  config/config.properties
sed -i 's#^dsmport=.*#dsmport='${dsm_port}'#g'  config/config.properties
sed -i 's#^httpserver=.*#httpserver=http\\://'${http_server}'#g'  config/config.properties
sed -i 's#^httpport=.*#httpport='${http_port}'#g'  config/config.properties
sed -i 's#^repoIP=.*#repoIP='${privateip}'#g'  config/config.properties
sed -i 's#^repoDatabase=.*#repoDatabase='${repodb}'#g'  config/config.properties
sed -i 's#^performance=.*#performance='bvt'#g'  config/config.properties

cd ${automation}/auto.root/auto.ui/com.ibm.datatools.dsm.tests.ui.base
chmod +x testFiles/others/geckodriver

cd ${automation}/auto.root
mvn clean test -P Ent_UI_BVT


#Softlink UI snapshot
if [ -d ${automation}/auto.root/auto.ui/com.ibm.datatools.dsm.tests.ui.enterprise/snapshots ]; then
    rm -r /builds/results/DSM_MAIN/N/snapshots
    cp -r ${automation}/auto.root/auto.ui/com.ibm.datatools.dsm.tests.ui.enterprise/snapshots /builds/results/DSM_MAIN/N/
fi

cd ${WORKSPACE}

if [ ! -d "test-output" ]; then 
    mkdir -p "test-output"
else
    rm -rf "test-output/*"
fi 

scp ${automation}/auto.root/auto.ui/com.ibm.datatools.dsm.tests.ui.enterprise/test-output/junitreports/*  ${WORKSPACE}/test-output/
scp ${automation}/auto.root/auto.api/enterprise/com.ibm.datatools.dsm.tests.api/test-output/junitreports/*  ${WORKSPACE}/test-output/
