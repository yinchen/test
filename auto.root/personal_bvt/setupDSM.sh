#!/bin/bash

export BUILD_ID="DonotKillMe"
cp /workspace/aps_tools/build/jenkins_jobs/job2/docker /usr/bin/

setup_dsm() {
    dsmbuild=ibm-datasrvrmgr-main-linux-x86_64.tgz
    DSMFolder="ibm-datasrvrmgr"
    myPath="/data/DSMbuild"
    ##########################Enter the default directory that install the DSM build############################
    if [ ! -d "$myPath" ]; then 
    mkdir -p "$myPath" 
    fi 
    cd "$myPath"
    ##############Delete old DSM build##############
    if [ -d "$DSMFolder" ]; then
    ./$DSMFolder/bin/stop.sh	   
    rm -rf "$DSMFolder"
    echo "Old DSM build file is deleted!"
    fi 
    ##########################Get the latest DSM build version############################
    cp /builds/results/DSM_MAIN/N/goodbuilds/ots/goodbuild.txt .
    build=$(cat goodbuild.txt)
    ##########################Get the latest DSM build version############################
    cp /builds/results/DSM_MAIN/N/${build}/$dsmbuild .
    ##########################Install DSM build ############################
    tar -xvf $dsmbuild
    rm ibm-datasrvrmgr/setup.conf
    cp /workspace/aps_tools/build/jenkins_jobs/job2/setup.conf ibm-datasrvrmgr/
    

     ######################Check LDAP Service############################
    if [[ $ldap == true ]]; then
    echo "Stop and remove old LDAP container and start a new one"
    docker -H ${hostip}:2375 stop ldap_dsm && docker -H $1:$2 rm ldap_dsm
    docker -H ${hostip}:2375 run -itd --name ldap_dsm -p 10389:10389 docker-registry-21.stage1.edge.bluemix.net/library/ldap:1108
    echo "=================Add this ldap info into dsm config================="
    rm ibm-datasrvrmgr/Config_tmp/customldap.properties
    cp /workspace/aps_tools/build/jenkins_jobs/job2/customldap.properties ibm-datasrvrmgr/Config_tmp/
    sed -i 's/ldap.host=.*/ldap.host='${hostip}'/g' ibm-datasrvrmgr/Config_tmp/customldap.properties
    cat ibm-datasrvrmgr/Config_tmp/customldap.properties
    echo "===================================================================="
    echo "dsweb.customauth.ldap=true" >> ibm-datasrvrmgr/Config_tmp/dswebserver_override.properties
    cat ibm-datasrvrmgr/Config_tmp/dswebserver_override.properties
    echo "===================================================================="
    fi

     ##########################Setup DSM  ############################
     ##for ssh
    echo -e "\ndsweb_set_ssh_port=2222" >> ibm-datasrvrmgr/Config_tmp/dswebserver.properties
    ##for enable WatchConn 
    echo -e "\nENABLE_WATCH_CONN=true" >> ibm-datasrvrmgr/Config_tmp/dswebserver.properties
    echo -e 'SERVER_JAVA_OPTS="$SERVER_JAVA_OPTS -DENABLE_WATCH_CONN=true"' >> ibm-datasrvrmgr/bin/serverenv.sh
    echo -e "export SERVER_JAVA_OPTS" >> ibm-datasrvrmgr/bin/serverenv.sh
    ##for JDBC timeout
    sed -i 's#^dsweb.java.sql.drivermanager.logintimeout=.*#dsweb.java.sql.drivermanager.logintimeout=60#g' ibm-datasrvrmgr/Config_tmp/dswebserver_override.properties
	cat ibm-datasrvrmgr/Config_tmp/dswebserver.properties
    echo "===================================================================="
    cd  $myPath/$DSMFolder
     ./setup.sh -silent
     rm -rf $myPath/$dsmbuild
}


######################Check DSM Service############################
URL=http://${hostip}:11080/console/dswebcustomauth/pages/login.jsp
retry1=1
retry2=1
OK=0
while [[ $retry1 -lt 3 ]]; do
	setup_dsm
    while [[ $retry2 -lt 19 ]]; do
        HTTP_CODE=`curl -o /dev/null -s -w "%{http_code}" "${URL}"` 
		echo $HTTP_CODE
		if [[ $HTTP_CODE -eq 200 ]]; then
        	OK=1
            break
        fi
        ((retry2 ++))
        sleep 5s
    done    
    if [[ $OK -eq 1 ]]; then
    	echo "DSM is ready! Triggering BVT job"
        echo "===================================================================="
        cat ${myPath}/ibm-datasrvrmgr/Config/dswebserver_override.properties
        echo "===================================================================="
        cat ${myPath}/ibm-datasrvrmgr/Config/dswebserver.properties
        echo "===================================================================="
        break
    fi
    ((retry1 ++))
done

if [[ $OK -eq 0 ]]; then
	echo "DSM service is not available after 2 tries, Terminate the job!!!"
    exit 1
fi



