 #!/bin/bash
#######################################################################################
## This script will download the latest DSM build, setup DSM without configuring repository db 
## OR config with specified repository db, and finally start DSM for testing... ## 
##
## Usage: ./DSM_Setup_Linux.sh  (The script will be called in Jenkins)
## Created on 2016-12-05
## Version: 2.0
#######################################################################################


#########################Parameters Initialization############################

#BuildNodeIP is the variable from Jenkins job

# steady ***
dsmInstallPath="/data/DSMbuild"
versionFile="goodbuild.txt"
versionOldFile="goodbuild_old.txt"
propertiesFile="goodbuild.properties"
extension="tgz"
zmonlicenseName="ibm-datasrvrmgr-2.1.5-zos_mon-license-activation-kit-linux-x86_64"
zconlicenseName="ibm-datasrvrmgr-2.1.5-zos_con-license-activation-kit-linux-x86_64"
dsmFolder="ibm-datasrvrmgr"


# steady **
if [ ! -n "$BuildVersion" ]; then
      BuildVersion="2.1.5"	  
fi
# steady *
if [ ${BuildVersion} == "DSM_Master" ]; then
	buildName="ibm-datasrvrmgr-NEW_INSTALL-main-linux-x86_64"
    buildPath="http://ds-fs01.svl.ibm.com/ots/ots_builds/DSM_MAIN/N"
elif [ ${BuildVersion} == "enterprise" ]; then
	buildName="ibm-datasrvrmgr-main-linux-x86_64"
    buildPath="http://9.30.122.83"
else
	buildName="ibm-datasrvrmgr-2.1.5-linux-x86_64"
	buildPath="http://9.30.248.224"	
fi

# steady **
retry=1
maxRetry=3

# steady *

username="feiye@cn.ibm.com"


goto_dsm_install_path() {
	if [ ! -d "$dsmInstallPath" ]; then 
		mkdir -p "$dsmInstallPath" 
	fi
	cd "$dsmInstallPath"	
}

remove_version_file() {
	if [ "$alwaysSetup" = "true" ]; then
		rm -rf "$versionFile"
	fi
}

check_new_build() {
	OLD="0"
	if [ -f "$versionFile" ]; then
	   OLD=`cat goodbuild.txt`
	   mv "$versionFile" "$versionOldFile"
	fi
	wget --user=$username --password=$password --no-check-certificate ${buildPath}/goodbuilds/ots/goodbuild.txt 
	#wget --no-check-certificate ${buildPath}/goodbuilds/ots/goodbuild.txt                                                          
	LATEST=`cat goodbuild.txt`
	###Exit 1:  No new build
	if [ "$OLD" = "$LATEST" ]; then
		rm -rf "$versionOldFile"
		echo "No new build, Terminate the job!!!"
		exit 1
	fi
}

#For internal
restore_version_file() {
	rm -rf "$versionFile"
	mv "$versionOldFile" "$versionFile"
}

download_latest_build() {
	echo "Latest build is $LATEST"
	buildifExist=${buildName}_${LATEST}.${extension}
	echo $buildifExist
	if [ -e "$buildifExist" ];then
	   echo "######The latest build exists, so it needn't be download!######"
	else
	   echo "######The latest build doesn't exist, so it need be download! Now begin to download it!######"
	   #wget --user=$username --password=$password --no-check-certificate ${buildPath}/dsw-wlmweb/$LATEST/install/ots-packages/${buildName}.${extension} -O #${buildName}_${LATEST}.${extension}
	   wget --no-check-certificate ${buildPath}/$LATEST/${buildName}.${extension} -O ${buildName}_${LATEST}.${extension}
	   #wget --no-check-certificate ${buildPath}/dsw-wlmweb/$LATEST/install/ots-packages/${buildName}.${extension}   
	   #mv ${buildName}.${extension} ${buildName}_${LATEST}.${extension}
	fi
	###Exit 2: The latest build file does not exist
	if [ ! -f ${buildName}_${LATEST}.${extension} ]; then
		restore_version_file
		echo "The latest build file does not exist, Terminate the job!!!"
		exit 1
	fi 
}

get_lisence_path() {

	#if [ ${BuildVersion} == "enterprise" ]; then
	#	lisensePath="http://ds-fs01.svl.ibm.com/ots/dsm_releases/DSM_V2.1.4/2.1.4_GM_N20170616_1037_LAK"
	#else
	#	lisensePath="${buildPath}/dsw-wlmweb/${LATEST}/install/ots-packages"	
	#fi
	
	lisensePath="http://ds-fs01.svl.ibm.com/ots/dsm_release_work/DSM215_GM/20180104_1856"

}

download_latest_zmon_license() {
	echo "Latest zmonlicense is $LATEST"
	zmonlicenseifExist=${zmonlicenseName}_${LATEST}.${extension}
	echo $zmonlicenseifExist
	if [ -e "$zmonlicenseifExist" ];then
	   echo "######The latest zmonlicense exists, so it needn't be download!######"
	else
	   echo "######The latest zmonlicense doesn't exist, so it need be download! Now begin to download it!!!######"
	   wget --user=$username --password=$password --no-check-certificate ${lisensePath}/${zmonlicenseName}.${extension} -O ${zmonlicenseName}_${LATEST}.${extension}
	   #wget --no-check-certificate ${lisensePath}/${zmonlicenseName}.${extension}
	   #mv ${zmonlicenseName}.${extension} ${zmonlicenseName}_${LATEST}.${extension}
	fi
	###Exit 3: The latest zmonlicense file does not exist
	if [ ! -f ${zmonlicenseName}_${LATEST}.${extension} ]; then
		rm -rf ../*.${extension}
		restore_version_file	
		echo "The latest zmonlicense file does not exist, Terminate the job!!!"
		exit 1
	fi
}

download_latest_zcon_license() {
	echo "Latest zconlicense is $LATEST"
	zconlicenseifExist=${zconlicenseName}_${LATEST}.${extension}
	echo $zconlicenseifExist
	if [ -e "$zconlicenseifExist" ];then
	   echo "######The latest zconlicense exists, so it needn't be download!######"
	else
	   echo "######The latest zconlicense doesn't exist, so it need be download! Now begin to download it!!!######"
	   wget --user=$username --password=$password --no-check-certificate ${lisensePath}/${zconlicenseName}.${extension} -O ${zconlicenseName}_${LATEST}.${extension}
	   #wget --no-check-certificate ${lisensePath}/${zconlicenseName}.${extension}
	   #mv ${zconlicenseName}.${extension} ${zconlicenseName}_${LATEST}.${extension}
	fi
	###Exit 4: The latest zconlicense file does not exist
	if [ ! -f ${zconlicenseName}_${LATEST}.${extension} ]; then
		rm -rf ../*.${extension}
		restore_version_file	
		echo "The latest zconlicense file does not exist, Terminate the job!!!"
		exit 1
	fi
}

uninstallDSM() {
	###Delete the old DSM build files
	if [ -d "$dsmFolder" ]; then
	    DSM_PID=`ps -ef | grep ibm-datasrvrmgr | grep java | sed '/grep/d' | awk '{print $2}'`
		./$dsmFolder/bin/stop.sh
		sleep 5s
        if [[ -n "${DSM_PID}" ]]; then
		   echo "There still has other DSM process running, now kill it ..."
		   echo "Running DSM PID: ${DSM_PID}"
		   for PidNum in ${DSM_PID}
		   do
			 kill -9 ${PidNum}
		   done
		fi 		
		rm -rf "$dsmFolder"
		echo "Old DSM build file is deleted!"
	fi

}

installDSM() {
	uninstallDSM
	###Untar the DSM build
	tar -zxf ${buildName}_${LATEST}.${extension}
	###Untar the DSM zmonlicese
	tar -zxf ${zmonlicenseName}_${LATEST}.${extension} -C ${dsmFolder}/
	###Untar the DSM zconlicese
	tar -zxf ${zconlicenseName}_${LATEST}.${extension} -C ${dsmFolder}/
	###Enter the DSM installation directory 
	cd "$dsmInstallPath/$dsmFolder"
	###Config the setup.conf file
	if [ "$addRepoDB" = "true" ]; then
		Key=( product.license.accepted= admin.user= admin.password= repositoryDB.dataServerType= repositoryDB.host= repositoryDB.port= repositoryDB.databaseName= repositoryDB.user= repositoryDB.password= )
		Value=( y admin password DB2LUW 127.0.0.1 65000 EMPTY db2inst1 N1cetest )
	else
		Key=( product.license.accepted= admin.user= admin.password= )
		Value=( y admin password )	
	fi
	###Getting the crypt value of password
	Value[2]=`./dsutil/bin/crypt.sh "${Value[2]}" "${Value[2]}"`
	echo "The encrypted admin password is ${Value[2]}"
	Value[8]=`./dsutil/bin/crypt.sh "${Value[8]}" "${Value[8]}"`
	echo "The encrypted repositoryDB password is ${Value[8]}"
	###Uncomment line including the Key and update the corresponding Value of the Key
	for ((i=0; i<${#Key[@]}; ++i))
	do
		  tempKey=`grep "${Key[$i]}" ./setup.conf | awk -F"=" '{print $1}'`
		  sed -i "s/${tempKey}/${Key[$i]%?}/" ./setup.conf
		  tempValue=`grep "${Key[$i]}" ./setup.conf | awk -F"=" '{print $2}'`
		  sed -i "s/${Key[$i]}${tempValue}/${Key[$i]}${Value[$i]}/" ./setup.conf
	done
	###Cat the content fo setup.conf after updating it
	cat ./setup.conf
	##for enable WatchConn 
    #echo -e "\nENABLE_WATCH_CONN=true" >> ./Config_tmp/dswebserver.properties
    #echo -e 'SERVER_JAVA_OPTS="$SERVER_JAVA_OPTS -DENABLE_WATCH_CONN=true"' >> ./bin/serverenv.sh
    #echo -e "export SERVER_JAVA_OPTS" >> ./bin/serverenv.sh
	
	### Update the jdbc connection timeout 
	#echo "Update the jdbc connection timeout"
	#sed -i 's#^dsweb.java.sql.drivermanager.logintimeout=.*#dsweb.java.sql.drivermanager.logintimeout=120#g' ./Config_tmp/dswebserver_override.properties
	                                                                                                       
	#echo "update the setting for dsweb_tls_disabledAlgorithms"
	#sed -i 's#^dsweb_tls_disabledAlgorithms=.*#dsweb_tls_disabledAlgorithms=DHE_RSA#g' ./Config_tmp/dswebserver_override.properties                                                                                             
	
	#echo -n "DSINSTALL_TRACE=true" >> ./Config_tmp/dswebserver_override.properties
	###start the DSM to let the setup.conf work
	./setup.sh -silent
	####Remove old goodbuild file 
	rm -rf ../${versionOldFile}
	check_dsm_service
}

upgradeDSM() {
	uninstallDSM
    setupDSM2.1.4
	
	if [ -d "$dsmFolder" ]; then
	    DSM_PID=`ps -ef | grep ibm-datasrvrmgr | grep java | sed '/grep/d' | awk '{print $2}'`
		./$dsmFolder/bin/stop.sh
		if [[ -n "${DSM_PID}" ]]; then
		   echo "There still has other DSM process running, now kill it ..."
		   echo "Running DSM PID: ${DSM_PID}"
		   for PidNum in ${DSM_PID}
		   do
			 kill -9 ${PidNum}
		   done
		fi 		
		echo "Old DSM service is stopped!"
		
	fi		
	tar -zxf ${buildName}_${LATEST}.${extension}	
	cd "$dsmInstallPath/$dsmFolder"
	./setup.sh -silent	
	rm -rf ../${versionOldFile}	
	check_dsm_service
}
#this file is used by Jenkins
write_properties_file() {
	###Writedown DSM version to goodbuild.properties file
	if [ ! -f "$dsmInstallPath/$propertiesFile" ]; then
	   touch "$dsmInstallPath/$propertiesFile"
	   echo "productBuildID=" >> "$dsmInstallPath/$propertiesFile"
	   echo "DSMBuildLink=" >> "$dsmInstallPath/$propertiesFile"
	   echo "DSMURL=" >> "$dsmInstallPath/$propertiesFile"
	   echo "BuildVersion=" >> "$dsmInstallPath/$propertiesFile"
	   echo "BuildJob_Number=" >> "$dsmInstallPath/$propertiesFile"
	fi
	sed -i 's#^productBuildID=.*#productBuildID='${LATEST}'#g'  $dsmInstallPath/$propertiesFile
	sed -i 's#^DSMBuildLink=.*#DSMBuildLink='${buildPath}/${LATEST}/'#g'  $dsmInstallPath/$propertiesFile
	sed -i 's#^DSMURL=.*#DSMURL='${URL}'#g'  $dsmInstallPath/$propertiesFile
	sed -i 's#^BuildVersion=.*#BuildVersion='${BuildVersion}'#g'  $dsmInstallPath/$propertiesFile
	if [ -n "$BuildJob_Number" ]; then
		sed -i 's#^BuildJob_Number=.*#BuildJob_Number='${BuildJob_Number}'#g'  $dsmInstallPath/$propertiesFile
	fi
}

#For internal
check_dsm_service() {
    sleep 3m
	##DSM Cloud service is Ready or Not!!!
	URL=http://${NODE_NAME}:11080/console/dswebcustomauth/pages/login.jsp
	echo $URL 
	HTTP_CODE=`curl -o /dev/null -s -w "%{http_code}" "${URL}"` 
	echo $HTTP_CODE
}

retryDSM() {
	###Quit: The DSM service is not available
	while [[ $HTTP_CODE != 200 ]]
	do
	  if [ $retry -lt $maxRetry ]
	  then
		echo "The DSM service is not available, Re-start again!!!"
		cd "$dsmInstallPath"
		###
		if [ "$newInstall" = "true" ]; then	
			installDSM  
		else
			upgradeDSM
		fi
		###
		((retry ++))  
	  else
		echo "The DSM service is not available, Terminate the job!!!"
		exit 1
	  fi  
	done
	###Remove Build zip files
	#rm -rf ../*.${extension}	
	rm -rf $dsmInstallPath/*.${extension}
}

setupDSM2.1.3() {	
	
	if [ "$migrationType" = "BLU" ]; then	
		#restore the DSM 2.1.2 from backup, will update the function later 
		echo "The migration type is $migrationType"
		unzip -q -o /data/DSM2.1.3_BLU/ibm-datasrvrmgr.zip  -d $dsmInstallPath
	elif [ "$migrationType" = "ZOS" ]; then 
		#restore the DSM 2.1.2 from backup, will update the function later 
		echo "The migration type is $migrationType"
		unzip -q -o /data/DSM2.1.3_ZOS/ibm-datasrvrmgr.zip  -d $dsmInstallPath
    else	
		#restore the DSM 2.1.2 from backup, will update the function later 
		echo "The migration type is $migrationType"
		unzip -q -o /data/DSM2.1.3_LUW/ibm-datasrvrmgr.zip  -d $dsmInstallPath
	fi
}

setupDSM2.1.2() {	
	
	if [ "$migrationType" = "BLU" ]; then	
		#restore the DSM 2.1.2 from backup, will update the function later 
		echo "The migration type is $migrationType"
		unzip -q -o /data/DSM2.1.2_BLU/ibm-datasrvrmgr.zip  -d $dsmInstallPath
	elif [ "$migrationType" = "ZOS" ]; then 
		#restore the DSM 2.1.2 from backup, will update the function later 
		echo "The migration type is $migrationType"
		unzip -q -o /data/DSM2.1.2_ZOS/ibm-datasrvrmgr.zip  -d $dsmInstallPath
    else	
		#restore the DSM 2.1.2 from backup, will update the function later 
		echo "The migration type is $migrationType"
		unzip -q -o /data/DSM2.1.2/ibm-datasrvrmgr.zip  -d $dsmInstallPath
	fi
}

setupDSM2.1.4() {	
	
	if [ "$migrationType" = "BLU" ]; then	
		echo "The migration type is $migrationType"
		unzip -q -o /data/DSM214_BLU/ibm-datasrvrmgr.zip  -d $dsmInstallPath
	elif [ "$migrationType" = "ZOS" ]; then 
		echo "The migration type is $migrationType"
		unzip -q -o /data/DSM214_ZOS/ibm-datasrvrmgr.zip  -d $dsmInstallPath
    else	
		echo "The migration type is $migrationType"
		unzip -q -o /data/DSM214_LUW/ibm-datasrvrmgr.zip  -d $dsmInstallPath
	fi
}

setupJRE() {
	export JAVA_HOME=$dsmInstallPath/$dsmFolder/java/jre
	export CLASS_PATH=$JAVA_HOME/lib:$JAVA_HOME/jre/lib
	export PATH=$JAVA_HOME/bin:$PATH
	chmod -R 777 ${JAVA_HOME}
}

setupJRE
#Run script functions
goto_dsm_install_path
remove_version_file
check_new_build
download_latest_build

##new install or upgrade
if [ "$newInstall" = "true" ]; then	
    get_lisence_path
	download_latest_zmon_license
	download_latest_zcon_license	
	installDSM
else		
    upgradeDSM
fi
write_properties_file
retryDSM











