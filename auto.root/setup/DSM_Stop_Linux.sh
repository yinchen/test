 #!/bin/bash
#######################################################################################
## This script will stop the DSM build## 
##
## Usage: ./DSM_Stop_Linux_All.sh  (The script will be called in Jenkins)
## Created on 2016-12-05
## Version: 2.0
#######################################################################################


# steady ***
dsmInstallPath="/data/DSMbuild"
dsmFolder="ibm-datasrvrmgr"



goto_dsm_install_path() {
	if [ ! -d "$dsmInstallPath" ]; then 
		mkdir -p "$dsmInstallPath" 
	fi
	cd "$dsmInstallPath"	
}


stopDSM() {
	###Delete the old DSM build files
	if [ -d "$dsmFolder" ]; then
	    DSM_PID=`ps -ef | grep ibm-datasrvrmgr | grep java | sed '/grep/d' | awk '{print $2}'`
		./$dsmFolder/bin/stop.sh
		sleep 5s
        if [[ -n "${DSM_PID}" ]]; then
		   echo "There still has other DSM process running, now kill it ..."
		   echo "Running DSM PID: ${DSM_PID}"
		   for PidNum in ${DSM_PID}
		   do
			 kill -9 ${PidNum}
		   done
		fi 		
		echo "Old DSM build file is stopped!"
	fi

}


#Run script functions
goto_dsm_install_path
stopDSM













