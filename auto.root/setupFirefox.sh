#!/bin/sh

## initial parmeters
firefoxDownloadPath="/opt"
firefoxPath="/opt/firefox/firefox"
firefoxBinPath="/usr/bin/firefox"
firefoxBinPathOld="usr/bin/firefoxold"
firefoxVersion=""

## Start config Ubuntu system to run Selenium UI automation
echo "Start config Ubuntu system to run Selenium UI automation"

## Check the necessary packages 
## Check if package libgtk-3-0 installed on Ubuntu
echo "Check if package libgtk-3-0 installed on Ubuntu"
if [ `dpkg -l|grep libgtk-3-0|wc -l` -ne 0 ];then
        echo "Package libgtk-3-0 already installed!"
else
        echo "Package libgtk-3-0 NOT installed! Install it now!"
        apt-get install -y libgtk-3-0
fi

## Check if package libasound2 installed on Ubuntu
echo "Check if package libasound2 installed on Ubuntu"
if [ `dpkg -l|grep libasound2|wc -l` -ne 0 ];then
        echo "Package libasound2 already installed!"
else
        echo "Package libasound2 NOT installed! Install it now!"
        apt-get install -y libasound2
fi


## Check if package libdbus-glib-1-2 installed on Ubuntu
echo "Check if package libdbus-glib-1-2 installed on Ubuntu"
if [ `dpkg -l|grep libdbus-glib-1-2|wc -l` -ne 0 ];then
        echo "Package libdbus-glib-1-2 already installed!"
else
        echo "Package libdbus-glib-1-2 NOT installed! Install it now!"
        apt-get install -y libdbus-glib-1-2
fi

## Check if Firefox installed on Ubuntu
echo "Check if Firefox installed on Ubuntu"
if [ ! -d "$firefoxBinPath" ]; then
        echo "No Firefox installed on Ubuntu!"
else
        echo "Check Firefox version, if not version 48, remove the old and install new Firefox!"
        firefoxVersion=`firefox -version`

        if [ "$firefoxVersion" = "Mozilla Firefox 48.0" ]; then
                echo "Already installed Firefox 48, do nothing!"
                exit 0
        else
                echo "The installed Firefox not version 48, remove it!"
                mv $firefoxBinPath $firefoxBinPathOld
        fi
fi

## Download Firefox 48 binary from ftp site and unzip it
if [ ! -d "$firefoxDownloadPath" ]; then 
    mkdir -p "$firefoxDownloadPath" 
fi 

#cd $firefoxDownloadPath

#echo "Download Firefox 48 binary from ftp site"
#wget --user=shareuser --password=n1cetest --no-check-certificate ftp://9.123.120.32/individual/Helen/Browser/firefox-48.0.tar.bz2 1>/dev/null
#echo "Download Firefox 48 binary Done!"

echo "Unzip Firefox 48 binary"
tar -xjf $firefoxDownloadPath/firefox-48.0.tar.bz2 -C $firefoxDownloadPath
echo "Unzip Firefox 48 binary Done!"

## Install Firefox 48 on Ubuntu
echo "Install Firefox 48 on Ubuntu"
ln -s $firefoxPath $firefoxBinPath

## Install and configure Xvfb on Ubuntu
echo "Install Xvfb on Ubuntu"
apt-get -y install Xvfb

echo "Start Xvfb service on Ubuntu"
#Xvfb :1 -ac 2>/dev/null &
echo 'Xvfb :1 -ac 2>/dev/null &'>>/root/.bashrc 

echo "export DISPLAY=:1"
export DISPLAY=:1

## Configure hostname 
echo "Configure hostname in etc/hosts"
hostname=`hostname`
tmp=`cat /etc/hosts`
echo "127.0.0.1 localhost $hostname\n$tmp" > /etc/hosts

echo "Ubuntu system is ready to run Selenium UI automation!"
