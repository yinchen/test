-------------------------------------------------------------------------------
Author: Craig Tobias, ctobias@ca.ibm.com

-------------------------------------------------------------------------------
INTRODUCTION

The directory that contains this README file, (which we will call the "frisby"
directory) contains automated test cases for the dashDB REST
APIs that are built using a REST testing framework called "frisby". 
Frisby is a REST API testing framework build on node.js and Jasmine.
For more details visit "frisbyjs.com". 

The tests can be run in the following ways:
* Using Jenkins - select/input the REST-Frisby test
* From the command line using the run_tests.sh script - this runs all tests
* From the command line using ant - you can run one or more tests
* Using Eclipse - you can run one test at a time



-------------------------------------------------------------------------------
Setup for manual test runs (i.e. not Jenkins) 
=============================================

1. Install Node.js

   Before you can run the frisby.js test cases for dashDB, you must download and
   install node.js from "https://nodejs.org/en/download/".  
   
   After installing node.js, restart Eclipse to ensure it picks up the new 
   path environment for node.js".
   
2. (Eclipse only) Add Frisby ANT Build file
   In Eclipse, open ANT view.  Right click the ant view and select "Add Buildfiles...".
   Navigate to the "frisby" directory select "build.xml".
   
   The ANT build script has targets for installing the frisby framework as well as
   for running the testcases. Expand the "REST API Testing with Frisby.js" to see the 
   targets.

3. Create user in dashdb

4. Set required environment variables:

    export DASHDB_SERVER=<dashDB server IP address>
    export DASHDB_USER=<user name of dashDB user under which you want to run tests>
    export DASHDB_PASSWORD=<password for $DASHDB_USER>

  NOTE: if you are using Eclipse, you'll need to restart Eclipse after setting these variables

5. Create the tables on the dashDB server

   The file "misc/setup.ddl" contains the DDL for tables required by the
   testsuite. You must create these tables prior to test case execution.
   Tables must be created under schema <USER>.

   Create the tables using node:
      $ node misc/run_clp_script.js misc/setup.ddl @

   Alternatively, to run the script on a dashDB machine, 
   1. Copy the script to the test machine
   2. Log on to the machine
   3. connect as user <user> and run as follows:
     $ db2 -td@ -vf setup.ddl

   The tables can also be create using the dashDB web console, but you
   Only need to run the CREATE TABLE statements.

6. Upload files that are needed by the tests

   Run the "setup_files" ant target
 

-------------------------------------------------------------------------------
Running the tests manually:
===========================

Using run_tests.sh:
  run `bash ./run_tests.sh` . You will see a file `results.txt` that contains 
  summary info on the test run. For each test script that was run, there will 
  be a file in the `out` directory that contains the output of that test script.
  

Using ant:
  Running `ant -p` displays a list of ant targets. Valid tests begin with "RUN:".
  call `ant "<test target>" ...` to run one or more tests. Output appears in your
  terminal.

 
Using Eclipse:
  Running the tests is as simple as double clicking the ant targets.  Each 
  test target is prefixed with "RUN:" followed by the testcase name.



  The directory "./reportfolder" contains the logs for the latest run
  of each test case.  These are formatted as JUnit xml files, and can be 
  inspected within the JUnit view in Eclipse, or using a standard text editor.o
  
  There may be multiple XML files per test script.


Cleanup:
=======

1. Run the misc/cleanup.ddl script in a similar way to how it was done for setup.
2. Delete dashDB user
3. Uninstall Node.js


-------------------------------------------------------------------------------
FOLDERS within the "frisby" directory
=====================================

* misc - Contains the DDL files for any tables used by the REST APIs as well as setup scripts
* reportfolder - Contains log output from the last test runs in JUnit format.
* spec - Contains all the testcases
* datasets - CSV, XLS files used for the load REST APIs.
* lib - contains common modules
* out - where output from run_tests.sh will go



