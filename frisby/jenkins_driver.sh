#!/bin/bash -x



cd $(dirname $0)

echo "PATH=$PATH"

[ -z "$serverIP" ]       && echo "ERROR: required environment variable serverIP was not set" && exit 1 
[ -z "$dashDBUser" ]     && echo "ERROR: required environment variable dashDBUser was not set" && exit 1
[ -z "$dashDBPassword" ] && echo "ERROR: required environment variable dashDBPassword was not set" && exit 1 
[ -z "$tag" ] && echo "ERROR: required environment variable tag was not set" && exit 1 
#[ -z "$planTag" ] && echo "ERROR: required environment variable planTag was not set" && exit 1 
export SMPTYPE=$planTag
export NODETYPE=$tag
export DASHDB_SERVER=$serverIP
export DASHDB_USER=$dashDBUser
export DASHDB_PASSWORD=$dashDBPassword

setup ()
{
  retVal=0
  echo
  echo "============================================================================="
  echo " $(date) Setup"
  echo "============================================================================="
  echo 
  # nuke node_modules and install dependencies
  if [ -e ./node_modules ]
  then
    echo "Removing node_modules"
    rm -rf ./node_modules
      rc=$? ; [ $rc != 0 ] && echo "ERROR: Previous command returned $rc" && retVal=$rc
  fi

  echo "Running npm install to install dependencies"
  npm install
    rc=$? ; [ $rc != 0 ] && echo "ERROR: Previous command returned $rc" && retVal=$rc
  ant "setup_files" 
    rc=$? ; [ $rc != 0 ] && echo "ERROR: Previous command returned $rc" && retVal=$rc

  node misc/run_clp_script.js misc/setup.ddl @
    rc=$? ; [ $rc != 0 ] && echo "ERROR: Previous command returned $rc" && retVal=$rc

  if [ $retVal != 0 ]
  then
    echo "$(date) : ERROR : Setup failed. Returning $return"
  else
    echo "$(date) : Setup successful"
  fi
  echo "============================================================================="

  return $retVal
}

run_test()
{
  echo
  echo "============================================================================="
  echo " $(date) Running tests"
  echo "============================================================================="
  echo 
  ./run_tests.sh
  retVal=$?
  if [ $retVal != 0 ]
  then
    echo "$(date) : ERROR : Setup failed. Returning $return"
  else
    echo "$(date) : Setup successful"
  fi
  echo "============================================================================="

  return $retVal
}

cleanup()
{
  retVal=0
  echo
  echo "============================================================================="
  echo " $(date) Cleanup"
  echo "============================================================================="
  echo 

  node misc/run_clp_script.js misc/cleanup.ddl 
    rc=$? ; [ $rc != 0 ] && echo "ERROR: Previous command returned $rc" && retVal=$rc

  if [ $retVal != 0 ]
  then
    echo "$(date) : ERROR : Cleanup failed. Returning $return"
  else
    echo "$(date) : Cleanup successful"
  fi
  echo "============================================================================="

  return $retVal
}

export SMPTYPE=$planTag
export NODETYPE=$tag
export DASHDB_SERVER=$serverIP
export DASHDB_USER=$dashDBUser
export DASHDB_PASSWORD=$dashDBPassword

setup
exitStatus=$?
if [ $exitStatus != 0 ]
then
  cleanup
  exit $exitStatus
fi

run_test
exitStatus=$?

# if test fails, try and copy log files
if [ $exitStatus != 0 ] && [ ! -z "$JOB_LOGS" ]
then
  
   if [ -e $JOB_LOGS ]
   then
      if [ -d $JOB_LOGS ]
	  then
         thingsToSave="out results.txt"
		 files=""
		 for f in $thingsToSave
		 do
            [ -e $f ] && files="$files $f"
		 done
		 
		 if [ ! -z "$files" ]
	     then
            echo "INFO: Saving logs \"$files\" to $JOB_LOGS"
		    cp -r $files $JOB_LOGS
		else
            echo "WARNING: No log files to save. Possible files were $thingsToSave"
		fi
	  else
         echo "WARNING: JOB_LOGS \"$JOB_LOGS\" exists but is not a directory. Cannot save log files"
      fi
   else
      echo "WARNING: JOB_LOGS \"$JOB_LOGS\" does not exist. Cannot save log files"
   fi
fi

cleanup

exit $exitStatus

