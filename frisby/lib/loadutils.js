var expect = require('expect');
var fs = require('fs');
var request = require('sync-request');
var AdmZip = require('adm-zip');
var frisby = require('frisby');
var server = require("./server.js")
var sleep = require('sleep');
var ibmdb = require("ibm_db");
var path = require('path');




/*
 *
 * Function that verified the load results returned by the REST API
 *
 * we accept the old calling convention:
 *  arg 1 : jsonPassedIn (object)
 *  arg 2 : numRowsRead (number)
 *  arg 3 : numRowsLoaded (number)
 *  arg 4 : numRowsCommitted (number)
 *  arg 5 : numRowsDeleted (number)
 *  arg 6 : numRowsSkipped (number)
 *  arg 7 : errorMessageCode (string - optional)
 *
 *  or the new one:
 *  arg 1 : jsonPassedIn (object)
 *  arg 2 : expected (object)
 *     {
 *         numRowsRead : number
 *         numRowsSkipped : number
 *         numRowsLoaded : number
 *         numRowsRejected : number
 *         numRowsDeleted : number
 *         numRowsCommitted : number
 *         errorMessageCode : string (optional) 
 *         WARNING: string (optional)
 *         loadLogFileSqlCodeMsg : object (optional) // search LOAD_OUTPUT in the load log file for this object:
 *                                 { SQLCODE: "text", MSESSAGE: "text"}
 *     }
 *  arg 3 : calling test name (optional)
*/
exports.validateLoadResult = function() {
    var fname = "validateLoadResult()";

    var jsonPassedIn,
        numRowsRead,
        numRowsLoaded,
        numRowsCommitted,
        numRowsDeleted,
        numRowsSkipped,
        numRowsRejected,
        errorMessageCode,
		WARNING,
        loadLogFileSqlCodeMsg;

    var callerTest = "";
    // new
    if(arguments.length == 2 || arguments.length == 3) {
        for(i = 0; i < 2; ++i) {
            if(typeof arguments[i] != "object") {
                throw new Error(fmame + ": arg " + i + " should be an onject");
            }
        }
		if(arguments.length == 3) {
				callerTest = arguments[2];
		}

        var loadStats = [
            "numRowsRead", 
            "numRowsSkipped", 
            "numRowsLoaded", 
            "numRowsRejected", 
            "numRowsDeleted", 
            "numRowsCommitted"];

        // check that everything we need has been defined
        for(i = 0; i < loadStats.length; ++i) {
            if( ! (loadStats[i] in arguments[1]) ) {
                throw new Error(fname + ": missing property " + loadStats[i] + " in arg object");
            }
        }
        numRowsRead = arguments[1].numRowsRead;
        numRowsSkipped = arguments[1].numRowsSkipped;
        numRowsLoaded = arguments[1].numRowsLoaded;
        numRowsRejected = arguments[1].numRowsRejected;
        numRowsDeleted = arguments[1].numRowsDeleted;
        numRowsCommitted = arguments[1].numRowsCommitted;

        if("errorMessageCode" in arguments[1]) {
            errorMessageCode = arguments[1].errorMessageCode;
        }

        if("WARNING" in arguments[1]) {
            WARNING = arguments[1].WARNING;
        }


        if("loadLogFileSqlCodeMsg" in arguments[1]) {
            loadLogFileSqlCodeMsg = arguments[1].loadLogFileSqlCodeMsg;
        }
    }
    // old
    else if( arguments.length == 6 || arguments.length == 7) {
        numRowsRead = arguments[1];
        numRowsLoaded = arguments[2];
        numRowsCommitted = arguments[3];
        numRowsDeleted = arguments[4];
        numRowsSkipped = arguments[5];
        if(arguments.length == 7) {
            errorMessageCode = arguments[6];
        }
    } else {
        throw new Error(fname + ": incorrect number of arhuments");
    }
    jsonPassedIn = arguments[0];
    console.log(fname + ": json received: \n" , jsonPassedIn);

    var json = null;

    // this might be for a GET /load/load
    if(jsonPassedIn.result)
    {
            if(jsonPassedIn.result.result) {
                    json = jsonPassedIn.result;
            }
            else {
                json = jsonPassedIn;
            }
    }
    else {
        throw new Error("No 'result' key in JSON object");
    }
    
    if (json.result.LOAD_LOGFILE) {
        expect(json.result.LOAD_LOGFILE).toMatch(/loadlogs\/load_[0-9].*\.txt/);
    }
    else {
        throw new Error("No result.LOAD_LOGFILE");
    }

    if (json.result.LOAD_ID) {
        expect(json.result.LOAD_ID).toBeGreaterThan(0, "LOAD_ID: " + json.result.LOAD_ID + " should be greater than 0");
    }
    else {
        throw new Error("No result.LOAD_ID");
    }


    if(json.result.START_TIME) {
        expect(json.result.START_TIME).toMatch(/........ ..:..:.. .../, "START_TIME");
    }
    else {
        throw new Error("No result.START_TIME");
    }

    if( ! json.result.LOAD_STATUS ) {
        throw new Error("No result.LOAD_STATUS");
    }

    if(errorMessageCode) { 
        expect(json.errorMessageCode).toBe(errorMessageCode, "errorMessageCode: expected " + errorMessageCode + ",got " + json.errorMessageCode);
    } else {
        expect(json.errorMessageCode).toBe("NONE", "errorMessageCode: expected \"NONE\", got " + json.errorMessageCode);
    }

	if(WARNING) {
		 expect(json.result.WARNING).toMatch(WARNING,"WARNING: expected " + WARNING + ",got " + json.result.WARNING);
	}

    if ( (json.result.LOAD_STATUS == "RUNNING") || (json.result.LOAD_STATUS == 'INIT') ){
        if (json.result.LOAD_STATUS == "RUNNING") {
            expect(json.message).toBe('LOAD started successfully.', "message for LOAD_STATUS == RUNNING: expected \"LOAD started successfully.\", got " + json.message);
        }
        else {
            expect(json.message).toBe('Load Initializing.', "message for LOAD_STATUS != RUNNING: expected \"Load Initializing.\", got " + json.message);
        }

        expect(json.result.END_TIME).toBe('', "END_TIME: expected \'\', got " + json.result.END_TIME);
        expect(json.result.ROWS_DELETED).toBe('', "ROWS_DELETED: expected \'\', got " + json.result.ROWS_DELETED);
        expect(json.result.ROWS_SKIPPED).toBe('', "ROWS_SKIPPED: expected \'\', got " + json.result.ROWS_SKIPPED);
        expect(json.result.ROWS_REJECTED).toBe('', "ROWS_REJECTED: expected \'\', got " + json.result.ROWS_REJECTED);
        expect(json.result.ROWS_READ).toBe('', "ROWS_READ: expected \'\', got " + json.result.ROWS_READ);
        expect(json.result.ROWS_LOADED).toBe('', "ROWS_LOADED: expected \'\', got " + json.result.ROWS_LOADED);
        expect(json.result.ROWS_COMMITTED).toBe('', "ROWS_COMMITTED: expected \'\', got " + json.result.ROWS_COMMITTED);
    }
    else if (json.result.LOAD_STATUS == 'COMPLETE') {
        if(json.errorMessageCode == 'NONE') {
            expect(json.message).toBe('LOAD completed successfully.', "errorMessageCode for LOAD_STATUS == COMPLETE: expected \"NONE\", got " + json.errorMessageCode);
        } 
        else if(json.errorMessageCode == 'ERROR') {
            if(json.message === "LOAD completed with warnings.") {
                console.log(fname + ": encountered 'LOAD completed with warnings.'");
                    // expect(json.result).toContainKey('WARNING', "for message === \"LOAD completed with warnings.\", expected to see WARNING key in result");
            }
            else if(json.message === "No rows of data were read") {
                console.log(fname + ": encountered 'No rows of data were read'");
                // can be 0 or "". This should be ok, cos we compare to
                // numRowsRead later
//                expect([0,""]).toInclude(json.result.ROWS_READ, "for message === \"No rows of data were read\", expected ROWS_READ to be 0 or \'\', got " + json.result.ROWS_READ);
            }
            else if(json.message === "LOAD encountered rows that were rejected") {
                console.log(fname + ": encountered 'LOAD encountered rows that were rejected'");
                expect(json.result.ROWS_REJECTED).toBeGreaterThan(0, "for message === \"LOAD encountered rows that were rejected\", expected ROWS_REJECTED to be >0, got "+ json.result.ROWS_REJECTED);
            }
            else
            {
                throw new Error("Unepected value for \"message\" :" + json.message)
            }

        }
        else {
            throw new Error("errorMessageCode was neither ERROR nor NONE");
        }

        expect(json.result.END_TIME).toMatch(/........ ..:..:.. .../, "END_TIME");

        expect(json.result.ROWS_DELETED).toBe(numRowsDeleted, "ROWS_DELETED: expected " + numRowsDeleted + ", got " + json.result.ROWS_DELETED);
        expect(json.result.ROWS_SKIPPED).toBe(numRowsSkipped, "ROWS_SKIPPED: expected " + numRowsSkipped + ", got " + json.result.ROWS_SKIPPED);
        if(numRowsRejected) {
            expect(json.result.ROWS_REJECTED).toBe(numRowsRejected, "ROWS_REJECTED: expected " + numRowsRejected + ", got " + json.result.ROWS_REJECTED);
        }
        expect(json.result.ROWS_READ).toBe(numRowsRead, "ROWS_READ: expected " + numRowsRead+ ", got " + json.result.ROWS_READ);
        expect(json.result.ROWS_LOADED).toBe(numRowsLoaded, "ROWS_LOADED: expected " + numRowsLoaded + ", got " + json.result.ROWS_LOADED);
        expect(json.result.ROWS_COMMITTED).toBe(numRowsCommitted, "ROWS_COMMITTED: expected " + numRowsCommitted + ", got " + json.result.ROWS_COMMITTED);

        // get the load log
        var logfile = json.result.LOAD_LOGFILE;
        var basename = path.basename(logfile);
        var url = server.siteURL() + server.homeEndPoint() + 'loadlogs/' + encodeURIComponent(basename);
        console.log(fname + ":  URL: " + url);
        frisby.create(callerTest + "Get load log file "+ logfile)
              .get(url)
              .auth(server.userID(), server.password())
              .timeout(30*1000)
              .expectStatus(200)
              .expectHeader('Content-Type', 'application/octet-stream; charset=utf-8')
              .after(function(err, res, body) {
//                   console.log("res",res);
                   var bodyJSON = JSON.parse(body)
                   if(loadLogFileSqlCodeMsg){
                       console.log("Checking load log for SQLCODE \"" + loadLogFileSqlCodeMsg + "\"");
                       expect(bodyJSON.result.LOAD_OUTPUT).toContain(loadLogFileSqlCodeMsg);
                   }

              })
              .toss();
    }

};

exports.toArray = function(text) {
    var records = text.split("\n");
    for (var i = 0; i < records.length; i++) {
        if (!records[i] || !records[i].trim()) {
            records.splice(i, 1);
        }
    }
    return records;
};

// get contents from all files in a zipped file, then combine and return as a string  
exports.catZipFiles = function(path) {
    var zip = new AdmZip(path);
    var zipEntries = zip.getEntries();
    var text = "";
    zipEntries.forEach(function(zipEntry) {
        text = text + zip.readAsText(zipEntry.entryName);
    });
    return text;
};

// combine content from fileList and return as a string
exports.combineFilesIntoString = function (fileList) {
    var text = "";
    fileList.forEach(function(file) {
        text = text + fs.readFileSync(file);
    });
    return text;
};

exports.download = function(uri, user, pass, filename) {
    var res = request('GET', uri, {
        'headers' : {
            'encoding' : null,
            'authorization' : 'Basic '
                    + new Buffer(user + ':' + pass, 'ascii').toString('base64')
        }
    });
    fs.writeFileSync(filename, res.getBody());
};



function get_loadid (loadid) {
    var res = request('GET', server.siteURL() + "/load/" + loadid, {
        'headers' : {
            'encoding' : null,
            'authorization' : 'Basic '
                    + new Buffer(server.userID() + ':' + server.password(), 'ascii').toString('base64')
            }
    });
    var bod = JSON.parse(res.body);
    return bod;
}



exports.wait_for_load_to_complete = function (loadid, timeout) {
    var res;
   
    var i = 0;
    // yep, I know this is totally anti-javascript, but there was no easy way to do it in a rush.
    while(i < timeout)
    {
        var bod = get_loadid(loadid);
      
        console.log("wait_for_load_to_complete(): count " + i + " status " + bod.result.result.LOAD_STATUS);
      
        if (bod && bod.result && bod.result.result 
            && bod.result.result.LOAD_STATUS 
            && bod.result.result.LOAD_STATUS=== "COMPLETE")
        {
            return true;
        }
     
        sleep.sleep(1);
       ++i;
    }
    throw new Error("timed out waiting for load to finish");
}

exports.verifyAllTableContents = function (schemaName, tableName, contents, queryIn) {
    var query;
    if(queryIn)
    {
        query = queryIn
    } else {
        // check the values in the table are the utf-converted values
        var query = "select * from " + schemaName + "." + tableName + " ORDER BY 1";
    }

    try {
        var conn = ibmdb.openSync(server.connString());

        var rows = conn.querySync(query);
                       
        conn.closeSync();
    } catch (e) {
        console.log(e.message);
        server.printErrorLine()
        process.exit(1);
    }
    expect(rows).toEqual(contents);
}

exports.get_loadid = get_loadid;
