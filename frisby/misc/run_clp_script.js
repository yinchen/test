//
//
// Run the statements in a CLP script
//
//

var fs = require('fs'),
    path = require('path'),
    util = require('util');

// Debugging
var debugEnabled = process.env.NODE_DEBUG && /\brun_clp_script\b/.test(process.env.NODE_DEBUG)
function debug() {
    if (debugEnabled) {
        console.error('RUN_CLP_SCRIPT %s', util.format.apply(util, arguments))
    }
}

// Parse command line parameters
var myname = path.basename(__filename);

function usage() {
    console.log("Usage\n  " + myname + " <clp_script> [stmt_terminator]");
    console.log("\n default statement terminator is ;")
    process.exit(1);
}

if(process.argv.length != 3 && process.argv.length != 4) // node <me> arg1 arg2
{
    usage();
}

// readFile will throw an exception if file doesn't exist
var filename = process.argv[2];


// Specify the statement terminator character
stmtEndRegexpStr = '^(.*);\s*$';
if(process.argv.length == 4) {
    stmtEndRegexpStr = '^(.*)' + process.argv[3] +'\s*$' ;
}
debug("Statement terminator regexp string = " + stmtEndRegexpStr);
stmtEndRegexp = new RegExp(stmtEndRegexpStr);
debug("Statement terminator regexp = " + stmtEndRegexp);



// Set up db connection
var ibmdb = require('ibm_db');

// Need this to get dashDB server info
var server = require('../lib//server.js'); // Internal QA Test Server Info
var connString = server.connString();



// Open a connection
ibmdb.open(connString, 
           function (err,conn) {
               if (err) {
                   console.log(err);
                   process.exit(1);
               }

               fs.readFile(filename, 'utf8', function (err, data) {
                         if (err) {
                             console.log(err);
                             process.exit(1);
                         }

                         // get lines
                         lines = data.split(/\n/);
                         var stmts = [];
                        
                         var currentStmt = "";
                         for(i = 0; i < lines.length  ; ++i) {
                             var line = lines[i];
                             debug("LINE <<<" + line + ">>>");

                             // ignore comments
                             if( /^\s*--/.test(line) ) {
                                 debug("### COMMENT");
                                 continue;
                             }
               
                             // ignore blank or empty lines
                             if( /^\s*$/.test(line) ) {
                                 debug("### blanks or empty");
                                 continue;
                             }
                             
                             // statement
                             if(matches =  line.match(stmtEndRegexp) ) {
                                 if(currentStmt == "") {
                                     debug("### one-line statement");
                                     spc = "";
                                 } else {
                                     debug("### end of statement");
                                     spc = " ";
                                 }
                                 currentStmt += spc + matches[1];
                                 stmts.push(currentStmt);

                                 // reset
                                 currentStmt = "";
                             } else {
                                 if(currentStmt == "") {
                                     debug("### start of statement");
                                 } else {
                                     debug("### middle statement");
                                 }
                                 currentStmt += line + " ";
                             }
                         }

						 // if we have something in currentStmt, then we haven't parsed properly
						 if(stmts.length == 0 && currentStmt != "") {
                             console.log("Error parsing script. Are you using the correct statement terminator?");
                             process.exit(1);
						 }
                      
                         // run each statement
                         var OK = true;
                         for(i = 0; i< stmts.length; ++i) {
                             var ret = runQuery( stmts[i],conn);
                             if(!ret) {
                                 OK = false;
                             }
                         }
                         if(!OK) {
                             console.log("ERRORS DETECTED... please check output");
                             process.exit(1);
                         }
               
               });
});

               
         
// need to have a function to run the query because I want
// the statement text to visible to the callback
//
// This is deliberately synchronous, so as to preserve the order of 
// statements in the CLP file
function runQuery(statement,conn) {

    console.log(statement)
    try {
        conn.querySync(statement)
    } catch (e) {
        console.log(e);
        return false;
    } 
    return true;
}
