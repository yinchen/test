//
// Script to upload required files to the server
//
// We use the frisby framework for this for convenience
//

var frisby = require('frisby');
var expect = require('expect');
var server = require('../lib//server.js'); // Internal QA Test Server Info
var fs = require('fs');
var path = require('path');

// Creates multipart/form-data streams
var FormData = require('form-data');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

var debug=true; // write code trace to /opt/ibm/dsserver/logs/loadrestapis.log

// URL for the /home REST API
var rScriptURL = server.siteURL() + "/rscript";
var serverhomeURL = server.homeURL();
var user = server.userID();
var pass = server.password();

// API default vars
var format='json'
var i = 0;

// List of files to be uploaded.
var files = server.loadFiles;

for (i = 0; i < files.length; i++) {
   // Set up ReadStream objects, which readies files for uploading.  fs.statSync gets the size of the file
   var filename = path.resolve(__dirname, '../datasets/' +files[i]);
   var rStream = fs.createReadStream(filename);

   var form = new FormData();
   form.append('loadFile01', rStream, { knownLength: fs.statSync(filename).size, });

   console.log("File " + i + " : " + files[i]);

   frisby.create('setup_files.js: POST files ' + files[i] + ' using /home')
         .post(serverhomeURL,
              form,
              {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
                  'content-length': form.getLengthSync()
                }
              })
         .auth(user, pass)
         .expectStatus(200)
         .timeout(5*60* 1000)
         .expectHeaderContains('Content-Type', 'application/json')
         .expectJSON(
                     {
                         "errorMessageCode": "NONE",
                         "message": "NONE",
                         "result": {
                             "filesUploaded": [
                                 files[i]
                             ]
                         },
                         "resultCode": "SUCCESS"
                     })
         .toss();
}


// empty file
var f = "empty.csv";
var filename = path.resolve(__dirname, '../datasets/' + f);
var rStream = fs.createReadStream(filename);

var form = new FormData();
form.append('loadFile01', rStream, { knownLength: 0 });

console.log("File " + i + " : " + f);
frisby.create('upload empty file')
      .post(serverhomeURL,
            form,
            {
              json: false,
              headers: {
                'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
              }
            })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(5*60* 1000)
      .expectHeaderContains('Content-Type', 'application/json')
      .expectJSON(
                  {
                      "errorMessageCode": "NONE",
                      "message": "NONE",
                      "result": {
                          "filesUploaded": [
                              f
                          ]
                      },
                      "resultCode": "SUCCESS"
                  })
      .toss();
