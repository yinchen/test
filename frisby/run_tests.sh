#!/bin/bash


# This is a hacked-together script that will run all frisby tests 
# defined in the build.xml file and pull out results
#
# Steps:
# 1. follow SETUP instructions in the README
# 2. run this script.
#
# the script will output a line for each test run and will also display the 
# total elapsed time.
#
# Output for tests (one file per script) can be found in the out/ directory


total_failed_test_scripts=0
count=1

[ -z "$DASHDB_SERVER" ] && echo "ERROR: required environment variable, DASHDB_SERVER, not set" && exit 1
[ -z "$DASHDB_USER" ]  && echo "ERROR: required environment variable, DASHDB_USER, not set" && exit 1
[ -z "$DASHDB_PASSWORD" ]  && echo "ERROR: required environment variable, DASHDB_PASSWORD, not set" && exit 1
cd $(dirname $0)
tests=$(ant -p build.xml|grep RUN|cut -c2-)

RESULTS_TXT=results.txt
OUT_FOLDER=out
REPORT_FOLDER=reportfolder

[ -e $RESULTS_TXT ] && rm -f $RESULTS_TXT

[ -e $OUT_FOLDER ] && rm -rf $OUT_FOLDER
mkdir $OUT_FOLDER

[ -e $REPORT_FOLDER ] && rm -rf $REPORT_FOLDER
mkdir $REPORT_FOLDER

echo "Start timestamp $(date)"
START=$(date +%s);
while read -r test
do
   file=$(echo $test |awk -F' ' '{print($2)}'|sed "s/\///g")
   line1=$(printf  "%2d : %-70s ... " $count "$test")
   echo -n "$line1"
   op=$( (time ant "$test" > $OUT_FOLDER/${file} 2>&1 ) 2>&1  )
   time=$(echo "$op"|grep real |awk -F' ' '{print($2)}')
   time=$(printf "%10s" $time)
   result=$(grep assertions $OUT_FOLDER/${file}|sed -e  "s/\[exec\] //g")

   # result looks like "1 test, 6 assertions, 0 failures, 0 skipped"
   # we want to split on the ", "
   result2=
   SAVEIFS=$IFS
   IFS=,

   for l in  $result
   do 
      n=$(echo $l|awk -F' ' '{print($1)}')
      thing=$(echo $l|awk -F' ' '{print($2)}')
      # They print some things in singular form. Pluralise them to standardise
      # them to make it easier to process 
      [  "$thing" = "test" ] && thing="tests"
      [  "$thing" = "failure" ] && thing="failures"
      
      if [ "$result2" = "" ]
      then
         result2="$(printf "%s : %2d" $thing $n)"
      else
         result2="$result2,  $(printf "%s : %2d" $thing $n)"
      fi
      
      # Update the total umber of failing test scripts
      if [ "$thing" = "failures" ] && [ "$n" != "0" ]
      then
         ((total_failed_test_scripts ++))
      fi
   done
   IFS=$SAVEIFS
   line2="Time $time, Result - $result2"
   echo "$line2"
   echo "$line1 $line2" >> $RESULTS_TXT

   ((count ++))
done < <(echo "$tests")

echo "End timestamp $(date)"
END=$(date +%s);
TOTAL=$(echo $((END-START)) | awk '{print int($1/60)":"int($1%60)}')
totalsline="Total elapsed time (m:s) : $TOTAL\nNumber of test scripts run : $(($count - 1))\nNumber of test scripts with failures : $total_failed_test_scripts"
echo -e $totalsline
echo -e $totalsline >> $RESULTS_TXT

if [ "$total_failed_test_scripts" != 0 ]
then
   exit 1
fi

