//////////////////////////////////////////////////////
// ENDPOINT - POST:/aspera/getUploadSizeCap 
// TEST - 1 POST /aspera/getUploadSizeCap to get current 80% free space
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var server = require('../lib/server.js');
var path = require('path');
// Ignore rejected certificates 
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);

//////////////////////////////////////////////////////////////////////
// 
var msg = myname + ': Test 1 POST /aspera/getUploadSizeCap'
//
//////////////////////////////////////////////////////////////////////
frisby.create(msg)
	  .post(siteURL + server.asperaEndPoint()+'getUploadSizeCap')
	  .auth(user, pass)
	  .expectStatus(200)
	  .expectHeader('content-type', 'application/json')
	  .expectJSON(
					{
					  message: "Upload size cap successfully retrieved",
					  result: {
						  unit: "Bytes",
						  },
					  errorMessageCode: "NONE",
					  resultCode: "SUCCESS"
					}
				  )
	  .expectJSONTypes (
						{
						message: String,
						result: {
							  unit: String,
							  value: Number
							  },
						errorMessageCode: String,
						resultCode: String
						}) 
	  .inspectHeaders()
	  .inspectBody()
	  .toss();


