//////////////////////////////////////////////////////
// ENDPOINT - GET:/home,GET:/home/file 
// TEST - 1 GET /home 
// TEST - 2 GET /home/file for file that exists
// TEST - 3 GET /home/file for file that does not exist
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var server = require('../lib/server.js');
var path = require('path');
var fs = require('fs');
var FormData = require('form-data');

// Ignore rejected certificates 
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);

//////////////////////////////////////////////////////////////////////
// 
var msg = myname + ': Test 1: GET /home, expect a list of files in home dir';
//
//////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .get(siteURL + server.homeEndPoint())
      .auth(user, pass)
      .expectStatus(200)
      .timeout(20 * 1000)  // can take a long time if there are lots of files in home dir
      .expectHeader('content-type', 'application/json')
      .expectJSON(
                  {
                      message: "NONE",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS"
                  })
      .expectJSONTypes(
                       {
                           message: String,
                           result: String,
                           errorMessageCode: String,
                           resultCode: String
                       }) 
      .inspectJSON()
      .toss();



//////////////////////////////////////////////////////////////////////
// 
msg = myname + ': Test 2: GET /home/{file}';
//
//////////////////////////////////////////////////////////////////////
//frisby.create(msg)
//      .get(siteURL + server.homeEndPoint() + 'RSamples/militaryByGender.R')
//      .auth(user, pass)
//      .expectStatus(200)
//      .expectHeader('Content-Type', 'application/octet-stream')
//      .expectHeader('content-disposition', 'attachment; filename=militaryByGender.R')
//      .toss();

//////////////////////////////////////////////////////////////////////
// 
msg = myname + ': Test 3: GET /home/{file} that does not exist.';
//
//////////////////////////////////////////////////////////////////////
//frisby.create(msg)
//      .get(siteURL + server.homeEndPoint() + 'RSamples/blah.R')
//      .auth(user, pass)
//      .inspectJSON()
//      .expectStatus(400)
//      .expectHeader('Content-Type', 'application/json')
//      .expectJSON(
//                  {
//                      message: "/ldaphome/bluadmin/RSamples/blah.R File path is invalid. File path does not exist.",
//                      resultCode: "ERROR",
//                      errorMessageCode: "ERROR",
//                      result:[]
//                  })
//      .expectJSONTypes(
//                       {
//                           message: String,
//                           errorMessageCode: String,
//                           resultCode: String
//                       })
//      .toss();


//////////////////////////////////////////////////////
// 
// ENDPOINT - POST /home/{file or directory}
// ENDPOINT - DELETE /home/{file or directory}

//////////////////////////////////////////////////////////////////////
// 
msg = myname + ': Test 4: POST /home - upload file with non-ascii chars in file name (happy path)';
//
//////////////////////////////////////////////////////////////////////
var filename = path.resolve(__dirname, '../datasets/ドラゴンボール');
var rStream = fs.createReadStream(filename);

var form = new FormData();
form.append('loadFile01', rStream, { knownLength: fs.statSync(filename).size, });

frisby.create(msg)
      .post(siteURL + server.homeEndPoint(),
      form,
              {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
                  'content-length': form.getLengthSync()
                }
              })
      .auth(user, pass)      
      .expectStatus(200)
      .timeout(5*60* 1000)
      .expectHeaderContains('Content-Type', 'application/json')
      .inspectJSON()
      .expectJSON(
                     {
                         "errorMessageCode": "NONE",
                         "message": "NONE",
                         "result": {
                             "filesUploaded": [
                                 "ドラゴンボール"
                             ]
                         },
                         "resultCode": "SUCCESS"
                     })
      .toss();

//////////////////////////////////////////////////////////////////////
// 
msg = myname + ': Test 5: DELETE /home - delete file with non-ascii chars in file name (happy path)';
//
//////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .delete(siteURL + server.homeEndPoint() + encodeURIComponent('ドラゴンボール'))
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'application/json')
      .inspectJSON()
      .expectJSON(
                     {
                         "errorMessageCode": "NONE",
                         "message": "NONE",
                         "result": [],
                         "resultCode": "SUCCESS"
                     })               
      .toss();

//////////////////////////////////////////////////////////////////////
// 
msg = myname + ': Test 6: POST /home - upload file (happy path)';
//
//////////////////////////////////////////////////////////////////////
var filename = path.resolve(__dirname, '../datasets/newfile');
var rStream = fs.createReadStream(filename);

var form = new FormData();
form.append('loadFile01', rStream, { knownLength: fs.statSync(filename).size, });

frisby.create(msg)
      .post(siteURL + server.homeEndPoint(),
      form,
              {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
                  'content-length': form.getLengthSync()
                }
              })
      .auth(user, pass)      
      .expectStatus(200)
      .timeout(5*60* 1000)
      .expectHeaderContains('Content-Type', 'application/json')
      .inspectJSON()
      .expectJSON(
                     {
                         "errorMessageCode": "NONE",
                         "message": "NONE",
                         "result": {
                             "filesUploaded": [
                                 "newfile"
                             ]
                         },
                         "resultCode": "SUCCESS"
                     })
      .toss();

//////////////////////////////////////////////////////////////////////
// 
msg = myname + ': Test 7: DELETE /home - delete file (happy path)';
//
//////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .delete(siteURL + server.homeEndPoint() + 'newfile')
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'application/json')
      .inspectJSON()
      .expectJSON(
                     {
                         "errorMessageCode": "NONE",
                         "message": "NONE",
                         "result": [],
                         "resultCode": "SUCCESS"
                     })               
      .toss();


//////////////////////////////////////////////////////////////////////
// 
msg = myname + ': Test 8: POST /home/リンス - upload file with non-ascii chars in file name, inside a parent directory リンス (happy path)';
//
//////////////////////////////////////////////////////////////////////
var filename = path.resolve(__dirname, '../datasets/ドラゴンボール');
var rStream = fs.createReadStream(filename);

var form = new FormData();
form.append('loadFile01', rStream, { knownLength: fs.statSync(filename).size, });

frisby.create(msg)
      .post(siteURL + server.homeEndPoint() + '/' + encodeURIComponent('リンス'),
      form,
              {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
                  'content-length': form.getLengthSync()
                }
              })
      .auth(user, pass)      
      .expectStatus(200)
      .timeout(5*60* 1000)
      .expectHeaderContains('Content-Type', 'application/json')
      .inspectJSON()
      .expectJSON(
                     {
                         "errorMessageCode": "NONE",
                         "message": "NONE",
                         "result": {
                             "filesUploaded": [
                                 "リンス/ドラゴンボール"
                             ]
                         },
                         "resultCode": "SUCCESS"
                     })
      .toss();

//////////////////////////////////////////////////////////////////////
// 
msg = myname + ': Test 9: GET /home/リンス - list files directory リンス (happy path). This test depends on Test 7.';
//
//////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .get(siteURL + server.homeEndPoint() + '/' + encodeURIComponent('リンス'))
      .auth(user, pass)      
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'application/json')
      .inspectJSON()
      .expectJSON(
                     {
                         "errorMessageCode": "NONE",
                         "message": "NONE",
                         "result": './ドラゴンボール\n',
                         "resultCode": "SUCCESS"
                     })
      .toss();

//////////////////////////////////////////////////////////////////////
// 
msg = myname + ': Test 10: DELETE /home/リンス - delete directory リンス (negative test. directory is not empty). This test depends on Test 7.';
//
//////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .delete(siteURL + server.homeEndPoint() + '/' + encodeURIComponent('リンス'))
      .auth(user, pass)      
      .expectStatus(400)
      .expectHeaderContains('Content-Type', 'application/json')
      .inspectJSON()
      .expectJSON(
                     {
                         "errorMessageCode": "ERROR",
                         "result": [],
                         "resultCode": "ERROR"
                     })
      .afterJSON(function (json) {
          expect(json.message).toMatch(/Failed to delete file/);
      })                     
      .toss();

//////////////////////////////////////////////////////////////////////
// 
msg = myname + ': Test 11: DELETE /home/リンス - delete directory リンス (happy). It requires to delete the files in the directory first. This test depends on Test 7.';
//
//////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .delete(siteURL + server.homeEndPoint() + '/' + encodeURIComponent('リンス') + '/' + encodeURIComponent('ドラゴンボール')) // first we need to delete the files inside
      .auth(user, pass)      
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'application/json')
      .inspectJSON()
      .expectJSON(
                     {
                         "errorMessageCode": "NONE",
                         "result": [],
                         "resultCode": "SUCCESS"
                     })
      .afterJSON(function (json) {
          frisby.create(msg)
                .delete(siteURL + server.homeEndPoint() + '/' + encodeURIComponent('リンス')) // delete the directory
                .auth(user, pass)      
                .expectStatus(200)
                .expectHeaderContains('Content-Type', 'application/json')
                .inspectJSON()
                .expectJSON(
                     {
                         "message": "NONE",
                         "errorMessageCode": "NONE",
                         "result": [],
                         "resultCode": "SUCCESS"
                     })
                .toss();
      })                     
      .toss();          


//////ascii versions of test 8 - 11

//////////////////////////////////////////////////////////////////////
// 
msg = myname + ': Test 12: POST /home/newdir - upload file with non-ascii chars in file name, inside a parent directory newdir (happy path)';
//
//////////////////////////////////////////////////////////////////////
var filename = path.resolve(__dirname, '../datasets/newfile');
var rStream = fs.createReadStream(filename);

var form = new FormData();
form.append('loadFile01', rStream, { knownLength: fs.statSync(filename).size, });

frisby.create(msg)
      .post(siteURL + server.homeEndPoint() + '/newdir',
      form,
              {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
                  'content-length': form.getLengthSync()
                }
              })
      .auth(user, pass)      
      .expectStatus(200)
      .timeout(5*60* 1000)
      .expectHeaderContains('Content-Type', 'application/json')
      .inspectJSON()
      .expectJSON(
                     {
                         "errorMessageCode": "NONE",
                         "message": "NONE",
                         "result": {
                             "filesUploaded": [
                                 "newdir/newfile"
                             ]
                         },
                         "resultCode": "SUCCESS"
                     })
      .toss();



//////////////////////////////////////////////////////////////////////
// 
msg = myname + ': Test 13: GET /home/newdir - list files directory newdir (happy path). This test depends on Test 12.';
//
//////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .get(siteURL + server.homeEndPoint() + '/newdir')
      .auth(user, pass)      
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'application/json')
      .inspectJSON()
      .expectJSON(
                     {
                         "errorMessageCode": "NONE",
                         "message": "NONE",
                         "result": './newfile\n',
                         "resultCode": "SUCCESS"
                     })
      .toss();



//////////////////////////////////////////////////////////////////////
// 
msg = myname + ': Test 14: DELETE /home/newdir - delete directory newdir (negative test. directory is not empty). This test depends on Test 12.';
//
//////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .delete(siteURL + server.homeEndPoint() + '/newdir')
      .auth(user, pass)      
      .expectStatus(400)
      .expectHeaderContains('Content-Type', 'application/json')
      .inspectJSON()
      .expectJSON(
                     {
                         "errorMessageCode": "ERROR",
                         "result": [],
                         "resultCode": "ERROR"
                     })
      .afterJSON(function (json) {
          expect(json.message).toMatch(/Failed to delete file/);
      })                     
      .toss();

//////////////////////////////////////////////////////////////////////
// 
msg = myname + ': Test 15: DELETE /home/newdir - delete directory newdir (happy). This test depends on Test 12.';
//
//////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .delete(siteURL + server.homeEndPoint() + '/newdir/newfile') //delete the file first
      .auth(user, pass)      
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'application/json')
      .inspectJSON()
      .expectJSON(
                     {
                         "errorMessageCode": "NONE",
                         "result": [],
                         "resultCode": "SUCCESS"
                     })
      .afterJSON(function (json) {
          frisby.create(msg)
                .delete(siteURL + server.homeEndPoint() + '/newdir')
                .auth(user, pass)      
                .expectStatus(200)
                .expectHeaderContains('Content-Type', 'application/json')
                .inspectJSON()
                .expectJSON(
                     {
                         "errorMessageCode": "NONE",
                         "result": [],
                         "resultCode": "SUCCESS"
                     })
                               
                .toss();
      })                     
      .toss();                
