//////////////////////////////////////////////////////
// ENDPOINT - GET:/load/id,GET:/load/table
// TEST - various negative tests for GET /load/
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var server = require('../lib/server.js');
var path = require('path');


// Ignore rejected certificates 
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var user = server.userID();
var pass = server.password();
var tableSchema = server.tabSchema();
var myname = path.basename(__filename);


//Global setup for all tests
frisby.globalSetup ({
    timeout: (30 * 1000)
})



///////////////////////////////////////////////////////////////////
//
var msg = myname + " : Test 1: GET /load/LoadID - larget number test.";
//
//////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .get(siteURL + '/load/' + 394587342987342598579257932457)
      .auth(user, pass)
      .expectHeaderContains('Content-Type', 'json')
      .expectStatus(400)
      .expectJSON(
                  {
                      message: 'Table 3.945873429873426e+29 does not exist.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .inspectJSON()
      .toss();


//////////////////////////////////////////////////////////////////
//
msg = myname + " : Test 2: GET /load/LoadID - negative integer test.";
//
//////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .get(siteURL + '/load/' + -12345)
      .auth(user, pass)
      .expectHeaderContains('Content-Type', 'json')
      .expectStatus(400)
      .expectJSON(
                  {
                      message: 'The specified Load ID "-12345" is not valid.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .inspectJSON()
      .toss();
    
  
///////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 3: GET /load/LoadID - floating point number test.';
//
//////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .get(siteURL + '/load/' + 12456.67890)
      .auth(user, pass)
      .expectHeaderContains('Content-Type', 'json')
      .expectStatus(400)
      .expectJSON(
                  {
                      message: 'Table 12456.6789 does not exist.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .inspectJSON()
      .toss();


///////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 4: GET /load/LoadID - empty id/table.';
//
//////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .get(siteURL + '/load/')
      .auth(user, pass)
      .expectHeaderContains('Content-Type', 'json')
      .expectStatus(400)
      .expectJSON(
                  {
                      message: 'No loadID or tableName. Provide either a loadID or tableName for a list of active and complete loads',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .inspectJSON()
      .toss();


///////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 5: GET /load/LoadID - non-existent table.';
//
//////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .get(siteURL + '/load/'+ tableSchema + '.NONEXISTENT') 
      .auth(user, pass)
      .expectHeaderContains('Content-Type', 'json')
      .expectStatus(400)
      .expectJSON(
                  {
                      message: 'Table ' + tableSchema +'.NONEXISTENT does not exist.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .inspectJSON()
      .toss();



///////////////////////////////////////////////////////////////////
// 
msg = myname + ' : Test 6: GET /load/LoadID for a non-existent loadID';
//
//////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .get(siteURL + '/load/12')
      .auth(user, pass)
      .expectHeaderContains('Content-Type', 'json')
      .expectStatus(400)
      .expectJSON(
                  {
                      message: 'No load exists for provided load ID.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .inspectJSON()
      .toss();
  

var invalidChars = ["*","!",'"'];
var testNum = 10;

for (i=0; i< invalidChars.length;++i)
{
	ch = invalidChars[i];
    ///////////////////////////////////////////////////////////////////
    //
    msg = myname + ' : Test ' + testNum + ': GET /load/{tableName} with ' + ch + ' at start of table name';
    //
    //////////////////////////////////////////////////////////////////////
    tabName = ch + 'tab';
    frisby.create(msg)
          .get(siteURL + '/load/' + tabName)
          .auth(user, pass)
          .expectHeaderContains('Content-Type', 'json')
          .expectStatus(400)
          .expectJSON(
                      {
                          message: 'Ordinary (unquoted) identifiers can only contain letters, digits and the underscore character and must start with a letter. Identifier '+ tabName +' should be quoted',
                          errorMessageCode: "ERROR",
                          resultCode: "ERROR",
                          result: []
                      })
          .inspectJSON()
          .toss();

    ++testNum;


    ///////////////////////////////////////////////////////////////////
    //
    msg = myname + ' : Test ' + testNum + ': GET /load/{tableName} with ' + ch + ' at end of table name';
    //
    //////////////////////////////////////////////////////////////////////
    tabName = 'tab' + ch;
    frisby.create(msg)
          .get(siteURL + '/load/' + tabName)
          .auth(user, pass)
          .expectHeaderContains('Content-Type', 'json')
          .expectStatus(400)
          .expectJSON(
                      {
                          message: 'Ordinary (unquoted) identifiers can only contain letters, digits and the underscore character and must start with a letter. Identifier '+ tabName +' should be quoted',
                          errorMessageCode: "ERROR",
                          resultCode: "ERROR",
                          result: []
                      })
          .inspectJSON()
          .toss();

    ++testNum;


    ///////////////////////////////////////////////////////////////////
    //
    msg = myname + ' : Test ' + testNum + ': GET /load/{tableName} with ' + ch + ' in the middle of table name';
    //
    //////////////////////////////////////////////////////////////////////
    tabName = 'tab' + ch + 'tab';
    frisby.create(msg)
          .get(siteURL + '/load/' + tabName)
          .auth(user, pass)
          .expectHeaderContains('Content-Type', 'json')
          .expectStatus(400)
          .expectJSON(
                      {
                          message: 'Ordinary (unquoted) identifiers can only contain letters, digits and the underscore character and must start with a letter. Identifier '+ tabName +' should be quoted',
                          errorMessageCode: "ERROR",
                          resultCode: "ERROR",
                          result: []
                      })
          .inspectJSON()
          .toss();
    
    ++testNum;


    ///////////////////////////////////////////////////////////////////
    //
    msg = myname + ' : Test ' + testNum + ': GET /load/{tableName} with ' + ch + ' in the middle of table name';
    //
    //////////////////////////////////////////////////////////////////////
    tabName = 'tab' + ch + 'tab';
    frisby.create(msg)
          .get(siteURL + '/load/' + tabName)
          .auth(user, pass)
          .expectHeaderContains('Content-Type', 'json')
          .expectStatus(400)
          .expectJSON(
                      {
                          message: 'Ordinary (unquoted) identifiers can only contain letters, digits and the underscore character and must start with a letter. Identifier '+ tabName +' should be quoted',
                          errorMessageCode: "ERROR",
                          resultCode: "ERROR",
                          result: []
                      })
          .inspectJSON()
          .toss();
    
    ++testNum;



    ///////////////////////////////////////////////////////////////////
    //
    msg = myname + ' : Test ' + testNum + ': GET /load/{tableName} with ' + ch + ' as table name';
    //
    //////////////////////////////////////////////////////////////////////
    tabName = ch ;
    frisby.create(msg)
          .get(siteURL + '/load/' + tabName)
          .auth(user, pass)
          .expectHeaderContains('Content-Type', 'json')
          .expectStatus(400)
          .expectJSON(
                      {
                          message: 'Ordinary (unquoted) identifiers can only contain letters, digits and the underscore character and must start with a letter. Identifier '+ tabName +' should be quoted',
                          errorMessageCode: "ERROR",
                          resultCode: "ERROR",
                          result: []
                      })
          .inspectJSON()
          .toss();
    
    ++testNum;
}
