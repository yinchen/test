//////////////////////////////////////////////////////
// ENDPOINT - POST:/load/local/del
// TEST - 1 POST /load/local/del with large file to bigtable
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var expect = require('expect');
var server = require('../lib/server.js');
var fs = require('fs');
var path = require('path');
var FormData = require('form-data');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadEndPoint = server.loadLocalEndPoint();
var tableName = "BIGTABLE";
var tableSchema = server.tabSchema();
var user = server.userID();
var pass = server.password();

var nodeType = server.nodeType(); // SMP or MPP

if(nodeType == "MPP") {
    console.log("Skipping all tests as this tests usually results in OOM on MPP machines");
	server.printZeroTestsLine();
	process.exit(0);
}


// API default vars
var format = 'json'

//Global setup for all tests
frisby.globalSetup({
    timeout: (15 * 60 * 1000)
})


//////////////////////////////////////////////////////////////////////////
// TEST 1: Load into bigtable using bigtable.csv
var fileName="bigtable.csv"
var smallLoadInput = path.resolve(__dirname, '../datasets/bigtable.csv')
var rStream = fs.createReadStream(smallLoadInput)
var form = new FormData();
form.append('loadFile0', rStream,
  {
    knownLength: fs.statSync(smallLoadInput).size,
  });

var numRowsRead = 5000;
var numRowsLoaded = 5000;
var numRowsCommitted = 5000;
var numRowsRejected = 0;
var numRowsSkipped = 0;
var numRowsDeleted = 0;

frisby.create('load_bigtable_spec.js: Test 1:  Load BIGTABLE... POST /load/local/del/' + fileName)
  .post(siteURL + loadEndPoint + tableSchema + '.' + tableName + '?debug=true&hasHeaderRow=true&waitForLoadComplete=true&loadAction=INSERT&timestampFormat=\"YYYY-MM-DD%20HH:MM:SS.U\"',
    form,
    {
      json: false,
      headers: {
        'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
        'content-length': form.getLengthSync()
      }
    })
  .timeout(15 * 60 * 1000) // 15 minutes.
  .auth(user, pass)
  .expectStatus(200)
  .expectHeaderContains('Content-Type', 'json')
  .expectJSON(
    {
      errorMessageCode: "NONE",
      resultCode: "SUCCESS",
      result: {
        TABLE: tableName,
        SCHEMA: tableSchema
      }
    })
  .afterJSON(function (json) {
    loadUtils.validateLoadResult(json, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsDeleted, numRowsRejected);
  })
  .toss();
