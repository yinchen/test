var frisby = require('frisby');
var expect = require('expect');
var server = require('../lib/server.js');
var fs = require('fs');
var path = require('path');
var FormData = require('form-data');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadEndPoint = server.loadLocalEndPoint();
var tableName = server.tabName();
var tabSchema = server.tabSchema();
var user = server.userID();
var pass = server.password();


// API default vars
var format = 'json'

//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
})

///////////////////////////////////////////////////////////////////////////////////////
// Test 1: Load a basic XLS file.
var smallLoadInput = path.resolve(__dirname, '../datasets/order_details_header_small.xlsx')
var fileName="order_details_header_small.xlsx"
var rStream = fs.createReadStream(smallLoadInput)
var form = new FormData();
form.append('loadFile0', rStream,
  {
    knownLength: fs.statSync(smallLoadInput).size,
  });

var numRowsRead = 1000;
var numRowsLoaded = 1000;
var numRowsCommitted = 1000;
var numRowsRejected = 0;
var numRowsSkipped = 0;
var numRowsDeleted = 0;

frisby.create('load_local_xls_spec: Test 1: POST /load/local/del/' + fileName)
  .post(siteURL + loadEndPoint + tabSchema + '.' + tableName + '?debug=true&hasHeaderRow=true&waitForLoadComplete=true&loadAction=INSERT&timestampFormat=\"YYYY-MM-DD%20HH:MM:SS.U\"',
    form,
    {
      json: false,
      headers: {
        'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
        'content-length': form.getLengthSync()
      }
    })
  .timeout(300 * 1000) // 5 minutes.
  .auth(user, pass)
  .expectStatus(200)
  .expectHeaderContains('Content-Type', 'json')
//  .inspectJSON()
  .expectJSON(
    {
      errorMessageCode: "NONE",
      resultCode: "SUCCESS",
      result: {
        TABLE: server.tabName,
        SCHEMA: server.tabSchema
      }
    })
  .afterJSON(function (json) {
        loadUtils.validateLoadResult(json, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsDeleted, numRowsRejected);
  })
  .toss();

