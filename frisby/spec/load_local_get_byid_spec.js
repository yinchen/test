//////////////////////////////////////////////////////
// ENDPOINT - POST:/load/local/del,GET:/load/id
// TEST - 1 POST /load/local/del with small file followed by a GET /load/id with the correct ID
// COMMENT - doesn't wait for load to cmplete, so leaves load running
// NEEDS_REWORK - Y
// NEEDS_REWORK_COMMENT - not worth implementing. Just leave at end of test list

var frisby = require('frisby');
var server = require('../lib/server.js')
var fs = require('fs');
var path = require('path');
var FormData = require('form-data');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadEndPoint = server.loadLocalEndPoint();
var tabSchema = server.tabSchema();
var tabName = server.tabName();
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);


//Global setup for all tests
frisby.globalSetup({
        timeout: (30 * 1000)
})



//////////////////////////////////////////////////////////////////////
// 
var msg = myname + ': Test 1: Post load followed by GET by ID.p';
//
//////////////////////////////////////////////////////////////////////
var numRows = 49;  // number of rows in small.csv file.

// Create the form for the load file.
var form = new FormData();
var basicLoadInput = path.resolve(__dirname, '../datasets/small.csv')
var rStream = fs.createReadStream(basicLoadInput)
form.append('loadFile0', rStream,
            {
                knownLength: fs.statSync(basicLoadInput).size,
            });

// Start the test
frisby.create(msg)
      .post(siteURL + loadEndPoint + tabSchema + '.' + tabName + '?debug=true&hasHeaderRow="true"&waitForLoadComplete=true&loadAction="REPLACE"&timestampFormat="YYYY-MM-DD%20HH:MM:SS.U"&hasDateTypes="true"',
            form,
            {
                json: false,
                headers: {
                    'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
                    'content-length': form.getLengthSync()
                }
            })
      .auth(user, pass)
      .timeout(30000)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .expectJSON(
                  {
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: tabName,
                          SCHEMA: tabSchema
                      }
                  })
      .inspectJSON()
      .afterJSON(function (json) {
                     frisby.create(myname + ': Test 2: GET /load/LoadID')
                           .get(siteURL + '/load/' + json.result.LOAD_ID)
                           .auth(user, pass)
                           .expectHeaderContains('Content-Type', 'json')
                           .expectJSONTypes(
								            {
                                                errorMessageCode: String, // test is failing due to unknown reason.
                                                result: Object,
                                                resultCode: String
                                            })
                           .expectJSON(
                                       {
                                           message: "LOAD retrieved successfully.",
                                           errorMessageCode: "NONE",
                                           result: {
                                               errorMessageCode: "NONE",
                                               result: {
                                                   LOAD_ID: json.result.LOAD_ID,
                                                   TABLE: json.result.TABLE,
                                                   SCHEMA: json.result.SCHEMA,
                                                   START_TIME: json.result.START_TIME,
                                                   LOAD_LOGFILE: json.result.LOAD_LOGFILE
                                               },
                                               resultCode: 'SUCCESS'
                                           },
                                           resultCode: "SUCCESS"
                                       })
                           .afterJSON(function(json) {
                                          console.log(json);
                                      })
                           .toss()
                 })
      .toss();

