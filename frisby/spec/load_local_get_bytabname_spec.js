//////////////////////////////////////////////////////
// ENDPOINT - POST:/load/local/del,GET:/load/table
// TEST - POST /load/local/del followed by GET /load/table - second doesn't check anything
// COMMENT - the second test doesn't check anything, however, they use afterJSON
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - not worth the time

var frisby = require('frisby');
var server = require('../lib/server.js')
var fs = require('fs');
var path = require('path');
var FormData = require('form-data');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadEndPoint = server.loadLocalEndPoint();
var tabSchema = server.tabSchema();
var tabName = server.tabName();
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);


// API default vars
var format='json'

//Global setup for all tests
frisby.globalSetup(
				   {
                       timeout: (60 * 1000)
                   })


//////////////////////////////////////////////////////////////////////
// 
var msg = myname + ': Test 1: load data using POST /load/local/del';
//
//////////////////////////////////////////////////////////////////////
var form = new FormData();
var basicLoadInput = path.resolve(__dirname, '../datasets/small.csv')
var rStream = fs.createReadStream(basicLoadInput)

form.append('loadFile0', rStream,
            {
                knownLength: fs.statSync(basicLoadInput).size,
            });

var numRows = 49;  // number of rows in small.csv file.

frisby.create(msg)
      .post(siteURL + loadEndPoint + tabSchema + '.' + tabName + '?debug=true&hasHeaderRow=true&waitForLoadComplete=true&loadAction=INSERT&timestampFormat=\"YYYY-MM-DD%20HH:MM:SS.U\"',
            form,
            {
                json: false,
                headers: {
                    'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
                    'content-length': form.getLengthSync()
                }
            })
      .auth(user, pass)
      .timeout(30000)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .expectJSON(
                  {
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: tabName,
                          SCHEMA: tabSchema
                      }
                  })
      .afterJSON(function(json) {
                     loadUtils.validateLoadResult(json, numRows, numRows, numRows, 0, 0);
                  })
      .afterJSON(function(json) {
//////////////////////////////////////////////////////////////////////
// 
msg = myname + ': Test 2: fetcj results using GET /load/{tableName}';
//
//////////////////////////////////////////////////////////////////////
                     frisby.create(msg)
                           .get(siteURL + '/load/' +  tabSchema + '.' + tabName)
                           .auth(user,pass)
                           .expectHeaderContains('Content-Type', 'json')
                           .expectJSON(
                                       {
                                           message: 'LOAD retrieved successfully.',
                                           result: [],
                                           errorMessageCode: "NONE",
                                           resultCode: "SUCCESS"
                                       })
                           .afterJSON(function(json) {
                                          //loadUtils.validateLoadResultJson(json.result, numRows, numRows, numRows, 0, 0);
                                          // We need a different validate function in order to validate the array of results returned.
                                      })
                           .toss()
                })
      .toss();
