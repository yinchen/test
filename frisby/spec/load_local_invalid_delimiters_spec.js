var frisby = require('frisby');
var expect = require('expect');
var server = require('../lib/server.js');
var fs = require('fs');
var path = require('path');
var FormData = require('form-data');
var loadUtils = require('../lib/loadutils.js');



// Ignore rejected certificates 
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadEndPoint = server.loadLocalEndPoint();
var tableName = server.tabName();
var user = server.userID();
var pass = server.password();
var tinyTable = server.loadTinyTable();

// The file to load.
var tinyPipeFile = path.resolve(__dirname, '../datasets/tiny_table_coldel_pipe.csv');
var delimiter = "|";
var numRowsTiny = 11; // number of expected rows loaded from this file.

// API default vars
var format = 'json'

//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
});


var form = new FormData();

var rStream = fs.createReadStream(tinyPipeFile)

form.append('loadFile0', rStream,
  {
    knownLength: fs.statSync(tinyPipeFile).size,
  });



//////////////////////////////////////////////////////////////////////////////////////////////////////////
// Valid Delimiter test (|)
frisby.create('load_local_invalid_delimiters_spec.js: Test 1: load_local_invalid_delimiters (Valid delimiter |, 0x7c)')
    .post(siteURL + loadEndPoint + tinyTable + '?debug=true&delimiter=0x7c&hasHeaderRow=true&waitForLoadComplete=true&loadAction=INSERT&timestampFormat=\"YYYY-MM-DD%20HH:MM:SS.U\"',
        form,
        {
            json: false,
            headers: {
                'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
                'content-length': form.getLengthSync(),
            }
        })
    .auth(user, pass)
    .expectStatus(200)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
       { message: 'LOAD completed successfully.',
           result: 
            { 
              ROWS_COMMITTED: numRowsTiny,
              TABLE: server.tinyTabName,
              SCHEMA: server.tabSchema,
              ROWS_LOADED: numRowsTiny,
              ROWS_REJECTED: 0,
              ROWS_DELETED: 0,
              LOAD_STATUS: 'COMPLETE',
              ROWS_SKIPPED: 0,
              ROWS_READ: numRowsTiny
           },
           errorMessageCode: 'NONE',
           resultCode: 'SUCCESS'
        })
    .toss();


//////////////////////////////////////////////////////////////////////////////////////////////////////////
// Invalid Delimiter Tests: 0x0A, 0x0D, 0x2E

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// 0x0A - line feed
form = new FormData();

rStream = fs.createReadStream(tinyPipeFile)

form.append('loadFile0', rStream,
  {
    knownLength: fs.statSync(tinyPipeFile).size,
  });

frisby.create('load_local_invalid_delimiters_spec.js: Test 2: load_local_invalid_delimiters (delimiter 0x0A - line feed)')
  .post(siteURL + loadEndPoint + tinyTable + '?debug=true&delimiter=\"0x0A\"&hasHeaderRow=false&waitForLoadComplete=true&loadAction=INSERT&timestampFormat=\"YYYY-MM-DD%20HH:MM:SS.U\"',
    form,
    {
      json: false,
      headers: {
        'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
        'content-length': form.getLengthSync()
      }
    })
  .auth(user, pass)
  .expectStatus(400)
  .timeout(30000)
  .expectHeaderContains('Content-Type', 'json')
  .expectJSON(
       { message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
           errorMessageCode: 'ERROR',
           resultCode: 'ERROR',
           result: []
    })
    .afterJSON(function (json) {
        console.log(json);   
    })
  .toss();
  


//////////////////////////////////////////////////////////////////////////////////////////////////////////
// 0x0D  
form = new FormData();

rStream = fs.createReadStream(tinyPipeFile)

form.append('loadFile0', rStream,
{
   knownLength: fs.statSync(tinyPipeFile).size,
});


frisby.create('load_local_invalid_delimiters_spec.js: Test 3: load_local_invalid_delimiters (delimiter 0x0D - carriage return)')
  .post(siteURL + loadEndPoint + tinyTable + '?debug=true&delimiter=0x0D&hasHeaderRow=false&waitForLoadComplete=true&loadAction=INSERT&timestampFormat=\"YYYY-MM-DD%20HH:MM:SS.U\"',
    form,
    {
      json: false,
      headers: {
        'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
        'content-length': form.getLengthSync()
      }
    })
  .auth(user, pass)
  .timeout(30000)
  .expectStatus(400)
  .expectHeaderContains('Content-Type', 'json')
  .expectJSON(
       { message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
           errorMessageCode: 'ERROR',
           resultCode: 'ERROR',
           result: []
    })
  .toss();

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// 0x20 - space
form = new FormData();

rStream = fs.createReadStream(tinyPipeFile)

form.append('loadFile0', rStream,
{
   knownLength: fs.statSync(tinyPipeFile).size,
});


frisby.create('load_local_invalid_delimiters_spec.js: Test 4: load_local_invalid_delimiters (delimiter 0x20 - space)')
  .post(siteURL + loadEndPoint + tinyTable + '?debug=true&delimiter=0x20&hasHeaderRow=false&waitForLoadComplete=true&loadAction=INSERT&timestampFormat=\"YYYY-MM-DD%20HH:MM:SS.U\"',
    form,
    {
      json: false,
      headers: {
        'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
        'content-length': form.getLengthSync()
      }
    })
  .auth(user, pass)
  .timeout(30000)
  .expectStatus(400)
  .expectHeaderContains('Content-Type', 'json')
  .expectJSON(
       { message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
           errorMessageCode: 'ERROR',
           resultCode: 'ERROR',
           result: []
    })
  .toss();


//////////////////////////////////////////////////////////////////////////////////////////////////////////
// 0x2E - decimal
form = new FormData();

rStream = fs.createReadStream(tinyPipeFile)

form.append('loadFile0', rStream,
{
   knownLength: fs.statSync(tinyPipeFile).size,
});


frisby.create('load_local_invalid_delimiters_spec.js: Test 5: load_local_invalid_delimiters (delimiter 0x2E - decimal)')
  .post(siteURL + loadEndPoint + tinyTable + '?debug=true&delimiter=0x2E&hasHeaderRow=false&waitForLoadComplete=true&loadAction=INSERT&timestampFormat=\"YYYY-MM-DD%20HH:MM:SS.U\"',
    form,
    {
      json: false,
      headers: {
        'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
        'content-length': form.getLengthSync()
      }
    })
  .auth(user, pass)
  .timeout(30000)
  .expectStatus(400)
  .expectHeaderContains('Content-Type', 'json')
  .expectJSON(
       { message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
           errorMessageCode: 'ERROR',
           resultCode: 'ERROR',
           result: []
    })
  .toss();
