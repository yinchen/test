var frisby = require('frisby');
var expect = require('expect');
var server = require('../lib/server.js');
var fs = require('fs');
var path = require('path');
var FormData = require('form-data');

// Ignore rejected certificates 
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadEndPoint = server.loadLocalEndPoint();
var tableName = server.tabName();
var user = server.userID();
var pass = server.password();

var myname = path.basename(__filename);

var smallLoadInput = path.resolve(__dirname, '../datasets/small.csv')


//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
});




//////////////////////////////////////////////////////////////////////////////////////
// TEST 1: Invalid loadAction
// The file to load.
var rStream = fs.createReadStream(smallLoadInput)
var form = new FormData();
form.append('loadFile0', rStream,
  {
    knownLength: fs.statSync(smallLoadInput).size,
  });

var msg = myname + ': Test 1: invalid value for loadAction';
frisby.create(msg)
  .post(siteURL + loadEndPoint + tableName + '?debug=true&loadAction="INVALID"',
    form,
    {
      json: false,
      headers: {
        'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
        'content-length': form.getLengthSync()
      }
    })
  .auth(user, pass)
  .expectStatus(400)
  .expectHeaderContains('Content-Type', 'json')
  .expectJSON(
       { message: 'Invalid loadAction \'INVALID\'.',
           errorMessageCode: 'ERROR',
           resultCode: 'ERROR',
           result: []
    })
  .toss();
  



///////////////////////////////////////////////////////////////////////////////
// Test 2: Invalid Content-Type
rStream = fs.createReadStream(smallLoadInput)
form = new FormData();
form.append('loadFile0', rStream,
  {
    knownLength: fs.statSync(smallLoadInput).size,
  });

msg = myname + ': Test 2: invalid content-type in header';
frisby.create(msg)
  .post(siteURL + loadEndPoint + tableName + '?debug=true',
    form,
    {
      json: false,
      headers: {
        'content-type': 'application/octet-stream; boundary=' + form.getBoundary(),
        'content-length': form.getLengthSync()
      }
    })
  .auth(user, pass)
  .timeout(300 * 1000) // 5 minutes.
  .expectStatus(400)
  .expectHeaderContains('Content-Type', 'json')
  .expectJSON(
       { message: 'Load Resource Content-Type error',
           errorMessageCode: 'ERROR',
           resultCode: 'ERROR',
           result: []
    })
  .afterJSON(function (json) {
      console.log(json);
  })
  .toss();




/////////////////////////////////////////////////////////////////////////////
// Test 3: Invalid value for hasHeaderRow
rStream = fs.createReadStream(smallLoadInput)
form = new FormData();
form.append('loadFile0', rStream,
  {
    knownLength: fs.statSync(smallLoadInput).size,
  });

msg = myname + ': Test 3: invalid value for hasHeaderRow';
frisby.create(msg)
  .post(siteURL + loadEndPoint + tableName + '?debug=true&hasHeaderRow=maybe',
    form,
    {
      json: false,
      headers: {
        'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
        'content-length': form.getLengthSync()
      }
    })
  .auth(user, pass)
  .timeout(300 * 1000) // 5 minutes.
  .expectStatus(400)
  .expectHeaderContains('Content-Type', 'json')
  .expectJSON(
       { message: "Invalid value for parameter 'has_header_row'. Valid values are \"yes\" and \"no\".",
           errorMessageCode: 'ERROR',
           resultCode: 'ERROR' ,
           result: []
    })
  .toss();
  



/////////////////////////////////////////////////////////////////////////////
// Test 4: Invalid value for hasDateTypes
rStream = fs.createReadStream(smallLoadInput)
form = new FormData();
form.append('loadFile0', rStream,
  {
    knownLength: fs.statSync(smallLoadInput).size,
  });

msg = myname + ': Test 4: invalid value for hasDateTypes';
frisby.create(msg)
  .post(siteURL + loadEndPoint + tableName + '?debug=true&hasDateTypes="notsure"',
    form,
    {
      json: false,
      headers: {
        'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
        'content-length': form.getLengthSync()
      }
    })
  .auth(user, pass)
  .timeout(300 * 1000) // 5 minutes.
  .expectStatus(400)
  .expectHeaderContains('Content-Type', 'json')
  .expectJSON(
       { message: 'Invalid value for parameter hasDateTypes. Valid values are \"true\" and \"false\".',
           errorMessageCode: 'ERROR',
           resultCode: 'ERROR',
           result: [],
    })
  .toss();




/////////////////////////////////////////////////////////////////////////////
// Test 5: Table does not exist
rStream = fs.createReadStream(smallLoadInput)
form = new FormData();
form.append('loadFile0', rStream,
  {
    knownLength: fs.statSync(smallLoadInput).size,
  });

msg = myname + ': Test 5: table does not exist';
frisby.create(msg)
  .post(siteURL + loadEndPoint + 'bla.bla' + '?debug=true',
    form,
    {
      json: false,
      headers: {
        'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
        'content-length': form.getLengthSync()
      }
    })
  .auth(user, pass)
  .timeout(300 * 1000) // 5 minutes.
  .expectStatus(400)
  .expectHeaderContains('Content-Type', 'json')
  .expectJSON(
       { message: 'Table does not exist: bla.bla',
           errorMessageCode: 'ERROR',
           resultCode: 'ERROR',
           result: [],
    })
  .toss();

/////////////////////////////////////////////////////////////////////////////
// Test 6: no table name 
rStream = fs.createReadStream(smallLoadInput)
form = new FormData();
form.append('loadFile0', rStream,
  {
    knownLength: fs.statSync(smallLoadInput).size,
  });

msg = myname + ': Test 6: no table name in URL';
frisby.create(msg)
  .post(siteURL + loadEndPoint  + '?debug=true',
    form,
    {
      json: false,
      headers: {
        'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
        'content-length': form.getLengthSync()
      }
    })
  .auth(user, pass)
  .timeout(300 * 1000) // 5 minutes.
  .expectStatus(400)
  .expectHeaderContains('Content-Type', 'json')
  .expectJSON(
       { message: 'No table specified for LOAD target.',
           errorMessageCode: 'ERROR',
           resultCode: 'ERROR',
           result: []
    })
  .toss();


// this is a bug in the code. It doesn't handle POST /load/local/del/

/////////////////////////////////////////////////////////////////////////////
// Test 7: no table name and extra slash at end of URL
//rStream = fs.createReadStream(smallLoadInput)
//form = new FormData();
//form.append('loadFile0', rStream,
//  {
//    knownLength: fs.statSync(smallLoadInput).size,
//  });
//
//msg = myname + ': Test 7: no table name and extra slash at end of URL';
//frisby.create(msg)
//  .post(siteURL + loadEndPoint + '/' + '?debug=true',
//    form,
//    {
//      json: false,
//      headers: {
//        'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
//        'content-length': form.getLengthSync()
//      }
//    })
//  .auth(user, pass)
//  .timeout(300 * 1000) // 5 minutes.
//  .expectStatus(400)
//  .expectHeaderContains('Content-Type', 'json')
//  .expectJSON(
//       { message: 'No table specified for LOAD target.',
//           errorMessageCode: 'ERROR',
//           resultCode: 'ERROR',
//           result: []
//    })
//  .toss();
//
//

// XXX  we need tests for all of these
//
//load options
// * processLoadAction
// * processFormatType
// * processWaitForLoadComplete
// * processNumHeaderRows
// * processHasDateTypes
// * processHasHeaderRow
//
//// file type modifiers
//
// * processCodePage(loadOptionsJSON));
// * processDateFormat(loadOptionsJSON));
// * processTimeFormat(loadOptionsJSON));
// * processTimestampFormat(loadOptionsJSON));
// * processColumnDelimiter(loadOptionsJSON)); 
