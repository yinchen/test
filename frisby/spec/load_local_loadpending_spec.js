//////////////////////////////////////////////////////
// ENDPOINT - POST /load/local/del
// TEST - test that a tane in load pending state will be repaired and loaded 
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var server = require('../lib/server.js');
var path = require('path');
var fs = require('fs');
var loadUtils = require('../lib/loadutils.js');
var ibmdb = require('ibm_db');
var FormData = require('form-data');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadLocalEndPoint = server.loadLocalEndPoint();
var tableName = 'ORDER_DETAILS';
var tableSchema = server.tabSchema();
var user = server.userID();
var pass = server.password();
var connString = server.connString();
var myname = path.basename(__filename);
var printErrorLine = server.printErrorLine;

//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
})


////////////////////////////////////////////////////////////////////////////////
//
var msg  = myname + ': Test 1a: put a table in load pending state and then run a POST /load/local/del';
//
////////////////////////////////////////////////////////////////////////////////
var rowcount = 1000;
var numRowsRead = rowcount;
var numRowsLoaded = rowcount;
var numRowsCommitted = rowcount;
var numRowsRejected = 0;
var numRowsSkipped = 0;
var numRowsDeleted = 0;


var filePath = path.resolve(__dirname, '../datasets/order_details_small.csv')
var fileStream = fs.createReadStream(filePath)

var form = new FormData();
form.append('loadFile0', fileStream,
            {
                knownLength: fs.statSync(filePath).size,
            });

// We are doing this synchronously to avoid less callback hell. Frisby called from within 
// too many callbacks doesn't work well
try {
    // Open a connection
    var conn = ibmdb.openSync(connString);
} catch(e) {
    console.log(e.message);
    printErrorLine();
    process.exit(1);
}

// We need to put the table in load pending state. This statement should do the trick
// NOTE the file to be loaded needs to be given as an absolute path. I won't know the user's home directory
// from the client side, for the time being, /etc/passwd will do. 
//
// In future, the load paths will be restricted to user dirs. We'll need to come up with a new solution then
var loadStmt = "CALL ADMIN_CMD('LOAD FROM /etc/passwd OF DEL WARNINGCOUNT 1 MESSAGES ON SERVER REPLACE INTO "+ tableSchema + "." + tableName + "')";
var tabInfoStmt = "select LOAD_STATUS from TABLE(SYSPROC.ADMIN_GET_TAB_INFO('" + tableSchema + "','" + tableName + "')) where tabschema = '" + tableSchema + "' and tabname = '" + tableName + "'";
// run query. The loads will be kicked off in a callback
conn.query(loadStmt, 
           function (err, data) {

               if (err) {
                   console.log(err);
                   printErrorLine();
                   process.exit(1);
               } 

               // ensure the table is in load pending state
               try {
                   var rows = conn.querySync(tabInfoStmt);
               } catch(e) {
                   console.log(e.message);
                   printErrorLine();
                   process.exit(1);
               }

               if(rows[0].LOAD_STATUS != 'PENDING') {
                   console.log(e.message);
                   printErrorLine();
                   process.exit(1);
               } else {
                   console.log("table is in load pending state after execution of first load staetment");
               }

////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 1b: run a load via POST /load/server which should succeed as it first removes the table from load pending state';
//
////////////////////////////////////////////////////////////////////////////////
               frisby.create(msg)
                     .post(siteURL + loadLocalEndPoint + tableSchema + '.' + tableName + '?loadAction=INSERT&debug=true',
                           form,
                           {
                             json: false,
                             headers: {
                               'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
                               'content-length': form.getLengthSync()
                             }
                           })
                     .auth(user, pass)
                     .expectStatus(200)
                     .timeout(60000)
                     .expectHeaderContains('Content-Type', 'json')
                     .inspectJSON()
                     .expectJSON(
                                 {
                                     errorMessageCode : "NONE",
                                     resultCode : "SUCCESS",
                                     result : {
                                         TABLE : tableName,
                                         SCHEMA : tableSchema
                                     }
                                 })
                     .afterJSON(function(json) {
                                    loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                                    var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                                    loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, 0, 0);
                                })
                     .toss();
             });

//////////////////////////////////////////////////////////////////////
//
// Due to the way Frisby is implemented, if you call it from within a callback, as we've done above, 
// this script will end and no tests will be called. It appears that if you put a dummy test here at
// the end, that things actually work
//
//////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + server.asperaEndPoint()+'getUploadSizeCap')
      .auth(user, pass)
      .expectStatus(200)
      .expectHeader('content-type', 'application/json')
      .expectJSON(
                    {
                      message: "Upload size cap successfully retrieved",
                      result: {
                          unit: "Bytes",
                          },
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS"
                    }
                  )
      .expectJSONTypes (
                        {
                        message: String,
                        result: {
                              unit: String,
                              value: Number
                              },
                        errorMessageCode: String,
                        resultCode: String
                        }) 
      .inspectJSON()
      .after(  function() {setTimeout(function(){i = 1},3000); })
      .toss();

      // this test only seems to pass if you have a second dummy
frisby.create('workaround dummy test')
      .get(siteURL + server.homeEndPoint())
      .auth(user, pass)
      .expectStatus(200)
      .expectHeader('content-type', 'application/json')
      .expectJSON(
                  {
                      message: "NONE",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS"
                  })
      .expectJSONTypes(
                       {
                           message: String,
                           result: String,
                           errorMessageCode: String,
                           resultCode: String
                       }) 
      .toss();


      // this test only seems to pass if you have a second dummy
frisby.create('workaround dummy test')
      .get(siteURL + server.homeEndPoint())
      .auth(user, pass)
      .expectStatus(200)
      .expectHeader('content-type', 'application/json')
      .expectJSON(
                  {
                      message: "NONE",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS"
                  })
      .expectJSONTypes(
                       {
                           message: String,
                           result: String,
                           errorMessageCode: String,
                           resultCode: String
                       }) 
      .toss();


      // this test only seems to pass if you have a second dummy
frisby.create('workaround dummy test')
      .get(siteURL + server.homeEndPoint())
      .auth(user, pass)
      .expectStatus(200)
      .expectHeader('content-type', 'application/json')
      .expectJSON(
                  {
                      message: "NONE",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS"
                  })
      .expectJSONTypes(
                       {
                           message: String,
                           result: String,
                           errorMessageCode: String,
                           resultCode: String
                       }) 
      .toss();

