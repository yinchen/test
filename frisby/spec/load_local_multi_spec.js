var frisby = require('frisby');
var expect = require('expect');
var server = require('../lib/server.js'); // Internal QA Test Server Info
var fs = require('fs');
var path = require('path');
var FormData = require('form-data');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

var debug=true; // write code trace to /opt/ibm/dsserver/logs/loadrestapis.log

var loadEndPoint = server.loadLocalEndPoint();
var user = server.userID();
var pass = server.password();
var siteURL = server.siteURL();
var tableName = server.loadTable();

// API default vars
var format='json'

/////////////////////////////////////////////////////////////////////////////////////
// Test 1: Load multiple csv files in a single POST.

var basicLoadInput1 = path.resolve(__dirname, '../datasets/small2.csv')
var basicLoadInput2 = path.resolve(__dirname, '../datasets/small3.csv')
var basicLoadInput3 = path.resolve(__dirname, '../datasets/small4.csv')

var rStream1 = fs.createReadStream(basicLoadInput1)
var rStream2 = fs.createReadStream(basicLoadInput2)
var rStream3 = fs.createReadStream(basicLoadInput3)


var form = new FormData();

form.append('loadFile1', rStream1, {
    knownLength: fs.statSync(basicLoadInput1).size,
  });

form.append('loadFile2', rStream2, {
    knownLength: fs.statSync(basicLoadInput2).size,
  });

form.append('loadFile3', rStream3, {
    knownLength: fs.statSync(basicLoadInput3).size,
  });



var numRowsRead = 147;
var numRowsLoaded = 147;
var numRowsCommitted = 147;
var numRowsRejected = 0;
var numRowsSkipped = 0;
var numRowsDeleted = 0;

frisby.create('load_local_multi_spec.js: Test1 (load multiple files)')
     .post(siteURL + loadEndPoint + tableName + '?hasHeaderRow=false&loadAction="INSERT"&waitForLoadComplete=true&debug='+debug+'&timestampFormat="YYYY-MM-DD%20HH:MM:SS.U"&hasDateTypes="true"',
     form,
  {
    json: false,
    headers: {
      'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
      'content-length': form.getLengthSync()
    }
  })
  .timeout(100000)
  .auth(user, pass)
  .expectStatus(200)
  .expectHeaderContains('Content-Type', 'json')
  .expectJSON(
    {
      errorMessageCode: "NONE",
      resultCode: "SUCCESS",
      result: {
        TABLE: server.tabName,
        SCHEMA: server.tabSchema,
        LOAD_STATUS: "COMPLETE",
        ROWS_COMMITTED: numRowsCommitted,
        ROWS_READ: numRowsRead,
        ROWS_LOADED: numRowsLoaded,
        ROWS_DELETED: numRowsDeleted,
        ROWS_REJECTED: numRowsRejected
      }
    }
  )
   //.inspectJSON()
   .afterJSON( function(json) {
       expect(json.result.LOAD_LOGFILE).toMatch(/loadlogs\/load_[0-9].*\.txt/);
       expect(json.result.LOAD_ID).toBeGreaterThan(0);
       expect(json.result.START_TIME).toMatch(/........ ..:..:.. .../);
       expect(json.result.END_TIME).toMatch(/........ ..:..:.. .../);
  })
  .toss();


/////////////////////////////////////////////////////////////////////////////////////
// Load a mix of del and XLS files in a single POST.

basicLoadInput1 = path.resolve(__dirname, '../datasets/small2.csv')
basicLoadInput2 = path.resolve(__dirname, '../datasets/small3.csv')
basicLoadInput3 = path.resolve(__dirname, '../datasets/order_details_header_small.xlsx')

rStream1 = fs.createReadStream(basicLoadInput1)
rStream2 = fs.createReadStream(basicLoadInput2)
rStream3 = fs.createReadStream(basicLoadInput3)


form = new FormData();

form.append('loadFile1', rStream1, {
    knownLength: fs.statSync(basicLoadInput1).size,
  });

form.append('loadFile2', rStream2, {
    knownLength: fs.statSync(basicLoadInput2).size,
  });

form.append('loadFile3', rStream3, {
    knownLength: fs.statSync(basicLoadInput3).size,
  });

numRowsRead = 1096;
numRowsLoaded = 1096;
numRowsCommitted = 1096;
numRowsRejected = 0;
numRowsSkipped = 0;
numRowsDeleted = 0;

frisby.create('load_local_multi_spec.js: Test2 (mixed csv and xls)')
     .post(siteURL + loadEndPoint + tableName + '?hasHeaderRow=true&loadAction=\"INSERT\"&waitForLoadComplete=true&debug='+debug+'&timestampFormat=\"YYYY-MM-DD%20HH:MM:SS.U\"&hasDateTypes=true',
     form,
  {
    json: false,
    headers: {
      'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
      'content-length': form.getLengthSync()
    }
  })

  .timeout(300000)
  .auth(user, pass)
  .expectStatus(200)
  .expectHeaderContains('Content-Type', 'json')
  .expectJSON(
    {
      errorMessageCode: "NONE",
      resultCode: "SUCCESS",
      result: {
        TABLE: server.tabName,
        SCHEMA: server.tabSchema,
        LOAD_STATUS: "COMPLETE",
        ROWS_COMMITTED: numRowsCommitted,
        ROWS_READ: numRowsRead,
        ROWS_LOADED: numRowsLoaded,
        ROWS_DELETED: numRowsDeleted,
        ROWS_REJECTED: numRowsRejected
      }
    }
  )
   //.inspectJSON()
   .afterJSON( function(json) {
       expect(json.result.LOAD_LOGFILE).toMatch(/loadlogs\/load_[0-9].*\.txt/);
       expect(json.result.LOAD_ID).toBeGreaterThan(0);
       expect(json.result.START_TIME).toMatch(/........ ..:..:.. .../);
       expect(json.result.END_TIME).toMatch(/........ ..:..:.. .../);
  })
  .toss();

