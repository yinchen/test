var frisby = require('frisby');
var expect = require('expect');
var server = require('../lib/server.js');
var fs = require('fs');
var path = require('path');
var FormData = require('form-data');
var loadUtils = require('../lib/loadutils.js');



// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadEndPoint = server.loadLocalEndPoint();
var tabSchema = server.tabSchema();
var tableName = server.tabName();
var user = server.userID();
var pass = server.password();


var numRowsSmall = 49; // number of expected rows loaded from this file.

// API default vars
var format = 'json'

//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
});

/////////////////////////////////////////////////////////////////////////////////////
// Test 1: Post a small file. Base Case.

var smallLoadInput = path.resolve(__dirname, '../datasets/small.csv')
var rStream = fs.createReadStream(smallLoadInput)

var form = new FormData();
form.append('loadFile0', rStream,
  {
    knownLength: fs.statSync(smallLoadInput).size,
  });

frisby.create('load_local_spec.js: Test 1: POST /load/local/del/small.csv')
  .post(siteURL + loadEndPoint + tabSchema + '.' + tableName + '?debug=true&hasHeaderRow=true&hasDateTypes=true&waitForLoadComplete=true&loadAction=INSERT&timestampFormat=YYYY-MM-DD%20HH:MM:SS.U',
    form,
    {
      json: false,
      headers: {
        'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
        'content-length': form.getLengthSync()
      }
    })
  .auth(user, pass)
  .expectStatus(200)
  .timeout(60000)
  .expectHeaderContains('Content-Type', 'json')
  .inspectBody()
  .expectJSON(
    {
      errorMessageCode: "NONE",
      resultCode: "SUCCESS",
      result: {
        TABLE: server.tabName,
        SCHEMA: server.tabSchema
      }
    }
    )
  .afterJSON(function (json) {
    loadUtils.validateLoadResult(json, numRowsSmall, numRowsSmall, numRowsSmall, 0, 0);
  })
  .toss();


////////////////////////////////////////////////////////////////////////////////////////////////
// TEST2
// Larger File Test, LOAD_STATUS expected to be RUNNING when returned.
var bigLoadInput = path.resolve(__dirname, '../datasets/order_details.csv')
var numRowsRead = 446023;
var numRowsLoaded = 446023;
var numRowsCommitted = 446023;
var numRowsRejected = 0;
var numRowsSkipped = 0;
var numRowsDeleted = 0;


var form2 = new FormData();

var rStream2 = fs.createReadStream(bigLoadInput)

form2.append('loadFile0', rStream2,
  {
    knownLength: fs.statSync(bigLoadInput).size,
  });


frisby.create('load_local_spec.js: Test 2: POST /load/local/del/order_details.csv (Large File)')
  .post(siteURL + loadEndPoint + tabSchema + '.' + tableName + '?debug=true&hasHeaderRow=true&waitForLoadComplete=true&loadAction=REPLACE&timestampFormat=\"YYYY-MM-DD%20HH:MM:SS.U\"',
    form2,
    {
      json: false,
      headers: {
        'content-type': 'multipart/form-data; boundary=' + form2.getBoundary(),
        'content-length': form2.getLengthSync()
      }
    })
  .timeout(300 * 1000) // 5 minutes.
  .auth(user, pass)
  .expectStatus(200)
  .timeout(60000)
  .expectHeaderContains('Content-Type', 'json')
  .inspectJSON()
  .expectJSON(
    {
      errorMessageCode: "NONE",
      resultCode: "SUCCESS",
      result: {
          TABLE: server.tabName(),
          SCHEMA: server.tabSchema()
      }
    }
    )
  .afterJSON(function(json) {
     loadUtils.validateLoadResult(json, numRowsRead, numRowsLoaded, numRowsCommitted, 0, 0);

    // Return the results from the previous load.
    frisby.create('Test 3: GET /load/LoadID (Large Load)')
      .get(siteURL + '/load/' + json.result.LOAD_ID)
      .auth(user, pass)
      .expectHeaderContains('Content-Type', 'json')
      //.inspectJSON()
      .expectJSON({
        message: 'LOAD retrieved successfully.',
        errorMessageCode: "NONE",
        result:  {
          result: {
            TABLE: server.tabName(),
            SCHEMA: server.tabSchema()
          },
          errorMessageCode: "NONE",
          resultCode: "SUCCESS"
        },
        resultCode: "SUCCESS"
      })
      .afterJSON(function (json) {
         loadUtils.validateLoadResult(json, numRowsRead, numRowsLoaded, numRowsCommitted, 0, 0);
      })
      .toss();
  })
  .toss();
