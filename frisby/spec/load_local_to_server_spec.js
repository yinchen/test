var frisby = require('frisby');
var expect = require('expect');
var server = require('../lib/server.js');
var fs = require('fs');
var path = require('path');
// Creates multipart/form-data streams
var FormData = require('form-data');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var serverhomeURL = server.homeURL();
var user = server.userID();
var pass = server.password();


// API default vars
var format = 'json'

//Global setup for all tests
frisby.globalSetup({
    timeout: (60 * 1000)
});

/////////////////////////////////////////////////////////////////////////////////////
// Test 1: Post a small file to user home dir on server. Base Case.

var smallLoadInput = path.resolve(__dirname, '../datasets/small.csv')
// Set up a ReadStream object, which readies our file for uploading.  fs.statSync gets the size of the file
var rStream = fs.createReadStream(smallLoadInput)

var form = new FormData();
form.append('loadFile0', rStream,
  {
    knownLength: fs.statSync(smallLoadInput).size,
  });

frisby.create('load_local_to_server_spec.js: Test 1: POST /load/local/del/small.csv')
  .post(serverhomeURL,
    form,
    {
      json: false,
      headers: {
        'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
        'content-length': form.getLengthSync()
      }
    })
  .auth(user, pass)
  .expectStatus(200)
  .timeout(60000)
  .expectHeaderContains('Content-Type', 'application/json')
  .inspectBody()
  .expectJSON(
    {
      result: {"filesUploaded":["small.csv"]},
      errorMessageCode: "NONE",
      resultCode: "SUCCESS"
    }
    )
/*  .afterJSON(function (json) {
    loadUtils.validateLoadResultJson(json, numRowsSmall, numRowsSmall, numRowsSmall, 0, 0);
  })
*/
  .toss();

