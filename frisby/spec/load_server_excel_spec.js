//////////////////////////////////////////////////////
// ENDPOINT - POST:/load/server,GET:/load
// TEST - POST /load/server with excel file
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var server = require('../lib/server.js');
var fs = require('fs');
var path = require('path');
var FormData = require('form-data');
var loadUtils = require('../lib/loadutils.js');


// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadEndPoint = server.loadServerEndPoint();
var serverhomeURL = server.homeURL();
var user = server.userID();
var pass = server.password();
var tableSchema = server.tabSchema();
var myname = path.basename(__filename);
var msg;
var fileName = 'order_details_header_small.xlsx';
var filePath = path.resolve(__dirname, '../datasets/' + fileName);

var numRows = 1000; // number of expected rows loaded from this file is total-1 (first line will be stripped).

/////////////////////////////////////////////////////////////////////////////////////
// 
msg = myname + ": Test 1: Upload Excel file with a header row.";
//
/////////////////////////////////////////////////////////////////////////////////////

var rStream = fs.createReadStream(filePath)
var form = new FormData();
form.append('loadFile0', rStream,
            {
              knownLength: fs.statSync(filePath).size,
            });

frisby.create(msg)
      .post(serverhomeURL,
            form,
            {
               json: false,
               headers: {
                 'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
                 'content-length': form.getLengthSync()
               }
            })
     .auth(user, pass)
     .expectStatus(200)
     .timeout(5*60* 1000)
     .expectHeaderContains('Content-Type', 'application/json')
     .expectJSON({
                  errorMessageCode: "NONE",
                  resultCode: "SUCCESS",
			 	  message: "NONE",
			      result: {
						     "filesUploaded": [
			                fileName
			                 ]
			              },
                 } )
     .after(function(err, res, body) 
           {
/////////////////////////////////////////////////////////////////////////////////////
// 
              frisby.create(myname + ": Test 2: Load excel file with header row")
//
/////////////////////////////////////////////////////////////////////////////////////
              
                    .post(siteURL + loadEndPoint , 
                          {
                            "LOAD_SOURCE": "SERVER",
                            "SERVER": {
                              "SERVER_FILE_PATH": fileName
                            },
                            "LOAD_PARAMETERS": {
                              "SCHEMA": tableSchema,
                              "TABLE": "ORDER_DETAILS",
                              "LOAD_ACTION": "INSERT",
                              "HAS_HEADER_ROW": "TRUE",
                            }
                          },
                          {json: true })
                    .auth(user, pass)
                    .expectStatus(200)
                    .timeout(30000)
                    .expectHeaderContains('Content-Type', 'json')
                    .inspectBody()
                    .expectJSON({
                                  errorMessageCode: "NONE",
                                  resultCode: "SUCCESS",
                                  message: 'LOAD started successfully.',
                                  result: {
                                    TABLE: server.tabName,
                                    SCHEMA: server.tabSchema,
                                    LOAD_STATUS: 'RUNNING'
                                  }
                                })
                    .afterJSON(function (json) {
					     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
						 var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                         loadUtils.validateLoadResult(bodyJSON, numRows, numRows, numRows, 0, 0);
                       })
                   .toss();
           })
     .toss();
