//////////////////////////////////////////////////////
// ENDPOINT - POST /load/server,GET //load/{loadID}
// TEST - test different character delimiters
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var expect = require('expect');
var server = require('../lib/server.js');
var fs = require('fs');
var path = require('path');
var FormData = require('form-data');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var tableName = "TINY_TABLE_3COL";
var tableSchema = server.tabSchema();
// doesn't matter what this file is (once it exists), since it won't be loaded
var loadFileNameForErrorTests = "tiny_table_3col.csv";
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);

//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
})

var numRowsRead = 11;
var numRowsLoaded = 11;
var numRowsCommitted = 11;
var numRowsRejected = 0;
var numRowsSkipped = 0;
var numRowsDeleted = 0;

////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 1: CHARDEL ampersand';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table_3col_chardel_ampersand.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "CHARDEL": "0x26"
                    },
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: tableName,
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, 0, 0);
                     loadUtils.verifyAllTableContents(tableSchema, tableName, server.tiny_table_3col_expected_rows);
                 })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 2: CHARDEL "' ;
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table_3col.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "CHARDEL": "\""
                    }
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: tableName,
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, 0, 0);
                     loadUtils.verifyAllTableContents(tableSchema, tableName, server.tiny_table_3col_expected_rows);
                 })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 3: CHARDEL 0x22 (")' ;
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table_3col.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "CHARDEL": "0x22"
                    }
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: tableName,
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, 0, 0);
                     loadUtils.verifyAllTableContents(tableSchema, tableName, server.tiny_table_3col_expected_rows);
                 })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + " : Test 4: CHARDEL '" ;
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table_3col_chardel_singlequote.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "CHARDEL": "'"
                    }
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: tableName,
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, 0, 0);
                     loadUtils.verifyAllTableContents(tableSchema, tableName, server.tiny_table_3col_expected_rows);
                 })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + " : Test 5: CHARDEL 0x27 (')" ;
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table_3col_chardel_singlequote.csv"
                 },
                "LOAD_PARAMETERS": {
                "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                      "CHARDEL": "0x27"
                    }
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: tableName,
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, 0, 0);
                     loadUtils.verifyAllTableContents(tableSchema, tableName, server.tiny_table_3col_expected_rows);
                 })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 6: CHARDEL | (0x7c)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table_3col_chardel_pipe.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                      "CHARDEL": "|"
                    }
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: tableName,
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, 0, 0);
                     loadUtils.verifyAllTableContents(tableSchema, tableName, server.tiny_table_3col_expected_rows);
                 })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 7: invalid CHARDEL - weird value';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "FILE_TYPE_MODIFIERS": {
                        "CHARDEL": "�"
                    },
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 8: invalid CHARDEL 0x00';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                  "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "FILE_TYPE_MODIFIERS": {
                        "CHARDEL": "0x00"
                    },
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();



////  COMMENTED-OUT the following tests have been commented out until the following is fixed: https://github.ibm.com/CloudDataServices/dashdb-dsserver/issues/180
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 9: invalid CHARDEL space';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "CHARDEL": " "
////                      }
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 10: invalid CHARDEL 0x20 (space)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "FILE_TYPE_MODIFIERS": {
                        "CHARDEL": "0x20"
                    }
                }
            },
            {json: true })
      .auth(user, pass)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();


//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 11: invalid CHARDEL linefeed';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "FILE_TYPE_MODIFIERS": {
                        "CHARDEL": "0x0a"
                    },
                }
            },
            {json: true })
      .auth(user, pass)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();


//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 12: invalid CHARDEL carriage return';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "FILE_TYPE_MODIFIERS": {
                        "CHARDEL": "0x0d"
                    },
                }
            },
            {json: true })
      .auth(user, pass)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 13: invalid CHARDEL decimal point';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "FILE_TYPE_MODIFIERS": {
                        "CHARDEL": "."
                    },
                }
            },
            {json: true })
      .auth(user, pass)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 14: invalid CHARDEL 0x2e (decimal point)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "FILE_TYPE_MODIFIERS": {
                         "CHARDEL": "0x2e"
                    },
                }
            },
            {json: true })
      .auth(user, pass)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();

////  COMMENTED-OUT the following tests have been commented out until the following is fixed: https://github.ibm.com/CloudDataServices/dashdb-dsserver/issues/161
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 15: invalid CHARDEL ""';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "CHARDEL": "\"\""
////                      },
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
////  
////  
////  
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 16: empty CHARDEL (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "CHARDEL": ""
////                    },
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
////  
////  
////  
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 17: invalid CHARDEL - JSON array (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "CHARDEL": ["h"]
////                      },
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
////  
////
////
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 18: invalid CHARDEL - JSON object (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "CHARDEL": {"a":"b" }
////                      },
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
////  
////  
////  
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ': Test 19: invalid value, "hello" (5 byte string) for CHARDEL (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "LOAD_ACTION": "REPLACE",
////                      "FILE_TYPE_MODIFIERS": {
////                          "CHARDEL": "hello",
////                      },   
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .expectStatus(400)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
////                        errorMessageCode: "ERROR",
////                        resultCode: "ERROR",
////                        result: []
////                    })
////        .toss();
////  
////  
////  
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ': Test 20: invalid value, ",ello" (5 byte string beginning with a valid delimiter) for CHARDEL (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "LOAD_ACTION": "REPLACE",
////                      "FILE_TYPE_MODIFIERS": {
////                          "CHARDEL": ",ello",
////                      },   
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .expectStatus(400)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
////                        errorMessageCode: "ERROR",
////                        resultCode: "ERROR",
////                        result: []
////                    })
////        .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 21: invalid value, "blah" for CHARDEL (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "CHARDEL": "blah",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 22: invalid value, ",lah" for CHARDEL (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "CHARDEL": ",lah",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .toss();



////  COMMENTED-OUT the following tests have been commented out until the following is fixed: https://github.ibm.com/CloudDataServices/dashdb-dsserver/issues/161
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ': Test 23: invalid value, "0xZ/" (invalid hex)for CHARDEL (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "LOAD_ACTION": "REPLACE",
////                      "FILE_TYPE_MODIFIERS": {
////                          "CHARDEL": "0xZ/",
////                      },   
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .expectStatus(400)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
////                        errorMessageCode: "ERROR",
////                        resultCode: "ERROR",
////                        result: []
////                    })
////        .toss();
////  
////  
////  
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ': Test 24: invalid value, " ," for CHARDEL (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "LOAD_ACTION": "REPLACE",
////                      "FILE_TYPE_MODIFIERS": {
////                          "CHARDEL": " ,",
////                      },   
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .expectStatus(400)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
////                        errorMessageCode: "ERROR",
////                        resultCode: "ERROR",
////                        result: []
////                    })
////        .toss();
