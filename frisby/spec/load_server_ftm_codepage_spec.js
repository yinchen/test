//////////////////////////////////////////////////////
// ENDPOINT - POST:/load/server
// TEST - CODE_PAGE load parameter
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var server = require('../lib/server.js');
var fs = require('fs');
var path = require('path');
var FormData = require('form-data');
var loadUtils = require('../lib/loadutils.js');
var ibmdb = require("ibm_db");


// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var user = server.userID();
var pass = server.password();
var tableSchema = server.tabSchema();
var myname = path.basename(__filename);

var tableName = "TINY_TABLE";
var fileName = "tiny_table.csv";

////  COMMENTED-OUT the following tests have been commented out until the following is fixed: https://github.ibm.com/CloudDataServices/dashdb-dsserver/issues/172
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  var msg = myname + ': Test 1: CODE_PAGE is an empty string (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint,
////              {
////                  "LOAD_SOURCE" : "SERVER",
////                  "SERVER" : {
////                      "SERVER_FILE_PATH": fileName
////                  },
////                  "LOAD_PARAMETERS" : {
////                      "SCHEMA" : tableSchema,
////                      "TABLE" : tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "CODE_PAGE": ""
////                      }
////                  }
////              },
////              {json : true})
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(60000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        errorMessageCode : "ERROR",
////                        resultCode : "ERROR",
////                        message: 'blah',
////                        result : []
////                    })
////        .toss();
////  
////  
////  
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ': Test 2: CODE_PAGE is a blank string (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  var tableName = "TINY_TABLE";
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint,
////              {
////                  "LOAD_SOURCE" : "SERVER",
////                  "SERVER" : {
////                      "SERVER_FILE_PATH": fileName
////                  },
////                  "LOAD_PARAMETERS" : {
////                      "SCHEMA" : tableSchema,
////                      "TABLE" : tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "CODE_PAGE": " "
////                      }
////                  }
////              },
////              {json : true})
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(60000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        errorMessageCode : "ERROR",
////                        resultCode : "ERROR",
////                        message: 'blah',
////                        result : []
////                    })
////        .toss();
////  
////  
////  
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ': Test 3: CODE_PAGE is a JSON object (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  var tableName = "TINY_TABLE";
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint,
////              {
////                  "LOAD_SOURCE" : "SERVER",
////                  "SERVER" : {
////                      "SERVER_FILE_PATH": fileName
////                  },
////                  "LOAD_PARAMETERS" : {
////                      "SCHEMA" : tableSchema,
////                      "TABLE" : tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "CODE_PAGE": { "hello": "there"}
////                      }
////                  }
////              },
////              {json : true})
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(60000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        errorMessageCode : "ERROR",
////                        resultCode : "ERROR",
////                        message: 'blah',
////                        result : []
////                    })
////        .toss();
////  
////  
////  
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ': Test 4: CODE_PAGE is a JSON array (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  var tableName = "TINY_TABLE";
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint,
////              {
////                  "LOAD_SOURCE" : "SERVER",
////                  "SERVER" : {
////                      "SERVER_FILE_PATH": fileName
////                  },
////                  "LOAD_PARAMETERS" : {
////                      "SCHEMA" : tableSchema,
////                      "TABLE" : tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "CODE_PAGE": [ "hello", "there"]
////                      }
////                  }
////              },
////              {json : true})
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(60000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        errorMessageCode : "ERROR",
////                        resultCode : "ERROR",
////                        message: 'blah',
////                        result : []
////                    })
////        .toss();
////  
////  
////  
////  
////  ////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ': Test 5: Invalid string value for CODE_PAGE (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": fileName
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA" : tableSchema,
////                      "TABLE" : tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "CODE_PAGE": "2321gasdl2"
////                      }
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .expectStatus(200)
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: "LOAD started successfully.",
////                        errorMessageCode : "NONE",
////                        resultCode : "SUCCESS",
////                        result : {
////                            TABLE : tableName,
////                            SCHEMA : tableSchema
////                        }
////                    })
////         .afterJSON(
////                    function(json) {
////                       loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
////                       var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
////                       loadUtils.validateLoadResult(bodyJSON, "", "",
////                                                       "", "", "", "ERROR");
////                       expect(bodyJSON.result.message).toBe('No rows of data were read');
////                       // should be a WARNING
////                       expect(bodyJSON.result.result.WARNING).toMatch('Routine "SYSPROC.ADMIN_CMD" execution has completed, but at least one error, "SQL3527"');
////                               
////                    })
////         .toss();

 

////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 6: Invalid numeric value for CODE_PAGE (negative test)';
//
////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": fileName
                },
                "LOAD_PARAMETERS": {
                    "TABLE" : tableName,
                    "SCHEMA" : tableSchema,
                    "FILE_TYPE_MODIFIERS": {
                        "CODE_PAGE": "-2"
                    }
                }
            },
            {json: true })
      .auth(user, pass)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .expectStatus(400)
      .inspectJSON()
      .expectJSON(
                  {
                    errorMessageCode : "ERROR",
                    resultCode : "ERROR",
                    message: 'Invalid code page. Review the REST API documentation for possible values.',
                    result : []
      })
     .toss();

 

////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 7: load a file that is not UTF-8 using codepage=1383 and check it iss loaded correctly';
//
////////////////////////////////////////////////////////////////////
//
//
// I have created a cp1383 file from a UTF-8 file. 1383 is a Chinese encoding
// (GB2312) which is quite different from UTF-8 for Chinese characters (but the same for ASCII characters). 
//
// By loading the file as code page 1383, we should see it converted into uUTF-8 by the server (DB is UTF-8)
//
// We can then run a query to check we hav the correct values i the database
var numRows = 2;
var numRowsRead = numRows,
    numRowsLoaded = numRows,
    numRowsCommitted = numRows,
    numRowsSkipped = 0,
    numRowsDeleted = 0;

frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table_cp1383.csv"
                },
                "LOAD_PARAMETERS": {
                    "TABLE" : tableName,
                    "SCHEMA" : tableSchema,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "CODE_PAGE": "1383"
                    }
                }
            },
            {json: true })
      .auth(user, pass)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .expectStatus(200)
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode : "NONE",
                      resultCode : "SUCCESS",
                      result : {
                          TABLE : tableName,
                          SCHEMA : tableSchema
                      }
                  })
       .afterJSON(
                  function(json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON,numRowsRead, numRowsLoaded, numRowsCommitted, numRowsSkipped, numRowsDeleted);
                     loadUtils.verifyAllTableContents(tableSchema, tableName,
							 server.tiny_table_cp1383_expected_rows,
                             // check the values in the table are the utf-converted values
                             "select hex(col1) c1, hex(col2) c2 from " + tableName + " ORDER BY 1")
                  })
       .toss();
