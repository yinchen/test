//////////////////////////////////////////////////////
// ENDPOINT - POST /load/server,GET //load/{loadID} 
// TEST - test different column delimiters
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var server = require('../lib/server.js');
var path = require('path');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates 
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var tableName = "TINY_TABLE";
var tableSchema = server.tabSchema();
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);
var loadFileNameForErrorTests = "tiny_table.csv";

//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
})



var numRowsRead = 12;
var numRowsLoaded = 12;
var numRowsCommitted = 12;
var numRowsRejected = 0;
var numRowsSkipped = 0;
var numRowsDeleted = 0;

////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 1: COLDEL value "0x2c" (,)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "COLDEL": "0x2c",
                  },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: tableName,
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, 0, 0);
    				 loadUtils.verifyAllTableContents(tableSchema, tableName, server.tiny_table_expected_rows);
                 })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 2: COLDEL value ","';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "COLDEL": ",",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: tableName,
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, 0, 0);
	    			 loadUtils.verifyAllTableContents(tableSchema, tableName, server.tiny_table_expected_rows);
                  })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 3: COLDEL = "0x02" (.csv file)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table_coldel_0x02.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "COLDEL": "0x02",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: tableName,
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, 0, 0);
	    			 loadUtils.verifyAllTableContents(tableSchema, tableName, server.tiny_table_expected_rows);
                  })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 4: COLDEL = "0x02" (.dat file) - this was misconstrued as a non-text file by the now-removed file type checker';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table_coldel_0x02.dat"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "COLDEL": "0x02",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: tableName,
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, 0, 0);
	    			 loadUtils.verifyAllTableContents(tableSchema, tableName, server.tiny_table_expected_rows);
                  })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 5: COLDEL = "|"';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table_coldel_pipe_v002.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "COLDEL": "|",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: tableName,
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, 0, 0);
	    			 loadUtils.verifyAllTableContents(tableSchema, tableName, server.tiny_table_expected_rows);
                 })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 6: COLDEL = "0x7c" (|)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table_coldel_pipe_v003.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "COLDEL": "0x7c",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: tableName,
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, 0, 0);
	    			 loadUtils.verifyAllTableContents(tableSchema, tableName, server.tiny_table_expected_rows);
                 })
      .toss();



////  COMMENTED-OUT the following tests have been commented out until the following is fixed: https://github.ibm.com/CloudDataServices/dashdb-dsserver/issues/180
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ': Test 7: invalid value, " " for COLDEL (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "LOAD_ACTION": "REPLACE",
////                      "FILE_TYPE_MODIFIERS": {
////                          "COLDEL": " ",
////                      },   
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .expectStatus(400)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
////                        errorMessageCode: "ERROR",
////                        resultCode: "ERROR",
////                        result: []
////                    })
////        .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 8: invalid value, 0x20 (" ") for COLDEL (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "COLDEL": "0x20",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .toss();

 

////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 9: invalid value, 0x0A (line feed) for COLDEL (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "COLDEL": "0x0A",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 10: invalid value, 0x0D (cariage return) for COLDEL (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "COLDEL": "0x0D",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 11: invalid value, 0x00 for COLDEL (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "COLDEL": "0x00",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 12: invalid value, "." for COLDEL (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "COLDEL": ".",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 13: invalid value, 0x2e (".") for COLDEL (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "COLDEL": "0x2e",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .toss();



////  COMMENTED-OUT the following tests have been commented out until the following is fixed: https://github.ibm.com/CloudDataServices/dashdb-dsserver/issues/180
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ': Test 14: invalid value, "hello" (5 byte string) for COLDEL (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "LOAD_ACTION": "REPLACE",
////                      "FILE_TYPE_MODIFIERS": {
////                          "COLDEL": "hello",
////                      },   
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .expectStatus(400)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
////                        errorMessageCode: "ERROR",
////                        resultCode: "ERROR",
////                        result: []
////                    })
////        .toss();
////  
////  
////  
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ': Test 15: invalid value, ",ello" (5 byte string beginning with a valid delimiter) for COLDEL (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "LOAD_ACTION": "REPLACE",
////                      "FILE_TYPE_MODIFIERS": {
////                          "COLDEL": ",ello",
////                      },   
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .expectStatus(400)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
////                        errorMessageCode: "ERROR",
////                        resultCode: "ERROR",
////                        result: []
////                    })
////        .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 16: invalid value, "blah" for COLDEL (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "COLDEL": "blah",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 17: invalid value, ",lah" for COLDEL (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "COLDEL": ",lah",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 18: invalid value, "0xZ/" (invalid hex)for COLDEL (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "COLDEL": "0xZ/",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .toss();



////  COMMENTED-OUT the following tests have been commented out until the following is fixed: https://github.ibm.com/CloudDataServices/dashdb-dsserver/issues/180
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ': Test 19: invalid value, " ," for COLDEL (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "LOAD_ACTION": "REPLACE",
////                      "FILE_TYPE_MODIFIERS": {
////                          "COLDEL": " ,",
////                      },   
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .expectStatus(400)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
////                        errorMessageCode: "ERROR",
////                        resultCode: "ERROR",
////                        result: []
////                    })
////        .toss();
////  
////        
////        
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 20: empty COLDEL (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "COLDEL": ""
////                    },
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
////  
////  
////  
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 21: invalid COLDEL - JSON array (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "COLDEL": ["h"]
////                      },
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
////  
////  
////  
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 22: invalid COLDEL - JSON object (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "COLDEL": {"a":"b" }
////                      },
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'Invalid delimiter. Valid delimiters can be hexadecimal values 0x00 - 0x7F, inclusive; with the exception of binary zero (0x00), line-feed (0x0A), carriage return (0x0D), space (0x20), and decimal point (0x2E). You can enter either the hexadecimal or the character value for a delimiter. Hexadecimal values must include the "0x" prefix. The delimiter cannot be part of the date or the time data format.',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
