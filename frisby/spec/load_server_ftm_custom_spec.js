//////////////////////////////////////////////////////
// ENDPOINT - POST /load/server,GET //load/{loadID} 
// TEST - test CUSTOM file type modifiers



var frisby = require('frisby');
var expect = require('expect');
var server = require('../lib/server.js');
var path = require('path');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var tableName = "TINY_TABLE";
var tableSchema = server.tabSchema();
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);

//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
})



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 1: use FILE_TYPE_MODIFIERS and CUSTOM at the same time (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "CHARDEL": "0x22",
                        "COLDEL": "0x2c",
                        "CUSTOM": [
                            "chardel0x22",
                            "coldel0x2c"
                        ]
                    }
                }
            },
            {json: true })      
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {   
                      message: 'When the CUSTOM file type modifier key is set, you cannot specify any other file type modifiers in FILE_TYPE_MODIFIERS.',
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();



////  COMMENTED-OUT the following tests have been commented out until the following is fixed: https://github.ibm.com/CloudDataServices/dashdb-dsserver/issues/184
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 2: CUSTOM is a string (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": "tiny_table.csv"
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "LOAD_ACTION": "REPLACE",
////                      "FILE_TYPE_MODIFIERS": {
////                          "CUSTOM": "hello"
////                      }
////                  }
////              },
////              {json: true })      
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {   
////                        message: 'When the CUSTOM file type modifier key is set, you cannot specify any other file type modifiers in FILE_TYPE_MODIFIERS.',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
////  
////  
////  
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 3: CUSTOM is an empty string (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": "tiny_table.csv"
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "LOAD_ACTION": "REPLACE",
////                      "FILE_TYPE_MODIFIERS": {
////                          "CUSTOM": ""
////                      }
////                  }
////              },
////              {json: true })      
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {   
////                        message: 'When the CUSTOM file type modifier key is set, you cannot specify any other file type modifiers in FILE_TYPE_MODIFIERS.',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
////  
////  
////  
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 4: CUSTOM is a JSON object (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": "tiny_table.csv"
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "LOAD_ACTION": "REPLACE",
////                      "FILE_TYPE_MODIFIERS": {
////                          "CUSTOM": {"hello" : "there"}
////                      }
////                  }
////              },
////              {json: true })      
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {   
////                        message: 'When the CUSTOM file type modifier key is set, you cannot specify any other file type modifiers in FILE_TYPE_MODIFIERS.',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
////  
////  
////  
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 5: CUSTOM is an empty JSON object (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": "tiny_table.csv"
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "LOAD_ACTION": "REPLACE",
////                      "FILE_TYPE_MODIFIERS": {
////                          "CUSTOM": {"hello" : "there"}
////                      }
////                  }
////              },
////              {json: true })      
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {   
////                        message: 'When the CUSTOM file type modifier key is set, you cannot specify any other file type modifiers in FILE_TYPE_MODIFIERS.',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
////  
////  
////  
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 6: CUSTOM is an empty array (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": "tiny_table.csv"
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "LOAD_ACTION": "REPLACE",
////                      "FILE_TYPE_MODIFIERS": {
////                          "CUSTOM": {"hello" : "there"}
////                      }
////                  }
////              },
////              {json: true })      
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {   
////                        message: 'When the CUSTOM file type modifier key is set, you cannot specify any other file type modifiers in FILE_TYPE_MODIFIERS.',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 7: test that a CUSTOM FTMK is actually recognised (COLDEL|)';
//
////////////////////////////////////////////////////////////////////////////////
var expectedStats1 = {
    numRowsRead : 12,
    numRowsLoaded : 12,
    numRowsCommitted : 12,
    numRowsSkipped : 0,
	numRowsRejected : 0,
    numRowsDeleted : 0 };
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table_coldel_pipe_v004.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                            "CUSTOM" :["COLDEL|"],
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: tableName,
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, expectedStats1);
	    			 loadUtils.verifyAllTableContents(tableSchema, tableName, server.tiny_table_expected_rows);
                 })
      .toss();
