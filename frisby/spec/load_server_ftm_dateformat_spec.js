//////////////////////////////////////////////////////
// ENDPOINT - POST /load/server,GET //load/{loadID} 
// TEST - test DATE_FORMAT
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var server = require('../lib/server.js');
var path = require('path');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates 
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var tableName = "TINY_TABLE";
var tableSchema = server.tabSchema();
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);
var loadFileNameForErrorTests = "tiny_table.csv";

//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
})



var numRows = 19;
var numRowsRead = numRows;
    numRowsLoaded = numRows,
    numRowsCommitted = numRows,
    numRowsSkipped = 0,
    numRowsDeleted = 0;




////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 1: invalid value, "hello" for DATE_FORMAT (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "DATE_FORMAT": "hello",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'LOAD started successfully.',
                      errorMessageCode: 'NONE',
                      resultCode: 'SUCCESS',
                      result: {
                          TABLE: tableName,
                          SCHEMA: tableSchema
                    }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, "", "", "", "", "", "ERROR");
                     expect(bodyJSON.result.result.WARNING).toMatch('Routine "SYSPROC.ADMIN_CMD" execution has completed, but at least one error, "SQL3192", was encountered during the execution.');
                 })
      .toss();
      


////  COMMENTED-OUT the following tests have been commented out until the following is fixed: https://github.ibm.com/CloudDataServices/dashdb-dsserver/issues/181
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 2: empty DATE_FORMAT (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "DATE_FORMAT": ""
////                    },
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'TTTT',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
////  
////  
////  
////  
////  
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 3: blank DATE_FORMAT (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "DATE_FORMAT": " "
////                    },
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'TTTT',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
////  
////  
////  
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 4: invalid DATE_FORMAT - JSON array (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "DATE_FORMAT": ["h"]
////                      },
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'TTTT',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
////  
////  
////  
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 5: invalid DATE_FORMAT - JSON object (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "DATE_FORMAT": {"a":"b" }
////                      },
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'TTTT',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();



////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 6: happy path - test file with different date values';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "date_table.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "DATE_TABLE",
                    "LOAD_ACTION": "REPLACE",
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: "DATE_TABLE",
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsDeleted, numRowsSkipped);
    				 loadUtils.verifyAllTableContents(tableSchema, "DATE_TABLE", server.date_table_expected_rows);
                 })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 7: happy path - test file with DATE_FORMAT "h/mm"';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "date_table_format.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "DATE_TABLE",
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "DATE_FORMAT": "M*YYYY*D"
                    }
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: "DATE_TABLE",
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsDeleted, numRowsSkipped);
    				 loadUtils.verifyAllTableContents(tableSchema, "DATE_TABLE", server.date_table_expected_rows);
                 })
      .toss();
