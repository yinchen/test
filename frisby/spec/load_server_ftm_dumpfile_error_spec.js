var frisby = require('frisby');
var expect = require('expect');
var server = require('../lib/server.js');
var fs = require('fs');
var path = require('path');
var FormData = require('form-data');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var tableName = "DUMPFILE_TEST_TABLE";
var tableSchema = server.tabSchema();
var user = server.userID();
var pass = server.password();

// Global setup for all tests
frisby.globalSetup({
		timeout : (30 * 1000)
})

var fileName = "dumpfile_invalid_data.csv";

var numRowsRead = 1010;
var numRowsLoaded = 10;
var numRowsCommitted = 1010;
var numRowsRejected = 1000;
var numRowsSkipped = 0;
var numRowsDeleted = 0;

var param_dump_compression_without_dump = {
	"SERVER" : {
		"SERVER_FILE_PATH" : fileName
	},
	"LOAD_PARAMETERS" : {
		"FILE_TYPE_MODIFIERS" : {
			"HAS_DATE_TYPES" : "FALSE",
			"COMPRESS_DUMPFILE" : "TRUE",
		},
		"LOAD_ACTION" : "REPLACE",
		"TABLE" : tableName,
		"SCHEMA" : tableSchema,
	},
	"LOAD_SOURCE" : "SERVER"
};

var param_dump_compression_without_dump_custom = {
	"SERVER" : {
		"SERVER_FILE_PATH" : fileName
	},
	"LOAD_PARAMETERS" : {
		"FILE_TYPE_MODIFIERS" : {
			"CUSTOM" : [ "CODEPAGE=1208", "COMPRESS_DUMPFILE=TRUE" ]
		},
		"LOAD_ACTION" : "REPLACE",
		"TABLE" : tableName,
		"SCHEMA" : tableSchema,
	},
	"LOAD_SOURCE" : "SERVER"
};

var param_dump_invalid_compression_custom = {
	"SERVER" : {
		"SERVER_FILE_PATH" : fileName
	},
	"LOAD_PARAMETERS" : {
		"FILE_TYPE_MODIFIERS" : {
			"CUSTOM" : [ "CODEPAGE=1208", "COMPRESS_DUMPFILE=NO", "DUMPFILE=TRUE" ]
		},
		"LOAD_ACTION" : "REPLACE",
		"TABLE" : tableName,
		"SCHEMA" : tableSchema,
	},
	"LOAD_SOURCE" : "SERVER"
};

var param_dump_path_in_standard = {
		"SERVER" : {
			"SERVER_FILE_PATH" : fileName
		},
		"LOAD_PARAMETERS" : {
			"FILE_TYPE_MODIFIERS" : {
				"HAS_DATE_TYPES" : "FALSE",
				"DUMPFILE" : "/mnt/blumeta0/home/" + user,
			},
			"LOAD_ACTION" : "REPLACE",
			"TABLE" : tableName,
			"SCHEMA" : tableSchema,
		},
		"LOAD_SOURCE" : "SERVER"
	};

var param_dump_path_in_custom = {
	"SERVER" : {
		"SERVER_FILE_PATH" : fileName
	},
	"LOAD_PARAMETERS" : {
		"FILE_TYPE_MODIFIERS" : {
			"CUSTOM" : [ "CODEPAGE=1208", "DUMPFILE", "DUMPFILE=/mnt/blumeta0/home/" + user ]
		},
		"LOAD_ACTION" : "REPLACE",
		"TABLE" : tableName,
		"SCHEMA" : tableSchema,
	},
	"LOAD_SOURCE" : "SERVER"
};

frisby
		.create(
				'load_server_dumpfile_spec.js: Test 1: compression is specified without dumpfile option POST /load/server/')
		.post(siteURL + loadServerEndPoint, param_dump_compression_without_dump, {
			json : true
		}).auth(user, pass).timeout(60000)
          .expectStatus(400)
          .expectHeaderContains('Content-Type',
				'json').inspectJSON().expectJSON({
			message : "COMPRESS_DUMPFILE cannot be specified without DUMPFILE",
			errorMessageCode : "ERROR",
			resultCode : "ERROR",
            result: []
		}).toss();

frisby
		.create(
				'load_server_dumpfile_spec.js: Test 2: compression is specified without dumpfile option in CUSTOM file type modifiers POST /load/server/')
		.post(siteURL + loadServerEndPoint,
				param_dump_compression_without_dump_custom, {
					json : true
				}).auth(user, pass).timeout(60000)
        .expectStatus(400)
        .expectHeaderContains('Content-Type',
				'json').inspectJSON().expectJSON({
			message : "COMPRESS_DUMPFILE cannot be specified without DUMPFILE",
			errorMessageCode : "ERROR",
			resultCode : "ERROR",
            result: []
		}).toss();

frisby
		.create(
				'load_server_dumpfile_spec.js: Test 3: compression value is invalid (neither true nor false) type modifiers POST /load/server/')
		.post(siteURL + loadServerEndPoint, param_dump_invalid_compression_custom,
				{
					json : true
				}).auth(user, pass).timeout(60000)
         .expectStatus(400)
		 .expectHeaderContains('Content-Type',
				'json').inspectJSON().expectJSON({
			message : "Invalid value specified for COMPRESS_DUMPFILE NO",
			errorMessageCode : "ERROR",
			resultCode : "ERROR",
            result: []
		}).toss();

frisby
		.create(
				'load_server_dumpfile_spec.js: Test 4: dumpfile specified as a path in standard file modifiers POST /load/server/')
		.post(siteURL + loadServerEndPoint, param_dump_path_in_standard,
				{
					json : true
				}).auth(user, pass).timeout(60000)
        .expectStatus(400)
        .expectHeaderContains('Content-Type',
				'json').inspectJSON().expectJSON({
			message : "DUMPFILE value is not correct.",
			errorMessageCode : "ERROR",
			resultCode : "ERROR",
            result: []
		}).toss();

frisby
		.create(
				'load_server_dumpfile_spec.js: Test 5: dumpfile specified as a path in custom file modifiers POST /load/server/')
		.post(siteURL + loadServerEndPoint, param_dump_path_in_custom,
				{
					json : true
				}).auth(user, pass).timeout(60000)
        .expectStatus(400)
		.expectHeaderContains('Content-Type',
				'json').inspectJSON().expectJSON({
			message : "DUMPFILE value is not correct.",
			errorMessageCode : "ERROR",
			resultCode : "ERROR",
            result: []
		}).toss();
