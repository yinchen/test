var frisby = require('frisby');
var expect = require('expect');
var server = require('../lib/server.js');
var fs = require('fs');
var path = require('path');
var FormData = require('form-data');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var tableName = "DUMPFILE_TEST_TABLE";
var tableSchema = server.tabSchema();
var user = server.userID();
var pass = server.password();

// Global setup for all tests
frisby.globalSetup({
        timeout : (30 * 1000)
})

var fileName = "dumpfile_invalid_data.csv";

var numRowsRead = 1010;
var numRowsLoaded = 10;
var numRowsCommitted = 1010;
var numRowsRejected = 1000;
var numRowsSkipped = 0;
var numRowsDeleted = 0;

var param_dump_standard_compression = {
    "SERVER" : {
        "SERVER_FILE_PATH" : fileName
    },
    "LOAD_PARAMETERS" : {
        "FILE_TYPE_MODIFIERS" : {
            "HAS_DATE_TYPES" : "FALSE",
            "DUMPFILE" : "TRUE",
        },
        "LOAD_ACTION" : "REPLACE",
        "TABLE" : tableName,
        "SCHEMA" : tableSchema,
    },
    "LOAD_SOURCE" : "SERVER"
};

var param_dump_custom_compression = {
    "SERVER" : {
        "SERVER_FILE_PATH" : fileName
    },
    "LOAD_PARAMETERS" : {
        "FILE_TYPE_MODIFIERS" : {
            "CUSTOM" : [ "CODEPAGE=1208", "DUMPFILE =  true" ]
        },
        "LOAD_ACTION" : "REPLACE",
        "TABLE" : tableName,
        "SCHEMA" : tableSchema,
    },
    "LOAD_SOURCE" : "SERVER"
};

var param_dump_standard_no_compression = {
    "SERVER" : {
        "SERVER_FILE_PATH" : fileName
    },
    "LOAD_PARAMETERS" : {
        "FILE_TYPE_MODIFIERS" : {
            "HAS_DATE_TYPES" : "FALSE",
            "DUMPFILE" : "TRUE",
            "COMPRESS_DUMPFILE" : "FALSE"
        },
        "LOAD_ACTION" : "REPLACE",
        "TABLE" : tableName,
        "SCHEMA" : tableSchema,
    },
    "LOAD_SOURCE" : "SERVER"
};

var param_dump_custom_no_compression = {
    "SERVER" : {
        "SERVER_FILE_PATH" : fileName
    },
    "LOAD_PARAMETERS" : {
        "FILE_TYPE_MODIFIERS" : {
            "CUSTOM" : [ "CODEPAGE=1208", "COMPRESS_DUMPFILE=FALSE", "DUMPFILE=TRUE" ]
        },
        "LOAD_ACTION" : "REPLACE",
        "TABLE" : tableName,
        "SCHEMA" : tableSchema,
    },
    "LOAD_SOURCE" : "SERVER"
};

var param_dump_false_standard = {
    "SERVER" : {
        "SERVER_FILE_PATH" : fileName
    },
    "LOAD_PARAMETERS" : {
        "FILE_TYPE_MODIFIERS" : {
            "HAS_DATE_TYPES" : "FALSE",
            "DUMPFILE" : "FALSE"
        },
        "LOAD_ACTION" : "REPLACE",
        "TABLE" : tableName,
        "SCHEMA" : tableSchema,
    },
    "LOAD_SOURCE" : "SERVER"
};

var param_dump_false_custom = {
    "SERVER" : {
        "SERVER_FILE_PATH" : fileName
    },
    "LOAD_PARAMETERS" : {
        "FILE_TYPE_MODIFIERS" : {
            "CUSTOM" : [ "CODEPAGE=1208", "DUMPFILE=FALSE" ]
        },
        "LOAD_ACTION" : "REPLACE",
        "TABLE" : tableName,
        "SCHEMA" : tableSchema,
    },
    "LOAD_SOURCE" : "SERVER"
};

// This test suite contains 4 scenarios:
// 1) dumpfile as standard parameter with compression
// 2) dumpfile as custom parameter with compression
// 3) dumpfile as standard parameter without compression
// 4) dumpfile as custom parameter without compression
// 5) dumpfile explicitly specified as false in standard modifiers
// 6) dumpfile explicitly specified as false in custom modifiers

frisby.create( 'load_server_dumpfile_spec.js: Test 1: dumpfile as standard parameter with compression POST /load/server/')
      .post(siteURL + loadServerEndPoint, param_dump_standard_compression, 
            { json : true })
      .auth(user, pass)
      .timeout(60000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON({
                       errorMessageCode : "NONE",
                       resultCode : "SUCCESS",
                       result : {
                           TABLE : tableName,
                           SCHEMA : tableSchema
                       }
                  })
      .afterJSON(function(json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded,
                                                  numRowsCommitted, 0, 0, "ERROR");
                     expect(bodyJSON.result.message).toBe('LOAD encountered rows that were rejected');
                     expect(bodyJSON.result.result.DUMPFILE_PATH).toBe('load_dump_files_' + bodyJSON.result.result.LOAD_ID);
                    
                     var loadDumpFile = bodyJSON.result.result.DUMPFILE_PATH + "/dump_file.zip";
                      // verify rejected rows
                     var url = siteURL + server.homeEndPoint() + loadDumpFile;
                     frisby.create( 'load_server_dumpfile_spec.js: Test 1.1: Download the dump zip file and verify rejected rows'
                                            + loadDumpFile)
                           .get(url, { encoding : null })
                           .auth(user, pass)
                           .expectStatus(200)
                           .expectHeader('Content-Type', 'application/octet-stream; charset=utf-8')
                           .expectHeader('content-disposition',
                                         'attachment; filename=dump_file.zip')
                           .after(function(err, res, body) {
                                      if (!err) {
                                          var destFile = '/tmp/dump_file.zip';
                                          fs.writeFileSync(destFile, body);
                                          var text = loadUtils.catZipFiles(destFile);
                                          verifyRejectedRows(text, 
                                                             fs.readFileSync(__dirname + '/../datasets/dumpfile_invalid_data.csv', 'utf-8'));
                                      } else {
                                          console.error(err);
                                      }
                                  })
                           .toss();
                 })
      .toss();


// dump file option is specified as custom parameter, compress dumpfile(s) by
// default
frisby.create('load_server_dumpfile_spec.js: Test 2:  dumpfile as custom parameter with compression')
      .post(siteURL + loadServerEndPoint, param_dump_custom_compression, 
            { json : true })
      .auth(user, pass)
      .timeout(60000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON({
                       errorMessageCode : "NONE",
                       resultCode : "SUCCESS",
                       result : {
                           TABLE : tableName,
                           SCHEMA : tableSchema
                       }
                   })
        .afterJSON(
                function(json) {
                    loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                    var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                    loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded,
                            numRowsCommitted, 0, 0, "ERROR");
                    expect(bodyJSON.result.message).toBe('LOAD encountered rows that were rejected');
                    expect(bodyJSON.result.result.DUMPFILE_PATH).toBe(
                            'load_dump_files_' + bodyJSON.result.result.LOAD_ID);
                    var loadDumpFile = bodyJSON.result.result.DUMPFILE_PATH + "/dump_file.zip";

                    // verify rejected rows
                    var url = siteURL + server.homeEndPoint() + loadDumpFile;
                    frisby
                            .create(
                                    'load_server_dumpfile_spec.js: Test 2.1: Download the dump zip file and verify rejected rows')
                            .get(url, { encoding : null })
                            .auth(user, pass)
                            .expectStatus(200)
                            .expectHeader('Content-Type', 'application/octet-stream; charset=utf-8')
                            .expectHeader('content-disposition',
                                    'attachment; filename=dump_file.zip')
                            .after(function(err, res, body) {
                                       if (!err) {
                                           var destFile = '/tmp/dump_file.zip';
                                           fs.writeFileSync(destFile, body);
                                           // get the aggregated content from zipped files
                                           var text = loadUtils.catZipFiles(destFile);
                                           verifyRejectedRows(text, 
                                                              fs.readFileSync(__dirname + '/../datasets/dumpfile_invalid_data.csv', 'utf-8'));
                                       } else {
                                           console.error(err);
                                       }
                                   })
                            .toss();
                })
      .toss();

// dump file option is specified as standard parameter, disable compress option
frisby
        .create(
                'load_server_dumpfile_spec.js: Test 3:  dumpfile as standard parameter without compression ')
        .post(siteURL + loadServerEndPoint, param_dump_standard_no_compression, {
            json : true
        })
        .auth(user, pass)
        .timeout(60000)
        .expectHeaderContains('Content-Type', 'json')
        .inspectJSON()
        .expectJSON({
            errorMessageCode : "NONE",
            resultCode : "SUCCESS",
            result : {
                TABLE : tableName,
                SCHEMA : tableSchema
            }
        })
        .afterJSON(
                function(json) {
                    loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                    var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                    loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded,
                            numRowsCommitted, 0, 0, "ERROR");
                    expect(bodyJSON.result.message).toBe('LOAD encountered rows that were rejected');
                    var loadId = bodyJSON.result.result.LOAD_ID;
                    var loadDumpDir = 'load_dump_files_' + loadId;
                    expect(bodyJSON.result.result.DUMPFILE_PATH).toBe(loadDumpDir);

                    var url = siteURL + server.homeEndPoint();
                    frisby
                            .create(
                                    'load_server_dumpfile_spec.js: Test 3.1: Download the dumpfiles and verify the rejected rows')
                            .get(url).auth(user, pass).expectStatus(200).afterJSON(
                                    function(json) {
                                        var result = json.result;
                                        expect(result != null).toBe(true);
                                        var list = result.split("\n");
                                        var fileListDest = [];
                                        var temp = '/tmp/' + loadDumpDir;
                                        fs.mkdirSync(temp);
                                        for (var i = 0; i < list.length; i++) {
                                            var index = list[i].indexOf(loadDumpDir + '/dump_file');
                                            if (index != -1) {
                                                var filePath = list[i].substring(index);
                                                var baseName = path.basename(filePath);
                                                var destName = temp + '/' + baseName;
                                                loadUtils
                                                        .download(url + filePath, user, pass, destName);
                                                fileListDest.push(destName);
                                            }
                                        }

                                        var text = loadUtils.combineFilesIntoString(fileListDest);
                                        verifyRejectedRows(text, fs.readFileSync(__dirname
                                                + '/../datasets/dumpfile_invalid_data.csv', 'utf-8'));
                                    }).toss();

                }).toss();

// dump file option is specified as custom parameter, disable compress option
frisby
        .create(
                'load_server_dumpfile_spec.js: Test 4:  dumpfiele as custom parameter without compression')
        .post(siteURL + loadServerEndPoint, param_dump_custom_no_compression, {
            json : true
        })
        .auth(user, pass)
        .timeout(60000)
        .expectHeaderContains('Content-Type', 'json')
        .inspectJSON()
        .expectJSON({
            errorMessageCode : "NONE",
            resultCode : "SUCCESS",
            result : {
                TABLE : tableName,
                SCHEMA : tableSchema
            }
        })
        .afterJSON(
                function(json) {
                    loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                    var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                    loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded,
                            numRowsCommitted, 0, 0, "ERROR");
                    expect(bodyJSON.result.message).toBe('LOAD encountered rows that were rejected');
                    var loadId = bodyJSON.result.result.LOAD_ID;
                    var loadDumpDir = 'load_dump_files_' + loadId;
                    expect(bodyJSON.result.result.DUMPFILE_PATH).toBe(loadDumpDir);

                    var url = siteURL + server.homeEndPoint();
                    frisby.create(
                            'load_server_dumpfile_spec.js: Test 4.1: GET /home/' + loadDumpDir)
                            .get(url).auth(user, pass).expectStatus(200).afterJSON(
                                    function(json) {
                                        var result = json.result;
                                        expect(result != null).toBe(true);
                                        var list = result.split("\n");
                                        var fileListDest = [];
                                        var temp = '/tmp/' + loadDumpDir;
                                        fs.mkdirSync(temp);
                                        for (var i = 0; i < list.length; i++) {
                                            var index = list[i].indexOf(loadDumpDir + '/dump_file');
                                            if (index != -1) {
                                                var filePath = list[i].substring(index);
                                                var baseName = path.basename(filePath);
                                                var destName = temp + '/' + baseName;
                                                loadUtils
                                                        .download(url + filePath, user, pass, destName);
                                                fileListDest.push(destName);
                                            }
                                        }
                                        
                                        var text = loadUtils.combineFilesIntoString(fileListDest);
                                        verifyRejectedRows(text, fs.readFileSync(__dirname
                                                + '/../datasets/dumpfile_invalid_data.csv', 'utf-8'));
                                    }).toss();

                }).toss();

frisby
        .create(
                'load_server_dumpfile_spec.js: Test 5:  dumpfile explicitly specified as false in standard modifiers')
        .post(siteURL + loadServerEndPoint, param_dump_false_standard, {
            json : true
        })
        .auth(user, pass)
        .timeout(60000)
        .expectHeaderContains('Content-Type', 'json')
        .inspectJSON()
        .expectJSON({
            errorMessageCode : "NONE",
            resultCode : "SUCCESS",
            result : {
                TABLE : tableName,
                SCHEMA : tableSchema
            }
        })
        .afterJSON(
                function(json) {
                    loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                    var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                    loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded,
                            numRowsCommitted, 0, 0, "ERROR");
                    expect(bodyJSON.result.message).toBe('LOAD encountered rows that were rejected');
                    expect(bodyJSON.result.result.DUMPFILE_PATH).toBe(undefined);
                }).toss();


frisby
        .create(
                'load_server_dumpfile_spec.js: Test 6:  dumpfile explicitly specified as false in custom modifiers')
        .post(siteURL + loadServerEndPoint, param_dump_false_custom, {
            json : true
        })
        .auth(user, pass)
        .timeout(60000)
        .expectHeaderContains('Content-Type', 'json')
        .inspectJSON()
        .expectJSON({
            errorMessageCode : "NONE",
            resultCode : "SUCCESS",
            result : {
                TABLE : tableName,
                SCHEMA : tableSchema
            }
        })
        .afterJSON(
                function(json) {
                    loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                    var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                    loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded,
                            numRowsCommitted, 0, 0, "ERROR");
                    expect(bodyJSON.result.message).toBe('LOAD encountered rows that were rejected');
                    expect(bodyJSON.result.result.DUMPFILE_PATH).toBe(undefined);
                }).toss();


function verifyRejectedRows(rejectedRowText, dataFileText) {
    var expectedRecords = loadUtils.toArray(dataFileText);
    var actualRecords = loadUtils.toArray(rejectedRowText);
    expect(actualRecords.length).toBe(numRowsRejected);
    actualRecords.forEach(function(actualRecord) {
        expect(expectedRecords.indexOf(actualRecord) !== -1).toBe(true);
    });
}
