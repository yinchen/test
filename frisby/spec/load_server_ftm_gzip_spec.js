//////////////////////////////////////////////////////
// ENDPOINT - POST /load/server,GET //load/{loadID}
// TEST - test GZIP file type modifier
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var expect = require('expect');
var server = require('../lib/server.js');
var fs = require('fs');
var path = require('path');
var FormData = require('form-data');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var tableName = "TINY_TABLE";
var tableSchema = server.tabSchema();
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);

//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
})


////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 1: GZIP FTM and HAS_HEADER_ROW are blocked';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "FILE_TYPE_MODIFIERS": {
                        "CUSTOM": [ "GZIP" ]
                    },
                "HAS_HEADER_ROW":"TRUE"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      message: 'The file type modifier GZIP and option HAS_HEADER_ROW may not be specified together',
                      result: []
                  })
      .toss();
