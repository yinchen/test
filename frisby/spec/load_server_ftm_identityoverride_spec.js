//////////////////////////////////////////////////////
// ENDPOINT - POST /load/server,GET //load/{loadID} 
// TEST - test IDENTITYOVERRIDE

var frisby = require('frisby');
var server = require('../lib/server.js');
var path = require('path');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates 
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var tableSchema = server.tabSchema();
var tableName = server.tabName();
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);
var loadFileNameForErrorTests = "tiny_table.csv";

var identOverrifeFile = "identityoverridetable.csv";
var identGenAlwaysTableName = "IDENTITYALWAYSTABLE",
    identGenByDefaultTableName = "IDENTITYDEFAULTTABLE";

//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
})

////  COMMENTED-OUT the following tests have been commented out until the following is fixed: https://github.ibm.com/CloudDataServices/dashdb-dsserver/issues/182
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 1: empty IDENTITYOVERRIDE (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "IDENTITYOVERRIDE": ""
////                    },
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'TTTT',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 2: blank IDENTITYOVERRIDE (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "FILE_TYPE_MODIFIERS": {
                        "IDENTITYOVERRIDE": " "
                  },
                }
            },
            {json: true })
      .auth(user, pass)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'The value for JSON input key IDENTITYOVERRIDE must be either "true" or "false".',
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 3: invalid IDENTITYOVERRIDE - JSON array (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "FILE_TYPE_MODIFIERS": {
                        "IDENTITYOVERRIDE": ["h"]
                    },
                }
            },
            {json: true })
      .auth(user, pass)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'The value for JSON input key IDENTITYOVERRIDE must be either "true" or "false".',
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 4: invalid IDENTITYOVERRIDE - JSON object (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "FILE_TYPE_MODIFIERS": {
                        "IDENTITYOVERRIDE": {"a":"b" }
                    },
                }
            },
            {json: true })
      .auth(user, pass)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'The value for JSON input key IDENTITYOVERRIDE must be either "true" or "false".',
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 5: invalid IDENTITYOVERRIDE - "blah" (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "FILE_TYPE_MODIFIERS": {
                        "IDENTITYOVERRIDE": "blah" 
                    },
                }
            },
            {json: true })
      .auth(user, pass)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'The value for JSON input key IDENTITYOVERRIDE must be either "true" or "false".',
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 6: GEN ALWAYS IDENTITY col, IDENTITYOVERRIDE:true';
//
////////////////////////////////////////////////////////////////////////////////
var expectedStats1 = {
    numRowsRead : 5,
    numRowsLoaded :5,
    numRowsCommitted : 5,
    numRowsSkipped : 0,
	numRowsRejected : 0,
    numRowsDeleted : 0 };

frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH":identOverrifeFile 
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": identGenAlwaysTableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "IDENTITYOVERRIDE": "true",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
          {
              message: "LOAD started successfully.",
              errorMessageCode: "NONE",
              resultCode: "SUCCESS",
              result: {
                  TABLE: identGenAlwaysTableName,
                  SCHEMA: tableSchema
              }
          })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, expectedStats1);
                     loadUtils.verifyAllTableContents(tableSchema, identGenAlwaysTableName, server.identityoverridetable_expected_rows);

                 })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 7: GEN ALWAYS IDENTITY col, IDENTITYOVERRIDE:TRUE';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH":identOverrifeFile 
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": identGenAlwaysTableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "IDENTITYOVERRIDE": "TRUE",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
          {
              message: "LOAD started successfully.",
              errorMessageCode: "NONE",
              resultCode: "SUCCESS",
              result: {
                  TABLE: identGenAlwaysTableName,
                  SCHEMA: tableSchema
              }
          })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, expectedStats1);
                     loadUtils.verifyAllTableContents(tableSchema, identGenAlwaysTableName, server.identityoverridetable_expected_rows);

                 })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 8: GEN ALWAYS IDENTITY col, IDENTITYOVERRIDE:false (nagative test)';
//
////////////////////////////////////////////////////////////////////////////////
var expectedStats2 = {
    numRowsRead : 5,
    numRowsLoaded : 0,
    numRowsCommitted : 5,
    numRowsSkipped : 0,
	numRowsRejected : 5,
    numRowsDeleted : 0,
    errorMessageCode : "ERROR"};

frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH":identOverrifeFile 
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": identGenAlwaysTableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "IDENTITYOVERRIDE": "false",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
          {
              message: "LOAD started successfully.",
              errorMessageCode: "NONE",
              resultCode: "SUCCESS",
              result: {
                  TABLE: identGenAlwaysTableName,
                  SCHEMA: tableSchema
              }
          })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, expectedStats2);
                     loadUtils.verifyAllTableContents(tableSchema, identGenAlwaysTableName, []);

                 })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 9: GEN ALWAYS IDENTITY col, IDENTITYOVERRIDE:FALSE (nagative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH":identOverrifeFile 
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": identGenAlwaysTableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "IDENTITYOVERRIDE": "false",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
          {
              message: "LOAD started successfully.",
              errorMessageCode: "NONE",
              resultCode: "SUCCESS",
              result: {
                  TABLE: identGenAlwaysTableName,
                  SCHEMA: tableSchema
              }
          })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, expectedStats2);
                     loadUtils.verifyAllTableContents(tableSchema, identGenAlwaysTableName, []);
                 })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 10: GEN ALWAYS IDENTITY col, IDENTITYOVERRIDE not present (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH":identOverrifeFile 
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": identGenAlwaysTableName,
                    "LOAD_ACTION": "REPLACE",
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
          {
              message: "LOAD started successfully.",
              errorMessageCode: "NONE",
              resultCode: "SUCCESS",
              result: {
                  TABLE: identGenAlwaysTableName,
                  SCHEMA: tableSchema
              }
          })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, expectedStats2);
                     loadUtils.verifyAllTableContents(tableSchema, identGenAlwaysTableName, []);

                 })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 11: GEN BY DEFAULT IDENTITY col, IDENTITYOVERRIDE:true (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
var expectedStats3 = {
    numRowsRead : "",
    numRowsLoaded : "",
    numRowsCommitted : "",
    numRowsSkipped : "",
	numRowsRejected : "",
    numRowsDeleted : "",
    errorMessageCode : "ERROR"};

frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH":identOverrifeFile 
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": identGenByDefaultTableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "IDENTITYOVERRIDE": "TRUE",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
          {
              message: "LOAD started successfully.",
              errorMessageCode: "NONE",
              resultCode: "SUCCESS",
              result: {
                  TABLE: identGenByDefaultTableName,
                  SCHEMA: tableSchema
              }
          })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, expectedStats3);
                     expect(bodyJSON.result.result.WARNING).toMatch('Routine "SYSPROC.ADMIN_CMD" execution has completed, but at least one error, "SQL3526", was encountered during the execution.');
                 })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 12: GEN BY DEFAULT IDENTITY col, IDENTITYOVERRIDE:false (rows load fine)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH":identOverrifeFile 
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": identGenByDefaultTableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "IDENTITYOVERRIDE": "false",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
          {
              message: "LOAD started successfully.",
              errorMessageCode: "NONE",
              resultCode: "SUCCESS",
              result: {
                  TABLE: identGenByDefaultTableName,
                  SCHEMA: tableSchema
              }
          })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, expectedStats1);
                     loadUtils.verifyAllTableContents(tableSchema, identGenByDefaultTableName, server.identityoverridetable_expected_rows);

                 })
      .toss();
