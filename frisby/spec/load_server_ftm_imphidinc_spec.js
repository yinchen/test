//////////////////////////////////////////////////////
// ENDPOINT - POST /load/server,GET //load/{loadID} 
// TEST - test IMPLICITLYHIDDENINCLUDE

var frisby = require('frisby');
var server = require('../lib/server.js');
var path = require('path');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates 
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var tableSchema = server.tabSchema();
var tableName = server.tabName();
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);
var loadFileNameForErrorTests = "tiny_table.csv";



//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
})

//////////////////////////////////////////////////////////////////////////
//
var msg = myname + ' : Test 1: table has hidden column - IMPLICITLYHIDDENINCLUDE:true';
//
////////////////////////////////////////////////////////////////////////////////
var numRows = 4;
var numRowsRead = numRows,
    numRowsLoaded = numRows,
    numRowsCommitted = numRows,
    numRowsSkipped = 0,
    numRowsDeleted = 0;

frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "small_3_int_cols.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "HIDDENCOLTABLE",
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "IMPLICITLYHIDDENINCLUDE": "true",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
          {
              message: "LOAD started successfully.",
              errorMessageCode: "NONE",
              resultCode: "SUCCESS",
              result: {
                  TABLE: "HIDDENCOLTABLE",
                  SCHEMA: tableSchema
              }
          })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, 0, 0);
                 })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 2: table has hidden column - IMPLICITLYHIDDENINCLUDE:TRUE';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "small_3_int_cols.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "HIDDENCOLTABLE",
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "IMPLICITLYHIDDENINCLUDE": "TRUE",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
          {
              message: "LOAD started successfully.",
              errorMessageCode: "NONE",
              resultCode: "SUCCESS",
              result: {
                  TABLE: "HIDDENCOLTABLE",
                  SCHEMA: tableSchema
              }
          })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, 0, 0);
                 })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 3: table has hidden column - IMPLICITLYHIDDENINCLUDE:false';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "small_3_int_cols.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "HIDDENCOLTABLE",
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "IMPLICITLYHIDDENINCLUDE": "false",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
          {
              message: "LOAD started successfully.",
              errorMessageCode: "NONE",
              resultCode: "SUCCESS",
              result: {
                  TABLE: "HIDDENCOLTABLE",
                  SCHEMA: tableSchema
              }
          })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     // loadUtils.validateLoadResult(bodyJSON, "", "", "", "", "", "ERROR");
                     expect(bodyJSON.result.result.WARNING).toMatch('Routine "SYSPROC.ADMIN_CMD" execution has completed, but at least one error, "SQL2437", was encountered during the execution. More information is available.. SQLCODE=20397, SQLSTATE=01H52, DRIVER=4.22.36');
                 })
      .toss();
   


//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 4: table has hidden column - IMPLICITLYHIDDENINCLUDE:FALSE (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "small_3_int_cols.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "HIDDENCOLTABLE",
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "IMPLICITLYHIDDENINCLUDE": "FALSE",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
          {
              message: "LOAD started successfully.",
              errorMessageCode: "NONE",
              resultCode: "SUCCESS",
              result: {
                  TABLE: "HIDDENCOLTABLE",
                  SCHEMA: tableSchema
              }
          })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     // loadUtils.validateLoadResult(bodyJSON, "", "", "", "", "", "ERROR");
                     expect(bodyJSON.result.result.WARNING).toMatch('Routine "SYSPROC.ADMIN_CMD" execution has completed, but at least one error, "SQL2437", was encountered during the execution. More information is available.. SQLCODE=20397, SQLSTATE=01H52, DRIVER=4.22.36');
                 })
      .toss();


 
//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 5: table has hidden column - IMPLICITLYHIDDENINCLUDE not speficied (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "small_3_int_cols.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "HIDDENCOLTABLE",
                    "LOAD_ACTION": "REPLACE",
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
          {
              message: "LOAD started successfully.",
              errorMessageCode: "NONE",
              resultCode: "SUCCESS",
              result: {
                  TABLE: "HIDDENCOLTABLE",
                  SCHEMA: tableSchema
              }
          })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     // loadUtils.validateLoadResult(bodyJSON, "", "", "", "", "", "ERROR");
                     expect(bodyJSON.result.result.WARNING).toMatch('Routine "SYSPROC.ADMIN_CMD" execution has completed, but at least one error, "SQL2437", was encountered during the execution. More information is available.. SQLCODE=20397, SQLSTATE=01H52, DRIVER=4.22.36');
                 })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 6: table has no implicitly hidden columns and IMPLICITLYHIDDENINCLUDE:true is used (load proceeds normally)';
//
////////////////////////////////////////////////////////////////////////////////
var numRows2 = 12;
var numRowsRead2 = numRows2,
    numRowsLoaded2 = numRows2,
    numRowsCommitted2 = numRows2,
    numRowsSkipped2 = 0,
    numRowsDeleted2 = 0;

frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE",
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "IMPLICITLYHIDDENINCLUDE": "true",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
          {
              message: "LOAD started successfully.",
              errorMessageCode: "NONE",
              resultCode: "SUCCESS",
              result: {
                  TABLE: "TINY_TABLE",
                  SCHEMA: tableSchema
              }
          })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, numRowsRead2, numRowsLoaded2, numRowsCommitted2, 0, 0);
                 })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 7: table has no implicitly hidden columns and IMPLICITLYHIDDENINCLUDE:false is used (load proceeds normally)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE",
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "IMPLICITLYHIDDENINCLUDE": "false",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
          {
              message: "LOAD started successfully.",
              errorMessageCode: "NONE",
              resultCode: "SUCCESS",
              result: {
                  TABLE: "TINY_TABLE",
                  SCHEMA: tableSchema
              }
          })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, numRowsRead2, numRowsLoaded2, numRowsCommitted2, 0, 0);
                 })
      .toss();



////  COMMENTED-OUT the following tests have been commented out until the following is fixed: https://github.ibm.com/CloudDataServices/dashdb-dsserver/issues/182
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 8: empty IMPLICITLYHIDDENINCLUDE (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "IMPLICITLYHIDDENINCLUDE": ""
////                    },
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'TTTT',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 9: blank IMPLICITLYHIDDENINCLUDE (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "FILE_TYPE_MODIFIERS": {
                        "IMPLICITLYHIDDENINCLUDE": " "
                  },
                }
            },
            {json: true })
      .auth(user, pass)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'The value for JSON input key IMPLICITLYHIDDENINCLUDE must be either "true" or "false".',
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 10: invalid IMPLICITLYHIDDENINCLUDE - JSON array (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "FILE_TYPE_MODIFIERS": {
                        "IMPLICITLYHIDDENINCLUDE": ["h"]
                    },
                }
            },
            {json: true })
      .auth(user, pass)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'The value for JSON input key IMPLICITLYHIDDENINCLUDE must be either "true" or "false".',
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 11: invalid IMPLICITLYHIDDENINCLUDE - JSON object (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "FILE_TYPE_MODIFIERS": {
                        "IMPLICITLYHIDDENINCLUDE": {"a":"b" }
                    },
                }
            },
            {json: true })
      .auth(user, pass)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'The value for JSON input key IMPLICITLYHIDDENINCLUDE must be either "true" or "false".',
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ' : Test 12: invalid IMPLICITLYHIDDENINCLUDE - "blah" (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "FILE_TYPE_MODIFIERS": {
                        "IMPLICITLYHIDDENINCLUDE": "blah" 
                    },
                }
            },
            {json: true })
      .auth(user, pass)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'The value for JSON input key IMPLICITLYHIDDENINCLUDE must be either "true" or "false".',
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();
 


