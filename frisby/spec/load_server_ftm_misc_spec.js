//////////////////////////////////////////////////////
// ENDPOINT - POST /load/server,GET //load/{loadID}
// TEST - miscellaneous tests for file type modifiers
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var server = require('../lib/server.js');
var path = require('path');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates 
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var tableName = "TINY_TABLE";
var tableSchema = server.tabSchema();
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);

//Global setup for all tests
frisby.globalSetup({
    timeout: (120 * 1000)
})

var numRows = 12;
var  numRowsRead = numRows, 
     numRowsLoaded = numRows, 
     numRowsComitted = numRows, 
     numRowsDeleted = 0, 
     numRowsSkipped = 0;



////  COMMENTED-OUT the following tests have been commented out until the following is fixed: https://github.ibm.com/CloudDataServices/dashdb-dsserver/issues/167
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  msg  = myname + ': Test 1: FILE_TYPE_MODIFIERS is empty JSON object (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint,
////              {
////                  "LOAD_SOURCE" : "SERVER",
////                  "SERVER" : {
////                      "SERVER_FILE_PATH": "tiny_table.csv"
////                  },
////                  "LOAD_PARAMETERS" : {
////                      "SCHEMA" : tableSchema,
////                      "TABLE" : tableName,
////                      "FILE_TYPE_MODIFIERS": {}
////                  }
////              },
////              {json : true})
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(60000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: "jhkjhk",
////                        errorMessageCode : "ERROR",
////                        resultCode : "ERROR",
////                        result : []
////                    })
////        .afterJSON(function (json) {
////                       loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
////                       var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
////                       loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsComitted, numRowsDeleted, numRowsSkipped);
////                   })
////       .toss();
////  
////  
////  
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  msg  = myname + ': Test 2: FILE_TYPE_MODIFIERS is JSON array (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint,
////              {
////                  "LOAD_SOURCE" : "SERVER",
////                  "SERVER" : {
////                      "SERVER_FILE_PATH": "tiny_table.csv"
////                  },
////                  "LOAD_PARAMETERS" : {
////                      "SCHEMA" : tableSchema,
////                      "TABLE" : tableName,
////                      "FILE_TYPE_MODIFIERS": [ "hello" ]
////                  }
////              },
////              {json : true})
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(60000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: "jhkjhk",
////                        errorMessageCode : "ERROR",
////                        resultCode : "ERROR",
////                        result : []
////                    })
////       .toss();
////  
////  
////  
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  msg  = myname + ': Test 3: FILE_TYPE_MODIFIERS is a string (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint,
////              {
////                  "LOAD_SOURCE" : "SERVER",
////                  "SERVER" : {
////                      "SERVER_FILE_PATH": "tiny_table.csv"
////                  },
////                  "LOAD_PARAMETERS" : {
////                      "SCHEMA" : tableSchema,
////                      "TABLE" : tableName,
////                      "FILE_TYPE_MODIFIERS":  "hello" 
////                  }
////              },
////              {json : true})
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(60000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: "jhkjhk",
////                        errorMessageCode : "ERROR",
////                        resultCode : "ERROR",
////                        result : []
////                    })
////       .toss();
////  
////  
////  
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  msg  = myname + ': Test 4: FILE_TYPE_MODIFIERS is a number (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint,
////              {
////                  "LOAD_SOURCE" : "SERVER",
////                  "SERVER" : {
////                      "SERVER_FILE_PATH": "tiny_table.csv"
////                  },
////                  "LOAD_PARAMETERS" : {
////                      "SCHEMA" : tableSchema,
////                      "TABLE" : tableName,
////                      "FILE_TYPE_MODIFIERS": 12
////                  }
////              },
////              {json : true})
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(60000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: "jhkjhk",
////                        errorMessageCode : "ERROR",
////                        resultCode : "ERROR",
////                        result : []
////                    })
////       .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg  = myname + ': Test 5: FILE_TYPE_MODIFIERS is JSON object containing unfamiliar keys';
//
////////////////////////////////////////////////////////////////////////////////
var expectedStats1 = {
    numRowsRead : 12,
    numRowsLoaded : 12,
    numRowsCommitted : 12,
    numRowsSkipped : 0,
	numRowsRejected : 0,
    numRowsDeleted : 0 };
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE" : tableName,
                    "FILE_TYPE_MODIFIERS": {
                        "john": "paul",
                        "george": "ringo"
                    }
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(200)
      .timeout(60000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'LOAD started successfully.',
                      errorMessageCode : "NONE",
                      resultCode : "SUCCESS",
                      result: {
                          TABLE: server.tabName,
                          SCHEMA: server.tabSchema,
                          LOAD_STATUS: 'RUNNING'
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, expectedStats1);
                 })
     .toss();


