//////////////////////////////////////////////////////
// ENDPOINT - POST /load/server,GET //load/{loadID} 
// TEST - test TIME_FORMAT
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var server = require('../lib/server.js');
var path = require('path');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates 
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var tableName = "TINY_TABLE";
var tableSchema = server.tabSchema();
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);
var loadFileNameForErrorTests = "tiny_table.csv";

//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
})


var numRows = 11;
var numRowsRead = numRows;
    numRowsLoaded = numRows,
    numRowsCommitted = numRows,
    numRowsSkipped = 0,
    numRowsDeleted = 0;




////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 1: invalid value, "hello" for TIME_FORMAT (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": loadFileNameForErrorTests
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "TIME_FORMAT": "hello",
                    },   
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'LOAD started successfully.',
                      errorMessageCode: 'NONE',
                      resultCode: 'SUCCESS',
                      result: {
                          TABLE: tableName,
                          SCHEMA: tableSchema
                    }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     // loadUtils.validateLoadResult(bodyJSON, "", "", "", "", "", "ERROR");
                     expect(bodyJSON.result.result.WARNING).toMatch('Routine "SYSPROC.ADMIN_CMD" execution has completed, but at least one error, "SQL3192", was encountered during the execution. More information is available.. SQLCODE=20397, SQLSTATE=01H52, DRIVER=4.22.36');
                 })
      .toss();
      


////  COMMENTED-OUT the following tests have been commented out until the following is fixed: https://github.ibm.com/CloudDataServices/dashdb-dsserver/issues/180
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 2: empty TIME_FORMAT (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "TIME_FORMAT": ""
////                    },
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'TTTT',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
////  
////  
////  
////  
////  
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 3: blank TIME_FORMAT (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "TIME_FORMAT": " "
////                    },
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'TTTT',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
////  
////  
////  
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 4: invalid TIME_FORMAT - JSON array (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "TIME_FORMAT": ["h"]
////                      },
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'TTTT',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
////  
////  
////  
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ' : Test 5: invalid TIME_FORMAT - JSON object (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": loadFileNameForErrorTests
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "FILE_TYPE_MODIFIERS": {
////                          "TIME_FORMAT": {"a":"b" }
////                      },
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'TTTT',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();



////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 6: happy path - test file with different time values';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "time_table.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TIME_TABLE",
                    "LOAD_ACTION": "REPLACE",
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: "TIME_TABLE",
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsDeleted, numRowsSkipped);
    				 loadUtils.verifyAllTableContents(tableSchema, "TIME_TABLE", server.time_table_expected_rows);
                 })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 7: happy path - test file with TIME_FORMAT "h/mm"';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "time_table_format.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TIME_TABLE",
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                        "TIME_FORMAT": "h/mm"
                    }
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: "TIME_TABLE",
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsDeleted, numRowsSkipped);
    				 loadUtils.verifyAllTableContents(tableSchema, "TIME_TABLE", server.time_table_expected_rows);
                 })
      .toss();
