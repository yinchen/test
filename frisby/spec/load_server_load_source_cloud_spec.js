//////////////////////////////////////////////////////
// ENDPOINT - POST:/load/server,GET/load/id 
// TEST - test for LOAD_SOURCE and CLOUD options
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var server = require('../lib/server.js');
var path = require('path');
var loadUtils = require('../lib/loadutils.js');
var msg;

// Ignore rejected certificates 
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var loadServerURL = siteURL + loadServerEndPoint;
var tableName = "order_details";
var tableSchema = server.tabSchema();
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);
var nodeType = server.nodeType(); // SMP or MPP


//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
})


var randomAuth1 = "user" + Date.now();
var randomAuth2 = "password" + Date.now();
var expectedStatsAuthErr = {
    numRowsRead: "",
    numRowsLoaded: "",
    numRowsCommitted: "",
    numRowsSkipped: "",
    numRowsRejected: "",
    numRowsDeleted: "",
    errorMessageCode: "ERROR"
}
// for some reason on MPP we get a blank for the num roes*.

var nrows = ( nodeType == 'MPP' ? '':0);
var expectedStatsFileNotFoundS3 = { 
    numRowsRead: nrows,
    numRowsLoaded: nrows,
    numRowsCommitted: nrows,
    numRowsSkipped: nrows,
    numRowsRejected: nrows,
    numRowsDeleted: nrows,
    errorMessageCode: "ERROR",
    WARNING: ( nodeType == 'MPP' ) ? 
            'Routine \"SYSPROC.ADMIN_CMD\" execution has completed, but at least one error, \"SQL2036\", was encountered during the execution. More information is available.. SQLCODE=20397, SQLSTATE=01H52, DRIVER=4.22.36' :
            'At least one warning message was encountered during LOAD processing..',
  loadLogFileSqlCodeMsg: (( nodeType == 'MPP' ) ?
      {
         "MESSAGE": "A parameter specifying a filename or path is not valid.",
         "SQLCODE": "SQL3025N"
       } :
       {
           "MESSAGE": 'The path for the file, named pipe, or device "s3" is not valid.',
		   "SQLCODE": "SQL2036N"
       } )
}
var expectedStatsFileNotFoundSoftLayer = {
    numRowsRead: nrows,
    numRowsLoaded: nrows,
    numRowsCommitted: nrows,
    numRowsSkipped: nrows,
    numRowsRejected: nrows,
    numRowsDeleted: nrows,
    WARNING: ( nodeType == 'MPP' ) ? 
            'Routine \"SYSPROC.ADMIN_CMD\" execution has completed, but at least one error, \"SQL2036\", was encountered during the execution. More information is available.. SQLCODE=20397, SQLSTATE=01H52, DRIVER=4.22.36' :
            'At least one warning message was encountered during LOAD processing..',
    errorMessageCode: "ERROR",
  loadLogFileSqlCodeMsg: (( nodeType == 'MPP' ) ?
      {
         "MESSAGE": "A parameter specifying a filename or path is not valid.",
         "SQLCODE": "SQL3025N"
       } :
       {
           "MESSAGE": 'The path for the file, named pipe, or device "softlayer" is not valid.',
		   "SQLCODE": "SQL2036N"
       } )
}

var expectedStats = {
    numRowsRead: 12,
    numRowsLoaded: 12,
    numRowsCommitted: 12,
    numRowsSkipped: 0,
    numRowsRejected: 0,
    numRowsDeleted: 0,
    errorMessageCode: "NONE"
}
/*
 * general tests for the JSON object for load from cloud passed to the API
 */

//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 1: CLOUD defined but missing LOAD_SOURCE';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "CLOUD": {
                    "PROVIDER": "softlayer",
                    "ENDPOINT": server.swiftEndpointUrl(),
                    "AUTH1": "fakeid",
                    "AUTH2": "fakepassword",
                    "BUCKET": "fakebucket",
                    "FILE_PATH": "fakefile"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            { json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Parameter LOAD_SOURCE was not specified',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .inspectJSON()
      .toss();


      
//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 2a: LOAD_SOURCE is CLOUD, but missing CLOUD parameter'
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE": "CLOUD",
                "LOAD_PARAMETERS": {
                    "SCHEMA": "BLUAMIN",
                    "TABLE": "TINY_TABLE"
                }
            },
            { json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Parameter CLOUD was not specified',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .inspectJSON()
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 2b: CLOUD is a string (negative test)';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE": "CLOUD",
                "CLOUD":"blah",
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            { json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'The value for key: [CLOUD] was not a JSONObject',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .inspectJSON()
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 2c: CLOUD is a JSON array (negative test)';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE": "CLOUD",
                "CLOUD":["blah"],
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            { json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'The value for key: [CLOUD] was not a JSONObject',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .inspectJSON()
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 2d: CLOUD is an empty JSON object (negative test)';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE": "CLOUD",
                "CLOUD":{},
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            { json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Invlaid JSON object provided for load from Cloud.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .inspectJSON()
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 2e: CLOUD does not contain any required parameters (negative test)';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE": "CLOUD",
                "CLOUD":{"blah": "bleh", "blib": "blob"},
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            { json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'blib is not a valid key for CLOUD object.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .inspectJSON()
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 3a: PROVIDER contains invalid value';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayet",
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "BUCKET": "fakebucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'PROVIDER must be either S3 or softlayer',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();

//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 3b: PROVIDER is a number';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": 1,
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "BUCKET": "fakebucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'PROVIDER must be a string',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();

//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 3c: PROVIDER is a json object';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": { "NAME": "softlayer" },
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "BUCKET": "fakebucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'PROVIDER must be a string',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 3d: PROVIDER value is blank';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": " ",
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "BUCKET": "fakebucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'PROVIDER must be either S3 or softlayer',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 3e: PROVIDER value is empty';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "",
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "BUCKET": "fakebucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'PROVIDER must be either S3 or softlayer',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 3f: missing PROVIDER';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "BUCKET": "fakebucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'PROVIDER key not found.',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 4a: ENDPOINT is a number';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": 1,
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "BUCKET": "fakebucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'ENDPOINT must be a string',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 4b: ENDPOINT is a JSON object';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": { "URL": "https://tor01.objectstorage.softlayer.net/auth/v1.0" },
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "BUCKET": "fakebucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'ENDPOINT must be a string',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 4c: ENDPOINT is empty';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": "",
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "BUCKET": "fakebucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'ENDPOINT must contain a value',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 4d: ENDPOINT is blank';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": " ",
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "BUCKET": "fakebucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'ENDPOINT must contain a value',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 4e: ENDPOINT missing';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "BUCKET": "fakebucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'ENDPOINT key not found.',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 5a: AUTH1 is a number';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH1": 1234,
            "AUTH2": "fakepassword",
            "BUCKET": "fakebucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'AUTH1 must be a string',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 5b: AUTH1 is a JSON object';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH1": { "value": "blah"},
            "AUTH2": "fakepassword",
            "BUCKET": "fakebucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'AUTH1 must be a string',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 5c: AUTH1 is empty';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH1": "",
            "AUTH2": "fakepassword",
            "BUCKET": "fakebucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'AUTH1 must contain a value',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();


//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 5d: AUTH1 is blank';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH1": " ",
            "AUTH2": "fakepassword",
            "BUCKET": "fakebucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'AUTH1 must contain a value',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 5e: AUTH1 is missing';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH2": "fakepassword",
            "BUCKET": "fakebucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'AUTH1 key not found.',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 6a: AUTH2 is a number';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH1": "fakeid",
            "AUTH2": 1234,
            "BUCKET": "fakebucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'AUTH2 must be a string',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 6b: AUTH2 is a JSON object';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH1": "fakepassword",
            "AUTH2": { "value": "blah"},
            "BUCKET": "fakebucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'AUTH2 must be a string',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 6c: AUTH2 is empty';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH1": "fakeid",
            "AUTH2": "",
            "BUCKET": "fakebucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'AUTH2 must contain a value',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 6d: AUTH2 is blank';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH1": "fakeid",
            "AUTH2": "",
            "BUCKET": "fakebucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'AUTH2 must contain a value',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 6e: AUTH2 is missing';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH1": "fakeid",
            "BUCKET": "fakebucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'AUTH2 key not found.',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 7a: BUCKET is a number';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "BUCKET": 1234,
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'BUCKET must be a string',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 7b: BUCKET is a JSON object';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "BUCKET": {"value": "blah"},
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'BUCKET must be a string',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 7c: BUCKET is empty';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": "https://tor01.objectstorage.softlayer.net/auth/v1.0",
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "BUCKET": "",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'BUCKET must contain a value',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 7d: BUCKET is blank';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": "https://tor01.objectstorage.softlayer.net/auth/v1.0",
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "BUCKET": " ",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'BUCKET must contain a value',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 7e: BUCKET is missing';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": "https://tor01.objectstorage.softlayer.net/auth/v1.0",
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'BUCKET key not found.',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();




//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 8a: FILE_PATH is a number';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "BUCKET": "fakebucket",
            "FILE_PATH": 1234
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'FILE_PATH must be a string',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 8b: FILE_PATH is a JSON object';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "BUCKET": "fakebucket",
            "FILE_PATH": {"value": "blah"}
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'FILE_PATH must be a string',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 8c: FILE_PATH is empty';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": "https://tor01.objectstorage.softlayer.net/auth/v1.0",
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "BUCKET": "fakebucket",
            "FILE_PATH": ""
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'FILE_PATH must contain a value',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 8d: FILE_PATH is blank';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": "https://tor01.objectstorage.softlayer.net/auth/v1.0",
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "BUCKET": "fakefile",
            "FILE_PATH": " "
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'FILE_PATH must contain a value',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 8e: FILE_PATH is missing';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": "https://tor01.objectstorage.softlayer.net/auth/v1.0",
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "BUCKET": "bucket"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'FILE_PATH key not found.',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();




//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 9: HAS_HEADER_ROW and CLOUD type cannot be used together';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": "https://tor01.objectstorage.softlayer.net/auth/v1.0",
            "AUTH1": "fakeid",
            "AUTH2": "fakepassword",
            "BUCKET": "bucket",
            "FILE_PATH": "fakefile"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE",
            "HAS_HEADER_ROW": "TRUE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'Load parameter HAS_HEADER_ROW and load source type CLOUD cannot be specified together',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 10a: invalid url (s3)';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(siteURL + loadServerEndPoint,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "s3",
            "ENDPOINT": "invalid.s3.url.com",
            "AUTH1": server.s3Auth1(),
            "AUTH2": server.s3Auth2(),
            "BUCKET": server.s3Bucket(),
            "FILE_PATH": "dashdb_load/tiny_table.csv.gz"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(200)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: "LOAD started successfully.",
        errorMessageCode: "NONE",
        resultCode: "SUCCESS",
        result: {
            TABLE: "TINY_TABLE",
            SCHEMA: tableSchema
        }
    })
    .afterJSON(function (json) {
        loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 120);
        var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
        expect(bodyJSON.result.errorMessageCode).toBe("ERROR");
        expect(bodyJSON.result.result.WARNING != null).toBe(true);
    })
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 10b: invalid url (swift)';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(siteURL + loadServerEndPoint,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": "https://invalid.softlayer.url.com",
            "AUTH1": server.swiftAuth1(),
            "AUTH2": server.swiftAuth2(),
            "BUCKET": server.swiftBucket(),
            "FILE_PATH": "dashdb_load/tiny_table.csv.gz"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(200)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: "LOAD started successfully.",
        errorMessageCode: "NONE",
        resultCode: "SUCCESS",
        result: {
            TABLE: "TINY_TABLE",
            SCHEMA: tableSchema
        }
    })
    .afterJSON(function (json) {
        loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 120);
        var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
        expect(bodyJSON.result.errorMessageCode).toBe("ERROR");
        expect(bodyJSON.result.result.WARNING != null).toBe(true);
    })
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 11a: LOAD from xls file is blocked (negative test)';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(siteURL + loadServerEndPoint,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "s3",
            "ENDPOINT": server.s3EndpointUrl(),
            "AUTH1": server.s3Auth1(),
            "AUTH2": server.s3Auth2(),
            "BUCKET": server.s3Bucket(),
            "FILE_PATH": "dashdb_load/order_details_small.xls"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'xls and xlsx are not supported for load from cloud',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();


//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 11b: LOAD from xlsx file is blocked (negative test)';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(siteURL + loadServerEndPoint,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "s3",
            "ENDPOINT": server.s3EndpointUrl(),
            "AUTH1": server.s3Auth1(),
            "AUTH2": server.s3Auth2(),
            "BUCKET": server.s3Bucket(),
            "FILE_PATH": "dashdb_load/order_details_small.xlsx"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        message: 'xls and xlsx are not supported for load from cloud',
        errorMessageCode: "ERROR",
        resultCode: "ERROR",
        result: []
    })
    .inspectJSON()
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 20a: incorrect authentication credentials (s3)';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "s3",
            "ENDPOINT": server.s3EndpointUrl(),
            "AUTH1": randomAuth1,
            "AUTH2": randomAuth2,
            "BUCKET": server.s3Bucket(),
            "FILE_PATH": "dashdb_load/order_details.csv"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(200)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON({
        message: "LOAD started successfully.",
        errorMessageCode: "NONE",
        resultCode: "SUCCESS",
        result: {
            TABLE: "TINY_TABLE",
            SCHEMA: tableSchema
        }
    })
    .afterJSON(function (json) {
        loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
        var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
        // loadUtils.validateLoadResult(bodyJSON, expectedStatsAuthErr);
// looks like on MPP we see 
//    SQL3025 A parameter specifying a filename or path is not valid.
//  whereas on SMP we see
//     SQL2018  The utility encountered an error "<error>" while attempting to
//         verify the user's authorization ID or database authorizations.
        expect(bodyJSON.result.result.WARNING).toContain('Routine "SYSPROC.ADMIN_CMD" execution has completed, but at least one error, "' + ( nodeType == 'MPP' ? 'SQL2036':'SQL2018') + '", was encountered during the execution. More information is available.');
    })
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 20b: incorrect authentication credentials (swift)';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH1": randomAuth1,
            "AUTH2": randomAuth2,
            "BUCKET": server.swiftBucket(),
            "FILE_PATH": "dashdb_load/order_details.csv"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(200)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON({
        message: "LOAD started successfully.",
        errorMessageCode: "NONE",
        resultCode: "SUCCESS",
        result: {
            TABLE: "TINY_TABLE",
            SCHEMA: tableSchema
        }
    })
    .afterJSON(function (json) {
        loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
        var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
// I think we  really should be getting a 
// SQL2018  The utility encountered an error "<error>" while attempting to
//       verify the user's authorization ID or database authorizations.
//  but on SMP we get a 
//    SQL1652 File I/O error occurred.
//  and on MPP we get a
//    SQL3025 A parameter specifying a filename or path is not valid.
//        expect(bodyJSON.result.result.WARNING).toMatch('Routine "SYSPROC.ADMIN_CMD" execution has completed, but at least one error, "SQL2018", was encountered during the execution. More information is available.');
//         expect(bodyJSON.result.result.WARNING).toMatch('Routine "SYSPROC.ADMIN_CMD" execution has completed, but at least one error, "' + ( nodeType == 'MPP' ? 'SQL3025' : 'SQL1652' )+'", was encountered during the execution. More information is available.');
        expect(bodyJSON.result.result.WARNING).toContain('Routine "SYSPROC.ADMIN_CMD" execution has completed, but at least one error, "SQL2036", was encountered during the execution. More information is available.');
        // loadUtils.validateLoadResult(bodyJSON, expectedStatsAuthErr);
    })
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 21a: file not found (s3)';
//
//////////////////////////////////////////////////////////////////////////
//frisby.create(msg)
//    .post(loadServerURL,
//    {
//        "LOAD_SOURCE": "CLOUD",
//        "CLOUD": {
//            "PROVIDER": "s3",
//            "ENDPOINT": server.s3EndpointUrl(),
//            "AUTH1": server.s3Auth1(),
//            "AUTH2": server.s3Auth2(),
//            "BUCKET": server.s3Bucket(),
//            "FILE_PATH": "dashdb_load/non_exist.csv"
//        },
//        "LOAD_PARAMETERS": {
//            "SCHEMA": tableSchema,
//            "TABLE": "TINY_TABLE"
//        }
//    },
//    { json: true })
//    .auth(user, pass)
//    .expectStatus(200)
//    .timeout(30000)
//    .expectHeaderContains('Content-Type', 'json')
//    .inspectJSON()
//    .expectJSON({
//        message: "LOAD started successfully.",
//        errorMessageCode: "NONE",
//        resultCode: "SUCCESS",
//        result: {
//            TABLE: "TINY_TABLE",
//            SCHEMA: tableSchema
//        }
//    })
//    .afterJSON(function (json) {
//        loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
//        var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
//        loadUtils.validateLoadResult(bodyJSON, expectedStatsFileNotFoundS3,this.current.describe);
//    })
//    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 21b: file not found (swift)';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(loadServerURL,
    {
        "LOAD_SOURCE": "CLOUD",
        "CLOUD": {
            "PROVIDER": "softlayer",
            "ENDPOINT": server.swiftEndpointUrl(),
            "AUTH1": server.swiftAuth1(),
            "AUTH2": server.swiftAuth2(),
            "BUCKET": server.swiftBucket(),
            "FILE_PATH": "dashdb_load/non_exist.csv"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": "TINY_TABLE"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(200)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON({
        message: "LOAD started successfully.",
        errorMessageCode: "NONE",
        resultCode: "SUCCESS",
        result: {
            TABLE: "TINY_TABLE",
            SCHEMA: tableSchema
        }
    })
    .afterJSON(function (json) {
        loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
        var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
//        loadUtils.validateLoadResult(bodyJSON, expectedStatsFileNotFoundSoftLayer);//should recover
    })
    .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 23: Load csv file from S3, happy path';
//
//////////////////////////////////////////////////////////////////////////
//frisby.create(msg)
//    .post(siteURL + loadServerEndPoint,
//    {
//        "LOAD_SOURCE": "CLOUD",
//        "CLOUD": {
//            "PROVIDER": "s3",
//            "ENDPOINT": server.s3EndpointUrl(),
//            "AUTH1": server.s3Auth1(),
//            "AUTH2": server.s3Auth2(),
//            "BUCKET": server.s3Bucket(),
//            "FILE_PATH": "dashdb_load/tiny_table.csv"
//        },
//        "LOAD_PARAMETERS": {
//            "SCHEMA": tableSchema,
//            "TABLE": "TINY_TABLE"
//        }
//    },
//    { json: true })
//    .auth(user, pass)
//    .expectStatus(200)
//    .expectHeaderContains('Content-Type', 'json')
//    .inspectJSON()
//    .expectJSON(
//    {
//        message: "LOAD started successfully.",
//        errorMessageCode: "NONE",
//        resultCode: "SUCCESS",
//        result: {
//            TABLE: "TINY_TABLE",
//            SCHEMA: tableSchema
//        }
//    })
//    .afterJSON(function (json) {
//        loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
//        var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
//        loadUtils.validateLoadResult(bodyJSON, expectedStats,this.current.describe);
//    })
//    .toss();


//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 24: Load csv file from softlayer';
//
//////////////////////////////////////////////////////////////////////////
//frisby.create(msg)
//    .post(siteURL + loadServerEndPoint,
//    {
//        "LOAD_SOURCE": "CLOUD",
//        "CLOUD": {
//            "PROVIDER": "softlayer",
//            "ENDPOINT": server.swiftEndpointUrl(),
//            "AUTH1": server.swiftAuth1(),
//            "AUTH2": server.swiftAuth2(),
//            "BUCKET": server.swiftBucket(),
//            "FILE_PATH": "dashdb_load/tiny_table.csv"
//        },
//        "LOAD_PARAMETERS": {
//            "SCHEMA": tableSchema,
//            "TABLE": "TINY_TABLE"
//        }
//    },
//    { json: true })
//    .auth(user, pass)
//    .expectStatus(200)
//    .expectHeaderContains('Content-Type', 'json')
//    .inspectJSON()
//    .expectJSON(
//    {
//        message: "LOAD started successfully.",
//        errorMessageCode: "NONE",
//        resultCode: "SUCCESS",
//        result: {
//            TABLE: "TINY_TABLE",
//            SCHEMA: tableSchema
//        }
//    })
//    .afterJSON(function (json) {
//        loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
//        var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
//        loadUtils.validateLoadResult(bodyJSON, expectedStats);
//    })
//    .toss();


//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 25: Load zip file from S3';
//
//////////////////////////////////////////////////////////////////////////
//frisby.create(msg)
//    .post(siteURL + loadServerEndPoint,
//    {
//        "LOAD_SOURCE": "CLOUD",
//        "CLOUD": {
//            "PROVIDER": "s3",
//            "ENDPOINT": server.s3EndpointUrl(),
//            "AUTH1": server.s3Auth1(),
//            "AUTH2": server.s3Auth2(),
//            "BUCKET": server.s3Bucket(),
//            "FILE_PATH": "dashdb_load/tiny_table.csv.gz"
//        },
//        "LOAD_PARAMETERS": {
//            "SCHEMA": tableSchema,
//            "TABLE": "TINY_TABLE",
//            "FILE_TYPE_MODIFIERS": {
//                "CUSTOM": ["GZIP"]
//            }
//        }
//    },
//    { json: true })
//    .auth(user, pass)
//    .expectStatus(200)
//    .expectHeaderContains('Content-Type', 'json')
//    .inspectJSON()
//    .expectJSON(
//    {
//        message: "LOAD started successfully.",
//        errorMessageCode: "NONE",
//        resultCode: "SUCCESS",
//        result: {
//            TABLE: "TINY_TABLE",
//            SCHEMA: tableSchema
//        }
//    })
//    .afterJSON(function (json) {
//        loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
//        var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
//        loadUtils.validateLoadResult(bodyJSON, expectedStats);
//    })
//    .toss();


//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 26: Load zip file from softlayer';
//
//////////////////////////////////////////////////////////////////////////
//frisby.create(msg)
//    .post(siteURL + loadServerEndPoint,
//    {
//        "LOAD_SOURCE": "CLOUD",
//        "CLOUD": {
//            "PROVIDER": "softlayer",
//            "ENDPOINT": server.swiftEndpointUrl(),
//            "AUTH1": server.swiftAuth1(),
//            "AUTH2": server.swiftAuth2(),
//            "BUCKET": server.swiftBucket(),
//            "FILE_PATH": "dashdb_load/tiny_table.csv.gz"
//        },
//        "LOAD_PARAMETERS": {
//            "SCHEMA": tableSchema,
//            "TABLE": "TINY_TABLE",
//            "FILE_TYPE_MODIFIERS": {
//                "CUSTOM": ["GZIP"]
//            }
//        }
//    },
//    { json: true })
//    .auth(user, pass)
//    .expectStatus(200)
//    .expectHeaderContains('Content-Type', 'json')
//    .inspectJSON()
//    .expectJSON(
//    {
//        message: "LOAD started successfully.",
//        errorMessageCode: "NONE",
//        resultCode: "SUCCESS",
//        result: {
//            TABLE: "TINY_TABLE",
//            SCHEMA: tableSchema
//        }
//    })
//    .afterJSON(function (json) {
//        loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
//        var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
//        loadUtils.validateLoadResult(bodyJSON, expectedStats);
//    })
//    .toss();


