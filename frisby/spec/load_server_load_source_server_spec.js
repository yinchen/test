//////////////////////////////////////////////////////
// ENDPOINT - POST:/load/server,GET/load/id 
// TEST - test for LOAD_SOURCE and SERVER options
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var server = require('../lib/server.js');
var path = require('path');
var loadUtils = require('../lib/loadutils.js');
var msg;

// Ignore rejected certificates 
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var loadServerURL = siteURL + loadServerEndPoint;
var tableName = "order_details";
var tableSchema = server.tabSchema();
var user = server.userID();
var pass = server.password();

var myname = path.basename(__filename);



//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
})


/*
 * general tests for the JSON object passed to the API
 */



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 1: Empty JSON passed to API';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            { },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                   message:'JSON is empty',         
                   errorMessageCode: "ERROR",
                   resultCode: "ERROR",      
                   result: []
                  })
      .inspectJSON()
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 2: JSON passed to API contains just name-value pair that we do not process';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            { "hello": "there"},
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                   message:'Parameter LOAD_PARAMETERS was not specified',         
                   errorMessageCode: "ERROR",
                   resultCode: "ERROR",      
                   result: []
                  })
      .inspectJSON()
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 3: missing LOAD_SOURCE and SERVER';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:'Parameter LOAD_SOURCE was not specified',         
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",      
                      result: []
                  })
      .inspectJSON()
      .toss();


/*
 *
 * LOAD_SOURCE tests
 *
 */



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 4: missing LOAD_SOURCE';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:'Parameter LOAD_SOURCE was not specified',         
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",      
                      result: []
                  })
      .inspectJSON()
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 5: empty LOAD_SOURCE';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE":"",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:'Invalid input value for LOAD_SOURCE json key.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",      
                      result: []
                  })
      .inspectJSON()
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 6: blank LOAD_SOURCE';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE":" ",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                 }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:'Invalid input value for LOAD_SOURCE json key.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",      
                      result: []
                  })
      .inspectJSON()
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 6.1: LOAD_SOURCE value is a JSON object';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE": {"hello" : "there"},
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                 }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:'Invalid input value for LOAD_SOURCE json key.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",      
                      result: []
                  })
      .inspectJSON()
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 6.2: LOAD_SOURCE value is a JSON array';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE": ["hello" , "there"],
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                 }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:'Invalid input value for LOAD_SOURCE json key.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",      
                      result: []
                  })
      .inspectJSON()
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 7: invalid value for LOAD_SOURCE';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE":"blah",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:'Invalid input value for LOAD_SOURCE json key.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",      
                      result: []
                  })
      .inspectJSON()
      .toss();



////  COMMENTED-OUT the following tests have been commented out until the following is fixed: https://github.ibm.com/CloudDataServices/dashdb-dsserver/issues/190
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ': Test 8: use LOAD_SOURCE:DESKTOP, which is a valid LOAD_SOURCE value (in the code) but not valid in the case of POST /load/server';
////  //
////  //////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(loadServerURL,
////              {
////                  "LOAD_SOURCE": "DESKTOP",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": "tiny_table.csv"
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": "TINY_TABLE"
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message:'Invalid input value for LOAD_SOURCE json key.',
////                        errorMessageCode: "ERROR",
////                        resultCode: "ERROR",      
////                        result: []
////                    })
////        .inspectJSON()
////        .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 9: specify LOAD_SOURCE:SERVER but omit SERVER key';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE": "SERVER",
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:'Parameter SERVER was not specified',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",      
                      result: []
                  })
      .inspectJSON()
      .toss();



/*
 *
 * SERVER_FILE_PATH tests
 *
 */



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 10: value of SERVER is an empty JSON object';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE":"SERVER",
                "SERVER": {},
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:'SERVER_FILE_PATH key not found.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",      
                      result: []
                  })
      .inspectJSON()
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 11: value of SERVER is a JSON object that contains just one key and it is not SERVER_FILE_PATH';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE":"SERVER",
                "SERVER": {"hello":"there"},
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:'SERVER_FILE_PATH key not found.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",      
                      result: []
                  })
      .inspectJSON()
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 11.1: value of server is a string';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE":"SERVER",
                "SERVER": "hello",
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:'The value for key: [SERVER] was not a JSONObject',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",      
                      result: []
                  })
      .inspectJSON()
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 11.2: value of server is a blank string';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE":"SERVER",
                "SERVER": " ",
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:'The value for key: [SERVER] was not a JSONObject',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",      
                      result: []
                  })
      .inspectJSON()
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 11.3: value of server is an empty string';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE":"SERVER",
                "SERVER": "",
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:'The value for key: [SERVER] was not a JSONObject',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",      
                      result: []
                  })
      .inspectJSON()
      .toss();
 


//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 11.4: value of server is a JSON array';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE":"SERVER",
                "SERVER": ["hi"],
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:'The value for key: [SERVER] was not a JSONObject',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",      
                      result: []
                  })
      .inspectJSON()
      .toss();
 


//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 12: empty SERVER_FILE_PATH';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE":"SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": ""
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:'SERVER_FILE_PATH must contain a value',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",      
                      result: []
                  })
      .inspectJSON()
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 13: blank value in SERVER_FILE_PATH';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE":"SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "  "
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:'SERVER_FILE_PATH must contain a value',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",      
                      result: []
                  })
      .inspectJSON()
      .toss();


////  COMMENTED-OUT the following tests have been commented out until the following is fixed: https://github.ibm.com/CloudDataServices/dashdb-dsserver/issues/149
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ': Test 13.1: value of SERVER_FILE_PATH is a JSON object (negative test)';
////  //
////  //////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(loadServerURL,
////              {
////                  "LOAD_SOURCE":"SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": {"hello": "there"}
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": "TINY_TABLE"
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message:'SERVER_FILE_PATH must contain a value',
////                        errorMessageCode: "ERROR",
////                        resultCode: "ERROR",      
////                        result: []
////                    })
////        .inspectJSON()
////        .toss();
////  
////  
////  
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ': Test 13.2: value of SERVER_FILE_PATH is a JSON array (negative test)';
////  //
////  //////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(loadServerURL,
////              {
////                  "LOAD_SOURCE":"SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": ["hello", "there"]
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": "TINY_TABLE"
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message:'SERVER_FILE_PATH must contain a value',
////                        errorMessageCode: "ERROR",
////                        resultCode: "ERROR",      
////                        result: []
////                    })
////        .inspectJSON()
////        .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 14: file specified by SERVER_FILE_PATH does not exist';
//
//////////////////////////////////////////////////////////////////////////

frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE":"SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "wrqwer321weqr5fqef.txt"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:'The file specified for SERVER_FILE_PATH does not exist for user "' + user + '".',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",      
                      result: []
                  })
      .inspectJSON()
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 15: file specified by SERVER_FILE_PATH is a directory';
//
//////////////////////////////////////////////////////////////////////////

frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE":"SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "loadlogs"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:'The file specified for SERVER_FILE_PATH is a directory.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",      
                      result: []
                  })
      .inspectJSON()
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 16: file specified by SERVER_FILE_PATH is "." (negative test)';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE":"SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "."
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:'The file specified for SERVER_FILE_PATH is a directory.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",      
                      result: []
                  })
      .inspectJSON()
      .toss();



////  COMMENTED-OUT the following tests have been commented out until the following is fixed: https://github.ibm.com/CloudDataServices/dashdb-dsserver/issues/149
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ': Test 17: file specified by SERVER_FILE_PATH contains ".." (negative test)';
////  //
////  //////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(loadServerURL,
////              {
////                  "LOAD_SOURCE":"SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": "a/../b.csc"
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": "TINY_TABLE"
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message:'test test test',
////                        errorMessageCode: "ERROR",
////                        resultCode: "ERROR",      
////                        result: []
////                    })
////        .inspectJSON()
////        .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 18: file specified by SERVER_FILE_PATH is ".." (negative test)';
//
//////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(loadServerURL,
            {
                "LOAD_SOURCE":"SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": ".."
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:'The file specified for SERVER_FILE_PATH is a directory.',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",      
                      result: []
                  })
      .inspectJSON()
      .toss();



////  COMMENTED-OUT the following tests have been commented out until the following is fixed: https://github.ibm.com/CloudDataServices/dashdb-dsserver/issues/149
////  //////////////////////////////////////////////////////////////////////////
////  //
////  msg = myname + ': Test 19: file specified by SERVER_FILE_PATH is in a subdir (negative test)';
////  //
////  //////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(loadServerURL,
////              {
////                  "LOAD_SOURCE":"SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": "loadlogs/blah.csv"
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": "TINY_TABLE"
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message:'test test test',
////                        errorMessageCode: "ERROR",
////                        resultCode: "ERROR",      
////                        result: []
////                    })
////        .inspectJSON()
////        .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 20: SERVER_FILE_PATH is a file with no extension';
//
////////////////////////////////////////////////////////////////////////////////
var expectedStats = {
    numRowsRead : 12,
    numRowsLoaded : 12,
    numRowsCommitted : 12,
    numRowsSkipped : 0,
    numRowsRejected : 0,
    numRowsDeleted : 0 };
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table_noextension"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE",
                    "LOAD_ACTION": "REPLACE",
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: "TINY_TABLE",
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, expectedStats);
                     loadUtils.verifyAllTableContents(tableSchema, "TINY_TABLE", server.tiny_table_expected_rows);
                 })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 20: SERVER_FILE_PATH is a file with no extension and a 0x02 delimiter (previously would have been mistaken for a binary file)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table_coldel_0x02_noextension"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE",
                    "LOAD_ACTION": "REPLACE",
                    "FILE_TYPE_MODIFIERS": {
                         "COLDEL": "0x02"
                    }
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: "TINY_TABLE",
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, expectedStats);
                     loadUtils.verifyAllTableContents(tableSchema, "TINY_TABLE", server.tiny_table_expected_rows);
                 })
      .toss();


/*
 * The following tests use ZIP, GZ and TAR files made from tiny_table.csv. Since these files
 * are not CSV files, LOAD will parse them as garbage. I've put in expected values based
 * on a run on a system.
 */

////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 21: SERVER_FILE_PATH is a .zip file (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
var expectedStatsZip = {
    numRowsRead : 2,
    numRowsLoaded : 2,
    numRowsCommitted : 2,
    numRowsSkipped : 0,
    numRowsRejected : 0,
    numRowsDeleted : 0,
    errorMessageCode: "ERROR"}
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table.zip"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE",
                    "LOAD_ACTION": "REPLACE",
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: "TINY_TABLE",
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, expectedStatsZip);
                 })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 22: SERVER_FILE_PATH is a .gz file (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
var expectedStatsGz = {
    numRowsRead : 2,
    numRowsLoaded : 2,
    numRowsCommitted : 2,
    numRowsSkipped : 0,
    numRowsRejected : 0,
    numRowsDeleted : 0,
    errorMessageCode: "ERROR"}
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table.csv.gz"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE",
                    "LOAD_ACTION": "REPLACE",
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: "TINY_TABLE",
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, expectedStatsGz);
                 })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 23: SERVER_FILE_PATH is a .tar file (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
var expectedStatsTar = {
    numRowsRead : 13,
    numRowsLoaded : 13,
    numRowsCommitted : 13,
    numRowsSkipped : 0,
    numRowsRejected : 0,
    numRowsDeleted : 0,
    errorMessageCode: "ERROR"}
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table.tar"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TINY_TABLE",
                    "LOAD_ACTION": "REPLACE",
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: "TINY_TABLE",
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, expectedStatsTar);
                 })
      .toss();






