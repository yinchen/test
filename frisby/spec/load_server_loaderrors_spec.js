//////////////////////////////////////////////////////
// ENDPOINT - POST:/load/server,GET/load/id 
// TEST - various load scenarios that produce ERROR in the completed load
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var expect = require('expect');
var server = require('../lib/server.js');
var fs = require('fs');
var path = require('path');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var tableName = "DUMPFILE_TEST_TABLE";
var tableSchema = server.tabSchema();
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);

// Global setup for all tests
frisby.globalSetup({
		timeout : (30 * 1000)
})



////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 1: loading an empty file should produce a "no rows were read" message';
//
////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
              "LOAD_SOURCE": "SERVER",
              "SERVER": {
                "SERVER_FILE_PATH": "empty.csv"
              },
                "LOAD_PARAMETERS": {
                "TABLE" : tableName,
                "SCHEMA" : tableSchema
              },
           },
           {json: true })
  .auth(user, pass)
  .timeout(30000)
  .expectHeaderContains('Content-Type', 'json')
  .inspectJSON()
  .expectJSON(
              {
                errorMessageCode : "NONE",
                resultCode : "SUCCESS",
                result : {
                  TABLE : tableName,
                  SCHEMA : tableSchema
		        }
              })
   .afterJSON(
              function(json) {
                 loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                 var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                 loadUtils.validateLoadResult(bodyJSON, 0, 0,
                                                 0, 0, 0);
			  })
   .toss();



////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 2: Force a syntax error via custom file type modifiers';
//
////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
              "LOAD_SOURCE": "SERVER",
              "SERVER": {
                "SERVER_FILE_PATH": "empty.csv"
              },
                "LOAD_PARAMETERS": {
                "TABLE" : tableName,
                "SCHEMA" : tableSchema,
			    "FILE_TYPE_MODIFIERS": {
                  CUSTOM: [ "MESSAGES"]
				}
              },
           },
           {json: true })
  .auth(user, pass)
  .timeout(30000)
  .expectHeaderContains('Content-Type', 'json')
  .inspectJSON()
  .expectJSON(
              {
                errorMessageCode : "NONE",
                resultCode : "SUCCESS",
                result : {
                  TABLE : tableName,
                  SCHEMA : tableSchema
		        }
              })
   .afterJSON(
              function(json) {
                 loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                 var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                 loadUtils.validateLoadResult(bodyJSON, "", "",
                                                 "", "", "", "ERROR");
                 expect(bodyJSON.result.message).toBe('No rows of data were read');
				 // should be a WARNING
				 // expect(bodyJSON.result.result.WARNING).toExist();
			  })
   .toss();



////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 3: a load with truncated values should return roduce a "LOAD completed with warnings" message';
//
////////////////////////////////////////////////////////////////////
tableName = 'ERRORTABLE';
var numRowsRead = 7;
var numRowsLoaded = 7;
var numRowsCommitted = 7;
var numRowsDeleted = 0;
var numRowsSkipped = 0;

frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
              "LOAD_SOURCE": "SERVER",
              "SERVER": {
                "SERVER_FILE_PATH": "truncate.csv"
              },
                "LOAD_PARAMETERS": {
                "TABLE" : tableName,
                "SCHEMA" : tableSchema
              },
           },
           {json: true })
  .auth(user, pass)
  .timeout(30000)
  .expectHeaderContains('Content-Type', 'json')
  .inspectJSON()
  .expectJSON(
              {
                errorMessageCode : "NONE",
                resultCode : "SUCCESS",
                result : {
                  TABLE : tableName,
                  SCHEMA : tableSchema
		        }
              })
   .afterJSON(
              function(json) {
                 loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                 var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                 loadUtils.validateLoadResult(bodyJSON,
                                                 numRowsRead,
                                                 numRowsLoaded,
                                                 numRowsCommitted,
                                                 numRowsDeleted,
                                                 numRowsSkipped,
                                                 "ERROR");
                 expect(bodyJSON.result.message).toBe('LOAD completed with warnings.');
			  })
   .toss();



////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 4: a load with conversion errors should return a "LOAD completed with warnings" message';
//
////////////////////////////////////////////////////////////////////
tableName = 'ERRORTABLE';
var numRowsRead = 7;
var numRowsLoaded = 7;
var numRowsCommitted = 7;
var numRowsDeleted = 0;
var numRowsSkipped = 0;

frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
              "LOAD_SOURCE": "SERVER",
              "SERVER": {
                "SERVER_FILE_PATH": "conversionerror.csv"
              },
                "LOAD_PARAMETERS": {
                "LOAD_ACTION": "REPLACE",
                "TABLE" : tableName,
                "SCHEMA" : tableSchema
              },
           },
           {json: true })
  .auth(user, pass)
  .timeout(30000)
  .expectHeaderContains('Content-Type', 'json')
  .inspectJSON()
  .expectJSON(
              {
                errorMessageCode : "NONE",
                resultCode : "SUCCESS",
                result : {
                  TABLE : tableName,
                  SCHEMA : tableSchema
		        }
              })
   .afterJSON(
              function(json) {
                 loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                 var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                 loadUtils.validateLoadResult(bodyJSON,
                                                 numRowsRead,
                                                 numRowsLoaded,
                                                 numRowsCommitted,
                                                 numRowsDeleted,
                                                 numRowsSkipped,
                                                 "ERROR");
                 expect(bodyJSON.result.message).toBe('LOAD completed with warnings.');
			  })
   .toss();
// tests for rejected rows are covered in the dumpfile testcase
