//////////////////////////////////////////////////////
// ENDPOINT - POST /load/server
// TEST - test input files with 'CR' and 'CRLF' as the newline charecter
// COMMENT - db2 load supports 'CRLF' and 'LF' natively. Files with 'CR'
// will be converted by the load API. Since 'LF' is tested everywhere else,
// we are only testing 'CR' and 'CRLF' in this script.
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var server = require('../lib/server.js');
var path = require('path');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var tableName = "TINY_TABLE";
var tableSchema = server.tabSchema();
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);

//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
})

var numRowsRead = 2;
var numRowsLoaded = 2;
var numRowsCommitted = 2;
var numRowsDeleted = 0;
var numRowsSkipped = 0;

////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 1: input file with CR (0x0D) as newline';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
              "LOAD_SOURCE": "SERVER",
              "SERVER": {
                "SERVER_FILE_PATH": "newline_CR.csv"
              },
              "LOAD_PARAMETERS": {
                "SCHEMA": tableSchema,
                "TABLE": tableName,
              }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                    errorMessageCode: 'NONE',
                    resultCode: 'SUCCESS',
                    result: {
                      TABLE: tableName,
                      SCHEMA: tableSchema
                    }
                  })
      .afterJSON(function (json) {
        loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
        var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
	      loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsDeleted, numRowsSkipped);
      })
      .toss();

////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 2: input file with CRLF (0x0D0A) as newline';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
              "LOAD_SOURCE": "SERVER",
              "SERVER": {
                "SERVER_FILE_PATH": "newline_CRLF.csv"
              },
              "LOAD_PARAMETERS": {
                "SCHEMA": tableSchema,
                "TABLE": tableName,
              }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                    errorMessageCode: 'NONE',
                    resultCode: 'SUCCESS',
                    result: {
                      TABLE: tableName,
                      SCHEMA: tableSchema
                    }
                  })
      .afterJSON(function (json) {
        loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
        var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
	      loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsDeleted, numRowsSkipped);
      })
      .toss();
