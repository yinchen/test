//////////////////////////////////////////////////////
// ENDPOINT - POST /load/server
// TEST - test the one load per table scenario
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var server = require('../lib/server.js');
var path = require('path');
var loadUtils = require('../lib/loadutils.js');
var ibmdb = require('ibm_db');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var tableName = 'TINY_TABLE';
var tableSchema = server.tabSchema();
var user = server.userID();
var pass = server.password();
var connString = server.connString();
var myname = path.basename(__filename);
var printErrorLine = server.printErrorLine;

//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
})


////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 1: Only 1 load allowed at a time. 1a First lock table and kick off a load';
//
////////////////////////////////////////////////////////////////////////////////
/*
 * due to the nature of frisby and node.js, the approach is:
 * a) 
 *    1) lock table
 *    2) Issue a load against table
 * b) In the afterJSON callback from the frisby in 1.
 *    1) grab the load id. 
 *    2) issue a load against same table and expect an error due to table being in use
 *    3) in the after() callback for the frisby in b)
 *       i)  wait for the load in 1) to complete (we saved its load id
 *       ii) verifty the load from 1) has completed successfully
*/
var rowcount = 12;
var numRowsRead = rowcount;
var numRowsLoaded = rowcount;
var numRowsCommitted = rowcount;
var numRowsRejected = 0;
var numRowsSkipped = 0;
var numRowsDeleted = 0;

// We are doing this asynchronously to avoid less callback hell
try {
    // Open a connection
    var conn = ibmdb.openSync(connString);

    // begin a transaction (so lock doesn't disappear as it would with auto-commit)
    conn.beginTransactionSync();
} catch(e) {
    console.log(e.message);
    printErrorLine();
    process.exit(1);
}

// lock the table so that the first load will hang
var lockStmt = 'LOCK TABLE ' + tableSchema + '.' + tableName +
               ' IN EXCLUSIVE MODE';
// run query. The loads will be kicked off in a callback
conn.query(lockStmt, 
           function (err) {

               if (err) {
                   console.log(err);
                   printErrorLine();
                   process.exit(1);
               } 
           
               frisby.create(msg)
                     .post(siteURL + loadServerEndPoint,
                           {
                               "LOAD_SOURCE" : "SERVER",
                               "SERVER" : {
                                   "SERVER_FILE_PATH": "tiny_table.csv"
                               },
                               "LOAD_PARAMETERS" : {
                                   "SCHEMA" : tableSchema,
                                   "TABLE" : tableName,
                                   "LOAD_ACTION": "REPLACE"
                               },
                           },
                           {json : true})
                     .auth(user, pass)
                     .expectStatus(200)
                     .timeout(60000)
                     .expectHeaderContains('Content-Type', 'json')
                     .inspectJSON()
                     .expectJSON(
                                 {
                                     errorMessageCode : "NONE",
                                     resultCode : "SUCCESS",
                                     result : {
                                         TABLE : tableName,
                                         SCHEMA : tableSchema
                                     }
                                 })
                     .afterJSON(function(json) {
                                    var load_id = json.result.LOAD_ID;
                                    
                                    ////////////////////////////////////////////////////////////////////////////////
                                    //
                                    var msg = myname + ': Test 1b: send a 2nd request for a load into the same table, expect an error';
                                    //
                                    ////////////////////////////////////////////////////////////////////////////////
                                    frisby.create(msg)
                                              .post(siteURL + loadServerEndPoint,
                                                    {
                                                        "LOAD_SOURCE" : "SERVER",
                                                        "SERVER" : {
                                                            "SERVER_FILE_PATH": "tiny_table.csv"
                                                        },
                                                        "LOAD_PARAMETERS" : {
                                                            "SCHEMA" : tableSchema,
                                                            "TABLE" : tableName,
                                                        },
                                                    },
                                                    {json : true})
                                          .auth(user, pass)
                                          .expectStatus(400)
                                          .timeout(60000)
                                          .expectHeaderContains('Content-Type', 'json')
                                          .inspectJSON()
                                          .expectJSON(
                                                      {
                                                          message: 'Table ' + tableSchema + '.' + tableName + ' is currently in use by another load process',
                                                          errorMessageCode : "ERROR",
                                                          resultCode : "ERROR",
                                                          result : []
                                                      })
                                          .after(function() {
                                                     // this will unlock the table
                                                     conn.commitTransactionSync();
                                                     conn.closeSync();
                                                     loadUtils.wait_for_load_to_complete(load_id, 50);
                                                     var bodyJSON = loadUtils.get_loadid(load_id);
                                                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsSkipped, numRowsDeleted);
                                                 })
                                          .toss();
                                    })
                     .toss();
             });


////  COMMENTED-OUT test 2 is commented out until https://github.ibm.com/CloudDataServices/dashdb-dsserver/issues/156 is fixed
////  
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  var msg = myname + ': Test 2: Only 1 load allowed at a time. 1a First lock table and kick off a load';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  /*
////   * due to the nature of frisby and node.js, the approach is:
////   * a) 
////   *    1) lock table
////   *    2) Issue a load against table
////   * b) In the afterJSON callback from the frisby in 1.
////   *    1) grab the load id. 
////   *    2) issue a load against same table and expect an error due to table being in use
////   *    3) in the after() callback for the frisby in b)
////   *       i)  wait for the load in 1) to complete (we saved its load id
////   *       ii) verifty the load from 1) has completed successfully
////  */
////  var tableName2 = 'TINY_TABLE2';
////  var tableName2quoted = '"TINY_TABLE2"';
////  // We are doing this asynchronously to avoid less callback hell
////  try {
////      // Open a connection
////      var conn2 = ibmdb.openSync(connString);
////  
////      // begin a transaction (so lock doesn't disappear as it would with auto-commit)
////      conn2.beginTransactionSync();
////  } catch(e) {
////      console.log(e.message);
////      printErrorLine();
////      process.exit(1);
////  }
////  
////  // lock the table so that the first load will hang
////  var lockStmt = 'LOCK TABLE ' + tableSchema + '.' + tableName2 +
////                 ' IN EXCLUSIVE MODE';
////  // run query. The loads will be kicked off in a callback
////  conn2.query(lockStmt, 
////             function (err) {
////  
////                 if (err) {
////                     console.log(err);
////                     printErrorLine();
////                     process.exit(1);
////                 } 
////             
////                 frisby.create(msg)
////                       .post(siteURL + loadServerEndPoint,
////                             {
////                                 "LOAD_SOURCE" : "SERVER",
////                                 "SERVER" : {
////                                     "SERVER_FILE_PATH": "tiny_table.csv"
////                                 },
////                                 "LOAD_PARAMETERS" : {
////                                     "SCHEMA" : tableSchema,
////                                     "TABLE" : tableName2,
////                                     "LOAD_ACTION": "REPLACE"
////                                 },
////                             },
////                             {json : true})
////                       .auth(user, pass)
////                       .expectStatus(200)
////                       .timeout(60000)
////                       .expectHeaderContains('Content-Type', 'json')
////                       .inspectJSON()
////                       .expectJSON(
////                                   {
////                                       errorMessageCode : "NONE",
////                                       resultCode : "SUCCESS",
////                                       result : {
////                                           TABLE : tableName2,
////                                           SCHEMA : tableSchema
////                                       }
////                                   })
////                       .afterJSON(function(json) {
////                                      var load_id = json.result.LOAD_ID;
////                                      
////                                      ////////////////////////////////////////////////////////////////////////////////
////                                      //
////                                      var msg = myname + ': Test 2b: send a 2nd request for a load into the same table, the table being quoted, expect an error';
////                                      //
////                                      ////////////////////////////////////////////////////////////////////////////////
////                                      frisby.create(msg)
////                                                .post(siteURL + loadServerEndPoint,
////                                                      {
////                                                          "LOAD_SOURCE" : "SERVER",
////                                                          "SERVER" : {
////                                                              "SERVER_FILE_PATH": "tiny_table.csv"
////                                                          },
////                                                          "LOAD_PARAMETERS" : {
////                                                              "SCHEMA" : tableSchema,
////                                                              "TABLE" : tableName2quoted,
////                                                          },
////                                                      },
////                                                      {json : true})
////                                            .auth(user, pass)
////                                            .expectStatus(400)
////                                            .timeout(60000)
////                                            .expectHeaderContains('Content-Type', 'json')
////                                            .inspectJSON()
////                                            .expectJSON(
////                                                        {
////                                                            message: 'Table ' + tableSchema + '.' + tableName2quoted + ' is currently in use by another load process',
////                                                            errorMessageCode : "ERROR",
////                                                            resultCode : "ERROR",
////                                                            result : []
////                                                        })
////                                            .after(function() {
////                                                       // this will unlock the table
////                                                       conn2.commitTransactionSync();
////                                                       conn2.closeSync();
////                                                       loadUtils.wait_for_load_to_complete(load_id, 50);
////                                                       var bodyJSON = loadUtils.get_loadid(load_id);
////                                                       loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsSkipped, numRowsDeleted);
////                                                   })
////                                            .toss();
////                                      })
////                       .toss();
////               });



////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 3: Only 1 load allowed at a time. 1a First lock table and kick off a load';
//
////////////////////////////////////////////////////////////////////////////////
/*
 * due to the nature of frisby and node.js, the approach is:
 * a) 
 *    1) lock table
 *    2) Issue a load against table
 * b) In the afterJSON callback from the frisby in 1.
 *    1) grab the load id. 
 *    2) issue a load against same table and expect an error due to table being in use
 *    3) in the after() callback for the frisby in b)
 *       i)  wait for the load in 1) to complete (we saved its load id
 *       ii) verifty the load from 1) has completed successfully
*/
var tableName3 = "TINY_TABLE3";
var tableName3lc = "tiny_table3";
// We are doing this asynchronously to avoid less callback hell
try {
    // Open a connection
    var conn3 = ibmdb.openSync(connString);

    // begin a transaction (so lock doesn't disappear as it would with auto-commit)
    conn3.beginTransactionSync();
} catch(e) {
    console.log(e.message);
    printErrorLine();
    process.exit(1);
}

// lock the table so that the first load will hang
var lockStmt = 'LOCK TABLE ' + tableSchema + '.' + tableName3 +
               ' IN EXCLUSIVE MODE';
// run query. The loads will be kicked off in a callback
conn3.query(lockStmt, 
           function (err) {

               if (err) {
                   console.log(err);
                   printErrorLine();
                   process.exit(1);
               } 
           
               frisby.create(msg)
                     .post(siteURL + loadServerEndPoint,
                           {
                               "LOAD_SOURCE" : "SERVER",
                               "SERVER" : {
                                   "SERVER_FILE_PATH": "tiny_table.csv"
                               },
                               "LOAD_PARAMETERS" : {
                                   "SCHEMA" : tableSchema,
                                   "TABLE" : tableName3,
                                   "LOAD_ACTION": "REPLACE"
                               },
                           },
                           {json : true})
                     .auth(user, pass)
                     .expectStatus(200)
                     .timeout(60000)
                     .expectHeaderContains('Content-Type', 'json')
                     .inspectJSON()
                     .expectJSON(
                                 {
                                     errorMessageCode : "NONE",
                                     resultCode : "SUCCESS",
                                     result : {
                                         TABLE : tableName3,
                                         SCHEMA : tableSchema
                                     }
                                 })
                     .afterJSON(function(json) {
                                    var load_id = json.result.LOAD_ID;
                                    
                                    ////////////////////////////////////////////////////////////////////////////////
                                    //
                                    var msg = myname + ': Test 3b: send a 2nd request for a load into the same table, the table being lowercase, expect an error';
                                    //
                                    ////////////////////////////////////////////////////////////////////////////////
                                    frisby.create(msg)
                                              .post(siteURL + loadServerEndPoint,
                                                    {
                                                        "LOAD_SOURCE" : "SERVER",
                                                        "SERVER" : {
                                                            "SERVER_FILE_PATH": "tiny_table.csv"
                                                        },
                                                        "LOAD_PARAMETERS" : {
                                                            "SCHEMA" : tableSchema,
                                                            "TABLE" : tableName3lc,
                                                        },
                                                    },
                                                    {json : true})
                                          .auth(user, pass)
                                          .expectStatus(400)
                                          .timeout(60000)
                                          .expectHeaderContains('Content-Type', 'json')
                                          .inspectJSON()
                                          .expectJSON(
                                                      {
                                                          message: 'Table ' + tableSchema + '.' + tableName3lc + ' is currently in use by another load process',
                                                          errorMessageCode : "ERROR",
                                                          resultCode : "ERROR",
                                                          result : []
                                                      })
                                          .after(function() {
                                                     // this will unlock the table
                                                     conn3.commitTransactionSync();
                                                     conn3.closeSync();
                                                     loadUtils.wait_for_load_to_complete(load_id, 50);
                                                     var bodyJSON = loadUtils.get_loadid(load_id);
                                                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsSkipped, numRowsDeleted);
                                                 })
                                          .toss();
                                    })
                     .toss();
             });
//////////////////////////////////////////////////////////////////////
//
// Due to the way Frisby is implemented, if you call it from within a callback, as we've done above, 
// this script will end and no tests will be called. It appears that if you put a dummy test here at
// the end, that things actually work
//
//////////////////////////////////////////////////////////////////////
frisby.create('workaround dummy test')
      .get(siteURL + server.homeEndPoint())
      .auth(user, pass)
      .expectStatus(200)
      .expectHeader('content-type', 'application/json')
      .expectJSON(
                  {
                      message: "NONE",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS"
                  })
      .expectJSONTypes(
                       {
                           message: String,
                           result: String,
                           errorMessageCode: String,
                           resultCode: String
                       }) 
      .toss();

