//////////////////////////////////////////////////////
// ENDPOINT - POST /load/server
// TEST - test the db2 load action for load replace
// COMMENT - none

var frisby = require('frisby');
var server = require('../lib/server.js');
var path = require('path');
var expect = require('expect');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var tableName = "TINY_TABLE";
var tableSchema = server.tabSchema();
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);

//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
})

var numRowsRead = 12;
var numRowsLoaded = 12;
var numRowsCommitted = 12;
var numRowsRejected = 0;
var numRowsSkipped = 0;
var numRowsDeleted = 0;

////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 1a: valid options REPLACE keepdictionary';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(siteURL + loadServerEndPoint,
    {
        "LOAD_SOURCE": "SERVER",
        "SERVER": {
            "SERVER_FILE_PATH": "tiny_table.csv"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": tableName,
            "LOAD_ACTION": "REPLACE keepdictionary"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(200)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        errorMessageCode: 'NONE',
        resultCode: 'SUCCESS',
        result: {
            TABLE: tableName,
            SCHEMA: tableSchema
        }
    })
    .afterJSON(function (json) {
        loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 30);
        var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
        loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsSkipped, numRowsDeleted);
    })
    .toss();

////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 1b: valid options REPLACE RESETDICTIONARY';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(siteURL + loadServerEndPoint,
    {
        "LOAD_SOURCE": "SERVER",
        "SERVER": {
            "SERVER_FILE_PATH": "tiny_table.csv"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": tableName,
            "LOAD_ACTION": "REPLACE RESETDICTIONARY"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(200)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        errorMessageCode: 'NONE',
        resultCode: 'SUCCESS',
        result: {
            TABLE: tableName,
            SCHEMA: tableSchema
        }
    })
    .afterJSON(function (json) {
        loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 30);
        var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
        loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsSkipped, numRowsDeleted);
    })
    .toss();

//////////////////////////////////////////////////////////////////////////////////
////
//var msg = myname + ': Test 1c: valid options REPLACE RESETDICTIONARYONLY';
////
//////////////////////////////////////////////////////////////////////////////////
//frisby.create(msg)
//    .post(siteURL + loadServerEndPoint,
//    {
//        "LOAD_SOURCE": "SERVER",
//        "SERVER": {
//            "SERVER_FILE_PATH": "tiny_table.csv"
//        },
//        "LOAD_PARAMETERS": {
//            "SCHEMA": tableSchema,
//            "TABLE": tableName,
//            "LOAD_ACTION": "REPLACE RESETDICTIONARYONLY"
//        }
//    },
//    { json: true })
//    .auth(user, pass)
//    .expectStatus(200)
//    .timeout(30000)
//    .expectHeaderContains('Content-Type', 'json')
//    .inspectJSON()
//    .expectJSON(
//    {
//        errorMessageCode: 'NONE',
//        resultCode: 'SUCCESS',
//        result: {
//            TABLE: tableName,
//            SCHEMA: tableSchema
//        }
//    })
//    .afterJSON(function (json) {
//        loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 30);
//        var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
//        loadUtils.validateLoadResult(bodyJSON, numRowsRead, 0, 0, numRowsSkipped, numRowsDeleted);
//    })
//    .toss();


////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 2: invalid options combination INSERT keepdictionary';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(siteURL + loadServerEndPoint,
    {
        "LOAD_SOURCE": "SERVER",
        "SERVER": {
            "SERVER_FILE_PATH": "tiny_table.csv"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": tableName,
            "LOAD_ACTION": "INSERT KEEPDICTIONARY"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        errorMessageCode: 'ERROR',
        resultCode: 'ERROR',
        result: [],
        message:"Invalid loadAction 'INSERT KEEPDICTIONARY'."
    })
    .toss();

////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 3: invalid action options REPLACE INVALID_ACTION';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(siteURL + loadServerEndPoint,
    {
        "LOAD_SOURCE": "SERVER",
        "SERVER": {
            "SERVER_FILE_PATH": "tiny_table.csv"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": tableName,
            "LOAD_ACTION": "REPLACE KEEPDICTIONARY INVALID_ACTION"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        errorMessageCode: 'ERROR',
        resultCode: 'ERROR',
        result: [],
        message:"Invalid loadAction 'REPLACE KEEPDICTIONARY INVALID_ACTION'."
    })
    .toss();
