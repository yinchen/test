//////////////////////////////////////////////////////
// ENDPOINT - POST:/load/server
// TEST - HAS_HEADER_ROW load parameter
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var server = require('../lib/server.js');
var fs = require('fs');
var path = require('path');
var FormData = require('form-data');
var loadUtils = require('../lib/loadutils.js');


// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var serverhomeURL = server.homeURL();
var user = server.userID();
var pass = server.password();
var tableSchema = server.tabSchema();
var myname = path.basename(__filename);



////////////////////////////////////////////////////////////////////////////////
//
var msg  = myname + ': Test 1a: Upload and for use with the HAS_HEADER_ROW load parameter';
var msg2 = myname + ': Test 1b: load a file using the HAS_HEADER_ROW load parameter';
//
////////////////////////////////////////////////////////////////////////////////
var numRows = 999; // number of expected rows loaded from this file is total-1 (first line will be stripped).
var file = path.resolve(__dirname, '../datasets/order_details_header_small.csv') //has header row
var rStream = fs.createReadStream(file)
var form = new FormData();
form.append('loadFile0', rStream,
            {
              knownLength: fs.statSync(file).size,
            });

frisby.create(msg)
      .post(serverhomeURL,
            form,
            {
               json: false,
               headers: {
                   'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
                   'content-length': form.getLengthSync()
               }
            })
     .auth(user, pass)
     .expectStatus(200)
     .timeout(5*60* 1000)
     .expectHeaderContains('Content-Type', 'application/json')
     .expectJSON({
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      message: "NONE",
                      result: {
                          "filesUploaded": [
                              "order_details_header_small.csv"
                          ]
                      },
                 } )
     .after(function(err, res, body) {
              frisby.create(msg2)
                    .post(siteURL + loadServerEndPoint , 
                          {
                              "LOAD_SOURCE": "SERVER",
                              "SERVER": {
                                  "SERVER_FILE_PATH": "order_details_header_small.csv"
                              },
                              "LOAD_PARAMETERS": {
                                  "SCHEMA": tableSchema,
                                  "TABLE": "ORDER_DETAILS",
                                  "LOAD_ACTION": "REPLACE",
                                  "HAS_HEADER_ROW": "TRUE",
                              },
                          },
                          {json: true })
                    .auth(user, pass)
                    .expectStatus(200)
                    .timeout(30000)
                    .expectHeaderContains('Content-Type', 'json')
                    .inspectBody()
                    .expectJSON({
                                    errorMessageCode: "NONE",
                                    resultCode: "SUCCESS",
                                    message: 'LOAD started successfully.',
                                    result: {
                                        TABLE: server.tabName,
                                        SCHEMA: server.tabSchema,
                                        LOAD_STATUS: 'RUNNING'
                                    }
                                })
                    .afterJSON(function (json) {
                         loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                         var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                         loadUtils.validateLoadResult(bodyJSON, numRows, numRows, numRows, 0, 0);
                       })
                   .toss();
           })
     .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg  = myname + ': Test 2: load file with HAS_HEADER_ROW:FALSE';
//
////////////////////////////////////////////////////////////////////////////////
var rowcount = 12;
var numRowsRead = rowcount;
var numRowsLoaded = rowcount;
var numRowsCommitted = rowcount;
var numRowsRejected = 0;
var numRowsSkipped = 0;
var numRowsDeleted = 0;
var tableName = "TINY_TABLE";

frisby.create(msg)
       .post(siteURL + loadServerEndPoint,
             {
                 "LOAD_SOURCE" : "SERVER",
                 "SERVER" : {
                     "SERVER_FILE_PATH": "tiny_table.csv"
                 },
                 "LOAD_PARAMETERS" : {
                     "SCHEMA" : tableSchema,
                     "TABLE" : tableName,
                     "HAS_HEADER_ROW": "FALSE"
                 }
             },
             {json : true})
       .auth(user, pass)
       .expectStatus(200)
       .timeout(60000)
       .expectHeaderContains('Content-Type', 'json')
       .inspectJSON()
       .expectJSON(
                   {
                       errorMessageCode : "NONE",
                       resultCode : "SUCCESS",
                       result : {
                           TABLE : tableName,
                           SCHEMA : tableSchema
                       }
                   })
      .afterJSON(function(json) {
            loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsSkipped, numRowsDeleted);
            })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg  = myname + ': Test 3: HAS_HEADER_ROW is an invalid string value (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
       .post(siteURL + loadServerEndPoint,
             {
                 "LOAD_SOURCE" : "SERVER",
                 "SERVER" : {
                     "SERVER_FILE_PATH": "tiny_table.csv"
                 },
                 "LOAD_PARAMETERS" : {
                     "SCHEMA" : tableSchema,
                     "TABLE" : tableName,
                     "HAS_HEADER_ROW": "blah"
                 }
             },
             {json : true})
       .auth(user, pass)
       .expectStatus(400)
       .timeout(60000)
       .expectHeaderContains('Content-Type', 'json')
       .inspectJSON()
       .expectJSON(
                   {
                       errorMessageCode : "ERROR",
                       resultCode : "ERROR",
                       message: "Invalid value for parameter 'has_header_row'. Valid values are \"yes\" and \"no\".",
                       result : []
                   })
      .toss();


////  COMMENTED-OUT the following tests have been commented out until the following is fixed: https://github.ibm.com/CloudDataServices/dashdb-dsserver/issues/168
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  msg  = myname + ': Test 4: HAS_HEADER_ROW is an empty string value (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////         .post(siteURL + loadServerEndPoint,
////               {
////                   "LOAD_SOURCE" : "SERVER",
////                   "SERVER" : {
////                       "SERVER_FILE_PATH": "tiny_table.csv"
////                   },
////                   "LOAD_PARAMETERS" : {
////                       "SCHEMA" : tableSchema,
////                       "TABLE" : tableName,
////                       "HAS_HEADER_ROW": ""
////                   }
////               },
////               {json : true})
////         .auth(user, pass)
////         .expectStatus(400)
////         .timeout(60000)
////         .expectHeaderContains('Content-Type', 'json')
////         .inspectJSON()
////         .expectJSON(
////                     {
////                         errorMessageCode : "ERROR",
////                         resultCode : "ERROR",
////                         message: 'Invalid value for parameter hasHeaderRow. Valid values are "true" and "false".',
////                         result : []
////                     })
////        .toss();
////  
////  
////  
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  msg  = myname + ': Test 5: HAS_HEADER_ROW is a blank string value (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////         .post(siteURL + loadServerEndPoint,
////               {
////                   "LOAD_SOURCE" : "SERVER",
////                   "SERVER" : {
////                       "SERVER_FILE_PATH": "tiny_table.csv"
////                   },
////                   "LOAD_PARAMETERS" : {
////                       "SCHEMA" : tableSchema,
////                       "TABLE" : tableName,
////                       "HAS_HEADER_ROW": " "
////                   }
////               },
////               {json : true})
////         .auth(user, pass)
////         .expectStatus(400)
////         .timeout(60000)
////         .expectHeaderContains('Content-Type', 'json')
////         .inspectJSON()
////         .expectJSON(
////                     {
////                         errorMessageCode : "ERROR",
////                         resultCode : "ERROR",
////                         message: 'Invalid value for parameter hasHeaderRow. Valid values are "true" and "false".',
////                         result : []
////                     })
////        .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg  = myname + ': Test 6: HAS_HEADER_ROW is a JSON object (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
       .post(siteURL + loadServerEndPoint,
             {
                 "LOAD_SOURCE" : "SERVER",
                 "SERVER" : {
                     "SERVER_FILE_PATH": "tiny_table.csv"
                 },
                 "LOAD_PARAMETERS" : {
                     "SCHEMA" : tableSchema,
                     "TABLE" : tableName,
                     "HAS_HEADER_ROW": {"blah": "blib"}
                 }
             },
             {json : true})
       .auth(user, pass)
       .expectStatus(400)
       .timeout(60000)
       .expectHeaderContains('Content-Type', 'json')
       .inspectJSON()
       .expectJSON(
                   {
                       errorMessageCode : "ERROR",
                       resultCode : "ERROR",
                       message: "Invalid value for parameter 'has_header_row'. Valid values are \"yes\" and \"no\".",
                       result : []
                   })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg  = myname + ': Test 7: HAS_HEADER_ROW is an empty JSON object (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
       .post(siteURL + loadServerEndPoint,
             {
                 "LOAD_SOURCE" : "SERVER",
                 "SERVER" : {
                     "SERVER_FILE_PATH": "tiny_table.csv"
                 },
                 "LOAD_PARAMETERS" : {
                     "SCHEMA" : tableSchema,
                     "TABLE" : tableName,
                     "HAS_HEADER_ROW": {}
                 }
             },
             {json : true})
       .auth(user, pass)
       .expectStatus(400)
       .timeout(60000)
       .expectHeaderContains('Content-Type', 'json')
       .inspectJSON()
       .expectJSON(
                   {
                       errorMessageCode : "ERROR",
                       resultCode : "ERROR",
                       message: "Invalid value for parameter 'has_header_row'. Valid values are \"yes\" and \"no\".",
                       result : []
                   })
      .toss();


      
////////////////////////////////////////////////////////////////////////////////
//
msg  = myname + ': Test 8: HAS_HEADER_ROW is a JSON array (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
       .post(siteURL + loadServerEndPoint,
             {
                 "LOAD_SOURCE" : "SERVER",
                 "SERVER" : {
                     "SERVER_FILE_PATH": "tiny_table.csv"
                 },
                 "LOAD_PARAMETERS" : {
                     "SCHEMA" : tableSchema,
                     "TABLE" : tableName,
                     "HAS_HEADER_ROW": ["blah", "blib"]
                 }
             },
             {json : true})
       .auth(user, pass)
       .expectStatus(400)
       .timeout(60000)
       .expectHeaderContains('Content-Type', 'json')
       .inspectJSON()
       .expectJSON(
                   {
                       errorMessageCode : "ERROR",
                       resultCode : "ERROR",
                       message: "Invalid value for parameter 'has_header_row'. Valid values are \"yes\" and \"no\".",
                       result : []
                   })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg  = myname + ': Test 9: HAS_HEADER_ROW is an empty JSON array (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
       .post(siteURL + loadServerEndPoint,
             {
                 "LOAD_SOURCE" : "SERVER",
                 "SERVER" : {
                     "SERVER_FILE_PATH": "tiny_table.csv"
                 },
                 "LOAD_PARAMETERS" : {
                     "SCHEMA" : tableSchema,
                     "TABLE" : tableName,
                     "HAS_HEADER_ROW": []
                 }
             },
             {json : true})
       .auth(user, pass)
       .expectStatus(400)
       .timeout(60000)
       .expectHeaderContains('Content-Type', 'json')
       .inspectJSON()
       .expectJSON(
                   {
                       errorMessageCode : "ERROR",
                       resultCode : "ERROR",
                       message: "Invalid value for parameter 'has_header_row'. Valid values are \"yes\" and \"no\".",
                       result : []
                   })
      .toss();




/////////////////////////////////////////////////////////////////////////////////////
// 
msg  = myname + ': Test 10: XLSX file with HAS_HEADER_ROW:TRUE';
//
/////////////////////////////////////////////////////////////////////////////////////
var numRows2 = 1000; // need a new variable as numRows is used in callbacks in other tests so we can't modify it here
frisby.create(msg)
     .post(siteURL + loadServerEndPoint , 
           {
             "LOAD_SOURCE": "SERVER",
             "SERVER": {
               "SERVER_FILE_PATH": "order_details_header_small.xlsx"
             },
             "LOAD_PARAMETERS": {
               "SCHEMA": tableSchema,
               "TABLE": "ORDER_DETAILS",
               "HAS_HEADER_ROW": "TRUE",
             }
           },
           {json: true })
     .auth(user, pass)
     .expectStatus(200)
     .timeout(30000)
     .expectHeaderContains('Content-Type', 'json')
     .inspectBody()
     .expectJSON({
                     errorMessageCode: "NONE",
                     resultCode: "SUCCESS",
                     message: 'LOAD started successfully.',
                     result: {
                         TABLE: "ORDER_DETAILS",
                         SCHEMA: tableSchema,
                         LOAD_STATUS: 'RUNNING'
                     }
                 })
     .afterJSON(function (json) {
                    loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                    var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                    loadUtils.validateLoadResult(bodyJSON, numRows2, numRows2, numRows2, 0, 0);
                })
     .toss();




/////////////////////////////////////////////////////////////////////////////////////
// 
msg  = myname + ': Test 11: XLS file with HAS_HEADER_ROW:TRUE';
//
/////////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
     .post(siteURL + loadServerEndPoint , 
           {
             "LOAD_SOURCE": "SERVER",
             "SERVER": {
               "SERVER_FILE_PATH": "order_details_header_small.xls"
             },
             "LOAD_PARAMETERS": {
               "SCHEMA": tableSchema,
               "TABLE": "ORDER_DETAILS2",
               "HAS_HEADER_ROW": "TRUE",
             }
           },
           {json: true })
     .auth(user, pass)
     .expectStatus(200)
     .timeout(30000)
     .expectHeaderContains('Content-Type', 'json')
     .inspectBody()
     .expectJSON({
                     errorMessageCode: "NONE",
                     resultCode: "SUCCESS",
                     message: 'LOAD started successfully.',
                     result: {
                         TABLE: "ORDER_DETAILS2",
                         SCHEMA: tableSchema,
                         LOAD_STATUS: 'RUNNING'
                     }
                 })
     .afterJSON(function (json) {
                    loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                    var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                    loadUtils.validateLoadResult(bodyJSON, numRows2, numRows2, numRows2, 0, 0);
                })
     .toss();



/////////////////////////////////////////////////////////////////////////////////////
// 
msg  = myname + ': Test 12: XLSX file with HAS_HEADER_ROW:FALSE';
//
/////////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
     .post(siteURL + loadServerEndPoint , 
           {
             "LOAD_SOURCE": "SERVER",
             "SERVER": {
               "SERVER_FILE_PATH": "order_details_small.xlsx"
             },
             "LOAD_PARAMETERS": {
               "SCHEMA": tableSchema,
               "TABLE": "ORDER_DETAILS3",
               "HAS_HEADER_ROW": "FALSE",
             }
           },
           {json: true })
     .auth(user, pass)
     .expectStatus(200)
     .timeout(30000)
     .expectHeaderContains('Content-Type', 'json')
     .inspectBody()
     .expectJSON({
                     errorMessageCode: "NONE",
                     resultCode: "SUCCESS",
                     message: 'LOAD started successfully.',
                     result: {
                         TABLE: "ORDER_DETAILS3",
                         SCHEMA: tableSchema,
                         LOAD_STATUS: 'RUNNING'
                     }
                 })
     .afterJSON(function (json) {
                    loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                    var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                    loadUtils.validateLoadResult(bodyJSON, numRows2, numRows2, numRows2, 0, 0);
                })
     .toss();




/////////////////////////////////////////////////////////////////////////////////////
// 
msg  = myname + ': Test 13: XLS file with HAS_HEADER_ROW:FALSE';
//
/////////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
     .post(siteURL + loadServerEndPoint , 
           {
             "LOAD_SOURCE": "SERVER",
             "SERVER": {
               "SERVER_FILE_PATH": "order_details_small.xls"
             },
             "LOAD_PARAMETERS": {
               "SCHEMA": tableSchema,
               "TABLE": "ORDER_DETAILS4",
               "HAS_HEADER_ROW": "FALSE",
             }
           },
           {json: true })
     .auth(user, pass)
     .expectStatus(200)
     .timeout(30000)
     .expectHeaderContains('Content-Type', 'json')
     .inspectBody()
     .expectJSON({
                     errorMessageCode: "NONE",
                     resultCode: "SUCCESS",
                     message: 'LOAD started successfully.',
                     result: {
                         TABLE: "ORDER_DETAILS4",
                         SCHEMA: tableSchema,
                         LOAD_STATUS: 'RUNNING'
                     }
                 })
     .afterJSON(function (json) {
                    loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                    var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                    loadUtils.validateLoadResult(bodyJSON, numRows2, numRows2, numRows2, 0, 0);
                })
     .toss();
