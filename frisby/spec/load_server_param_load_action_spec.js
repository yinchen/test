//////////////////////////////////////////////////////
// ENDPOINT - POST /load/server
// TEST - test the LOAD_ACTION load parameter.
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var server = require('../lib/server.js');
var path = require('path');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var tableSchema = server.tabSchema();
var tableName = "TINY_TABLE";
var fileName = "tiny_table.csv";
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);


//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
})


////  COMMENTED-OUT the following tests have been commented out until the following is fixed: https://github.ibm.com/CloudDataServices/dashdb-dsserver/issues/159
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  var msg = myname + ': Test 1: invalid value - number - for LOAD_ACTION (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": fileName
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "LOAD_ACTION": 13
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'blab',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
////  
////  
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  var msg = myname + ': Test 2: invalid value - string - for LOAD_ACTION (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": fileName
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "LOAD_ACTION": "hello"
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'blab',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
////  
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  var msg = myname + ': Test 3: invalid value - JSON object - for LOAD_ACTION (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": fileName
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "LOAD_ACTION": {"hello":"there"}
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'blab',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
////  
////  
////  
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  var msg = myname + ': Test 4: invalid value - JSON array - for LOAD_ACTION (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": fileName
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "LOAD_ACTION": ["hello"]
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'blab',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
////  
////  
////  
////  
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  var msg = myname + ': Test 5: LOAD_ACTION value is an empty string (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": fileName
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "LOAD_ACTION": ""
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'blab',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();
////  
////  
////  
////  
////  ////////////////////////////////////////////////////////////////////////////////
////  //
////  var msg = myname + ': Test 6: LOAD_ACTION value is a blank string (negative test)';
////  //
////  ////////////////////////////////////////////////////////////////////////////////
////  frisby.create(msg)
////        .post(siteURL + loadServerEndPoint ,
////              {
////                  "LOAD_SOURCE": "SERVER",
////                  "SERVER": {
////                      "SERVER_FILE_PATH": fileName
////                  },
////                  "LOAD_PARAMETERS": {
////                      "SCHEMA": tableSchema,
////                      "TABLE": tableName,
////                      "LOAD_ACTION": " "
////                  }
////              },
////              {json: true })
////        .auth(user, pass)
////        .expectStatus(400)
////        .timeout(30000)
////        .expectHeaderContains('Content-Type', 'json')
////        .inspectJSON()
////        .expectJSON(
////                    {
////                        message: 'blab',
////                        errorMessageCode: 'ERROR',
////                        resultCode: 'ERROR',
////                        result: []
////                    })
////        .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 7: test that a REPLACE will clobber existing data';
//
////////////////////////////////////////////////////////////////////////////////
var expectedStats = {
    numRowsRead : 12,
    numRowsLoaded : 12,
    numRowsCommitted : 12,
    numRowsSkipped : 0,
    numRowsRejected : 0,
    numRowsDeleted : 0 };
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: tableName,
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, expectedStats);
                     loadUtils.verifyAllTableContents(tableSchema, tableName, server.tiny_table_expected_rows);
                 })
      .toss();


// I now that ideally we should truncate the table first and subsequent actions should be done in callbacks,
// however frisby doesn't work well when called from synchronous callbacks. I think what we have is sufficient
////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 8: test that a INSERT appends data';
//
////////////////////////////////////////////////////////////////////////////////
var expectedRowsInsert = [];
// We first did a load replace of file then load insert
// expect two of each row
for(i = 0 ; i < server.tiny_table_expected_rows.length; ++i) {
    expectedRowsInsert.push(server.tiny_table_expected_rows[i], server.tiny_table_expected_rows[i]);
}
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "INSERT",
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: tableName,
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, expectedStats);
                     loadUtils.verifyAllTableContents(tableSchema, tableName, expectedRowsInsert);
                 })
      .toss();


// the last test will have inserted 24 rows. If we do a replace here, we should get 12
// in total in the table
////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 9: test that a REPLACE will clobber existing data (again)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "LOAD_ACTION": "REPLACE",
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: tableName,
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, expectedStats);
                     loadUtils.verifyAllTableContents(tableSchema, tableName, server.tiny_table_expected_rows);
                 })
      .toss();


////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 10: test that when LOAD_ACTION is not specified, an INSERT is done';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: tableName,
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, expectedStats);
                     loadUtils.verifyAllTableContents(tableSchema, tableName, expectedRowsInsert);
                 })
      .toss();

