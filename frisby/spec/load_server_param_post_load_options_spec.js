//////////////////////////////////////////////////////
// ENDPOINT - POST /load/server
// TEST - test the post load options. The db2 load parameters after (INSERT|REPLACE|TERMINATE|RESTART) INTO TABLE
// COMMENT - none

var frisby = require('frisby');
var server = require('../lib/server.js');
var path = require('path');
var expect = require('expect');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var tableName = "TINY_TABLE";
var tableSchema = server.tabSchema();
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);

//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
})

var numRowsRead = 12;
var numRowsLoaded = 12;
var numRowsCommitted = 12;
var numRowsRejected = 0;
var numRowsSkipped = 0;
var numRowsDeleted = 0;
////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 1: valid options';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
    .post(siteURL + loadServerEndPoint,
    {
        "LOAD_SOURCE": "SERVER",
        "SERVER": {
            "SERVER_FILE_PATH": "tiny_table.csv"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": tableName,
            "CUSTOM_POST_LOAD_OPTIONS": ["DATA BUFFER 40000"]
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(200)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        errorMessageCode: 'NONE',
        resultCode: 'SUCCESS',
        result: {
            TABLE: tableName,
            SCHEMA: tableSchema
        }
    })
    .afterJSON(function (json) {
        loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 30);
        var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
        loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsSkipped, numRowsDeleted);
    })
    .toss();

////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 2: invalid options, invalid value';
//
////////////////////////////////////////////////////////////////////////////////
expectedJson = {
    numRowsRead : 2,
    numRowsLoaded : 2,
    numRowsCommitted : 2,
    numRowsSkipped : 0,
    numRowsRejected : 0,
    numRowsDeleted : 0,
    errorMessageCode: "ERROR"}
frisby.create(msg)
    .post(siteURL + loadServerEndPoint,
    {
        "LOAD_SOURCE": "SERVER",
        "SERVER": {
            "SERVER_FILE_PATH": "tiny_table.csv"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": tableName,
            "CUSTOM_POST_LOAD_OPTIONS": ["INVALID OPTIONS"]
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(200)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
        {
            errorMessageCode: 'NONE',
            resultCode: 'SUCCESS',
            result: {
                TABLE: tableName,
                SCHEMA: tableSchema
            }
        })
        .afterJSON(function (json) {
        loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 30);
        var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
        //loadUtils.validateLoadResult(bodyJSON, '', '', '', '', '', 'ERROR');
    })
    .toss();

////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 3: invalid options, invalid data type - integer';
//
////////////////////////////////////////////////////////////////////////////////
expectedJson = {
    numRowsRead : 2,
    numRowsLoaded : 2,
    numRowsCommitted : 2,
    numRowsSkipped : 0,
    numRowsRejected : 0,
    numRowsDeleted : 0,
    errorMessageCode: "ERROR"}
frisby.create(msg)
    .post(siteURL + loadServerEndPoint,
    {
        "LOAD_SOURCE": "SERVER",
        "SERVER": {
            "SERVER_FILE_PATH": "tiny_table.csv"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": tableName,
            "CUSTOM_POST_LOAD_OPTIONS": 7
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        errorMessageCode: 'ERROR',
        resultCode: 'ERROR',
        result: [],
        message:"CUSTOM_POST_LOAD_OPTIONS is expected to be a json array."
    })
    .toss();

////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 4: invalid options, invalid data type - string';
//
////////////////////////////////////////////////////////////////////////////////
expectedJson = {
    numRowsRead : 2,
    numRowsLoaded : 2,
    numRowsCommitted : 2,
    numRowsSkipped : 0,
    numRowsRejected : 0,
    numRowsDeleted : 0,
    errorMessageCode: "ERROR"}
frisby.create(msg)
    .post(siteURL + loadServerEndPoint,
    {
        "LOAD_SOURCE": "SERVER",
        "SERVER": {
            "SERVER_FILE_PATH": "tiny_table.csv"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": tableName,
            "CUSTOM_POST_LOAD_OPTIONS": "DATA BUFFER 50000"
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        errorMessageCode: 'ERROR',
        resultCode: 'ERROR',
        result: [],
        message: "CUSTOM_POST_LOAD_OPTIONS is expected to be a json array."
    })
    .toss();


////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 5: invalid options, empty array';
//
////////////////////////////////////////////////////////////////////////////////
expectedJson = {
    numRowsRead : 2,
    numRowsLoaded : 2,
    numRowsCommitted : 2,
    numRowsSkipped : 0,
    numRowsRejected : 0,
    numRowsDeleted : 0,
    errorMessageCode: "ERROR"}
frisby.create(msg)
    .post(siteURL + loadServerEndPoint,
    {
        "LOAD_SOURCE": "SERVER",
        "SERVER": {
            "SERVER_FILE_PATH": "tiny_table.csv"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": tableName,
            "CUSTOM_POST_LOAD_OPTIONS": []
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        errorMessageCode: 'ERROR',
        resultCode: 'ERROR',
        result: [],
        message: "CUSTOM_POST_LOAD_OPTIONS array is empty."
    })
    .toss();

////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 6: invalid options, invalid type json';
//
////////////////////////////////////////////////////////////////////////////////
expectedJson = {
    numRowsRead : 2,
    numRowsLoaded : 2,
    numRowsCommitted : 2,
    numRowsSkipped : 0,
    numRowsRejected : 0,
    numRowsDeleted : 0,
    errorMessageCode: "ERROR"}
frisby.create(msg)
    .post(siteURL + loadServerEndPoint,
    {
        "LOAD_SOURCE": "SERVER",
        "SERVER": {
            "SERVER_FILE_PATH": "tiny_table.csv"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": tableName,
            "CUSTOM_POST_LOAD_OPTIONS": {value: "DATA BUFFER 50000"}
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        errorMessageCode: 'ERROR',
        resultCode: 'ERROR',
        result: [],
        message: "CUSTOM_POST_LOAD_OPTIONS is expected to be a json array."
    })
    .toss();


////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 6: invalid options, invalid type in json arry, an element is not string';
//
////////////////////////////////////////////////////////////////////////////////
expectedJson = {
    numRowsRead : 2,
    numRowsLoaded : 2,
    numRowsCommitted : 2,
    numRowsSkipped : 0,
    numRowsRejected : 0,
    numRowsDeleted : 0,
    errorMessageCode: "ERROR"}
frisby.create(msg)
    .post(siteURL + loadServerEndPoint,
    {
        "LOAD_SOURCE": "SERVER",
        "SERVER": {
            "SERVER_FILE_PATH": "tiny_table.csv"
        },
        "LOAD_PARAMETERS": {
            "SCHEMA": tableSchema,
            "TABLE": tableName,
            "CUSTOM_POST_LOAD_OPTIONS": ["DATA BUFFER 50000", 123]
        }
    },
    { json: true })
    .auth(user, pass)
    .expectStatus(400)
    .timeout(30000)
    .expectHeaderContains('Content-Type', 'json')
    .inspectJSON()
    .expectJSON(
    {
        errorMessageCode: 'ERROR',
        resultCode: 'ERROR',
        result: [],
        message: "All elements in CUSTOM_POST_LOAD_OPTIONS are expected to be strings but an element's type is java.lang.Integer"
    })
    .toss();    
