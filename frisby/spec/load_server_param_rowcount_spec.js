//////////////////////////////////////////////////////
// ENDPOINT - POST /load/server
// TEST - test the ROWCOUNT load parameter. Will skip for MPP machines as the parameter is not valid
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var server = require('../lib/server.js');
var path = require('path');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var tableSchema = server.tabSchema();
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);
var nodeType = server.nodeType(); // SMP or MPP


//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
})


// ROWCOUNT is not supported on MPP and in an MPP instance, we will get an error 
// as such when we use the ROWCOUNT parameter
console.log("nodeType is :" + nodeType)
var expectedErrMessage =  nodeType == 'MPP' ?
          "SQL27961N: ROWCOUNT cannot be specified in a partitioned database environment."
        : "Invalid value for parameter 'max_row_count'. Review the REST API documentation for possible values.";

expectedErrMessage = "Invalid value for parameter 'max_row_count'. Review the REST API documentation for possible values."
////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 1: rowcount value of -1 (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "order_details_1.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "MYTABLE",
                    "ROWCOUNT": "-1"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: expectedErrMessage,
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();


////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 2: rowcount value of 0 (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "order_details_1.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "MYTABLE",
                    "ROWCOUNT": "0"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  { 
                      message: expectedErrMessage,
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();

////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 3: rowcount value too large (100000000000000000) (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "order_details_1.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "MYTABLE",
                    "ROWCOUNT": "100000000000000000"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  { 
                      message: expectedErrMessage,
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();


////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 4: rowcount value is an alphabetic string (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "order_details_1.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "MYTABLE",
                    "ROWCOUNT": "fred"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  { 
                      message: expectedErrMessage,
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();


////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 5: rowcount value is a number with decimal point (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "order_details_1.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "MYTABLE",
                    "ROWCOUNT": "5.9"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  { 
                      message: expectedErrMessage,
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();

// if(nodeType != 'MPP') {
//     ////////////////////////////////////////////////////////////////////////////////
//     //
//     var msg = myname + ': Test 6: test correct behaviour of ROWCOUNT';
//     //
//     ////////////////////////////////////////////////////////////////////////////////
//     var rowcount = 5;
//     var numRowsRead = rowcount;
//     var numRowsLoaded = rowcount;
//     var numRowsCommitted = rowcount;
//     var numRowsRejected = 0;
//     var numRowsSkipped = 0;
//     var numRowsDeleted = 0;
//     var tableName = "TINY_TABLE";
//     
//     frisby.create(msg)
//            .post(siteURL + loadServerEndPoint,
//                  {
//                      "LOAD_SOURCE" : "SERVER",
//                      "SERVER" : {
//                          "SERVER_FILE_PATH": "tiny_table.csv"
//                      },
//                      "LOAD_PARAMETERS" : {
//                          "SCHEMA" : tableSchema,
//                          "TABLE" : tableName,
//                          "ROWCOUNT": rowcount 
//                      }
//                  },
//                  {json : true})
//            .auth(user, pass)
//            .expectStatus(200)
//            .timeout(60000)
//            .expectHeaderContains('Content-Type', 'json')
//            .inspectJSON()
//            .expectJSON(
//                        {
//                            errorMessageCode : "NONE",
//                            resultCode : "SUCCESS",
//                            result : {
//                                TABLE : tableName,
//                                SCHEMA : tableSchema
//                            }
//                        })
//           .afterJSON(function(json) {
//                 loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
//                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
//                     loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsSkipped, numRowsDeleted);
//                 })
//           .toss();
// }



////////////////////////////////////////////////////////////////////////////////
//
msg  = myname + ': Test 7: rowcount is an empty string value (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE" : "MYTABLE",
                    "ROWCOUNT": ""
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(400)
      .timeout(60000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      errorMessageCode : "ERROR",
                      resultCode : "ERROR",
                      message: expectedErrMessage,
                      result : []
                  })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg  = myname + ': Test 8: ROWCOUNT is a blank string value (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE" : "MYTABLE",
                    "ROWCOUNT": " "
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(400)
      .timeout(60000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      errorMessageCode : "ERROR",
                      resultCode : "ERROR",
                      message: expectedErrMessage,
                      result : []
                  })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg  = myname + ': Test 9: ROWCOUNT is a JSON object (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE" : "MYTABLE",
                    "ROWCOUNT": {"blah": "blib"}
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(400)
      .timeout(60000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      errorMessageCode : "ERROR",
                      resultCode : "ERROR",
                      message: expectedErrMessage,
                      result : []
                  })
      .toss();


////////////////////////////////////////////////////////////////////////////////
//
msg  = myname + ': Test 10: ROWCOUNT is a JSON array (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE" : "MYTABLE",
                    "ROWCOUNT": ["blah", "blib"]
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(400)
      .timeout(60000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      errorMessageCode : "ERROR",
                      resultCode : "ERROR",
                      message: expectedErrMessage,
                      result : []
                  })
      .toss();
