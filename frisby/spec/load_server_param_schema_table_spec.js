//////////////////////////////////////////////////////
// ENDPOINT - POST /load/server
// TEST - test the SCHEMA and TABLE load parameters
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var server = require('../lib/server.js');
var ibmdb = require('ibm_db');
var path = require('path');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var tableName = '"TINY_TABLE_1.0"';
var tableSchema = server.tabSchema();
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);
var connString = server.connString();
var smpType = server.smpType();

//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
})

var numRowsRead = 12;
var numRowsLoaded = 12;
var numRowsCommitted = 12;
var numRowsRejected = 0;
var numRowsSkipped = 0;
var numRowsDeleted = 0;

////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 1: table with special character (dot) in table name';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE" : tableName
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(200)
      .timeout(60000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'LOAD started successfully.',
                      errorMessageCode : "NONE",
                      resultCode : "SUCCESS",
                      result : {
                          TABLE : tableName,
                          SCHEMA : tableSchema
                      }
                  })
     .afterJSON(function(json) {
                    loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                    var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                    loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsSkipped, numRowsDeleted);
                })
     .toss();

////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 2: schema with double quotes';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : '"' + tableSchema + '"',
                    "TABLE" : tableName
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'LOAD started successfully.',
                      errorMessageCode : "NONE",
                      resultCode : "SUCCESS",
                      result : {
                          TABLE : tableName,
                          SCHEMA : '"' + tableSchema + '"'
                      }
                  })
      .afterJSON(function(json) {
            loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsSkipped, numRowsDeleted);
            })
      .toss();

////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 3: unquoted table name contains dot (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE" : "TINY_TABLE_1.0"
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Ordinary (unquoted) identifiers can only contain letters, digits and the underscore character and must start with a letter. Identifier TINY_TABLE_1.0 should be quoted',
                      errorMessageCode : "ERROR",
                      resultCode : "ERROR",
                      result: []
                  })
      .toss();

////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 4: quoted table name starting with special character';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE" : "\"*Tiny_Table\""
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(200)
      .timeout(60000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'LOAD started successfully.',
                      errorMessageCode : "NONE",
                      resultCode : "SUCCESS",
                      result : {
                          TABLE : "\"*Tiny_Table\"",
                          SCHEMA : tableSchema
                      }
                  })
      .afterJSON(function(json) {
            loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsSkipped, numRowsDeleted);
            })
      .toss();

////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 5: un-quoted table name starting with special character (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE" : "*Tiny_Table"
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Table does not exist: ' + tableSchema + '.*Tiny_Table',
                      message: 'Ordinary (unquoted) identifiers can only contain letters, digits and the underscore character and must start with a letter. Identifier *Tiny_Table should be quoted',
                      errorMessageCode : "ERROR",
                      resultCode : "ERROR",
                       result: []
                  })
      .toss();

////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 6: load "TinyTable" where there exists TinyTable in the catalog';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "\"TinyTable\"",
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'LOAD started successfully.',
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: "\"TinyTable\"",
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
            loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsSkipped, numRowsDeleted);
       })
      .toss();

//common out this test case, since we add double quote for table name when doing load to handle special register, 
//this is conflict with this case.
////////////////////////////////////////////////////////////////////////////////
//
// msg = myname + ': Test 7: load Tiny_Table where there exists TINY_TABLE in the catalog';
//
////////////////////////////////////////////////////////////////////////////////
// frisby.create(msg)
//       .post(siteURL + loadServerEndPoint,
//             {
//                 "LOAD_SOURCE": "SERVER",
//                 "SERVER": {
//                     "SERVER_FILE_PATH": "tiny_table.csv"
//                 },
//                 "LOAD_PARAMETERS": {
//                     "SCHEMA": tableSchema,
//                     "TABLE": "Tiny_Table",
//                 }
//             },
//             {json: true })
//       .auth(user, pass)
//       .expectStatus(200)
//       .timeout(30000)
//       .expectHeaderContains('Content-Type', 'json')
//       .inspectJSON()
//       .expectJSON(
//                   {
//                       message: 'LOAD started successfully.',
//                       errorMessageCode: "NONE",
//                       resultCode: "SUCCESS",
//                       result: {
//                           TABLE: "Tiny_Table",
//                           SCHEMA: tableSchema
//                       }
//                   })
//       .afterJSON(function (json) {
//             loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
//                 var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
//                 loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsSkipped, numRowsDeleted);
//       })
//       .toss();

////////////////////////////////////////////////////////////////////////////////
//
// msg = myname + ': Test 8: load "Tiny_Table" where there exists TINY_TABLE in the catalog (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
// frisby.create(msg)
//       .post(siteURL + loadServerEndPoint,
//             {
//                 "LOAD_SOURCE": "SERVER",
//                 "SERVER": {
//                     "SERVER_FILE_PATH": "tiny_table.csv"
//                 },
//                 "LOAD_PARAMETERS": {
//                     "SCHEMA": tableSchema,
//                     "TABLE": "\"Tiny_Table\"",
//                 }
//             },
//             {json: true })
//       .auth(user, pass)
//       .expectStatus(400)
//       .timeout(30000)
//       .expectHeaderContains('Content-Type', 'json')
//       .inspectJSON()
//       .expectJSON(
//                   {
//                       message: 'Table does not exist: ' + tableSchema + '."Tiny_Table"',
//                       errorMessageCode: "ERROR",
//                       resultCode: "ERROR",
//                       result: []
//                   })
//       .toss();

////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 9: load TinyTable where there exists TinyTable in the catalog (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "TinyTable",
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'Table does not exist: ' + tableSchema + '.TinyTable',
                      errorMessageCode: "ERROR",
                      resultCode: "ERROR",
                      result: []
                  })
      .toss();

////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 10: empty SCHEMA (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "order_details_1.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": "",
                    "TABLE": "ORDER_DETAILS",
                },
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'SCHEMA is empty',
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .afterJSON(function (json) {
            console.log(json);
        })
      .toss();


  
////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 11: SCHEMA is a blank string (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
tableName = 'TINY_TABLE';
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : " ",
                    "TABLE" : tableName,
                    "LOAD_ACTION": "REPLACE"
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'SCHEMA is empty',
                      errorMessageCode : "ERROR",
                      resultCode : "ERROR",
                      result : []
                  })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg  = myname + ': Test 11.1: SCHEMA is a JSON object (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "TABLE" : tableName,
                    "SCHEMA": {"blah": "blib"}
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(400)
      .timeout(60000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      errorMessageCode : "ERROR",
                      resultCode : "ERROR",
                      message: 'Ordinary (unquoted) identifiers can only contain letters, digits and the underscore character and must start with a letter. Identifier {"blah":"blib"} should be quoted',
                      result : []
                  })
      .toss();
     
      

////////////////////////////////////////////////////////////////////////////////
//
msg  = myname + ': Test 11.2: SCHEMA is a JSON array (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "TABLE" : tableName,
                    "SCHEMA": ["blah", "blib"]
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(400)
      .timeout(60000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      errorMessageCode : "ERROR",
                      resultCode : "ERROR",
                      message: 'Ordinary (unquoted) identifiers can only contain letters, digits and the underscore character and must start with a letter. Identifier ["blah","blib"] should be quoted',
                      result : []
                  })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 12: TABLE is a blank string (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE" : " ",
                    "LOAD_ACTION": "REPLACE"
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'TABLE is empty',
                      errorMessageCode : "ERROR",
                      resultCode : "ERROR",
                      result : []
                  })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 13: empty TABLE (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "order_details_1.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "",
                },
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'TABLE is empty',
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg  = myname + ': Test 13.1: TABLE is a JSON object (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE": {"blah": "blib"}
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(400)
      .timeout(60000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      errorMessageCode : "ERROR",
                      resultCode : "ERROR",
                      message: 'Ordinary (unquoted) identifiers can only contain letters, digits and the underscore character and must start with a letter. Identifier {"blah":"blib"} should be quoted',
                      result : []
                  })
      .toss();
     
      

////////////////////////////////////////////////////////////////////////////////
//
msg  = myname + ': Test 13.1: TABLEE is a JSON array (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE": ["blah", "blib"]
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(400)
      .timeout(60000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      errorMessageCode : "ERROR",
                      resultCode : "ERROR",
                      message: 'Ordinary (unquoted) identifiers can only contain letters, digits and the underscore character and must start with a letter. Identifier ["blah","blib"] should be quoted',
                      result : []
                  })
      .toss();






//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 14: missing SCHEMA (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "order_details_1.csv"
                },
                "LOAD_PARAMETERS": {
                    "TABLE": "ORDER_DETAILS",
                },
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'The value for key: [SCHEMA] was null.  Object required.',
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();



//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 15: missing TABLE (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "order_details_1.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                },
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'The value for key: [TABLE] was null.  Object required.',
                      errorMessageCode: 'ERROR',
                      resultCode: 'ERROR',
                      result: []
                  })
      .toss();




//////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 16: Table that does not exist (negative test)';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint ,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "order_details_1.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "GRAMOCHROI"
                }
            },
            {json: true })
        .auth(user, pass)
        .expectStatus(400)
        .timeout(30000)
        .expectHeaderContains('Content-Type', 'json')
        .inspectJSON()
        .expectJSON(
                    {
                        message: 'Table does not exist: ' + tableSchema + '.GRAMOCHROI',
                        errorMessageCode: 'ERROR',
                        resultCode: 'ERROR',
                        result: []
                    })
        .toss();


////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 17: specify table with trailing whitespace (ordinary identifier) in JSON input (table name in catalogs does not have trailing whitespace)';
//
////////////////////////////////////////////////////////////////////////////////
tableName = 'TINY_TABLE  ';
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE" : tableName,
                    "LOAD_ACTION": "REPLACE"
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'LOAD started successfully.',
                      errorMessageCode : "NONE",
                      resultCode : "SUCCESS",
                      result : {
                          SCHEMA : tableSchema,
                          TABLE : tableName
                      }
                  })
      .afterJSON(function(json) {
            loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsSkipped, numRowsDeleted);
            })
      .toss();

////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 18: specify table with trailing whitespace (delimited identifier) in JSON input (table name in catalogs does not have trailing whitespace)';
//
////////////////////////////////////////////////////////////////////////////////
tableName = '"TINY_TABLE  "';
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE" : tableName,
                    "LOAD_ACTION": "REPLACE"
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'LOAD started successfully.',
                      errorMessageCode : "NONE",
                      resultCode : "SUCCESS",
                      result : {
                          SCHEMA : tableSchema,
                          TABLE : '"TINY_TABLE  "'
                      }
                  })
      .afterJSON(function(json) {
            loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsSkipped, numRowsDeleted);
            })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 19: table name in input JSON has no trailing whitespace (delimited identifier), table name in catalogs has trailing whitespace)';
//
////////////////////////////////////////////////////////////////////////////////
tableName = '"TAB_WITH_TRAILING_SPACE"';
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE" : tableName,
                    "LOAD_ACTION": "REPLACE"
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'LOAD started successfully.',
                      errorMessageCode : "NONE",
                      resultCode : "SUCCESS",
                      result : {
                          SCHEMA : tableSchema,
                          TABLE : tableName
                      }
                  })
      .afterJSON(function(json) {
            loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsSkipped, numRowsDeleted);
            })
      .toss();




////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 20: table name in input JSON has trailing whitespace (delimited identifier), table name in catalogs has trailing whitespace)';
//
////////////////////////////////////////////////////////////////////////////////
tableName = '"TAB_WITH_TRAILING_SPACE   "';
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE" : tableName,
                    "LOAD_ACTION": "REPLACE"
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'LOAD started successfully.',
                      errorMessageCode : "NONE",
                      resultCode : "SUCCESS",
                      result : {
                          SCHEMA : tableSchema,
                          TABLE : tableName
                      }
                  })
      .afterJSON(function(json) {
            loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsSkipped, numRowsDeleted);
            })
      .toss();




////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 21: table name in input JSON has no trailing whitespace (ordinary identifier), table name in catalogs has trailing whitespace)';
//
////////////////////////////////////////////////////////////////////////////////
tableName = 'TAB_WITH_TRAILING_SPACE';
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE" : tableName,
                    "LOAD_ACTION": "REPLACE"
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'LOAD started successfully.',
                      errorMessageCode : "NONE",
                      resultCode : "SUCCESS",
                      result : {
                          SCHEMA : tableSchema,
                          TABLE : tableName
                      }
                  })
      .afterJSON(function(json) {
            loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsSkipped, numRowsDeleted);
            })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 22: table name in input JSON has trailing whitespace (ordinary identifier), table name in catalogs has trailing whitespace)';
//
////////////////////////////////////////////////////////////////////////////////
tableName = 'TAB_WITH_TRAILING_SPACE  ';
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE" : tableName,
                    "LOAD_ACTION": "REPLACE"
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'LOAD started successfully.',
                      errorMessageCode : "NONE",
                      resultCode : "SUCCESS",
                      result : {
                          SCHEMA : tableSchema,
                          TABLE : tableName
                      }
                  })
      .afterJSON(function(json) {
            loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsSkipped, numRowsDeleted);
            })
      .toss();




////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 23: table name in input JSON has leading whitespace (delimited identifier), table name in catalogs has leading whitespace)';
//
////////////////////////////////////////////////////////////////////////////////
tableName = '"  TAB_WITH_LEADING_SPACE"';
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE" : tableName,
                    "LOAD_ACTION": "REPLACE"
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'LOAD started successfully.',
                      errorMessageCode : "NONE",
                      resultCode : "SUCCESS",
                      result : {
                          SCHEMA : tableSchema,
                          TABLE : tableName
                      }
                  })
      .afterJSON(function(json) {
            loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsSkipped, numRowsDeleted);
            })
      .toss();
 
 

////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 24: table name in input JSON has incorrect amount of leading whitespace (delimited identifier), table name in catalogs has leading whitespace) - negative test';
//
////////////////////////////////////////////////////////////////////////////////
tableName = '"   TAB_WITH_LEADING_SPACE"';
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE" : tableName,
                    "LOAD_ACTION": "REPLACE"
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:  'Table does not exist: ' + tableSchema + '.' + tableName,
                      errorMessageCode : "ERROR",
                      resultCode : "ERROR",
                      result : []
                  })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 25: table name in input JSON has no leading whitespace (delimited identifier), table name in catalogs has leading whitespace) - negative test';
//
////////////////////////////////////////////////////////////////////////////////
tableName = '"TAB_WITH_LEADING_SPACE"';
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE" : tableName,
                    "LOAD_ACTION": "REPLACE"
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:  'Table does not exist: ' + tableSchema + '.' + tableName,
                      errorMessageCode : "ERROR",
                      resultCode : "ERROR",
                      result : []
                  })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 26: table name in input JSON has no leading whitespace (ordinary identifier), table name in catalogs has leading whitespace) - negative test';
//
////////////////////////////////////////////////////////////////////////////////
tableName = 'TAB_WITH_LEADING_SPACE';
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE" : tableName,
                    "LOAD_ACTION": "REPLACE"
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:  'Table does not exist: ' + tableSchema + '.' + tableName,
                      errorMessageCode : "ERROR",
                      resultCode : "ERROR",
                      result : []
                  })
      .toss();




////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 27: table name in input JSON has leading whitespace (ordinary identifier), table name in catalogs has leading whitespace) - negative test';
//
////////////////////////////////////////////////////////////////////////////////
tableName = '  TAB_WITH_LEADING_SPACE';
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE" : "SERVER",
                "SERVER" : {
                    "SERVER_FILE_PATH": "tiny_table.csv"
                },
                "LOAD_PARAMETERS" : {
                    "SCHEMA" : tableSchema,
                    "TABLE" : tableName,
                    "LOAD_ACTION": "REPLACE"
                }
            },
            {json : true})
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message:  'Table does not exist: ' + tableSchema + '.' + tableName,
                      errorMessageCode : "ERROR",
                      resultCode : "ERROR",
                      result : []
                  })
      .toss();

////////////////////////////////////////////////////////////////////////////////
//
msg = myname + ': Test 28: table name has non-ascii characters';
//
////////////////////////////////////////////////////////////////////////////////
var expected = {
    numRowsRead : 9,
    numRowsLoaded : 9,
    numRowsCommitted : 9,
    numRowsSkipped : 0,
    numRowsRejected : 0,
    numRowsDeleted : 0,
    errorMessageCode: "NONE"}
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "Linuxディストリビューション.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": "LINUXディストリビューション",
                    "LOAD_ACTION": "REPLACE",
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: "LOAD started successfully.",
                      errorMessageCode: "NONE",
                      resultCode: "SUCCESS",
                      result: {
                          TABLE: "LINUXディストリビューション",
                          SCHEMA: tableSchema
                      }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     loadUtils.validateLoadResult(bodyJSON, expected);
                 })
      .toss();

////////////////////////////////////////////////////////////////////////////////
// we used to create a hardcoded non-ascii schema and table name for the below test
// but because test users are different across different test executions
// if test A failed without clean up the tables, in test B, the user won't be able
// to drop the tables and load data into them. So we create tables dynamically here
var msg = myname + ': dummy test to set up tables';
var non_ascii_schema = server.userID().toUpperCase() + "ディストリビューション";
var non_ascii_table = "ディストリビューション";


if(smpType != 'ENTRY') {
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + server.asperaEndPoint()+'getUploadSizeCap')
      .auth(user, pass)
      .expectStatus(200)
      .expectHeader('content-type', 'application/json')
      .after(function () {
            var conn = ibmdb.openSync(connString);
            conn.querySync("CREATE TABLE " + non_ascii_schema + ".LINUX (名 VARCHAR(20))");
            conn.querySync("CREATE TABLE " + non_ascii_schema + "." + non_ascii_table + " (名 VARCHAR(20))");
            conn.closeSync();
       })
      .toss();
      
	////////////////////////////////////////////////////////////////////////////////
	//
	msg = myname + ': Test 29: schema name has non-ascii characters';
	//
	////////////////////////////////////////////////////////////////////////////////
	var expected = {
		numRowsRead : 9,
		numRowsLoaded : 9,
		numRowsCommitted : 9,
		numRowsSkipped : 0,
		numRowsRejected : 0,
		numRowsDeleted : 0,
		errorMessageCode: "NONE"}
	frisby.create(msg)
		  .post(siteURL + loadServerEndPoint,
				{
					"LOAD_SOURCE": "SERVER",
					"SERVER": {
						"SERVER_FILE_PATH": "Linuxディストリビューション.csv"
					},
					"LOAD_PARAMETERS": {
						"SCHEMA": non_ascii_schema,
						"TABLE": "LINUX",
						"LOAD_ACTION": "REPLACE",
					}
				},
				{json: true })
		  .auth(user, pass)
		  .expectStatus(200)
		  .expectHeaderContains('Content-Type', 'json')
		  .inspectJSON()
		  .expectJSON(
					  {
						  message: "LOAD started successfully.",
						  errorMessageCode: "NONE",
						  resultCode: "SUCCESS",
						  result: {
							  TABLE: "LINUX",
							  SCHEMA: non_ascii_schema
						  }
					  })
		  .afterJSON(function (json) {
						 loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
						 var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
						 loadUtils.validateLoadResult(bodyJSON, expected);
					 })
		  .toss();      
}


if(smpType != 'ENTRY') {
	////////////////////////////////////////////////////////////////////////////////
	//
	msg = myname + ': Test 30: both schema name and table name have non-ascii characters';
	//
	////////////////////////////////////////////////////////////////////////////////
	var expected = {
		numRowsRead : 9,
		numRowsLoaded : 9,
		numRowsCommitted : 9,
		numRowsSkipped : 0,
		numRowsRejected : 0,
		numRowsDeleted : 0,
		errorMessageCode: "NONE"}
	frisby.create(msg)
		  .post(siteURL + loadServerEndPoint,
				{
					"LOAD_SOURCE": "SERVER",
					"SERVER": {
						"SERVER_FILE_PATH": "Linuxディストリビューション.csv"
					},
					"LOAD_PARAMETERS": {
						"SCHEMA": non_ascii_schema,
						"TABLE": non_ascii_table,
						"LOAD_ACTION": "REPLACE",
					}
				},
				{json: true })
		  .auth(user, pass)
		  .expectStatus(200)
		  .expectHeaderContains('Content-Type', 'json')
		  .inspectJSON()
		  .expectJSON(
					  {
						  message: "LOAD started successfully.",
						  errorMessageCode: "NONE",
						  resultCode: "SUCCESS",
						  result: {
							  TABLE: non_ascii_table,
							  SCHEMA: non_ascii_schema
						  }
					  })
		  .afterJSON(function (json) {
						 loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
						 var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
						 loadUtils.validateLoadResult(bodyJSON, expected);
					 })
		  .toss();

	msg = myname + ': Tear down non-ascii schema and its tables';
	frisby.create(msg)
		  .post(siteURL + server.asperaEndPoint()+'getUploadSizeCap')
		  .auth(user, pass)
		  .expectStatus(200)
		  .expectHeader('content-type', 'application/json')
		  .after(function () {
				var conn = ibmdb.openSync(connString);
				conn.querySync("DROP TABLE " + non_ascii_schema + ".LINUX");
				conn.querySync("DROP TABLE " + non_ascii_schema + "." + non_ascii_table);
				conn.closeSync();
		   })
		  .toss();
}	 
