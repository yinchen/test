//////////////////////////////////////////////////////
// ENDPOINT - POST /load/server
// TEST - test different WARNINGCOUNT parameters
// COMMENT - none
// NEEDS_REWORK - N
// NEEDS_REWORK_COMMENT - none

var frisby = require('frisby');
var server = require('../lib/server.js');
var path = require('path');
var expect = require('expect');
var loadUtils = require('../lib/loadutils.js');

// Ignore rejected certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var siteURL = server.siteURL();
var loadServerEndPoint = server.loadServerEndPoint();
var tableName = "TINY_TABLE";
var tableSchema = server.tabSchema();
var user = server.userID();
var pass = server.password();
var myname = path.basename(__filename);

//Global setup for all tests
frisby.globalSetup({
    timeout: (30 * 1000)
})

var numRowsRead = '';
var numRowsLoaded = '';
var numRowsCommitted = '';
var numRowsDeleted = '';
var numRowsSkipped = '';

////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 1: WARNINGCOUNT triggers SQL3502n';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
                "LOAD_SOURCE": "SERVER",
                "SERVER": {
                    "SERVER_FILE_PATH": "load_warnings.csv"
                },
                "LOAD_PARAMETERS": {
                    "SCHEMA": tableSchema,
                    "TABLE": tableName,
                    "WARNINGCOUNT": "1"
                }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(200)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                      message: 'LOAD started successfully.',
                      errorMessageCode: 'NONE',
                      resultCode: 'SUCCESS',
                      result: {
                          TABLE: tableName,
                          SCHEMA: tableSchema
                    }
                  })
      .afterJSON(function (json) {
                     loadUtils.wait_for_load_to_complete(json.result.LOAD_ID, 50);
                     var bodyJSON = loadUtils.get_loadid(json.result.LOAD_ID);
                     //loadUtils.validateLoadResult(bodyJSON, numRowsRead, numRowsLoaded, numRowsCommitted, numRowsDeleted, numRowsSkipped, "ERROR");
                     expect(bodyJSON.result.result.WARNING).toMatch('Routine "SYSPROC.ADMIN_CMD" execution has completed, but at least one error, "SQL3502", was encountered during the execution. More information is available.. SQLCODE=20397, SQLSTATE=01H52, DRIVER=4.22.36');
                 })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 2: WARNINGCOUNT is an invalid value (-1) - negative test';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
              "LOAD_SOURCE": "SERVER",
              "SERVER": {
                "SERVER_FILE_PATH": "load_warnings.csv"
              },
              "LOAD_PARAMETERS": {
                "SCHEMA": tableSchema,
                "TABLE": tableName,
                "WARNINGCOUNT": "-1"
              }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                    message: 'The value for JSON input key WARNINGCOUNT must be a value greater than or equal to 0.',
                    errorMessageCode: 'ERROR',
                    resultCode: 'ERROR',
                    result: []
                  })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 3: WARNINGCOUNT is a non-integer value - negative test';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
              "LOAD_SOURCE": "SERVER",
              "SERVER": {
                "SERVER_FILE_PATH": "load_warnings.csv"
              },
              "LOAD_PARAMETERS": {
                "SCHEMA": tableSchema,
                "TABLE": tableName,
                "WARNINGCOUNT": "123a1jaasg"
              }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                    message: 'The value for JSON input key WARNINGCOUNT must be an integer value greater than or equal to 0.',
                    errorMessageCode: 'ERROR',
                    resultCode: 'ERROR',
                    result: []
                  })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
// WARNINGCOUNT is designed to be an un-signed integer that can take values
// up to 4294967296. We are testing the max value + 1 here to generate an
// overflow error.
var msg = myname + ': Test 4: WARNINGCOUNT is an overflow value - negative test';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
              "LOAD_SOURCE": "SERVER",
              "SERVER": {
                "SERVER_FILE_PATH": "load_warnings.csv"
              },
              "LOAD_PARAMETERS": {
                "SCHEMA": tableSchema,
                "TABLE": tableName,
                "WARNINGCOUNT": "4294967296"
              }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                    message: 'The value for JSON input key WARNINGCOUNT must be an integer value greater than or equal to 0.',
                    errorMessageCode: 'ERROR',
                    resultCode: 'ERROR',
                    result: []
                  })
      .toss();



////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 5: WARNINGCOUNT is an empty string - negative test';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
              "LOAD_SOURCE": "SERVER",
              "SERVER": {
                "SERVER_FILE_PATH": "load_warnings.csv"
              },
              "LOAD_PARAMETERS": {
                "SCHEMA": tableSchema,
                "TABLE": tableName,
                "WARNINGCOUNT": ""
              }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                    message: 'The value for JSON input key WARNINGCOUNT must be an integer value greater than or equal to 0.',
                    errorMessageCode: 'ERROR',
                    resultCode: 'ERROR',
                    result: []
                  })
      .toss();




////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 6: WARNINGCOUNT is a blank string - negative test';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
              "LOAD_SOURCE": "SERVER",
              "SERVER": {
                "SERVER_FILE_PATH": "load_warnings.csv"
              },
              "LOAD_PARAMETERS": {
                "SCHEMA": tableSchema,
                "TABLE": tableName,
                "WARNINGCOUNT": " "
              }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                    message: 'The value for JSON input key WARNINGCOUNT must be an integer value greater than or equal to 0.',
                    errorMessageCode: 'ERROR',
                    resultCode: 'ERROR',
                    result: []
                  })
      .toss();




////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 7: WARNINGCOUNT is a JSON object - negative test';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
              "LOAD_SOURCE": "SERVER",
              "SERVER": {
                "SERVER_FILE_PATH": "load_warnings.csv"
              },
              "LOAD_PARAMETERS": {
                "SCHEMA": tableSchema,
                "TABLE": tableName,
                "WARNINGCOUNT": { "hello": "there"}
              }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                    message: 'The value for JSON input key WARNINGCOUNT must be an integer value greater than or equal to 0.',
                    errorMessageCode: 'ERROR',
                    resultCode: 'ERROR',
                    result: []
                  })
      .toss();




////////////////////////////////////////////////////////////////////////////////
//
var msg = myname + ': Test 8: WARNINGCOUNT is a JSON array - negative test';
//
////////////////////////////////////////////////////////////////////////////////
frisby.create(msg)
      .post(siteURL + loadServerEndPoint,
            {
              "LOAD_SOURCE": "SERVER",
              "SERVER": {
                "SERVER_FILE_PATH": "load_warnings.csv"
              },
              "LOAD_PARAMETERS": {
                "SCHEMA": tableSchema,
                "TABLE": tableName,
                "WARNINGCOUNT": ["blah"]
              }
            },
            {json: true })
      .auth(user, pass)
      .expectStatus(400)
      .timeout(30000)
      .expectHeaderContains('Content-Type', 'json')
      .inspectJSON()
      .expectJSON(
                  {
                    message: 'The value for JSON input key WARNINGCOUNT must be an integer value greater than or equal to 0.',
                    errorMessageCode: 'ERROR',
                    resultCode: 'ERROR',
                    result: []
                  })
      .toss();

